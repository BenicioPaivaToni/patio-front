const express = require('express')
var passwordHash = require('password-hash');
const validarJWT = require('./middleware/validarJWT');
const {check, validationResult, } = require('express-validator');
var app = express();
var mysql = require('mysql');
const pool = require('./pool-factory');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const cors = require('cors');

const connectionMiddleware = require('./connection-middleware');
//sha1$833491a0$1$1e533d92951ed7b5323819b0b8cac83078472396

app.use(cors());
app.use(helmet());
app.use(bodyParser.urlencoded({limit: "10mb", extended: true, parameterLimit: 10000}));
app.use(bodyParser.json({limit: "10mb"}));
app.use(connectionMiddleware(pool));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    res.setHeader('X-Requested-With', 'XMLHttpRequest');
    next();
});

var apiRoutes = express.Router();

function query(connection, sql, parameters) {
    return new Promise(function (resolve, reject) {
        connection.query(sql, parameters, (err, itens) => {
            if (err)
                reject(err);
            resolve(itens);
        });
    });
}

apiRoutes.get('/', function (req, res) {
    res.json({message: 'API aMenu!!! '});
});

apiRoutes.get('/relatorio-operadores', function (req, res) {
    res.json(null);
});
apiRoutes.get('/relatorio-filtros-veiculo', function (req, res) {
    res.json([
        {label: "PÁTIO", field: "id_patio", tabela: "ncv", operador: "list"},
        {label: "DATA LIBERAÇÃO", field: "data", tabela: "ncv_liberacao", operador: "date"},
        {label: "DATA APREENÇÃO", field: "data", tabela: "ncv_apreensao", operador: "date"},
        {label: "STATUS", field: "status", tabela: "ncv", operador: "list"},
        {label: "TIPO VEICULO", field: "tipo_veiculo", tabela: "ncv", operador: "number"},
        {label: "KM PERCORRIDO", field: "km_percorrido", tabela: "ncv_guinchos", operador: "number"},
        {label: "MOTORISTA", field: "id_motorista", tabela: "ncv_guinchos", operador: "number"},
        {label: "TIPO DESCONTO", field: "tipo_desconto", tabela: "ncv_liberacao", operador: "list"},
        {label: "FORMA PAGTO", field: "tipo", tabela: "ncv_pagamentos", operador: "list"}
    ]);
});

apiRoutes.post('/relatorio-geral', function (req, res) {

    if (Array.isArray(req.body)) {

        let list = req.body;
        let consulta = "";
        let tabelas = list.map(function (item) {
            //console.log(item.filtro);
            return item.filtro.tabela;
        });

        if (tabelas.indexOf("ncv") !== -1) {
            consulta = consulta + "SELECT *";
            consulta = consulta + " FROM";
            consulta = consulta + " ncv as ncv_t1";
        }

        if (tabelas.indexOf("ncv_guinchos") !== -1) {
            let t_as = " ncv_guinchos as ncv_guinchos_t2";
            if (tabelas.indexOf("ncv") !== -1) {
                consulta = consulta + " LEFT JOIN" + t_as;
                consulta = consulta + " ON ncv_t1.id = ncv_guinchos_t2.ncv";
            } else {
                consulta = consulta + " SELECT *";
                consulta = consulta + " FROM" + t_as;
            }
        }

        if (tabelas.indexOf("ncv_liberacao") !== -1) {
            let t_as = " ncv_liberacao as ncv_liberacao_t3";
            if (tabelas.indexOf("ncv") !== -1) {
                consulta = consulta.replace("*", "*, ncv_liberacao_t3.`data` as data_liberacao");
                consulta = consulta + " LEFT JOIN" + t_as;
                consulta = consulta + " ON ncv_t1.id = ncv_liberacao_t3.ncv";
            } else {
                consulta = consulta + " SELECT *, ncv_liberacao_t3.`data` as data_liberacao";
                consulta = consulta + " FROM" + t_as;
            }
        }

        if (tabelas.indexOf("ncv_apreensao") !== -1) {
            let t_as = " ncv_apreensao as ncv_apreensao_t4";
            if (tabelas.indexOf("ncv") !== -1) {
                consulta = consulta.replace("*", "*, ncv_apreensao_t4.`data` as data_apreensao");
                consulta = consulta + " LEFT JOIN" + t_as;
                consulta = consulta + " ON ncv_t1.id = ncv_apreensao_t4.ncv";
            } else {
                consulta = consulta + " SELECT *, ncv_apreensao_t4.`data` as data_apreensao";
                consulta = consulta + " FROM" + t_as;
                ;
            }
        }

        consulta = consulta + " WHERE";

        //list = list.sort((a, b) => (a.filtro.field > b.filtro.field) ? 1 : -1);

        let groupBy = function () {
            return list.reduce(function (rv, item) {
                (rv[item.filtro.field] = rv[item.filtro.field] || []).push(item);
                return rv;
            }, {});
        };

        let paramsGroup = groupBy();

// => {3: ["one", "two"], 5: ["three"]}
        for (let param in paramsGroup) {
            consulta =  consulta + " (";
            paramsGroup[param].forEach(function (item, i) {
                if (item.filtro.field === 'id_patio' && item.filtro.value !== '') {
                    consulta = consulta + " ncv_t1.id_patio " + item.operador.value + " " + item.valor01;
                }
                if (item.filtro.field === 'status' && item.filtro.value !== '') {
                    consulta = consulta + " ncv_t1.status " + item.operador.value + " '" + item.valor01 + "'";
                }
                if (item.filtro.field === 'tipo_veiculo' && item.filtro.value !== '') {
                    consulta = consulta + " ncv_t1.tipo_veiculo" + item.operador.value + " " + item.valor01 + "";
                }
                if (item.filtro.field === 'id_motorista' && item.filtro.value !== '') {
                    consulta = consulta + " ncv_guinchos_t2.id_motorista " + item.operador.value + " " + item.valor01;
                }
                if (item.filtro.field === 'km_percorrido' && item.filtro.value !== '') {
                    consulta = consulta + " ncv_guinchos_t2.km_percorrido BETWEEN " + item.valor01 + " AND " + item.valor02;
                }
                if (item.filtro.field === 'tipo_desconto' && item.filtro.value !== '') {
                    consulta = consulta + " ncv_liberacao_t3.tipo_desconto = 'NEGOCIAÇÃO'";
                }
                if (item.filtro.field === 'forma_pagamento' && item.filtro.value !== '') {
                    consulta = consulta + " ncv_liberacao_t3.forma_pagamento " + item.operador.value + " " + item.valor01;
                }
                if (item.filtro.field === 'data' && item.operador.value !== 'BETWEEN' && item.filtro.tabela == 'ncv_liberacao') {
                    consulta = consulta + " ncv_liberacao_t3.`data` " + item.operador.value + " CAST('" + (item.valor01.substr(0, 10)) + "' AS DATE)";
                }
                if (item.filtro.field === 'data' && item.operador.value === 'BETWEEN' && item.filtro.tabela == 'ncv_liberacao') {
                    consulta = consulta + " (ncv_liberacao_t3.`data` BETWEEN CAST('" + (item.valor01.substr(0, 10)) + "' AS DATE) AND CAST('" + (item.valor02.substr(0, 10)) + "' AS DATE))";
                }
                if (item.filtro.field === 'data' && item.operador.value !== 'BETWEEN' && item.filtro.tabela == 'ncv_apreensao') {
                    consulta = consulta + " ncv_apreensao_t4.`data` " + item.operador.value + " CAST('" + (item.valor01.substr(0, 10)) + "' AS DATE)";
                }
                if (item.filtro.field === 'data' && item.operador.value === 'BETWEEN' && item.filtro.tabela == 'ncv_apreensao') {
                    consulta = consulta + " (ncv_apreensao_t4.`data` BETWEEN CAST('" + (item.valor01.substr(0, 10)) + "' AS DATE) AND CAST('" + (item.valor02.substr(0, 10)) + "' AS DATE))";
                }

                consulta = consulta + (paramsGroup[param].length - 1 === i ? "" : " OR");
            });
            consulta = consulta + ") AND";
        }

        consulta = consulta + " 1=1;";

        //console.log(list);
        console.log(consulta);

        req.connection.query(consulta, (err, itens) => {
            if (err) {
                res.json(err);
            } else {
                res.json(itens);
            }
        });
    } else {
        res.json([]);
    }
});

app.use('/', apiRoutes);

var port = process.env.PORT || 1337;

app.listen(port, function () {
    console.log('Example app listening on port 1337!');
});