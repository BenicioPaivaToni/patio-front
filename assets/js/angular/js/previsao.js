app.controller('previsaoCtrl', function ($scope, $location, $http, urlServidor,myFunctions) {


    $scope.novo = function (ev) {
        $location.path('previsao-novo');
    };

    buscaPatios().then(function (result) {
        $scope.patios = result;
    });
    buscaGrupos().then(function (result) {
        $scope.grupos = result;
    });

    $scope.dados = { patio: null, competencia: null};

    $scope.limpar = function (ev) {

        $scope.dados.patio = null ;
        $scope.dados.competencia = null ;
    };

    function buscaPatios(idgrupo) {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    };
    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        $location.path('previsao-editar').search({dados});
    };
    function buscaGrupos() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/grupos/listar').then(function (response) {

                let dados = response.data;
                dados.sort(function (a, b) {
                    if (a.descricao < b.descricao) {
                        return 1;
                    }
                    if (a.descricao > b.descricao) {
                        return -1;
                    }
                    return 0;
                });
                resolve(dados);
            });
        })

    };

    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("previsao-orçamento.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }
    $scope.pesquisar = function (ev) {

        $http.put(urlServidor.urlServidorChatAdmin + '/previsao-orcamentaria/listar', $scope.dados).then(function (response) {


            let dados = response.data;
            
            
            

            $scope.dadosList = dados;
        });
    };
   
    $scope.gerarPdf = function () {
        var doc = new jsPDF({orientation: "landscape"});
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: {patio: {halign: 'left'}},
            body: $scope.dados,
            columns: [
                {header: '*', dataKey: 'id'},
                {header: 'Grupo', dataKey: 'grupo'},
                {header: 'Categoria', dataKey: 'categoria'},
                {header: 'Competencia', dataKey: 'competencia'},
                {header: 'Valor', dataKey: 'valor'},
                {header: 'Patio', dataKey: 'patio'}
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                doc.text('Previsões com competencia em ' + $scope.dados.competencia.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22)

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: {top: 30},
        });
        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('relatorio-previsão.pdf')
    };

    $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', {idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa')}).then(function (response) {
        $scope.patios = response.data;
    });


});