app.controller('empresasCtrl', function ($scope, $location, $http, urlServidor) {

    $scope.novo = function (ev, id) {
        $location.path('empresas-novo');
    };
    $scope.limpar = function (ev) {
        $scope.dados.nome_empresa = ''
        $scope.dados.repres_nome = ''
        $scope.dadosList = []
    };

    $scope.dados = {
        nome_empresa: '',
        repres_nome: ''
    };


    $scope.pesquisar = function (ev) {

        $scope.dadosList = []

        let dadosConsulta = {
            nome: $scope.dados.nome_empresa,
            representante: $scope.dados.repres_nome
        }

        $http.put(urlServidor.urlServidorChatAdmin + '/empresas/listar', dadosConsulta).then(function (response) {

            let dados = response.data

            if (dados.length === 0) {

                myFunctions.showAlert('Pesquisa não encontrou registros! Verifique!')

            }else{

               $scope.dadosList = dados 
            }

        })
    }
    


    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        $location.path('empresas-editar').search({ dados })
    }



    $scope.gerarPlanilha = function () {

        if ($scope.dadosList){

            alasql('SELECT * INTO XLSX("empresas.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);

        }else{

            myFunctions.showAlert("Sem dados para gerar Planilha!") 

        }
    }



    $scope.gerarPdf = function () {
        
        if ($scope.dadosList){
        
            var doc = new jsPDF({orientation: "landscape"});
            var totalPagesExp = '{total_pages_count_string}'

            doc.setFontSize(12);
            doc.autoTable({

                body: $scope.dadosList,
                columns: [
                    {header: '*', dataKey: 'id'},
                    {header: 'Nome Empresa', dataKey: 'nome_empresa'},
                    {header: 'CNPJ', dataKey: 'cnpj'},
                    {header: 'Representante', dataKey: 'repres_nome'},
                    {header: 'Telefone', dataKey: 'repres_telefone'},
                    {header: 'Cidade', dataKey: 'cidade'},
                    {header: 'Estado', dataKey: 'uf'},
                ],
                bodyStyles: {
                    margin: 10,
                    fontSize: 08,
                },
                didDrawPage: function (data) {
                    // Header
                    doc.setFontSize(18)
                    doc.setTextColor(40)
                    doc.setFontStyle('normal')

                    // Footer
                    var str = 'Pagina ' + doc.internal.getNumberOfPages()
                    // Total page number plugin only available in jspdf v1.0+
                    if (typeof doc.putTotalPages === 'function') {
                        str = str + ' de ' + totalPagesExp
                    }

                    doc.setFontSize(10)

                    // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                    var pageSize = doc.internal.pageSize
                    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                    doc.text(str, data.settings.margin.left, pageHeight - 10)
                },
                margin: {top: 30},
            });
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp)
            }

            doc.save('empresas.pdf')

        }else{

            myFunctions.showAlert("Sem dados para gerar PDF!") 

        }
    };


});