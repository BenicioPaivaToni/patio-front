angular.module('app')
  .controller("comissaoMotoristasCtrl", function ($scope, $http, urlServidor, myFunctions) {

    $scope.dados = {
      data_Inicio: new Date(),
      data_Final: new Date(),
      id_motorista: "",
      patio: ""
    }


    

    const getPatios = async () => {
      const response = await $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') })
      return await response.data

    }

    const getEmpresas = async () => {
      const response = await $http.put(urlServidor.urlServidorChatAdmin + '/empresas_reboques/listar')
      return await response.data
    }

    async function init() {
      $scope.patios = await getPatios()
      $scope.empresas = await getEmpresas()
    }

    init()

    $scope.getMotoristaByEmpresa = async () => {
      const response = await $http.get(urlServidor.urlServidorChatAdmin + '/motoristas/listar-motoristas', { params : { id_empresa_reboque: $scope.dados.empresa } } )
      const { data } = response
      $scope.motoristas = data
    }

    $scope.limpar = () =>{
      
    $scope.dados = {
      data_Inicio: new Date(),
      data_Final: new Date(),
      id_motorista: "",
      patio: ""
    }
    }

    function getMoney(str) {
      var valor = str.replace(/[^\d,]/g, ''); //remove todos os caracteres menos numeros e virgula
      valor = valor.replace(',', '.'); //troca virgula por ponto
      return parseFloat(valor);

    }

    $scope.datalist = []

    $scope.Buscar = async (dados) => {

      const response = await $http.get(urlServidor.urlServidorChatAdmin + '/motoristas/comissoes', { params: dados })
      const { data } = response
      $scope.length = data.length
      if (data == false) return myFunctions.showAlert('Filtro não encontrou registros, verifique!')



      const DataMapped = data.map((item) => ({

        ...item,
        nome:`${item.nome.slice(0,17)}...`,
        Perc_Comissao: `${item.Perc_Comissao || 0.0}%`,
        Valor_Guincho: `${myFunctions.numberToReal(item.Valor_Guincho || 0.0)}`,
        valor_comissao_number: getMoney(item.valor_comissao || '0,0'),
      }));



      const Dados = await criarTotalMotoristas(DataMapped)
      const Dataarr = Object.values(Dados)

        Dataarr.forEach(Item => {
           $scope.datalist=Item.map(Moto =>({...Moto}))
        })
     

    }




    const criarTotalMotoristas = async (lancamentos) => {
      const Lanca = lancamentos

      const groupBy = (x, f) => x.reduce((a, b) => ((a[f(b)] ||= []).push(b), a), {});

      const groupedMotoristas = groupBy(Lanca, v => v.nome)

      const arrgroupedMotoristas = new Array(groupedMotoristas)

      const Total = await geraValorComissao(...arrgroupedMotoristas)
      return Total


    }




    const geraValorComissao = async (...arguments) => {
      let result;

      arguments.forEach(async (Item) => {


        for (const key in Item) {
          if (Object.hasOwnProperty.call(Item, key)) {
            const element = Item[key];

            const Istotal = element.reduce((accumulate, dados) => {
              return accumulate + dados.valor_comissao_number

            }, 0)


            const Motorista = `<div style="width:150px"><h4 style="font-size:14px;color:blue;padding-left:10px,padding-rigth:10px">TOTAL MOTORISTA</h4></div>`
            const total = `<div style="width:100px"><h4 style="font-size:14px;color:blue"> ${myFunctions.numberToReal(Istotal)}</h4></div>`


            element.push({ nome: Motorista, PATIO: total })

            result = arguments[0]

          }
        }
      })

      return result

    }







    $scope.gerarPlanilha = function () {



      const excellInfo = $scope.datalist.map(index => ({
        ...index,
        PATIO: index.PATIO.replace(/(<([^>]+)>)/ig, " "),
        nome: index.nome.replace(/(<([^>]+)>)/ig, " "),
      }))

      alasql('SELECT * INTO XLSX("relatorio-comissao-motoristas.xlsx",{headers:true}) FROM ?', [excellInfo]);
    };



    $scope.gerarPdf = function () {




console.log( $scope.datalist);


      const PDFInformações = $scope.datalist.map(index => ({
        ...index,
        PATIO: index.PATIO.replace(/(<([^>]+)>)/ig, " "),
        nome: index.nome.replace(/(<([^>]+)>)/ig, " "),
      }))

      var doc = new jsPDF({ orientation: "landscape" });
      var totalPagesExp = '{total_pages_count_string}'

      doc.setFontSize(10);
      doc.autoTable({

        columnStyles: {
          vencimento: { halign: 'left' },
        },

        body: PDFInformações,


        columns: [
          { header: 'NCV', dataKey: 'NCV' },
          { header: 'Nome', dataKey: 'nome' },
          { header: 'PATIO', dataKey: 'PATIO' },
          { header: 'Guincho', dataKey: 'Valor_Guincho' },
          { header: 'Comissão %', dataKey: 'Perc_Comissao' },
          { header: 'Ano', dataKey: 'ano' },
          { header: 'Cor', dataKey: 'cor' },
          { header: 'Apreensão', dataKey: 'data_apreensao' },
          { header: 'Marca/Modelo', dataKey: 'marca_modelo' },
          { header: 'Placa', dataKey: 'placa' },
          { header: 'Tipo Veiculo', dataKey: 'tipo_veiculo' },
          { header: 'Valor Comissão', dataKey: 'valor_comissao' },
        ],
        bodyStyles: {
          margin: 10,
          fontSize: 08,
        },
        didDrawPage: function (data) {
          // Header
          doc.setFontSize(18)
          doc.setTextColor(40)
          doc.setFontStyle('normal')

          doc.text('Comissões no Período de ' + $scope.dados.data_Inicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.data_Final.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22);

          // Footer
          var str = 'Pagina ' + doc.internal.getNumberOfPages()
          // Total page number plugin only available in jspdf v1.0+
          if (typeof doc.putTotalPages === 'function') {
            str = str + ' de ' + totalPagesExp
            var totalpaginas = totalPagesExp;
          }

          var strTotal = ''
          doc.setFontSize(10)

          // jsPDF 1.4+ uses getWidth, <1.4 uses .width
          var pageSize = doc.internal.pageSize
          var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()

          doc.text(str + '                        ' + strTotal, data.settings.margin.left, pageHeight - 10)
        },
        margin: { top: 30 },
      });
      if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp)
      }

      doc.save('Comissão_Motorista.pdf');
    }











  });