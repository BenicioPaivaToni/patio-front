angular.module('app').controller('chamadosCtrl', function ($scope, $location, $http, urlServidor) {

    $scope.limpar = function (ev) {
        $scope.dados.data_chamados = null;
        dataInicio: null;
        dataFinal: null;
        $scope.dados.patio = null;
        $scope.dados.status = null;
        $scope.dados.orderby = null;
    };
    let hoje = new Date();
    $scope.dados = {
        dataInicio: new Date(hoje.getFullYear(), hoje.getMonth(), hoje.getDate() - 2),
        dataFinal: new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0),
        idEmpresa: localStorage.getItem('id_empresa'),
        origem: 'chamados',
        patio: '',
        usuario: localStorage.getItem('id_usuario'),
        orderby: 1
    };

    buscaPatios().then(function (result) {
        $scope.patios = result;
    });

    $scope.pesquisar = function (ev) {

        if (($scope.dados.dataInicio == null || $scope.dados.dataFinal == null) ||
            ($scope.dados.dataInicio == undefined || $scope.dados.dataFinal == undefined)) {
            myFunctions.showAlert('Obrigatório informar periodo para filtro!')
        } else {

            $scope.dados.dataFinal.setHours(23, 59);
            $http.get(urlServidor.urlServidorChatAdmin + '/chamados/listar', { params: $scope.dados }).then(function (response) {

                let dados = response.data;
                $scope.dadosList = dados;
                
            });
        }
    };


    function buscaPatios() {

        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') }).then(function (response) {
                resolve(response.data);
            });
        });
    }

 


    $scope.detalhe = function (ev, id) {
        
     const dados =$scope.dadosList.filter( obj =>(obj.id === id))

      const _Id =dados[0].id



      window.open(`/#/chamados-editar?id=${_Id} `);


    };



    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("chamados.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({orientation: "landscape"});
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: {empresa: {halign: 'left'},
                nome: {halign: 'left'},
                endereco: {halign: 'left'},
                },
            body: $scope.dadosList,
            columns: [
                {header: 'Nome Chamador', dataKey: 'nome_chamador'},
                {header: 'Data/Hora', dataKey: 'data_hora_chamador'},
                {header: 'Motorista', dataKey: 'nome_usuario_motorista'},
                {header: 'Status', dataKey: 'status'},
                {header: 'NCV', dataKey: 'ncv'},
                {header: 'Patio', dataKey: 'patio'},
                              
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 8,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

               // doc.text('Relátorio de chamados')

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: {top: 30},
        });

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        } 
       
        doc.save('chamados.pdf')
    }


})