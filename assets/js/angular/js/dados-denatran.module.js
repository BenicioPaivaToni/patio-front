angular
  .module("app")
  .controller(
    "dadosDenatranCtrl",
    function ($scope, $http, myFunctions, urlServidor, $mdDialog) {
      /* Busca de Dados Denatran */

      $scope.state = {
        patios: null,
        patiosGrupo: null,
        dataInicio: new Date(),
        dataFinal: new Date(),
        ncv_placas: "",
        id_patio: "",
        id_patioGrupo: "",
        dados: "",
        dados_ncv_placa: "",
      };

      $scope.methods = {
        async buscaPatios() {
          $scope.state.patios = await $http
            .put(
              urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
              {
                idUsuario: localStorage.getItem("id_usuario"),
                idEmpresa: localStorage.getItem("id_empresa"),
              }
            )
            .then(({ data }) => data.filter((item) => item.status == !0));
        },
        async buscaPatiosGrupos() {
          $scope.state.patiosGrupo = await $http
            .get(`${urlServidor.urlServidorChatAdmin}/grupos-patios/listar`)
            .then(({ data }) => data.filter((item) => item.ativo === 1));
        },

        clear() {
          $scope.state = {
            ...$scope.state,
            dataInicio: new Date(),
            dataFinal: new Date(),
            id_patio: null,
            id_patioGrupo: null,
          };
        },

        jsonParser(data) {
          if (!data) return;
          console.info("iniciando jsonParser");
          return JSON.parse(data);
        },

        createInterval(length = 0) {
          const ms = length * 5000;
          const seconds = ms / 1000;
          const minutes = seconds / 60;
          return { ms, seconds, minutes };
        },

        verificaTimer(ms) {
          ms = ms - 5000;
          if (ms <= 0) {
            return 0;
          } else {
            return 1;
          }
        },

        transforminHours(minutes) {
          if (minutes > 60) {
            // console.log(minutes);
            return Math.floor(minutes / 60) === 1
              ? `${Math.floor(minutes / 60)} hora`
              : `${Math.floor(minutes / 60)} horas`;
          } else if (minutes <= 1) {
            return `${1} minuto`;
          } else {
            if (Math.round(minutes) === 1) {
              return `${Math.round(minutes)} minuto`;
            } else {
              return `${Math.round(minutes)} minutos`;
            }
          }
        },

        onInit() {
          this.buscaPatios();
          this.buscaPatiosGrupos();
        },
        parserRestricaoMessage(data) {
          try {
            if (!data) {
              return "";
            }
            if ("Message" in data) {
              return "Erro na Busca.";
            }
            return data.Result.BIN.RestricaoJudicial === "1"
              ? "Restrição encontrada"
              : "Nada consta";
          } catch (error) {
            return "";
          }
        },

        dataHoraReqGenerator(data) {
          console.log(data);

          if (!data) return "";
          if ("Request" in data) {
            return data.Request.DataHoraRequisicao;
          }
        },
        async showAlert(alert, returnError = true) {
          await myFunctions.showAlert(alert);
          if (returnError) {
            throw new Error(alert);
          }
        },

        async showModal(html, title) {
          $scope.modal = () => {
            const confirm = $mdDialog
              .confirm()
              .title(title)
              .htmlContent(html)
              .ariaLabel("Lucky day")
              .ok("Sim")
              .cancel("Cancelar");

            return $mdDialog
              .show(confirm)
              .then(() => {
                return true;
              })
              .catch((err) => {
                return false;
              });
          };
          return await $scope.modal();
        },
      };
      $scope.methods.onInit();

      $scope.rules = {
        methods: $scope.methods,
        patioOrGroupCannotBeEmpty() {
          this.methods.showAlert(
            "Nenhum pátio ou grupo de pátios foi selecionado, verifique!"
          );
        },
        renavamNotExists() {
          return this.methods.showModal(
            `
          <div>
            <h4>
              Veiculos sem Renavam encontrados, deseja iniciar
              atualização Detran ?
            </h4>
            <h4>
             **  Somentes os com Pendência do renavam serão atualizados
            </h4>
          </div>
        `,
            "Veiculos sem Renavam"
          );
        },

        notContainsAtt(dados) {
          const containsErrorRenavam = this.containsErrorRenavam(dados);
          const notContainsAtt = dados.filter(
            (data) => !data.dados_denatran_data
          );
          const arr = new Array(...containsErrorRenavam, ...notContainsAtt);
          return arr;
        },
        containsErrorRenavam(dados) {
          return dados.filter((data) => data.Message);
        },
        attExists() {
          return this.methods.showModal(
            `
          <div>
            <h4>
              Veiculos com historico de processamento encontrado, deseja realizar uma atualização completa ? 
            </h4>
            <h4>
             **  Clicando em não, somente os não atualizados, ou com erro de atualização, serão atualizados.
            </h4>
          </div>
        `,
            "Veiculos com historico de processamento encontrado"
          );
        },
      };

      $scope.useCases = {
        methods: $scope.methods,
        rules: $scope.rules,
        state: $scope.state,

        async buscaDados(isNormalSearch = true) {
          try {
            const { ncv_placas } = this.state;

            if (isNormalSearch) {
              console.info("Seguindo fluxo normal");
              this.state.dados = await this.buscaDadosPorDataPatio(
                this.state,
                this.rules,
                this.methods
              );
              console.log(this.state.dados);
              if (!this.state.dados.length) {
                this.methods.showAlert(
                  "Nenhum registro encontrado!<br/><br/>Verifique parâmetros da pesquisa!"
                );
              }
            } else {
              console.info("Seguindo fluxo busca por ncv ou placa");
              this.state.dados_ncv_placa = await this.buscaDadosPortNcvOuPlaca(
                ncv_placas,
                this.rules,
                this.methods
              );
              console.log(this.state.dados_ncv_placa);

              if (!this.state.dados_ncv_placa.length) {
                this.methods.showAlert(
                  "Nenhum registro encontrado!<br/><br/>Verifique parâmetros da pesquisa!"
                );
              }
            }
          } catch (error) {
            console.error(error);
          }
        },
        async buscaDadosPortNcvOuPlaca(ncv_placa, rulesm, methods) {
          const infos = ncv_placa.split(";");
          let mappedInfos;
          if (infos) {
            mappedInfos = infos.map((item) => ({
              type: Number(item) ? "id" : "placa",
              identifier: Number(item) ? Number(item) : item.toUpperCase(),
            }));
          }

          const response = Promise.all(
            mappedInfos.map(async (item) => {
              const { data } = await $http.post(
                `${urlServidor.urlServidorChatAdmin}/integracoes/detran-placa-ncv`,
                {
                  ...item,
                }
              );
              return data;
            })
          );

          const data = await response;
          return data.map((item) => {
            const parsedJson = methods.jsonParser(item.data);

            return {
              ...item,
              dataHoraDenatran: methods.dataHoraReqGenerator(parsedJson),
              restricaoMessage: methods.parserRestricaoMessage(parsedJson),
              data_hora_importacao: moment(item.data_hora_importacao)
                .locale("pt")
                .format("LLLL"),
              tipo_ncv: this.typeGenerator(
                item.restricao,
                item.codigo_financeira
              ),
              data_apreensao: moment(item.data_apreensao)
                .locale("pt")
                .format("LLLL"),
            };
          });
        },
        async buscaDadosPorDataPatio(
          { id_patio, id_patioGrupo, dataInicio, dataFinal },
          rules,
          methods
        ) {
          if (!id_patio && !id_patioGrupo) {
            rules.patioOrGroupCannotBeEmpty();
          }

          const { data } = await $http.get(
            `${urlServidor.urlServidorChatAdmin}/separacao-leilao/listar-dados-detran`,
            {
              params: {
                data_inicial: dataInicio,
                data_final: dataFinal,
                id_patios: id_patio,
                id_grupo_patios: id_patioGrupo,
              },
            }
          );

          const notContainsRenavam = data.filter((data) => !data.renvam);

          if (notContainsRenavam) {
            const response = await rules.renavamNotExists(
              "answerNotContainsRenavam"
            );
            if (response) {
              console.info("iniciando busca Detran");
              await this.atualizaDetran(notContainsRenavam, methods);
            }
          }

          return data.map((item) => {
            const parsedJson = methods.jsonParser(item.dados_denatran_data);
            return {
              ...item,
              dados_denatran_data: parsedJson,
              dataHoraDenatran: methods.dataHoraReqGenerator(parsedJson),
              restricaoMessage: methods.parserRestricaoMessage(parsedJson),
              tipo_ncv: this.typeGenerator(
                item.restricao,
                item.codigo_financeira
              ),
            };
          });
        },
        async atualizaDetran(dados, methods) {
          $scope.dadosFordetran = dados.map((item) => ({
            placa: item.placa,
            ncv: item.NCV,
            processado: 0,
          }));

          const timertoShow = methods.createInterval(
            $scope.dadosFordetran.length
          );

          $scope.ShowMessage = true;

          $scope.message = `Buscando Dados Dos Veiculos, por favor aguarde,\n
          Tempo estimado : ${methods.transforminHours(timertoShow.minutes)} `;

          await $http
            .post(
              `${urlServidor.urlServidorChatAdmin}/integracoes/grava-placas`,
              { dados: $scope.dadosFordetran }
            )
            .then(async () => {
              await $http.get(`https://sync-detran.herokuapp.com/processar`);
            });

          const { ms } = methods.createInterval($scope.dadosFordetran.length);

          const Interval = setInterval(async function () {
            const response = methods.verificaTimer(ms);
            if (response == 0) {
              $scope.message = "Processamento concluido com sucesso";
              clearInterval(Interval);
              this.buscaDadosPorDataPatio();
              $scope.ShowMessage = false;
            }
          }, 5000);
        },

        async atualizaDetranAll() {
          try {
            return await Promise.all(
              this.state.dados.map(async (dados) => {
                return (
                  dados.placa_ncv &&
                  dados.renavam &&
                  (await $http.post(
                    `${urlServidor.urlServidorChatAdmin}/integracoes/atualiza-dados-denatran`,
                    {
                      Placa: dados.placa_ncv,
                      Renavam: dados.renavam,
                      Documento:
                        dados.cpf_cnpj_cvenda || dados.cpf_cnpj_proprietario,
                    }
                  ))
                );
              })
            );
          } catch (error) {
            console.log(error);
          }
        },

        async atualizaDetranPartial(dados) {
          try {
            return await Promise.all(
              dados.map(async (dados) => {
                return (
                  dados.placa_ncv &&
                  dados.renavam &&
                  (await $http.post(
                    `${urlServidor.urlServidorChatAdmin}/integracoes/atualiza-dados-denatran`,
                    {
                      Placa: dados.placa_ncv,
                      Renavam: dados.renavam,
                      Documento:
                        dados.cpf_cnpj_cvenda || dados.cpf_cnpj_proprietario,
                    }
                  ))
                );
              })
            );
          } catch (error) {
            console.log(error);
          }
        },

        async atualizarDenatran(isNormalSearch = true) {
          if (!isNormalSearch) {
            this.methods.showAlert("Iniciando Processamento total");

            await this.atualizaDetranPartial(this.state.dados_ncv_placa).then(
              () => {
                this.methods.showAlert("Processamento concluido", false);
              }
            );
            return;
          }

          const notContainsAttAndContainsError = this.rules.notContainsAtt(
            this.state.dados
          );

          // Caso o numero de não att ou com erro seja igual a quantidade total, quer dizer que não há atualização. dai vamos processar todos
          if (
            this.state.dados.length - 1 ===
            notContainsAttAndContainsError.length
          ) {
            this.methods.showAlert("Iniciando Processamento total");
            await this.atualizaDetranAll().then(() => {
              this.methods.showAlert(
                "Aguarde a finalização do carregamento",
                false
              );
            });
            return;
          }

          //Caso já haja veiculos com historico de procssamento, vamos processar todos ou parcial com base na escolha do usuario

          const processaTodos = await this.rules.attExists(this.state.dados);
          if (processaTodos) {
            this.methods.showAlert("Iniciando Processamento total");

            await this.atualizaDetranAll().then(() => {
              this.methods.showAlert("Processamento concluido", false);
            });
            return;
          } else {
            this.methods.showAlert("Iniciando Processamento parcial");

            await this.atualizaDetranPartial(
              notContainsAttAndContainsError
            ).then(() => {
              this.methods.showAlert(
                "Aguarde a finalização do carregamento",
                false
              );
            });
            return;
          }
        },
        typeGenerator(restricao, codigoFinanceira) {
          /*             
                RENAJUD  = RESTRICOES:  "RENAJUD" + "CODIGO FINANCEIRA"
                JUDICIAL = RESTRICOES: "JUDICIAL" / "RENAJUD" SEM CODIGO FINANCEIRA
                ADMINISTRATIVO = TODOS OUTROS CASOS
           */
          if (!restricao) return;

          if (restricao.includes("RENAJUD") && codigoFinanceira !== null) {
            return "RENAJUD";
          }
          if (restricao.includes("RENAJUD") && codigoFinanceira === null) {
            return "JUDICIAL";
          }
          if (restricao.includes("JUDICIAL") && codigoFinanceira === null) {
            return "JUDICIAL";
          }
          if (restricao.includes("JUDICIAL") && codigoFinanceira !== null) {
            return "JUDICIAL";
          }
          return "ADMINISTRATIVO";
        },

        async GerarRenajudOrJudicial(Label, base = "normal") {
          const dados =
            base === "normal" ? this.state.dados : this.state.dados_ncv_placa;

          console.log(dados);
          if (Label === "RENAJUD") {
            this.RenajudOrJudicial = dados.filter(
              (item) => item.tipo_ncv === "RENAJUD"
            );
            this.GerarPDFJudicialOrRenajud(Label, this.RenajudOrJudicial);
          } else if (Label === "JUDICIAL") {
            this.RenajudOrJudicial = dados.filter(
              (item) => item.tipo_ncv === "JUDICIAL"
            );
            this.GerarPDFJudicialOrRenajud(Label, this.RenajudOrJudicial);
          } else {
            this.RenajudOrJudicial = dados.filter(
              (item) => item.tipo_ncv === "ADMINISTRATIVO"
            );
            this.GerarPDFJudicialOrRenajud(Label, this.RenajudOrJudicial);
          }
        },

        GerarPDFJudicialOrRenajud(label, dados) {
          if (dados.length !== 0) {
            var doc = new jsPDF({ orientation: "landscape" });

            var totalPagesExp = "{total_pages_count_string}";

            doc.setFontSize(10);

            doc.autoTable({
              columnStyles: {
                vencimento: { halign: "left" },
              },

              body: dados,
              columns: [
                { header: "NCV", dataKey: "NCV" },
                { header: "Patio", dataKey: "patio" },
                { header: "Data Apreensão", dataKey: "data_apreensao" },
                { header: "Placa", dataKey: "placa_ncv" },
                { header: "Marca/Modelo", dataKey: "marca_modelo_ncv" },
                { header: "Ano", dataKey: "ano" },
                { header: "Tipo do Veiculo", dataKey: "tipo_veiculo" },
                { header: "Atualização", dataKey: "data_hora_atualizacao" },
                { header: "Restrição/Tipo", dataKey: "tipo_ncv" },
              ],
              bodyStyles: {
                margin: 10,
                fontSize: 8,
              },

              didDrawPage: function (data) {
                // Header
                doc.setFontSize(18);
                doc.setTextColor(40);
                doc.setFontStyle("normal");

                doc.text(
                  "Veiculo(s) com restrição " + label,
                  data.settings.margin.left + 5,
                  18
                );
                doc.text(
                  "Quantidade: " +
                    dados.length +
                    "            " +
                    "Data: " +
                    new Date().toLocaleDateString("pt-BR"),
                  data.settings.margin.left + 5,
                  25
                );

                // Footer
                var str = "Pagina " + doc.internal.getNumberOfPages();
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === "function") {
                  str = str + " de " + totalPagesExp;
                  var totalpaginas = totalPagesExp;
                }

                doc.setFontSize(10);

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize;
                var pageHeight = pageSize.height
                  ? pageSize.height
                  : pageSize.getHeight();

                doc.text(
                  str + "                        ",
                  data.settings.margin.left,
                  pageHeight - 10
                );
              },
              margin: { top: 30 },
            });
            if (typeof doc.putTotalPages === "function") {
              doc.putTotalPages(totalPagesExp);
            }

            doc.save(`${label}/dados-detran.pdf`);
          } else {
            myFunctions.showAlert("Sem dados para gerar PDF!");
          }
        },

        gerarPlanilha(target) {
          const dados = target === "ncv" ? this.dados_ncv_placa : this.dados;

          if (!this.state.dados && !this.state.dados_ncv_placa) {
            myFunctions.showAlert("Sem dados para gerar PDF! Verifique!");
            return;
          }
          alasql(
            'SELECT * INTO XLSX("relatorio-dados-detran.xlsx",{headers:true}) FROM ?',
            [dados]
          );
        },

        gerarPDF(target) {
          console.log(this.dados, this.dados_ncv_placa);
          const dados = target === "ncv" ? this.dados_ncv_placa : this.dados;
          if (!this.state.dados && !this.state.dados_ncv_placa) {
            myFunctions.showAlert("Sem dados para gerar PDF! Verifique!");
            return;
          }
          var doc = new jsPDF({ orientation: "landscape" });

          var totalPagesExp = "{total_pages_count_string}";

          doc.setFontSize(10);

          doc.autoTable({
            columnStyles: {
              vencimento: { halign: "left" },
            },

            body: dados,
            columns: [
              { header: "NCV", dataKey: "NCV" },
              { header: "Patio", dataKey: "patio" },
              { header: "Data de Apreensão", dataKey: "data_apreensao" },
              { header: "Placa", dataKey: "placa_ncv" },
              { header: "Marca/Modelo", dataKey: "marca_modelo_ncv" },
              { header: "Ano", dataKey: "ano" },
              { header: "Tipo do Veiculo", dataKey: "tipo_veiculo" },
              {
                header: "Atualização Detran",
                dataKey: "data_hora_atualizacao",
              },
              { header: "Renavam", dataKey: "renavam" },
              { header: "Restrição", dataKey: "restricaoMessage" },
              { header: "Atualização Renavam", dataKey: "dataHoraDenatran" },
            ],
            bodyStyles: {
              margin: 10,
              fontSize: 8,
            },

            didDrawPage: function (data) {
              // Header
              doc.setFontSize(18);
              doc.setTextColor(40);
              doc.setFontStyle("normal");

              doc.text(
                "Relatorio veículos dados Denatran ",
                data.settings.margin.left + 5,
                18
              );
              doc.text(
                "Quantidade: " +
                  $scope.Dados.length +
                  "            " +
                  "Data: " +
                  new Date().toLocaleDateString("pt-BR"),
                data.settings.margin.left + 5,
                25
              );

              // Footer
              var str = "Pagina " + doc.internal.getNumberOfPages();
              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
                var totalpaginas = totalPagesExp;
              }

              doc.setFontSize(10);

              // jsPDF 1.4+ uses getWidth, <1.4 uses .width
              var pageSize = doc.internal.pageSize;
              var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();

              doc.text(
                str + "                        ",
                data.settings.margin.left,
                pageHeight - 10
              );
            },
            margin: { top: 30 },
          });
          if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
          }

          doc.save("relatorio-veiculos-dados-detran.pdf");
        },
      };
    }
  );
