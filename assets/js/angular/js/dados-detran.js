angular
  .module("app")
  .controller(
    "dadosDetranCtrl",
    function ($scope, $http, myFunctions, urlServidor, $mdDialog) {
      $scope.disableAtualizaDetran = true;
      $scope.Disable = true;
      $scope.ncv = 0;
      $scope.disableCustomPDFs = true;

      $scope.Enable = true;

      $scope.DadosPeriodo = {
        dataInicio: new Date(),
        dataFinal: new Date(),
        id_patio: "",
        id_leilao: "",
        id_patioGrupo: "",
        sem_atualizacao: null,
        com_atualizacao: null,
      };

      $http
        .put(
          urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
          {
            idUsuario: localStorage.getItem("id_usuario"),
            idEmpresa: localStorage.getItem("id_empresa"),
          }
        )
        .then(({ data }) => {
          $scope.patios = data.filter((item) => {
            return item.status == !0;
          });
        });

      const buscaPatiosGrupo = async () => {
        const response = await $http.get(
          `${urlServidor.urlServidorChatAdmin}/grupos-patios/listar`
        );
        const { data } = response;
        return data.filter((item) => item.ativo == 1);
      };

      buscaPatiosGrupo().then((Response) => ($scope.patiosGrupo = Response));

      $scope.SeparacaoPeriodo = {
        hoje: new Date(),
        mostrarPatiosCadastrado: true,
        //Methodos
        mesAtual: function () {
          $scope.DadosPeriodo.ApreeDe = new Date(
            this.hoje.getFullYear(),
            this.hoje.getMonth(),
            01
          );
          $scope.DadosPeriodo.ApreeAte = new Date(
            this.hoje.getFullYear(),
            this.hoje.getMonth() + 1,
            0
          );
        },
        /*
        getPatioCadastrados: async ({ id: id_leilao }) => {
          const response = await $http.get(
            `${urlServidor.urlServidorChatAdmin}/patios/listar-leilaopatio`,
            { params: { id_leilao } }
          );
          const { data } = response;
          
          $scope.patiosCadastrados = data.map((item) => item.nome);
          $scope.patiosCadastradosID = data.map((item) => item.id);
        },

       */
        //Busca NCVs do patio selecionado
        buscaDados: async function () {
          $scope.Dados = null;

          if (
            $scope.DadosPeriodo.id_patio ||
            $scope.DadosPeriodo.id_patioGrupo
          ) {
            const response = await $http.get(
              `${urlServidor.urlServidorChatAdmin}/separacao-leilao/listar-dados-detran`,
              {
                params: {
                  data_inicial: $scope.DadosPeriodo.dataInicio,
                  data_final: $scope.DadosPeriodo.dataFinal,
                  id_patios: $scope.DadosPeriodo.id_patio,
                  id_grupo_patios: $scope.DadosPeriodo.id_patioGrupo,
                  sem_atualizacao: $scope.DadosPeriodo.sem_atualizacao,
                  com_atualizacao: $scope.DadosPeriodo.com_atualizacao,
                },
              }
            );
            const { data } = response;

            if (data == false) {
              myFunctions.showAlertCustom(
                "Nenhum registro encontrado!<br/><br/>Verifique parâmetros da pesquisa!"
              );

              return;
            }

            const NotNUllNcvs = this.filterPlaca(data);

            $scope.Dados = NotNUllNcvs.map((item) => ({
              ...item,
              restricoes:
                item.restricoes === null ? "NADA CONSTA" : item.restricoes,
            }));

            const NotContainsRenavam = $scope.Dados.find(
              (dados) => !dados.renavam
            );
            console.log({
              NotContainsRenavam,
            });
            $scope.disableAtualizaDetran = NotContainsRenavam ? true : false;
            $scope.disableCustomPDFs = false;

            $scope.totalRecebidos = data.length;
            $scope.diferenca = data.length - $scope.Dados.length;

            let arrDiferenca = data.filter(
              (x) => !$scope.Dados.some((y) => x.NCV === y.NCV)
            );
            $scope.pdfDiferenca = arrDiferenca;

            $mdDialog
              .show({
                controller: function ($scope, $mdDialog, $mdToast) {
                  $scope.cancel = function () {
                    $mdDialog.cancel();
                    $scope.isDisabled = false;
                  };
                },
                templateUrl: "detalhes-dados-detran.html",
                scope: $scope,
                preserveScope: true,
                scopoAnterior: $scope,
              })
              .then(
                function (answer) {
                  let a = answer;
                },
                function () {
                  $scope.statusdialog = "You cancelled the dialog.";
                }
              );
          } else {
            myFunctions.showAlert(
              `Nenhum pátio ou grupo de pátios foi selecionado, verifique!`
            );
          }
        },

        filterPlaca: (data) => {
          const placaNormal = /^[a-zA-Z]{3}[0-9]{4}$/; // XXX-9999
          const placaMercosulCarro = /^[a-zA-Z]{3}[0-9]{1}[a-zA-Z]{1}[0-9]{2}$/; // XXX-9X99
          const placaMercosulMoto = /^[a-zA-Z]{3}[0-9]{2}[a-zA-Z]{1}[0-9]{1}$/; // XXX-99X9

          const notNull = data.filter((item) => {
            if (
              placaNormal.test(item.placa_ncv) ||
              placaMercosulCarro.test(item.placa_ncv) ||
              placaMercosulMoto.test(item.placa_ncv)
            ) {
              return {
                ...item,
              };
            }
          });

          return notNull;
        },

        createInterval(length = 0) {
          const ms = length * 5000;
          const seconds = ms / 1000;
          const minutes = seconds / 60;
          return { ms, seconds, minutes };
        },

        transforminHours(minutes) {
          if (minutes > 60) {
            // console.log(minutes);
            return Math.floor(minutes / 60) === 1
              ? `${Math.floor(minutes / 60)} hora`
              : `${Math.floor(minutes / 60)} horas`;
          } else if (minutes <= 1) {
            return `${1} minuto`;
          } else {
            if (Math.round(minutes) === 1) {
              return `${Math.round(minutes)} minuto`;
            } else {
              return `${Math.round(minutes)} minutos`;
            }
          }
        },

        atualizaDetran: async () => {
          $scope.SeparacaoPeriodo.inserir();

          if ($scope.dadosAtualizaDetran.length > 0) {
            $scope.ShowMessage = true;

            $scope.message = `Atualizando dados de ${$scope.dadosAtualizaDetran.length} veiculo(s)! Por favor aguarde...`;

            await $http
              .post(
                urlServidor.urlServidorChatAdmin + "/integracoes/grava-placas",
                { dados: $scope.dadosAtualizaDetran }
              )
              .then(function (response) {});

            var { ms } = $scope.SeparacaoPeriodo.createInterval(
              $scope.dadosAtualizaDetran.length,
              false
            );

            buscaPlacas = async () => {
              const response = await $http
                .get("https://sync-detran.herokuapp.com/placas")
                .then((response) => {
                  let dados = response.data;

                  let separaPlaca = $scope.dadosAtualizaDetran.map(
                    (item) => item.placa
                  );

                  let compara = separaPlaca.filter(
                    (e) => dados.indexOf(e) !== -1
                  );

                  $scope.comparaPlaca = compara;
                });
            };

            const Interval = setInterval(async () => {
              const retorno = buscaPlacas();

              if ($scope.comparaPlaca.length == 0) {
                clearInterval(Interval);

                const response = await $http.get(
                  `${urlServidor.urlServidorChatAdmin}/separacao-leilao/listar-dados-detran`,
                  {
                    params: {
                      data_inicial: $scope.DadosPeriodo.dataInicio,
                      data_final: $scope.DadosPeriodo.dataFinal,
                      id_patios: $scope.DadosPeriodo.id_patio,
                      id_grupo_patios: $scope.DadosPeriodo.id_patioGrupo,
                      sem_atualizacao: $scope.DadosPeriodo.sem_atualizacao,
                      com_atualizacao: $scope.DadosPeriodo.com_atualizacao,
                    },
                  }
                );
                const { data } = response;

                const NotNUllNcvs = $scope.SeparacaoPeriodo.filterPlaca(data);

                let tmpDados = NotNUllNcvs.map((item) => ({
                  ...item,
                  restricoes:
                    item.restricoes === null ? "NADA CONSTA" : item.restricoes,
                }));

                const resultTmp = [];

                for (let item of $scope.dadosAtualizaDetranTemp) {
                  const findNcv = tmpDados.find((x) => x.NCV === item.NCV);

                  if (!findNcv) continue;

                  resultTmp.push(findNcv);
                }

                /*             
                RENAJUD  = RESTRICOES:  "RENAJUD" + "CODIGO FINANCEIRA"
                JUDICIAL = RESTRICOES: "JUDICIAL" / "RENAJUD" SEM CODIGO FINANCEIRA
                ADMINISTRATIVO = TODOS OUTROS CASOS
                */

                let dados = resultTmp.map((item) => {
                  let tipo;

                  if (
                    item.restricoes.includes("RENAJUD") &&
                    item.codigo_financeira !== null
                  ) {
                    tipo = "RENAJUD";
                  } else if (
                    item.restricoes.includes("RENAJUD") &&
                    item.codigo_financeira === null
                  ) {
                    tipo = "JUDICIAL";
                  } else if (
                    item.restricoes.includes("JUDICIAL") &&
                    item.codigo_financeira === null
                  ) {
                    tipo = "JUDICIAL";
                  } else if (
                    item.restricoes.includes("JUDICIAL") &&
                    item.codigo_financeira !== null
                  ) {
                    tipo = "JUDICIAL";
                  } else {
                    tipo = "ADMINISTRATIVO";
                  }
                  return {
                    id: item.NCV,
                    tipo: tipo,
                  };
                });

                for (let [index, item] of dados.entries()) {
                  $scope.message = `Processando veiculo ${index} de ${$scope.dadosAtualizaDetran.length} veiculo(s)! Por favor aguarde...`;

                  await $http
                    .post(
                      urlServidor.urlServidorChatAdmin +
                        "/separacao-leilao/atualiza-ncv-tipo",
                      { id: item.id, tipo: item.tipo }
                    )
                    .then(function (response) {
                      if (response.data.code) {
                        myFunctions.showAlert("Erro na gravação!");
                      }
                    });
                }

                myFunctions.showAlert("Processamento Finalizado!");

                $scope.Dados = resultTmp;

                $scope.ShowMessage = false;

                $scope.SeparacaoPeriodo.Selecionar = "SELECIONAR TODOS";
              } else {
                $scope.ShowMessage = true;
              }
            }, 30000);
          } else {
            myFunctions.showAlert(`Nenhum veículo foi selecionado, verifique!`);
          }
        },

        atualizaNCV: async () => {
          $scope.SeparacaoPeriodo.inserir();

          if ($scope.dadosAtualizaDetranTemp.length > 0) {
            $scope.ShowMessage = true;

            let dados = $scope.dadosAtualizaDetranTemp.map((item) => {
              let tipo;

              if (
                item.restricoes.includes("RENAJUD") &&
                item.codigo_financeira !== null
              ) {
                tipo = "RENAJUD";
              } else if (
                item.restricoes.includes("RENAJUD") &&
                item.codigo_financeira === null
              ) {
                tipo = "JUDICIAL";
              } else if (
                item.restricoes.includes("JUDICIAL") &&
                item.codigo_financeira === null
              ) {
                tipo = "JUDICIAL";
              } else if (
                item.restricoes.includes("JUDICIAL") &&
                item.codigo_financeira !== null
              ) {
                tipo = "JUDICIAL";
              } else {
                tipo = "ADMINISTRATIVO";
              }
              return {
                id: item.NCV,
                tipo: tipo,
              };
            });

            for (let [index, item] of dados.entries()) {
              $scope.message = `Processando veiculo ${index} de ${$scope.dadosAtualizaDetran.length} veiculo(s)! Por favor aguarde...`;

              await $http
                .post(
                  urlServidor.urlServidorChatAdmin +
                    "/separacao-leilao/atualiza-ncv-tipo",
                  { id: item.id, tipo: item.tipo }
                )
                .then(function (response) {
                  if (response.data.code) {
                    myFunctions.showAlert("Erro na gravação!");
                  }
                });
            }

            const response = await $http.get(
              `${urlServidor.urlServidorChatAdmin}/separacao-leilao/listar-dados-detran`,
              {
                params: {
                  data_inicial: $scope.DadosPeriodo.dataInicio,
                  data_final: $scope.DadosPeriodo.dataFinal,
                  id_patios: $scope.DadosPeriodo.id_patio,
                  id_grupo_patios: $scope.DadosPeriodo.id_patioGrupo,
                  sem_atualizacao: $scope.DadosPeriodo.sem_atualizacao,
                  com_atualizacao: $scope.DadosPeriodo.com_atualizacao,
                },
              }
            );
            const { data } = response;

            const resultTmp = [];

            for (let item of $scope.dadosAtualizaDetranTemp) {
              const findNcv = data.find((x) => x.NCV === item.NCV);

              if (!findNcv) continue;
              resultTmp.push(findNcv);
            }

            myFunctions.showAlert("Atualizações NCV finalizadas!");

            $scope.Dados = resultTmp;
            $scope.ShowMessage = false;
            $scope.SeparacaoPeriodo.Selecionar = "SELECIONAR TODOS";
          } else {
            myFunctions.showAlert(`Nenhum veículo foi selecionado, verifique!`);
          }
        },

        //Habilita as datas
        enableDate: () => {
          $scope.Enable = false;
        },

        //Desabilita as datas
        DisableDate: () => {
          $scope.Enable = true;

          $scope.DadosPeriodo = {
            ApreeDe: new Date(),
            hoje: new Date(),
            id_patio: "",
            id_leilao: "",
          };
        },

        //Limpa os campos
        limpar: () => {
          ($scope.DadosPeriodo.id_patio = ""),
            ($scope.DadosPeriodo.dataInicio = new Date()),
            ($scope.DadosPeriodo.dataFinal = new Date()),
            ($scope.DadosPeriodo.id_patioGrupo = ""),
            ($scope.disableAtualizaDetran = true),
            ($scope.Dados = ""),
            ($scope.disableCustomPDFs = true),
            ($scope.DadosPeriodo.sem_atualizacao = null),
            ($scope.DadosPeriodo.com_atualizacao = null);
        },

        Selecionar: "SELECIONAR TODOS",

        SelecionarTodos: function (Dados) {
          if (this.Selecionar === "SELECIONAR TODOS") {
            Dados.forEach((dados) => {
              dados.selecionado = true;
            });
            this.Selecionar = "DESMARCAR TODOS";
          } else {
            Dados.forEach((dados) => {
              dados.selecionado = false;
            });
            this.Selecionar = "SELECIONAR TODOS";
          }
        },

        inserir: function (Dados) {
          const result = $scope.Dados.filter((Veiculo) => {
            return Veiculo.selecionado;
          });

          $scope.dadosAtualizaDetranTemp = result;

          $scope.dadosAtualizaDetran = result.map((item) => ({
            placa: item.placa_ncv,
            ncv: item.NCV,
            id_leilao: 0,
            processado: 0,
          }));
        },

        ///RENAJUD E JUDICIAL PDF

        async GerarRenajudOrJudicial(Label) {
          if (Label === "RENAJUD") {
            this.RenajudOrJudicial = $scope.Dados.filter(
              (item) => item.tipo_ncv === "RENAJUD"
            );
            this.GerarPDFJudicialOrRenajud(Label, this.RenajudOrJudicial);
          } else if (Label === "JUDICIAL") {
            this.RenajudOrJudicial = $scope.Dados.filter(
              (item) => item.tipo_ncv === "JUDICIAL"
            );
            this.GerarPDFJudicialOrRenajud(Label, this.RenajudOrJudicial);
          } else {
            this.RenajudOrJudicial = $scope.Dados.filter(
              (item) => item.tipo_ncv === "ADMINISTRATIVO"
            );
            this.GerarPDFJudicialOrRenajud(Label, this.RenajudOrJudicial);
          }
        },

        GerarPDFJudicialOrRenajud(label, dados) {
          if (dados.length !== 0) {
            var doc = new jsPDF({ orientation: "landscape" });

            var totalPagesExp = "{total_pages_count_string}";

            doc.setFontSize(10);

            doc.autoTable({
              columnStyles: {
                vencimento: { halign: "left" },
              },

              body: dados,
              columns: [
                { header: "NCV", dataKey: "NCV" },
                { header: "Patio", dataKey: "patio" },
                { header: "Data Apreensão", dataKey: "data_apreensao" },
                { header: "Placa", dataKey: "placa_ncv" },
                { header: "Marca/Modelo", dataKey: "marca_modelo_ncv" },
                { header: "Ano", dataKey: "ano" },
                { header: "Tipo do Veiculo", dataKey: "tipo_veiculo" },
                { header: "Atualização", dataKey: "data_hora_atualizacao" },
                { header: "Restrição/Tipo", dataKey: "tipo_ncv" },
              ],
              bodyStyles: {
                margin: 10,
                fontSize: 08,
              },

              didDrawPage: function (data) {
                // Header
                doc.setFontSize(18);
                doc.setTextColor(40);
                doc.setFontStyle("normal");

                doc.text(
                  "Veiculo(s) com restrição " + label,
                  data.settings.margin.left + 5,
                  18
                );
                doc.text(
                  "Quantidade: " +
                    dados.length +
                    "            " +
                    "Data: " +
                    new Date().toLocaleDateString("pt-BR"),
                  data.settings.margin.left + 5,
                  25
                );

                // Footer
                var str = "Pagina " + doc.internal.getNumberOfPages();
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === "function") {
                  str = str + " de " + totalPagesExp;
                  var totalpaginas = totalPagesExp;
                }

                doc.setFontSize(10);

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize;
                var pageHeight = pageSize.height
                  ? pageSize.height
                  : pageSize.getHeight();

                doc.text(
                  str + "                        ",
                  data.settings.margin.left,
                  pageHeight - 10
                );
              },
              margin: { top: 30 },
            });
            if (typeof doc.putTotalPages === "function") {
              doc.putTotalPages(totalPagesExp);
            }

            doc.save(`${label}/dados-detran.pdf`);
          } else {
            myFunctions.showAlert("Sem dados para gerar PDF!");
          }
        },
      };

      $scope.gerarPlanilha = () => {
        if ($scope.Dados) {
          alasql(
            'SELECT * INTO XLSX("relatorio-dados-detran.xlsx",{headers:true}) FROM ?',
            [$scope.Dados]
          );
        } else {
          myFunctions.showAlert("Sem dados para gerar planilha! Verifique!");
        }
      };

      $scope.gerarPDFDiferenca = function () {
        if ($scope.pdfDiferenca) {
          var doc = new jsPDF({ orientation: "landscape" });

          var totalPagesExp = "{total_pages_count_string}";

          doc.setFontSize(10);

          doc.autoTable({
            columnStyles: {
              vencimento: { halign: "left" },
            },

            body: $scope.pdfDiferenca,
            columns: [
              { header: "NCV", dataKey: "NCV" },
              { header: "Patio", dataKey: "patio" },
              { header: "Data de Apreensão", dataKey: "data_apreensao" },
              { header: "Placa", dataKey: "placa_ncv" },
              { header: "Marca/Modelo", dataKey: "marca_modelo_ncv" },
              { header: "Ano", dataKey: "ano" },
              { header: "Tipo do Veiculo", dataKey: "tipo_veiculo" },
            ],
            bodyStyles: {
              margin: 10,
              fontSize: 08,
            },

            didDrawPage: function (data) {
              // Header
              doc.setFontSize(18);
              doc.setTextColor(40);
              doc.setFontStyle("normal");

              doc.text(
                "Relatorio veículos excluídos da pesquisa Detran",
                data.settings.margin.left + 5,
                18
              );
              doc.text(
                "Quantidade: " +
                  $scope.pdfDiferenca.length +
                  "            " +
                  "Data: " +
                  new Date().toLocaleDateString("pt-BR"),
                data.settings.margin.left + 5,
                25
              );

              // Footer
              var str = "Pagina " + doc.internal.getNumberOfPages();
              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
                var totalpaginas = totalPagesExp;
              }

              doc.setFontSize(10);

              // jsPDF 1.4+ uses getWidth, <1.4 uses .width
              var pageSize = doc.internal.pageSize;
              var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();

              doc.text(
                str + "                        ",
                data.settings.margin.left,
                pageHeight - 10
              );
            },
            margin: { top: 30 },
          });
          if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
          }

          doc.save("veiculos-removidos-pesquisa-detran.pdf");
        } else {
          myFunctions.showAlert("Sem dados para gerar PDF!");
        }
      };

      $scope.gerarPDF = function () {
        let cabecalho;

        if ($scope.DadosPeriodo.com_atualizacao) {
          cabecalho = "com atualização";
        } else if ($scope.DadosPeriodo.sem_atualizacao) {
          cabecalho = "sem atualização";
        } else {
          cabecalho = "";
        }

        if ($scope.Dados) {
          var doc = new jsPDF({ orientation: "landscape" });

          var totalPagesExp = "{total_pages_count_string}";

          doc.setFontSize(10);

          doc.autoTable({
            columnStyles: {
              vencimento: { halign: "left" },
            },

            body: $scope.Dados,
            columns: [
              { header: "NCV", dataKey: "NCV" },
              { header: "Patio", dataKey: "patio" },
              { header: "Data de Apreensão", dataKey: "data_apreensao" },
              { header: "Placa", dataKey: "placa_ncv" },
              { header: "Marca/Modelo", dataKey: "marca_modelo_ncv" },
              { header: "Ano", dataKey: "ano" },
              { header: "Tipo do Veiculo", dataKey: "tipo_veiculo" },
              { header: "Atualização", dataKey: "data_hora_atualizacao" },
              { header: "Restrição", dataKey: "restricoes" },
            ],
            bodyStyles: {
              margin: 10,
              fontSize: 08,
            },

            didDrawPage: function (data) {
              // Header
              doc.setFontSize(18);
              doc.setTextColor(40);
              doc.setFontStyle("normal");

              doc.text(
                "Relatorio veículos dados Detran " + cabecalho,
                data.settings.margin.left + 5,
                18
              );
              doc.text(
                "Quantidade: " +
                  $scope.Dados.length +
                  "            " +
                  "Data: " +
                  new Date().toLocaleDateString("pt-BR"),
                data.settings.margin.left + 5,
                25
              );

              // Footer
              var str = "Pagina " + doc.internal.getNumberOfPages();
              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
                var totalpaginas = totalPagesExp;
              }

              doc.setFontSize(10);

              // jsPDF 1.4+ uses getWidth, <1.4 uses .width
              var pageSize = doc.internal.pageSize;
              var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();

              doc.text(
                str + "                        ",
                data.settings.margin.left,
                pageHeight - 10
              );
            },
            margin: { top: 30 },
          });
          if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
          }

          doc.save("relatorio-veiculos-dados-detran.pdf");
        } else {
          myFunctions.showAlert("Sem dados para gerar PDF! Verifique!");
        }
      };
    }
  );
