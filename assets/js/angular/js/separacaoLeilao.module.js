angular
  .module("app")
  .controller(
    "separacaoLeilaoCtrl",
    function ($scope, $http, myFunctions, urlServidor, $mdDialog) {
      $scope.ncv = 0;

      $http
        .get(`${urlServidor.urlServidorChatAdmin}/leiloes/listar`)
        .then(({ data }) => ($scope.Leilao = data));

      $scope.Ajustes = {
        HiddeAr: true,
        dataForArGeneration: "",
        leilao: "",
        Disable: true,
        RenajudOrJudicial: null,
        quantidadeArs: 0,
        classificacoes: "",
        async GetLeilao({ Leilao: id_leilao }) {
          const dados = $scope.Leilao.find((Leilao) => Leilao.id === id_leilao);
          this.leilao = dados;
          this.getClassificacao();

          const response = await $http.get(
            `${urlServidor.urlServidorChatAdmin}/patios/listar-leilaopatio`,
            {
              params: {
                id_leilao: this.leilao.id,
              },
            }
          );
          const { data } = response;
          $scope.patioscad = data.map((item) => item.nome);
        },

        async pesq() {
          $scope.dadosList;
          const { data } = await $http.get(
            `${urlServidor.urlServidorChatAdmin}/separacao-leilao/listar`,
            {
              params: {
                id_leilao: $scope.dados.Leilao,
              },
            }
          );
          this.Disable = false;
          this.getClassificacao();

          let Res = null;
          if ("id_classificacaoFiltro" in $scope.dados) {
            Res = data.filter(
              ({ id_classificacao }) =>
                id_classificacao === $scope.dados.id_classificacaoFiltro
            );
          }
          $scope.dadosList =
            Res ||
            data.map((item, idx) => ({
              ...item,
              lote: item?.lote || idx + 1,
              restricoes:
                item.restricoes === null ? "NADA CONSTA" : item.restricoes,
            }));
          console.log($scope.dadosList);

          this.filterPlaca($scope.dadosList);
        },

        async OpenFotos(id, chassi, motor) {
          window.open(
            `/#/fotos-separacao-leilao?id=${id}&chassi=${chassi}&motor=${motor} `,
            "_blank"
          );
        },

        async getClassificacao() {
          const { data } = await $http.get(
            `${urlServidor.urlServidorChatAdmin}/classificacao-leilao/listar`
          );

          this.classificacoes = data;
        },
        filterPlaca(data) {
          const placaNormal = /^[a-zA-Z]{3}[0-9]{4}$/; // XXX-9999
          const placaMercosulCarro = /^[a-zA-Z]{3}[0-9]{1}[a-zA-Z]{1}[0-9]{2}$/; // XXX-9X99
          const placaMercosulMoto = /^[a-zA-Z]{3}[0-9]{2}[a-zA-Z]{1}[0-9]{1}$/; // XXX-99X9

          const placaValida = data.filter(
            (veiculo) =>
              placaNormal.test(veiculo.placa) ||
              placaMercosulCarro.test(veiculo.placa) ||
              placaMercosulMoto.test(veiculo.placa)
          );

          this.dataForArGeneration = placaValida;
        },

        createIinterval(length = 0) {
          const ms = length * 5000;
          const seconds = ms / 1000;
          const minutes = seconds / 60;
          return { ms, seconds, minutes };
        },

        transforminHours(minutes) {
          if (minutes > 60) {
            console.log(minutes);
            return Math.floor(minutes / 60) === 1
              ? `${Math.floor(minutes / 60)} Hora`
              : `${Math.floor(minutes / 60)} Horas`;
          } else if (minutes === 1) {
            return `${1} Minuto`;
          } else {
            if (Math.round(minutes) === 1) {
              return `${Math.round(minutes)} Minuto`;
            } else {
              return `${Math.round(minutes)} Minutos`;
            }
          }
        },

        async BuscaDetran() {
          $scope.dadosFordetran = this.dataForArGeneration.map((item) => ({
            placa: item.placa,
            ncv: item.NCV,
            id_leilao: this.leilao.id,
            processado: 0,
          }));

          const timertoShow = this.createIinterval(
            $scope.dadosFordetran.length
          );

          $scope.ShowMessage = true;

          $scope.message = `Buscando Dados Do Veiculo, por favor aguarde,\n
          Tempos estimado : ${this.transforminHours(timertoShow.minutes)} `;

          await $http
            .post(
              `${urlServidor.urlServidorChatAdmin}/integracoes/grava-placas`,
              { dados: $scope.dadosFordetran }
            )
            .then(async () => {
              await $http.get(`https://sync-detran.herokuapp.com/processar`);
            });

          var { ms } = this.createIinterval(
            $scope.dadosFordetran.length,
            false
          );

          const VerificaDados = () => {
            ms = ms - 5000;
            console.log(ms);

            if (ms === 0) {
              return 0;
            } else {
              return 1;
            }
          };

          const Interval = setInterval(async () => {
            const response = VerificaDados();
            if (response == 0) {
              $scope.message = "Processamento concluido com sucesso";

              clearInterval(Interval);
              this.pesq();

              $scope.ShowMessage = false;
            } else {
              $scope.ShowMessage = true;
            }
          }, 5000);
        },

        // /RENAJUD E JUDICIAL PDF

        async GerarRenajudOrJudicial(Label) {
          if (Label === "RENAJUD") {
            this.RenajudOrJudicial = this.dataForArGeneration.filter((item) =>
              item.restricoes.includes("RENAJUD")
            );
            this.GerarPDFJudicialOrRenajud(Label, this.RenajudOrJudicial);
          } else {
            this.RenajudOrJudicial = this.dataForArGeneration.filter((item) =>
              item.restricoes.includes("JUDICIAL")
            );
            this.GerarPDFJudicialOrRenajud(Label, this.RenajudOrJudicial);
          }
        },

        GerarPDFJudicialOrRenajud(label, dados) {
          var doc = new jsPDF({ orientation: "landscape" });

          var totalPagesExp = "{total_pages_count_string}";

          doc.setFontSize(10);

          doc.autoTable({
            columnStyles: {
              vencimento: {
                halign: "left",
              },
            },

            body: dados,
            columns: [
              {
                header: "NCV",
                dataKey: "id",
              },

              {
                header: "Ano",
                dataKey: "ano",
              },

              {
                header: "Placa",
                dataKey: "placa",
              },

              {
                header: "Marca/Modelo",
                dataKey: "marca_modelo",
              },
              {
                header: "Número do Motor",
                dataKey: "numero_motor",
              },
              {
                header: "Chassi",
                dataKey: "chassi",
              },
              {
                header: "Restrições",
                dataKey: "restricoes",
              },
            ],
            bodyStyles: {
              margin: 10,
              fontSize: 08,
            },

            didDrawPage: function (data) {
              // Header
              doc.setFontSize(18);
              doc.setTextColor(40);
              doc.setFontStyle("normal");

              doc.text(
                "Veiculos com Restrições" + label,
                data.settings.margin.left + 15,
                22
              );

              // Footer
              var str = "Pagina " + doc.internal.getNumberOfPages();
              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
                var totalpaginas = totalPagesExp;
              }

              doc.setFontSize(10);

              // jsPDF 1.4+ uses getWidth, <1.4 uses .width
              var pageSize = doc.internal.pageSize;
              var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();

              doc.text(
                str + "                        ",
                data.settings.margin.left,
                pageHeight - 10
              );
            },
            margin: {
              top: 30,
            },
          });
          if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
          }

          doc.save(`${label}/separacao-leilao.pdf`);
        },

        BuscaNcvPlaca: async function (placa) {
          const Placas = placa.split(";");

          Placas.forEach(async (placa) => {
            const { data } = await $http.get(
              `${urlServidor.urlServidorChatAdmin}/ncv/ncv-placa`,
              {
                params: {
                  ncv_placa: placa,
                },
              }
            );
            if (!Boolean(data[0])) {
              myFunctions.showAlert(
                `Veiculo ${placa} não encontrato, verifique se digitou corretamente e o veiculo está APREENDIDO`
              );
              return;
            }

            const responseCad = await $http.post(
              `${urlServidor.urlServidorChatAdmin}/separacao-leilao/cadastrar`,
              {
                ncv_placa: placa,
                id_leilao: $scope.dados.Leilao,
              }
            );

            if (responseCad?.data?.status === "200") {
              myFunctions.showAlert(
                `Veiculo ${placa} inserido com sucesso no Leilao`
              );
              delete $scope.dados.ncv_placa;

              this.pesq();
            } else {
              myFunctions.showAlert(
                `Veiculo ${placa} não inserido, verifique e tente novamente`
              );
            }
          });
        },

        limpar: (ev) => {
          delete $scope.dados;
        },

        Apagar(rowid) {
          let confirm = $mdDialog
            .confirm()
            .ariaLabel("Lucky day")
            .title("Deseja Deletar Permanentemente este Item ?")
            .ok("Deletar")
            .cancel("Cancelar");
          $mdDialog.show(confirm).then(() => {
            $http
              .post(
                `${urlServidor.urlServidorChatAdmin}/separacao-leilao/apagar`,
                { id: rowid }
              )
              .then((response) => {
                myFunctions.showAlert(`Veiculo Deletado do leilão com sucesso`);
                this.pesq();
              });
          });
        },
        BrowserUser() {
          var sBrowser,
            sUsrAg = navigator.userAgent;

          if (sUsrAg.indexOf("Chrome") > -1) {
            sBrowser = "Google Chrome";
          } else if (sUsrAg.indexOf("Safari") > -1) {
            sBrowser = "Apple Safari";
          } else if (sUsrAg.indexOf("Opera") > -1) {
            sBrowser = "Opera";
          } else if (sUsrAg.indexOf("Firefox") > -1) {
            sBrowser = "Mozilla Firefox";
          } else if (sUsrAg.indexOf("Edg") > -1) {
            sBrowser = "Microsoft Internet Explorer";
          }

          return sBrowser;
        },

        GerarArHtml: async function () {
          const Browser = this.BrowserUser();

          if (Browser === "Google Chrome") {
            alert(
              "Navegador Google Chrome detectado, por favor use outro navegador para gerar ars ex:  Mozila "
            );
            return;
          }

          $scope.container1 = false;
          $scope.container2 = false;

          const result = await Promise.all(
            this.dataForArGeneration.map(async (item) => {
              const GetDadosByPlaca = await $http.get(
                `${urlServidor.urlServidorChatAdmin}/integracoes/detran/${item.placa}`
              );

              const { data } = GetDadosByPlaca;
              let NotNull = data.length === 1 && data[0];
              NotNull.lote = item.lote;
              return NotNull;
            })
          );
          const filterResults = result.filter((item) => Boolean(item));
          this.gerarArPorquantidade(filterResults);
        },
        async gerarArPorquantidade(dados) {
          var comQuantidade;
          $mdDialog
            .show({
              controller: function ($scope, $mdDialog) {
                $scope.saveAndClose = function (quantidadeArs) {
                  if (quantidadeArs == undefined) {
                    comQuantidade = dados;
                  } else {
                    comQuantidade = dados.slice(0, parseInt(quantidadeArs));
                  }
                  $mdDialog.hide();
                };

                $scope.calcel = () => $mdDialog.hide();
              },
              templateUrl: "quantidadeArs.html",
            })
            .then(() => {
              $scope.Ajustes.ImprimirArs(comQuantidade, 0);
            });
        },

        async ImprimirArs(dates, idx) {
          var dados = dates[idx];
          const length = dates.length - 1;
          const doc = new jsPDF("p", "px", "a4");
          $scope.message2 = `Gerando ${dates.length} AR's, por favor aguarde até o download terminar`;

          let container = document.getElementById("ARCorreios");
          let container2 = document.getElementById("ARsecondPage");
          container.innerHTML =
            /*HTML*/
            `
  <div class="FirstArea" >


    <div class="header">


        <div class="logoCorreios">
            <img src="../../../../assets/images/CorreioLogo.jpg" alt="">
            <h1>Aviso de Recebimento </h1>
            <h1 class="ArH1">AR</h1>
        </div>

        <div class="dataPostagem">
            <h3>Data Postagem</h3>
        </div>

    </div>

    <div class="bodyAr">
        <div class="destinatario">
            <h1 class="Dest">Destinatário</h1>


            <h4 class="nome">${
              dados.restricao1.includes("COMUNICACAO DE VENDA")
                ? dados.nome_cvenda
                : dados.nome_proprietario
            }</h4>
            <h4>${
              dados.restricao1.includes("COMUNICACAO DE VENDA")
                ? dados.endereco_cvenda
                : dados.endereco_proprietario
            }, ${
              dados.restricao1.includes("COMUNICACAO DE VENDA")
                ? dados.numero_cvenda
                : dados.numero_proprietario
            }</h4>

            <div class="cep">
                <h4>${
                  dados.restricao1.includes("COMUNICACAO DE VENDA")
                    ? dados.bairro_cvenda
                    : dados.bairro_proprietario
                }</h4>
                <h4 class="CEP">
                     CEP :

                    <h4>${
                      dados.restricao1.includes("COMUNICACAO DE VENDA")
                        ? dados.cep_cvenda
                        : dados.cep_proprietario
                    }</h4>
                </h4>
            </div>

            <h4>${
              dados.restricao1.includes("COMUNICACAO DE VENDA")
                ? dados.municipio_cvenda
                : dados.municipio_proprietario
            } / ${
              dados.restricao1.includes("COMUNICACAO DE VENDA")
                ? dados.uf_cvenda
                : dados.uf_proprietario
            }</h4>
           ${` <h4>${this.leilao.orgao} ${
             dados.lote
           }- ${new Date().getFullYear()} / ${this.leilao.lote_inicial} </h4>`} 


        </div>



        <div class="EmptySquare1"></div>
    </div>

    <div class="footerAr">
        <div class="destinatario">
            <h1 class="Dest">ENDEREÇO PARA DEVOLUÇÃO DO AR:</h1>
            <h4 class="Endereço">AUTO SOCORRO E MECÂNICA CARVALHO LTDA</h4>
            <h4 class="Rua">${this.leilao.devolucao_endereco}</h4>

            <div class="cep">
                <h4>${this.leilao.devolucao_cidade}/${
              this.leilao.devolucao_uf
            }</h4>

                <h4 class="CEP">
                    CEP :
                    <h4>${this.leilao.devolucao_cep}</h4>
                </h4>
            </div>

        </div>
        <div class="EmptySquare1"></div>
    </div>



</div>

<div class="detranCarta">

      ${
        this.leilao.orgao === "DER"
          ? `<img src="../../../../assets/images/Der.svg" alt="">`
          : `<img src="../../../../assets/images/Detran.png" alt="">`
      }
         
        
      
    <div class="content">

        <div class="bodyDetranCarta">
            <p>
                Fica Vossa Senhoria notificado(a) de que o veículo abaixo identificado, que consta ser de Vossa
                Propriedade (*)
                conforme pesquisa PRODESP-DETRAN/SP, encontra-se recolhido no pátio municipalizado de ${
                  this.leilao.patio_bairro
                },
                localizado na
                reserva técnica no endereço ${
                  this.leilao.patio_endereco
                }, S/N, ${this.leilao.patio_cidade}/${
              this.leilao.patio_uf
            }, CEP:
                ${this.leilao.patio_cep}, Telefone 0800-970 9752.

            </p>

            <p>
                Outrossim, se no prazo de 20 (vinte) dias a contar do recebimento do presente e, após 60 (sessenta)
                dias do
                recolhimento, não houver manifestação de interesse sobre o referido veículo junto a este
                Departamento, o mesmo
                será levado a hasta publica, conforme os termos do Artigo 328 da Lei Federal 9.503 de 23/09/1997,
                Código de Trânsito
                Brasileiro - CTB da Lei Estadual 15.911/2015 e normas complementares, inclusive como “SUCATA
                FERROSA” ficando V.
                Sa. Responsável pela quitação dos débitos existentes de IPVA e multas incidentes até a dados do
                leilão, que não forem
                passíveis de pagamento com saldo de venda do veículo.
            </p>


            <p>
                Importante destacar que os débitos não cobertos pela receita em leilão, da venda do referido bem,
                poderão ser
                cobrados pelos respectivos credores, conforme Resolução Contran nº 623, de 6 de Setembro de 2016,
                Publicada no
                Diário Oficial da União em 08/09/2016, Art. 37: “ Os débitos que não forem cobertos pelo valor
                alcançado com a
                alienação do veículo, poderão ser cobrados pelos credores na forma da legislação em vigor, por meio
                de ação própria e
                inclusão em Dívida Ativa em nome dos ex-proprietários.”
            </p>

            <p>
                "A distribuição do valor obtido com o praceamento do veículo obedecerá ao Art. 328, §6º da Lei
                9.503/97. Nesse
                sentido, caso sejam quitados todos os débitos, o eventual saldo remanescente ficará em conta
                específica deste órgão,
            </p>



        </div>
    </div>

</div>
<div class="dadosUser">

    <div class="line1">
        <h4>LEILÃO / LOTE :<span> DER ${
          dados.lote
        }-${new Date().getFullYear()} /  ${this.leilao.lote_inicial}</span></h4>
        <h4>RENAVAM :<span >${dados.renavam}</span></h4>
    </div>

    <div class="line2">

        <h4>PROPRIETÁRIO : <span>${
          dados.restricao1.includes("COMUNICACAO DE VENDA")
            ? dados.nome_cvenda
            : dados.nome_proprietario
        }</span></h4>
        <h4>CNPJ / CPF : <span>${
          dados.restricao1.includes("COMUNICACAO DE VENDA")
            ? dados.cpf_cnpj_cvenda
            : dados.cpf_cnpj_proprietario
        }</span></h4>
    </div>


    <div class="line3">

        <h4>MARCA/MODELO :<span>${dados.marca_modelo}</span></h4>
    </div>


    <div class="line4">

        <h4>ANO / MODELO :<span>${dados.ano_modelo}</span></h4>
        <h4>PLACA :<span>${dados.placa}</span></h4>
        <h4>MUNICÍPIO :<span>SIQUEIRA CAMPOS / PR</span></h4>
    </div>


    <div class="line5">
    <h4>Nº. ARV :<span></span></h4>
    <h4>DATA ARV: :<span></span></h4>
        <h4>CHASSI :<span>${dados.chassi}</span></h4>
        <h4>MOTOR :<span> ${dados.numero_motor_bin}</span></h4>
    </div>


    <div class="line6">

        <h4>NOME DA FIN / ARR :<span></span></h4>

    </div>

    <div class="line7">
        <h4>COMUNICAÇÃO DE VENDA: <span>${
          dados.restricao1.includes("COMUNICACAO DE VENDA") ? "Sim" : "Não"
        }</span></h4>

    </div>


    </div>



    `;
          container2.innerHTML =
            /*HTML*/
            `
        <div class="containerNotificacao">
        <img src="/assets/images/Notificacao.png" alt="">
    </div>
    <div class="ar">
        <h3>AR</h3>
    </div>
    <div class="arSender">

            <div class="content">
                <h1>DESTINATÁRIO</h1>
                <h4>${
                  dados.restricao1.includes("COMUNICACAO DE VENDA")
                    ? dados.nome_cvenda
                    : dados.nome_proprietario
                }</h4>
                <h4>${
                  dados.restricao1.includes("COMUNICACAO DE VENDA")
                    ? dados.endereco_cvenda
                    : dados.endereco_proprietario
                }, ${
              dados.restricao1.includes("COMUNICACAO DE VENDA")
                ? dados.numero_cvenda
                : dados.numero_proprietario
            }</h4>
                
                ${
                  dados.restricao1.includes("COMUNICACAO DE VENDA")
                    ? dados.municipio_cvenda
                    : dados.municipio_proprietario
                }
                </h4>
                <div class="cepAndUf">
                <h4>CEP:  ${
                  dados.restricao1.includes("COMUNICACAO DE VENDA")
                    ? dados.cep_proprietario
                    : dados.cep_cvenda
                }</h4> 
                <h4>${
                  dados.restricao1.includes("COMUNICACAO DE VENDA")
                    ? dados.bairro_proprietario
                    : dados.bairro_proprietario
                }/ ${dados.uf_proprietario.toUpperCase()}</h4>

                </div>
            </div>
    </div>

        `;

          await domtoimage.toPng(container).then(async (image) => {
            console.log(image);
            doc.addImage(image, "png", 0, 0, 450.46, 620);

            const image2 = await domtoimage.toPng(container2);
            doc.addPage("p", "px", "a4");

            doc.addImage(image2, "png", 0, 0, 450.46, 500);

            doc.save(`${dados.placa}/${dados.lote}.pdf`);
            if (idx === length) {
              $scope.Showmodal2 = false;
              $scope.container1 = true;
              $scope.container2 = true;
            } else {
              idx++;

              this.ImprimirArs(dates, idx);
            }
          });
        },

        async gerarEdital() {
          $scope.edital = true;
          const result = await Promise.all(
            this.dataForArGeneration.map(async (item) => {
              const GetDadosByPlaca = await $http.get(
                `${urlServidor.urlServidorChatAdmin}/integracoes/detran/${item.placa}`
              );

              const { data } = GetDadosByPlaca;
              const NotNull = data.length === 1 && data[0];

              return NotNull;
            })
          );
          const filterResults = result.filter((item) => Boolean(item));
          console.log(filterResults);
          this.printDiv("Edital", filterResults);
        },

        printDiv(title, filterResults) {
          let mywindow = window.open(
            "",
            "PRINT",
            "height=650,width=900,top=100,left=150"
          );

          mywindow.document.write(
            /*HTML*/
            `

          <html lang="en">
              <head>
                  <meta charset="UTF-8">
                  <meta http-equiv="X-UA-Compatible" content="IE=edge">
                  <meta name="viewport" content="width=device-width, initial-scale=1.0">
                  <title>${title}</title>
                  <link rel="stylesheet" href="/assets/css/edital/index.css">

              </head>
                  <body style="padding: 10px;">
                  <h1
                  style="
                  font-size: 25px;
                  text-align: center;
                  ">


                  ${this.leilao.edital_titulo || ""}
                   </h1>

                  <h1
                  style="
                  font-size: 25px;
                  text-align: center;
                  "
                  >
                  ${this.leilao.edital_subtitulo1 || ""}
                </h1>

                  <h1
                  style="
                  font-size: 25px;
                  "

                  >
                    ${this.leilao.edital_subtitulo2 || ""}
                  </h1>

                  <p

                  style="
                  font-size: 20px;
                  "
                  > ${this.leilao.edital_texto || ""}</p>


             <h1

             style="
                  font-size: 25px;

                  margin-top: 35px;
                  margin-bottom: 35px;
                  "

             >
             LOCALIZADO: ${this.leilao.patio_endereco.toLocaleUpperCase()} - ${this.leilao.patio_bairro.toLocaleUpperCase()} - ${this.leilao.patio_cidade.toLocaleUpperCase()}
              </h1>
              ${filterResults
                .map(
                  (item, idx) => `
               <h5
               style="font-size: 18px;"

               >

               ${idx + 1} PLACA: ${item.placa || ""}; ${
                    item.municipio_proprietario || ""
                  }-${item.uf_proprietario || ""}; CHASSI:  ${
                    item.chassi || ""
                  } MOTOR: ${item.numero_motor2_bin || ""}; ${
                    item.marca_modelo || ""
                  }; ${item.ano_fabricacao || ""} / ${
                    item.ano_modelo || ""
                  } ; ${item.nome_proprietario || ""}; RENAVAM: ${
                    item.renavam || ""
                  }
                   </h5>
               `
                )
                .join("\n")
                .toLocaleUpperCase()}

                  </body>
              </html>
          `
          );

          mywindow.document.close(); // necessary for IE >= 10
          mywindow.focus(); // necessary for IE >= 10*/

          mywindow.print();
          mywindow.close();

          return true;
        },

        async ExportCsv() {
          const data = $scope.dadosList.map((item) => ({
            ...item,
            leilao: this.leilao.id,
          }));
          const marca_modelo = [];
          data.forEach((item) => {
            marca_modelo.push(
              `('${item.marca_modelo_detran || item.marca_modelo}','${
                item.marca_modelo_detran || item.marca_modelo
              }')`
            );
          });
          console.log(marca_modelo);
          const { data: Result } = await axios.post(
            "https://extractor-api.herokuapp.com/csvextractor",
            {
              headers: {
                "Content-Type":
                  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
              },
              data,
            }
          );

          const res = await this.converterBase64ParaBlob(
            Result,
            `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"`
          );
          saveAs(res, `arc de exportacao ${this.leilao.descricao}.xls`);
        },

        async converterBase64ParaBlob(base64, type) {
          try {
            const res = await fetch(`data:${type};base64,${base64}`);
            return res.blob();
          } catch (error) {
            return error.message;
          }
        },

        async getPatioById(id_patio, returing = "all") {
          const { data } = await $http.get(
            `${urlServidor.urlServidorChatAdmin}/patios/busca/${id_patio}`
          );

          if (returing === "nome") {
            return data[0].nome;
          }

          if (returing === "all") {
            return data;
          }
        },

        async liberarleilaoUnique(id) {
          const result = $scope.dadosList.find(
            (Veiculo) => Veiculo.id_separacao_leila === id
          );
          try {
            await $http.post(
              `${urlServidor.urlServidorChatAdmin}/separacao-leilao/gravar-liberacao`,
              {
                ncv: result.NCV,
                id_leilao: this.leilao.id,
                data: this.leilao.data,
                lote: result.lote,
                id: result.id_separacao_leila,
                id_usuario: localStorage.getItem("id_usuario"),
                id_classificacao: result.id_classificacao,
                valor_abertura: result.valor_abertura,
              }
            );

            myFunctions.showAlert(
              `Veiculo ${result.NCV} Foi liberado com sucesso`
            );
          } catch (error) {
            myFunctions.showAlert(
              `Não foi possível fazer a liberação, entre em contato com o Adm do sistema`
            );
          }
        },

        async liberarleilao() {
          const result = $scope.dadosList.filter((Veiculo) => {
            if (Veiculo.lote) {
              return Veiculo;
            }
          });

          if (result.length == $scope.dadosList.length) {
            try {
              const res = result.map(async (item) => {
                console.log(item);
                return await Promise.all(
                  $http.post(
                    `${urlServidor.urlServidorChatAdmin}/separacao-leilao/gravar-liberacao`,
                    {
                      ncv: item.NCV,
                      id_leilao: this.leilao.id,
                      data: this.leilao.data,
                      lote: item.lote,
                      id: item.id_separacao_leila,
                      id_usuario: localStorage.getItem("id_usuario"),
                      id_classificacao: item.id_classificacao,
                      valor_abertura: item.valor_abertura || 0,
                    }
                  )
                );
              });

              myFunctions.showAlert(
                `Um total de ${result.length} Veículos foram liberados para Leilão`
              );
            } catch (error) {
              myFunctions.showAlert(
                `Não foi possível fazer a liberação, entre em contato com o Adm do sistema`
              );
            }
          } else {
            myFunctions.showAlert(
              `Todos os Veículos precisam de um Lote para liberação`
            );
          }
        },

        async gerarPlanilha() {
          const result = await Promise.all(
            this.dataForArGeneration.map(async (item) => {
              const GetDadosByPlaca = await $http.get(
                `${urlServidor.urlServidorChatAdmin}/integracoes/detran/${item.placa}`
              );

              const { data } = GetDadosByPlaca;
              const NotNull = data.length === 1 && data[0];

              return NotNull;
            })
          );
          const origem = this.dataForArGeneration;

          var dadosPlanilha = result.map((item, idx) => ({
            ...item,
            COMUNICAÇÃO: "",
            NCV: origem[idx].NCV,
            Placa: item.placa || origem[idx].placa,
            Marca: item.marca_modelo || origem[idx].marca_modelo_detran,
            Modelo: item.marca_modelo || origem[idx].marca_modelo_detran,
            Cor: item.cor || origem[idx].cor_detran,
            Ano: item.ano_modelo || origem[idx].ano_detran,
            Fabricação: item.ano_fabricacao || "",
            Renavan: item.renavam || "",
            "Num .Motor": item.numero_motor_bin || "",
            "Chassi Base": item.chassi || origem[idx].chassi_detran,
            Cliente: item.nome_proprietario || "",
            ...(item.cnpj_financeira && {
              "cnpj_financeira": item.cnpj_financeira || "",
            }),
            ...(item.cpf_cnpj_proprietario && {
              "cpf_cnpj_proprietario": item.cpf_cnpj_proprietario || "",
            }),
            ...(item.cpf_cnpj_cvenda && {
              "cpf_cnpj_cvenda": item.cpf_cnpj_cvenda || "",
            }),

            Endereço: item.endereco_proprietario || "",
            Bairro: item.bairro_proprietario || "",
            Cidade: item.municipio_proprietario || "",
            UF: item.uf_proprietario || "",
            CEP: item.cep_proprietario || "",
            TipoVeiculo: item.tipo || "",
            Restrição:
              `${item.restricao1} / ${item.bloqueio_furto} ` ||
              origem[idx].restricoes,
          }));
          const dados = [];
          dadosPlanilha.forEach((item,idx) => {
            dados.push({
              COMUNICAÇÃO: "0",
              NCV: item.NCV,
              Placa: item.Placa,
              Marca: item.Marca,
              Modelo: item.marca_modelo || origem[idx].marca_modelo_detran,
              Cor: item.Cor,
              Ano: item.Ano,
              Fabricação: item.Fabricação,
              Renavan: item.Renavan,
              "Num .Motor": item["Num .Motor"],
              "Chassi Base": item["Chassi Base"],
              Cliente: item.nome_proprietario,
              ...(item.cpf_cnpj_proprietario && {
                "CPF/CNPJ": item.cpf_cnpj_proprietario || "",
              }),
              Endereço: item.endereco_proprietario,
              Bairro: item.bairro_proprietario,
              Cidade: item.municipio_proprietario,
              UF: item.uf_proprietario,
              CEP: item.cep_proprietario,
              TipoVeiculo: item.tipo,
              Restrição: `${item.restricao1} / ${item.bloqueio_furto} `,
            });

            if (item.Restrição.includes("VENDA")) {
              dados.push({
                COMUNICAÇÃO: "1",
                NCV: item.NCV,
                Placa: item.Placa,
                Marca: item.Marca,
                Modelo: item.marca_modelo || origem[idx].marca_modelo_detran,
                Cor: item.Cor,
                Ano: item.Ano,
                Fabricação: item.Fabricação,
                Renavan: item.Renavan,
                "Num .Motor": item["Num .Motor"],
                "Chassi Base": item["Chassi Base"],
                Cliente: item.nome_cvenda,
                ...(item.cpf_cnpj_cvenda && {
                  "CPF/CNPJ": item.cpf_cnpj_cvenda || "",
                }),
                Endereço: item.endereco_cvenda,
                Bairro: item.bairro_cvenda,
                Cidade: item.municipio_cvenda,
                UF: item.uf_cvenda,
                TipoVeiculo: item.tipo,
                Restrição: `${item.restricao1} / ${item.bloqueio_furto} `,
              });
            }
            if (item.cnpj_financeira) {
              dados.push({
                COMUNICAÇÃO: "2",
                NCV: item.NCV,
                Placa: item.Placa,
                Marca: item.Marca,
                Modelo: item.marca_modelo || origem[idx].marca_modelo_detran,
                Cor: item.Cor,
                Ano: item.Ano,
                Fabricação: item.Fabricação,
                Renavan: item.Renavan,
                "Num .Motor": item["Num .Motor"],
                "Chassi Base": item["Chassi Base"],
                Cliente: item.nome_financeira || "",
                ...(item.cnpj_financeira && {
                  "CPF/CNPJ": item.cnpj_financeira || "",
                }),
                TipoVeiculo: item.tipo,
                Restrição: `${item.restricao1} / ${item.bloqueio_furto} `,
              });
            }
          });
          alasql(
            'SELECT * INTO XLSX("veiculos-leilao.xlsx",{headers:true}) FROM ?',
            [dados]
          );
        },
      };

      $scope.gerarPDF = function () {
        var doc = new jsPDF({ orientation: "landscape" });

        var totalPagesExp = "{total_pages_count_string}";

        doc.setFontSize(10);

        doc.autoTable({
          columnStyles: {
            vencimento: {
              halign: "left",
            },
          },

          body: $scope.dadosList,
          columns: [
            {
              header: "NCV",
              dataKey: "id",
            },

            {
              header: "Ano",
              dataKey: "ano",
            },

            {
              header: "Placa",
              dataKey: "placa",
            },

            {
              header: "Placa Municipio",
              dataKey: "placa_municipio",
            },
            {
              header: "Placa UF",
              dataKey: "placa_uf",
            },
            {
              header: "Marca/Modelo",
              dataKey: "marca_modelo",
            },
            {
              header: "Número do Motor",
              dataKey: "numero_motor",
            },
            {
              header: "Chassi",
              dataKey: "chassi",
            },
          ],
          bodyStyles: {
            margin: 10,
            fontSize: 08,
          },

          didDrawPage: function (data) {
            // Header
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle("normal");

            doc.text(
              "Relatorio de veiculos separados para leilão",
              data.settings.margin.left + 15,
              22
            );

            // Footer
            var str = "Pagina " + doc.internal.getNumberOfPages();
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === "function") {
              str = str + " de " + totalPagesExp;
              var totalpaginas = totalPagesExp;
            }

            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height
              ? pageSize.height
              : pageSize.getHeight();

            doc.text(
              str + "                        ",
              data.settings.margin.left,
              pageHeight - 10
            );
          },
          margin: {
            top: 30,
          },
        });
        if (typeof doc.putTotalPages === "function") {
          doc.putTotalPages(totalPagesExp);
        }

        doc.save("relatorio-separacao-leilao.pdf");
      };

      $scope.Enable = true;

      $scope.DadosPeriodo = {
        ApreeDe: new Date(),
        hoje: new Date(),
        id_patio: "",
        id_leilao: "",
      };

      $http
        .put(
          urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
          {
            idUsuario: localStorage.getItem("id_usuario"),
            idEmpresa: localStorage.getItem("id_empresa"),
          }
        )
        .then(({ data }) => {
          $scope.patios = data.filter((item) => {
            return item.status == !0;
          });
        });

      $scope.SeparacaoPeriodo = {
        hoje: new Date(),
        mostrarPatiosCadastrado: true,
        // Methodos
        mesAtual: function () {
          $scope.DadosPeriodo.ApreeDe = new Date(
            this.hoje.getFullYear(),
            this.hoje.getMonth(),
            01
          );
          $scope.DadosPeriodo.ApreeAte = new Date(
            this.hoje.getFullYear(),
            this.hoje.getMonth() + 1,
            0
          );
        },
        getPatioCadastrados: async ({ id: id_leilao }) => {
          const response = await $http.get(
            `${urlServidor.urlServidorChatAdmin}/patios/listar-leilaopatio`,
            {
              params: {
                id_leilao,
              },
            }
          );
          const { data } = response;
          $scope.patiosCadastrados = data.map((item) => item.nome);
          $scope.patiosCadastradosID = data.map((item) => item.id);
        },

        GetLeilao: function (dados) {
          this.mostrarPatiosCadastrado = false;

          $scope.DadosPeriodo.id_patio = dados.id_patio;
          $scope.DadosPeriodo.id_leilao = dados.id;
        },

        // Busca NCV ou Placa
        Buscar: async function () {
          const response = await $http.get(
            `${urlServidor.urlServidorChatAdmin}/separacao-leilao/listar-periodo`,
            {
              params: {
                data_final: $scope.DadosPeriodo.ApreeDe,
                id_patios: $scope.patiosCadastradosID,
                // id_patio: $scope.DadosPeriodo.id_patio,
              },
            }
          );
          const { data } = response;

          $scope.Dados = data.map((item) => ({
            ...item,
            restricoes:
              item.restricoes === null ? "NADA CONSTA" : item.restricoes,
          }));
          $scope.Enable = false;
        },

        // Habilita as datas
        enableDate: () => {
          $scope.Enable = false;
        },

        // Desabilita as datas

        DisableDate: () => {
          $scope.Enable = true;

          $scope.DadosPeriodo = {
            ApreeDe: new Date(),
            hoje: new Date(),
            id_patio: "",
            id_leilao: "",
          };
        },

        // Limpa os campos
        limpar: () => {
          delete $scope.DadosPeriodo;
        },

        LimparResultado: () => {
          $scope.Dados = "";
        },

        Selecionar: "SELECIONAR TODOS",

        SelecionarTodos(Dados) {
          if (this.Selecionar === "SELECIONAR TODOS") {
            Dados.forEach((dados) => {
              dados.selecionado = true;
            });
            this.Selecionar = "DESMARCAR TODOS";
          } else {
            Dados.forEach((dados) => {
              dados.selecionado = false;
            });
            this.Selecionar = "SELECIONAR TODOS";
          }
        },

        Inserir(Dados) {
          const result = Dados.filter((Veiculo) => {
            return Veiculo.selecionado;
          });

          if (result.length > 0) {
            result.forEach((element) => {
              $http.post(
                `${urlServidor.urlServidorChatAdmin}/separacao-leilao/cadastrar`,
                {
                  ncv_placa: element.NCV,
                  id_leilao: $scope.DadosPeriodo.Leilao.id,
                }
              );
            });
          } else {
            return myFunctions.showAlert(
              `Nenhum Veiculo foi selecionado, Verifique`
            );
          }
          myFunctions.showAlert(
            `${result.length} Veiculos foram Adicionados à  o leilão selecionado`
          );
          this.LimparResultado();
          this.DisableDate();
        },
      };

      $scope.gerarPlanilhaPerido = () => {
        alasql(
          'SELECT * INTO XLSX("relatorio-contas-a-receber.xlsx",{headers:true}) FROM ?',
          [$scope.Dados]
        );
      };

      $scope.gerarPDFPeriodo = function () {
        $scope.Dados;

        var doc = new jsPDF({ orientation: "landscape" });

        var totalPagesExp = "{total_pages_count_string}";

        doc.setFontSize(10);

        doc.autoTable({
          columnStyles: {
            vencimento: {
              halign: "left",
            },
          },

          body: $scope.Dados,
          columns: [
            {
              header: "NCV",
              dataKey: "id",
            },

            {
              header: "Ano",
              dataKey: "ano",
            },

            {
              header: "Data de Apreensão",
              dataKey: "data_apreensao",
            },

            {
              header: "Hora de Apreensão",
              dataKey: "hora_apreensao",
            },
            {
              header: "KM percorrido",
              dataKey: "km_percorrido",
            },
            {
              header: "Marca/Modelo",
              dataKey: "marca_modelo",
            },
            {
              header: "Patio",
              dataKey: "patio",
            },
            {
              header: "Placa",
              dataKey: "placa",
            },
            {
              header: "Tipo do Veiculo",
              dataKey: "tipo_veiculo",
            },
          ],
          bodyStyles: {
            margin: 10,
            fontSize: 08,
          },

          didDrawPage: function (data) {
            // Header
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle("normal");

            doc.text(
              "Relatorio de veiculos separados para leilão",
              data.settings.margin.left + 15,
              22
            );

            // Footer
            var str = "Pagina " + doc.internal.getNumberOfPages();
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === "function") {
              str = str + " de " + totalPagesExp;
              var totalpaginas = totalPagesExp;
            }

            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height
              ? pageSize.height
              : pageSize.getHeight();

            doc.text(
              str + "                        ",
              data.settings.margin.left,
              pageHeight - 10
            );
          },
          margin: {
            top: 30,
          },
        });
        if (typeof doc.putTotalPages === "function") {
          doc.putTotalPages(totalPagesExp);
        }

        doc.save("relatorio-separacao-selecao-periodo.pdf");
      };

      $scope.ImageExport = {
        images: "",
        imageToBeSent: "",
        ShowSucessModal: false,
        leiloes: "",
        dados: [],
        ShowExportingStatusModal: false,

        uploadFotoConferente(files) {
          this.images = files.map((item, key) => ({
            originalItem: item,
            ...item,
            targetLeilao: item.name.split("_")[1].split(".")[0],
            lote: item.name.split("_")[0].replace(/[^0-9]/g, ""),
            id: key,
            blob: "",
          }));
        },

        async createImage(target) {
          const blob = await target.arrayBuffer().then(
            (arrayBuffer) =>
              new Blob([new Uint8Array(arrayBuffer)], {
                type: target.type,
              })
          );
          return blob;
        },

        async sendImage() {
          try {
            alert("Iniciando Envio de imagens, aguarde a mensagem de sucesso");
            const data = await Promise.all(
              this.images.map(async (item) => {
                const formData = new FormData();

                const blob = await this.createImage(item.originalItem);

                formData.append("foto", blob);
                formData.append("targetLeilao", item.targetLeilao);
                formData.append("lote", item.lote);

                return await axios.post(
                  "https://extractor-api.herokuapp.com/photosextractor",
                  formData,
                  {
                    headers: {
                      "Content-Type": `multipart/form-data`,
                    },
                  }
                );
              })
            );

            data.forEach(({ data }) =>
              data.forEach(({ modified }) => this.dados.push(modified))
            );
            const dados = this.dados;
            await $mdDialog.show({
              controller: function ($scope, $mdDialog) {
                $scope.dados = dados;

                $scope.calcel = function () {
                  $mdDialog.hide();
                };
              },
              templateUrl: "sucessModal.html",
              scope: $scope,
              preserveScope: true,
              scopoAnterior: $scope,
            });
            this.images = [];
          } catch (error) {
            alert("um erro ocorreu", error);
          }
        },

        deleteFotoConferente(id) {
          console.log(id);
          this.images = this.images.filter((image) => image.id !== id);
        },
      };
    }
  );
