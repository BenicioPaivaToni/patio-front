'use strict';
angular.module('app')
  .controller("receberNovoCtrl", function ($location, $scope, $http, urlServidor, myFunctions, $mdDialog, estados, viaCep, $rootScope) {

    $scope.dados = {
      id_cliente: '',
      valor: '',
      documento: '',
      vencimento: '',
      tipo_recebimento: '',
      grupo: '',
      categoria: '',
      data_venda: new Date(),
      comentario: '',
      patio: '',
      idempresa: localStorage.getItem('id_empresa')
    }


    // Vamos esconder nossa mensagem de erro e tambem o container
    document.getElementById("Container").style.display = "none"
    document.getElementById("ErrorLength").style.display = "none"

    // Caso o usuário Clique No input porém não mude nada, corre o risco do Modal não sumir
    //Para isso chamaremos uma função caso o usuario não mude nada e desfoque o input

    $scope.HideModal = () => {
      // o Set timeout vai nos ajudar para não correr o risco de o modal fechar antes do usuario clicar
      setTimeout(() => {
        document.getElementById("Container").style.display = "none"

      }, 2000);
    }



    $scope.isDisabled = false



    $scope.ChangeScope = (clientes) => {
      // Vamos substituir o inupt texte, visto que não precisamos mais do container ele receber um none
      $scope.DadosGeraisClientes = clientes
      $scope.dados.id_cliente = clientes.nome

      document.getElementById("Container").style.display = "none"

    }


    $scope.buscarClientes = async () => {


      const length = $scope.dados.id_cliente.length
      //Somente com 3 letras iremos iniciar a pesquisa
      // Tendo 3 letras o container vai aparecere e o erro some
      if (length >= 3) {
        document.getElementById("Container").style.display = "block"
        document.getElementById("ErrorLength").style.display = "none"

        // fazemos a chamda e retornamos os dados
        const response = await $http.put(urlServidor.urlServidorChatAdmin + '/clientes/listar', { razao_social: $scope.dados.id_cliente })
        let dados = response.data;
        $scope.clientes = dados;
        //se for menor que 3 o modal some e o erro aparece
      } else if (length < 3) {
        document.getElementById("ErrorLength").style.display = "block"
        document.getElementById("Container").style.display = "none"
        // se estiver vazio o container some
      } else if ($scope.dados.id_cliente == '') {
        document.getElementById("Container").style.display = "none"
        alert("h")
      }


    }


    async function buscaClientes() {


      const length = $scope.dados.id_cliente.length + 1
      if (length >= 3) {
        const response = await $http.put(urlServidor.urlServidorChatAdmin + '/clientes/listar', { razao_social: $scope.dados.id_cliente })
        let dados = response.data;
        $scope.clientes = dados;

      }


    }


    buscaGrupos().then(function (result) {
      $scope.grupos = result;
    });

    buscaPatios().then(function (result) {
      $scope.patios = result;
    });

    buscaTipoRecebimento().then(function (result) {
      $scope.tiposRecebimentos = result;
    });

    $scope.SelecionaCategoria = function (idgrupo) {
      buscaCategorias(idgrupo).then(function (result) {
        $scope.categorias = result;
      });
    }



    $scope.gravar = async () => {
      $scope.dados.id_cliente = $scope.DadosGeraisClientes.id

      const response = await $http.post(`${urlServidor.urlServidorChatAdmin}/receber/cadastrar`, $scope.dados)

      const { data } = response
      if (data.code) {
        myFunctions.showAlert('Erro na gravação!')

      } else if (response.data == '11000') {
        myFunctions.showAlert('Receber já existe!')

      } else {
        myFunctions.showAlert('Cadastro executado com sucesso!');
        $scope.isDisabled = true;
        console.log(data);
      }
    };






    function buscaTipoRecebimento() {
      return new Promise(function (resolve, reject) {
        $http.put(urlServidor.urlServidorChatAdmin + '/tipos-recebimento/listar-tipo', { tipo: 'RECEBER' }).then(function (response) {
          let dados = response.data;
          resolve(dados);
        });
      })
    }

    function buscaPatios(idgrupo) {
      return new Promise(function (resolve, reject) {
        $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar').then(function (response) {
          let dados = response.data;
          resolve(dados);
        });
      })
    }

    function buscaCategorias(idgrupo) {
      return new Promise(function (resolve, reject) {
        $http.put(urlServidor.urlServidorChatAdmin + '/categorias/listar', { grupo: idgrupo }).then(function (response) {
          let dados = response.data;
          resolve(dados);
        });
      })
    }

    function buscaGrupos() {

      return new Promise(function (resolve, reject) {

        $http.put(urlServidor.urlServidorChatAdmin + '/grupos/listar').then(function (response) {

          let dados = response.data;

          dados.sort(function (a, b) {
            if (a.descricao < b.descricao) {
              return 1;
            }
            if (a.descricao > b.descricao) {
              return -1;
            }
            return 0;
          });

          resolve(dados);

        });

      })

    }




    $scope.incluiCliente = () => {


      $mdDialog.show({

        controller: function ($scope, $mdDialog, $mdToast) {


          var _novo = false;



          $scope.dados_cliente = {
            nome: '',
            cpf_cnpj: '',
            endereco: '',
            bairro: '',
            cidade: '',
            cep: '',
            uf: '',
            email: '',
            telefone: '',
            status: '1',
            idempresa: localStorage.getItem('id_empresa')
          }


          $scope.cancel = () => { $mdDialog.cancel() };

          $scope.gravaCliente = () => {

            if (_novo) {

              $http.post(urlServidor.urlServidorChatAdmin + '/clientes/cadastrar', $scope.dados_cliente).then(function (response) {


                BuscaClientesIfIsNew()



                $rootScope.idCliente = response.data.insertId;
              });

            } else {
              myFunctions.showAlert('CPF já cadastrado!')
              $rootScope.idCliente = $scope.dados_cliente.id;

            }

          };

          $scope.saveAndClose = function () {
            $scope.gravaCliente();





            $rootScope._cliente = $scope.dados_cliente;
            $mdDialog.hide();

          };



          $scope.estados = estados.uf;

          $scope.buscaCep = (cep) => {

            if (cep.length == 9) {

              viaCep.get(cep).then(function (response) {

                $scope.dados_cliente.endereco = response.logradouro.toUpperCase();
                $scope.dados_cliente.bairro = response.bairro.toUpperCase();
                $scope.dados_cliente.cidade = response.localidade.toUpperCase();
                $scope.dados_cliente.uf = response.uf.toUpperCase();

              });

            }

          }

          $scope.buscaCpfCnpj = async (cpfCnpj) => {


            try {


              if (cpfCnpj.length == 14 || cpfCnpj.length == 18) {

                const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/clientes/busca-cpfcnpj`, { params: { cpf_cnpj: cpfCnpj } })

                const { data } = response




              }


            } catch (error) {
              console.log(error);
            }




            if (cpfCnpj.length == 14 || cpfCnpj.length == 18) {
              $http.get(urlServidor.urlServidorChatAdmin + '/clientes/busca-cpfcnpj', { params: { cpf_cnpj: cpfCnpj } }).then(function (response) {

                $scope.motoristas = response.data;

                if (response.data.length > 0) {
                  $scope.dados_cliente.cep = response.data[0].cep;
                  $scope.dados_cliente.nome = response.data[0].nome;
                  $scope.dados_cliente.endereco = response.data[0].endereco;
                  $scope.dados_cliente.bairro = response.data[0].bairro;
                  $scope.dados_cliente.cidade = response.data[0].cidade;
                  $scope.dados_cliente.uf = response.data[0].uf;
                  $scope.dados_cliente.email = response.data[0].email;
                  $scope.dados_cliente.telefone = response.data[0].telefone;
                  $scope.dados_cliente.id = response.data[0].id;
                  _novo = false;

                } else {
                  _novo = true;

                }

              });

            }

          }


        },
        templateUrl: 'cliente-receber.html',
        preserveScope: true

      }).then(function (answer) {
        let a = answer;
        buscaClientes().then(function (response) {
          $scope.$resolve.$scope.clientes = response;
          $scope.$resolve.$scope.dados.id_cliente = $rootScope.idCliente;
        });
      }, function () {
        $scope.statusdialog = 'You cancelled the dialog.';
      });


    }
  })
