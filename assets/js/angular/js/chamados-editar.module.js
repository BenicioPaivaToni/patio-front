angular.module('app').controller('chamadosEditarCtrl', function ($scope,$location, $http, urlServidor,  $mdDialog,myFunctions) {


    const _id = $location.search().id;
    

    $scope.dados = null;
    $scope.id_empresa=  localStorage.getItem('id_empresa')

    var mymap = '';
    var mymap_chamado = '';
    $scope.disabled_cancelar = false;
    var lat_long_chamado = ''     


    $http.get(urlServidor.urlServidorChatAdmin + '/autoridades/vercodigo').then(function (response) {        
        $scope.chavegoole = response.data.googlemapsKey
    }).finally( function(){

        let script = document.createElement('script')
        script.type = 'text/javascript';
/*
        script.src = 'https://maps.googleapis.com/maps/api/js?key='+ $scope.chavegoole +'&callback=initialize';
*/
        script.src = 'https://maps.googleapis.com/maps/api/js?key='+ $scope.chavegoole 
        document.body.appendChild(script);
        setTimeout(function() {

            $http.get(urlServidor.urlServidorChatAdmin + '/chamados/converte-endereco-googlemaps', { params: { endereco: $scope.dados.local_chamado } }).then(function (response) {
                if(response.data.lat !== undefined  ){
                    $scope.dados.latitude_chamador = response.data.lat;
                    $scope.dados.longitude_chamador = response.data.lng; 
                    lat_long_chamado =   response.data.lat+','+response.data.lng
                    $scope.initialize(response.data.lat,response.data.lng)    
                    $scope.localizacao_naoOk = false;        
                } else {
                    $scope.localizacao_naoOk = true;
                    $scope.initialize(0.00,0.0)
                }
            });            

        }, 500);

    })

    $scope.initialize = function(lat,long) {
        $scope.mapOptions = {
            zoom: 18,
            center: new google.maps.LatLng(lat,long)
        };
        const cairo = { lat: lat, lng: long };
        let map = new google.maps.Map(document.getElementById('googleMap'), $scope.mapOptions);
        const marker = new google.maps.Marker({ map, position: cairo });
    }



    const buscaDados = async (id) => {
        const response = await $http.get(urlServidor.urlServidorChatAdmin + '/chamados/busca-chamado-id', { params: { id: id } })
        return await response.data[0]
    }


    buscaDados(_id).then(function (result) {
            $scope.dados = result;        

            $scope.buscaCidadesByPatio = async () => {
                const result = await buscaCidadesByPatio()
                $scope.CidadesByPatio = result ;
                const cidade = result.find((x) => x.id_cidade === $scope.dados.id_cidade)
                $scope.dados.id_cidade = cidade ? cidade.id_cidade : null              
            }


              buscaPatios().then(function (result) {
                var id_patio = $scope.dados.id_patio;
                $scope.patios = result ;
                $scope.patio_termo = $scope.patios.filter(function (obj) {
                   
                return obj.id == id_patio;
                })
                
                $scope.endereco_patio = $scope.patio_termo[0];
            });

            $scope.buscaCidadesByPatio()


            $http.get(urlServidor.urlServidorChatAdmin + '/tipos_veiculo/listar', {params: {idEmpresa: $scope.id_empresa}}).then(function (response) {
                $scope.tiposVeiculos = response.data;
            })
/*
            $http.get(urlServidor.urlServidorChatAdmin + '/chamados/converte-endereco', { params: { endereco: $scope.dados.local_chamado } }).then(function (response) {
                if(response.data.length > 0 ){
                    $scope.dados.latitude_chamador = response.data[0].lat;
                    $scope.dados.longitude_chamador = response.data[0].lon; 
                    lat_long_chamado =   response.data[0].lat+','+response.data[0].lon
                    
                    $http.post('https://nominatim.openstreetmap.org/reverse?lat=' + $scope.dados.latitude_chamador + '&lon=' + $scope.dados.longitude_chamador + '&format=json' + '&addressdetails=1').then(function (response) {

                        let resultado = response.data.address;
                        if (resultado.house_number == undefined) {
                            $scope.dados.localizacao_chamador = resultado.road + ' ' +
                                '  - ' + resultado.suburb +
                                '  CEP ' + resultado.postcode +
                                '  - ' + resultado.city_district;
                        } else {
                            $scope.dados.localizacao_chamador = resultado.road + ' ' + resultado.house_number +
                                '  - ' + resultado.suburb +
                                '  CEP ' + resultado.postcode +
                                '  - ' + resultado.city_district;
                        }
        
        
                    });
                            
                } else {
                    $scope.localizacao_naoOk = true;
                }
            });
*/


let map;

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: $scope.dados.latitude_chamador  , lng: $scope.dados.longitude_chamador },
    zoom: 8,
  });
}

window.initMap = initMap;


            lat_long_patio = $scope.dados.latitude_chamador + ',' + $scope.dados.longitude_chamador;
            $scope.listaImagensChamado = [];
            $scope.listaImagensChamado.push({
                imagens:
                  `https://s3.sa-east-1.amazonaws.com/fotos.chamados/${$scope.dados.ncv}/imagem1.jpg`,
              });
              $scope.listaImagensChamado.push({
                imagens:
                `https://s3.sa-east-1.amazonaws.com/fotos.chamados/${$scope.dados.ncv}/imagem2.jpg`,
              });
              $scope.listaImagensChamado.push({
                imagens:
                `https://s3.sa-east-1.amazonaws.com/fotos.chamados/${$scope.dados.ncv}/imagem3.jpg`,
              });

            $scope.botao_encaminhar = ($scope.dados.status !== 'Aguardando');
            $scope.botao_cancelar = (($scope.dados.status == 'Aguardando' || $scope.dados.status == 'Em Atendimento') && $scope.autorizar);



            $http.post('https://nominatim.openstreetmap.org/reverse?lat=' + $scope.dados.latitude_atendimento + '&lon=' + $scope.dados.longitude_atendimento + '&format=json' + '&addressdetails=1').then(function (response) {

                let resultado = response.data.address;
                if (resultado.house_number == undefined) {
                    $scope.dados.localizacao_motorista = resultado.road + ' ' +
                        '  - ' + resultado.suburb +
                        '  CEP ' + resultado.postcode +
                        '  - ' + resultado.city_district;
                } else {
                    $scope.dados.localizacao_motorista = resultado.road + ' ' + resultado.house_number +
                        '  - ' + resultado.suburb +
                        '  CEP ' + resultado.postcode +
                        '  - ' + resultado.city_district;
                }

            });

    });
    
          
    $scope.ShowButtonGravar = false
    $scope.IsEditable = false

    $scope.EditarChamado = {


        async GetInformaçoes() {
            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/chamados/busca-chamado-id', { params: { id: _id } })
            const { status } = response.data[0]


            status === 'Aguardando' ? $scope.ShowButtonGravar = true : $scope.ShowButtonGravar = false
            $scope.IsEditable =  !$scope.ShowButtonGravar

        },
        async Gravar(Dados) {
           await $http.post(urlServidor.urlServidorChatAdmin + '/chamados/alterar-chamado',
                {
                    idchamado: Dados.id,
                    equipamentoSolicitado: Dados.equipamento,
                    tipoVeiculo: Dados.id_tipo_veiculo ,
                    estadoVeiculo: Dados.estado_veiculo,
                    localChamado: Dados.local_chamado,
                    ncv:Dados.ncv,
                    id_patio:Dados.id_patio,
                    id_cidade:Dados.id_cidade,
                    complemento:Dados.complemento
                    
                })
                .then(()=>{
                    alert("Chamado alterado com sucesso")
                    window.location.replace('/#/chamados')
                })
                
              

        }


    }

    $scope.EditarChamado.GetInformaçoes()

    if (localStorage.getItem('tipo_usuario') == 'admin' || localStorage.getItem('tipo_usuario') == 'cco') {
        $scope.autorizar = true;
    } else {
        $scope.autorizar = false;
    }



    $scope.selectTabMapa = function () {

        //	var latlng = L.latLng(urlParams.dados[0].latitude_atendimento, urlParams.dados[0].longitude_atendimento);

        if (mymap.off != undefined) {
            mymap.off();
            mymap.remove();
        }
        mymap = L.map('mapid').setView([$scope.dados.latitude_atendimento, $scope.dados.longitude_atendimento], 15);
        mapLink = '<a href="https://openstreetmap.org">OpenStreetMap</a>';
        L.tileLayer(
            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; ' + mapLink,
            maxZoom: 20,
        }).addTo(mymap);
        //		var marker = L.marker([$scope.dados.latitude_chamador, $scope.dados.longitude_chamador]).addTo(mymap);

        var marker = L.marker([$scope.dados.latitude_atendimento, $scope.dados.longitude_atendimento]).addTo(mymap);
    }

    $scope.atualizar = function () {

        $http.get(urlServidor.urlServidorChatAdmin + '/chamados/converte-endereco-googlemaps', { params: { endereco: $scope.dados.local_chamado } }).then(function (response) {

            if( response.data.erro ){

                $scope.localizacao_naoOk = true;    

            } else {

                $scope.dados.latitude_chamador = response.data.lat;
                $scope.dados.longitude_chamador = response.data.lng;
                lat_long_chamado =   response.data.lat+','+response.data.lng  
                $scope.initialize(response.data.lat,response.data.lng) 
                $scope.localizacao_naoOk = false;    
    
            }

        });

    }


    $scope.selectTabMapaChamado = function () {

        //	var latlng = L.latLng(urlParams.dados[0].latitude_atendimento, urlParams.dados[0].longitude_atendimento);

        if (mymap_chamado.off != undefined) {
            mymap_chamado.off();
            mymap_chamado.remove();
        }
        mymap_chamado = L.map('mapid_chamado').setView([$scope.dados.latitude_chamador, $scope.dados.longitude_chamador], 15);
        mapLink = '<a href="https://openstreetmap.org">OpenStreetMap</a>';
        L.tileLayer(
            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; ' + mapLink,
            maxZoom: 20,
        }).addTo(mymap_chamado);
        var marker_chamado = L.marker([$scope.dados.latitude_chamador, $scope.dados.longitude_chamador]).addTo(mymap_chamado);
        //		var marker = L.marker([$scope.dados.latitude_atendimento, $scope.dados.longitude_atendimento]).addTo(mymap);		

    }


    $scope.selectTabMapaChamadoGoogle = function () {

        let map;

        function initMap() {
          map = new google.maps.Map(document.getElementById("map"), {
            center: { lat: -34.397, lng: 150.644 },
            zoom: 8,
          });
        }
        
        window.initMap = initMap;        

    }


    function atualizaTabMapaChamado () {

        //	var latlng = L.latLng(urlParams.dados[0].latitude_atendimento, urlParams.dados[0].longitude_atendimento);
        return new Promise(function (resolve, reject) {

            if (mymap_chamado.off != undefined) {
                mymap_chamado.off();
                mymap_chamado.remove();
            }
            mymap_chamado = L.map('mapid_chamado').setView([$scope.dados.latitude_chamador, $scope.dados.longitude_chamador], 15);
            mapLink = '<a href="https://openstreetmap.org">OpenStreetMap</a>';
            L.tileLayer(
                'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: 'Map data &copy; ' + mapLink,
                maxZoom: 20,
            }).addTo(mymap_chamado);
            var marker_chamado = L.marker([$scope.dados.latitude_chamador, $scope.dados.longitude_chamador]).addTo(mymap_chamado);
            //		var marker = L.marker([$scope.dados.latitude_atendimento, $scope.dados.longitude_atendimento]).addTo(mymap);		

            resolve(true);

        })
    }


    function atualizaTabMapaChamadoGoogle () {

        let map;

        function initMap() {
          map = new google.maps.Map(document.getElementById("map"), {
            center: { lat: -34.397, lng: 150.644 },
            zoom: 8,
          });
        }
        
        window.initMap = initMap;        

    }


    $scope.cancelar = function (id) {

        $mdDialog.show({
            controller: function ($scope, $mdDialog, $mdToast) {

                $scope.cancelar_chamado = function () {
                    $http.post(urlServidor.urlServidorChatAdmin + '/chamados/cancelar', { id: id, ncv: $scope.dados.ncv, motivo_cancelamento: $scope.dados.motivo_cancelamento }).then(function (response) {
                        if (response.data.code) {
                            myFunctions.showAlert('Erro na gravação!')
                        } else {
                            myFunctions.showAlert('Cancelado com sucesso!');
                            $scope.disabled_cancelar = true;
                        }
                        ;
                    });
                }
                $scope.fechar = function () {
                    $mdDialog.cancel();
                };


            },
            templateUrl: 'cancelar-chamado.html',
            scope: $scope,
            preserveScope: true,
            scopoAnterior: $scope
        }).then(function (answer) {
            let a = answer;

        }, function () {
            $scope.statusdialog = 'You cancelled the dialog.';
        });

    }

    $scope.concluir = function (id) {

        let confirm = $mdDialog.confirm()
            .title('Concluir chamado!')
            .textContent('Deseja concluir manualmente o checklist?')
            .ariaLabel('Lucky day')
            .ok('Sim')
            .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {

            $http.post(urlServidor.urlServidorChatAdmin + '/chamados/concluir', { idchamado: id }).then(function (response) {
                if (response.data.code) {
                    myFunctions.showAlert('Erro na gravação!')
                } else {
                    myFunctions.showAlert('Concluido com sucesso!');
                    $scope.disabled_concluir = true;
                }
                ;
            });

        });

    }


    $scope.showCustomConfirm = function (idchamado, idmotorista) {

        var id_ncv = $scope.dados.ncv

        var id_chamado = idchamado;
        function buscaItemArray(arrayItens, id) {
            return new Promise(function (resolve, reject) {

                for (var x in arrayItens) {
                    if (arrayItens[x].id == id) {
                        resolve(arrayItens[x].latitude + ',' + arrayItens[x].longitude);
                    }
                }

            })
        }

        function buscaMotoristaArray(arrayItens, id) {
            return new Promise(function (resolve, reject) {

                for (var x in arrayItens) {
                    if (arrayItens[x].id_usuario_app == id) {
                        resolve(x);
                    }
                }

            })
        }

        $mdDialog.show({
            controller: function ($scope, $rootScope, $mdDialog) {
/*
                $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') }).then(function (response) {
                    $scope.patios = response.data;
                });
                
                $http.put(urlServidor.urlServidorChatAdmin + '/empresas_reboques/listar').then(function (response) {
                    $scope.empresas = response.data;
                });
*/
//                $http.get(urlServidor.urlServidorChatAdmin + '/motoristas/motoristas-distancia', { params: { id_reboque: idEmpresaReboque, dadospatio: lat_long_patio } }).then(function (response) {
                $http.get(urlServidor.urlServidorChatAdmin + '/motoristas/motoristas-distancia-novo', { params: { dadospatio: lat_long_chamado } }).then(function (response) {
                   // console.log(response)
//                    $scope.motoristas = response.data;
                    $scope.motoristas = response.data.filter(function(obj) { return obj.distancia; });

                    setTimeout(() => {
                        $scope.motoristas.sort(function(a,b) {
                            if(parseInt(a.distancia) < parseInt(b.distancia)) return -1;
                            if(parseInt(a.distancia) > parseInt(b.distancia)) return 1;
                            return 0;
                        });
                          }, 1000);


                });

                $scope.SelecionaPatio = function (patio) {
                    buscaItemArray($scope.patios, patio).then(function (resp) {
                        $scope.lat_long_patio = resp;
                    })
                }

                $scope.MotoristaSelecionado = function (dados) {
                    $scope.id_Motorista_app = dados.idMotorista;
                }


                $scope.atualizaDistancia = function () {

                    $http.get(urlServidor.urlServidorChatAdmin + '/motoristas/motoristas-distancia-novo', { params: { dadospatio: lat_long_chamado, botaodistancia: true } }).then(function (response) {
                        
                        $scope.motoristas = response.data.filter(function(obj) { return obj.distancia; });
            
                        setTimeout(() => {
                        
                            $scope.motoristas.sort(function(a,b) {
                                if(parseInt(a.distancia) < parseInt(b.distancia)) return -1;
                                if(parseInt(a.distancia) > parseInt(b.distancia)) return 1;
                                return 0;
                            });
                        
                        }, 2000);
            
            
                    });
                }


                $scope.SelecionaMotoristas = function (idEmpresaReboque) {

                    $http.get(urlServidor.urlServidorChatAdmin + '/motoristas/motoristas-distancia-novo', { params: { id_reboque: idEmpresaReboque, dadospatio: lat_long_patio } }).then(function (response) {

                        $scope.motoristas = response.data;

                        $scope.motoristas = response.data.filter(function(obj) { return obj.distancia; });
    
                        setTimeout(() => {
                            $scope.motoristas.sort(function(a,b) {
                                if(parseInt(a.distancia) < parseInt(b.distancia)) return -1;
                                if(parseInt(a.distancia) > parseInt(b.distancia)) return 1;
                                return 0;
                            });
                        }, 1000);

                    });

                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.save = function (idmotoristaApp) {

                    $scope.dados.idchamado = id_chamado;
                    // $scope.dados.idMotorista = $scope.id_Motorista_app

                    var id_motorista = $scope.motoristas.find((el) => el.id_usuario_app === $scope.id_Motorista_app)

                    $http.post(urlServidor.urlServidorChatAdmin + '/chamados/encaminhar', $scope.dados).then(function (response) {

                        if (response.data.code) {
                            myFunctions.showAlert('Erro na gravação!')
                        } else {

                          $http.post(urlServidor.urlServidorChatAdmin + '/chamados/atualiza-motorista-ncv',
                                { id_ncv: id_ncv, id_motorista: $scope.dados.idMotorista }).then(function (response) {
                      
                                if (response.data.code) {
                                    myFunctions.showAlert("Erro na gravação!");
                                }
                            })


                            buscaMotoristaArray($scope.motoristas, $scope.dados.idMotorista).then(function (resp) {

                                $scope.nomeMotorista = $scope.motoristas[resp].apelido;
                                $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/notificacao-motorista', { idpush: $scope.motoristas[resp].id_push, apelido: $scope.motoristas[resp].apelido, id: $scope.motoristas[resp].id_usuario_app }).then(function (response) {



                                });
                            })

                            myFunctions.showAlert('Alteração executada com sucesso!');
                        }
                        ;
                        $mdDialog.cancel();
                    });
                };
                $scope.saveAndClose = function () {
                    $mdDialog.hide();
                };
            },
            templateUrl: 'custom-confirm.html',
            preserveScope: true
        }).then(function (answer) {
            let a = answer;
        }, function () {
            $scope.statusdialog = 'You cancelled the dialog.';
        });
    };
    $scope.confirmar = function (idmotorista) {

        $scope.dados.encaminhar = false;
        $http.post(urlServidor.urlServidorChatAdmin + '/chamados/encaminhar', { idchamado: id, idmotorista: idmotorista }).then(function (response) {
            if (response.data.code) {
                myFunctions.showAlert('Erro na gravação!')
            } else {
                myFunctions.showAlert('Encaminhado com sucesso!');
            }
            ;
        });
    }


    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/tipos_veiculo/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    myFunctions.showAlert('Tarifa já existe!')
                } else {
                    myFunctions.showAlert('Alteração executada com sucesso!');
                }

            }
            ;
        });
    };



    function buscaPatios() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', {idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa')}).then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }



    function buscaCidadesByPatio() {
        return new Promise(function (resolve, reject) {

            $http.get(`${urlServidor.urlServidorChatAdmin}/cidades/listar-patios-cidades`, { params: { id_patio: $scope.dados.id_patio } }).then( function (response){
                let dados = response.data;
                resolve(dados);
            })       

        })    
    }    


   


})