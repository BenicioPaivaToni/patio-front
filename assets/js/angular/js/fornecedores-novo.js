'use strict';
angular.module('app')
.controller("fornecedoresNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions,estados,viaCep) {  

  $scope.dados = {
    razao_nome:'',
     tipo_pessoa:'',  
     id_tipo_fornecedor: 0 ,
     cpf_cnpj: '' ,
     endereco: '' ,
     bairro: '' ,
     cidade: '' ,
     cep: '' ,
     uf: '' ,
     email: '' ,
     telefone_comercial: '',
     celular: '',
     responsavel: '',
     responsavel_faturamento: '',
     tel_resp_faturamento: '',
     responsavel_logistica: '',
     tel_resp_logistica: '',
     dados_bancarios: '',
     forma_pagto: '',
     status: '' ,
     pix_chave: '',
     pix_tipo: '',
     idempresa: localStorage.getItem('id_empresa')
  }

  $scope.estados = estados.uf ;

  $scope.buscaCep = (cep) => {

		if(cep.length == 9){
      
			viaCep.get(cep).then(function(response){
			
				$scope.dados.endereco = response.logradouro.toUpperCase() ;
				$scope.dados.bairro = response.bairro.toUpperCase() ;
				$scope.dados.cidade = response.localidade.toUpperCase() ;
				$scope.dados.uf = response.uf.toUpperCase() ;				 

      });

		}	

	}



$scope.tipos = [
  {
    id: 1,
    descricao: 'IMPOSTOS,TAXAS'
  },
  {
    id: 2,
    descricao: 'AGUA,LUZ,TELEFONE,GAZ'
  },
  {
    id: 3,
    descricao: 'REBOQUES'
  },
  {
    id: 4,
    descricao: 'TRANSPORTADORAS'
  },
  {
    id: 5,
    descricao: 'ALUGUEL'
  },
  {
    id: 6,
    descricao: 'SEGUROS'
  },
  {
    id: 7,
    descricao: 'FOLHA PAGTO, COMISSOES'
  },
  {
    id: 8,
    descricao: 'PRESTADOR AUTONOMO'
  },
  {
    id: 9,
    descricao: 'MATERIAL ADMINISTRACAO'
  } ,
  {
    id: 10,
    descricao: 'OFICINAS'
  },
  {
    id: 11,
    descricao: 'OUTROS'
  } 
]
  

  $scope.isDisabled = false

  $scope.gravar = function() {        

    $http.post(urlServidor.urlServidorChatAdmin+'/fornecedores/cadastrar', $scope.dados ).then( function(response){

        if (response.data.code){
            
          myFunctions.showAlert('Erro na gravação : '+response.data.sqlMessage )

        }else{

          if ( response.data == '11000' )                 
          {
            myFunctions.showAlert('Fornecedor já existe!')
          }else
          {                    

            myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
            $scope.isDisabled = true ;            

          }  
                
        }; 

    }); 

  }; 

    
function buscaEmpresas(){

  return new Promise( function( resolve,reject){

  $http.put(urlServidor.urlServidorChatAdmin+'/empresas_reboques/listar').then( function(response){

    let dados = response.data ;

    dados.sort(function(a, b){
      if (a.nome < b.nome) {
      return 1;
      }
      if (a.nome > b.nome) {
      return -1;
      }        
      return 0;
    });       

    resolve(dados) ;

  });

  })

}
    
});