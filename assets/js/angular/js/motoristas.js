app.controller(
    "motoristasCtrl",
    function ($scope, $location, $http, urlServidor,$filter) {
      $scope.novo = function (ev, id) {
        $location.path("motoristas-novo");
      };
  
      $scope.limpar = function (ev) {
        $scope.dados.empresa = "";
        $scope.dados.nome = "";
        $scope.dados.cidade = "";
        $scope.dados.bairro = "";
        $scope.dados.endereco = "";
        $scope.dados.filtrar_por = "";
      };
      buscaEmpresas().then(function (result) {
        $scope.empresas = result;
      });
      $scope.pesquisar = function (ev) {
        $http
          .put(
            urlServidor.urlServidorChatAdmin + "/motoristas/listar",
            $scope.dados
          )
          .then(function (response) {
            let dados = response.data;
            $scope.dadosList = dados;
          });
      };
  
      $scope.detalhe = function (ev, rowId) {
        var dados = $scope.dadosList.filter(function (obj) {
          return obj.id == rowId;
        });
        $location.path("motoristas-editar").search({ dados });
      };
      $scope.excluir = function (ev, rowId, Id) {
        var dados = $scope.dadosList.filter(function (obj) {
          return obj.id == id;
        });
        $location.path("motoristas-editar").search({ dados });
      };
      function buscaEmpresas() {
        return new Promise(function (resolve, reject) {
          $http
            .put(urlServidor.urlServidorChatAdmin + "/empresas_reboques/listar")
            .then(function (response) {
              let dados = response.data;
              dados.sort(function (a, b) {
                
                if (a.nome < b.nome) {
                  return -1
                }
                if (a.nome > b.nome) {
                  return 1
                }
                return 0
                
              });
              resolve(dados);
            });
        });
      }
      
      $scope.gerarExcel= function () {
          const dadosPdf = $scope.dadosList.map((motorista) => ({
              ...motorista,
              cnh_validade: $filter("date")(
                motorista.cnh_validade,
                "dd/MM/yyyy",
                "+0000"
              ),
  
              data_cadastro: $filter("date")(
                motorista.data_cadastro,
                "dd/MM/yyyy",
                "+0000"
              ),
            }));
  
              alasql('SELECT * INTO XLSX("relatorio-contas-a-pagar.xlsx",{headers:true}) FROM ?', [dadosPdf]);
      };
  
  
  
      $scope.gerarPdf = function () {
        const dadosPdf = $scope.dadosList.map((motorista) => ({
          ...motorista,
          cnh_validade: $filter("date")(
            motorista.cnh_validade,
            "dd/MM/yyyy",
            "+0000"
          ),
  
          data_cadastro: $filter("date")(
            motorista.data_cadastro,
            "dd/MM/yyyy",
            "+0000"
          ),
        }));
  
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = "{total_pages_count_string}";
  
        doc.setFontSize(10);
        doc.autoTable({
          columnStyles: {
            vencimento: { halign: "left" },
          },
  
          body: dadosPdf,
  
          columns: [
            { header: "Nome", dataKey: "nome" },
            { header: "Email", dataKey: "email" },
            { header: "Celular", dataKey: "celular" },
  
            { header: "Uf", dataKey: "uf" },
            { header: "Cidade", dataKey: "cidade" },
            { header: "Bairro", dataKey: "bairro" },
            { header: "Cep", dataKey: "cep" },
            { header: "Endereço", dataKey: "endereco" },
  
            { header: "Cnh", dataKey: "cnh" },
            { header: "Categoria/Cnh", dataKey: "cnh_categoria" },
            { header: "Validade", dataKey: "cnh_validade" },
  
            { header: "Categoria", dataKey: "categoria" },
            { header: "Validade", dataKey: "cnh_validade" },
            { header: "Categoria", dataKey: "categoria" },
  
            { header: "Ativo", dataKey: "desc_ativo" },
            { header: "Observação", dataKey: "observacao" },
          ],
          bodyStyles: {
            margin: 10,
            fontSize: 08,
          },
          didDrawPage: function (data) {
            // Header
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle("normal");
  
            doc.text(
              `Relatorio de Motoristas empresa de Reboque`,
              data.settings.margin.left + 15,
              22
            );
  
            // Footer
            var str = "Pagina " + doc.internal.getNumberOfPages();
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === "function") {
              str = str + " de " + totalPagesExp;
              var totalpaginas = totalPagesExp;
            }
  
            var strTotal = "";
            doc.setFontSize(10);
  
            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height
              ? pageSize.height
              : pageSize.getHeight();
  
            doc.text(
              str + "                        " + strTotal,
              data.settings.margin.left,
              pageHeight - 10
            );
          },
          margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === "function") {
          doc.putTotalPages(totalPagesExp);
        }
  
        doc.save("Motoristas.pdf");
      };
    }
  );