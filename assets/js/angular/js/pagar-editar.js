angular.module('app')
.controller('pagarEditarCtrl', function ($scope, Upload, S3UploadService, $http, $location, myFunctions, urlServidor, $mdDialog, $sce, urlImagens,buckets) {

    const queryString = window.location.hash;

    var _id =  queryString.slice(queryString.indexOf("%") + 3);
    var origem;
    var novos_dados;

$scope.$on('$viewContentLoaded', function () {

    buscaDados().then(function (result) {
        $scope.dados = result;

        setTimeout(function (){
            $scope.dados.vencimento = new Date($scope.dados.vencimento.substr(3, 2) + '-' + $scope.dados.vencimento.substr(0, 2) + '-' + $scope.dados.vencimento.substr(6, 4));
            if (($scope.dados.data_pagto !== '') || ($scope.dados.data_cancelamento !== '')) {

                if (localStorage.getItem('tipo_usuario') == 'admin') {
                    $scope.isDisabled = false
                    $scope.isEditable = false
                } else {
                    $scope.isDisabled = true
                    $scope.isEditable = true
                }

            } else {
                $scope.isDisabled = false
                $scope.isEditable = false
            }

            if ($scope.dados.data_pagto !== '') {
                $scope.dados.data_pagto = new Date($scope.dados.data_pagto.substr(3, 2) + '-' + $scope.dados.data_pagto.substr(0, 2) + '-' + $scope.dados.data_pagto.substr(6, 4))
            }

            if ($scope.dados.data_cancelamento !== '') {
                $scope.dados.data_cancelamento = new Date($scope.dados.data_cancelamento.substr(3, 2) + '-' + $scope.dados.data_cancelamento.substr(0, 2) + '-' + $scope.dados.data_cancelamento.substr(6, 4))
            }

            $scope.dados.contabilizado = ( $scope.dados.contabilizado == 'SIM' ? true : false );

            $scope.dados.prestador_servico = ( $scope.dados.prestador_servico == 1 ? true : false );

            $scope.dados.centro_custo = $scope.dados.id_centro_custo

            $scope.dados_contabilizado = {
                id: $scope.dados.id,
                contabilizado: ''
            }

            origem = {
                id: _id,
                id_fornecedor: $scope.dados.id_fornecedor,
                documento: $scope.dados.documento,
                vencimento: $scope.dados.vencimento,
                tipo_recebimento: $scope.dados.tipo_recebimento,
                categoria: $scope.dados.categoria,
                comentario: $scope.dados.comentario,
                competencia: $scope.dados.competencia,
                desc_categoria: $scope.dados.desc_categoria,
                desc_grupo: $scope.dados.desc_grupo,
                data_pagto: $scope.dados.data_pagto,
                data_cancelamento: $scope.dados.data_cancelamento,
                valor: $scope.dados.valor,
                valor_baixa: $scope.dados.valor_baixa,
                acrescimo: $scope.dados.acrescimo,
                desconto: $scope.dados.desconto,
                status: $scope.dados.status,
                valor_semformatacao: $scope.dados.valor_semformatacao,
                classificacao: $scope.dados.classificacao,
                grupo: $scope.dados.grupo,
                pdf: $scope.dados.pdf,
                razao_nome: $scope.dados.razao_nome,
                nome: $scope.dados.nome,
                id_patio: $scope.dados.id_patio,
                desc_tiporecebimento:$scope.dados.desc_tiporecebimento,
                contabilizado: $scope.dados.contabilizado,
                saldo_pagar: $scope.dados.saldo_pagar,
                centro_custo: $scope.dados.id_centro_custo,
                prestador_servico: $scope.dados.prestador_servico,
                cpf_cnpj: $scope.dados.cpf_cnpj

            }
            var idpagar = $scope.dados.id;

            buscaCategorias($scope.dados.grupo).then(function (result) {
                $scope.categorias = result.filter((item) => {
                    return item.ativo == 1
                })
            });

            $http.get(urlServidor.urlServidorChatAdmin + '/pagar/busca-documentospagar', {params: {id_pagar: $scope.dados.id}}).then(function (response) {
                $scope.listaDocumentos = response.data;
            });
            if ($scope.dados.status === "PAGO") {
                $scope.IsEstorno = true
            }else{
                $scope.IsEstorno = false
            }

            buscaParciais($scope.dados.id).then(function (result) {

                if (result.length > 0){
                    $scope.parciais = result;
                    $scope.totalparcial = result[0].total;
                }else{
                    $scope.parciais = 0;
                    $scope.totalparcial = 0;
                }

            });

        },1500);

    });

})




    $scope.urlImagemNf = urlImagens.urlImagemNf;
    $scope.trustSrc = function(src) {
        return  $sce.trustAsResourceUrl(src);
    }

    var bucket = null;

    $http.get(urlServidor.urlServidorChatAdmin + '/autoridades/vercodigo').then(function (response) {

        AWS.config.update({accessKeyId: response.data.ac, secretAccessKey: response.data.sc});
        AWS.config.region = 'us-east-1';
    }).finally( function(){

        bucket = new AWS.S3();
    });


    buscaCentroCusto()

    buscaFornecedores().then(function (result) {
        $scope.fornecedores = result;
    });
    
    buscaPatios().then(function (result) {
        $scope.patios = result.filter((item) => {
            return item.ativo == 1
        })
    });

    buscaGrupos().then(function (result) {
        $scope.grupos = result.filter((item) => {
            return item.ativo == 1
        })
    });

    buscaTipoRecebimento().then(function (result) {
        $scope.tiposRecebimentos = result;
    });
    buscaContas().then(function (result) {
        $scope.contas = result;
    });


    buscaCategorias().then( function(result){
        $scope.categorias = result.filter((item) => {
          return item.ativo == 1
        })
    })
/*
    $scope.SelecionaCategoria = function (idgrupo) {
        buscaCategorias(idgrupo).then(function (result) {
            $scope.categorias = result;
        });
    }
*/

    $scope.fechar = function () {
        window.close();
    }

    $scope.estornar = function () {


        var alterados = {
            valor_baixa: $scope.dados.valor_baixa,
            acrescimo: $scope.dados.acrescimo,
            desconto: $scope.dados.desconto,
            data_pagto: $scope.dados.data_pagto
        }
        $http.post(urlServidor.urlServidorChatAdmin + '/pagar/estorna-pagamento', {id: $scope.dados.id }).then(function (response){
            if(response.data.code){
                myFunctions.showAlert('Não foi possível realizar o estorno!!');
            }else {

                buscaDados().then(function (result) {
                    $scope.dados = result;


                        var json = Object.assign({}, alterados);
                        if(JSON.stringify(json) !== '{}'){



                            $scope.logs = {
                                id: $scope.dados.id,
                                operacao: 'ESTORNO',
                                tipo: 'ESTORNO DE PAGAMENTO',
                                id_usuario: localStorage.getItem('id_usuario'),
                                alteracoes: JSON.stringify(json),
                                anterior: null

                                //data_hora: new Date()

                            };
                            $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro',$scope.logs).then(function (response) {
                                if (response.data.code) {
                                    $scope.erro = response;

                                }
                            });
                        }
                });
                setTimeout(function (){
                    myFunctions.showAlert('Estorno realizado com sucesso !!');
                },1000);
            }
        });
    }

    $scope.exists = function (itemContabilizado) {

        $http.post(urlServidor.urlServidorChatAdmin + '/pagar/altera-contabilizado', {id: $scope.dados.id, contabilizado: itemContabilizado }).then(function (response) {

            if (response.data.code) {
                $scope.error = response.data;
            }

            myFunctions.showAlert('CONTABILIZADO GRAVADO COM SUCESSO!')

//            $scope.dados.contabilizado = ( $scope.dados.contabilizado == 'SIM' ? true : false );

        });

    };

/*
    $scope.statusContabilizado = function (){

        $scope.dados.contabilizado = ( $scope.dados.contabilizado  ? 'SIM' : 'NÃO' );

        $http.post(urlServidor.urlServidorChatAdmin + '/pagar/altera-contabilizado', {id: $scope.dados.id, contabilizado: $scope.dados.contabilizado}).then(function (response) {

            if (response.data.code) {
                $scope.error = response.data;
            }
            $scope.dados.contabilizado = ( $scope.dados.contabilizado == 'SIM' ? true : false );

        });

    }
*/


  // Vamos esconder nossa mensagem de erro e tambem o container
  setTimeout(()=>{

    document.getElementById("Container").style.display = "none"

    document.getElementById("ErrorLength").style.display = "none"
  },100)

  // Caso o usuário Clique No input porém não mude nada, corre o risco do Modal não sumir
  //Para isso chamaremos uma função caso o usuario não mude nada e desfoque o input

  $scope.HideModal = () => {
      // o Set timeout vai nos ajudar para não correr o risco de o modal fechar antes do usuario clicar
      setTimeout(() => {
          document.getElementById("Container").style.display = "none"
      }, 2000);
  }


  $scope.buscarClientes = async () => {


      const length = $scope.dados.razao_nome.length
      //Somente com 3 letras iremos iniciar a pesquisa
      // Tendo 3 letras o container vai aparecere e o erro some
      if (length >= 3) {
          document.getElementById("Container").style.display = "block"
          document.getElementById("ErrorLength").style.display = "none"

          // fazemos a chamda e retornamos os dados
          const response = await $http.put(urlServidor.urlServidorChatAdmin + '/fornecedores/listar', { razao_social: $scope.dados.razao_nome })
          let dados = response.data;
          $scope.fornecedores = dados;
          //se for menor que 3 o modal some e o erro aparece
      } else if (length < 3) {
          document.getElementById("ErrorLength").style.display = "block"
          document.getElementById("Container").style.display = "none"
          // se estiver vazio o container some
      } else if ($scope.dados.razao_nome === '') {
          document.getElementById("Container").style.display = "none"

      }


  }


  $scope.ChangeScope = (fornecedor) => {
      // Vamos substituir o inupt texte, visto que não precisamos mais do container ele receber um none
      if (fornecedor) {
          $scope.DadosGeraisClientes = fornecedor
          $scope.dados.razao_nome = fornecedor.razao_nome

          const cpfCnpj = $scope.fornecedores.find((item) => item.razao_nome === $scope.dados.razao_nome).cpf_cnpj
     
          $scope.dados.cpf_cnpj = cpfCnpj

          document.getElementById("Container").style.display = "none"
      } else {
          $scope.DadosGeraisClientes = false

      }
  }



    $scope.alteraContaBaixada = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/pagar/altera-conta-baixada', {id: $scope.dados.id, id_conta: $scope.dados.id_conta }).then(function (response) {

            if (response.data.code) {
                myFunctions.showAlert('Erro na alteração da conta!')
            }else{
                myFunctions.showAlert('Conta alterada com sucesso!')

                var alt = {
                    id_conta: $scope.dados.id_conta
                }

                var json = Object.assign({}, alt)
                $scope.logs = {
                   id: $scope.dados.id, 
                   operacao: 'ALTERAÇÃO',
                   tipo: 'CONTA PAGAMENTO',
                   id_usuario: localStorage.getItem('id_usuario'),
                   alteracoes: JSON.stringify(json),
                   anterior: null

                }

                $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro',$scope.logs).then(function (response) {
                   if (response.data.code) {
                       $scope.erro = response;
                    } 
                })

            }

        })

    }


    $scope.gravarPagar = function () {

//        $scope.dados.contabilizado = ( $scope.dados.contabilizado  ? 'SIM' : 'NÃO' );
        $scope.dados.prestador_servico = ( $scope.dados.prestador_servico == true ? 1 : 0 );

        novos_dados = $scope.dados;

        function isEquivalent(origem, novos_dados) {
            // nome das propriedades
            var origemProps = Object.getOwnPropertyNames(origem);
            var novos_dadosProps = Object.getOwnPropertyNames(novos_dados);

            //comparando se os dois arrays são iguais
            if (origemProps.length != novos_dadosProps.length) {
                return false;
            }
            var diference = [];

            for (var i = 0; i < origemProps.length; i++) {
                var propName = origemProps[i];

               //comparando se os valores são iguais
                if (origem[propName] != novos_dados[propName]) {

                        diference[propName] = novos_dados[propName];



                    //se forem diferente o array diference recebe o valor
                }

            }

            return diference;

        }

        var alterados = isEquivalent(origem, novos_dados);
        var json = Object.assign({}, alterados);

        if ($scope.DadosGeraisClientes) {

            $scope.dados.id_fornecedor = $scope.DadosGeraisClientes.id;
          } else {
            $scope.dados;
          }

        $http.post(urlServidor.urlServidorChatAdmin + '/pagar/alterar', $scope.dados).then(function (response) {

          //  $scope.dados.contabilizado = ( $scope.dados.contabilizado == 'SIM' ? true : false );
          $scope.dados.prestador_servico = ( $scope.dados.prestador_servico == 1 ? true : false );

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000')
                {
                    myFunctions.showAlert('Conta a Pagar já existe!')
                } else
                {
                    myFunctions.showAlert('Alteração executada com sucesso!');

                    var alterados = isEquivalent(origem, novos_dados);
                    var json = Object.assign({}, alterados);
                    if(JSON.stringify(json) !== '{}'){

                        var origem_json = Object.assign({}, origem);

                        $scope.logs = {
                            id: _id,
                            operacao: 'ALTERAÇÃO',
                            tipo: 'DADOS',
                            id_usuario: localStorage.getItem('id_usuario'),
                            alteracoes: JSON.stringify(json),
                            anterior: JSON.stringify(origem_json)

                            //data_hora: new Date()

                        };
                        $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro',$scope.logs).then(function (response) {
                            if (response.data.code) {
                                $scope.erro = response;

                            }
                        });
                    }
                }

            }
        });

    };


    $scope.apagarDocumento = (dados) => {
        let idimagem = dados.id;
        let confirm = $mdDialog.confirm()
        .title('Documentos!')
        .textContent('Tem certeza que deseja apagar esse documento?')
        .ariaLabel('Lucky day')
        .ok('Apagar')
        .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {

          $http.post(urlServidor.urlServidorChatAdmin + '/pagar/apaga-documento', {id: idimagem}).then(function (response){
              if(response.data.code){
                  myFunctions.showAlert('Não foi possível apagar o documento !!');
              }else {

                  var alt = {
                    idImagem: dados.id,
                    nome: dados.documento

                }


                var json = Object.assign({}, alt);
                $scope.logs = {
                    id: _id,
                    operacao: 'EXCLUSÃO',
                    tipo: 'DOCUMENTO',
                    id_usuario: localStorage.getItem('id_usuario'),
                    alteracoes: '',
                    anterior: JSON.stringify(json)
                };

                $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro', $scope.logs).then(function (response){
                    if(response.data.code){
                        $scope.erro = response;
                    }else{
                        dados = null;
                        alt = null;
                        setTimeout(function(){

                            $http.get(urlServidor.urlServidorChatAdmin + '/pagar/busca-documentospagar', {params: {id_pagar: $scope.dados.id  }}).then(function (response) {
                                $scope.listaDocumentos = response.data;
                              });
                              myFunctions.showAlert('Documento apagado !!');

                          }, 1500);

                    }
                });

              }

          });

        });

      }



    $scope.uploadDocumento = (file) => {

        let nomefoto = '';
        if(file){
            if (file.type !== null && file.type !== undefined) {

                if (file.type.substr(0, 5) == 'image' || file.type == 'application/pdf') {

                    if (file.type == 'application/pdf') {
                        nomefoto = $scope.dados.id + '/' + makeid() + '.' + file.type.substr(12, 3);
                        S3UploadService.Upload(file, buckets.documentos_pagar , nomefoto).then(function (result) {
                            // Mark as success
                            file.Success = true;

                            $http.post(urlServidor.urlServidorChatAdmin + '/pagar/incluir-documento',
                                    {id_pagar: $scope.dados.id, documento: nomefoto}
                            ).then(function (response) {
                                $scope.listaDocumentos.push({documento: nomefoto, id: response.data.insertId, id_pagar: $scope.dados.id});
                            });


                        }, function (error) {
                            // Mark the error
                            $scope.Error = error;
                        }, function (progress) {
                            // Write the progress as a percentage

                            var alt = {

                                imagem: nomefoto

                            }
                            var json = Object.assign({}, alt);
                            $scope.logs = {
                                id: _id,
                                operacao: 'INCLUSÃO',
                                tipo: 'DOCUMENTOS',
                                id_usuario: localStorage.getItem('id_usuario'),
                                alteracoes: JSON.stringify(json),
                                anterior: null

                            };
                            $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro', $scope.logs).then(function (response){
                                if(response.data.code){
                                    $scope.erro = response;
                                }
                            });

                            file.Progress = (progress.loaded / progress.total) * 100
                        });

                    } else {
                        myFunctions.showAlert("Formato inválido!");
                    }


                }

            }

        }

    }

    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }

    function buscaDados() {

        return new Promise(function (resolve, reject) {

            $http.get(urlServidor.urlServidorChatAdmin + '/pagar/busca-id',{params: {id_pagar: _id}}).then(function (response) {

                resolve(response.data[0]);

            });

        })

    }

    function buscaFornecedores() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/fornecedores/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })

    }

    function buscaPatios(idgrupo) {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }

    function buscaTipoRecebimento() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/tipos-recebimento/listar-tipo', {tipo: 'PAGAR'}).then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }

    function buscaCategorias(idgrupo) {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/categorias/listar', {grupo: idgrupo}).then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }

    $scope.apagarParcial = function(id, valor) {


        id_parc = id;


        $http.post(urlServidor.urlServidorChatAdmin + '/pagar/apaga-parcial', {id: id_parc}).then(function (response){
            if(response.data.code){
                myFunctions.showAlert('Não foi possível excluir o pagamento!!');
            }else {
                buscaParciais($scope.dados.id).then(function (result) {



                    if (result.length > 0){
                        $scope.parciais = result;
                        $scope.totalparcial = result[0].total;
                    }else{
                        $scope.parciais = 0;
                        $scope.totalparcial = 0;
                    }



                });
                var alt = {

                    idparc: id_parc,
                    valor: valor

                }
                var json = Object.assign({}, alt);
                $scope.logs = {
                    id: _id,
                    operacao: 'EXCLUSÃO',
                    tipo: 'EXCLUSÃO DE PARCIAL',
                    id_usuario: localStorage.getItem('id_usuario'),
                    alteracoes: JSON.stringify(json),
                    anterior: null

                };
                $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro', $scope.logs).then(function (response){
                    if(response.data.code){
                        $scope.erro = response;
                    }
                });

                setTimeout(function (){
                    myFunctions.showAlert('Pagamento excluido com sucesso !!');
                }, 1000);

            }
        });
    }


    function buscaParciais(idbaixa) {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/pagar/listar-parciais', {id: idbaixa}).then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }


    function buscaContas() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/contas/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }


    function buscaGrupos() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/grupos/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })

    }



    function buscaCentroCusto() {
        return new Promise(function (resolve, reject) {
            $http.get(urlServidor.urlServidorChatAdmin + '/centro-custo/listar').then(function (response) {
                let dados = response.data
                resolve(dados)

                $scope.centroCustos = dados.filter((item) => {
                    return item.ativo == 1
                })
            })
        })
    }

})
