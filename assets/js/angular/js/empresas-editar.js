app.controller('empresasEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep, $filter, $sce, urlImagens, $mdDialog, buckets) {
 
    var urlParams = $location.search();

    $scope.dados = {
        id: urlParams.dados[0].id,
        nome_empresa: urlParams.dados[0].nome_empresa,
        nome_fantasia: urlParams.dados[0].nome_fantasia,
        endereco: urlParams.dados[0].endereco,
        bairro: urlParams.dados[0].bairro,
        cidade: urlParams.dados[0].cidade,
        cep: urlParams.dados[0].cep,
        uf: urlParams.dados[0].uf,
        cnpj: urlParams.dados[0].cnpj,  
        
        repres_nome: urlParams.dados[0].repres_nome,
        repres_cpf: urlParams.dados[0].repres_cpf,
        repres_rg: urlParams.dados[0].repres_rg,
        repres_telefone: urlParams.dados[0].repres_telefone,
        repres_email: urlParams.dados[0].repres_email,
        repres_estado_civil: urlParams.dados[0].repres_estado_civil,
        repres_profissao: urlParams.dados[0].repres_profissao,
        repres_nacionalidade: urlParams.dados[0].repres_nacionalidade,
        repres_cep: urlParams.dados[0].repres_cep,
        repres_endereco: urlParams.dados[0].repres_endereco,
        repres_bairro: urlParams.dados[0].repres_bairro,
        repres_cidade: urlParams.dados[0].repres_cidade,
        repres_uf: urlParams.dados[0].repres_uf
        
    }

   // console.log ("Dados: ", $scope.dados)


    $scope.estados = estados.uf;

    $scope.buscaCep = (cep) => {

        if (cep.length == 9) {

            viaCep.get(cep).then(function (response) {

                $scope.dados.endereco = response.logradouro.toUpperCase();
                $scope.dados.bairro = response.bairro.toUpperCase();
                $scope.dados.cidade = response.localidade.toUpperCase();
                $scope.dados.uf = response.uf.toUpperCase();
            });
        }

    }


    $scope.buscaCepRepres = (repres_cep) => {

        if(repres_cep.length == 9){
    
          viaCep.get(repres_cep).then(function(response){
          
            $scope.dados.repres_endereco = response.logradouro.toUpperCase() ;
            $scope.dados.repres_bairro = response.bairro.toUpperCase() ;
            $scope.dados.repres_cidade = response.localidade.toUpperCase() ;
            $scope.dados.repres_uf = response.uf.toUpperCase() ;				 
    
          });
    
        }	
    
    }



    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/empresas/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    myFunctions.showAlert('Empresa já existe!')
                } else {
                    myFunctions.showAlert('Alteração executada com sucesso!');

                }

            }
            ;
        });

    };


});