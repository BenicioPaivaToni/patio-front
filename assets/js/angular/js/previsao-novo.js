'use strict';
angular.module('app')
.controller("previsaoNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions,$sce,urlImagens) {

    $scope.dados = {
        id_grupo: '',
        id_categoria: '',
        competencia: '',
        valor: '',
        id_patio: '',
        idempresa: localStorage.getItem('id_empresa')
    };

    buscaPatios().then( function(result){
        $scope.patios = result ;
      }); 
    
      buscaGrupos().then( function(result){
        $scope.grupos = result ;
      });  

      $scope.SelecionaCategoria = function(idgrupo){
        buscaCategorias(idgrupo).then( function(result){
          $scope.categorias = result ;
        });  
      }



    $scope.gravar = function (){
        $http.post(urlServidor.urlServidorChatAdmin + '/previsao-orcamentaria/cadastrar', $scope.dados)
				.then(function (response) {
					if (response.data.code) {
						myFunctions.showAlert('Erro na gravação!')
					} else {
						if (response.data == '11000') {
							myFunctions.showAlert('Já existe!')
						} else {
							myFunctions.showAlert('Cadastro executado com sucesso!')
							$scope.dados.id = response.data.insertId
							$scope.isDisabled = true
						}
					}
				})

    };


      function buscaCategorias(idgrupo){
		return new Promise( function( resolve,reject){
		  $http.put(urlServidor.urlServidorChatAdmin+'/categorias/listar',{ grupo: idgrupo}).then( function(response){
			let dados = response.data ;
			resolve(dados) ;
		  });
		})
  }
  
  function buscaPatios(idgrupo){
		return new Promise( function( resolve,reject){
		  $http.put(urlServidor.urlServidorChatAdmin+'/patios/listar').then( function(response){
			let dados = response.data ;
			resolve(dados) ;
		  });
		})
	}	
    function buscaGrupos(){

        return new Promise( function( resolve,reject){
    
          $http.put(urlServidor.urlServidorChatAdmin+'/grupos/listar').then( function(response){
    
            let dados = response.data ;
    
            dados.sort(function(a, b){
              if (a.descricao < b.descricao) {
              return 1;
              }
              if (a.descricao > b.descricao) {
              return -1;
              }        
              return 0;
            });       
    
            resolve(dados) ;
    
          });
    
        })
    
      }


   

});