angular.module('app').controller('advogadosNovoCtrl', function ($scope, $http, urlServidor, myFunctions, estados) {
	
    $scope.dados = {
	    nome: '',
		telefone: '',
        email: '',
        cidade: '',
		uf: '',
		ativo: ''
	}

	$scope.estados = estados.uf

		
	$scope.gravar = function () {
			
		$http.post(urlServidor.urlServidorChatAdmin + '/advogados/cadastrar', $scope.dados).then(function (response) {
			
            if (response.data.code) {
				myFunctions.showAlert('Erro na gravação!')
			} else {
				if (response.data == '11000') {
						myFunctions.showAlert('Advogado já existe!')
				} else {
					myFunctions.showAlert('Cadastro executado com sucesso!')
					$scope.patio.id = response.data.insertId
					$scope.isDisabled = true
				}
			}
		})
			
	}
})
