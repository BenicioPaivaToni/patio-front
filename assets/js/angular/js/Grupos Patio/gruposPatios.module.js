
angular.module("app").controller('gruposPatiosCtrl', function ($scope, $location, $http, urlServidor, myFunctions) {



    $scope.dados = {
        descricao: "",
        ativo: ""
    }



    $scope.gruposPatio = {

        //Methods .
        Novo: () => $location.path('grupos-patios-novo'),

        Limpar: () => delete $scope.dados,



        Pesquisar: async () => {
            const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/grupos-patios/listar`,{ params: $scope.dados })
            console.log(response.data);
            return $scope.response = response.data

        }



    }



    $scope.Detalhe = (ev,id) => {
        var dados = $scope.response.filter((obj) => obj.id == id)
        $location.path('grupos-patios-editar').search({ dados })
    }

});