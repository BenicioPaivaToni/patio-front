
angular.module("app").controller('gruposPatios-EditarCtrl', function ($scope, $location, $http, urlServidor, myFunctions) {

    const { dados } = $location.search();


    $scope.dados = dados[0]

    $scope.gruposPatio = {

        //Methods .

        Limpar: () => delete $scope.dados,



        Salvar: async () => {

            const response = await $http.post(`${urlServidor.urlServidorChatAdmin}/grupos-patios/alterar`, $scope.dados)

            if (response.status === 200) { myFunctions.showAlert('Grupo Alterado com sucesso') }

        },

        Fechar: () => window.history.back(),


    }

    const buscaPatios = async () => {
        const response = await $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar');
        return $scope.patios = response.data
    }

    const buscaPatiosGrupo = async () => {
        const response = await $http.get(urlServidor.urlServidorChatAdmin + '/grupos-patios/listar-patios', { params: { id: $scope.dados.id } })

        $scope.patiosGrupo = response.data

    }




    $scope.incluiPatio = async (id) => {

        const Ids = {
            idPatio: id,
            idGrupo: $scope.dados.id,
        };

        await $http.post(urlServidor.urlServidorChatAdmin + '/grupos-patios/grava-patio', Ids)
        buscaPatiosGrupo()

    };

    $scope.removePatio = async (id) => {
        await $http.post(urlServidor.urlServidorChatAdmin + '/grupos-patios/remove-patio', { id: id })
        buscaPatiosGrupo()
    };








    setTimeout(() => {
        buscaPatios()
        buscaPatiosGrupo()


    }, 100);

})


