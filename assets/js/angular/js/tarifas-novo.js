'use strict';
angular.module('app')
.controller("tarifasNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions) {  

  $scope.dados = {
     descricao:'',
     tipoveiculo:'',
     valorEstadia: 0 ,
     valorGuinchoDia: 0 ,
     valorGuinchoNoite: 0 ,
     limiteKm: '' ,
     valorKm: 0 ,
     maxEstadias: '',      
     idEmpresa: localStorage.getItem('id_empresa'),
     idTipoVeiculo: 0,
     idTarifa:0,
     patio:0
  }

  $http.get(urlServidor.urlServidorChatAdmin+'/tipos_veiculo/listar', { params: $scope.dados } ).then( function(response){
    $scope.tiposVeiculos = response.data ;
  });

  buscaPatios().then( function(result){
    $scope.patios = result ;
  }); 

  $scope.isDisabled = false

  $scope.gravarItem = function(dados) {

    $http.get(urlServidor.urlServidorChatAdmin+'/tarifas/busca-tarifa', { params: $scope.dados } ).then( function(response){

      if( response.data == 0 ){
        $http.post(urlServidor.urlServidorChatAdmin+'/tarifas/cadastrar-tarifa', { params: $scope.dados } ).then( function(response){
          if(response.data.insertId > 0){

            $scope.dados.idTarifa = response.data.insertId ;

            $http.post(urlServidor.urlServidorChatAdmin+'/tarifas/cadastrar-tarifadetalhes', $scope.dados ).then( function(response){

              if(response.data.insertId > 0){

                  $http.get(urlServidor.urlServidorChatAdmin+'/tarifas/listar', { params: { descricao: $scope.dados.descricao , idEmpresa: $scope.dados.idEmpresa } } ).then( function(response){
                    $scope.dadosTarifas = response.data ;
                  }); 
                  myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
                  
              }               

            })  
          } 
        });  
      }else{  

        myFunctions.showAlert( 'Esta tarifa já esta cadastrada, verifique na opção EDITAR!' ); 

      }  

    });
  
  }
  
  
  $scope.gravar = function() {        

    $http.post(urlServidor.urlServidorChatAdmin+'/tarifas/cadastrar', $scope.dados ).then( function(response){

        if (response.data.code){
            
          myFunctions.showAlert('Erro na gravação!')

        }else{

          if ( response.data == '11000' )                 
          {
            myFunctions.showAlert('Tarifa já existe!')
          }else
          {                    

            myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
            $scope.isDisabled = true ;            

          }  
                
        }; 

    }); 

  };
  
  function buscaPatios(){
		return new Promise( function( resolve,reject){
		  $http.put(urlServidor.urlServidorChatAdmin+'/patios/listar').then( function(response){
			let dados = response.data ;
			resolve(dados) ;
		  });
		})
	}	
  
    
});