
angular.module('app').controller('juridicoEditarCtrl', function ($scope, $location, $http, urlServidor, myFunctions, $mdDialog,  $sce, Upload, S3UploadService, ncvFac, urlImagens, buckets) {

    /*Global Vars */
    const queryString = window.location.hash;
    var _id = queryString.slice(queryString.indexOf("%") + 3);

    $scope.user = localStorage.getItem('tipo_usuario');

    $scope.dados = null;

    $scope.dataList = []

    $scope.documentos = { 
        novotipo: '' , 
        novadescricao: ''
    }  


    $scope.tipo = {
        tipo: ''
    }


    $scope.historico = {
        tipo: '',
        descricao: ''
    }

    const buscaAdvogados = async () => {
        
       const response = await $http.put(urlServidor.urlServidorChatAdmin + '/advogados/listar')

        let dados = response.data
        
        $scope.advogados = dados.filter((item) => {
            return item.status !== 0
        })
 
    }


    

    const buscaDados = async () => {
                 
        const response = await $http.put(`${urlServidor.urlServidorChatAdmin}/advogados/listar-dados-ncv`,
            { ncv: _id, idusuario: localStorage.getItem('id_usuario') })
                
        const { data } = response
                
        $scope.dados = data[0]
                
    }

 

    $scope.urlDocumentos = urlImagens.urlImagensDocumentos;

        
    if (localStorage.getItem('nome_usuario') == 'admin') {
        $scope.isDisabled = false
    } else {
        $scope.isDisabled = true
    }
     

    $scope.historico = {
        ncv: _id,
        tipo: null,
        descricao: null,
        usuario: localStorage.getItem('nome_usuario')
    };


    $scope.trustSrc = function(src) {
        return  $sce.trustAsResourceUrl(src);
    }


    const buscaDocumentos = async () => {
        
        const response = await $http.get(urlServidor.urlServidorChatAdmin + '/advogados/busca-documentos', {params: {ncv: _id}})
        
        $scope.listaDocumentosPdf = response.data;
        
    }

   
    setTimeout(() => {
      
        buscaAdvogados()
        buscaDados()
        buscaHistorico()
        buscaDocumentos()
    
    }, 200);

 

    $scope.limparHistorico = function () {
        $scope.historico.tipo = ''
        $scope.historico.descricao = ''
    }


    const buscaHistorico = async () => {
        const response = await $http.get(urlServidor.urlServidorChatAdmin + '/advogados/busca-historico', {params: {ncv: _id}})  
        $scope.historicoList = response.data  
    }


    $scope.uploadDocumento = (file) => {

        let nomefoto = '';

        if (file !== null) {

            if (file.type !== null && file.type !== undefined) {

                if (file.type.substr(0, 5) == 'image' || file.type == 'application/pdf') {

                    nomefoto = _id + '/' + makeid() + '.' + file.type.substr(12, 3);
                
                    S3UploadService.Upload(file, buckets.documentos_juridico, nomefoto).then(function (result) {
                     
                        file.Success = true;

                        $http.post(urlServidor.urlServidorChatAdmin + '/advogados/incluir-documentos',
                            {
                                ncv: _id, 
                                tipo: $scope.documentos.novotipo, 
                                descricao: $scope.documentos.novadescricao,
                                documento: nomefoto,
                                id_usuario: localStorage.getItem('id_usuario')
                            }
                        ).then(function (response) {
                                
                            $scope.listaDocumentosPdf.push({ tipo: $scope.documentos.novotipo, documento: nomefoto, id: response.data.insertId });

                        })
                        
                    }, function (error) {
                        //Mark the error
                        $scope.Error = error;
                    }, function (progress) {
                        // Write the progress as a percentage
                        var alt = {
                            tipo: $scope.documentos.novotipo,
                           descricao: $scope.documentos.novadescricao,
                            imagem: nomefoto
                       }
                        var json = Object.assign({}, alt);
                        $scope.logs = {
                            ncv: _id,
                            operacao: 'INCLUSÃO',
                            tipo: 'DOCUMENTOS',
                            id_usuario: localStorage.getItem('id_usuario'),
                            alteracoes: JSON.stringify(json),
                            anterior: null

                        };
                        $http.post(urlServidor.urlServidorChatAdmin + '/ncv/logs-ncv', $scope.logs).then(function (response) {
                            if (response.data.code) {
                                $scope.erro = response;
                            }
                        });
                        file.Progress = (progress.loaded / progress.total) * 100
                   });
                  
                }

            }

        }

    }




    $scope.apagarDocumento = (dados) => {
        idimagem = dados.id;
        let confirm = $mdDialog.confirm()
        .title('Atenção !!!')
        .textContent('Tem certeza que deseja apagar o documento?')
        .ariaLabel('Lucky day')
        .ok('Apagar')
        .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {

            $http.post(urlServidor.urlServidorChatAdmin + '/advogados/apagar-documento', {id: idimagem}).then(function (response){
                if(response.data.code){
                    myFunctions.showAlert('Não foi possível apagar o documento!');
                    
                }else{
                    myFunctions.showAlert('Documento apagado!');
                }
            });
            var alt = {
                idImagem: dados.id,
                imagem: dados.imagem,
                tipo: dados.tipo,
                descricao: dados.descricao
            }
                
            var json = Object.assign({}, alt);
            
            $scope.logs = {
                ncv: _id, 
                operacao: 'EXCLUSÃO',
                tipo: 'DOCUMENTOS',
                id_usuario: localStorage.getItem('id_usuario'),
                alteracoes: '',
                anterior: JSON.stringify(json)
            };

            $http.post(urlServidor.urlServidorChatAdmin + '/ncv/logs-ncv', $scope.logs).then(function (response){
                if(response.data.code){
                    $scope.erro = response;
                }else{
                   
                    $http.get(urlServidor.urlServidorChatAdmin + '/advogados/busca-documentos', {params: {ncv: _id}}).then(function (response) {
                        $scope.listaDocumentosPdf = response.data;
                    });
                }
            });
            
            dados = null;
        });

    }
    

    $scope.fechar = function () {
        window.close()
    }



        
    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }



    $scope.$watch("dados.id", function (nv, ov) {
        if (!ncvFac.controlePatioFac.isEmpty(nv)) {
         
        }
    })



    $scope.gravarHistorico = function() {

        if (!$scope.historico.tipo || !$scope.historico.descricao) {

            myFunctions.showAlert( 'Tipo e Descrição precisam estar preenchidos! Verifique!' )

            return

        }else{
   
            $http.post(urlServidor.urlServidorChatAdmin + '/advogados/cadastrar-historico',
                { 
                    ncv: $scope.dados.id,
                    tipo: $scope.historico.tipo,
                    descricao: $scope.historico.descricao,
                    id_advogado: $scope.dados.id_advogado, 
                    id_usuario: localStorage.getItem('id_usuario'),
                                    
                }).then(function (response) {
                        
                if (response.data.code) {
                    myFunctions.showAlert('Erro na gravação!');
                }else {
                    
                    buscaHistorico()

                    myFunctions.showAlert('Gravação efetuada com sucesso!');

                }
            })

        }
     
    }



    $scope.consultarDetran = function(){
         
      window.open(`/#/Consulta-Detran?id=${$scope.dados.id}`);

    }


    

});