
angular.module('app').controller('TransferenciasCtrl', function (myFunctions,$location, $http, $scope, urlServidor, $filter, $mdDialog) {

    $scope.dados = {
        DataInicio: new Date(),
        DataFinal: new Date()
    }
    $scope.mesAtual = function (ev, id) {
        let hoje = new Date()
        $scope.dados.DataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
        $scope.dados.DataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    };


    $scope.novo = function (ev, id) {
        $location.path('Transferencias-novo');
    };
    $scope.limpar = function (ev) {
        $scope.dados.DataInicio = '';
        $scope.dados.DataFinal = '';
    };


    $scope.pesquisar = async function (ev) {
        try {
            const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/transferencias/listar`, { params: $scope.dados })


            const { data } = response

             return $scope.dadosList = data.map(Item => ({
                data: $filter('date')(Item.data, 'dd/MM/yyyy'),
                id: Item.id,
                conta_destino: Item.conta_destino,
                conta_origem: Item.conta_origem,
                patio_destino: Item.patio_destino,
                patio_origem: Item.patio_origem,
                valor: Item.valor.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})

            }))


              

        } catch (error) {
            console.log(error);
        }

    };


    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        window.open('/#/Transferencias-editar?id ' + id, '_blank');
    };


    $scope.gerarPlanilha = () => {

        alasql('SELECT * INTO XLSX("relatorio-contas-a-receber.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    };


    $scope.gerarPdf = function () {
        console.log(  $scope.dadosList
        );



        var doc = new jsPDF({ orientation: "landscape" });

        var totalPagesExp = '{total_pages_count_string}'


        doc.setFontSize(10);


        doc.autoTable({

            columnStyles: {
                vencimento: { halign: 'left' },
            },

            body: $scope.dadosList
            ,
            columns: [
                { header: 'Pátio Origem', dataKey: 'patio_origem' },

                { header: 'Pátio Destino', dataKey: 'patio_destino' },



                { header: 'Conta Origem', dataKey: 'conta_origem' },

                { header: 'Conta Destino', dataKey: 'conta_destino' },



                { header: 'data', dataKey: 'data' },


                { header: 'valor', dataKey: 'valor' },


            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },

            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')


                /* Guardando Em constantes para ajudar na oraganização*/
                const dataInicioStr = $scope.dados.DataInicio.toLocaleDateString('pt-BR')
                const dataFinalStr = $scope.dados.DataFinal.toLocaleDateString('pt-BR')

                /* Por usar interpolação de Strings, precisamos criar uma variavel de espaço*/
                const WillGiveSpace = '            '



                doc.text(`Transferencias efetuadas no periodo de  ${dataInicioStr} a ${dataFinalStr}`, data.settings.margin.left + 15, 22);




                // Footer
                var str = `Pagina ${doc.internal.getNumberOfPages()}`

                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') { str = `${str} de ${totalPagesExp}` }


                doc.setFontSize(10)





                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()




            },

            margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('relatorio-Transferencias.pdf');
    }







    $scope.cancelar = function (rowId) {
        let confirm = $mdDialog.confirm()
            .title('Contrato')
            .textContent('Tem certeza que deseja apagar essa transferencia?')
            .ariaLabel('Lucky day')
            .ok('Apagar')
            .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {
            try {
                $http.post(`${urlServidor.urlServidorChatAdmin}/transferencias/apaga-transferencia`, { id: rowId })
                myFunctions.showAlert('Transferencia Deletada com sucesso');
                document.location.reload(true)

            } catch (error) {
                console.log(error);
            }
        })
    }





})

