angular.module("app").controller("agendamentoVeiculosCtrl",function ($scope, $http, myFunctions, urlServidor, $mdDialog) {   
      
  $scope.dados = {
    id_patio: "",
    ncv_placa: "",
    data_transferencia: new Date(),
  };

  $scope.alterardados = {
    id_patio: "",
    ncv_placa: "",
    data_transferencia: new Date(),
    id_motorista: ""
  };

  $scope.dadosAgendados = {
    data_inicial: new Date(),
    data_final: new Date(),
    cliente:"",
    searchText : "",
    selectedItem : "",
    sem_data:"",
    pendentes:"",
    entregues:"",

  };

  $scope.querySearch = async (query) => {
    if (!query) return $scope.dadosAgendados.cliente;
    return await getAutocompleteClientes(query);
  };


  const getAutocompleteClientes = async (query) => {
    return await $http
      .get(
        `${urlServidor.urlServidorChatAdmin}/clientes/autocomplete/${query}`
      )
      .then(({ data }) => data);
  };



  $scope.DadosEntrega = {
    id_cliente: null,
    pendentes: null,
    entregues: null
  }

  setTimeout(() => {
    document.getElementById("Container").style.display = "none";
    document.getElementById("Container_cliente_entrega").style.display = "none"

    document.getElementById("ErrorLength").style.display = "none";
    document.getElementById("ErrorLength_1").style.display = "none"

  }, 100);

  $scope.HideModal = () => {
    // o Set timeout vai nos ajudar para não correr o risco de o modal fechar antes do usuario clicar
    setTimeout(() => {
      document.getElementById("Container").style.display = "none";
    }, 2000);
  };

  $scope.hideModalClienteEntrega = () => {
    setTimeout(() => {
      document.getElementById("Container_cliente_entrega").style.display = "none"
    }, 2000);
  }

  $scope.ChangeScope = (clientes) => {
    $scope.DadosGeraisClientes = clientes;
    $scope.DadosPeriodo.id_cliente = clientes.nome;
    if ($scope.DadosPeriodo.id_cliente !== null) {
      $scope.DadosPeriodo.telefone_cliente = clientes.telefone
    } else {
      $scope.DadosPeriodo.telefone_cliente = ''
    }

    document.getElementById("Container").style.display = "none";
    document.getElementById("Container_cliente_entrega").style.display = "none"
  };


  $scope.changeScopeClienteEntrega = (cliente) => {
    if (cliente) {
      $scope.filtroClienteBusca = cliente.id

      $scope.DadosEntrega.id_cliente = cliente.nome
      document.getElementById("Container_cliente_entrega").style.display = "none"
    } else {
      //  $scope.DadosGeraisClientes = false

    }
  }


    
  $scope.checkboxClienteRetira = function () {
    if (!$scope.DadosEntrega.delivery) {
      $scope.DadosEntrega.id_reboque = null;
      $scope.DadosEntrega.id_motorista = null;
    }
  };
   

  $scope.Disable = true;
  $scope.ncv = 0;

  $scope.id = 0;
  $scope.isCanceled = false;
  $scope.isRecebido = false;
  $scope.usuarioCancelamento = "";
  $scope.dataCancelamento = "";
  $scope.motivoCancelamento = "";

  $scope.usuarioRecebimento = "";
  // $scope.dataRecebimento = "";

  $scope.Dados = [];

  $http.put(urlServidor.urlServidorChatAdmin + "/motoristas/listar", {
      idUsuario: localStorage.getItem("id_usuario"),
      ativo: "1",
    })
    .then(function (response) {
      $scope.motoristas = response.data;
  });

  $http
    .put(urlServidor.urlServidorChatAdmin + "/reboques/listar-ativos")
    .then(function (response) {
      $scope.reboques = response.data;
  });

  $scope.pesquisar = {
    cancelarAgendamento: async () => {
      $mdDialog
        .show({
          controller: function ($scope, $mdDialog, $mdToast) {
            $scope.cancelar_agendamento = function () {
              if ($scope.dados.motivo_cancelamento !== undefined) {
                $http
                  .post(
                    urlServidor.urlServidorChatAdmin +
                    "/leiloes/cancela-agendamento",
                    {
                      id: $scope.dados.numero_protocolo,
                      id_usuario_cancelamento: localStorage.getItem("id_usuario"),
                      motivo_cancelamento: $scope.dados.motivo_cancelamento,
                    }
                  )
                  .then(function (response) {
                    if (response.data.code) {
                      myFunctions.showAlert("Erro na gravação!");
                    } else {
                      myFunctions.showAlert(
                        "Agendameno cancelado com sucesso!"
                      );
                      $scope.dados.motivo_cancelamento = ''
                      $scope.pesquisar.buscaDadosProtocolo();
                    }
                  });

                $scope.fechar();
              } else {
                myFunctions.showAlert("Descreva o motivo do cancelamento!");
              }
            };

            $scope.fechar = function () {
              $mdDialog.cancel();
            };
          },
          templateUrl: "cancelar-agendamento.html",
          scope: $scope,
          preserveScope: true,
          scopoAnterior: $scope,
        })
        .then(
          function (answer) {
            let a = answer;
          },
          function () {
            $scope.statusdialog = "You cancelled the dialog.";
          }
        );
    },

    buscaDadosProtocolo: async () => {
      $scope.nomeMotorista = "";
      $scope.nomeReboque = "";
      $scope.nomeCliente = "";
      $scope.dataEntrega = "";
      $scope.dataCadastro = "";
      $scope.usuarioCadastro = "";

      $scope.nomeUsuario = "";
      $scope.dataUsuario = "";

      $scope.usuarioCancelamento = "";
      $scope.dataCancelamento = "";
      $scope.motivoCancelamento = "";

      $scope.usuarioEntrega = "";
      // $scope.dataRecebimento = "";

      $scope.dadosList = [];

      let numeroProtocolo = $scope.dados.numero_protocolo;
      //let id_cliente = $scope.DadosEntrega.id_cliente
      if (!numeroProtocolo && !$scope.DadosEntrega.id_cliente) {
        myFunctions.showAlert("Informar número do Protocolo ou Nome do Cliente");
      } else {

     

        let DadosTransferencias = null;

        let protocolo = $scope.dados.numero_protocolo

        if (protocolo) {
          $scope.filtroClienteBusca = null
        }

        DadosTransferencias = await $http.get(
          `${urlServidor.urlServidorChatAdmin}/leiloes/listar-agendamento-veiculos`,
          {
            params: {
              numeroProtocolo: $scope.dados.numero_protocolo,
              pendentes: $scope.DadosEntrega.pendentes,
              entregues: $scope.DadosEntrega.entregues,
              id_cliente: $scope.filtroClienteBusca

            },
          }
        );

        if (DadosTransferencias.data.length == 0) {
          myFunctions.showAlert("Pesquisa não encontrou registros! Verifique!");

          return
        }

        let protocolos = DadosTransferencias.data

        let result = Object.values(
          protocolos.reduce((acc, obj) => {
            acc[obj.id_protocolo] = obj;
            return acc;
          }, {})
        );

        if (result.length >= 2) {

          $scope.listaProtocolos = result

          $mdDialog.show({
            controller: function ($scope, $mdDialog, $mdToast, myFunctions) {
              $scope.cancel = function () {

                $mdDialog.cancel();
                $scope.isDisabled = false;

                const dados = $scope.listaProtocolos.filter((obj) => {
                  return obj.selecionado
                })

                if (dados.length === 0) {

                  myFunctions.showAlert("Nenhum protocolo foi marcado! Verifique!");
                  return
                }

                if (dados.length > 1) {

                  myFunctions.showAlert("Marque somente um protocolo! Verifique!");
                  return
                }

                $scope.numeroProtocolo = dados[0].id_protocolo
                $scope.dados.numero_protocolo = dados[0].id_protocolo
                $scope.filtroClienteBusca = null
                $scope.pesquisar.buscaDadosProtocolo()

              };
            },
            templateUrl: "selecao-protocolo.html",
            scope: $scope,
            preserveScope: true,
            scopoAnterior: $scope,
          })
            .then(
              function (answer) {
                let a = answer;
              },
              function () {
                $scope.statusdialog = "You cancelled the dialog.";
              }
            );

        } else {
          $scope.dados.numero_protocolo = DadosTransferencias.data[0].id_protocolo
          $scope.DadosEntrega.id_cliente = DadosTransferencias.data[0].nome_cliente

          $scope.nomeMotorista = DadosTransferencias.data[0].motorista;
          $scope.nomeReboque = DadosTransferencias.data[0].reboque;
          $scope.isNotNull = DadosTransferencias.data[0].motorista !== null;
          $scope.nomeCliente = DadosTransferencias.data[0].nome_cliente;
          $scope.dataEntrega = DadosTransferencias.data[0].data_entrega;
          $scope.dataCadastro = DadosTransferencias.data[0].data_cadastro;
          $scope.usuarioCadastro = DadosTransferencias.data[0].usuario_cadastro;
          $scope.nomeUsuario = DadosTransferencias.data[0].nome_usuario;
          $scope.dataUsuario = DadosTransferencias.data[0].data_usuario;
          $scope.isCanceled = DadosTransferencias.data[0].id_usuario_cancelamento !== null;
          $scope.usuarioCancelamento = DadosTransferencias.data[0].usuario_cancelamento;
          $scope.dataCancelamento = DadosTransferencias.data[0].data_cancelamento;
          $scope.motivoCancelamento = DadosTransferencias.data[0].motivo_cancelamento;
          $scope.usuarioEntrega = DadosTransferencias.data[0].usuario_entrega;
          //  $scope.dataRecebimento = DadosTransferencias.data[0].data_recebimento;
          $scope.isRecebido = DadosTransferencias.data[0].usuario_entrega !== null;
          $scope.dadosList = DadosTransferencias.data;


        }

      }
    },

    BuscaNcvPlaca: async () => {
      if (
        !$scope.DadosPeriodo.id_cliente ||
        !$scope.DadosPeriodo.classificacao ||
        !$scope.alterardados.ncv_placa
      ) {
        myFunctions.showAlert(
          `Confira se todos as informações estão preenchidas antes de inserir veiculos!`
        );
      } else {

        const response = await $http.get(
          `${urlServidor.urlServidorChatAdmin}/leiloes/agendamento-busca-ncv-placa`,
          {
            params: {
              ncv_placa: $scope.alterardados.ncv_placa
            },
          }
        );

        if (response.data.length > 1) {

          myFunctions.showAlertCustom('Veiculo com placa ' + $scope.alterardados.ncv_placa + ' possui mais de um NCV associado!<br/><br/>'
            + 'Verifique cadastro ou digite o NCV específico referente à movimentação deste veiculo!')

          return
        }  

        const data = response.data[0];

        if (data == undefined) {
          
          myFunctions.showAlertCustom("Veiculo não encontrado! <br/><br/>" +
            "Verifique se foi digitado corretamente e se o mesmo está com status APREENDIDO!")

        } else {
          $scope.ncv = response.data[0].ncv;

          const txt = `
                        <div>
                        <header>
                    
                          <title>
                            
                          </title>
                    
                        </header>
                        </br>
                    
                        <body>

                        <div> 
                          <div>STATUS :</div>
                          <div><strong>${data.status}</strong></div>
                        </div>
                        </br>
                        
                        <div> 
                          <div>NCV :</div>
                          <div><strong>${data.ncv}</strong></div>
                        </div>
                        </br>

                        <div> 
                          <div>PLACA :</div>
                          <div><strong>${data.placa}</strong></div>
                        </div>
                        </br>

                        <div> 
                        <div>MARCA/MODELO :</div>
                        <div><strong>${data.marca_modelo || 0}</strong></div>
                        </div>

                        </br>

                        <div> 
                          <div>ANO :</div>
                          <div><strong>${data.ano || 0}</strong></div>
                        </div>
                        </br>
                    

                        <div> 
                          <div>NÚMERO DO MOTOR :</div>
                          <div><strong>${data.numero_motor || 0}</strong></div>
                        </div>
                        </br>
                        
                        <div> 
                          <div>CHASSI :</div>
                          <div><strong>${data.chassi || 0}</strong></div>
                        </div>
                        </br>
                        </body>
                        
                        </div>
                    
                        `;

          let confirm = $mdDialog
            .confirm()
            .ariaLabel("Lucky day")
            .title("Confira os dados do veículo")
            .ok("ADICIONAR VEICULO")
            .cancel("Cancelar")
            .htmlContent(txt);
          $mdDialog.show(confirm).then(() => {

            response.data[0].classificacao = $scope.DadosPeriodo.classificacao

            $scope.Dados.push(response.data[0]);

            $scope.pesquisar.limparAgendados()

          });
        }
      }
    },

    limparAgendados: (ev) => {
      $scope.DadosPeriodo.classificacao = ""
      $scope.alterardados.ncv_placa = "";
    },

    limparAgendamentos: (ev) => {
      ($scope.dadosAgendados.data_inicial = new Date()),
        ($scope.dadosAgendados.data_final = new Date()),
        ($scope.dadosAgendados.cliente = ""),

        ($scope.agendamentosList = [])
    },
  };

  $scope.limparPesquisaAgendamentos = function () {

    // $scope.nomeMotorista = "";
    $scope.nomeReboque = "";
    $scope.nomeCliente = "";
    $scope.dataEntrega = "";
    $scope.dataCadastro = "";
    $scope.usuarioCadastro = "";

    $scope.DadosEntrega.id_cliente = null
    $scope.DadosEntrega.entregues = null
    $scope.DadosEntrega.pendentes = null
    $scope.dados.numero_protocolo = null
    $scope.DadosEntrega.cliente_retira = null
    $scope.DadosEntrega.delivery = null


    $scope.nomeUsuario = "";
    $scope.dataUsuario = "";

    $scope.usuarioCancelamento = "";
    $scope.dataCancelamento = "";
    $scope.motivoCancelamento = "";

    $scope.usuarioEntrega = "";
    // $scope.dataRecebimento = "";

    $scope.dadosList = [];


  }

  $scope.DadosPeriodo = {
    data: null,
    id_cliente: null,
    hora: null,
    classificacao: null,
    telefone_cliente: null
  };

  $scope.SeparacaoPeriodo = {
    hoje: new Date(),

    //Limpa os campos tela
    limpar: function (ev) {
      $scope.alterardados.ncv_placa = "";
      $scope.DadosPeriodo.classificacao = "";
    },

    LimparResultado: () => {
      $scope.Dados = [];
    },

    //Botão de selecionar todos ou desmarcar todos
    Selecionar: "SELECIONAR TODOS",

    SelecionarTodos: function (Dados) {
      if (this.Selecionar === "SELECIONAR TODOS") {
        Dados.forEach((dados) => {
          dados.selecionado = true;
        });
        this.Selecionar = "DESMARCAR TODOS";
      } else {
        Dados.forEach((dados) => {
          dados.selecionado = false;
        });
        this.Selecionar = "SELECIONAR TODOS";
      }
    },

    confirmarEntrega: async (dadosList) => {
      if (
        !$scope.DadosEntrega.cliente_retira &&
        !$scope.DadosEntrega.delivery
      ) {
        myFunctions.showAlert(
          "Informar se o cliente retira ou os dados do reboque e motorista!"
        );

        return;
      }

      const selected = dadosList.filter((item) => {
        return item.selecionado == true;
      });

      const unSelected = dadosList.filter((item) => {
        return !item.selecionado;
      });

      if (selected.length > 0) {
        $http
          .post(
            urlServidor.urlServidorChatAdmin +
              "/leiloes/grava-confirma-entrega",
            {
              id: $scope.dados.numero_protocolo,
              id_usuario_entrega: localStorage.getItem("id_usuario"),
              id_veiculo: $scope.DadosEntrega.id_reboque,
              id_motorista: $scope.DadosEntrega.id_motorista,
            }
          )
          .then(function (response) {
            if (response.data.code) {
              myFunctions.showAlert("Erro na gravação!");
            }
          });

        if (unSelected.length > 0) {
          let removerAgendado = unSelected.map((x) => {
            return {
              ncv: x.id,
              id_protocolo: $scope.dados.numero_protocolo,
            };
          });

          $http
            .post(
              urlServidor.urlServidorChatAdmin + "/leiloes/entrega-parcial",
              removerAgendado
            )
            .then(function (response) {
              if (response.data.code) {
                myFunctions.showAlert("Erro na gravação!");
              }
            });
        }

        $scope.DadosEntrega.pendentes = false;
        $scope.DadosEntrega.entregues = true;

        myFunctions.showAlert(`Confirmação de Entrega gravada com sucesso!`);

        $scope.pesquisar.buscaDadosProtocolo();
      } else {
        return myFunctions.showAlert(
          `Nenhum veiculo na lista de entrega ou selecionado! Verifique!`
        );
      }
    },

    //Botão de gravar dados selecionados
    Inserir: async (Dados) => {
      const result = Dados.filter((Veiculo) => {
        return Veiculo.selecionado;
      });

      if (result.length > 0) {
        $scope.dados.id_cliente = $scope.DadosGeraisClientes.id;
        let idProtocolo = 0;

        let dados = {
          usuario_cadastro: localStorage.getItem("id_usuario"),
          id_cliente: $scope.DadosGeraisClientes.id,
          ...($scope.DadosPeriodo.data
            ? { data_entrega: $scope.DadosPeriodo.data }
            : { data_entrega: null }),

          ...($scope.DadosPeriodo.hora
            ? {
                hora_entrega: $scope.DadosPeriodo.hora.toLocaleTimeString(
                  "pt-BR",
                  {
                    hour: "2-digit",
                    minute: "2-digit",
                  }
                ),
              }
            : { hora_entrega: null }),
        };

        $http
          .post(
            urlServidor.urlServidorChatAdmin +
              "/leiloes/grava-protocolo-agendamento",
            dados
          )
          .then(function (response) {
            idProtocolo = response.data.insertId;
            $scope.idProtocoloTransferencia = idProtocolo;
          })
          .finally(async function () {
            var transferencia = result.map((x) => {
              return {
                ncv: x.id,
                classificacao: x.classificacao,
                id_protocolo: idProtocolo,
              };
            });

            let responseRomaneio = await $http.post(
              `${urlServidor.urlServidorChatAdmin}/leiloes/grava-veiculos-agendamento`,
              transferencia
            );

            let confirm = $mdDialog
              .confirm()
              .title("Agendamento de Entrega")
              .textContent("Número Protocolo de Agendamento: " + idProtocolo)
              .ariaLabel("Lucky day")
              .ok("Ok");
            $mdDialog.show(confirm).then(function () {
              $scope.gerarPDFAgendamento();

              $scope.DadosPeriodo = "";

              $scope.Dados = [];
            });
          });
      } else {
        return myFunctions.showAlert(
          `Nenhum veículo foi selecionado, Verifique!`
        );
      }
    },
  };

  $scope.converterHora = (hora) =>  {
    if(!hora){
      return null
    }
    // Crie uma data de referência em 1970-01-01
    var dataReferencia = new Date(1970, 0, 1);
  
    // Divida a hora fornecida em horas e minutos
    var horaMinutos = hora.split(":");
    var horas = parseInt(horaMinutos[0], 10);
    var minutos = parseInt(horaMinutos[1], 10);
  
    // Configure a hora na data de referência
    dataReferencia.setHours(horas);
    dataReferencia.setMinutes(minutos);
  
    // Define o fuso horário para GMT-0300 (Horário Padrão de Brasília)
    dataReferencia.setTime(dataReferencia.getTime() + (3 * 60 * 60 * 1000));
  
    // Converte a data em uma string no formato desejado
    var resultado = dataReferencia.toString();
  
    return new Date(resultado);
  }
  

  $scope.formatData = (data) => {
    if (data) {
      // Converter a data para o formato desejado (dia/mês/ano)
      const partes = data.split("/");
      if (partes.length === 3) {
        data = partes[1] + "/" + partes[0] + "/" + partes[2];
      }
    }
    return data
  };


$scope.atualizaAgendamento = async (data) => {
  try {
    if (data.status !== "PENDENTE") {
      myFunctions.showAlert(
        `Status do veiculo ${data.status}, não permite edição`
      );
      return;
    }
    const payload = {
      ...data,
      ...(data.hora_entrega
        ? {
            hora_entrega: data.hora_entrega.toLocaleTimeString("pt-BR", {
              hour: "2-digit",
              minute: "2-digit",
            }),
          }
        : { hora_entrega: null }),
    };

    await $http.patch(
      `${urlServidor.urlServidorChatAdmin}/leiloes/altera-veiculos-agendamento/${payload.id_protocolo}`,
      {
        ...payload,
      }
    ); 
    myFunctions.showAlert(
      `Veiculo com protocolo ${data.id_protocolo} editado com sucesso`
    );
$scope.buscaAgendamentos()
  } catch (error) {
    console.log(error);
    alert("um erro ocorreu");
  }
};

  $scope.buscaAgendamentos = async () => {
    let cliente = null;
    let sem_data = false;
    if ($scope.dadosAgendados.cliente) {
      cliente = $scope.dadosAgendados.cliente.id;
    }

    if ($scope.dadosAgendados.sem_data) {
      sem_data = $scope.dadosAgendados.sem_data;
    }

    $scope.agendamentosList = [];
    let dados = {
      ...$scope.dadosAgendados,
      cliente,
      sem_data,
    };

    $http
      .get(urlServidor.urlServidorChatAdmin + "/leiloes/busca-agendamentos", {
        params: dados,
      })
      .then(({ data }) => {
        if (!data.length > 0) {
          ("Pesquisa não encontrou dados! Revise parâmetros da pesquisa!");

          $scope.agendamentosList = [];
          return;
        }
        let dados = data.map((item) => {
          let status;
          if (item.id_usuario_entrega !== null) {
            status = "ENTREGUE";
          } else if (item.id_usuario_cancelamento !== null) {
            status = "CANCELADO";
          } else {
            status = "PENDENTE";
          }
          return {
            ...item,
            hora_entrega: $scope.converterHora(item.hora_entrega),
            data_entrega: $scope.formatData(item.data_entrega),
            status,
          };
        });

        const filterBy = (pendentes, entregues, dados) => {
          if (pendentes) {
            return dados.filter((item) => item.status === "PENDENTE");
          }

          if (entregues) {
            return dados.filter((item) => item.status === "ENTREGUE");
          }

          return dados;
        };

        $scope.agendamentosList = filterBy(
          $scope.dadosAgendados.pendentes,
          $scope.dadosAgendados.entregues,
          dados
        );
      });
  };

  $scope.buscarClientes = async () => {
    const length = $scope.DadosPeriodo.id_cliente.length;
    //Somente com 3 letras para iniciar a pesquisa
    // Tendo 3 letras o container vai aparecer e o erro some
    if (length >= 3) {
      document.getElementById("Container").style.display = "block";
      document.getElementById("ErrorLength").style.display = "none";

      // faz a chamada e retorna os dados
      const response = await $http.put(
        urlServidor.urlServidorChatAdmin + "/clientes/listar",
        { razao_social: $scope.DadosPeriodo.id_cliente }
      );
      let dados = response.data;

      $scope.clientes = dados;

      //se for menor que 3 o modal some e o erro aparece
    } else if (length < 3) {
      document.getElementById("ErrorLength").style.display = "block";
      document.getElementById("Container").style.display = "none";
      $scope.DadosPeriodo.telefone_cliente = ''
      // se estiver vazio o container some
    } else if ($scope.DadosPeriodo.id_cliente == "") {
      document.getElementById("Container").style.display = "none";
      alert("h");
    }
  };


  $scope.buscaClienteEntrega = async () => {

    const length = $scope.DadosEntrega.id_cliente.length
    //Somente com 3 letras iremos iniciar a pesquisa
    // Tendo 3 letras o container vai aparecer e o erro some
    if (length >= 3) {
      document.getElementById("Container_cliente_entrega").style.display = "block"
      document.getElementById("ErrorLength_1").style.display = "none"

      const response = await $http.put(urlServidor.urlServidorChatAdmin + "/clientes/listar", { razao_social: $scope.DadosEntrega.id_cliente });
      let dados = response.data;
      $scope.clientesEntrega = dados;

    } else if (length < 3) {
      document.getElementById("ErrorLength_1").style.display = "block"
      document.getElementById("Container_cliente_entrega").style.display = "none"
      // se estiver vazio o container some
    } else if ($scope.DadosEntrega.id_cliente === '') {
      document.getElementById("Container_cliente_entrega").style.display = "none"

    }
  }
    

     
  $scope.gerarPlanilhaAgendamento = () => {

    if ($scope.Dados.length > 0) {

      var excelSelecao = $scope.Dados.map((x) => {
        return {
          ncv: x.id,
          edital: x.edital,
          lota: x.lote,
          rua: x.rua,
          placa: x.placa,
          marca_modelo: x.marca_modelo,
          chassi: x.chassi,
          tipo_veiculo: x.tipo_veiculo,
          status: x.status,
          classificacao: x.classificacao

        };
      });

      alasql(
        'SELECT * INTO XLSX("Agendamento-entrega-veiculos.xlsx",{headers:true}) FROM ?',
        [excelSelecao]
      );

    } else {
      myFunctions.showAlert("Sem dados para gerar Excel! Verifique!");
    }

  };


  $scope.gerarPDFAgendamento = function () {

    if ($scope.Dados.length > 0) {
      // let patio = $scope.patios.find((item) => item.id === $scope.DadosPeriodo.id_patio_origem);
      let cliente = $scope.DadosPeriodo.id_cliente;
      let dataAtual = new Date().toISOString().slice(0, 10);
      let idProtocolo = $scope.idProtocoloTransferencia;

      idProtocolo = $scope.idProtocoloTransferencia !== undefined ? $scope.idProtocoloTransferencia : "---"

      var PDFSelecao = $scope.Dados.map((x) => {
        return {
          ncv: x.id,
          edital: x.edital,
          lota: x.lote,
          rua: x.rua,
          placa: x.placa,
          marca_modelo: x.marca_modelo,
          chassi: x.chassi,
          tipo_veiculo: x.tipo_veiculo,
          status: x.status,
          classificacao: x.classificacao
        };
      });

      var doc = new jsPDF({ orientation: "landscape" });
      var totalPagesExp = "{total_pages_count_string}";

      doc.setFontSize(10);

      doc.autoTable({
        columnStyles: {
          vencimento: { halign: "left" },
        },

        body: PDFSelecao,
        columns: [
          { header: "NCV", dataKey: "ncv" },
          { header: "Edital", dataKey: "edital" },
          { header: "Lote", dataKey: "lote" },
          { header: "Rua", dataKey: "rua" },
          { header: "Placa", dataKey: "placa" },
          { header: "Marca/Modelo", dataKey: "marca_modelo" },
          { header: "Chassi", dataKey: "chassi" },
          { header: "Tipo de Veiculo", dataKey: "tipo_veiculo" },
          { header: "Status", dataKey: "status" },
          { header: "Classificação", dataKey: "classificacao" }
        ],
        bodyStyles: {
          margin: 10,
          fontSize: 8,
        },

        didDrawPage: function (data) {
          // Header
          doc.setFontSize(12);
          doc.setTextColor(40);
          doc.setFontStyle("normal");

          doc.text(
            "Número Protocolo: " + idProtocolo,
            data.settings.margin.left + 5, 18
          );

          doc.text(
            "Cliente: " + cliente + "        " +
            "Data Entrega: " + $scope.DadosPeriodo.data.toLocaleDateString("pt-BR") +
            "   " + "Hora: " + $scope.DadosPeriodo.hora.toLocaleTimeString('pt-BR', { hour: '2-digit', minute: '2-digit' }),
            data.settings.margin.left + 5, 25
          );

          // Footer
          var str = "Pagina " + doc.internal.getNumberOfPages();
          // Total page number plugin only available in jspdf v1.0+
          if (typeof doc.putTotalPages === "function") {
            str = str + " de " + totalPagesExp;
            str =
              str +
              "                                                                           Data " +
              dataAtual.substr(8, 2) +
              "/" +
              dataAtual.substr(5, 2) +
              "/" +
              dataAtual.substr(0, 4) +
              "                  Usuário " +
              localStorage.getItem("nome_usuario");
            var totalpaginas = totalPagesExp;
          }

          doc.setFontSize(10);

          // jsPDF 1.4+ uses getWidth, <1.4 uses .width
          var pageSize = doc.internal.pageSize;
          var pageHeight = pageSize.height
            ? pageSize.height
            : pageSize.getHeight();

          doc.text(
            str + "                        ",
            data.settings.margin.left,
            pageHeight - 10
          );
        },
        margin: { top: 30 },
      });
      if (typeof doc.putTotalPages === "function") {
        doc.putTotalPages(totalPagesExp);
      }

      doc.save("Agendamento-entrega-veiculos.pdf");
    } else {
      myFunctions.showAlert("Sem dados para gerar PDF! Verifique!");
    }
  };


  $scope.gerarPlanilhaEntrega = () => {

    if ($scope.dadosList) {

      var excelSelecao = $scope.dadosList.map((x) => {
        return {
          protocolo: x.id_protocolo,
          cliente: x.nome_cliente,
          telefone: x.telefone_cliente,
          data: x.data_entrega,
          hora: x.hora_entrega,
          ncv: x.id,
          edital: x.edital,
          lota: x.lote,
          rua: x.rua,
          classificacao: x.classificacao,
          placa: x.placa,
          classi: x.chassi,
          marca_modelo: x.marca_modelo,
          tipo_veiculo: x.tipo_veiculo,
          cor: x.cor

        };
      });

      alasql(
        'SELECT * INTO XLSX("Entrega-veiculos-agendados.xlsx",{headers:true}) FROM ?',
        [excelSelecao]
      );

    } else {
      myFunctions.showAlert("Sem dados para gerar Excel! Verifique!");
    }

  };
    



  $scope.gerarPDFEntrega = function () {
    if ($scope.dadosList) {
      let dataAtual = new Date().toISOString().slice(0, 10);
      let reboque = $scope.nomeReboque !== null ? $scope.nomeReboque : "---";
      let motorista = $scope.nomeMotorista !== null ? $scope.nomeMotorista : "---";

      var PDFSelecao = $scope.dadosList.map((x) => {
        return {
          id_protocolo: x.id_protocolo,
          nome_cliente: x.nome_cliente,
          telefone_cliente: x.telefone_cliente,
          data_entrega: $scope.formatData(item.data_entrega),
          hora_entrega: new Date(x.hora_entrega).toLocaleTimeString(
            "pt-BR",
            {
              hour: "2-digit",
              minute: "2-digit",
            }
          ),
          ncv: x.id,
          edital: x.edital,
          lote: x.lote,
          rua: x.rua,
          classificacao: x.classificacao,
          placa: x.placa,
          chassi: x.chassi,
          marca_modelo: x.marca_modelo,
          tipo_veiculo: x.tipo_veiculo,
          cor: x.cor
        };
      });

      var doc = new jsPDF({ orientation: "landscape" });

      var totalPagesExp = "{total_pages_count_string}";

      doc.setFontSize(10);

      doc.autoTable({
        columnStyles: {
          vencimento: { halign: "left" },
        },

        body: PDFSelecao,
        columns: [
          { header: "Protocolo", dataKey: "id_protocolo" },
          { header: "Cliente", dataKey: "nome_cliente" },
          { header: "Telefone", dataKey: "telefone_cliente" },
          { header: "Data Agendada", dataKey: "data_entrega" },
          { header: "Hora Agendada", dataKey: "hora_entrega" },
          { header: "NCV", dataKey: "ncv" },
          { header: "Edital", dataKey: "edital" },
          { header: "Lote", dataKey: "lote" },
          { header: "Rua", dataKey: "rua" },
          { header: "Classificação", dataKey: "classificacao" },
          { header: "Placa", dataKey: "placa" },
          { header: "Chassi", dataKey: "chassi" },
          { header: "Marca/Modelo", dataKey: "marca_modelo" },
          { header: "Tipo Veiculo", dataKey: "tipo_veiculo" },
          { header: "Cor", dataKey: "cor" },

        ],
        bodyStyles: {
          margin: 10,
          fontSize: 8,
        },

        didDrawPage: function (data) {
          // Header
          doc.setFontSize(12);
          doc.setTextColor(40);
          doc.setFontStyle("normal");

          doc.text(
            "Número Protocolo: " + $scope.dados.numero_protocolo + "           " + "Cliente: " + $scope.nomeCliente,
            data.settings.margin.left + 5, 18
          );

          doc.text(
            "Motorista: " + motorista + "     Transp.: " + reboque + "      Quantidade: " + PDFSelecao.length,
            data.settings.margin.left + 5, 25
          );

          // Footer
          var str = "Pagina " + doc.internal.getNumberOfPages();
          // Total page number plugin only available in jspdf v1.0+
          if (typeof doc.putTotalPages === "function") {
            str = str + " de " + totalPagesExp;
            str =
              str +
              "                                                                           Data " +
              dataAtual.substr(8, 2) +
              "/" +
              dataAtual.substr(5, 2) +
              "/" +
              dataAtual.substr(0, 4) +
              "                  Usuário " +
              localStorage.getItem("nome_usuario");
            var totalpaginas = totalPagesExp;
          }

          doc.setFontSize(10);

          // jsPDF 1.4+ uses getWidth, <1.4 uses .width
          var pageSize = doc.internal.pageSize;
          var pageHeight = pageSize.height
            ? pageSize.height
            : pageSize.getHeight();

          doc.text(
            str + "                        ", data.settings.margin.left, pageHeight - 10
          );
        },
        margin: { top: 30 },
      });
      if (typeof doc.putTotalPages === "function") {
        doc.putTotalPages(totalPagesExp);
      }

      doc.save("Entrega-veiculos-agendados.pdf");
    } else {
      myFunctions.showAlert("Sem dados para gerar PDF! Verifique!");
    }
  };
    

 
  $scope.gerarPlanilhaAgendamentos = function () {
    if ($scope.agendamentosList) {
      alasql(
        'SELECT * INTO XLSX("relatorio-lista-agendamentos.xlsx",{headers:true}) FROM ?',
        [$scope.agendamentosList]
      );
    } else {
      myFunctions.showAlert("Sem dados para gerar planilha! Verifique!");
    }
  };

   
  $scope.gerarPDFAgendamentos = function () {
    if ($scope.agendamentosList) {
      $scope.agendamentosList = $scope.agendamentosList.map((data)=>({
        ...data,
        data_entrega: $scope.formatData(data.data_entrega),
        ...(data.hora_entrega ? {hora_entrega:new Date(data.hora_entrega).toLocaleTimeString(
          "pt-BR",
          {
            hour: "2-digit",
            minute: "2-digit",
          }
        ),} : {hora_entrega: "Sem data" }),
      }))
     

      var doc = new jsPDF({ orientation: "landscape" });
      var totalPagesExp = "{total_pages_count_string}";

      doc.setFontSize(10);
      doc.autoTable({
        columnStyles: {
          vencimento: { halign: "left" },
        },

        body: $scope.agendamentosList,
        columns: [
          { header: "Protocolo", dataKey: "id_protocolo" },
          { header: "Data Agendada", dataKey: "data_entrega" },
          { header: "Hora Agendada", dataKey: "hora_entrega" },
          { header: "Cliente", dataKey: "nome_cliente" },
          { header: "Telefone", dataKey: "telefone_cliente" },
          { header: "Status", dataKey: "status" },
        ],
        bodyStyles: {
          margin: 10,
          fontSize: 8,
        },
        didDrawPage: function (data) {
          // Header
          doc.setFontSize(18);
          doc.setTextColor(40);
          doc.setFontStyle("normal");

          doc.text("Agendamentos de entrega entre: " + $scope.dadosAgendados.data_inicial.toLocaleDateString("pt-BR") + " a "
            + $scope.dadosAgendados.data_final.toLocaleDateString("pt-BR") + "     Quantidade: " + $scope.agendamentosList.length, data.settings.margin.left + 5, 18);

          doc.text("Data geração: " + new Date().toLocaleDateString("pt-BR"), data.settings.margin.left + 5, 25);

          // Footer
          var str = "Pagina " + doc.internal.getNumberOfPages();
          // Total page number plugin only available in jspdf v1.0+
          if (typeof doc.putTotalPages === "function") {
            str = str + " de " + totalPagesExp;
            var totalpaginas = totalPagesExp;
          }

          doc.setFontSize(10);

          // jsPDF 1.4+ uses getWidth, <1.4 uses .width
          var pageSize = doc.internal.pageSize;
          var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();

          doc.text(
            str + "                        ",
            data.settings.margin.left,
            pageHeight - 10
          );

        }, margin: { top: 30 },
      });

      if (typeof doc.putTotalPages === "function") {
        doc.putTotalPages(totalPagesExp);
      }

      doc.save("relatorio-lista-agendamentos.pdf");

    } else {
      myFunctions.showAlert("Sem dados para gerar PDF! Verifique!");
    }
  };
});
