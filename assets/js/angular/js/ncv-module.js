/* 
 * Luiz A C Costa - 1 de Set 2020: 21:03
 */
/* global app */

app.factory('controlePatioFac', ['$http', '$q', function ($http, $q) {
        let controlePatioFac = {
            isEmpty: function (value, isProperts) {
                isProperts = isProperts === undefined ? false : isProperts;
                if (value === null) {
                    return true;
                }
                if (angular.isUndefined(value)) {
                    return true;
                }
                if (value instanceof Date) {
                    return false;
                }
                if (value instanceof RegExp) {
                    return false;
                }
                var type = value instanceof Date ? "date" : typeof value !== "object" ? typeof value : angular.isArray(value) ? "array" : "object";
                switch (type) {
                    case "object":
                        if (value === {}) {
                            return true;
                        }
                        if (isProperts) {
                            for (var prop in value) {
                                if (!controlePatioFac.isEmpty(value[prop])) {
                                    return false;
                                }
                            }
                            return true;
                        }
                        return Object.keys(value).length === 0 ? true : false;
                        break;
                    case "array":
                        return value.length === 0 || value === [] ? true : false;
                        break;
                    case "number":
                        return false;
                        break;
                    case "boolean":
                        return false;
                        break;
                    case "function":
                        return false;
                        break;
                    default:
                        if (value.length > 0) {
                            return false;
                        }
                        return true;
                        break;
                }
            },
            getlocalStorageObj: function (name) {
                var valor = localStorage.getItem("cp_" + name);
                if (!controlePatioFac.isEmpty(valor)) {
                    try {
                        var retorno = angular.isString(valor) ? angular.fromJson(valor) : valor;
                        return retorno;
                    } catch (e) {
                        return null;
                    }
                }
                return null;
            },
            setlocalStorageObj: function (name, obj) {
                try {
                    localStorage.setItem("cp_" + name, angular.toJson(obj));
                } catch (e) {
                    console.error("Erro ao carregar", e);
                }
            },
            getHtml: function (url) {

                let deferred = $q.defer();
                let isLocalHost = window.location.hostname.indexOf("localhost") !== -1;
                let getSuccess = function (data) {
                    controlePatioFac.setlocalStorageObj(url, data.data);
                    deferred.resolve(data.data);
                };
                let getError = function (rejection) {
                    deferred.resolve("<div>Erro: 404</div>");
                    console.error("Erro ao carregar o html:" + rejection);
                };
                if (!isLocalHost) {
                    let html = controlePatioFac.getlocalStorageObj(url);
                    if (controlePatioFac.isEmpty(html)) {
                        $http.get(url).then(getSuccess, getError);
                    } else {
                        deferred.resolve(html);
                    }
                } else {
                    $http.get(url).then(getSuccess, getError);
                }
                return deferred.promise;
            },
            multHttp: function (list, method) {
                let promises = [];
                if (!controlePatioFac.isEmpty(list)) {
                    for (var i = 0; i < list.length; i++) {
                        if (!controlePatioFac.isEmpty(list[i])) {
                            try {
                                promises[i] = $http[method || "get"](list.url, {
                                    data: list[i].data
                                });
                            } catch (e) {
                                promises[i] = {data: null};
                            }
                        }
                    }
                }
                return $q.all(promises);
            }
        };
        return controlePatioFac;
    }
]);
app.factory('caixaFac', [
    '$http',
    'controlePatioFac',
    'urlServidor',
    'myFunctions', function ($http, controlePatioFac, urlServidor, myFunctions) {
        let caixaFac = {
            controlePatioFac: controlePatioFac,
            myFunctions: myFunctions,
            objLancamento: function (ncv, id_cliente_fornec, valor, data, observacao,id_patio) {
                let novoLancamento = {
                    id_cliente_fornec: id_cliente_fornec || null,
                    valor: valor || 0,
                    grupo: 12,
                    comentario: observacao || "",
                    data: data || new Date(),
                    categoria: 74,
                    documento: ncv || null,
                    idempresa: 1,
                    conta: 8,
                    id_patio: id_patio
                };
                return novoLancamento;
            },
            cadastrar: function (lancamento) {
                //Endpoint : /caixa/cadastrar
                if (!controlePatioFac.isEmpty(lancamento)) {
                    let url = urlServidor.urlServidorChatAdmin + "/caixa/cadastrar-liberacao";
                    return $http.post(url, lancamento).then(function (cb) {
                        return cb.data;
                    }, function () {
                        return [];
                    });
                } else {
                    return [];
                }
            }
        };
        return caixaFac;
    }
]);
app.factory('ncvFac', [
    '$http',
    '$q',
    'controlePatioFac',
    'caixaFac',
    'urlServidor',
    'myFunctions', function ($http, $q, controlePatioFac, caixaFac, urlServidor, myFunctions) {
        let ncvFac = {
            validaCamposObrigatorios: function (ncv, guincho, apreensao, liberacao) {
                return new Promise((resolve, reject) => {
                    let _campos = '';
                    if (ncvFac.controlePatioFac.isEmpty(liberacao) || ncvFac.controlePatioFac.isEmpty(guincho) 
                    || ncvFac.controlePatioFac.isEmpty(apreensao)) {
                        _campos = _campos + '<p>Existem campos obrigatórios sem preencher!</p>';
                        resolve(_campos);
                    }
                    if (ncv.tipo_veiculo == '' || ncv.tipo_veiculo == null) {
                        _campos = _campos + '<p>Informar Tipo Veiculo </p>';
                    }
                    if (ncv.placa == '' || ncv.placa == null) {
                        _campos = _campos + '<p>Informar Placa Veiculo </p>';
                    }
                    if (ncv.id_patio == '' || ncv.id_patio == null) {
                        _campos = _campos + '<p>Informar Patio </p>';
                    }
                    if (ncv.id_tarifa == '' || ncv.id_tarifa == null) {
                        _campos = _campos + '<p>Informar Tabela de Tarifa </p>';
                    }
                    if (guincho.blitz == '' || guincho.blitz == null) {
                        _campos = _campos + '<p>Informar Se Blitz	 </p>';
                    }
                    if (guincho.id_reboque === '' || guincho.id_reboque == null) {
                        _campos = _campos + '<p>Informar Reboque </p>';
                    }
                    if (guincho.id_motorista === '' || guincho.id_motorista == null) {
                        _campos = _campos + '<p>Informar Motorista </p>';
                    }
                    if (guincho.km_percorrido === '' || guincho.km_percorrido == null) {
                        _campos = _campos + '<p>Informar Km Percorrido </p>';
                    }
                    if (apreensao.data == '' || apreensao.data == null) {
                        _campos = _campos + '<p>Informar Data Apreensão </p>';
                    }
                    if (apreensao.hora == '' || apreensao.hora == null) {
                        _campos = _campos + '<p>Informar Hora da Apreensão </p>';
                    }
                    if (apreensao.local == '' || apreensao.local == null) {
                        _campos = _campos + '<p>Informar Local da Apreensão </p>';
                    }
                    if (liberacao.liberado_para == '' || liberacao.liberado_para == null) {
                        _campos = _campos + '<p>Informar Liberado Para </p>';
                    }
                    if (liberacao.faturado_para == '' || liberacao.faturado_para == null) {
                        _campos = _campos + '<p>Informar Faturado Para </p>';
                    }
                    if (liberacao.tipo_alvara == '' || liberacao.tipo_alvara == null) {
                        _campos = _campos + '<p>Informar Tipo Alvara </p>';
                    }
                    if (liberacao.tipo_desconto == '' || liberacao.tipo_desconto == null) {
                        _campos = _campos + '<p>Informar Tipo Desconto </p>';
                    }
                    if (liberacao.tipoliberacao !== 'LIBERADO' && liberacao.tipoliberacao !== 'LIBERADO ARR') {
                        _campos = _campos + '<p>Informar Tipo de Liberação </p>';
                    }
/*
                    if (listaImagensConferente.length == 0 ) {
                        _campos = _campos + '<p>Sem Fotos de CONFERENTE (Necessário realizar a conferência antes da liberação)</p>';
                    }
*/
                    resolve(_campos);

                });
            },
            validaCamposObrigatoriosPagto: function (ncv, guincho, apreensao, liberacao) {
                return new Promise((resolve, reject) => {
                    let _campos = '';
                    if (ncvFac.controlePatioFac.isEmpty(liberacao) || ncvFac.controlePatioFac.isEmpty(guincho) || ncvFac.controlePatioFac.isEmpty(apreensao)) {
                        _campos = _campos + '<p>Exitem campos obrigatórios sem preencher!</p>';
                        resolve(_campos);
                    }
                    if (ncv.tipo_veiculo == '' || ncv.tipo_veiculo == null) {
                        _campos = _campos + '<p>Informar Tipo Veiculo </p>';
                    }
                    if (ncv.placa == '' || ncv.placa == null) {
                        _campos = _campos + '<p>Informar Placa Veiculo </p>';
                    }
                    if (ncv.id_patio == '' || ncv.id_patio == null) {
                        _campos = _campos + '<p>Informar Patio </p>';
                    }
                    if (ncv.id_tarifa == '' || ncv.id_tarifa == null) {
                        _campos = _campos + '<p>Informar Tabela de Tarifa </p>';
                    }
                    if (liberacao.faturado_para == '' || liberacao.faturado_para == null) {
                        _campos = _campos + '<p>Informar Faturado Para </p>';
                    }

                    resolve(_campos);

                });
            },
            controlePatioFac: controlePatioFac,
            caixaFac: caixaFac,
            myFunctions: myFunctions,
            buscaPagamentos: function (ncv_id) {
                let url = urlServidor.urlServidorChatAdmin + "/ncv/busca-pagamentos?ncv=" + ncv_id;
                return $http.get(url).then(function (cb) {
                    return cb.data;
                }, function () {
                    return [];
                });
            },
            gravaPagamento: function (ncv_pagamentos, ncv, liberacao) {
                let url = urlServidor.urlServidorChatAdmin + "/ncv/grava-pagamento";
                let promises = [];
                for (let i = 0; i < ncv_pagamentos.length; i++) {
                    promises.push($http.post(url, ncv_pagamentos[i], {
                        data: ncv_pagamentos[i]
                    }).then(function (cb) {

                        if( liberacao.tipoliberacao == 'LIBERADO' ){                   

                            if (controlePatioFac.isEmpty(liberacao.cliente) || controlePatioFac.isEmpty(liberacao.cliente.id)) {
                                return $http.get(urlServidor.urlServidorChatAdmin + '/clientes/busca-cpfcnpj', {params: {cpf_cnpj: liberacao.cpf_faturado_para}}).then(function (response) {
                                    if (response.data.length > 0) {
                                        caixaFac.cadastrar(caixaFac.objLancamento(ncv.id, response.data[0].id, cb.config.data.valor, cb.config.data.data, liberacao.observacao,ncv.id_patio));
                                    }
                                    return true;
                                });
                            } else {
                                caixaFac.cadastrar(caixaFac.objLancamento(ncv.id, liberacao.cliente.id, cb.config.data.valor, cb.config.data.data, liberacao.observacao,ncv.id_patio));
                                return true;
                            }

                        };

                    }, function () {
                        return false;
                    }));
                }
                return $q.all(promises);
            }
        };
        return ncvFac;
    }
]);
app.directive('listFormaPagto', ["ncvFac", function (ncvFac) {
    
    return {
            restrict: "A",
            scope: {
                ncv: "=?",
                infoPgto: "=?",
                action: "&"
            },
            templateUrl: "./views/ncv/liberacao-lista-forma-pagto.html",
            link: function ($scope) {
                $scope.isEmpty = ncvFac.controlePatioFac.isEmpty;
                $scope.listFormaPagto = [];
                $scope.total = 0;

                $scope.$on('chamarFuncaoNaDiretiva', function (event, data) { 
                    $scope.watchNcv()
                })
                
                $scope.watchNcv = function() {

                    $scope.$watch('ncv', function (ncv) {
                        if (!ncvFac.controlePatioFac.isEmpty(ncv) && !ncvFac.controlePatioFac.isEmpty(ncv.id)) {
                            ncvFac.buscaPagamentos($scope.ncv.id).then(function (list) {
                                $scope.listFormaPagto = list;
                                $scope.total = list.reduce(function (t, i) {
                                    return t + parseFloat(i.valor);
                                }, 0);
                                $scope.infoPgto = {
                                    temPgto: list.length > 0,
                                    total: $scope.total
                                };
                                $scope.$applyAsync();
                            });
                        }
                    }, true);
                }

                $scope.watchNcv()
            }
            
        };
    }
]);

app.directive('openFormaPagto', ["ncvFac", "$mdDialog", "$location","$http","urlServidor" ,function (ncvFac, $mdDialog,$location,$http,urlServidor) {
        return {
            restrict: "A",
            controller: "ncvEditarCtrl",
            scope: {
                ncv: "=?",
                liberacao: "=?",
                guincho: "=?",
                apreensao: "=?",
                disabled: "=?",
                action: "&"
            },
            link: function ($scope, elem) {
                let pgto = function () {
                    let t = 0, v = parseFloat($scope.liberacao.total);
                    if (!ncvFac.controlePatioFac.isEmpty($scope.pagamentos)) {
                        t = $scope.pagamentos.reduce(function (t, i) {
                            return t + parseFloat(i.valor);
                        }, 0);
                    }
                    v = parseFloat($scope.liberacao.total) - t;
                    return {
                        data: new Date(),
                        forma_pagamento: $scope.liberacao.forma_pagamento || $scope.tiposRecebimento[0].id,
                        valor: v,
                        obs: "",
                        ncv: $scope.ncv.id
                    };
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.addFormaPgto = function () {
                    $scope.ncv_pagamentos.push(pgto());
                    $scope.habilitadoInclusao = $scope.ncv_pagamentos.length === 3;
                };
                $scope.removePgto = function (index) {
                    if ($scope.ncv_pagamentos.length > 1) {
                        $scope.ncv_pagamentos.splice(index, 1);
                        return;
                    }
                    ncvFac.myFunctions.showAlert('Deve existir uma forma de Pagto');
                };

                $scope.emiteBoleto = function (index) {

                    let dados = $scope.liberacao ;
                    
                    let parametros = { 
                        valor:index , 
                        cpf_cnpj: dados.cpf_faturado_para ,
                        nome:dados.faturado_para,
                        cep:dados.cliente.cep
                    }

                    $http.post(urlServidor.urlServidorChatAdmin + '/receber/emite-boleto', {params: parametros}).then(function (response) {

                        window.open(response.data.detalhes.url, '_blank');

                    });                    
                    

                };

                $scope.actionLocal = async () => {
                    $scope.desabilitado = true;
                   try {
                    let t = $scope.ncv_pagamentos.reduce(function (t, i) {
                        return t + parseFloat(i.valor);
                    }, 0);
                    if (t > $scope.liberacao.total) {
                        ncvFac.myFunctions.showAlert('Valor não deve ultrapassar: ' + $scope.liberacao.total);
                        return;
                    }

            
                    for (const result of $scope.ncv_pagamentos) {
                        if ( result.cpf_cnpj_pagador && result.nome_pagador) continue
                        
                        return ncvFac.myFunctions.showAlert('CPF/CNPJ Pagador e Nome Pagador precisam estar preenchidos!')
                    }

                    for (const pagto of $scope.ncv_pagamentos) {
                        if (pagto.forma_pagamento !== 9) continue
                        if (pagto.valor === 0) continue

                        return ncvFac.myFunctions.showAlert('O valor para ISENTO deve ser 0,00! Verifique!');
                    }

                     const cb = await ncvFac.gravaPagamento($scope.ncv_pagamentos, $scope.ncv, $scope.liberacao)

                     if (cb.every(i => i)) {
                        $scope.disabled = true;
                        $mdDialog.cancel();
                        $scope.action({
                            infoPgto: {
                                total: t
                            }
                        });
                    } else {

                        $mdDialog.cancel();
                        $scope.action({
                            infoPgto: {
                                total: t
                            }
                        });
                        
                    }
                   } catch (error) {
                    ncvFac.myFunctions.showAlert('Erro ao gravar pagamento!');
                   } finally {
                    $scope.desabilitado = false;
                   }

                };
                elem.click(function (ev) {
                    $scope.desabilitado = false;

                    ncvFac.validaCamposObrigatoriosPagto($scope.ncv, $scope.guincho, $scope.apreensao, $scope.liberacao).then(function (_resultado) {
                    var textoDados = _resultado;
                    if (_resultado === '') {

                        if (!ncvFac.controlePatioFac.isEmpty($scope.ncv.id)) {
                            $scope.habilitadoInclusao = $scope.disabled;
                            $scope.ncv_pagamentos = [];
                            $scope.ncv_pagamentos.push(pgto());
                            ncvFac.controlePatioFac.getHtml("./views/ncv/liberacao-forma-pagto.html").then(function (html) {
                                $mdDialog.show({
                                    template: html,
                                    parent: angular.element(document.body),
                                    targetEvent: ev,
                                    scope: $scope,
                                    preserveScope: true
                                }).then(function (answer) {
                                    //cs;
                                }, function () {
                                    //cs;
                                });
                            });
                        } else {
                            let confirm = $mdDialog.alert()
                                    .title('ATENÇÃO!')
                                    .htmlContent("Selecione um NCV.")
                                    .ariaLabel('Lucky day')
                                    .ok('Ok');
                            $mdDialog.show(confirm).then(function () {
                                let ok = '';
                            });
                        }

                    } else {
                        let confirm = $mdDialog.alert()
                        .title('Campos Obrigatórios!')
                        .htmlContent(textoDados)
                        .ariaLabel('Lucky day')
                        .ok('Ok');
                        $mdDialog.show(confirm).then(function () {
                        let ok = '';
                        });
                    }    
                    });

                });    
 
            }
        }
    }
]);