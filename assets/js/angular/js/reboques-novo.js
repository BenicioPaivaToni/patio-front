"use strict";
angular
  .module("app")
  .controller(
    "reboquesNovoCtrl",
    function (
      $location,
      $scope,
      $http,
      urlServidor,
      myFunctions,
      $sce,
      urlImagens,
      buckets
    ) {
      $scope.urlImagens = urlImagens.urlDocumentosReboque;

      $scope.dados = {
        empresaReboque: "",
        descricao: "",
        placa: "",
        tipo: "",
        categoria: "",
        ativo: "",
        observacao: "",
        pdf_clrv: "",
        pdf_seguro: "",
        vencimento_seguro: "",
        vencimento_clrv: "",
        idempresa: localStorage.getItem("id_empresa"),
        ano_fabricacao: "",
        ano_modelo: "",
        chassi: "",
        nome_proprietario: "",
        combustivel: "",
        cor: "",
      };
      $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
      };
      var bucket = null;

      $http
        .get(urlServidor.urlServidorChatAdmin + "/autoridades/vercodigo")
        .then(function (response) {
          AWS.config.update({
            accessKeyId: response.data.ac,
            secretAccessKey: response.data.sc,
          });
          AWS.config.region = response.data.regiao;
        })
        .finally(function () {
          bucket = new AWS.S3();
        });

      $scope.arquivoCLRV = "";
      $scope.arquivoSeguro = "";

      $scope.uploadCLRV = (file) => {
        $scope.arquivoCLRV = file;
        $scope.dados.pdf_clrv = file.name;
        $scope.pdfCLRV = file;
        if ($scope.arquivoCLRV.name != undefined) {
          var params = {
            Key: $scope.arquivoCLRV.name,
            ContentType: $scope.arquivoCLRV.type,
            ContentDisposition: "inline",
            Body: $scope.arquivoCLRV,
            ACL: "public-read",
            Bucket: buckets.reboques,
          };
          $scope.arquivoCLRV = null;
          $scope.dados.pdf_clrv = null;
          $scope.pdfCLRV = null;
          bucket.putObject(params, function (err, data) {
            if (err) {
              myFunctions.showAlert(
                "Erro envio PDF da CLRV, verifique..." + err
              );
            } else {
              setTimeout(function () {
                myFunctions.showAlert("Arquivo cadastrado com sucesso!");
              }, 1500);
              $scope.arquivoCLRV = file;
              $scope.dados.pdf_clrv = file.name;
              $scope.pdfCLRV = file;
            }
          });
        }
      };

      $scope.uploadSeguro = (file) => {
        $scope.arquivoSeguro = file;
        $scope.dados.pdf_seguro = file.name;
        $scope.pdfSeguro = file;
        if ($scope.arquivoSeguro.name != undefined) {
          var paramsSeguro = {
            Key: $scope.arquivoSeguro.name,
            ContentType: $scope.arquivoSeguro.type,
            ContentDisposition: "inline",
            Body: $scope.arquivoSeguro,
            ACL: "public-read",
            Bucket: buckets.reboques,
          };
          $scope.arquivoSeguro = null;
          $scope.dados.pdf_seguro = null;
          $scope.pdfSeguro = null;

          bucket.putObject(paramsSeguro, function (err, data) {
            if (err) {
              myFunctions.showAlert(
                "Erro envio PDF Seguro, verifique..." + err
              );
            } else {
              setTimeout(function () {
                myFunctions.showAlert("Arquivo cadastrado com sucesso!");
              }, 1500);

              $scope.arquivoSeguro = file;
              $scope.dados.pdf_seguro = file.name;
              $scope.pdfSeguro = file;
            }
          });
        }
      };
      $scope.empresas =[]
      buscaEmpresas().then((result) => {
        $scope.empresas = result;
      });
      $scope.searchText = "";
      $scope.selectedItem = "";

      $scope.isDisabled = false;

      const getAutocompleteEmpresas = async (query) => {
        return await $http
          .get(
            `${urlServidor.urlServidorChatAdmin}/empresas_reboques/autocomplete/${query}`
          )
          .then(({ data }) => data);
      };

      $scope.querySearch = async (query) => {
        if (!query) return $scope.empresas;
        return await getAutocompleteEmpresas(query);
      };

      $scope.gravar = () => {
        const payload = {
          ...$scope.dados,
          empresa: $scope.dados.empresa.id,
        };

        $http
          .post(
            urlServidor.urlServidorChatAdmin + "/reboques/cadastrar",
            payload
          )
          .then(({ data }) => {
            if (data.code) {
              myFunctions.showAlert("Erro na gravação!");
              return;
            }

            if (data == "11000") {
              myFunctions.showAlert("Reboque já existe!");
              return;
            }

            myFunctions.showAlert("Cadastro executado com sucesso!");
            $scope.isDisabled = true;
          });
      };

      function buscaEmpresas() {
        return new Promise(function (resolve, reject) {
          $http
            .put(urlServidor.urlServidorChatAdmin + "/empresas_reboques/listar")
            .then(function (response) {
              let dados = response.data;

              dados.sort(function (a, b) {
                if (a.nome < b.nome) {
                  return 1;
                }
                if (a.contato > b.contato) {
                  return -1;
                }
                return 0;
              });

              resolve(dados);
            });
        });
      }
    }
  );
