angular.module('app').controller('TransferenciasEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep) {



    const queryString = window.location.hash;
    const id = queryString.slice(queryString.indexOf("%") + 3);




    $http.get(`${urlServidor.urlServidorChatAdmin}/transferencias/busca-transferencia`, { params: { id: id } }).then((response) => {

        const { data } = response

        const map = data.map(Item => ({
            conta_destino: Item.conta_destino,
            conta_origem: Item.conta_origem,
            data: Item.data,
            id: Item.id,
            patio_destino: Item.patio_destino,
            patio_origem: Item.patio_origem,
            valor: Item.valor.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}),

        }))
        $scope.dados = map[0]

        const buscaPatios = async () => {
            const response = await $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar')
            const { data } = response
            $scope.patios = data
        }


        const buscaContas = async () => {
            const response = await $http.put(urlServidor.urlServidorChatAdmin + '/contas/listar')
            $scope.contas = response.data;
        }

        $scope.gravar = async function () {

            try {
                await $http.post(`${urlServidor.urlServidorChatAdmin}/transferencias/alterar`, $scope.dados)
                myFunctions.showAlert('Cadastro executado com sucesso!');
                $scope.isDisabled = true;



            } catch (error) {
                console.error(error);
                myFunctions.showAlert('Erro na gravação!')
            }
        }

        $scope.paginaAnterior = () => {
            window.close()
        }


        setTimeout(() => {
            buscaPatios()
            buscaContas()
        }, 1000);





    })






}
)