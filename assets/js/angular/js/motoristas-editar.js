app.controller('motoristasEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep, $filter, $sce, urlImagens, $mdDialog, buckets) {
    $scope.urlDocumentos = urlImagens.urlDocumentosMotoristas;

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }
    var bucket = null;

    var urlParams = $location.search();
    $scope.dados = {
        id: urlParams.dados[0].id,
        empresaReboque: urlParams.dados[0].id_empresa_reboque,
        nome: urlParams.dados[0].nome,
        apelido: urlParams.dados[0].apelido,
        email: urlParams.dados[0].email,
        celular: urlParams.dados[0].celular,
        cnh: urlParams.dados[0].cnh,
        cnhCat: urlParams.dados[0].cnh_categoria,
        cnhVal: $filter('date')(urlParams.dados[0].cnh_validade, 'dd/MM/yyyy', "+0000"),
        categoria: urlParams.dados[0].categoria,
        ativo: urlParams.dados[0].ativo,
        observacao: urlParams.dados[0].observacao,
        endereco: urlParams.dados[0].endereco,
        bairro: urlParams.dados[0].bairro,
        cidade: urlParams.dados[0].cidade,
        cep: urlParams.dados[0].cep,
        uf: urlParams.dados[0].uf,
        inicio_contrato: $filter('date')(urlParams.dados[0].inicio_contrato, 'dd/MM/yyyy', "+0000"),
        fim_contrato: $filter('date')(urlParams.dados[0].fim_contrato, 'dd/MM/yyyy', "+0000"),
        pdf_contrato: urlParams.dados[0].pdf_contrato,
        pdf_cnh: urlParams.dados[0].pdf_cnh,
        id_usuario_app: urlParams.dados[0].id_usuario_app,
        comissao:urlParams.dados[0].comissao,

    }


    $scope.indices = {
        id_motorista: $scope.dados.id,
        id_patio: null,
        id_tipo_veiculo: null,
        km_padrao: null,
        saida_dia: null,
        saida_noite: null,
        valor_km: null
    
    }     



    buscaEmpresasReboque().then(function (result) {
        $scope.empresasReboque = result;
       
        let dados = result.filter(function(obj) {
            return obj.id == $scope.dados.empresaReboque
        })

        $scope.empresaReboque = dados

        if (dados.length > 0){

            $scope.identificador = dados[0].id

        }else{
            $scope.identificador = "00"

        }

    });

    $scope.termoHTML = ''


    var urlParams = $location.search();
    $http.get(urlServidor.urlServidorChatAdmin + '/autoridades/vercodigo').then(function (response) {

        AWS.config.update({ accessKeyId: response.data.ac, secretAccessKey: response.data.sc });
        AWS.config.region = response.data.regiao;
    }).finally(function () {

        bucket = new AWS.S3();
        $http.get(urlServidor.urlServidorChatAdmin + '/usuarios/usuarios-app-motorista', { params: { idEmpresa: localStorage.getItem('id_empresa') } }).then(function (response) {
            $scope.usuariosapp = response.data;
        });/*
        if ((urlParams.dados[0].pdf_contrato != '') && (urlParams.dados[0].pdf_contrato != undefined) && (urlParams.dados[0].pdf_contrato != null) ) {

            bucket.getObject(
                    {Bucket: "documentos-reidospatios", Key: urlParams.dados[0].pdf_contrato, ResponseContentDisposition: 'inline'},
                    function (error, data) {
                        if (error != null) {
                            myFunctions.showAlert("Falha no carregamento do PDF: " + error);
                        } else {

                            $scope.pdfContrato = new Blob([data.Body], {type: "application/pdf"});
                        }
                    }
            );
        }

        if ((urlParams.dados[0].pdf_cnh != '') && (urlParams.dados[0].pdf_cnh != undefined) && (urlParams.dados[0].pdf_cnh != null) ) {

            bucket.getObject(
                    {Bucket: "documentos-reidospatios", Key: urlParams.dados[0].pdf_cnh, ResponseContentDisposition: 'inline'},
                    function (error, data) {
                        if (error != null) {
                            myFunctions.showAlert("Falha no carregamento do PDF , arquivo invalido ou inexistente!");
                        } else {

                            $scope.pdfCnh = new Blob([data.Body], {type: "application/pdf"});

                        }
                    }
            );
        }*/

    });


    $scope.estados = estados.uf;
    $scope.uploadContrato = (file) => {

        $scope.pdfContrato = null;
        $scope.arquivoContrato = null;
        $scope.dados.pdf_contrato = null;
        if (file) {
            $scope.pdfContrato = file;
            $scope.arquivoContrato = file;
            $scope.dados.pdf_contrato = file.name;
            if ($scope.arquivoContrato != undefined && $scope.arquivoContrato != null) {

                var params = {
                    Key: $scope.arquivoContrato.name,
                    ContentType: $scope.arquivoContrato.type,
                    ContentDisposition: 'inline',
                    Body: $scope.arquivoContrato,
                    ACL: 'public-read',
                    Bucket: buckets.motoristas

                };
                $scope.pdfContrato = null;
                $scope.arquivoContrato = null;
                $scope.dados.pdf_contrato = null;
                bucket.putObject(params, function (err, data) {
                    if (err) {
                        myFunctions.showAlert('Erro envio PDF Contrato, verifique...' + err);

                    } else {

                        setTimeout(function () {
                            myFunctions.showAlert('Arquivo alterado!');

                        }, 1500);
                        $scope.pdfContrato = file;
                        $scope.arquivoContrato = file;
                        $scope.dados.pdf_contrato = file.name;

                    };
                });
            }
        }

    }
    
    $scope.uploadCnh = (file) => {

        $scope.pdfCnh = null;
        $scope.arquivoCnh = null;
        $scope.dados.pdf_cnh = null;
        if (file) {
            $scope.pdfCnh = file;
            $scope.arquivoCnh = file;
            $scope.dados.pdf_cnh = file.name;

            if ($scope.arquivoCnh != undefined || $scope.arquivoCnh != null) {

                var paramsChecklist = {
                    Key: $scope.arquivoCnh.name,
                    ContentType: $scope.arquivoCnh.type,
                    ContentDisposition: 'inline',
                    Body: $scope.arquivoCnh,
                    ACL: 'public-read',
                    Bucket: buckets.motoristas

                };
                $scope.pdfCnh = null;
                $scope.arquivoCnh = null;
                $scope.dados.pdf_cnh = null;
                bucket.putObject(paramsChecklist, function (err, data) {
                    if (err) {
                        myFunctions.showAlert('Erro envio PDF Checklist, verifique...' + err);
                    } else {

                        setTimeout(function () {

                            myFunctions.showAlert('Documento alterado!');

                        }, 1500);
                        $scope.pdfCnh = file;
                        $scope.arquivoCnh = file;
                        $scope.dados.pdf_cnh = file.name;

                    }

                });

            };

        };
    };
    $scope.limparPDFContrato = () => {
        let confirm = $mdDialog.confirm()
            .title('Contrato')
            .textContent('Tem certeza que deseja apagar esse documento?')
            .ariaLabel('Lucky day')
            .ok('Apagar')
            .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {


            var params = { Bucket: buckets.motoristas, Key: $scope.dados.pdf_contrato };

            bucket.deleteObject(params, function (err, data) {
                if (err) {
                    myFunctions.showAlert("Não foi possivel apagar o arquivo!");
                }  // error
                else {

                    $scope.pdfContrato = null;
                    $scope.arquivoContrato = null;
                    $scope.dados.pdf_contrato = null;
                    myFunctions.showAlert('Arquivo apagado!');

                }
            });
        });

    };

    $scope.limparPDFCnh = () => {
        let confirm = $mdDialog.confirm()
            .title('CNH')
            .textContent('Tem certeza que deseja apagar esse documento?')
            .ariaLabel('Lucky day')
            .ok('Apagar')
            .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {

            var params = {
                Bucket: buckets.motoristas,
                Key: $scope.dados.pdf_cnh
            }

            bucket.deleteObject(params, function (err, data) {
                if (err) {
                    myFunctions.showAlert("Não foi possivel apagar o arquivo!");
                }  // error
                else {
                    $scope.pdfCnh = null;
                    $scope.arquivoCnh = null;
                    $scope.dados.pdf_cnh = null;
                    myFunctions.showAlert('Arquivo apagado!');
                }
            });
        });

    }

    $scope.buscaCep = (cep) => {

        if (cep.length == 9) {

            viaCep.get(cep).then(function (response) {

                $scope.dados.endereco = response.logradouro.toUpperCase();
                $scope.dados.bairro = response.bairro.toUpperCase();
                $scope.dados.cidade = response.localidade.toUpperCase();
                $scope.dados.uf = response.uf.toUpperCase();
            });
        }

    }

    buscaPatios().then(function (result) {
        $scope.patios = result.filter((item ) => item.status == 1)
       
    })





    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/motoristas/alterar', $scope.dados).then(function (response) {
        
            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {

                    myFunctions.showAlert('Motorista já existe!')

                } else {

                    myFunctions.showAlert('Alteração executada com sucesso!');

                }

            };
        });
    };

    buscaTipoVeiculos().then((data) => {
        $scope.tiposVeiculos = data
        
    })

 
    
    $scope.limparIndices = function(){
    
        $scope.indices.id_patio = null
        $scope.indices.id_tipo_veiculo = null
        $scope.indices.km_padrao = 0
        $scope.indices.valor_km = "R$0,00"
        $scope.indices.saida_dia = "R$0,00"
        $scope.indices.saida_noite = "R$0,00"
        
    }
    


    $scope.removerIndice = async function (indices){
     
        let id = indices.id.toString()
     
       await $http.post(urlServidor.urlServidorChatAdmin + '/motoristas/indices-apagar', { id: id} ).then(function (response) {
            
            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                myFunctions.showAlert('Registro removido com sucesso!')

            }
        })

        buscaIndices()

    
    }
    

    const buscaIndices =  () => {
        
        setTimeout(function () {

        const id_motorista = $scope.dados.id

        $scope.indicesList = []

        $http.get(urlServidor.urlServidorChatAdmin + '/motoristas/indices', { params: { id_motorista: id_motorista } }).then(function (response) {

            let dadosRecebidos = response.data
            $scope.indicesRecebidos = dadosRecebidos
        
            let dados = dadosRecebidos.map(item => ({
                id: item.id,
                patio: $scope.patios.find((x) => x.id == item.id_patio).nome,
                tipo_veiculo: $scope.tiposVeiculos.find((x) => x.id == item.id_tipo_veiculo).descricao,
                km_padrao: item.km_padrao,
                valor_km: item.valor_km,
                saida_dia: item.saida_dia,
                saida_noite: item.saida_noite
            }))

            $scope.indicesList = dados
        
        });

    }, 1500)

    }



    buscaIndices()


    $scope.gravarIndices = function() {

        if (!$scope.indices.id_tipo_veiculo || !$scope.indices.id_patio) {

            myFunctions.showAlert( 'Pátio e Tipo de veiculo precisam estar preenchidos! Verifique!' )

            return

        }else{

        let buscaTipoVeiculo = null
        let buscaPatio = null
       
        buscaPatio = $scope.indicesRecebidos.filter(x => x.id_patio === $scope.indices.id_patio)
        
        buscaTipoVeiculo = buscaPatio.filter(y => y.id_tipo_veiculo === $scope.indices.id_tipo_veiculo)

        if (buscaTipoVeiculo.length > 0 ){

            myFunctions.showAlert( 'Já existe um registro para o Pátio e Tipo de Veiculo selecionado!' )

               return 

            }else{

                $http.post(urlServidor.urlServidorChatAdmin+'/motoristas/indices-cadastro', $scope.indices).then( function(response){
        
                    if (response.data.code){
                    
                        myFunctions.showAlert('Erro na gravação!')
        
                    }else{
        
                                            
                        myFunctions.showAlert( 'Dados gravados com sucesso!' );
                            
                        buscaIndices()

                        $scope.limparIndices()
                                
                    }
                })
            
            }
        }       

    }



    function buscaTipoVeiculos() {
        return new Promise(function (resolve, reject) {
          $http.get(urlServidor.urlServidorChatAdmin + '/tipos_veiculo/listar', {params: {idEmpresa: localStorage.getItem('id_empresa')}}).then(function (response) {
                 
            let dados = response.data
            resolve(dados)
            })
        })
    }


    function buscaPatios() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') }).then(function (response) {
                 
                let dados = response.data
                resolve(dados)
            })
        })
    }


    buscaEmpresas().then(function (result) {
        $scope.empresas = result;

    });




    function buscaEmpresas() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/empresas/listar', ).then(function (response) {
                 
                let dados = response.data
                resolve(dados)
            })
        })
    }



    function buscaEmpresasReboque() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/empresas_reboques/listar').then(function (response) {

                let dados = response.data;
                dados.sort(function (a, b) {
                    if (a.nome < b.nome) {
                        return 1;
                    }
                    if (a.nome > b.nome) {
                        return -1;
                    }
                    return 0;
                });
                resolve(dados);
            });
        });
    };


    $scope.abrirPagina = function () {

     //  <a ng-href="/#/signature.html" target="_blank" rel="noopener noreferrer"></a>
       
        window.open(`/signature.html`);

    }

    $scope.veiculo
    

    $scope.dataAtual = new Date().getDate() + ' de ' + new Date().toLocaleString('default', { month: 'long' }) + ' de ' + new Date().getFullYear()


    $scope.dadosEmpresa = function () {

        let dados = $scope.empresas.filter(function(obj) {
            return obj.id == $scope.empresa.id
        })

        $scope.empresaGrupo = dados

        carregarPaginasHtml()

    }

    $scope.atualizaDados = function() {

        carregarPaginasHtml()

    }
    
    
    $scope.limparPlacaReboque = function () {

        $scope.reboque.placa = null

    }

    $scope.reboquesList = []
    
    $scope.reboque = {
        placa: null,
        percentual_pedagio: null
    }


    

    $scope.buscaPlacaReboque = async () => {
        
        if ( $scope.reboque.placa == null ) {

          myFunctions.showAlert( `Informe a placa do reboque cadastrado no sistema!` );
        
        } else { 

            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/empresas_reboques/busca-reboque-placa', {
                params: {
                  placa: $scope.reboque.placa,
                  id_empresa_reboque: $scope.identificador
                }
            })
            
            const data = response.data            

          if (data.length == 0) {
            myFunctions.showAlert('Veiculo não encontrado! Verifique se a placa foi digitada corretamente e se o mesmo pertence a esta empresa de reboque!');
          } else {

            const txt = 
                `
                <div>
                    <header>
                  
                        <title>
                          
                        </title>
                  
                    </header>
                    <br/>
                  
                    <body> 
                      
                    <div> 
                        <div>PLACA:</div>
                        <div><strong>${data[0].placa}</strong></div>
                    </div>
                    <br/>

                    <div> 
                    <div>DESCRICAO:</div>
                    <div><strong>${data[0].descricao}</strong></div>
                    </div>

                    <br/>

                    <div> 
                        <div>TIPO:</div>
                        <div><strong>${data[0].tipo || 0}</strong></div>
                    </div>
                    <br/>
                  

                    <div> 
                        <div>CATEGORIA:</div>
                        <div><strong>${data[0].categoria || 0 }</strong></div>
                    </div>
                    <br/>
                      
                    <div> 
                        <div>CHASSI:</div>
                        <div><strong>${data[0].chassi || 0}</strong></div>
                    </div>
                    <br/>
                    </body>
                      
                </div>
                  
            `

            let confirm = $mdDialog
              .confirm()
              .ariaLabel("Lucky day")
              .title("Confira os dados do reboque")
              .ok("ADICIONAR AO CONTRATO")
              .cancel("Cancelar")
              .htmlContent(txt);
            $mdDialog.show(confirm).then(() => {
            $scope.reboquesList.push(data[0])
            
            $scope.limparPlacaReboque()

            carregarPaginasHtml()
                
            });
          }
        }
      }



    function termoCredenciamentoHtml() {
    
        var htmlString = 
        `
            <div>
            
                <body> 

                    <br/>
                    <div>  <h3 style="font-size: 24px; margin: 10px 0; text-align: center;">TERMO DE CREDENCIAMENTO</h3> </div>
                    <br/>
                    
                    <div id="emp-id" style="display:none">${$scope.identificador}</div>      
                    
                    <div> 
                        
                        <p>Aos ${$scope.dataAtual}, comparece na presença da empresa <strong>${$scope.empresaGrupo[0]?.nome_empresa ? $scope.empresaGrupo[0]?.nome_empresa : "????"}</strong>,
                        pessoa jurídica de direito privado inscrita no cnpj sob n° ${$scope.empresaGrupo[0]?.cnpj ? $scope.empresaGrupo[0]?.cnpj : "????"}, com sede na 
                        ${$scope.empresaGrupo[0]?.endereco ? $scope.empresaGrupo[0]?.endereco : "????"}, bairro ${$scope.empresaGrupo[0]?.bairro ? $scope.empresaGrupo[0]?.bairro : "????"}, 
                        na cidade de ${$scope.empresaGrupo[0]?.cidade? $scope.empresaGrupo[0]?.cidade : "????"}, CEP ${$scope.empresaGrupo[0]?.cep ? $scope.empresaGrupo[0]?.cep : "????"}, 
                        neste ato representada pelo seu representante legal ${$scope.empresaGrupo[0]?.repres_nome ? $scope.empresaGrupo[0]?.repres_nome : "????"},  
                        portador do RG ${$scope.empresaGrupo[0]?.repres_rg ? $scope.empresaGrupo[0]?.repres_rg : "????"} e do CPF ${$scope.empresaGrupo[0]?.repres_cpf ? $scope.empresaGrupo[0]?.repres_cpf : "????"}, 
                        residente e domiciliados na cidade de ${$scope.empresaGrupo[0]?.repres_cidade ? $scope.empresaGrupo[0]?.repres_cidade : "????"}, estado de ${$scope.empresaGrupo[0]?.repres_uf ? $scope.empresaGrupo[0]?.repres_uf : "????"}, 
                        a empresa ${$scope.empresaReboque[0]?.nome ? $scope.empresaReboque[0]?.nome : "????"}, inscrita no CNPJ sob nº ${$scope.empresaReboque[0]?.cpf_cnpj ? $scope.empresaReboque[0]?.cpf_cnpj : "????"}, estabelecida no endereço 
                        ${$scope.empresaReboque[0]?.endereco ? $scope.empresaReboque[0]?.endereco : "????"} – ${$scope.empresaReboque[0]?.bairro ? $scope.empresaReboque[0]?.bairro : "????"} 
                        – ${$scope.empresaReboque[0]?.cidade ? $scope.empresaReboque[0]?.cidade : "????"} – ${$scope.empresaReboque[0]?.uf ? $scope.empresaReboque[0]?.uf : "????"} 
                        (CEP: ${$scope.empresaReboque[0]?.cep ? $scope.empresaReboque[0]?.cep : "????"}), neste ato representada por 
                        ${$scope.empresaReboque[0]?.rep_nome ? $scope.empresaReboque[0]?.rep_nome : "????"}, portador do RG nº ${$scope.empresaReboque[0]?.rep_rg ? $scope.empresaReboque[0]?.rep_rg : "????"} 
                        e CPF nº ${$scope.empresaReboque[0]?.rep_cpf ? $scope.empresaReboque[0]?.rep_cpf : "????"}, de livre e espontânea vontade firma o presente 
                        <strong>TERMO DE CREDENCIAMENTO</strong> para <strong>prestação de serviços de remoção de veículos</strong>, prometendo fielmente observar 
                        as condições constantes do ANEXO I, que fica fazendo parte integrante deste para o desempenho dos trabalhos.</p>
                
                    </div>

                    <br/>

                    <p>Por ser verdade firmo o presente.</p>
                                
                    <br/>

                    <p>Santa Cruz do Rio Pardo, ${$scope.dataAtual}.</p>

               
                      <div class="col-md-3 ">
                        <img id="signature"  width="400px" height="80px">
                      </div>
                   
                    <hr noshade=”noshade” width="50%" size="2px" align="left"/> 
                    <p style="margin: 0;">Credenciado: ${$scope.empresaReboque[0]?.nome ? $scope.empresaReboque[0]?.nome : "????"} - 
                    ${$scope.empresaReboque[0]?.cpf_cnpj ? $scope.empresaReboque[0]?.cpf_cnpj : "????"}</p>
                    <p style="margin: 0;" id="representante">Representante legal: ${$scope.empresaReboque[0]?.rep_nome ? $scope.empresaReboque[0]?.rep_nome : "????"} - 
                    ${$scope.empresaReboque[0]?.rep_cpf ? $scope.empresaReboque[0]?.rep_cpf : "????"} </p>
                    
                    <br/>
                    <br/>
                    <br/>
                    <hr noshade=”noshade” width="50%" size="2px" align="left"/> 
                    <p style="margin: 0;">Credenciante: ${$scope.empresaGrupo[0]?.nome_empresa ? $scope.empresaGrupo[0]?.nome_empresa : "????"} - 
                    ${$scope.empresaGrupo[0]?.cnpj ? $scope.empresaGrupo[0]?.cnpj : "????"}</p>
                    <p style="margin: 0;">Representante legal: ${$scope.empresaGrupo[0]?.repres_nome ? $scope.empresaGrupo[0]?.repres_nome : "????"} - 
                    ${$scope.empresaGrupo[0]?.repres_cpf ? $scope.empresaGrupo[0]?.repres_cpf : "????"} </p>
                   
                    <br/>                   
                    <br/>
                    <br/>
                    <br/>
                    <hr noshade=”noshade” width="50%" size="2px" align="left"/> 
                    <p style="margin: 0;">Vistoriador:   
                    <p style="margin: 0;">Representante legal:
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
              
                        
                </body>
            
            </div>
        `

    $scope.termoCredenciamento = $sce.trustAsHtml(htmlString);

    }


    function anexoInicioHtml(){

        var htmlString = 
        `
            <div> <h3 style="font-size: 24px; margin: 10px 0; text-align: center;"> ANEXO I </h3> </div>

            <br/>
            <br/>

            <div> <strong>ITEM 01 – DOS DOCUMENTOS NECESSÁRIOS PARA O CREDENCIAMENTO</strong></div>
                <ol type="a">
                    <li>Cópia do Contrato Social;</li>
                    <li>Cópia de um documento com foto do responsável pela assinatura do termo de credenciamento que conste no contrato social;</li>
                    <li>Cópia do CRLV dos veículos que serão utilizados para a prestação de serviços com licenciamento em dia;</li>
                    <li>Cópia da CNH do responsável pela direção do veículo guincho constando de sua devida categoria.</li>
                </ol>
        
            <br/>

            <div><strong>ITEM 02 – DO PRAZO DO CREDENCIAMENTO</strong></div>
                <ol type="a">
                    <li>12 (doze) meses, a partir da data de credenciamento, podendo ser prorrogado mediante aditivo por escrito, desde que haja 
                        interesse entre as partes, com pelo menos 30 (trinta) dias de antecedência.
                    </li>
                </ol>
            <br/>

            <div><strong>ITEM 03 – DO LOCAL DA PRESTAÇÃO DO SERVIÇO E TABELA DE VALORES</strong></div>
            <br/>    
            
        `

        $scope.anexoInicio = $sce.trustAsHtml(htmlString);

    }



    function gerarValoresHtml() {

        var dados = $scope.indicesList

        dados.sort((a,b) => (a.patio > b.patio) ? 1 : ((b.patio > a.patio) ? -1 : 0))
        
    let htmlString = 

        '<table>' +
        '<thead>' +
        '<tr>' +
        '<th>Local / Pátio</th>' +
        '<th>Tipo Veiculo</th>' +
        '<th>KM Padrão</th>' +
        '<th>Valor Dia</th>' +
        '<th>Valor Noite</th>' +
        '<th>KM Excedente</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>'


    for (let i = 0; i < dados.length; i++) {
        const currentRow = dados[i]
        const nextRow = dados[i + 1]
      
        htmlString += '<tr>'
        htmlString += `<td style="font-size: 14px;">${currentRow.patio}</td>`
        htmlString += `<td style="font-size: 14px;">${currentRow.tipo_veiculo}</td>`
        htmlString += `<td style="font-size: 14px;">${currentRow.km_padrao}</td>`
        htmlString += `<td style="font-size: 14px;">${currentRow.saida_dia}</td>`
        htmlString += `<td style="font-size: 14px;">${currentRow.saida_noite}</td>`
        htmlString += `<td style="font-size: 14px;">${currentRow.valor_km}</td>`
        htmlString += '</tr>'

        if (nextRow && nextRow.patio !== currentRow.patio) {

          htmlString += '<tr><td colspan="2"></td></tr>'
        }
    }

    htmlString += '</table>';

        htmlString += 
            '</tbody>' +
            '</table>' +
            '<br/>' +
            '<ul>' +
                '<li>Havendo pedágio, a empresa credenciante arcará com o equivalente a ' + ($scope.reboque.percentual_pedagio || 0) + '% do valor, mediante apresentação dos recibos.</li>' +
            '<ul>'

        $scope.valores = $sce.trustAsHtml(htmlString);

    }



    function formaPagamentoHtml() {

        var htmlString = 
            `
                <br/>
                <div><strong>ITEM 04 – DA FORMA DE PAGAMENTO</strong></div>
                <ol type="a"> 
                    <li>O pagamento será realizado mediante apresentação de nota fiscal dos serviços prestados;</li>
                    <li>As remoções efetuadas entre os dias 01 a 15 de cada mês, serão pagas no dia 20 e as remoções efetuadas entre os dias 16 a 30 de cada mês, serão pagas no dia 05;</li>
                    <li>Para que os pagamentos sejam devidamente efetuados deverá obrigatoriamente utilizar o aplicativo de acionamento “Rei dos Pátios” que será disponibilizado para utilização 
                    de forma gratuita enquanto da prestação dos serviços, incluindo fotos, check list, dados gerais, etc.</li>
                </ol>
                <br/>

            `
        $scope.formaPagamento = $sce.trustAsHtml(htmlString);

    }



    function caracteristicasVeiculoHtml() {
        
        let reboques = $scope.reboquesList
        
        var htmlString = 

            '<p><strong>ITEM 05 – DAS CARACTERÍSTICAS DO VEÍCULO PRESTADOR DO SERVIÇO</strong></p>' +

            '<tbody>' 
                
        htmlString += `<ol type="a">`
        
        reboques.forEach(item => {           
            
            htmlString += 
            
                `<li>Veículo ${item.descricao}, carga ${item.tipo}, ano de fabricação  ${item.ano_fabricacao}, modelo ${item.ano_modelo}, cor ${item.cor}, 
                placa ${item.placa}, chassi ${item.chassi}, combustível ${item.combustivel}, categoria ${item.categoria}, de propriedade de ${item.nome_proprietario}.</li>`
            
        });
        
        htmlString += `</ol>`

        htmlString += '</tbody>'
            //'</table>' +
          //  '<br/>' +
            '<ul>' +          
                '<li>O(s) veículo(s) listado(s) acima, deverá(ão) passar por vistoria prévia efetuada pelo CREDENCIANTE, o qual assinará se o veículo ' +
                    'atende ou não todas as exigências deste termo. Em não havendo atendimento, não será autorizado o credenciamento até que haja a correção.</li>'+
            '</ul>'
        
        $scope.caracteristicasVeiculo = $sce.trustAsHtml(htmlString);

    }



    function acionamentoServicoHtml() {

        var htmlString = 
        `
            <br/>

            <div><strong>ITEM 06 – DA FORMA DE ACIONAMENTO DO CREDENCIADO</strong></div>
                <ol type="a">
                    <li>Acionamentos feitos para remoção de veículos do DER serão feitos por meio do aplicativo “Rei dos Pátios” o qual será disponibilizado de forma 
                    gratuita durante a prestação de serviços. O aplicativo deverá ser mantido online para o recebimento dos chamados de serviços;</li>
                    <li>Acionamento para o resgate dos veículos ANTT e ARTEST é feito pelo 08009709752 (Central de Controle de Operações) e o atendente aciona o guincheiro por meio do aplicativo.</li>
                </ol>
            <br/>

            <div><strong>ITEM 07 – DA FORMA DA PRESTAÇÃO DO SERVIÇO</strong></div>
            <ol type="a">
                <li>Os veículos deverão atender as condições mínimas de potência em relação ao peso rebocado ou carga transportada (art.100 do CTB), devendo apresentar bom estado de funcionamento</li>
                <li>O condutor/operador deverá possuir capacidade técnica para a realização das operações contratadas, devendo ser habilitado na categoria correspondente ao veículo conduzido, 
                    especialmente, quando houver acoplamento de veículos;</li>
                <li>Os serviços serão executados sempre que o credenciado for acionado, por meio do aplicativo “Rei dos Pátios” ou por meio do 08009709752 (Central de Controle de Operações);</li>
                <li>A chegada deverá ocorrer preferencialmente até 45 (quarenta e cinco)  minutos, a contar do instante da solicitação da remoção do veículo até a chegada ao local indicado;</li>
                <li>O deslocamento em direção ao local determinado deverá iniciar imediatamente após a mobilização, com obediência integral às normas de circulação e conduta do CTB, e sem 
                    paradas ou estacionamentos desnecessários durante o percurso, objetivando a chegada no menor tempo possível, com segurança;</li>
                <li>O credenciado deverá providenciar o registro fotográfico digital do veículo, check list e dados gerais, por meio do uso do sistema/aplicativo fornecido, antes de efetuar 
                    o transporte ao depósito, sob pena de não recebimento do serviço realizado. Salvo em casos fortuitos ou de força maior;</li>
                <li>O registro fotográfico deverá conter, no mínimo, uma imagem de cada face externa completa do veículo (frontal, lateral direita, lateral esquerda, traseira, cofre do motor,  
                    painel de instrumentos e parte interna do veículo, e também registrar a imagem de acessórios existentes), evitando-se imagens fracionadas dos elementos citados neste item</li>
                <li>No caso de verificação de avarias, deve haver o registro fotográfico detalhado correspondente;</li>
                <li>A falta dos registros fotográficos, comprovando a preexistência de todas as avarias constantes no veículo anteriormente ao recolhimento e transporte implicará na assunção 
                    do ônus de ressarcimento de toda e qualquer avaria reclamada;</li>
                <li>O veículo guincho será previamente vistoriado, e somente após a sua aprovação é que será aprovado o credenciamento</li>
                <li>Será instalado no veículo guincho que for utilizado para a prestação de serviços um equipamento rastreador, que ao final do credenciamento deverá ser devolvido pelo 
                    credenciado em perfeitas condições, sendo descontado do pagamento auferido no mês o valor de R$ 19,90 (dezenove reais e noventa centavos) por rastreador para custear as 
                    despesas de uso da taxa de dados do equipamento;</li>
                <li>Caso não haja a devolução do equipamento rastreador pelo CREDENCIADO, o mesmo se compromete a efetuar o pagamento de uma multa no valor de R$ 300,00 (trezentos reais);</li>
                <li>Itens indispensáveis do veículo:
            
                    <ul>

                        <li> Conservação do estado geral do veículo;</li>
                        <li> Conservação de Pneus;</li>
                        <li> Iluminação do veiculo;</li>
                        <li> Documento devidamente regularizada do veículo;</li>
                        <li> Tacógrafo aferido;</li>
                        <li> Utilização da logo marca DER, quando for o caso;</li>
                        <li> Uso e manutenção do Giroflex;</li>
                        <li> Uso da faixa Refletiva;</li>
                        <li> Uso de Cone de sinalização;</li>
                        <li> Sinalizador para veículos na asa delta;</li>
                        <li> Cabo de aço;</li>
                        <li> Cinta para amarração dos veículos;</li>
                        <li> Correntes para veículos pesados;</li>
                        <li> Guancheia.</li>
                    </ul>
                </li>
            
            </ol>
            
            <br/>
            <br/>
            <br/>

            <p id="linha" style="text-align: left; font-size: 10px; "> </p>
            <p id="empresa" style="text-align: left; font-size: 10px; "> </p>
            <p id="local" style="text-align: left; font-size: 10px; "> </p>
            <p id="device" style="text-align: left; font-size: 10px; "> </p>
           

        `

        $scope.acionamentoServico = $sce.trustAsHtml(htmlString);


    }


    
    function carregarPaginasHtml() {
        
        setTimeout(function() {
               
            termoCredenciamentoHtml();

            anexoInicioHtml()
            
            gerarValoresHtml();

            formaPagamentoHtml();

            caracteristicasVeiculoHtml();

            acionamentoServicoHtml();
            
            $scope.$apply();

        }, 1000);
    }
  
    


    $scope.anexo = $sce.trustAsHtml(
    `
    <div>
       
        <body> 

            <div> <h3 style="font-size: 24px; margin: 10px 0; text-align: center;"> ANEXO I </h3> </div>
            <br>
            
                <div> <strong>ITEM 01 – DOS DOCUMENTOS NECESSÁRIOS PARA O CREDENCIAMENTO</strong></div>
                    <ol type="a">
                        <li>Cópia do Contrato Social;</li>
                        <li>Cópia de um documento com foto do responsável pela assinatura do termo de credenciamento que conste no contrato social;</li>
                        <li>Cópia do CRLV dos veículos que serão utilizados para a prestação de serviços com licenciamento em dia;</li>
                        <li>Cópia da CNH do responsável pela direção do veículo guincho constando de sua devida categoria.</li>
                    </ol>
                <br>
                
                <div><strong>ITEM 02 – DO LOCAL DA PRESTAÇÃO DO SERVIÇO</strong></div>
                    <ol type="a">    
                        <li>Recolhimento do veículo na Região da Zona Leste da Capital de São Paulo para: Operação SP, Artesp, Antt e DER.</li>
                    </ol>
                <br>
                
                <div><strong>ITEM 03 – DO PRAZO DO CREDENCIAMENTO</strong></div>
                    <ol type="a">
                        <li>12 (doze) meses, a partir da data de credenciamento, podendo ser prorrogado mediante aditivo por escrito, desde que haja intereresse entre as partes, com pelo menos 30 (trinta) dias de antecedencia.</li>
                    </ol>
                <br>
                
                <div><strong>ITEM 04 – DA TABELA DE VALORES DOS SERVIÇOS (DER)</strong></div>
                    <ol type="a">    
                        <li>Valor da remoção por moto: R$ 130,00 (cento e trinta reais) até 40 km rodados;</li>
                        <li>Valor por KM excedente (acima de 40  KM): R$ 1,70 (um real e setenta centavos);</li>
                        <li>Havendo pedágio, a empresa credenciante arcará com o equivalente a 50% (cinquenta por cento) mediante apresentação dos recibos;</li>
                        <li>Valor da remoção por carro: R$ 130,00 (cento e trinta reais) até 40 km rodados;</li>
                        <li>Valor por KM excedente (acima de 40 KM): R$ 1,70 (um real e setenta centavos);</li>
                        <li>Havendo pedágio, a empresa credenciante arcará com o equivalente a 50% (cinquenta por cento) mediante apresentação dos recibos;</li>
                        <li>Valor da remoção por pesado: R$ 350,00 (trezentos e cinquenta reais) até 40 km rodados;</li>
                        <li>Valor por KM excedente (acima de 40 KM): R$ 3,50 (três reais e cinquenta centaos);</li>
                        <li>Havendo pedágio, a empresa credenciante arcará com o equivalente a 50% (cinquenta por cento) mediante apresentação dos recibos.</li>
                    </ol>
                <br>
            
                <div><strong>ITEM 05 – DA TABELA DE VALORES DOS SERVIÇOS OPERAÇÃO SP ZONA LESTE DA CAPITAL DE SÃO PAULO</strong></div>
                    <ol type="a">
                        <li>Valor da remoção por moto: R$ 80,00 (oitenta reais);</li>
                        <li>Valor da remoção por carro: R$ 120,00 (cento e vinte reais);</li>
                        <li>Valor da remoção por caminhão: R$ 350,00 (trezentos e cinquenta reais).</li>
                    </ol>
                <br>
            
                <div><strong>ITEM 06 – DA TABELA DE VALORES DOS SERVIÇOS (RESGATE ANTT e ARTESP)</strong></div>
                    <ol type="a">    
                        <li>Valor do resgate veículos ANTT e ARTESP: R$ 250,00 (duzentos e cinquenta reais) até 40 km rodados;</li>
                        <li>Valor por KM excedente (acima de 40 KM): R$ 3,30 (três reais e trinta centavos);</li>
                        <li>Havendo pedágio, a empresa credenciante arcará com o equivalente a 50% (cinquenta por cento) mediante apresentação dos recibos.</li>
                    </ol>
                <br>

                <div><strong>ITEM 07 – DA FORMA DE PAGAMENTO</strong></div>
                    <ol type="a"> 
                        <li>O pagamento será realizado mediante apresentação de nota fiscal dos serviços prestados;</li>
                        <li>As remoções efetuadas entre os dias 01 a 15 de cada mês, serão pagas no dia 20 e as remoções efetuadas entre os dias 16 a 30 de cada mês, serão pagas no dia 05;</li>
                        <li>Para que os pagamentos sejam devidamente efetuados deverá obrigatoriamente utilizar o aplicativo de acionamento “Rei dos Pátios” que será disponibilizado para utilização de forma gratuita enquanto da prestação dos serviços, incluindo fotos, check list, dados gerais, etc.</li>
                    </ol>
                <br>

                <div><strong>ITEM 08 – DAS CARACTERÍSTICAS DO VEÍCULO PRESTADOR DO SERVIÇO</strong></div>
                    <ol type="a">
                        <li>Veículo Ford / Cargo 815 E, Carga Caminhão, ano fabricação 2006, modelo 2006, cor branca, de placas DPE4D36, CHASSI 9BFVCE1N26BB69138, diesel, categoria aluguel de propriedade de Jeferson Ribeiro da Silva;</li>
                        <li>O veículo objeto da alinea “a” deverá passar por vistoria prévia efetuada pelo CREDENCIANTE, o qual assinará se o veículo atende ou não todas as exigências deste termo. Em não havendo atendimento, não será autorizado o credenciamento até que haja a correção.</li>
                    </ol>
                <br>
            
                <div><strong>ITEM 09 – DA FORMA DE ACIONAMENTO DO CREDENCIADO</strong></div>
                    <ol type="a">
                        <li>Acionamentos feitos para remoção de veículos do DER serão feitos por meio do aplicativo “Rei dos Pátios” o qual será disponibilizado de forma gratuita durante a prestação de serviços. O aplicativo deverá ser mantido online para o recebimento dos chamados de serviços;</li>
                        <li>Acionamento para o resgate dos veículos ANTT e ARTEST é feito pelo 08009709752 (Central de Controle de Operações) e o atendente aciona o guincheiro por meio do aplicativo.</li>
                    </ol>
                <br>

                <div><strong>ITEM 10 – DA FORMA DA PRESTAÇÃO DO SERVIÇO</strong></div>
                <ol type="a">
                    <li>Os veículos deverão atender as condições mínimas de potência em relação ao peso rebocado ou carga transportada (art.100 do CTB), devendo apresentar bom estado de funcionamento</li>
                    <li>O condutor/operador deverá possuir capacidade técnica para a realização das operações contratadas, devendo ser habilitado na categoria correspondente ao veículo conduzido, especialmente, quando houver acoplamento de veículos;</li>
                    <li>Os serviços serão executados sempre que o credenciado for acionado, por meio do aplicativo “Rei dos Pátios” ou por meio do 08009709752 (Central de Controle de Operações);</li>
                    <li>A chegada deverá ocorrer preferencialmente até 45 (quarenta e cinco)  minutos, a contar do instante da solicitação da remoção do veículo até a chegada ao local indicado;</li>
                    <li>O deslocamento em direção ao local determinado deverá iniciar imediatamente após a mobilização, com obediência integral às normas de circulação e conduta do CTB, e sem paradas ou estacionamentos desnecessários durante o percurso, objetivando a chegada no menor tempo possível, com segurança;</li>
                    <li>O credenciado deverá providenciar o registro fotográfico digital do veículo, check list e dados gerais, por meio do uso do sistema/aplicativo fornecido , antes de efetuar o transporte ao depósito, sob pena de não recebimento do serviço realizado. Salvo em casos fortuitos ou de força maior;</li>
                    <li>O registro fotográfico deverá conter, no mínimo, uma imagem de cada face externa completa do veículo (frontal, lateral direita, lateral esquerda, traseira, cofre do motor,  painel de instrumentos e parte interna do veículo, e também registrar a imagem de acessórios existentes), evitando-se imagens fracionadas dos elementos citados neste item</li>
                    <li>No caso de verificação de avarias, deve haver o registro fotográfico detalhado correspondente;</li>
                    <li>A falta dos registros fotográficos, comprovando a preexistência de todas as avarias constantes no veículo anteriormente ao recolhimento e transporte implicará na assunção do ônus de ressarcimento de toda e qualquer avaria reclamada;</li>
                    <li>O veículo guincho será previamente vistoriado, e somente após a sua aprovação é que será aprovado o credenciamento</li>
                    <li>Será instalado no veículo guincho que for utilizado para a prestação de serviços um equipamento rastreador, que ao final do credenciamento deverá ser devolvido pelo credenciado em perfeitas condições, sendo descontado do pagamente auferido no mês o valor de R$ 19,90 (dezenove reais e noventa centavos) por rastreador para custear as despeas de uso da taxa de dados do equipamento;</li>
                    <li>Caso não haja a devolução do equipamento rastreador pelo CREDENCIADO, o mesmo se compromete a efetuar o pagamento de uma multa no valor de R$ 300,00 (trezentos reais);</li>
                    <li>Itens indispensáveis do veículo:</li>
                </ol>

                <ul >

                    <li> Conservação do estado geral do veículo;</li>
                    <li> Conservação de Pneus;</li>
                    <li> Iluminação do veiculo;</li>
                    <li> Documento devidamente regularizada do veículo;</li>
                    <li> Tacógrafo aferido;</li>
                    <li> Utilização da Logo marca DER, quando for o caso;</li>
                    <li> Uso e manutenção do Giro flex;</li>
                    <li> Uso da faixa Refletiva;</li>
                    <li> Uso de Cone de sinalização;</li>
                    <li> Sinalizador para veículos na asa delta;</li>
                    <li> Cabo de aço;</li>
                    <li> Cinta para amarração dos veículos;</li>
                    <li> Correntes para veículos pesados;</li>
                    <li> Guancheia.</li>
                </ul>

            </br>

        </body>
    
    </div>

    `);



    $scope.adicionaisHtml_1 =
    
    ` 
    
    <!DOCTYPE html>
    <html lang="en" dir="ltr">
      <head>
        <meta charset="utf-8">
        <title>Grupo Carvalho - Termo de Credenciamento</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      </head>
      <body onselectstart="return false" style="text-align: justify">
      
        <div class="d-flex justify-content-center">
          <div class="col-md-8" style="margin-top: 2%">
           
            <div class="col-md-10">
                <div id="geraPDF" style="text-align: justify">
    
    
    
    `

    $scope.adicionaisHtml_2 =

    `
                   
                </div>               
    
                <br>
                  
                <div class="col-md-10 d-flex justify-content-left ">
                <h3>Assine no campo abaixo</h3>
                </div>
    
    
                <div class="col-md-12 d-flex justify-content-left ">
                    <canvas id="canvas" height="300" width="700" style="border: 1px solid black"></canvas>
                </div>

                <div class="col-md-12 d-flex justify-content-left ">
                <button type="button" style="margin-top: 5%; margin-bottom: 5%; margin-left: 0%; width: 250px; height: 80px; font-size: 24px;" class="btn btn-dark" data-action="action-enviar">Enviar Documento</button>
                <button type="button" style="margin-top: 5%; margin-bottom: 5%; margin-left: 5%; width: 200px; height: 80px; font-size: 24px;" class="btn btn-dark" data-action="action-limpar">Limpar</button>
                <button type="button" style="margin-top: 5%; margin-bottom: 5%; margin-left: 5%; width: 200px; height: 80px; font-size: 24px;" class="btn btn-dark" data-action="action-pdf">Gerar PDF</button>
                
                </div>
          </div>
        </div>
      </div>
      
      
    </body>
  </html>
  
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@4.0.1/dist/signature_pad.umd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.1/jspdf.umd.min.js"></script>
    <script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script> 
  
  
    <script>
  
    var canvas = document.getElementById("canvas");
    var enviarBtn = document.querySelector("[data-action=action-enviar]");
    var limparBtn = document.querySelector("[data-action=action-limpar]");
    var createPDFBtn = document.querySelector("[data-action=action-pdf]");
    
       
    var ourPad = new SignaturePad(canvas, {
      backgroundColor: 'rgb(255, 255, 255, 0)'
      
    });
    
    canvas.getContext("2d")
    
    function processImage() {
      canvas.toBlob(function (blob) {
        var targetImg = document.querySelector('img'),
          url = URL.createObjectURL(blob);
        targetImg.src = url;
      });
    
    }

    var dadosLocais

    $.getJSON('https://json.geoiplookup.io/?callback=?', function(data) {
      
      dadosLocais = data
      
    })
    
    
    function getIP() {
        
        toastr.info("Processando informações! Aguarde...");  
  
        const camposObjeto = ['ip', 'latitude', 'longitude', 'city', 'hostname', 'region', 'device_data'];
  
        const dataToSend = {};
  
        for (let chave in dadosLocais) {
            if (camposObjeto.includes(chave)) {
              dataToSend[chave] = dadosLocais[chave];
            }
        }
          
        dataToSend['device_data'] = navigator.userAgent
  
        var idValue = document.getElementById("emp-id").textContent;
        dataToSend.id = parseInt(idValue)
          
        let nomeArquivo = window.location.pathname.split('/').pop()
        nomeArquivo = nomeArquivo.replace('.html', '.pdf')
        dataToSend['arquivo'] = nomeArquivo         
          
        fetch('https://app-termo-gateway-15a59c9f5568.herokuapp.com/dados', {
          
            method: 'POST',
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify(dataToSend)
            })
            .then(response => response)
            .then(data => {
              toastr.success("Processamento concluido!");
            })
            .catch(error => {
              
        })
         
    }


    function adicionarRodape () {

        const textoRepresentante = document.querySelector('#representante').innerText;
        const textoProcurado = 'Representante legal:';
        const posicaoInicio = textoRepresentante.indexOf(textoProcurado) + textoProcurado.length;
        const nomeRepresentante = textoRepresentante.substring(posicaoInicio).trim();   
            
        const dataHoraAtual = new Date();
        const opcoesData = { day: 'numeric', month: 'long', year: 'numeric' };
        const opcoesHora = { hour: '2-digit', minute: '2-digit', second: '2-digit' };
        const dataFormatada = dataHoraAtual.toLocaleDateString('pt-BR', opcoesData);
        const horaFormatada = dataHoraAtual.toLocaleTimeString('pt-BR', opcoesHora);

        const linha = '<hr noshade="”noshade”" width="100%" size="2px">'
        document.getElementById("linha").innerHTML = linha;

        const representante = 'Assinado por ' + nomeRepresentante + ', no dia ' + dataFormatada + ', as ' + horaFormatada
        document.getElementById("empresa").innerHTML = representante;

        const local = 'por meio do IP ' + dadosLocais.ip + ', na cidade de ' + dadosLocais.city + ' - ' + dadosLocais.region
        document.getElementById("local").innerHTML = local;

        const device = 'Dispositivo: ' + navigator.userAgent
        document.getElementById("device").innerHTML = device;
        
    } 




    function enviarArquivoPdf (){

      toastr.info("Preparando documento para ser enviado! Aguarde...");

      const div = document.getElementById('geraPDF')

        const options = {
            margin: 10,
            filename: 'arquivo.pdf',
            image: { type: 'jpeg', quality: 1 },
            html2canvas: {
              dpi: 192,
                scale: 2,
                letterRendering: true,
                useCORS: true
            },
          jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' }
        }
   
        let nomeArquivo = window.location.pathname.split('/').pop()
        
        nomeArquivo = nomeArquivo.replace('.html', '')

        adicionarRodape()
        
        const conteudoHTML = document.getElementById('geraPDF')

        html2pdf().set(options).from(conteudoHTML).toPdf().get('pdf').then(function(pdfObj) {
      
        const perBlob = pdfObj.output('blob');
        var formData = new FormData();
        formData.append('arquivo', perBlob, nomeArquivo + '.pdf');

          fetch('https://app-termo-gateway-15a59c9f5568.herokuapp.com/pdf/', {
            method: 'POST',
            body: formData
          }).then(function(response) {
          if (response.ok) {
    
            toastr.success("Documento enviado com sucesso!");

            getIP() 

          } else {
              
            toastr.error("Erro ao enviar o arquivo! Tente novamente e caso o erro continue, contate a empresa");
            }
          })
          .catch(function(error) {
            toastr.error('Erro ao enviar o arquivo PDF:', error);
          });
        });

    }

   
    
    enviarBtn.addEventListener("click", function (event) {
      if (ourPad.isEmpty()) {
        toastr.warning("Antes de enviar o documento, precisa ter uma assinatura no campo 'Assine no campo abaixo'!");
      } else {
        
        processImage();

        setTimeout(enviarArquivoPdf, 1000);
          
      }
    
    });
    


    limparBtn.addEventListener("click", function (event) {
      if (ourPad.isEmpty()) {
        toastr.warning("Não foi encontrado uma assinatura para ser apagada!");
      } else {
        ourPad.clear();
      }
    });


        
    createPDFBtn.addEventListener("click", function (event) {
       let valor = document.getElementById('signature')
       let getAttr = signature.getAttribute('src')
      
      if (!getAttr){
        
        toastr.warning("Antes de Gerar o arquivo PDF, é necessário assinar e enviar o documento!");
    
        return

      }

      const div = document.getElementById('geraPDF')

      const options = {
            margin: 10,
            filename: 'arquivo.pdf',
            image: { type: 'jpeg', quality: 1 },
            html2canvas: {
                dpi: 192,
                scale: 2,
                letterRendering: true,
                useCORS: true
            },
            jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' }
        }

      html2pdf().set(options).from(div).save()

    })
    
    </script> 
    
    `


    $scope.gerarPDF = function (){
    
        const div = document.getElementById('termo')

        const options = {
            margin: 10,
            filename: 'termo-credenciamento.pdf',
            image: { type: 'jpeg', quality: 1 },
            html2canvas: {
                dpi: 192,
                scale: 2,
                letterRendering: true,
                useCORS: true
            },
            jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' }
        }

        html2pdf().set(options).from(div).save();

    }


  
    $scope.gerarHTML = function() {
       
        if ($scope.empresaReboque[0].rep_telefone == null){

            myFunctions.showAlert("Telefone do representante legal da empresa " + $scope.empresaReboque[0].nome + " está inválido! Revise cadastro!")

            return
        }
       
        let codigoPais = "55"
        let telefone = codigoPais + $scope.empresaReboque[0].rep_telefone.replace(/[^0-9]/g, "")
       // let telefone = '5551999641672' //telefone para testes
        
        if (telefone.length == 13){            
                
            var divs = document.querySelectorAll('div[ng-bind-html]')
            var htmlString = ''

            htmlString += $scope.adicionaisHtml_1

            for (var i = 0; i < divs.length; i++) {
                htmlString += divs[i].innerHTML
            }

            htmlString += $scope.adicionaisHtml_2

            var blob = new Blob([htmlString], { type: 'text/html' })
            
            function gerarNomeArquivoAleatorio() {
                const caracteres = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
                const comprimento = 8
            
                let nomeArquivo = ''
                for (let i = 0; i < comprimento; i++) {
                nomeArquivo += caracteres.charAt(Math.floor(Math.random() * caracteres.length))
                }
            
                return nomeArquivo
            }

            let arquivo = gerarNomeArquivoAleatorio()          
            let arquivoHtml = arquivo + '.html'
            let arquivoPdf = arquivo + '.pdf'

            var formData = new FormData();
            formData.append('htmlFile', blob, arquivoHtml); 

            $http.post('https://app-termo-gateway-15a59c9f5568.herokuapp.com/upload/', formData, {  
             //   $http.post('http://192.168.0.195:3000/upload/', formData, { 
                headers: { 'Content-Type': undefined }
            }).then(function(response) {

                let dados = {
                    id_motorista: $scope.dados.id,
                    id_empresa_reboque: $scope.identificador,
                    arquivo: arquivoPdf,
                    id_usuario_enviou: localStorage.getItem('id_usuario')
                }

                $http.post(urlServidor.urlServidorChatAdmin+'/empresas_reboques/termo-credenciamento-cadastrar', dados).then( function(response){
            
                    if (response.data.code){
                    
                        myFunctions.showAlert('Erro na gravação!')
                    }
                })
            
                let server = "https://app-termo-gateway-15a59c9f5568.herokuapp.com/html/"     
                let texto = 'Clique no link para abrir e assinar o contrato: ' + server + arquivoHtml 
                let url_link = `https://api.whatsapp.com/send?phone=${encodeURIComponent(telefone)}&text=${encodeURIComponent(texto)}`
                    
                window.open(url_link)

                }).catch(function(error) {
                
                myFunctions.showAlert( `Erro ao enviar arquivo para o servidor! Tente novamente e caso o erro continue, entre em contato com o suporte.` );
                
            });   
        
        }else{
            
            myFunctions.showAlertCustom("O número de telefone cadastrado do representante legal desta empresa de reboque, não está correto! <br/><br/>"
            + "Telefone cadastrado: " + $scope.empresaReboque[0].telefone + ". Padrão normalmente utilizado:(XX)XXXXX-XXXX. <br/><br/>"
            + " Revise o cadastro do representante da empresa: " + $scope.empresaReboque[0].nome) 

        }  

    }

})