'use strict';
angular.module('app').controller("empresasNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions,estados,viaCep,$sce, urlImagens,buckets) {  

  $scope.dados = {
    nome_empresa: '',
    nome_fantasia: '',
    endereco: '',
    bairro: '',
    cidade: '',
    cep: '',
    uf: '',
    cnpj: '',
    repres_nome: '',
    repres_cpf: '',
    repres_rg: '',
    repres_telefone: '',
    repres_email: '',
    repres_estado_civil: '',
    repres_profissao: '',
    repres_nacionalidade: '',
    repres_cep: '',
    repres_endereco: '',
    repres_bairro: '',
    repres_cidade: '',
    repres_uf: ''
  }
 
  
  $scope.estados = estados.uf ;
 
  var urlParams = $location.search();
 
  
  $scope.buscaCep = (cep) => {

    if(cep.length == 9){

      viaCep.get(cep).then(function(response){
     
        $scope.dados.endereco = response.logradouro.toUpperCase() ;
        $scope.dados.bairro = response.bairro.toUpperCase() ;
        $scope.dados.cidade = response.localidade.toUpperCase() ;
        $scope.dados.uf = response.uf.toUpperCase() ;				 

      });

    }	

  }


  $scope.buscaCepRepres = (repres_cep) => {

    if(repres_cep.length == 9){

      viaCep.get(repres_cep).then(function(response){
      
        $scope.dados.repres_endereco = response.logradouro.toUpperCase() ;
        $scope.dados.repres_bairro = response.bairro.toUpperCase() ;
        $scope.dados.repres_cidade = response.localidade.toUpperCase() ;
        $scope.dados.repres_uf = response.uf.toUpperCase() ;				 

      });

    }	

  }

 

  $scope.isDisabled = false

  $scope.gravar = function() {        

    $http.post(urlServidor.urlServidorChatAdmin+'/empresas/cadastrar', $scope.dados ).then( function(response){

      if (response.data.code){
           
        myFunctions.showAlert('Erro na gravação!')

      }else{

        if ( response.data == '11000' )                 
        {
          myFunctions.showAlert('Empresa já existe!')
        }else{                    

          myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
          $scope.isDisabled = true ;     

        }  
               
      }; 

    }); 

  }
    
});