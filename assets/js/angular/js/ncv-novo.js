'use strict';
angular.module('app')
.controller("ncvNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions,$mdDialog, estados) {  

  $scope.dados = {
     id_patio:'',
     chaves:'',
     tabelaTarifas:'',
     tipo_veiculo: '' ,
     placa: '' ,
     estado_veiculo: '' ,
     veiculo_trancado: '' ,
     marca_modelo: '' ,
     ano: '',
     cor: '',
     chassi: '',
     numero_motor: '',
     placa_uf: '',
     placa_municip: '',
     idempresa: localStorage.getItem('id_empresa'),
     nomeUsuario: localStorage.getItem('nome_usuario'),
     cb_adulterado: false ,
     cb_crimestransito : false ,
     cb_emtela : false ,
     cb_foracirculacao : false ,
     cb_judicial : false ,
     cb_leasing : false ,
     cb_motorqueixa : false ,
     cb_pedirbaixa : false ,
     cb_policialcivil : false ,
     cb_reversivelanatel : false ,
     cb_roubofurto : false ,
     cb_semdocumentocrv : false ,
     cb_traficodedrogas : false 

  }

  $http.get(urlServidor.urlServidorChatAdmin+'/tipos_veiculo/listar', { params: { idEmpresa:  $scope.dados.idempresa } } ).then( function(response){
		$scope.tiposVeiculos = response.data ;
	});

  $http.get(urlServidor.urlServidorChatAdmin+'/tarifas/itens-tarifas', { params: { idEmpresa: localStorage.getItem('id_empresa') } }).then( function(response){						
    $scope.tabelaTarifas = response.data ;
  });

  buscaPatios(localStorage.getItem('id_usuario'),localStorage.getItem('id_empresa')).then( function(result){
     $scope.patios = result ;
  });  

  $scope.estados = estados.uf;

  $scope.isDisabled = false

  $scope.buscaPlaca = function(placa) {  

    $http.get(urlServidor.urlServidorChatAdmin+'/busca-placa', { params: { placa: placa } }).then( function(response){						

      var texto = '<h2>Marca/Modelo: '+response.data.modelo+'</h2><h2>Ano: '+response.data.ano+'</h2>'
      texto = texto + '<h2>Cor: '+response.data.cor+'</h2><h2>Municipio: '+response.data.municipio+'</h2>' 
      texto = texto + '<h2>Motor: '+response.data.motor+'</h2>'

      let confirm = $mdDialog.confirm()
      .title('Dados de Veiculo!')
  //    .textContent(texto )
      .htmlContent(texto)
      .ariaLabel('Lucky day')
      .ok('Gravar')
      .cancel('Não Gravar');

      $mdDialog.show(confirm).then(function() {

        $scope.dados.ano = response.data.ano ;
        $scope.dados.marca_modelo = response.data.modelo ;
        $scope.dados.cor = response.data.cor ;
        $scope.dados.placa_municipio = response.data.municipio;
        $scope.dados.chassi = response.data.chassi;
        $scope.dados.numero_motor = response.data.motor;

      }, function() {
        $scope.dados.ano = '' ;
        $scope.dados.marca_modelo = '' ;
        $scope.dados.cor = '' ;
        $scope.dados.placa_municipio = response.data.municipio;
        $scope.dados.chassi = '';
        $scope.dados.numero_motor = '';
        
      })   


    }, function errorCallback(response) {

      myFunctions.showAlert( "Servidor Sinesp retornando erro , tente novamente ou digite manualmente!" )

    })

    
    ;   

  }  

  $scope.gravar = function() {        

    $http.post(urlServidor.urlServidorChatAdmin+'/ncv/cadastrar-ncv ', $scope.dados ).then( function(response){

        if (response.data.code){
            
          myFunctions.showAlert('Erro na gravação!')
          
        }else{

          if ( response.data == '11000' )                 
          {
            myFunctions.showAlert('NCV já existe!')
          }else
          {                    

            myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
            $scope.isDisabled = true ;            

          }  
                
        }; 

    }); 

  }; 

  
  function buscaPatios(idUsuario,idEmpresa){

    return new Promise( function( resolve,reject){

    $http.put(urlServidor.urlServidorChatAdmin+'/patios/listar-patiosusuario',{ idUsuario: idUsuario , idEmpresa : idEmpresa  }).then( function(response){

      let dados = response.data ;

      dados.sort(function(a, b){
        if (a.nome < b.nome) {
        return 1;
        }
        if (a.nome > b.nome) {
        return -1;
        }        
        return 0;
      });       

      resolve(dados) ;

    });

    })

  }
    
});