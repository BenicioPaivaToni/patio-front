
angular.module('app')
.controller('pagarCancelarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep,$mdDialog) {

    const queryString = window.location.hash;

    var _id =  queryString.slice(queryString.indexOf("%") + 3); 

$scope.$on('$viewContentLoaded', function () {

    buscaDados().then(function (result) {
        $scope.dados = result;
    });
    

    buscaFornecedores().then(function (result) {
        $scope.fornecedores = result;
    });
    buscaCategorias().then(function (result) {
        $scope.categorias = result;
    });

});

//----------

    $scope.showConfirm = function(dados_pagar,ev) {
        var confirm = $mdDialog.confirm()
            .title('Cancelar Pagamento?')
            .textContent('')
            .ariaLabel('Lucky day')
            .targetEvent(ev)
            .ok('Sim')
            .cancel('Não');
    
        $mdDialog.show(confirm).then(function () {

            $http.post(urlServidor.urlServidorChatAdmin + '/pagar/cancela', dados_pagar).then(function (response) {
                if (response.data.code) {
                    myFunctions.showAlert('Erro na gravação!');
                } else {
                    myFunctions.showAlert('Cancelamento executado com sucesso!')
                    $scope.isDisabled = true;
                    var alt = dados_pagar;
                    var json = Object.assign({}, alt);
                    $scope.logs = {
                        id: _id, 
                        operacao: 'CANCELAR',
                        tipo: 'CANCELAR PAGAMENTO',
                        id_usuario: localStorage.getItem('id_usuario'),
                        alteracoes: JSON.stringify(json),
                        anterior: null
    
                    };
                    $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro',$scope.logs).then(function (response) {
                        if (response.data.code) {
                            $scope.erro = response;
            
                        } 
                    });
                };
            });

        }, function () {

            $scope.isDisabled = false;

        });
    };    
        
    

    function buscaFornecedores() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/fornecedores/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }

    function buscaDados() {

        return new Promise(function (resolve, reject) {   

            $http.get(urlServidor.urlServidorChatAdmin + '/pagar/busca-id',{params: {id_pagar: _id}}).then(function (response) {
                resolve(response.data[0]); 
            });
            
        })

    }

    
    $scope.fechar = function () {
        window.close();
    }

    function buscaCategorias() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/categorias/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }


});
