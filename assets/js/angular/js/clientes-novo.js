'use strict';
angular.module('app')
.controller("clientesNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions,estados,viaCep) {  

  $scope.dados = {
     nome:'',
     cpf_cnpj: '' ,
     endereco: '' ,
     bairro: '' ,
     cidade: '' ,
     cep: '' ,
     uf: '' ,
     email: '' ,
     telefone: '',
     status: '' ,
     idempresa: localStorage.getItem('id_empresa')
  }

  $scope.estados = estados.uf ;

  $scope.buscaCep = (cep) => {

		if(cep.length == 9){

			viaCep.get(cep).then(function(response){
			
				$scope.dados.endereco = response.logradouro.toUpperCase() ;
				$scope.dados.bairro = response.bairro.toUpperCase() ;
				$scope.dados.cidade = response.localidade.toUpperCase() ;
				$scope.dados.uf = response.uf.toUpperCase() ;				 

      });

		}	

	}  

  $scope.isDisabled = false

  $scope.gravar = function() {        

    $http.post(urlServidor.urlServidorChatAdmin+'/clientes/cadastrar', $scope.dados ).then( function(response){

        if (response.data.code){
            
          myFunctions.showAlert('Erro na gravação!')

        }else{

          if ( response.data == '11000' )                 
          {
            myFunctions.showAlert('Cliente já existe!')
          }else
          {                    

            myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
            $scope.isDisabled = true ;            

          }  
                
        }; 

    }); 

  }; 
  
    
});