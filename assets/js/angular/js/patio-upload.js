angular.module('app').directive('patioUpload', function ($http, urlServidor) {
	const tipos = ['CONTRATO', 'SEGURO', 'VISTORIA', 'DETETIZAÇÃO']
	return {
		restrict: 'E',
		templateUrl: 'patio-uploads.html',
		scope: {
			uploaded: '&',
			updateFunction: '=',
			patio: '='
		},
		link: function (scope, element) {
			const baseUrl = urlServidor.urlServidorChatAdmin + '/patios/documentos'
			const patio = scope.patio

			scope.tipos = tipos

			scope.getFile = function () {
				const input = element.find('#inputFile')
				let fileName = input.val()
				if (fileName) {
					fileName = fileName.toLowerCase().replace('c:\\fakepath\\', '')
					if (fileName.indexOf('.pdf') !== -1) {
						// const name = fileName.split('.')[0]
						const file = input[0].files[0]
						return file
					} else return 'Invalid file'
				} else return 'No file selected'
			}

			scope.resetFields = function () {
				angular.copy(scope.documento)
				angular.element('#inputFile').val(null)

				scope.tipo = null
				scope.data_inicio = null
				scope.data_fim = null
				scope.observacao = null
				scope.dias = null
			}
			scope.getFields = function () {
				const { tipo, data_inicio: dtIN, data_fim: dtFN, observacao , dias } = scope
				const data_inicio = dtIN ? dtIN.toString() : null
				const data_fim = dtFN ? dtFN.toString() : null
				const documento = scope.getFile() || {}
				return {
					tipo,
					data_inicio,
					data_fim,
					observacao,
					dias,
					documento
				}
			}

			scope.getFormData = function (data) {
				var fd = new FormData()
				Object.keys(data).map((key) => {
					const value = data[key]
					if (value && key !== 'documento') {
						fd.append(key, value)
					}
				})
				const doc = data['documento']
				fd.append('documento', doc, doc.name)
				return fd
			}

			scope.checkEnviarArquivo = function () {
				const { tipo, documento } = scope.getFields()
				const { enviouForm } = scope
				return !(!enviouForm && tipo && documento.name)
			}

			scope.uploadFile = function () {
				scope.enviouForm = true
				const fields = scope.getFields()
				const { documento } = fields
				if (documento.name) {
					const form = scope.getFormData(fields)
					return new Promise(function () {
						var url = `${baseUrl}/${patio}`
						$http
							.post(url, form, {
								transformRequest: angular.identity,
								headers: { 'Content-Type': undefined }
							})
							.then(function (response) {
								scope.enviouForm = false
								scope.resetFields()
								scope.updateFunction()
							})
							.catch((error) => {
								scope.enviouForm = false
							})
					})
				} else {
					scope.enviouForm = false
				}
			}
		}
	}
})
