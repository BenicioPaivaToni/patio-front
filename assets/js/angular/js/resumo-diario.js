angular.module('app')
.controller('resumoDiarioCtrl', function ($scope, $mdDialog, $http, urlServidor, myFunctions) {

    $scope.dados = {
        data: null,
        patio: null
    }
  
    $scope.cancelar = function() {
        $mdDialog.cancel();
      };
   

    $scope.limpaPesquisa = function (ev) {
       $scope.dados.patio = null;
       $scope.dados.data = null;
        
    }

    $scope.tipousuario = localStorage.getItem('tipo_usuario')
   
    $scope.idUsuario = localStorage.getItem('id_usuario')
    
    $scope.patios = '';

    dataPesquisa()

 
    buscaPatios().then(function (result) {
      $scope.patios = result;
    });


$scope.buscaInfosGrafico = async () => {
        
    await buscaDadosGraficos().then(function (result) {
  
        const resultArrayTmp = [];
    
            for(const itemObject in result) {
                for(const itemArray of result[itemObject][0]) {
                    const patioArrayIndex = resultArrayTmp.findIndex((x) => x.patio === itemArray.patio);
            
                    const objectTmp = {}
            
                    for(const resultItem in itemArray) {
                        const field = resultItem.indexOf(itemObject) === -1 && resultItem !== 'patio' ? `${itemObject}_${resultItem}` : resultItem
                        objectTmp[field] = itemArray[resultItem];
                    }
            
                        patioArrayIndex !== -1 ? resultArrayTmp[patioArrayIndex] = {...objectTmp, ...resultArrayTmp[patioArrayIndex]} : resultArrayTmp.push(objectTmp)
            
                }
            }
                          
           
            const listaDados = resultArrayTmp.map((index) => {
              return {
                nomePatio: index.patio,
                apreensoesDia: index.apreensoes_dia,
                apreensoesAcumulado: index.apreensoes_acumulado,
                liberacoesDia: index.liberacoes_dia,
                liberacoesAcumulado: index.liberacoes_acumulado,
                apreensoesMeta: index.apreensoes_acumulado_meta,
                liberacoesMeta: index.liberacoes_acumulado_meta,
                data: String($scope.dados.data.getDate()).padStart(2, "0"),
                };
            });
            
        $scope.listaDados = listaDados

        var dadosGraficoApreensoes = [['Patio', 'Apreensoes', {role: 'annotation'}, 'Acumulado', {role: 'annotation'}, 'Meta', {role: 'annotation'}]];
            for(const item of listaDados) {
            let resultApreensoes = [item.nomePatio, item.apreensoesDia, item.apreensoesDia, item.apreensoesAcumulado, item.apreensoesAcumulado, item.apreensoesMeta, item.apreensoesMeta]
            if(item.apreensoesDia && item.apreensoesAcumulado) {
                dadosGraficoApreensoes.push(resultApreensoes)
            }

        }
    
        $scope.dadosGraficoApreensoes = dadosGraficoApreensoes 
        
  
        var dadosGraficoLiberacoes = [['Patio', 'Liberacoes', {role: 'annotation'}, 'Acumulado', {role: 'annotation'}, 'Meta', {role: 'annotation'}]];
        for(const item of listaDados) {
            let resultLiberacoes = [item.nomePatio, item.liberacoesDia, item.liberacoesDia, item.liberacoesAcumulado, item.liberacoesAcumulado, item.liberacoesMeta, item.liberacoesMeta]
            if(item.liberacoesDia && item.liberacoesAcumulado) {
                dadosGraficoLiberacoes.push(resultLiberacoes)
            }
                      
        }
            
        $scope.dadosGraficoLiberacoes = dadosGraficoLiberacoes   
          
      
        var dadosGraficoApreensoesAcumulado = [['Patio', 'Apreenssoes Acumulado']];
        for(const item of listaDados) {
            let resultApreensoesAcumulado = [item.nomePatio, item.apreensoesAcumulado]
            if(item.apreensoesAcumulado) {
                dadosGraficoApreensoesAcumulado.push(resultApreensoesAcumulado)
            }
        }
           
        $scope.dadosGraficoApreensoesAcumulado = dadosGraficoApreensoesAcumulado           
          
        var dadosGraficoLiberacoesAcumulado = [['Patio', 'Liberacoes Acumulado']];
        for(const item of listaDados) {
            let resultLiberacoesAcumulado = [item.nomePatio, item.liberacoesAcumulado]
            if(item.apreensoesAcumulado) {
                dadosGraficoLiberacoesAcumulado.push(resultLiberacoesAcumulado)
            }

        }
        
        $scope.dadosGraficoLiberacoesAcumulado = dadosGraficoLiberacoesAcumulado  
        
        
        totalMovimentacoes();

    })


    $scope.imagemGraficoApreensoes = null
    $scope.imagemGraficoLiberacoes = null 
    $scope.imagemGraficoApreensoesAcumulado = null
    $scope.imagemGraficoLiberacoesAcumulado = null
                 
    graficoApreensoes()
    graficoLiberacoes()
    graficoApreensoesAcumulado()
    graficoLiberacoesAcumulado()
    
};




function graficoApreensoes(){
       
    let alturaGrafico = ''
    
        if ($scope.dadosGraficoApreensoes.length > 11){
            alturaGrafico = 1600
        }else{
            alturaGrafico = 800
        }

    let grafico = document.getElementById('chart_apreensoes');
            
        while (grafico.hasChildNodes()) {
            grafico.removeChild(grafico.lastChild);
            }  

            if ($scope.dadosGraficoApreensoes.length === 1){

                myFunctions.showAlert('Sem dados de APREENSÕES!')  
                
                }else{

                    google.charts.load('current', {'packages':['corechart']});
                    google.setOnLoadCallback(drawChart);
    
                    function drawChart() {
                        var data = google.visualization.arrayToDataTable($scope.dadosGraficoApreensoes);
                        
                        var options = {
                          title:
                            "APREENSÕES - " +
                            String($scope.dados.data.getDate()).padStart(2, "0") + " de " + 
                            $scope.dados.data.toLocaleString("default", {month: "long"}) 
                            + " de " + $scope.dados.data.getFullYear(),
                          titleTextStyle: { color: "#000000", fontName: "arial", fontSize: 16, bold: true, italic: false},
                          legend: { position: "top", maxLines: 1, alignment: "end"},
                          animation: { duration: 3000, easing: "out", startup: true},
                          width: 1300,
                          height: alturaGrafico,
                          fontSize: 12,
                          chartArea: { width: "80%", height: "90%" },
                          seriesType: "bars",
                          series: { 3: { type: "line" } },
                          backgroundColor: "transparent",
                          annotations: { textStyle: { color: "black", fontSize: 10 },
                          }
                        };

                var chart = new google.visualization.BarChart(document.getElementById('chart_apreensoes'));
                google.visualization.events.addListener(chart, 'error', function (googleError) {
                google.visualization.errors.removeError(googleError.id);
                });
                
                google.visualization.events.addListener(chart, 'ready', function () {
                $scope.imagemGraficoApreensoes = chart.getImageURI();
                });

            chart.draw(data, options);
   
        } 
    }
        
}



function graficoLiberacoes() {

    let alturaGrafico = ''
    
        if ($scope.dadosGraficoLiberacoes.length > 11){
            alturaGrafico = 1600
        }else{
            alturaGrafico = 800
        }
   
    let grafico = document.getElementById('chart_liberacoes');
        while (grafico.hasChildNodes()) {
        grafico.removeChild(grafico.lastChild);
        }  


    if ($scope.dadosGraficoLiberacoes.length === 1){        
        myFunctions.showAlert('Sem dados de LIBERAÇÕES!')  
        }else{
        google.charts.load('current', {'packages':['corechart']});
        google.setOnLoadCallback(drawChart);
    
        function drawChart() {
          
        var data = google.visualization.arrayToDataTable($scope.dadosGraficoLiberacoes);
        
            var options = {
                title:
                "LIBERAÇÕES - " +
                String($scope.dados.data.getDate()).padStart(2, "0") + " de " + 
                $scope.dados.data.toLocaleString("default", {month: "long"}) 
                + " de " + $scope.dados.data.getFullYear(),
                titleTextStyle: { color: "#000000", fontName: "arial", fontSize: 16, bold: true, italic: false},
                legend: { position: "top", maxLines: 1, alignment: "end"},
                animation: { duration: 3000, easing: "out", startup: true},
                width: 1300,
                height: alturaGrafico,
                fontSize: 12,
                chartArea: { width: "80%", height: "90%" },
                seriesType: "bars",
                series: { 3: { type: "line" } },
                backgroundColor: "transparent",
                annotations: { textStyle: { color: "black", fontSize: 10 },
                }
            };


            var chart = new google.visualization.BarChart(document.getElementById('chart_liberacoes'));
            google.visualization.events.addListener(chart, 'error', function (googleError) {
            google.visualization.errors.removeError(googleError.id);
            });
    
            google.visualization.events.addListener(chart, 'ready', function () {
            $scope.imagemGraficoLiberacoes = chart.getImageURI();
            });

            chart.draw(data, options);

        } 
    } 

}



function graficoApreensoesAcumulado(){
          
    let alturaGrafico = ''
    let larguraGrafico = ''
    
        if ($scope.dadosGraficoApreensoesAcumulado.length > 11){
            alturaGrafico = 800
            larguraGrafico = 1250
        }else{
            alturaGrafico = 600
            larguraGrafico = 1000
        }

    let grafico = document.getElementById("chart_apreensoesAcumulado");
        while (grafico.hasChildNodes()) {
        grafico.removeChild(grafico.lastChild);
        }

        if ($scope.dadosGraficoApreensoesAcumulado.length === 1) {
        myFunctions.showAlert("Sem dados de APREENSÕES ACUMULADO!");
        } else {
        google.charts.load("current", { packages: ["corechart"] });
        google.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable(
            $scope.dadosGraficoApreensoesAcumulado
            );

                var options = {
                    title:
                    "APREENSÕES ACUMULADO - " +
                    String($scope.dados.data.getDate()).padStart(2, "0") + " de " +
                    $scope.dados.data.toLocaleString("default", { month: "long" }) +
                    " de " + $scope.dados.data.getFullYear(),
                    titleTextStyle: { color: "#000000", fontName: "arial", fontSize: 16, bold: true, italic: false },
                    animation: { duration: 3000, easing: "out", startup: true },
                    is3D: true,
                    width: larguraGrafico,
                    height: alturaGrafico,
                    fontSize: 12,
                    chartArea: { width: "90%", height: "80%" },
                    backgroundColor: "transparent",
                    legend: { position: "labeled" },
                };

                var chart = new google.visualization.PieChart(
                document.getElementById("chart_apreensoesAcumulado")
                );

                google.visualization.events.addListener(
                chart, "error", function (googleError) {
                    google.visualization.errors.removeError(googleError.id);
                }
            );

            google.visualization.events.addListener(chart, "ready", function () {
            $scope.imagemGraficoApreensoesAcumulado = chart.getImageURI();
            });

            chart.draw(data, options);
        }
    }
}



function graficoLiberacoesAcumulado(){
        
    let alturaGrafico = ''
    let larguraGrafico = ''
    
        if ($scope.dadosGraficoLiberacoesAcumulado.length > 11){
            alturaGrafico = 800
            larguraGrafico = 1250
        }else{
            alturaGrafico = 600
            larguraGrafico = 1000
        }
   


    if ($scope.dadosGraficoLiberacoesAcumulado.length === 1){
       myFunctions.showAlert('Sem dados de LIBERAÇÕES ACUMULADO!')  
    
    }else{   
    google.charts.load('current', {'packages':['corechart']});
    google.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable($scope.dadosGraficoLiberacoesAcumulado);

        var options = {
            title : 'LIBERAÇÕES ACUMULADO - ' + String($scope.dados.data.getDate()).padStart(2, '0') + ' de ' 
            + $scope.dados.data.toLocaleString('default', {month: 'long'})+' de '+ $scope.dados.data.getFullYear(),
            titleTextStyle:{ color: '#000000', fontName: 'arial', fontSize: 16, bold: true, italic: false },
            animation:{ duration: 3000, easing: 'out', startup: true },
            is3D: true,
            width: larguraGrafico, 
            height: alturaGrafico,
            fontSize: 12,
            chartArea: {width: "90%", height: "80%"},
            backgroundColor: 'transparent',
            legend: { position: 'labeled' },
                    
            };


            var chart = new google.visualization.PieChart(document.getElementById('chart_liberacoesAcumulado'));
            
                google.visualization.events.addListener(chart, 'error', function (googleError) {
                google.visualization.errors.removeError(googleError.id);
                
                });

                google.visualization.events.addListener(chart, 'ready', function () {
                $scope.imagemGraficoLiberacoesAcumulado = chart.getImageURI();
                });

                chart.draw(data, options);

            } 
        }    
    }




$scope.gerarPDF = function () {

    var doc = new jsPDF({ orientation: "landscape" });
    var totalPagesExp = '{total_pages_count_string}'

    var image1 = $scope.imagemGraficoApreensoes;
    var image2 = $scope.imagemGraficoLiberacoes;
    var image3 = $scope.imagemGraficoApreensoesAcumulado;
    var image4 = $scope.imagemGraficoLiberacoesAcumulado;
  

    doc.autoTable({

        didDrawPage: function (data) {
            
            doc.setFontSize(16);
            doc.text("Gráficos - Resumo Diário", 10, 15);        
            doc.setFontSize(10);
            
            if(image1) {
                doc.addImage(image1, "JPEG", 20, 20, 250, 150); //ajustes: margem esquerda, margem superior, largura, altura
                doc.text('Pagina ' + doc.internal.getNumberOfPages() + ' de ' + totalPagesExp, 15, 200)   
            }
           
            if(image2){
                doc.addPage(); 
                doc.addImage(image2, "JPEG", 20, 20, 250, 150); //ajustes: margem esquerda, margem superior, largura, altura
                doc.text('Pagina ' + doc.internal.getNumberOfPages() + ' de ' + totalPagesExp, 15, 200)
            }
            
            if(image3){
                doc.addPage();
                doc.addImage(image3, "JPEG", 20, 20, 250, 150); //ajustes: margem esquerda, margem superior, largura, altura
                doc.text('Pagina ' + doc.internal.getNumberOfPages() + ' de ' + totalPagesExp, 15, 200)
            }
            
            if(image4){
                doc.addPage();
                doc.addImage(image4, "JPEG", 20, 20, 250, 150); //ajustes: margem esquerda, margem superior, largura, altura
            }
            
            var str = 'Pagina ' + doc.internal.getNumberOfPages()
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === 'function') {
                str = str + ' de ' + totalPagesExp
            }

            doc.setFontSize(10)
            
           // doc.setLineWidth(1);
           // doc.rect(10, 7, 250, 100); //coloca um contorno no gráfico
           // doc.rect(10, 110, 250, 90);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize
            var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
            doc.text(str, data.settings.margin.left, pageHeight - 10)
            
        },
        margin: { top: 10 },
    });
        if (typeof doc.putTotalPages === 'function') {
        doc.putTotalPages(totalPagesExp)
        };


    doc.save('Grafico - Resumo Diario.pdf')

}




$scope.enviarEmail = function () {

    if ($scope.dadosGraficoApreensoes || $scope.dadosGraficoLiberacoes || $scope.dadosGraficoLiberacoesAcumulado || $scope.dadosGraficoApreensoesAcumulado !== 1) {

    $mdDialog.show({
        controller: function ($scope, $mdDialog, myFunctions) {

            $scope.enviarPDF = function () {
               
                var doc = new jsPDF({ orientation: "landscape" });
                var totalPagesExp = '{total_pages_count_string}'


                    var imgData1 = $scope.imagemGraficoApreensoes;
                    var imgData2 = $scope.imagemGraficoLiberacoes;
                    var imgData3 = $scope.imagemGraficoLiberacoesAcumulado;
                    var imgData4 = $scope.imagemGraficoApreensoesAcumulado;

                    var doc = new jsPDF({ orientation: "landscape" });
                    //doc.setFontSize(20);
                    doc.setFont("Helvetica");
                    doc.setFontSize(16);
                    doc.text("Gráfico - Resumo Diário", 10, 15);
                    doc.addImage(imgData1, "JPEG", 10, 20, 250, 150); 
                    doc.setFontSize(10);
                    doc.text('Pagina ' + doc.internal.getNumberOfPages() + ' de 4' , 15, 200)
                    doc.addPage();
                    doc.addImage(imgData2, "JPEG", 10, 20, 250, 150); //ajustes: margem esquerda, margem superior, largura, altura
                    doc.text('Pagina ' + doc.internal.getNumberOfPages() + ' de 4' , 15, 200)
                    doc.addPage();
                    doc.addImage(imgData3, "JPEG", 10, 20, 250, 150);
                    doc.text('Pagina ' + doc.internal.getNumberOfPages() + ' de 4' , 15, 200)
                    doc.addPage();
                    doc.addImage(imgData4, "JPEG", 10, 20, 250, 150);
                    doc.text('Pagina ' + doc.internal.getNumberOfPages() + ' de 4' , 15, 200)
                    
                    var str = 'Pagina ' + doc.internal.getNumberOfPages()
                    // Total page number plugin only available in jspdf v1.0+
                        if (typeof doc.putTotalPages === 'function') {
                        str = str + ' de ' + totalPagesExp
                        }
                    
                    doc.setFontSize(10)

                   const data = doc.output("blob");

                    var formData = new FormData();
                    formData.append("file", data, "Movimentacoes-diaria"+Date.now()+".pdf");

                    var request = new XMLHttpRequest();
                    
                   // request.onreadystatechange = function() {
                    //    if (request.readyState == XMLHttpRequest.DONE) {
                      //      alert(request.responseText);
                            
                     //   }
                   // }
                    request.responseType = 'document';                  
                    request.open("POST", "http://192.168.0.124:3000/receive");
                    request.send(formData);
                    request.access
                 
                  
                if (data.code === 200){
                   
                           var confirm = $mdDialog.confirm()
                            .title('Email enviado com sucesso!')
                            .textContent('Deseja enviar outro email?')
                            .ariaLabel('Lucky day')
                            .targetEvent()
                            .ok('Sim')
                            .cancel('Não');
                        $mdDialog.show(confirm).then(function () {
                            
                            $scope.enviarEmail()
                        });                 
                  
                }else{
                    myFunctions.showAlert("Erro no envio! Verifique!"); 
                }


            }

        },
        templateUrl: 'resumo-diario-email.html',
        scope: $scope,
        preserveScope: true,
        scopoAnterior: $scope
    }).then(function (answer) {
        let a = answer;
    

       
    }, function () {
        $scope.statusdialog = 'You cancelled the dialog.';
    });

    }else{
        myFunctions.showAlert("Não foi encontrado dados! Revise pesquisa!"); 
    }

 }



function buscaPatios() {
    return new Promise(function (resolve, reject) {
        $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') }).then(function (response) {
            let dados = response.data;
            resolve(dados);
        });
    })
}


function buscaDadosGraficos() {
    return new Promise(function (resolve, reject) {
     // console.log('Enviados Servidor: ', $scope.dados)
       
        $http.get(urlServidor.urlServidorChatAdmin + '/analitico/cco-graficos', { params: $scope.dados }).then(function (response) {
            let dados = response.data;
            resolve(dados);
        //  console.log('Recebidos Servidor: ', response.data)
        });
    })
}



function dataPesquisa(){
    var date = new Date();
    $scope.dados.data = new Date(new Date().setDate(new Date().getDate() -1 ));

}




 function totalMovimentacoes() {

    let dados = $scope.listaDados || []

    const totalApreensoesDia = dados.reduce((a, b) => a + (b.apreensoesDia || 0), 0);
    const totalApreensoesAcumulado = dados.reduce((a, b) => a + (b.apreensoesAcumulado || 0), 0);
    const totalLiberacoesDia = dados.reduce((a, b) => a + (b.liberacoesDia || 0), 0);
    const totalLiberacoesAcumulado = dados.reduce((a, b) => a + (b.liberacoesAcumulado || 0), 0);

    const totalText = `<div style="width:50px"><h4 style="font-size:14px;color:blue;padding-top:10px,padding-rigth:10px">TOTAL</h4></div>`

    const totalApreensoesDiaValue = `<div style="width:50px"><h4 style="font-size:14px;color:blue"> ${totalApreensoesDia} </h4></div>`
    const totalApreensoesAcumuladoValue = `<div style="width:50px"><h4 style="font-size:14px;color:blue"> ${totalApreensoesAcumulado} </h4></div>`
    const totalLiberacoesDiaValue = `<div style="width:50px"><h4 style="font-size:14px;color:blue"> ${totalLiberacoesDia} </h4></div>`
    const totalLiberacoesAcumuladoValue = `<div style="width:50px"><h4 style="font-size:14px;color:blue"> ${totalLiberacoesAcumulado} </h4></div>`

    dados.push({
        nomePatio: totalText,
        apreensoesDia: totalApreensoesDiaValue,
        apreensoesAcumulado: totalApreensoesAcumuladoValue,
        liberacoesDia: totalLiberacoesDiaValue,
        liberacoesAcumulado: totalLiberacoesAcumuladoValue
    })

 }


});
