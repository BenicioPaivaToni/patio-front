angular.module("app").controller('advogadosEditarCtrl', function ($scope, $location, $http, urlServidor, myFunctions, estados) {

    $scope.dados = {
        Search: ''
    }


    var urlParams = $location.search();

    $scope.dados = {
        id: urlParams.dados[0].id,
        nome: urlParams.dados[0].nome,    
        cidade: urlParams.dados[0].cidade,
        uf: urlParams.dados[0].uf,
        fone: urlParams.dados[0].fone,
        ativo: urlParams.dados[0].status,
        email: urlParams.dados[0].email
        
    }

    console.log($scope.dados)
    console.log(urlParams)
   
    $scope.estados = estados.uf;
    
    $scope.cidades

    
    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/advogados/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {

                    myFunctions.showAlert('Advogado já existe!')

                } else {

                    myFunctions.showAlert('Alteração executada com sucesso!');
                }
                        
                   
            }         
           
        });
    };


    $scope.fechar = function () {
        window.close();
    }

   
})