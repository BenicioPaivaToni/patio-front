'use strict';
angular.module('app')
.controller("usuariosNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions) {  

  $scope.usuario = {
     nome:'',  
     login: '' ,
     apelido: '' ,
     email: '' ,
     nascimento: '' ,
     cargo: '' ,
     senha: '123456' ,
     tipo: '' ,
     ativo: '1',
     apagar_docs_fotos: 'NÃO',
     alterar_liberacao: 'NÃO',
     tipo_situacao: '0',
     ultimo_acesso:'',
     observacao: '' ,  
     tiponet:'TODAS',
     idempresa: localStorage.getItem('id_empresa'),
     status_ncv:"0",
     ajuste_logistica:"0",
     altera_no_patio:"0",
     altera_localizado_leilao:"0",
     altera_data_apreensao:"0"
  }
  

 

	$scope.list = [
		{
			id: 1,
			menu: 'CADASTROS',
			aplicacao: 'AUTORIDADES',
			tipo: 'MENU'
		},
		{
			id: 2,
			menu: 'CADASTROS',
			aplicacao: 'CONFIGURAÇÕES',
			tipo: 'MENU'
		},
		{
			id: 3,
			menu: 'CADASTROS',
			aplicacao: 'EMPRESAS REBOQUE',
			tipo: 'MENU'
		},
		{
			id: 4,
			menu: 'CADASTROS',
			aplicacao: 'MOTORISTAS',
			tipo: 'MENU'
		},
		{
			id: 5,
			menu: 'CADASTROS',
			aplicacao: 'PATIOS',
			tipo: 'MENU'
		},
		{
			id: 6,
			menu: 'CADASTROS',
			aplicacao: 'PERMISSOES',
			tipo: 'MENU'
		},
		{
			id: 7,
			menu: 'CADASTROS',
			aplicacao: 'REBOQUES',
			tipo: 'MENU'
		},
		{
			id: 8,
			menu: 'CADASTROS',
			aplicacao: 'USUARIOS',
			tipo: 'MENU'
		}
	];

  buscaPatios().then( function(result){
     $scope.patios = result ;
  });  

  $scope.isDisabled = false

  $scope.gravar = function() {        
  
    $http.post(urlServidor.urlServidorChatAdmin+'/usuarios/cadastrar', $scope.usuario ).then( function(response){

        if (response.data.code){
            
          myFunctions.showAlert('Erro na gravação!')

        }else{

          if ( response.data == '11000' )                 
          {
            myFunctions.showAlert('Usuario já existe!')
          }else
          {                    

						myFunctions.showAlert( 'Cadastro executado com sucesso!' );
						$scope.usuario.idUsuario = response.data.insertId ; 
            $scope.isDisabled = true ;            

          }  
					
        }; 

    }); 

  }; 

  $scope.gravarPatios = function(id_usuario) {        

    if( id_usuario == undefined ){
			myFunctions.showAlert('Gravar cadastro do usuário!')
		} else {	

      let dados = $scope.patios ;
      
      dados.push({idusuario: id_usuario});

    /*  

      $http.post(urlServidor.urlServidorChatAdmin+'/usuarios/cadastrar_patios', dados ).then( function(response){

          if (response.data.code){            
            myFunctions.showAlert('Erro na gravação!')
          }else{
            myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
            $scope.isDisabled = true ;                            
          }; 

      });
      
    */  

    }  

  }; 

  
function buscaPatios(){

  return new Promise( function( resolve,reject){

  $http.put(urlServidor.urlServidorChatAdmin+'/patios/listar').then( function(response){

    let dados = response.data ;

    dados.sort(function(a, b){
      if (a.nome > b.nome) {
      return 1;
      }
      if (a.nome < b.nome) {
      return -1;
      }        
      return 0;
    });       

    resolve(dados) ;

  });

  })

}
    
});