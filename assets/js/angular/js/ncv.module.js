angular
  .module("app")
  .controller(
    "ncvCtrl",
    function ($scope, $location, $http, urlServidor, myFunctions) {
      $scope.novo = () => window.open("/#/ncv-novo");

      $scope.dados = {
        ncv: null,
        placa: null,
        codigo: null,
        chassi: null,
        numero_motor: null,
        patio: null,
        status: null,
        data_cadastro: null,
      };

      $scope.noPatio = 0;
      $scope.localizadoLeilao = 0;
      $scope.alteraLocalizadoLeilao = localStorage.getItem("altera_localizado_leilao") === "1" ? false : true;
      
      $scope.limpar = function (ev) {
        $scope.dados.ncv = "";
        $scope.dados.placa = "";
        $scope.dados.codigo = "";
        $scope.dados.chassi = "";
        $scope.dados.numero_motor = "";
        $scope.dados.patio = "";
        $scope.dados.status = "";
        //		$scope.dados.data_cadastro = null
      };
      /*
      buscaPatios().then( function(result){
      $scope.patios = result ;
      });
      */

      $scope.pesquisar = () => {
        $scope.dadosList = null;
        if (
          !Boolean($scope.dados.ncv) &&
          !Boolean($scope.dados.placa) &&
          !Boolean($scope.dados.codigo) &&
          !Boolean($scope.dados.chassi) &&
          !Boolean($scope.dados.numero_motor)
        ) {
          myFunctions.showAlert("Informe NCV , Placa ou CODIGO para pesquisa!");
          return;
        }

        if ($scope.dados.placa?.length > 0 && $scope.dados.placa.length < 3) {
          myFunctions.showAlert(
            "Pesquisa por placa , necessario ao menos 3 caracteres!"
          );
          return;
        }

        $scope.dados.idusuario = localStorage.getItem("id_usuario");

        $http
          .put(urlServidor.urlServidorChatAdmin + "/ncv/listar", $scope.dados)
          .then(({ data }) => {
            if (data.length === 0) {
              myFunctions.showAlert("Pesquisa não encontrou registros!");
              return;
            }
            let dados = data;

            $scope.tipoUsuario = localStorage.getItem("tipo_usuario");
            $scope.alterarNoPatio = localStorage.getItem("altera_no_patio");

            if($scope.tipoUsuario == 'admin' ||  $scope.alterarNoPatio == "1"){
              $scope.altera_no_patio = true;
            }else{
              $scope.altera_no_patio = false;
            }

            let dadosNota = dados[0]?.nota_fiscal;

            if ($scope.tipoUsuario === "consulta contabil") {
              if (dadosNota === null) {
                myFunctions.showAlert("Pesquisa não encontrou registros!");
              } else {
                $scope.dadosList = dados;
              }
            } else {
              $scope.dadosList = dados;
            }
          });
      };

      const buscaPatios = async () => {
        try {
          return await $http.put(
            urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
            {
              idUsuario: localStorage.getItem("id_usuario"),
              idEmpresa: localStorage.getItem("id_empresa"),
            }
          );
        } catch (error) {
          console.error(error);
        }
      };

      const permiteEdicao = async (id_ncv) => {
        const { data } = await buscaPatios();
        return Boolean(data.find((item) => item.id_patio === id_ncv));
      };

      $scope.detalhe = async (ev, id) => {
        const data = await $http
          .put(`${urlServidor.urlServidorChatAdmin}/ncv/listar-usuario`, {
            ncv: id.toString(),
            idusuario: localStorage.getItem("id_usuario"),
          })
          .then(({ data }) => data[0] )
          .catch((err) => console.log(err));

        if (!data) {
          myFunctions.showAlert(
            `Você não possui acesso a esse pátio edição não permitida `
          );
          return;
        }

        if (await permiteEdicao(data.id_patio)) {
          window.open("/#/ncv-editar?id " + data.id, "_blank");
        } else {
          myFunctions.showAlert(
            `Você não possui acesso a esse pátio edição não permitida `
          );
        }
      };


      $scope.informaPatio = async (ev, rowId) =>{

        $scope.rowData = $scope.dadosList.findIndex((x) => x.id == rowId)
        
        const { data } = await buscaPatios()
      
        let patioApreensao = Boolean(data.find((item) => item.id_patio === $scope.dadosList[0].id_patio))
        let patioDestino = Boolean(data.find((item) => item.nome === $scope.dadosList[0].nome_patio_destino))

        if ( patioDestino == true ){

         $scope.gravarNoPatio()

        }
        
        else if (!$scope.dadosList.nome_patio_destino && patioApreensao == true){

          $scope.gravarNoPatio()

        }else{
            
          if($scope.dadosList[0].nome_patio_destino !== null && patioDestino == false){

            myFunctions.showAlertCustom("Você não possui acesso a este pátio para efetuar a alteração! <br/><br/>"
            + "Pátio: " + $scope.dadosList[0].nome_patio_destino )

          }else{

            myFunctions.showAlert(`Você não possui acesso a este pátio para efetuar a alteração!`)
          }
          
          $scope.pesquisar()

        }

      }
     

      
      $scope.gravarLocalizadoLeilao = async (ev, rowId) => {

        let rowData = $scope.dadosList.findIndex((x) => x.id == rowId)
        
        var dadosTmp = $scope.dadosList[rowData]

        dadosTmp.localizado_leilao = dadosTmp.localizado_leilao === 1 ? 0 : 1
  
        $scope.dadosList[rowData] = dadosTmp
  
        $http.post(urlServidor.urlServidorChatAdmin + '/ncv/altera-localizado-leilao', {id: dadosTmp.id, localizado_leilao: dadosTmp.localizado_leilao }).then(function (response) {
             
          if (response.data.code) {
            $scope.error = response.data
          }
  
          myFunctions.showAlert('Informação de Localizado Leilão alterada com sucesso!');
    
        })

        if(dadosTmp.no_patio !== 1 && dadosTmp.localizado_leilao === 1){

          $http.post(urlServidor.urlServidorChatAdmin + '/ncv/altera-patio', {id: dadosTmp.id, no_patio: 1 }).then(function (response) {
             
            if (response.data.code) {
              $scope.error = response.data;
            }
    
          })

        }
        
        setTimeout(() => {

          $scope.pesquisar();

        }, 1000);

      }



      $scope.checkboxDetalhe = function (ev, rowId) {

        if(!rowId) return false;

        var row = $scope.dadosList.find((x) => x.id === rowId);
        $scope.noPatio = row.no_patio;

        if(!row){
          return false;
        }else{
          return row.no_patio === 1 ? true : false;
        }
        

      }


      
      $scope.checkboxLocalizado = function (ev, rowId) {
        if (!rowId || !Array.isArray($scope.dadosList)) {
          return false;
        }

        var row = $scope.dadosList.find((x) => x.id === rowId);

        if (row) {
          $scope.localizadoLeilao = row.localizado_leilao;
          return row.localizado_leilao === 1 ? true : false;
        } else {
          return false;
        }
      }


      

      $scope.gravarNoPatio = () => {
        
        var dadosTmp = $scope.dadosList[$scope.rowData];

        dadosTmp.no_patio = dadosTmp.no_patio === 1 ? 0 : 1;
  
        $scope.dadosList[$scope.rowData] = dadosTmp;
  
        $http.post(urlServidor.urlServidorChatAdmin + '/ncv/altera-patio', {id: dadosTmp.id, no_patio: dadosTmp.no_patio }).then(function (response) {
             
          if (response.data.code) {
            $scope.error = response.data;
          }
  
          myFunctions.showAlert('Informação de pátio alterada com sucesso!');
  
          $scope.pesquisar();
  
        })

      }

    
      $scope.hideCheck = function (ev, rowId){
          
        if(!rowId) return true;
      }


    }
);
