angular.module("app").controller('RequisicaoCtrl', function ($scope, $http, urlServidor, $filter, $mdDialog, myFunctions) {


    $scope.Requests = {
        //Chamadas a endPoints Geral 

        fetchPatios: async () => {
            const response = await $http.put(`${urlServidor.urlServidorChatAdmin}/patios/listar-patiosusuario`, { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') })
            return $scope.patios = response.data
        },




    }


    const Init = () => {
        $scope.Requests.fetchPatios()
    }

    Init()


    $scope.dados = {
        dataInicio: new Date(),
        dataFinal: new Date(),
        patio: null,
        fornecedor: null,
        patioGrupo: null,
        fornecedor: null,
        status: null
    }

    $scope.Methods = {
        Pesquisar: async () => {
            const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/compras/listar-requisicoes`, { params: $scope.dados })
            const { data } = response
            $scope.OriginalInformatios = data
            return $scope.Dados = data.map(Item => ({


                data: $filter('date')(Item.data, 'dd/MM/yyyy', "+0000"),
                id: Item.id,
                id_patio: Item.id_patio,
                id_usuario: Item.id_usuario,
                patio: Item.patio,
                status: Item.status,
                usuario: Item.usuario,



            }))

        }
        ,




        Novo: () => {
            window.open('/#/Lançamentos-novo');
        },

        Limpar: () => { delete $scope.dados }
        ,

        MesAtual: () => {
            let hoje = new Date()
            $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
            $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);

        }
    }




    $scope.OpenOrcamento = (evt, rowid) => {
        const Obj = $scope.OriginalInformatios.filter((obj) => { return obj.id == rowid })
        if (Obj[0].status === 'CANCELADO') {
            myFunctions.showAlert(`Requisição Cancelada, Não permitido Orçamentos`)

        } else {
            const { id } = Obj[0]


            sessionStorage.setItem('Obj', JSON.stringify(Obj));

            window.open('/#/orcamento?rowId ' + id, '_blank');
        }

    }



    $scope.Deletar = async (evt, rowid) => {

        const Obj = $scope.OriginalInformatios.filter((obj) => { return obj.id == rowid })
        const { id } = Obj[0]


        let confirm = $mdDialog.confirm()
            .ariaLabel('Lucky day')
            .title('Deseja Cancelar esta Requisição ?')
            .ok('Deletar')
            .cancel('Cancelar')
        $mdDialog.show(confirm).then(async (response) => {
            var resposta;

            if (response === true) {
                resposta = true
            } else {
                resposta = false
            }

            if (resposta === true) {

                await $http.post(`${urlServidor.urlServidorChatAdmin}/compras/cancelar-requisicao`, { id: id }).then((res) => {
                    console.log(res);
                    myFunctions.showAlert(`Requisição Cancelada com sucesso`)

                    $scope.Methods.Pesquisar()

                })

            } else {
                myFunctions.showAlert(`Requisição não cancelada`)


            }


        })


    },



        $scope.gerarPdf = function () {



            var doc = new jsPDF({ orientation: "landscape" });
            var totalPagesExp = '{total_pages_count_string}'

            doc.setFontSize(10);
            doc.autoTable({

                columnStyles: {
                    vencimento: { halign: 'left' },
                },

                body: $scope.Dados,
                columns: [
                    { header: 'Data', dataKey: 'data' },
                    { header: 'Pátio', dataKey: 'patio' },

                    { header: 'Status', dataKey: 'status' },

                    { header: 'Usuário Requisição', dataKey: 'usuario' },
                ],
                bodyStyles: {
                    margin: 10,
                    fontSize: 08,
                },
                didDrawPage: function (data) {
                    // Header
                    doc.setFontSize(18)
                    doc.setTextColor(40)
                    doc.setFontStyle('normal')

                    const DataInicio = $filter('date')($scope.dados.dataInicio, 'dd/MM/yyyy', "+0000")
                    const DataFinal = $filter('date')($scope.dados.dataFinal, 'dd/MM/yyyy', "+0000")
                    doc.text(`Requisições feitas no Perídodo de: ${DataInicio} à  ${DataFinal}`, data.settings.margin.left + 15, 22);

                    // Footer
                    var str = 'Pagina ' + doc.internal.getNumberOfPages()
                    // Total page number plugin only available in jspdf v1.0+
                    if (typeof doc.putTotalPages === 'function') {
                        str = str + ' de ' + totalPagesExp
                        var totalpaginas = totalPagesExp;
                    }

                    var strTotal = ''

                    doc.setFontSize(10)

                    // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                    var pageSize = doc.internal.pageSize
                    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()

                    doc.text(str + '                        ' + strTotal, data.settings.margin.left, pageHeight - 10)
                },
                margin: { top: 30 },
            });
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp)
            }

            doc.save('relatorio-Requisições.pdf');
        }

})