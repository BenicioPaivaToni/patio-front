angular.module("app").controller('RequisicaoNovoCtrl', function ($scope, urlServidor, $http, myFunctions, $mdDialog, $scope, $http, urlServidor, myFunctions, $mdDialog, estados, viaCep, $rootScope) {

    $scope.Requests = {
        //Chamadas a endPoints Geral 


        fetchPatios: async () => {
            const response = await $http.put(`${urlServidor.urlServidorChatAdmin}/patios/listar-patiosusuario`, { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') })
            return $scope.patios = response.data
        },
    }


    const Init = () => { $scope.Requests.fetchPatios() }

    Init()


    $scope.dados = {
        patio: null,
    }

    $scope.Dados = {
        ID: 0,
        Servico: '',
        Quantidade: 0,
        
    }


    $scope.Orcamentos = []

    $scope.Methods = {

        limpaCampos: () => {
            $scope.Dados = {
                ID: 0,
                Servico: '',
                Quantidade: 0,
               

            }

        },


        Salvar: async function () {
            const { patio } = $scope.dados;







            if ($scope.Orcamentos.length != 0) {

                await $http.post(`${urlServidor.urlServidorChatAdmin}/compras/cadastrar-requisicao`, { id_patio: patio, id_usuario: localStorage.getItem('id_usuario') })
                    .then((Response) => {

                        const { insertId } = Response.data

                        $scope.Orcamentos.forEach(async (Items) => {
                            await $http.post(`${urlServidor.urlServidorChatAdmin}/compras/cadastrar-itensrequisicao`, { id_requisicao: insertId, descricao: Items.Servico, qtde: Items.Quantidade, valor: Items.Valor })
                                .then(() => {

                                    myFunctions.showAlert(`Requisição enviada com sucesso, total de ${$scope.Orcamentos.length} enviados`)
                                    $scope.Orcamentos = []

                                })

                        })
                    })


            } else {
                myFunctions.showAlert('Lista de itens não pode ser vazia');
            }
        }

        ,
        Cancelar: () => window.close(),



        IncluirOrcamento: function (Dados) {
            const { patio } = $scope.dados;

            if ($scope.Dados.Servico === '') {
                myFunctions.showAlert('Preencha as informações corretamente');

            } else if (patio == null) {
                myFunctions.showAlert('Pátio não pode ser Vazio');


            } else if (Dados.Quantidade < 1) {

                myFunctions.showAlert('Preencha as informações corretamente');

            }
            else {
                $scope.Orcamentos.push(angular.copy(Dados))

                this.limpaCampos()
                let ID = 0



                $scope.Orcamentos.forEach(element => {
                    element.ID = ID += 1
                });

            }
        },
    }



    $scope.RemoveOrcamento = (ev, rowId, id) => {

        if ($scope.Orcamentos.length == 0 || $scope.Orcamentos.length == 1) {

            $scope.Orcamentos = []


        } else {

            $scope.Orcamentos.forEach((element, idx) => {
                if (element.ID == rowId) {
                    $scope.Orcamentos.splice(idx, 1)
                }
            })
        }


    }








})
