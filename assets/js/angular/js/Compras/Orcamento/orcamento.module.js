angular.module("app").controller('orcamentoCtrl', function ($rootScope, $scope, $http, urlServidor, $filter, myFunctions, $mdDialog, S3UploadService, ncvFac, $sce, urlImagens, buckets) {
    const queryString = window.location.hash;

    const id = queryString.slice(queryString.indexOf("%") + 3);


    $scope.Requests = {
        //Chamadas a endPoints Geral 

        fetchPatios: async () => {
            const response = await $http.put(`${urlServidor.urlServidorChatAdmin}/patios/listar-patiosusuario`, { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') })
            return $scope.patios = response.data
        },

        fetchPatiosGrupo: async () => {
            const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/grupos-patios/listar`,)
            return $scope.patiosGrupo = response.data
        },

        fetchDados: async () => {
            const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/compras/busca-requisicao`, { params: { id: id } })
            const { data } = response
            $scope.dados = data[0]
        },
        fetchItems: async () => {
            const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/compras/busca-itensrequisicao`, { params: { id: id } })
            const { data } = response
            return $scope.Itens = data.map(Item => ({
                descricao: Item.descricao,
                id: Item.id,
                id_requisicao: Item.id_requisicao,
                qtde: Item.qtde,
                valor_unitario: '',
                total_Item: 0,
                ValorSemFormatacao: 0
            }))


        },

        fetchFornecedores: async () => {
            const response = await $http.put(urlServidor.urlServidorChatAdmin + '/fornecedores/listar')
            $scope.fornecedores = response.data
        },
        fetchTipoRecebimento: async () => {
            const response = await $http.put(urlServidor.urlServidorChatAdmin + '/tipos-recebimento/listar-tipo', { tipo: 'PAGAR' })
            $scope.tiposRecebimentos = response.data;

        }

    }

    $scope.dataHoje = { datarequisicao: new Date() }


    function getMoney(str) {
        var valor = str.replace(/[^\d,]/g, ''); //remove todos os caracteres menos numeros e virgula
        valor = valor.replace(',', '.'); //troca virgula por ponto
        return parseFloat(valor);

    }




    $scope.GerarTotalItem = (Itens) => {
        Itens.forEach((Item) => {
            Item.descricao;
            Item.id;
            Item.id_requisicao;
            Item.qtde;
            Item.valor_unitario;
            Item.ValorSemFormatacao = getMoney(Item.valor_unitario);
            Item.total = Item.ValorSemFormatacao * Item.qtde;

            total = Item.ValorSemFormatacao * Item.qtde;
            Item.total_Item = total.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' }).replace(/\s+/g, '')


        })
        var TotalGeral = 0;
        Itens.forEach((Item) => {
            TotalGeral += Item.total

        })
        return $scope.TotalGeral = TotalGeral.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })
    }





    $scope.Methods = {


        Salvar: async (Itens) => {


            const { forma_pagto, id_fornecedor } = $scope.dados

            if (forma_pagto == undefined || id_fornecedor == undefined) {
                myFunctions.showAlert('Campo fornecedor e Forma de pagamento Obrigatorio');

            } else {
                await $http.post(`${urlServidor.urlServidorChatAdmin}/compras/cadastrar-orcamento`, $scope.dados)
                    .then((response) => {
                        const { insertId } = response.data
                        Itens.forEach(async (Item) => {

                            await $http.post(`${urlServidor.urlServidorChatAdmin}/compras/cadastrar-itensorcamento`, {
                                id_orcamento: insertId,
                                descricao: Item.descricao,
                                qtde: Item.qtde,
                                valor: Item.valor_unitario

                            }).then(() => {

                                myFunctions.showAlert('Orçamento Enviado com sucesso');
                                $scope.dados.forma_pagto = ''
                                $scope.dados.id_fornecedor = ''

                                initLancamentos()
                            })


                        })


                    })

            }






        }
    }







    const Init = () => {
        $scope.Requests.fetchPatios()
        $scope.Requests.fetchPatiosGrupo()
        $scope.Requests.fetchDados()
        $scope.Requests.fetchItems()
        $scope.Requests.fetchFornecedores()
        $scope.Requests.fetchTipoRecebimento()

    }

    Init()





    var bucket = null;

    $http.get(urlServidor.urlServidorChatAdmin + '/autoridades/vercodigo').then(function (response) {

        AWS.config.update({ accessKeyId: response.data.ac, secretAccessKey: response.data.sc });
        AWS.config.region = 'us-east-1';
    }).finally(function () {

        $scope.bucket = new AWS.S3();

    });

    $scope.bucket = new AWS.S3();

    $scope.urlAnexosOrcamentos = urlImagens.urlAnexosOrcamentos;

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);


    }


    $scope.MethodsLancamentos = {


        fetchOrcamentos: async () => {
            const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/compras/listar-orcamentosrequisicao`, { params: { id_requisicao: id } })
            const { data } = response


            $scope.Lancamentos = data.map(Item => ({
                id_orcamento: Item.id_orcamento,
                id_requisicao: Item.id_requisicao,
                autorizado: (Item.autorizado == 'SIM') ? 'Autorizado' : 'Autorizar',

                data_orcamento: $filter('date')(Item.data_orcamento, 'dd/MM/yyyy', "+0000"),
                patio: Item.patio,
                fornecedor: Item.fornecedor || 'Não Fornecido',
                forma_pagamento: Item.forma_pagamento || 'Não Fornecido',
                anexo: Item.anexo || 'Anexar',
                HasAnexo: Item.anexo ? 'Ver Anexo' : 'Sem Anexo',
                color: Item.anexo ? 'blue' : 'red',
                Disable: Item.anexo ? false : true,
                total_orcamento:Item.total_orcamento.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })
            }))


        },
        fetchItemsOrcamento: async (dados) => {

            const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/compras/busca-itensorcamento`, { params: { id: dados.id_orcamento } })
            const { data } = response
            $scope.ItensOrcamento = data
            $scope.ItensOrcamento.forEach((Item) => {
                Item.descricao,
                    Item.id,
                    Item.id_orcamento,
                    Item.qtde,
                    Item.valor,
                    Item.valorString = Item.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' }),
                    total_item = Item.valor * Item.qtde,
                    Item.total_ItemString = total_item.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })

            })

console.log($scope.ItensOrcamento);

        },
        


       







        makeid: () => {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for (var i = 0; i < 7; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        },

        UploadPDF: async function (file, Dados) {




            let FileName = ''
            FileName = Dados.id_orcamento + '/' + this.makeid() + '.' + file.type.substr(12, 3);

            await S3UploadService.Upload(file, buckets.anexos_orcamentos, FileName).then(() => {
                $http.post(`${urlServidor.urlServidorChatAdmin}/compras/gravar-anexo`, { anexo: Dados.id_orcamento, id_orcamento: FileName, })
                    .then((response) => {
                        myFunctions.showAlert('PDF Anexado com sucesso')
                        location.reload()
                    })

            });






        },
        GetPDF: async (Dados) => {

            if (Dados.anexo === 'Anexar') {
                const a = document.getElementById("PDF")

                a.href = 'https://sistema.reidospatios.com.br/#/painel'

                myFunctions.showAlert('Anexo não fornecido')


            }



        },
        Liberar: async (Dados) => {
            console.log(Dados);
            if (Dados.autorizado === 'Autorizado') {
                myFunctions.showAlert('Orçamento Já Autorizado')

            } else {
                await $http.post(`${urlServidor.urlServidorChatAdmin}/compras/autoriza-orcamento`, { id_orcamento: Dados.id_orcamento, id_requisicao: Dados.id_requisicao, id_usuario: localStorage.getItem('id_usuario') })
                    .then(() => {
                        myFunctions.showAlert('Orçamento Autorizado com sucesso')
                        location.reload()
                    })
            }


        }
    }



    const initLancamentos = () => {
        $scope.MethodsLancamentos.fetchOrcamentos()

    }

    initLancamentos()



    $scope.incluiFornecedor = function incluiFornecedor() {


        $mdDialog.show({
            controller: function ($scope, $mdDialog, $mdToast, $rootScope, estados, myFunctions, viaCep) {

                $scope.fornecedor = {
                    razao_nome: '',
                    cpf_cnpj: '',
                    endereco: '',
                    bairro: '',
                    cidade: '',
                    tipo_pessoa: '',
                    id_tipo_fornecedor: 0,
                    cep: '',
                    uf: '',
                    email: '',
                    telefone_comercial: '',
                    status: '1',
                    idempresa: localStorage.getItem('id_empresa')
                }

                $scope.cancel = () => $mdDialog.cancel()

                var _novo = false;


                $scope.gravaFornecedor = async () => {


                    if (_novo) {

                        const response = await $http.post(urlServidor.urlServidorChatAdmin + '/fornecedores/cadastrar', $scope.fornecedor)
                        $rootScope.idFornecedor = response.data.insertId;

                    } else {

                        myFunctions.showAlert('CNPJ já cadastrado!')
                        $rootScope.idFornecedor = $scope.fornecedor.id;

                    }


                };

                $scope.saveAndClose = () => {
                    $scope.gravaFornecedor();
                    $rootScope._fornecedor = $scope.fornecedor;
                    $mdDialog.hide();

                };

                $scope.estados = estados.uf;

                $scope.buscaCep = async (cep) => {

                    if (cep.length == 9) {

                        const response = await viaCep.get(cep)
                        const { logradouro, bairro, uf } = response

                        $scope.fornecedor.endereco = logradouro.toUpperCase();
                        $scope.fornecedor.bairro = bairro.toUpperCase();
                        $scope.fornecedor.cidade = localidade.toUpperCase();
                        $scope.fornecedor.uf = uf.toUpperCase();
                    }

                }

                $scope.buscaCpfCnpj = async (cpfCnpj) => {

                    if (cpfCnpj.length == 14 || cpfCnpj.length == 18) {

                        const response = await $http.get(urlServidor.urlServidorChatAdmin + '/fornecedores/busca-cpfcnpj', { params: { cpf_cnpj: cpfCnpj } })
                        const { data } = response

                        $scope.motoristas = data;

                        if (response.data.length > 0) {
                            $scope.fornecedor.cep = data[0].cep;
                            $scope.fornecedor.nome = data[0].razao_nome;
                            $scope.fornecedor.endereco = data[0].endereco;
                            $scope.fornecedor.bairro = data[0].bairro;
                            $scope.fornecedor.cidade = data[0].cidade;
                            $scope.fornecedor.uf = data[0].uf;
                            $scope.fornecedor.email = data[0].email;
                            $scope.fornecedor.telefone = data[0].telefone_comercial;
                            $scope.fornecedor.id = data[0].id;
                            _novo = false;
                        } else {
                            _novo = true;
                        }



                    }

                }


            },
            templateUrl: 'cadastra-fornecedor.html',
            preserveScope: true
        }).then(function (answer) {
            let a = answer;
            buscaFornecedores().then(function (response) {
                $scope.$resolve.$scope.fornecedores = response;
                $scope.$resolve.$scope.dados.id_fornecedor = $rootScope.idFornecedor;
            });
        }, function () {
            $scope.statusdialog = 'You cancelled the dialog.';
        });

    };

})