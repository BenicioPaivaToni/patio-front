app.controller('veiculosCtrl', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.novo = function (ev, id) {
        $location.path('veiculos-novo');
    };

    $scope.dados = {
        empresa: null,
        descricao: null,
        placa: null
        
    }
    
    
    $scope.limpar = function (ev) {
        $scope.dados.empresa = null
        $scope.dados.descricao = null
        $scope.dados.placa = null
        $scope.dadosList = []
        
    };
    
    
    buscaEmpresas().then(function (result) {
        $scope.empresas = result

    })
    
    
    $scope.pesquisar = function (ev) {

        $scope.dadosList = []

        if(!$scope.dados.empresa && !$scope.dados.descricao && !$scope.dados.placa) {

            myFunctions.showAlert('Necessário preencher ao menos um campo para pesquisar!')

            return
        }

        $http.get(urlServidor.urlServidorChatAdmin + '/veiculos/listar', 
        
            {params: {  
                empresa: $scope.dados.empresa, 
                descricao: $scope.dados.descricao, 
                placa: $scope.dados.placa  
                }
            } 
            
            ).then(function (response) {

            let dados = response.data

            $scope.dadosList = dados
        })
    }


    
    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        $location.path('veiculos-editar').search({ dados })
    }
    
    
    function buscaEmpresas() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/empresas/listar', ).then(function (response) {
                 
                let dados = response.data
                resolve(dados)
            })
        })
    }


    $scope.gerarPlanilha = function () {

        if ($scope.dadosList){

            alasql('SELECT * INTO XLSX("veiculos.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);

        }else{

            myFunctions.showAlert("Sem dados para gerar Planilha!") 

        }
    }



    $scope.gerarPdf = function () {
        
        if ($scope.dadosList){
        
            var doc = new jsPDF({orientation: "landscape"});
            var totalPagesExp = '{total_pages_count_string}'

            doc.setFontSize(12);
            doc.autoTable({

                body: $scope.dadosList,
                columns: [
                    {header: 'Veiculo', dataKey: 'descricao'},
                    {header: 'Placa', dataKey: 'placa'},
                    {header: 'Ano', dataKey: 'ano'},
                    {header: 'Cor', dataKey: 'cor'},
                    {header: 'Combustivel', dataKey: 'combustivel'},
                    {header: 'Chassi', dataKey: 'chassi'},
                    {header: 'Ativo', dataKey: 'desc_ativo'},
                ],
                bodyStyles: {
                    margin: 10,
                    fontSize: 8,
                },
                didDrawPage: function (data) {
                    // Header
                    doc.setFontSize(18)
                    doc.setTextColor(40)
                    doc.setFontStyle('normal')

                    // Footer
                    var str = 'Pagina ' + doc.internal.getNumberOfPages()
                    // Total page number plugin only available in jspdf v1.0+
                    if (typeof doc.putTotalPages === 'function') {
                        str = str + ' de ' + totalPagesExp
                    }

                    doc.setFontSize(10)

                    // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                    var pageSize = doc.internal.pageSize
                    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                    doc.text(str, data.settings.margin.left, pageHeight - 10)
                },
                margin: {top: 30},
            });
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp)
            }

            doc.save('veiculos.pdf')

        }else{

            myFunctions.showAlert("Sem dados para gerar PDF!") 

        }
    }


});