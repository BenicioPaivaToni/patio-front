'use strict';
angular.module('app')
.controller("motoristasNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions,estados,viaCep,$sce, urlImagens,buckets) {  

  $scope.urlImagens = urlImagens.urlDocumentosMotoristas;


  $scope.dados = {
    empresaReboque:'',
    nome:'',  
    apelido: '' ,
    email: '' ,
    celular: '' ,
    cnh: '' ,
    cnh_categoria: '' ,
    cnh_validade: '' ,
    categoria: '' ,
    ativo: '',
    observacao: '',
    endereco: '',
    bairro:'' ,
    cidade: '' ,
    cep: '',
    uf: '',
    inicio_contrato: '',
    fim_contrato: '', 
    pdf_contrato:'',
    pdf_cnh:'' ,
    idempresa: localStorage.getItem('id_empresa'),
    idusuario_app: ''
 }


 var bucket = null;

 $scope.trustSrc = function(src) {
   return $sce.trustAsResourceUrl(src);
 }  
 
 $scope.arquivoContrato = '' ;
 $scope.arquivoCnh = '';

 $scope.estados = estados.uf ;

 var urlParams = $location.search();
 
 $http.get(urlServidor.urlServidorChatAdmin+'/autoridades/vercodigo').then( function(response){

   AWS.config.update({ accessKeyId: response.data.ac, secretAccessKey: response.data.sc });
   AWS.config.region = response.data.regiao;

 }).finally( function(){

   bucket = new AWS.S3();
$http.get(urlServidor.urlServidorChatAdmin + '/usuarios/usuarios-app-motorista', {params: {idEmpresa: localStorage.getItem('id_empresa')}}).then(function (response) {
       $scope.usuariosapp = response.data;
   });
/*if (urlParams.dados[0].pdf_contrato != undefined) {

       bucket.getObject(
               {Bucket: "documentos-reidospatios", Key: urlParams.dados[0].pdf_contrato, ResponseContentDisposition: 'inline'},
               function (error, data) {
                   if (error != null) {
                       myFunctions.showAlert("Falha no carregamento do PDF: " + error);
                   } else {

                       $scope.pdfContrato = new Blob([data.Body], {type: "application/pdf"});
                   }
               }
       );
   }

   if (urlParams.dados[0].pdf_cnh != undefined) {

       bucket.getObject(
               {Bucket: "documentos-reidospatios", Key: urlParams.dados[0].pdf_cnh, ResponseContentDisposition: 'inline'},
               function (error, data) {
                   if (error != null) {
                       myFunctions.showAlert("Falha no carregamento do PDF , arquivo invalido ou inexistente!");
                   } else {

                       $scope.pdfCnh = new Blob([data.Body], {type: "application/pdf"});
                       
                   }
               }
       );
   }*/
   
});


  buscaTipoVeiculos().then((data) => {
    $scope.tiposVeiculos = data
  })


    
  $scope.adicionarIndice = function () {
  
    if ($scope.indicesList) {
        
      let tipoVeiculo = $scope.indicesList.filter((x) => x.id_tipo_veiculo === $scope.indices.tipo_veiculo)[0]

      if ( tipoVeiculo ) {

        myFunctions.showAlert( 'Já existe um indice para o Tipo de Veiculo selecionado! Verifique!' )

        return

      }
    
      const addId=(arr)=> {
        return arr.map((obj, index)=>  {
          return ({...obj, id: index + 1})
        })
      }
    
    
      let newArray = [... $scope.indicesList, {
        tipo_veiculo: $scope.tiposVeiculos.find((x) => x.id === $scope.indices.tipo_veiculo).descricao,
        id_tipo_veiculo: $scope.tiposVeiculos.find((x) => x.id === $scope.indices.tipo_veiculo).id,
        km_excedente: $scope.indices.km_excedente,
        pedagio: $scope.indices.pedagio,
        extras: $scope.indices.extras
            
      }]
    
      $scope.indicesList = addId(newArray)

      }else{

        const dados = []

        const veiculo = $scope.tiposVeiculos.find((item) => item.id === $scope.indices.tipo_veiculo)

        dados.push({
          id: 1,
          tipo_veiculo: veiculo.descricao,
          id_tipo_veiculo: veiculo.id,
          km_excedente: $scope.indices.km_excedente,
          pedagio: $scope.indices.pedagio,
          extras: $scope.indices.extras

        })  

        $scope.indicesList = dados
    
      }

    $scope.limparIndices()

  }

  

  $scope.limparIndices = function(){

    $scope.indices.tipo_veiculo = ''
    $scope.indices.km_excedente = "R$0,00"
    $scope.indices.pedagio = "R$0,00"
    $scope.indices.extras = "R$0,00"

  }


  $scope.removerIndice = function (index){

    $scope.indicesList.splice(index, 1)
  
    console.log("Indices: ", $scope.indicesList)

    
  }



  $scope.buscaCep = (cep) => {

    if(cep.length == 9){

      viaCep.get(cep).then(function(response){
      
        $scope.dados.endereco = response.logradouro.toUpperCase() ;
        $scope.dados.bairro = response.bairro.toUpperCase() ;
        $scope.dados.cidade = response.localidade.toUpperCase() ;
        $scope.dados.uf = response.uf.toUpperCase() ;				 

      });
    }	
  }  
 


 $scope.uploadContrato = (file) => {   
   $scope.arquivoContrato = file ;
   $scope.dados.pdf_contrato = file.name;
   $scope.pdfContrato = file ;
   if( $scope.arquivoContrato.name != undefined  ){

     var params = {
         Key: $scope.arquivoContrato.name,
         ContentType: $scope.arquivoContrato.type ,
         ContentDisposition: 'inline',
         Body: $scope.arquivoContrato ,
         ACL: 'public-read' ,
         Bucket: buckets.motoristas

     };
     $scope.pdfContrato = null;
     $scope.arquivoContrato = null;
     $scope.dados.pdf_contrato = null;
     bucket.putObject(params, function(err, data) {
         if (err) {
           myFunctions.showAlert('Erro envio PDF Contrato, verifique...' + err);
         }else{
           setTimeout(function(){

             myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 

           }, 1500);
           $scope.arquivoContrato = file ;
           $scope.dados.pdf_contrato = file.name;
           $scope.pdfContrato = file ;
         };
         
     });
    
   };
 }
 
  

 $scope.uploadCnh = (file) => { 
   $scope.arquivoCnh = file ;
   $scope.dados.pdf_cnh = file.name;
   $scope.pdf_cnh = file ;

   if( $scope.arquivoCnh.name != undefined  ){

     var paramsChecklist = {
       Key: $scope.arquivoCnh.name,
       ContentType: $scope.arquivoCnh.type ,
       ContentDisposition: 'inline',
       Body: $scope.arquivoCnh ,
       ACL: 'public-read',
       Bucket: buckets.motoristas

     };
     $scope.arquivoCnh = null ;
     $scope.dados.pdf_cnh = null;
     $scope.pdf_cnh = null ;
     bucket.putObject(paramsChecklist, function(err, data) {
         if (err) {
           myFunctions.showAlert('Erro envio PDF Checklist, verifique...' + err);
         }else{

           setTimeout( function () {
    
             myFunctions.showAlert('Alteração executada com sucesso!');

           },1500); 

               $scope.pdfCnh = file;
               $scope.arquivoCnh = file;
               $scope.dados.pdf_cnh = file.name; 
         }
     });                

 };   
   
};
 

 buscaEmpresas().then( function(result){
    $scope.empresas = result ;
 });  


  $scope.isDisabled = false


  $scope.gravar = function() {        
  
    const body = {dados: $scope.dados, indices: $scope.indicesList}

    $http.post(urlServidor.urlServidorChatAdmin+'/motoristas/cadastrar', body ).then( function(response){

      if (response.data.code){
           
        myFunctions.showAlert('Erro na gravação!')

      }else{

      if ( response.data == '11000' )                 
        {
          myFunctions.showAlert('Motorista já existe!')
        }else{                    

          myFunctions.showAlert( 'Cadastro executado com sucesso!' )
          $scope.isDisabled = true 

        }  
      }
    })
  }



  function buscaEmpresas(){

    return new Promise( function( resolve,reject){

      $http.put(urlServidor.urlServidorChatAdmin+'/empresas_reboques/listar').then( function(response){

        let dados = response.data ;

        dados.sort(function(a, b){
          if (a.nome < b.nome) {
            return 1;
          }

          if (a.nome > b.nome) {
            return -1;
          }        
          return 0;
        });       

        resolve(dados) ;

      });

    })

  }


  function buscaTipoVeiculos() {
    return new Promise(function (resolve, reject) {
      $http.get(urlServidor.urlServidorChatAdmin + '/tipos_veiculo/listar', {params: {idEmpresa: localStorage.getItem('id_empresa')}}).then(function (response) {
             
        let dados = response.data
        resolve(dados)
      })
    })
  }


    
})