
app.controller('funcionariosCtrl', function ($scope, $location, $http, urlServidor) {

    $scope.novo = function (ev, id) {
        $location.path('funcionarios-novo');
    };
    $scope.limpar = function (ev) {
        $scope.dados.empresa = '';
        $scope.dados.nome = '';
        $scope.dados.departamento = '';
    };
   
    $scope.pesquisar = function (ev) {
        $scope.dadosList =null;

        $http.get(urlServidor.urlServidorChatAdmin + '/funcionarios/listar', {params:  $scope.dados}).then(function (response) {

            let dados = response.data;
           

            $scope.dadosList = dados;
        });
    };

    buscaEmpresas().then(function (result) {
        $scope.empresas = result
      
    })


    function buscaEmpresas() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/empresas/listar', ).then(function (response) {
                 
                let dados = response.data
                resolve(dados)
            })
        })
    }


   
    $scope.detalhe = function (ev, rowId) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == rowId
        })
        window.open('/#/funcionarios-editar?rowId '+ rowId,'_blank');
    };
    /*$scope.excluir = function (ev, rowId, Id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        //window.open('/#/funcionarios-editar?id '+ id,'_blank');
        //$location.path('motoristas-editar').search({dados});
    };*/
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("funcionarios.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({orientation: "landscape"});
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: {empresa: {halign: 'left'},
                nome: {halign: 'left'},
                endereco: {halign: 'left'},
                },
            body: $scope.dadosList,
            columns: [
                {header: 'Nome', dataKey: 'nome'},
                {header: 'Empresa', dataKey: 'empresa'},
                {header: 'Cidade', dataKey: 'cidade'+'-'+'uf'},
                {header: 'Celular', dataKey: 'celular'},
                {header: 'Departamento', dataKey: 'departamento'},
                {header: 'Tipo de Contrato', dataKey: 'tipo_contratacao'},
                {header: 'Admissão', dataKey: 'data_admissao'},
                {header: 'Férias', dataKey: 'data_ferias'},
                {header: 'Experiência', dataKey: 'data_experiencia'},
                {header: 'Demissão', dataKey: 'data_demissao'},
              
                
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 8,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

               // doc.text('Relátorio de funcionários')

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: {top: 30},
        });

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        } 
       
        doc.save('funcionarios.pdf')
    }


});
