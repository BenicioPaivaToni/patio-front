'use strict';
angular.module('app')
.controller("categoriasNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions) {  

  $scope.dados = {
     decricao:'',
     tipo: '',
     ativo:''
  }

  buscaGrupos().then( function(result){
		$scope.grupos = result ;
	});

  $scope.isDisabled = false

  $scope.gravar = function() {        

    $http.post(urlServidor.urlServidorChatAdmin+'/categorias/cadastrar', $scope.dados ).then( function(response){

        if (response.data.code){
            
          myFunctions.showAlert('Erro na gravação!')

        }else{

          if ( response.data == '11000' )                 
          {
            myFunctions.showAlert('Categoria já existe!')
          }else
          {                    

            myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
            $scope.isDisabled = true ;            

          }  
                
        }; 

    }); 

  }; 

	function buscaGrupos(){	
		return new Promise( function( resolve,reject){
			$http.put(urlServidor.urlServidorChatAdmin+'/grupos/listar').then( function(response){
				let dados = response.data ;
				resolve(dados) ;
			});
		})
	}	  
    
});