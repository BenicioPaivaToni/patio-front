angular
  .module("app")
  .controller(
    "transferenciaVeiculosCtrl",
    function ($scope, $http, myFunctions, urlServidor, $mdDialog) {
      $scope.dados = {
        id_patio: "",
        ncv_placa: "",
        data_transferencia: new Date(),
      };

      $scope.alterardados = {
        id_patio: "",
        ncv_placa: "",
        data_transferencia: new Date(),
        id_motorista: '',
        id_reboque:''
      };

      $scope.recebe_transferencia =  localStorage.getItem('recebe_transferencia');

      $scope.Disable = true;
      $scope.ncv = 0;

      $scope.id = 0;
      $scope.isCanceled = false ;
      $scope.isRecebido = false ;
      $scope.usuarioCancelamento = '' ;
      $scope.dataCancelamento = '' ;
      $scope.motivoCancelamento = '' ;

      $scope.usuarioRecebimento = '' ;
      $scope.dataRecebimento = '' ;

      $scope.Dados = [];

      $http.put(urlServidor.urlServidorChatAdmin + '/motoristas/listar', {idUsuario: localStorage.getItem('id_usuario'), ativo: '1' }).then(function (response) {
        $scope.motoristas = response.data;
      });

      $http.put(urlServidor.urlServidorChatAdmin + '/reboques/listar-ativos').then(function (response) {
        $scope.reboques = response.data;
      });

      $scope.pesquisar = {

        CancelarTransferencia: async () => {

          $mdDialog.show({
              controller: function ($scope, $mdDialog, $mdToast) {
  
                  $scope.cancelar_transferencia = function () {

                    if( $scope.dados.motivo_cancelamento !== undefined ){
                      $http.post(urlServidor.urlServidorChatAdmin + '/transferencias-veiculos/cancela-transferencia', { id: $scope.id, id_usuario_cancelamento: localStorage.getItem('id_usuario') , motivo_cancelamento: $scope.dados.motivo_cancelamento }).then(function (response) {
                          if (response.data.code) {
                              myFunctions.showAlert('Erro na gravação!')
                          } else {
                              myFunctions.showAlert('Cancelado com sucesso!');
                              buscaTransferencias();
                              $scope.disabled_cancelar = true;
                          }
                          ;
                      });

                      $scope.fechar()

                    } else {

                      myFunctions.showAlert('Descreva o motivo do cancelamento!');

                    }  

                  }
              
                  $scope.fechar = function () {
                      $mdDialog.cancel();
                  };
  
  
              },
              templateUrl: 'cancelar-transferencia.html',
              scope: $scope,
              preserveScope: true,
              scopoAnterior: $scope
          }).then(function (answer) {
              let a = answer;
  
          }, function () {
              $scope.statusdialog = 'You cancelled the dialog.';
          });
  
      },
  

        buscaTransferencias: async () => {
          let numeroRomaneio =  $scope.dados.numero_romaneio
          if ( !numeroRomaneio )
         {
            myFunctions.showAlert(
              "Informar numero do Romaneio"
            );
         
          } else {
            let DadosTransferencias = await $http.get(
              `${urlServidor.urlServidorChatAdmin}/transferencias-veiculos/listar-transferencias`,
              {
                params: {
                  numeroRomaneio                                
                },
              }
            );

            $scope.nomeMotorista = DadosTransferencias.data[0].motorista 
            $scope.nomeReboque = DadosTransferencias.data[0].reboque
            $scope.dataTransferencia = DadosTransferencias.data[0].data_transferencia
            $scope.dataTransferenciaII = DadosTransferencias.data[0].data_transferenciaII 

            $scope.nomeUsuario = DadosTransferencias.data[0].nome_usuario 
            $scope.dataUsuario = DadosTransferencias.data[0].data_usuario 
            $scope.id = DadosTransferencias.data[0].id_romaneio 

            $scope.isCanceled =  ( DadosTransferencias.data[0].id_usuario_cancelamento !== null ) ;
            $scope.usuarioCancelamento = DadosTransferencias.data[0].usuario_cancelamento ;
            $scope.dataCancelamento = DadosTransferencias.data[0].data_cancelamento ;
            $scope.motivoCancelamento = DadosTransferencias.data[0].motivo_cancelamento ;

            $scope.usuarioRecebimento = DadosTransferencias.data[0].usuario_recebimento ;
            $scope.dataRecebimento = DadosTransferencias.data[0].data_recebimento ;

            $scope.isRecebido = ( DadosTransferencias.data[0].usuario_recebimento !== null ) ;

            const { data } = DadosTransferencias;

            const mappedInformations = data.map((Registros) => ({
                ...Registros,
                divergencia: Registros.divergencia === 1 ? "Sim" : "Não",
                observacao: Registros.observacao,

            }));

            $scope.dadosList = mappedInformations;

            $scope.Disable = false;
          }
        },

        buscaTransferenciasAlteracao: async () => {
          let numeroRomaneio =  $scope.alterardados.numero_romaneio
          if ( !numeroRomaneio )
         {
            myFunctions.showAlert(
              "Informar numero do Romaneio"
            );
         
          } else {
            const response = await $http.get(
              `${urlServidor.urlServidorChatAdmin}/transferencias-veiculos/listar-transferencias`,
              {
                params: {
                  numeroRomaneio                                
                },
              }
            );
            const { data } = response;

            $scope.dadosListAlteracao = data;

            $scope.Disable = false;
          }
        },
    

        BuscaNcvPlaca: async (id_patio_origem,id_patio_destino,placa,id_reboque,id_motorista,data) => {
          if (!placa || !id_patio_origem || !id_patio_destino || !id_reboque || !id_motorista || !data) {
            myFunctions.showAlert(`Informe todos os dados antes de inserir veiculo!`);
          } else {
            const response = await $http.get(
              `${urlServidor.urlServidorChatAdmin}/transferencias-veiculos/ncv-placa`,
              {
                params: {
                  ncv_placa: placa,
                  id_patio: id_patio_origem,
                },
              }
            );

            const data = response.data[0];

            if (data == undefined) {
              myFunctions.showAlert(
                `Veiculo não encontrato! Verifique se foi digitado corretamente e se o mesmo é do pátio selecionado!`
              );
            } else {
              $scope.ncv = response.data[0].ncv;

              const txt = `
                        <div>
                        <header>
                    
                            <title>
                            
                            </title>
                    
                        </header>
                        </br>
                    
                        <body> 
                        
                        <div> 
                            <div>NCV :</div>
                            <div><strong>${data.ncv}</strong></div>
                        </div>
                        </br>

                        <div> 
                            <div>PLACA :</div>
                            <div><strong>${data.placa}</strong></div>
                        </div>
                        </br>

                        <div> 
                        <div>MARCA/MODELO :</div>
                        <div><strong>${data.marca_modelo || 0}</strong></div>
                        </div>

                        </br>

                        <div> 
                            <div>ANO :</div>
                            <div><strong>${data.ano || 0}</strong></div>
                        </div>
                        </br>
                    

                        <div> 
                            <div>NÚMERO DO MOTOR :</div>
                            <div><strong>${
                              data.numero_motor || 0
                            }</strong></div>
                        </div>
                        </br>
                        
                        <div> 
                            <div>CHASSI :</div>
                            <div><strong>${data.chassi || 0}</strong></div>
                        </div>
                        </br>
                        </body>
                        
                        </div>
                    
                        `;

              let confirm = $mdDialog
                .confirm()
                .ariaLabel("Lucky day")
                .title("Confira os dados do veículo antes de gravar")
                .ok("GRAVAR")
                .cancel("Cancelar")
                .htmlContent(txt);
              $mdDialog.show(confirm).then(() => {

                $scope.Dados.push( response.data[0] )

              });
            }
          }
        },

        limpar: (ev) => {
          delete $scope.dados;
        },

        Apagar: async (rowid) => {
          let id_transferencia = rowid;

          console.log("Apagar: ", id_transferencia);
          let confirm = $mdDialog
            .confirm()
            .ariaLabel("Lucky day")
            .title(
              "Deseja remover permanentemente este item da lista de transferências?"
            )
            .ok("Apagar")
            .cancel("Cancelar");
          $mdDialog.show(confirm).then(() => {
            $http
              .post(
                `${urlServidor.urlServidorChatAdmin}/transferencias-veiculos/apagar`,
                { id: id_transferencia }
              )
              .then((response) => {
                myFunctions.showAlert(
                  `Veiculo removido com sucesso da lista de transferências!`
                );

                $scope.pesquisar.buscaTransferencias();
              });
          });
        },
      };

      $scope.DadosPeriodo = {
        ApreeAte: new Date(),
        data: new Date(),
        id_patio_origem: "",
        id_patio_destino: "",
      };

      $http
        .put(
          urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
          {
            idUsuario: localStorage.getItem("id_usuario"),
            idEmpresa: localStorage.getItem("id_empresa"),
          }
        )
        .then(({ data }) => {
          $scope.patios = data.filter((item) => {
            return item.status == !0;
          });
        });

      $scope.SeparacaoPeriodo = {
        hoje: new Date(),

        //Methodos
        mesAtual: function () {
          $scope.DadosPeriodo.ApreeDe = new Date(
            this.hoje.getFullYear(),
            this.hoje.getMonth(),
            01
          );
          $scope.DadosPeriodo.ApreeAte = new Date(
            this.hoje.getFullYear(),
            this.hoje.getMonth() + 1,
            0
          );
        },

        //Busca NCV ou Placa
        Buscar: async () => {
          if (
            (!$scope.DadosPeriodo.id_patio_origem &&
              !$scope.DadosPeriodo.id_patio_destino) ||
            !$scope.DadosPeriodo.id_patio_origem ||
            !$scope.DadosPeriodo.id_patio_destino
          ) {
            myFunctions.showAlert(
              "Pátio de Origem e Pátio de Destino precisam ser preenchidos! Verifique!"
            );
          } else if (
            $scope.DadosPeriodo.id_patio_origem ===
            $scope.DadosPeriodo.id_patio_destino
          ) {
            myFunctions.showAlert(
              "Pátio de Origem e Pátio de Destino não podem ser iguais! Verifique!"
            );
          } else {
            const response = await $http.get(
              `${urlServidor.urlServidorChatAdmin}/transferencias-veiculos/listar-periodo`,
              {
                params: {
                  data_final: $scope.DadosPeriodo.ApreeAte,
                  id_patio: $scope.DadosPeriodo.id_patio_origem,
                },
              }
            );
            const { data } = response;

            $scope.Dados = data;
          }
        },

        //Limpa os campos tela
        limpar: function (ev) {
          $scope.DadosPeriodo.ApreeAte = null;
          $scope.DadosPeriodo.data = null;
          $scope.DadosPeriodo.id_patio_origem = null;
          $scope.DadosPeriodo.id_patio_destino = null;
          $scope.DadosPeriodo.id_reboque = null;
          $scope.DadosPeriodo.id_motorista = null ;         

          $scope.Dados = "";
        },

        LimparResultado: () => {
          $scope.Dados = "";
        },

        //Botão de selecionar todos ou desmarcar todos
        Selecionar: "SELECIONAR TODOS",

        SelecionarTodos: function (Dados) {
          if (this.Selecionar === "SELECIONAR TODOS") {
            Dados.forEach((dados) => {
              dados.selecionado = true;
            });
            this.Selecionar = "DESMARCAR TODOS";
          } else {
            Dados.forEach((dados) => {
              dados.selecionado = false;
            });
            this.Selecionar = "SELECIONAR TODOS";
          }
        },

        gravaConfirmacao: function (Dados,Dado) {

          if( Dados.selecionado){

            $http.post(urlServidor.urlServidorChatAdmin + 
                      '/transferencias-veiculos/grava-confirmacao',
                      { dados: { id: Dados.id_transferencia, id_usuario_confirmacao: localStorage.getItem("id_usuario")  } })
            .then(function (response){

            })  

          } else {

            $http.post(urlServidor.urlServidorChatAdmin + 
              '/transferencias-veiculos/cancela-confirmacao',
              { dados: { id: Dados.id_transferencia  } })
            .then(function (response){

            })  

          }

        },  

        confirmarTransferencia: async (Dados,Dado) => {

          const result = await Dados.filter((Veiculo) => {
            return Veiculo.selecionado;
          });

          var transferencia = await result.map((x) => {
            return {
              patio_destino: $scope.DadosPeriodo.id_patio_destino,
              ncv: x.id,
              id_romaneio : x.id_romaneio 
            };
          });

          if (result.length > 0) {

            $http.post(urlServidor.urlServidorChatAdmin + 
                      '/transferencias-veiculos/grava-recebimento',
                      { dados: { id: $scope.dados.numero_romaneio, transferencia: transferencia , id_usuario_recebimento : localStorage.getItem("id_usuario")  } })
            .then(function (response){

              myFunctions.showAlert(
                `Recebimento/Retençāo gravada com sucesso!`
              );
              $scope.pesquisar.buscaTransferencias();

            })  

          } else {
            return myFunctions.showAlert(
              `Nenhum veiculo foi selecionado, Verifique!`
            );
          }  

        }, 

        gravaObservacao: function (Dados) {

            $http.post(urlServidor.urlServidorChatAdmin + 
                      '/transferencias-veiculos/grava-observacao',
                      { dados: { id: Dados.id_transferencia, obervacao: Dados.observacao  } })
            .then(function (response){

            })  

        },  

        //Botão de gravar dados selecionados
        Inserir: async (Dados) => {

          $scope.nomeMotorista = '' 
          $scope.nomeReboque = ''

          const result = Dados.filter((Veiculo) => {
            return Veiculo.selecionado;
          });

          if (result.length > 0) {

            let idRomaneio = 0 

            $http.post(urlServidor.urlServidorChatAdmin + '/transferencias-veiculos/grava-romaneio-transferencia', { dados: { id_usuario: localStorage.getItem("id_usuario"), id_reboque: $scope.DadosPeriodo.id_reboque , id_motorista:  $scope.DadosPeriodo.id_motorista } }).then(function (response){

              idRomaneio = response.data

            }).finally(async function(){ 

              var transferencia = result.map((x) => {
                return {
                  patio_origem: $scope.DadosPeriodo.id_patio_origem,
                  patio_destino: $scope.DadosPeriodo.id_patio_destino,
                  id_usuario: localStorage.getItem("id_usuario"),
                  ncv: x.id,
                  data: $scope.DadosPeriodo.data ,
                  id_romaneio : idRomaneio 
                };
              });

              let responseRomaneio = await $http.post(
                `${urlServidor.urlServidorChatAdmin}/transferencias-veiculos/grava-veiculos-romaneio`,
                { dados: transferencia }
              );

              let responseMotoristas = await $http.get(
                `${urlServidor.urlServidorChatAdmin}/motoristas/buscar-id`,
                { params : { id: $scope.DadosPeriodo.id_motorista } }
              )  ;
              let responseReboque = await $http.get(
                `${urlServidor.urlServidorChatAdmin}/reboques/buscar-id`,
                { params : { id: $scope.DadosPeriodo.id_reboque } }
              )
              
              let responsePatio = await $http.get(
                `${urlServidor.urlServidorChatAdmin}/patios/buscar-id`,
                { params : { id: $scope.DadosPeriodo.id_patio_destino } }
              )

              $scope.changeStatusPatio(transferencia);

              let confirm = $mdDialog.confirm()
              .title('Gravar Romaneio de Transporte.')
              .textContent('ID do Romaneiro Gerado : '+idRomaneio)
              .ariaLabel('Lucky day')
              .ok('Ok')
              $mdDialog.show(confirm).then(function () {

                $scope.gerarPDFRomaneio(result,idRomaneio,responsePatio.data.nome,responseMotoristas.data.nome,responseReboque.data.descricao,$scope.DadosPeriodo.data);
                $scope.Dados = "";
                $scope.Dado = "";

              });  

            })

          } else {
            return myFunctions.showAlert(
              `Nenhum veiculo foi selecionado, Verifique!`
            );
          }

//          $scope.Dados = "";    

        },

      };


      

    $scope.gerarPlanilhaPerido = () => {
        let patioDestino = $scope.patios.filter(function (idx) {
          return idx.id === $scope.DadosPeriodo.id_patio_destino;
        });

        patioDestino = patioDestino.map((i) => {
          return {
            patio_destino: i.nome,
          };
        });

        let patioName = Object.assign({}, ...patioDestino);

        var excelSelecao = $scope.Dados.map((x) => {
          return {
            patio_origem: x.patio,
            patio_destino: patioName.patio_destino,
            ncv: x.id,
            placa: x.placa,
            data_apreensao: x.data_apreensao,
            marca_modelo: x.marca_modelo,
            cor: x.cor,
            ano: x.ano,
            hora_apreensao: x.hora_apreensao,
            km_percorrido: x.km_percorrido,
            tipo_veiculo: x.tipo_veiculo,
          };
        });

        alasql(
          'SELECT * INTO XLSX("Relatorio-transferencia-veiculos.xlsx",{headers:true}) FROM ?',
          [excelSelecao]
        );
      };

      $scope.gerarPDFRecebimento = async (Dados,dados,dataTransferencia) => {

        const result = Dados.filter((Veiculo) => {
          return Veiculo.selecionado;
        });
        
        $scope.gerarPDFRomaneio($scope.dadosList ,dados.numero_romaneio, $scope.dadosList[0].patio_destino ,$scope.nomeMotorista ,$scope.nomeReboque,$scope.dataTransferenciaII);

      }

      $scope.gerarPDFRomaneio = function (dados,Romaneio, nomePatioDestino,nomeMotorista,nomeReboque,dataTransf) {
        $scope.Dados =  dados ;

        let dataAtual = new Date().toISOString().slice(0, 10)
        let dataTransferencia = new Date(dataTransf).toISOString().slice(0, 10)

        $scope.nome_reboque = nomeReboque  
        $scope.nome_motorista = nomeMotorista

        var PDFSelecao = $scope.Dados.map((x) => {
          return {
            patio_origem: x.patio,
            patio_destino: nomePatioDestino,
            ncv: x.id,
            placa: x.placa, 
            data_apreensao: x.data_apreensao,
            marca_modelo: x.marca_modelo,
            cor: x.cor,
            ano: x.ano,
            hora_apreensao: x.hora_apreensao,
            tipo_veiculo: x.tipo_veiculo,
          };
        });

        var doc = new jsPDF({ orientation: "landscape" });

        var totalPagesExp = "{total_pages_count_string}";

        doc.setFontSize(10);

        doc.autoTable({
          columnStyles: {
            vencimento: { halign: "left" },
          },

          body: PDFSelecao,
          columns: [
            { header: "Patio Origem", dataKey: "patio_origem" },
            { header: "Patio Destino", dataKey: "patio_destino" },
            { header: "NCV", dataKey: "ncv" },
            { header: "Ano", dataKey: "ano" },
            { header: "Data Apreensão", dataKey: "data_apreensao" },
            { header: "Hora Apreensão", dataKey: "hora_apreensao" },
            { header: "Marca/Modelo", dataKey: "marca_modelo" },
            { header: "Placa", dataKey: "placa" },
            { header: "Tipo do Veiculo", dataKey: "tipo_veiculo" },
          ],
          bodyStyles: {
            margin: 10,
            fontSize: 08,
          },

          didDrawPage: function (data) {
            // Header
            doc.setFontSize(12);
            doc.setTextColor(40);
            doc.setFontStyle("normal");

            doc.text(
              "Romaneio : " + Romaneio+'     Motorista : '+ nomeMotorista+'   Transp.: '+ nomeReboque+'    Data Trasf.: '+dataTransferencia.substr(8, 2)+'/'+dataTransferencia.substr(5, 2)+'/'+dataTransferencia.substr(0, 4)  ,
              data.settings.margin.left + 10,
              22
            );

            // Footer
            var str = "Pagina " + doc.internal.getNumberOfPages();
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === "function") {
              str = str + " de " + totalPagesExp;
              str = str + "                                                                           Data "+dataAtual.substr(8, 2)+'/'+dataAtual.substr(5, 2)+'/'+dataAtual.substr(0, 4)+"                  Usuário "+localStorage.getItem("nome_usuario")
              var totalpaginas = totalPagesExp;
            }

            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height
              ? pageSize.height
              : pageSize.getHeight();

            doc.text(
              str + "                        ",
              data.settings.margin.left,
              pageHeight - 10
            );
          },
          margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === "function") {
          doc.putTotalPages(totalPagesExp);
        }

        doc.save("transferencia-veiculos.pdf");
      };





      $scope.gerarPlanilha = () => {
        const patioDestino = $scope.patios.find((item) => item.id === $scope.dados.patio_destino);

        const excelAjustes = $scope.dadosList.map((item) => ({
            patio_origem: item.patio,
            patio_destino: patioDestino.nome,
            ncv: item.id,
            placa: item.placa,
            data_apreensao: item.data_apreensao,
            marca_modelo: item.marca_modelo,
            cor: item.cor,
            ano: item.ano,
            hora_apreensao: item.hora_apreensao,
            km_percorrido: item.km_percorrido,
            tipo_veiculo: item.tipo_veiculo,
            observacao: item.observacao || 'Não',
            divergencia: item.divergencia || 'Não',
        }));


        alasql(
          'SELECT * INTO XLSX("relatorio-transferencia-veiculos-patio.xlsx",{headers:true}) FROM ?',
          [excelAjustes]
        );
      };

      $scope.changeStatusPatio = function(itens){

        var noPatio = 0

        for(let i=0; i< itens.length; i++){
          $http.post(urlServidor.urlServidorChatAdmin + '/ncv/altera-patio', {id: itens[i].ncv, no_patio: noPatio }).then(function (response) {
           
            if (response.data.code) {
              $scope.error = response.data
            }
          })
        }
      }

      
      $scope.gerarPDF = function () {
        const patioDestino = $scope.patios.find((item) => item.id === $scope.dados.patio_destino);



        const PDFAjustes = $scope.dadosList.map((item) => ({
            patio_origem: item.patio,
            patio_destino: patioDestino.nome,
            ncv: item.id,
            placa: item.placa,
            data_apreensao: item.data_apreensao,
            marca_modelo: item.marca_modelo,
            cor: item.cor,
            ano: item.ano,
            divergencia: item.divergencia || 'Não',
        }));

        var doc = new jsPDF({ orientation: "landscape" });

        var totalPagesExp = "{total_pages_count_string}";

        doc.setFontSize(10);

        doc.autoTable({
          columnStyles: {
            vencimento: { halign: "left" },
          },

          body: PDFAjustes,
          columns: [
            { header: "Patio Origem", dataKey: "patio_origem" },
            { header: "Patio Destino", dataKey: "patio_destino" },
            { header: "NCV", dataKey: "ncv" },
            { header: "Placa", dataKey: "placa" },
            { header: "Data de Apreensão", dataKey: "data_apreensao" },
            { header: "Marca/Modelo", dataKey: "marca_modelo" },
            { header: "Cor", dataKey: "cor" },
            { header: "Ano", dataKey: "ano" },
            { header: "Divergencias", dataKey: "divergencia" },

          ],
          bodyStyles: {
            margin: 10,
            fontSize: 08,
          },

          didDrawPage: function (data) {
            // Header
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle("normal");

            doc.text(
              "Relatório de transferências de veículos",
              data.settings.margin.left + 15,
              22
            );

            // Footer
            var str = "Pagina " + doc.internal.getNumberOfPages();
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === "function") {
              str = str + " de " + totalPagesExp;
              var totalpaginas = totalPagesExp;
            }

            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height
              ? pageSize.height
              : pageSize.getHeight();

            doc.text(
              str + "                        ",
              data.settings.margin.left,
              pageHeight - 10
            );
          },
          margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === "function") {
          doc.putTotalPages(totalPagesExp);
        }

        doc.save("relatorio-transferencia-veiculos.pdf");
      };
    }
  );
