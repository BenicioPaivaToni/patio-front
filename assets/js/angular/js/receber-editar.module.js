angular.module("app").controller('receberEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep, $filter) {



    const queryString = window.location.hash;

    var _id = queryString.slice(queryString.indexOf("%") + 3)


    $http.get(`${urlServidor.urlServidorChatAdmin}/receber/busca-id`, { params: { id_receber: _id } }).then(({data}) => {
        [$scope.dados] = data.map(Item => ({
            ...Item,
            patio:Item.id_patio,
            vencimento: new Date(Item.vencimento.substr(3, 2) + '-' + Item.vencimento.substr(0, 2) + '-' + Item.vencimento.substr(6, 4)),
            data_venda: Item.data_venda ? new Date(Item.data_venda.substr(3, 2) + '-' + Item.data_venda.substr(0, 2) + '-' + Item.data_venda.substr(6, 4)) : '',
            data_pagto: new Date(Item.data_pagto.substr(3, 2) + '-' + Item.data_pagto.substr(0, 2) + '-' + Item.data_pagto.substr(6, 4))
        }))

        // Vamos esconder nossa mensagem de erro e tambem o container
        document.getElementById("Container").style.display = "none"
        document.getElementById("ErrorLength").style.display = "none"


        // Caso o usuário Clique No input porém não mude nada, corre o risco do Modal não sumir
        //Para isso chamaremos uma função caso o usuario não mude nada e desfoque o input

        $scope.HideModal = () => {
            // o Set timeout vai nos ajudar para não correr o risco de o modal fechar antes do usuario clicar
            setTimeout(() => {
                document.getElementById("Container").style.display = "none"

            }, 2000);
        }


        // Confere se o status é Igual a pago, sendo assim não é possivel editar
        if ($scope.dados.status === 'PAGO') {

            document.getElementById("Gravar").style.display = "none"
            document.getElementById("IsDangerMessage").style.display = "block"

        }


        $scope.buscarClientes = async () => {


            const length = $scope.dados.nome.length
            //Somente com 3 letras iremos iniciar a pesquisa
            // Tendo 3 letras o container vai aparecere e o erro some
            if (length >= 3) {
                document.getElementById("Container").style.display = "block"
                document.getElementById("ErrorLength").style.display = "none"

                // fazemos a chamda e retornamos os dados
                const response = await $http.put(urlServidor.urlServidorChatAdmin + '/clientes/listar', { razao_social: $scope.dados.nome })
                let dados = response.data;
                $scope.clientes = dados;
                //se for menor que 3 o modal some e o erro aparece
            } else if (length < 3) {
                document.getElementById("ErrorLength").style.display = "block"
                document.getElementById("Container").style.display = "none"
                // se estiver vazio o container some
            } else if ($scope.dados.nome === '') {
                document.getElementById("Container").style.display = "none"

            }


        }


        $scope.ChangeScope = (clientes) => {
            // Vamos substituir o inupt texte, visto que não precisamos mais do container ele receber um none
            if (clientes) {
                $scope.DadosGeraisClientes = clientes
                $scope.dados.nome = clientes.nome
                document.getElementById("Container").style.display = "none"
            } else {
                $scope.DadosGeraisClientes = false

            }





        }



        $scope.fechar =  () => window.close


        if (($scope.dados.data_pagto !== '') || ($scope.dados.data_cancelamento !== '')) {

            if (localStorage.getItem('tipo_usuario') == 'admin') {
                $scope.isDisabled = false
                $scope.isEditable = false
            } else {
                $scope.isDisabled = true
                $scope.isEditable = true
            }

        } else {
            $scope.isDisabled = false
            $scope.isEditable = false
        }


        $scope.gravar = function () {
          try {
            if ($scope.DadosGeraisClientes) {
              $scope.dados.id_cliente = $scope.DadosGeraisClientes.id;
            } else {
              $scope.dados;
            }

            const { data } = $http.post(
              urlServidor.urlServidorChatAdmin + "/receber/alterar",
              $scope.dados
            );
            console.log("sucess", data);
            myFunctions.showAlert("Alteração executada com sucesso!");
          } catch (error) {
            console.error(error);
          }
        };




        const buscaGrupos = async () => {
            const response = await $http.put(`${urlServidor.urlServidorChatAdmin}/grupos/listar`)
            let dados = response.data
            dados.sort((a, b) => {
                if (a.descricao < b.descricao) {
                    return -1;
                }
                if (a.descricao > b.descricao) {
                    return 1;
                }
                return 0;
            });

            $scope.grupos = dados;
        }


        const buscaPatios = async () => {
            const response = await $http.put(`${urlServidor.urlServidorChatAdmin}/patios/listar`)
            const { data } = response
            $scope.patios = data;
        }

        const buscaTipoRecebimento = async () => {
            const response = await $http.put(urlServidor.urlServidorChatAdmin + '/tipos-recebimento/listar-tipo', { tipo: 'RECEBER' })
            const { data } = response
            $scope.tiposRecebimentos = data;
        }




        $scope.estornar = async () => {

            if ($scope.DadosGeraisClientes) {
                $scope.dados.id_cliente = $scope.DadosGeraisClientes.id

            } else {
                $scope.dados


            }

            var alterados = {
                valor_baixa: $scope.dados.valor_baixa,
                acrescimo: $scope.dados.acrescimo,
                desconto: $scope.dados.desconto,
                data_pagto: $scope.dados.data_pagto
            }

            try {

                await $http.post(urlServidor.urlServidorChatAdmin + '/receber/estorna-pagamento', { id: _id })


                var json = Object.assign({}, alterados);


                if (JSON.stringify(json) !== '{}') {



                    $scope.logs = {
                        id: $scope.dados.id,
                        operacao: 'ESTORNO',
                        tipo: 'ESTORNO DE PAGAMENTO',
                        id_usuario: localStorage.getItem('id_usuario'),
                        alteracoes: JSON.stringify(json),
                        anterior: null

                        //data_hora: new Date()

                    };
                    $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro', $scope.logs).then(function (response) {
                        if (response.data.code) {
                            $scope.erro = response;

                        }
                    });
                }



                setTimeout(function () {
                    myFunctions.showAlert('Estorno realizado com sucesso !!');
                }, 1000);



            } catch (error) {
                myFunctions.showAlert('Não foi possível realizar o estorno!!');
                console.log(error);
            }



        }







        async function buscaCategorias(idgrupo) {

            const response = await $http.put(urlServidor.urlServidorChatAdmin + '/categorias/listar', { grupo: idgrupo })
            const dados = response.data;
            return dados;

        }


        buscaCategorias($scope.dados.grupo).then(function (result) {
            $scope.categorias = result;
        });


        $scope.SelecionaCategoria = async (idgrupo) => {
            const result = await buscaCategorias(idgrupo)
            $scope.categorias = result;
        }

        $scope.fechar = function () {
            window.close();
        }

        setTimeout(() => {
            buscaGrupos()
            buscaPatios()
            buscaTipoRecebimento()
        }, 1000);









    })


})
