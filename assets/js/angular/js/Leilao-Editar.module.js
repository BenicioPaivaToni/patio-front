"use strict";
angular
  .module("app")
  .controller(
    "leilaoEditarCtrl",
    function ($scope, $http, myFunctions, urlServidor, $location, viaCep) {
      const Leilao = $location.search();
      $scope.dados;
      $scope.optionsOrgao = ["DETRAN", "DER", "PRF", "PREFEITURA"];

      (async () => {
        const { data } = await $http.get(
          `${urlServidor.urlServidorChatAdmin}/leiloes/buscar/${Leilao.id}`
        );
        $scope.dados = data;
      })();
      $scope.isDisabled = false;

      $http
        .put(
          urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
          {
            idUsuario: localStorage.getItem("id_usuario"),
            idEmpresa: localStorage.getItem("id_empresa"),
          }
        )
        .then(
          ({ data }) =>
            ($scope.patios = data.filter((item) => item.status == !0))
        );

      $scope.gravar = async () => {
        console.log($scope.dados)
        try {
          await $http.post(
            `${urlServidor.urlServidorChatAdmin}/leiloes/alterar`,
            $scope.dados
          );
          myFunctions.showAlert("Cadastro alterado com sucesso!");
          $scope.isDisabled = true;
        } catch (error) {
          myFunctions.showAlert("Erro na gravação!");
        }
      };

      $scope.getPatioCadastrados = async () => {
        const response = await $http.get(
          `${urlServidor.urlServidorChatAdmin}/patios/listar-leilaopatio`,
          { params: { id_leilao: Leilao.id } }
        );

        $scope.patiosCadastrados = response.data;
      };

      $scope.getPatioCadastrados();

      $scope.addPatio = async (patio) => {
        const id = $scope.patiosCadastrados.map((item) => item.id);
        const HasEqual = id.includes(patio.id);

        if (!HasEqual) {
          await $http
            .post(
              `${urlServidor.urlServidorChatAdmin}/patios/grava-leilaopatio`,
              { id_leilao: Leilao.id, id_patio: patio.id_patio }
            )
            .then(() => $scope.getPatioCadastrados());
        } else {
          myFunctions.showAlert(
            "Patio selecionado já existe na lista de pátios cadastrados, verique"
          );
        }
      };

      $scope.removePatio = async (patio) => {
        await $http
          .post(
            `${urlServidor.urlServidorChatAdmin}/patios/remove-leilaopatio`,
            { id_leilao: Leilao.id, id_patio: patio.id_patio }
          )
          .then(() => $scope.getPatioCadastrados());
      };

      $scope.paginaAnterior = () => window.close();

      $scope.buscaCep = (cep) => {
        if (cep.length == 9 && cep != null) {
          viaCep.get(cep).then((response) => {
            $scope.dados.patio_endereco = response.logradouro.toUpperCase();
            $scope.dados.patio_bairro = response.bairro.toUpperCase();
            $scope.dados.patio_cidade = response.localidade.toUpperCase();
            $scope.dados.patio_uf = response.uf;
          });
        }
      };

      $scope.buscaCepEndereco = (cep) => {
        if (cep.length == 9 && cep != null) {
          viaCep.get(cep).then((response) => {
            $scope.dados.devolucao_endereco = response.logradouro.toUpperCase();
            $scope.dados.devolucao_bairro = response.bairro.toUpperCase();
            $scope.dados.devolucao_cidade = response.localidade.toUpperCase();
            $scope.dados.devolucao_uf = response.uf;
          });
        }
      };
    }
  );
