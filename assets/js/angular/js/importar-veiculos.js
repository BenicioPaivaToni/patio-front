app.controller('importarVeiculosCtrl', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.dados = {
        
        placa: '',
        marca_modelo: '',
        tipo_veiculo: '',
        ano: '',
        cor: '',
        chassi: '',
        motor: '',
        data_apreensao: '',
        patio: '',
        leilao: ''
    }

    $scope.showMessage = false

    $scope.limparImportar = function (ev) {
        
        $scope.dados.patio = ''
        document.getElementById('arquivoLote').value = []
        $scope.arquivoSelecionado = ''
        $scope.importarList = ''
    }
    

    $scope.limparDistribuir = function (ev) {
        
        $scope.dados.patio = ''
        document.getElementById('arquivoLote').value = []
        $scope.arquivoSelecionado = ''
        $scope.distribuirList = ''
    }


    buscaPatios().then(function (result) {
        $scope.patios = result.filter((item ) => item.status == 1)
       
    })


    buscaLeiloes().then( function(result){
        $scope.leilao = result
        console.log ("Lieloes: ", result)
    })

    

    $scope.enviarDadosProcessados = async (dados) => {
        
       const response = await $http.post(urlServidor.urlServidorChatAdmin+'/integracoes/grava-importacao-veiculos', dados );

        if (response.data.code){

          throw new Error('Erro na gravaçao!')
        }


        if ($scope.showMessage === true){

           myFunctions.showAlert( 'Importação efetuada com sucesso!')
        }

        return response;
        
    }


    $scope.processar = async (ev) => {

        if ((!$scope.dados.patio) || (!$scope.arquivoSelecionado) ) 
            
        {

            myFunctions.showAlertCustom('Arquivo de lote ou pátio não selecionado!<br/><br/>Verifique e tente novamente!')

        } else {
            
            const dados = $scope.importarList.map((x) => {

                let tipo = x.tipo_veiculo

                if (tipo.includes('PESADO')){
                    tipo = 3
                }
                else if (tipo.includes('CARRO')){
                    tipo = 1
                }
                else if (tipo.includes('MOTO')){
                tipo = 2
                }    
                else if (tipo.includes('OUTROS')){
                    tipo = 4
                }    


                let apreensao = x.data_apreensao
                if (apreensao === "0" || ''){
                   // apreensao = new Date().toLocaleDateString()
                   apreensao = new Date().toISOString().slice(0, 10)
                   

                }
        
               return {
                placa: x.placa,
                marca_modelo: x.marca_modelo,
                cor: x.cor,
                ano: x.ano,
                chassi: x.chassi,
                numero_motor: x.motor,
                tipo_veiculo: tipo,
                data_cadastro: apreensao,
                id_patio: $scope.dados.patio,
                status: "APREENDIDO"
               }
            })

            
            const dadosProcessados = []

            for(let item of dados) {

              dadosProcessados.push($scope.enviarDadosProcessados(item))

            
              for (let i = 0; i < dados.length; i++) {
            
                if (i +1 === dados.length){
                    $scope.showMessage = true

                }else{

                    $scope.showMessage = false
                }

            }
          

            }

           try {
               const response = await Promise.all(dadosProcessados)
            } catch (error) {
                myFunctions.showAlert(error.message) // revisar se deixar a mensagem ou remover
            }

            /*
            $http.post(urlServidor.urlServidorChatAdmin+'/integracoes/grava-importacao-veiculos', dados ).then( function(response){

                console.log("dados_enviados: ", dados)
                if (response.data.code){
                    
                  myFunctions.showAlert('Erro na gravação!')
        
                }else{
        
                  if ( response.data == '11000' )                 
                  {
                    myFunctions.showAlert('Já existe!')
                  }else
                  {                    
        
                    myFunctions.showAlert( 'Importação efetuada com sucesso!' ); 
                              
        
                  }  
                        
                }; 
        
                
            }); 

            */      
      

        }

    }

    
    $scope.buscaArquivoLote = function(element){
        var resultado = element.files[0]
        const nomeArquivo = element.files[0].name
        $scope.arquivoSelecionado = nomeArquivo
        var reader = new FileReader()
        reader.onload = function(e){
            $scope.$apply(function(){
                resultado = reader.result
                
                let loteArray = resultado.split(/\r\n|\n/) //converte a lista de texto em string array
                
                if (loteArray[0].includes("PLACA")){
                   let lost = loteArray.shift()
                }

                const arrayFiltrado =  loteArray.filter(item => item !=='')

                console.log("")
                if (arrayFiltrado.lenth === 0 ){

                    myFunctions.showAlertCustom("Arquivo não possui lote para importação! Verifique!")

                }

                var dados = []              

                for(let linha of arrayFiltrado) {
                    var infoTmp = linha.split(';')
                
                    var obj = {}
                    obj["placa"] = infoTmp[0]
                    obj["marca_modelo"] = infoTmp[1]
                    obj["cor"] = infoTmp[2]
                    obj["ano"] = infoTmp[3]
                    obj["chassi"] = infoTmp[4]
                    obj["motor"] = infoTmp[5]
                    obj["tipo_veiculo"] = infoTmp[6]
                    obj["data_apreensao"] = infoTmp[7]
                
                    dados.push(obj)
                }

                const placasValidas = filtroPlacas(dados)

                let diferencaQty = dados.length - placasValidas.length

                $scope.importarList = placasValidas
                        
                if (diferencaQty !== 0){

                    myFunctions.showAlertCustom("Arquivo de lote importado contém: <br/><br/>"
                                            + " - <span class='txtGreen'>" + placasValidas.length + " veículo(s) com dados válidos. <br/></span>"
                                            + " - <span class='txtRed'>" + diferencaQty + " veiculo(s) com dados inválidos. VERIFIQUE!</span><br/><br/>"
                                            + " OBS.: Somente veículos com dados válidos serão gravados no sistema.")              
               
                }else{
                
                    myFunctions.showAlertCustom("Arquivo de lote importado com sucesso! <br/><br/>"
                                                + "<span class='txtGreen'>Lote contém " + placasValidas.length + " veiculo(s).</span>")

                }
            })
        }
        
        reader.readAsText(resultado, 'UTF-8')
        
    }



    function filtroPlacas(dados) {

        const placaNormal = /^[a-zA-Z]{3}[0-9]{4}$/                           // XXX-9999
        const placaMercosulCarro = /^[a-zA-Z]{3}[0-9]{1}[a-zA-Z]{1}[0-9]{2}$/ // XXX-9X99
        const placaMercosulMoto = /^[a-zA-Z]{3}[0-9]{2}[a-zA-Z]{1}[0-9]{1}$/  // XXX-99X9

        const filtroTmp = dados.filter((item)=> {
              
          if (
           placaNormal.test(item.placa) ||
           placaMercosulCarro.test(item.placa) ||
           placaMercosulMoto.test(item.placa)           

          )
            { 
             return {
              ...item

            }
          
          }
                      
        })

        return filtroTmp
    }




    function buscaPatios() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') }).then(function (response) {
                 
                let dados = response.data
                resolve(dados)
            })
        })
    }



    function buscaLeiloes() {
        return new Promise(function (resolve, reject) {
           
            $http.get(urlServidor.urlServidorChatAdmin + '/leiloes/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            })
        })
    }

 


    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("Importacao-Veiculos.xlsx",{headers:true}) FROM ?', [$scope.importarList])
    }



    $scope.gerarPDF = function () {
        var doc = new jsPDF({orientation: "landscape"})
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12)
        doc.autoTable({

            columnStyles: {empresa: {halign: 'left'},
                nome: {halign: 'left'},
                endereco: {halign: 'left'},
                },
            body: $scope.importarList,
            columns: [
                {header: 'Placa', dataKey: 'placa'},
                {header: 'Marca/Modelo', dataKey: 'marca_modelo'},
                {header: 'Cor', dataKey: 'cor'},
                {header: 'Ano', dataKey: 'ano'},
                {header: 'Tipo', dataKey: 'tipo_veiculo'},
                {header: 'Chassi', dataKey: 'chassi'},
                {header: 'Motor', dataKey: 'motor'},
                {header: 'Data Apreensão', dataKey: 'data_apreensao'},
                              
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                doc.text(
                    "Importação de veiculos",
                    data.settings.margin.left + 10, 20
                )


                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: {top: 30},
        });

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        } 
       
        doc.save('Importacao Veiculos.pdf')
    }

})