app.controller('empresasReboquesEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep, $filter, $sce, urlImagens, $mdDialog, buckets) {

    $scope.urlImagens = urlImagens.urlDocumentosEmpresaReb;

    $scope.urlImagemTermo = urlImagens.urlDocumentosJuridico

    var bucket = null;
    
    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    var urlParams = $location.search();
    $http.get(urlServidor.urlServidorChatAdmin + '/autoridades/vercodigo').then(function (response) {

        AWS.config.update({ accessKeyId: response.data.ac, secretAccessKey: response.data.sc });
        AWS.config.region = response.data.regiao;
    }).finally(function () {
        bucket = new AWS.S3();
        /* ((urlParams.dados[0].pdf_contrato != '') && (urlParams.dados[0].pdf_contrato != undefined) && (urlParams.dados[0].pdf_contrato != null)) {
            bucket.getObject(
                    {Bucket: "documentos-reidospatios", Key: urlParams.dados[0].pdf_contrato, ResponseContentDisposition: 'inline'},
                    function (error, data) {
                        if (error != null) {
                            myFunctions.showAlert("Falha no carregamento do PDF: " + error);
                        } else {

                            $scope.pdfContrato = new Blob([data.Body], {type: "application/pdf"});
                        }
                    }
            );
        }*/

    });

    $scope.dados = {
        id: urlParams.dados[0].id,
        nome: urlParams.dados[0].nome,
        contato: urlParams.dados[0].contato,
        telefone: urlParams.dados[0].telefone,
        ativo: urlParams.dados[0].ativo,
        observacao: urlParams.dados[0].observacao,
        endereco: urlParams.dados[0].endereco,
        bairro: urlParams.dados[0].bairro,
        cidade: urlParams.dados[0].cidade,
        cep: urlParams.dados[0].cep,
        uf: urlParams.dados[0].uf,
        email: urlParams.dados[0].email,
        inicio_contrato: $filter('date')(urlParams.dados[0].inicio_contrato, 'dd/MM/yyyy', "+0000"),
        fim_contrato: $filter('date')(urlParams.dados[0].fim_contrato, 'dd/MM/yyyy', "+0000"),
        pdf_contrato: urlParams.dados[0].pdf_contrato,
        cpf_cnpj: urlParams.dados[0].cpf_cnpj,  
        rep_nome: urlParams.dados[0].rep_nome,
        rep_cpf: urlParams.dados[0].rep_cpf,
        rep_rg: urlParams.dados[0].rep_rg,
        rep_telefone: urlParams.dados[0].rep_telefone,
        rep_email: urlParams.dados[0].rep_email,
        rep_estado_civil: urlParams.dados[0].rep_estado_civil,
        rep_profissao: urlParams.dados[0].rep_profissao,
        rep_nacionalidade: urlParams.dados[0].rep_nacionalidade,
        rep_cep: urlParams.dados[0].rep_cep,
        rep_endereco: urlParams.dados[0].rep_endereco,
        rep_bairro: urlParams.dados[0].rep_bairro,
        rep_cidade: urlParams.dados[0].rep_cidade,
        rep_uf: urlParams.dados[0].rep_uf
        
    }



    $scope.estados = estados.uf;

    $scope.buscaCep = (cep) => {

        if (cep.length == 9) {

            viaCep.get(cep).then(function (response) {

                $scope.dados.endereco = response.logradouro.toUpperCase();
                $scope.dados.bairro = response.bairro.toUpperCase();
                $scope.dados.cidade = response.localidade.toUpperCase();
                $scope.dados.uf = response.uf.toUpperCase();
            });
        }

    }


    $scope.buscaCepRep = (rep_cep) => {

        if(rep_cep.length == 9){
    
          viaCep.get(rep_cep).then(function(response){
          
            $scope.dados.rep_endereco = response.logradouro.toUpperCase() ;
            $scope.dados.rep_bairro = response.bairro.toUpperCase() ;
            $scope.dados.rep_cidade = response.localidade.toUpperCase() ;
            $scope.dados.rep_uf = response.uf.toUpperCase() ;				 
    
          });
    
        }	
    
    }



    $scope.uploadFile = (file) => {

        $scope.pdfContrato = file;
        $scope.arquivo = file;
        $scope.dados.pdf_contrato = file.name;
        var params = {
            Key: $scope.arquivo.name,
            ContentType: $scope.arquivo.type,
            Body: $scope.arquivo,
            ACL: 'public-read',
            Bucket: buckets.empresas_reboques
        };
        $scope.pdfContrato = null;
        $scope.arquivo = null;
        $scope.dados.pdf_contrato = null;
        bucket.putObject(params, function (err, data) {
            if (err) {
                myFunctions.showAlert('ERROR: ' + err);
            } else {
                setTimeout(function () {

                    myFunctions.showAlert('Arquivo alterado com sucesso!');

                }, 1500);
                $scope.pdfContrato = file;
                $scope.arquivo = file;
                $scope.dados.pdf_contrato = file.name;

            };
        });
    };
    $scope.limparPDF = () => {
        let confirm = $mdDialog.confirm()
            .title('Contrato')
            .textContent('Tem certeza que deseja apagar esse documento?')
            .ariaLabel('Lucky day')
            .ok('Apagar')
            .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {

            var params = { Bucket: buckets.empresas_reboques, Key: $scope.dados.pdf_contrato };

            bucket.deleteObject(params, function (err, data) {
                if (err) {
                    myFunctions.showAlert("Não foi possivel apagar o arquivo!");
                }  // error
                else {

                    $scope.dados.pdf_contrato = null;
                    $scope.pdfContrato = null;
                    $scope.arquivo = null;
                    myFunctions.showAlert('Arquivo apagado!');

                }
            });
        });
    }


    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/empresas_reboques/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    myFunctions.showAlert('Empresa já existe!')
                } else {
                    myFunctions.showAlert('Alteração executada com sucesso!');

                }

            }
            ;
        });

    };


    buscaPatios().then(function (result) {
        $scope.patios = result;
    });


    buscaPatiosEmpresaReboque($scope.dados.id, localStorage.getItem('id_empresa')).then(function (result) {
        $scope.patiosEmpresa = result;
    });


    buscaTermosCredenciamento()
    

    $scope.incluiPatio = function (id) {

        let dados = {
            idPatio: id,
            idEmpresareboque: $scope.dados.id,
            idEmpresa: localStorage.getItem('id_empresa')
        };
        $http.post(urlServidor.urlServidorChatAdmin + '/patios/grava-empresareboquepatio', dados).then(function (response) {

            $scope.patiosEmpresa = response.data;
        });
    };


    $scope.removePatio = function (id) {

        let dados = {
            idPatio: id,
            idEmpresareboque: $scope.dados.id,
            idEmpresa: localStorage.getItem('id_empresa')
        };
        $http.post(urlServidor.urlServidorChatAdmin + '/patios/remove-empresareboquepatio', dados).then(function (response) {

            $scope.patiosEmpresa = response.data;
        });
    };


    function buscaPatios() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar').then(function (response) {

                resolve(response.data);
            });
        });
    }


    function buscaPatiosEmpresaReboque(idempresareboque, idempresa) {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patioempresareboque', { idEmpresareboque: idempresareboque, idEmpresa: idempresa }).then(function (response) {

                resolve(response.data);
            });
        });
    }

    

    function buscaTermosCredenciamento() {

        $http.get(urlServidor.urlServidorChatAdmin + '/empresas_reboques/termo-credenciamento-listar', { params: {id_empresa_reboque: $scope.dados.id }}).then(function (response) {

            $scope.termosList = response.data 
            
        });

    }

    

    $scope.visualizarTermo = function (ev, rowId) {

        $scope.listaDocumentos = null

        $http.get (urlServidor.urlServidorChatAdmin + '/empresas_reboques/busca-arquivo-termo', {params: {id: rowId}}).then(function (response) { 
            
            if(response.data[0].data_hora_assinatura == null){

               myFunctions.showAlert('Documento PDF contendo Termo de Credenciamento não foi localizado, verifique!');
            
            }else{
                
                $scope.listaDocumentos = response.data

                setTimeout(() => {
            
                    $mdDialog.show({controller: function ($scope, $mdDialog, $mdToast) {
                    
                    $scope.fechar = function () {
                        $mdDialog.cancel();
                    };
                    },
                        templateUrl: 'visualizar-termo-credenciamento.html',
                        scope: $scope,
                        preserveScope: true,
                        scopoAnterior: $scope
                    }).then(function (answer) {
                            let a = answer;

                        }, function () {
                            $scope.statusdialog = 'You cancelled the dialog.';
                    });
        
                }, 2000);
            }
           
        })

        
    }




    $scope.removerTermo = function (ev, rowId) {
        
        let id = rowId
        
        let confirm = $mdDialog.confirm()
            .title('Contrato')
            .textContent('Tem certeza que deseja apagar este Termo?')
            .ariaLabel('Lucky day')
            .ok('Apagar')
            .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {
            try {
                    
                $http.post(urlServidor.urlServidorChatAdmin + '/empresas_reboques/termo-credenciamento-apagar', { id: id} ).then(function (response) {
                
                    if (response.data.code) {

                        myFunctions.showAlert('Erro na gravação!')

                    } else {

                        let nomeArquivo = $scope.termosList.find(x => x.id === id);

                        let fileNamePdf = nomeArquivo.arquivo

                        let fileNameHtml = fileNamePdf.replace('.pdf', '.html')

                        $http.delete('https://app-termo-gateway-15a59c9f5568.herokuapp.com/delete-html/' + fileNameHtml).then(function(response) {
                            

                        })

                        $http.post('https://app-termo-gateway-15a59c9f5568.herokuapp.com/delete-pdf', 
                            {
                                fileName: fileNamePdf
                            }
                        )

                        myFunctions.showAlert('Registro removido com sucesso!')
                    }
                        
                    buscaTermosCredenciamento()

                })      

                } catch (error) {
               console.log(error);
            }
        })
    }



    $scope.gerarPlanilha = () => {
        
        if ($scope.termosList) {
  
          const excelAjustes = $scope.termosList.map((item) => ({
            data_hora_envio: item.data_hora_envio,
            data_hora_assinatura: item.data_hora_assinatura,
            city: item.city,
            estado: item.estado,
            ip_device: item.ip_device, 
            hostname: item.hostname,
            latitude: item.latitude,
            longitude: item.longitude,        
              
          }))
  
          alasql(
            'SELECT * INTO XLSX("termos-credenciamento.xlsx",{headers:true}) FROM ?',[excelAjustes]
          )
  
        }else{
          
          myFunctions.showAlert('Sem dados para gerar planilha! Verifique!')
  
        }
    }



    $scope.gerarPDF = function () {
    
        if ($scope.termosList) {

        const PDFAjustes = $scope.termosList.map((item) => ({
            data_hora_envio: item.data_hora_envio,
            data_hora_assinatura: item.data_hora_assinatura,
            city: item.city,
            estado: item.estado,
            ip_device: item.ip_device,
            hostname: item.hostname,
            latitude: item.latitude,
            longitude: item.longitude       
           
        }))

        var doc = new jsPDF({ orientation: "landscape" });

        var totalPagesExp = "{total_pages_count_string}";

        doc.setFontSize(10);

        doc.autoTable({
          columnStyles: {
            vencimento: { halign: "left" },
          },

          body: PDFAjustes,
          columns: [
            { header: "Data Envio", dataKey: "data_hora_envio" },
            { header: "Data Assinatura", dataKey: "data_hora_assinatura" },
            { header: "Cidade", dataKey: "city" },
            { header: "Estado", dataKey: "estado" },
            { header: "IP", dataKey: "ip_device" },  
            { header: "Equipamento", dataKey: "hostname" },
            { header: "Latitude", dataKey: "latitude" },
            { header: "Longitude", dataKey: "longitude" },

          ],
          bodyStyles: {
            margin: 10,
            fontSize: 08,
          },

          didDrawPage: function (data) {
            // Header
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle("normal");

            doc.text(
              "Lista de Termos de Credenciamento",
              data.settings.margin.left + 15, 22
            );

            // Footer
            var str = "Pagina " + doc.internal.getNumberOfPages();
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === "function") {
              str = str + " de " + totalPagesExp;
              var totalpaginas = totalPagesExp;
            }

            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height
              ? pageSize.height
              : pageSize.getHeight();

            doc.text(
              str + "                        ",
              data.settings.margin.left,
              pageHeight - 10
            );
          },
          margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === "function") {
          doc.putTotalPages(totalPagesExp);
        }

        doc.save("termos-credenciamento.pdf")  
        
        }else{

            myFunctions.showAlert('Sem dados para gerar PDF! Verifique!')

        }
    }



});