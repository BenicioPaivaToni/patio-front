angular
  .module("app")
  .controller(
    "consultaDenatranCtrl",
    function ($scope, $http, urlServidor, urlImagens, $sce, $filter) {
      const { hash } = window.location;

      const renavam = hash.slice(hash.indexOf("=") + 1);

      $scope.PrintElem = async (elem) => {
        var pdf = new jsPDF({ orientation: "landscape" });
        const thumbnailsHtml = document.getElementById(elem);
        const canvas = await html2canvas(thumbnailsHtml, {
          windowWidth: 1400,
          windowHeight: 1500,
          scrollX: 0,
          scrollY: 0,
          allowTaint: true,
        });

        const imgData = canvas.toDataURL("image/jpeg", 1.0);

        const imgProps = pdf.getImageProperties(imgData);

        const pdfWidth = 300; /* this.pdf.internal.pageSize.getWidth() */
        const pageHeight = pdf.internal.pageSize.getHeight();
        const imgHeight =
          (imgProps.height * pdfWidth) / 1000; /* imgProps.width */
        var heightLeft = imgHeight;
        var position = 0;
        pdf.addImage(
          imgData,
          "JPEG",
          0, // Position of image from left and right
          0, // Position of image from top and bottom
          pdfWidth,
          imgHeight,
          undefined,
          "SLOW"
        );
        heightLeft -= pageHeight;
        while (heightLeft >= 0) {
          position = heightLeft - imgHeight;
          pdf.addPage();
          pdf.addImage(
            imgData,
            "JPEG",
            0,
            position + 0,
            pdfWidth,
            imgHeight,
            undefined,
            "SLOW"
          );
          heightLeft -= pageHeight;
        }
        $scope.ShowMessage = false;
        pdf.save("relatorio-denatran.pdf");
      };

      $scope.$on("$viewContentLoaded", async function () {
        $scope.camelCaseToWords = (s) => {
          if (s.includes("_")) {
            const splitedS = s.split("_");
            return `${splitedS[0].charAt(0).toUpperCase()} ${splitedS[1]}`;
          }

          const result = s.replace(/([A-Z])/g, " $1");
          return result.charAt(0).toUpperCase() + result.slice(1);
        };

        const GetDadosByRenavam = async () => {
          const { data } = await $http.get(
            `${urlServidor.urlServidorChatAdmin}/integracoes/busca-dados-denatran?renavam=${renavam}`
          );
          return data[data.length - 1];
        };

        const RenderVeiculosDados = (data) => {
          const { Result } = JSON.parse(data);
          const keys = Object.keys(Result.BIN);

          const half = keys.length / 2;
          const lastIndex = keys.length - 1;

          $scope.dadosVeiculo = keys.slice(0, half).map((key) => ({
            Label: `${$scope.camelCaseToWords(key)} :`,
            dataKey: Result.BIN[key],
          }));

          $scope.dadosVeiculo1 = keys.slice(half, lastIndex).map((key) => ({
            Label: `${$scope.camelCaseToWords(key)} :`,
            dataKey: Result.BIN[key],
          }));
        };

        const RenderMultas = (data) => {
          const { Result } = JSON.parse(data);
          $scope.multas = Result.Multas;
        };
        const RenderRenajud = (data) => {
          const { Result } = JSON.parse(data);
          $scope.renajud = Result.Renajud;
          console.log(Result);
        };

        const Render = () => {
          GetDadosByRenavam().then(({ data }) => {
            RenderVeiculosDados(data);
            RenderMultas(data);
            RenderRenajud(data);
          });
        };
        Render();
      });

      $scope.GerarPDF = (dados) => {
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = "{total_pages_count_string}";
        const Info = new Array(dados);

        doc.setFontSize(12);
        doc.autoTable({
          body: Info,

          columns: [
            { header: "NCV", dataKey: "id" },
            { header: "Ano", dataKey: "ano" },
            { header: "Chassi", dataKey: "chassi" },
            { header: "Cor", dataKey: "cor" },

            { header: "Data Apreensão", dataKey: "data_apreensao" },
            { header: "Data Cadastro", dataKey: "data_cadastro" },
            { header: "Estado", dataKey: "estado_veiculo" },

            { header: "Marca/Modelo", dataKey: "marca_modelo" },
            { header: "Pátio", dataKey: "nome" },
            { header: "Número Motor", dataKey: "numero_motor" },
            { header: "Pátio Destino", dataKey: "patio_destino" },
            { header: "Placa", dataKey: "placa" },
            { header: "Placa Municipio", dataKey: "placa_municipio" },
            { header: "Placa UF", dataKey: "placa_uf" },
            { header: "Situação", dataKey: "situacao" },
            { header: "Status", dataKey: "status" },
            { header: "Tipo Veiculo", dataKey: "tipo_veiculo" },
            { header: "Veiculo Trancado", dataKey: "veiculo_trancado" },
          ],
          bodyStyles: {
            margin: 0,
            fontSize: 08,
          },
          didDrawPage: function (data) {
            // Header
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle("normal");

            // Footer
            var str = "Pagina " + doc.internal.getNumberOfPages();
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === "function") {
              str = str + " de " + totalPagesExp;
            }

            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height
              ? pageSize.height
              : pageSize.getHeight();
            doc.text(str, data.settings.margin.left, pageHeight - 10);
          },
          margin: { top: 30 },
        });

        if (typeof doc.putTotalPages === "function") {
          doc.putTotalPages(totalPagesExp);
        }

        doc.save(`${_id}dados.pdf`);
      };
    }
  );
