
angular.module('app')
    .controller('pagarBaixarCtrl', function ($scope, $http, myFunctions, urlServidor, $mdDialog, S3UploadService,
        $http,
        myFunctions, urlServidor, $mdDialog, buckets,
        buckets,urlImagens, $sce) {


        const queryString = window.location.hash;

        const _id = queryString.slice(queryString.indexOf("%") + 3);

        var aux;
        var valor;
        var origem;



        $scope.$on('$viewContentLoaded', function () {


            buscaDados().then(function (result) {
                $scope.dados = result;

                setTimeout(function () {

                    valor = $scope.dados.valor.substring(2, $scope.dados.valor.length);
                    valor = valor.replace('.', '');
                    valor = valor.replace(',', '.');
                    valor = Number(valor);


                    buscaParciais($scope.dados.id).then(function (result) {
                        if (result.length > 0) {

                            $scope.parciais = result;
                            $scope.totalparcial = result[0].total;
                            aux = valor - $scope.totalparcial;
                            if (aux > 0) {

                                $scope.dados.valor_baixa = myFunctions.numberToReal(aux);
                            }



                        } else {
                            $scope.parciais = 0
                            $scope.totalparcial = 0;
                            $scope.dados.valor_baixa = $scope.dados.valor;
                        }


                    });
                    if ($scope.totalparcial >= $scope.dados.valor) {
                        $scope.baixadisable = true;
                    }


                    origem = {
                        id: _id,
                        id_fornecedor: $scope.dados.id_fornecedor,
                        documento: $scope.dados.documento,
                        vencimento: $scope.dados.vencimento,
                        tipo_recebimento: $scope.dados.tipo_recebimento,
                        categoria: $scope.dados.categoria,
                        comentario: $scope.dados.comentario,
                        competencia: $scope.dados.competencia,
                        desc_categoria: $scope.dados.desc_categoria,
                        desc_grupo: $scope.dados.desc_grupo,
                        data_pagto: $scope.dados.data_pagto,
                        data_cancelamento: $scope.dados.data_cancelamento,
                        valor: $scope.dados.valor,
                        valor_baixa: $scope.dados.valor_baixa,
                        acrescimo: $scope.dados.acrescimo,
                        desconto: $scope.dados.desconto,
                        status: $scope.dados.status,
                        valor_semformatacao: $scope.dados.valor_semformatacao,
                        classificacao: $scope.dados.classificacao,
                        grupo: $scope.dados.grupo,
                        pdf: $scope.dados.pdf,
                        razao_nome: $scope.dados.razao_nome,
                        nome: $scope.dados.nome,
                        id_patio: $scope.dados.id_patio,
                        desc_tiporecebimento: $scope.dados.desc_tiporecebimento,
                        contabilizado: $scope.dados.contabilizado,
                        saldo_pagar: $scope.dados.saldo_pagar,
                        conta: $scope.dados.conta,
                        valorbaixa: $scope.dados.valor_baixa,
                        valordesconto: $scope.dados.desconto,
                        databaixa: $scope.dados.data_pagto,
                        valoracrecismo: $scope.dados.acrescimo
                    }
                }, 1500);
            });




            $scope.urlImagemNf = urlImagens.urlImagemNf;
            $scope.trustSrc = function (src) {
                return $sce.trustAsResourceUrl(src);
            }

            var bucket = null;

            $http.get(urlServidor.urlServidorChatAdmin + '/autoridades/vercodigo').then(function (response) {

                AWS.config.update({ accessKeyId: response.data.ac, secretAccessKey: response.data.sc });
                AWS.config.region = 'us-east-1';
            }).finally(function () {

                bucket = new AWS.S3();
            });



            buscaDados().then( async (info) => {

                $scope.Dados = info

               

               const responseDocs = await $http.get(`${urlServidor.urlServidorChatAdmin}/pagar/busca-documentospagar`, {params: {id_pagar: _id }})
               const {data} = responseDocs
               $scope.listaDocumentos = data
               console.log($scope.listaDocumentos)



                $scope.uploadDocumento = (file) => {

                    let nomefoto = '';
                    if (file) {
                        if (file.type !== null && file.type !== undefined) {
    
                            if (file.type.substr(0, 5) == 'image' || file.type == 'application/pdf') {
                                if (file.type == 'application/pdf') {
                                    nomefoto =  _id + '/' + makeid() + '.' + file.type.substr(12, 3);
                                    S3UploadService.Upload(file, buckets.documentos_pagar, nomefoto).then(function (result) {
                                        // Mark as success
                                        file.Success = true;
    
                                        $http.post(urlServidor.urlServidorChatAdmin + '/pagar/incluir-documento',
                                            { id_pagar:  _id, documento: nomefoto }
                                        ).then(function (response) {
                                            $scope.listaDocumentos.push({ documento: nomefoto, id: response.data.insertId, id_pagar: _id });
                                        });
    
    
                                    }, function (error) {
                                        // Mark the error
                                        $scope.Error = error;
                                    }, function (progress) {
                                        // Write the progress as a percentage
    
                                        var alt = {
    
                                            imagem: nomefoto
    
                                        }
                                        var json = Object.assign({}, alt);
                                        $scope.logs = {
                                            id: _id,
                                            operacao: 'INCLUSÃO',
                                            tipo: 'DOCUMENTOS',
                                            id_usuario: localStorage.getItem('id_usuario'),
                                            alteracoes: JSON.stringify(json),
                                            anterior: null
    
                                        };
                                        $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro', $scope.logs).then(function (response) {
                                            if (response.data.code) {
                                                $scope.erro = response;
                                            }
                                        });
    
                                        file.Progress = (progress.loaded / progress.total) * 100
                                    });
    
                                } else {
                                    myFunctions.showAlert("Formato inválido!");
                                }
    
    
                            }
    
                        }
    
                    }
    
                }







            })


            $scope.apagarDocumento = (dados) => {
                let idimagem = dados.id;
                let confirm = $mdDialog.confirm()
                    .title('Documentos!')
                    .textContent('Tem certeza que deseja apagar esse documento?')
                    .ariaLabel('Lucky day')
                    .ok('Apagar')
                    .cancel('Cancelar');
                $mdDialog.show(confirm).then(function () {

                    $http.post(urlServidor.urlServidorChatAdmin + '/pagar/apaga-documento', { id: idimagem }).then(function (response) {
                        if (response.data.code) {
                            myFunctions.showAlert('Não foi possível apagar o documento !!');
                        } else {

                            var alt = {
                                idImagem: dados.id,
                                nome: dados.documento

                            }


                            var json = Object.assign({}, alt);
                            $scope.logs = {
                                id: _id,
                                operacao: 'EXCLUSÃO',
                                tipo: 'DOCUMENTO',
                                id_usuario: localStorage.getItem('id_usuario'),
                                alteracoes: '',
                                anterior: JSON.stringify(json)
                            };

                            $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro', $scope.logs).then(function (response) {
                                if (response.data.code) {
                                    $scope.erro = response;
                                } else {
                                    dados = null;
                                    alt = null;
                                    setTimeout(function () {

                                        $http.get(urlServidor.urlServidorChatAdmin + '/pagar/busca-documentospagar', { params: { id_pagar: $scope.dados.id } }).then(function (response) {
                                            $scope.listaDocumentos = response.data;
                                        });
                                        myFunctions.showAlert('Documento apagado !!');

                                    }, 1500);

                                }
                            });

                        }

                    });

                });

            }






           










        });


        var confirmTotal = $mdDialog.confirm()
            .title('Baixa de Contas a Pagar')
            .textContent('Confirma baixa total ? ')
            .ariaLabel('Lucky day')
            .ok('Sim')
            .cancel('Não');


        var confirm = $mdDialog.confirm()
            .title('Baixa de Contas a Pagar')
            .textContent('Valor informado menor que o valor em aberto, o lançamento será ? ')
            .ariaLabel('Lucky day')
            .ok('Diferença como Desconto')
            .cancel('Pagamento Parcial');

        var confirmAc = $mdDialog.confirm()
            .title('Baixa de Contas a Pagar')
            .textContent('Valor informado maior que o valor total, deseja aplicar a diferença como acrescimo ? ')
            .ariaLabel('Lucky day')
            .ok('Sim')
            .cancel('Cancela');

        var confirmParc = $mdDialog.confirm()
            .title('Pagamento Parcial')
            .textContent('Tem certeza que deseja registrar esse pagamento como parcial? ')
            .ariaLabel('Lucky day')
            .ok('Sim')
            .cancel('Cancelar');

        var confirmDesc = $mdDialog.confirm()
            .title('Diferença como Desconto')
            .textContent('Tem certeza que deseja aplicar a diferença como desconto? ')
            .ariaLabel('Lucky day')
            .ok('Sim')
            .cancel('Cancelar');

        var confirmAcres = $mdDialog.confirm()
            .title('Diferença como Acrescimo')
            .textContent('Tem certeza que deseja aplicar a diferença como acrescimo? ')
            .ariaLabel('Lucky day')
            .ok('Sim')
            .cancel('Cancelar');

        $scope.baixadisable = false;
        buscaPatios().then(function (result) {
            $scope.patios = result;
        });
        buscaFornecedores().then(function (result) {
            $scope.fornecedores = result;
        });
        buscaCategorias().then(function (result) {
            $scope.categorias = result;
        });
        buscaContas().then(function (result) {
            $scope.contas = result;
        });
        buscaTipoRecebimento().then(function (result) {
            $scope.tiposRecebimentos = result;
        });





        $scope.gravar = function () {

            var valorbaixa = $scope.dados.valor_baixa.substring(2, $scope.dados.valor_baixa.length);
            valorbaixa = valorbaixa.replace('.', '');
            valorbaixa = valorbaixa.replace(',', '.');
            valorbaixa = Number(valorbaixa);


            var totalp = valorbaixa + $scope.totalparcial;

            $scope.dados.valorbaixa = $scope.dados.valor_baixa;
            $scope.dados.valordesconto = $scope.dados.desconto;
            $scope.dados.databaixa = $scope.dados.data_pagto;
            $scope.dados.valoracrecismo = $scope.dados.acrescimo;

            novos_dados = $scope.dados;

            function isEquivalent(origem, novos_dados) {
                // nome das propriedades
                var origemProps = Object.getOwnPropertyNames(origem);
                var novos_dadosProps = Object.getOwnPropertyNames(novos_dados);



                //comparando se os dois arrays são iguais
                if (origemProps.length != novos_dadosProps.length) {
                    return false;
                }
                var diference = [];

                for (var i = 0; i < origemProps.length; i++) {
                    var propName = origemProps[i];

                    //comparando se os valores são iguais
                    if (origem[propName] != novos_dados[propName]) {

                        diference[propName] = novos_dados[propName];



                        //se forem diferente o array diference recebe o valor 
                    }

                }

                return diference;


            }

            var alterados = isEquivalent(origem, novos_dados);
            var json = Object.assign({}, alterados);


            if ($scope.dados.valor_baixa == $scope.dados.valor) {

                $mdDialog.show(confirmTotal).then(function () {

                    $http.post(urlServidor.urlServidorChatAdmin + '/pagar/baixa', $scope.dados).then(function (response) {
                        if (response.data.code) {
                            myFunctions.showAlert('Erro na gravação!')
                        } else {
                            myFunctions.showAlert('Baixa executada com sucesso!');
                            $scope.isDisabled = true;

                            var alterados = isEquivalent(origem, novos_dados);
                            var json = Object.assign({}, alterados);
                            if (JSON.stringify(json) !== '{}') {

                                var origem_json = Object.assign({}, origem);

                                $scope.logs = {
                                    id: _id,
                                    operacao: 'INCLUSÃO',
                                    tipo: 'BAIXA TOTAL',
                                    id_usuario: localStorage.getItem('id_usuario'),
                                    alteracoes: JSON.stringify(json),
                                    anterior: JSON.stringify(origem_json)
                                }
                            }
                            $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro', $scope.logs).then(function (response) {
                                if (response.data.code) {
                                    $scope.erro = response;

                                }
                            });
                        }
                        ;
                    });
                });

            } else {


                if (totalp.toFixed(2) < valor) {

                    $mdDialog.show(confirm).then(function () {
                        $mdDialog.show(confirmDesc).then(function () {

                            buscaParciais($scope.dados.id).then(function (result) {
                                if (result.length > 0) {
                                    $scope.parciais = result;
                                    $scope.totalparcial = result[0].total;


                                } else {
                                    $scope.parciais = 0;
                                    $scope.totalparcial = 0;
                                }

                                var totalp = valor - $scope.totalparcial;

                                var valordesconto = totalp - valorbaixa;

                                $scope.dados.desconto = myFunctions.numberToReal(valordesconto);
                                $scope.dados.valordesconto = $scope.dados.desconto;
                                $scope.dados.valor_baixa = myFunctions.numberToReal(valorbaixa);
                            });
                            setTimeout(function () {
                                /*   $scope.dadosDesc = {
                                        valor_baixa : $scope.dados.valorbaixa,
                                        data_pagto : new Date,
                                        desconto : $scope.dados.valordesconto,
                                        id_conta : $scope.dados.conta,
                                        tipo_recebimento : $scope.dados.tipo_recebimento
                                    }*/

                                $http.post(urlServidor.urlServidorChatAdmin + '/pagar/baixa-comdesconto', $scope.dados).then(function (response) {
                                    if (response.data.code) {
                                        myFunctions.showAlert('Erro na gravação!');
                                    } else {
                                        myFunctions.showAlert('Baixa executada com sucesso!')
                                        $scope.isDisabled = true;
                                        var alterados = isEquivalent(origem, novos_dados);
                                        var json = Object.assign({}, alterados);
                                        if (JSON.stringify(json) !== '{}') {

                                            var origem_json = Object.assign({}, origem);

                                            $scope.logs = {
                                                id: _id,
                                                operacao: 'INCLUSÃO',
                                                tipo: 'BAIXA COM DESCONTO',
                                                id_usuario: localStorage.getItem('id_usuario'),
                                                alteracoes: JSON.stringify(json),
                                                anterior: JSON.stringify(origem_json)
                                            }
                                        }
                                        $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro', $scope.logs).then(function (response) {
                                            if (response.data.code) {
                                                $scope.erro = response;

                                            }
                                        });
                                    }
                                    ;
                                });

                            }, 1500);



                        });
                    }, function () {
                        $mdDialog.show(confirmParc).then(function () {
                            var valor = $scope.dados.valor.substring(2, $scope.dados.valor.length);
                            valor = valor.replace('.', '');
                            valor = valor.replace(',', '.');
                            valor = Number(valor);

                            $http.post(urlServidor.urlServidorChatAdmin + '/pagar/baixa-parcial', $scope.dados).then(function (response) {
                                if (response.data.code) {
                                    myFunctions.showAlert('Erro na gravação!')
                                } else {
                                    buscaParciais($scope.dados.id).then(function (result) {
                                        $scope.parciais = result;
                                        $scope.totalparcial = result[0].total;
                                        var totalp = valor - $scope.totalparcial;
                                        $scope.dados.valor_baixa = myFunctions.numberToReal(totalp);
                                    });
                                    if ($scope.totalparcial >= $scope.dados.valor) {
                                        $scope.baixadisable = true;
                                    }
                                    myFunctions.showAlert('Baixa executada com sucesso!')
                                    $scope.isDisabled = true;
                                    var alterados = isEquivalent(origem, novos_dados);
                                    var json = Object.assign({}, alterados);
                                    if (JSON.stringify(json) !== '{}') {

                                        var origem_json = Object.assign({}, origem);

                                        $scope.logs = {
                                            id: _id,
                                            operacao: 'INCLUSÃO',
                                            tipo: 'BAIXA PARCIAL',
                                            id_usuario: localStorage.getItem('id_usuario'),
                                            alteracoes: JSON.stringify(json),
                                            anterior: JSON.stringify(origem_json)
                                        }
                                    }
                                    $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro', $scope.logs).then(function (response) {
                                        if (response.data.code) {
                                            $scope.erro = response;

                                        }
                                    });

                                }
                                ;
                            });
                        });
                    });
                } else if (totalp > valor) {

                    $mdDialog.show(confirmAc).then(function () {

                        $mdDialog.show(confirmAcres).then(function () {
                            var acrescimo = (valorbaixa - valor);
                            if (acrescimo < 0) {
                                $scope.dados.acrescimo = myFunctions.numberToReal(acrescimo * (-1));
                            } else {
                                $scope.dados.acrescimo = myFunctions.numberToReal(acrescimo);
                            }

                            $http.post(urlServidor.urlServidorChatAdmin + '/pagar/baixa-comacrescimo', $scope.dados).then(function (response) {
                                if (response.data.code) {
                                    myFunctions.showAlert('Erro na gravação!')
                                } else {
                                    myFunctions.showAlert('Baixa executada com sucesso!');
                                    $scope.isDisabled = true;
                                    var alterados = isEquivalent(origem, novos_dados);
                                    var json = Object.assign({}, alterados);
                                    if (JSON.stringify(json) !== '{}') {

                                        var origem_json = Object.assign({}, origem);

                                        $scope.logs = {
                                            id: _id,
                                            operacao: 'INCLUSÃO',
                                            tipo: 'BAIXA COM ACRESCIMO',
                                            id_usuario: localStorage.getItem('id_usuario'),
                                            alteracoes: JSON.stringify(json),
                                            anterior: JSON.stringify(origem_json)
                                        }
                                    }
                                    $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro', $scope.logs).then(function (response) {
                                        if (response.data.code) {
                                            $scope.erro = response;

                                        }
                                    });
                                };
                            });
                        });
                    });

                } else {


                    $mdDialog.show(confirmTotal).then(function () {

                        $http.post(urlServidor.urlServidorChatAdmin + '/pagar/baixatotalparcial', $scope.dados).then(function (response) {
                            if (response.data.code) {
                                myFunctions.showAlert('Erro na gravação!')
                            } else {
                                myFunctions.showAlert('Baixa executada com sucesso!');
                                $scope.isDisabled = true;

                                var alterados = isEquivalent(origem, novos_dados);
                                var json = Object.assign({}, alterados);
                                if (JSON.stringify(json) !== '{}') {

                                    var origem_json = Object.assign({}, origem);

                                    $scope.logs = {
                                        id: _id,
                                        operacao: 'INCLUSÃO',
                                        tipo: 'BAIXA FINAL',
                                        id_usuario: localStorage.getItem('id_usuario'),
                                        alteracoes: JSON.stringify(json),
                                        anterior: JSON.stringify(origem_json)
                                    }
                                }
                                $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro', $scope.logs).then(function (response) {
                                    if (response.data.code) {
                                        $scope.erro = response;

                                    }
                                });
                            }
                            ;
                        });

                    });

                }

            }


        };
        function buscaPatios(idgrupo) {
            return new Promise(function (resolve, reject) {
                $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar').then(function (response) {
                    let dados = response.data;
                    resolve(dados);
                });
            })
        }

        function buscaFornecedores() {
            return new Promise(function (resolve, reject) {
                $http.put(urlServidor.urlServidorChatAdmin + '/fornecedores/listar').then(function (response) {
                    let dados = response.data;
                    resolve(dados);
                });
            })
        }

        function buscaCategorias() {
            return new Promise(function (resolve, reject) {
                $http.put(urlServidor.urlServidorChatAdmin + '/categorias/listar').then(function (response) {
                    let dados = response.data;
                    resolve(dados);
                });
            })
        }

        function buscaTipoRecebimento() {
            return new Promise(function (resolve, reject) {
                $http.put(urlServidor.urlServidorChatAdmin + '/tipos-recebimento/listar-tipo', { tipo: 'PAGAR' }).then(function (response) {
                    let dados = response.data;
                    resolve(dados);
                });
            })
        }

        function buscaDados() {

            return new Promise(function (resolve, reject) {

                $http.get(urlServidor.urlServidorChatAdmin + '/pagar/busca-id', { params: { id_pagar: _id } }).then(function (response) {
                    resolve(response.data[0]);
                });

            })

        }

        function buscaContas() {
            return new Promise(function (resolve, reject) {
                $http.put(urlServidor.urlServidorChatAdmin + '/contas/listar').then(function (response) {
                    let dados = response.data;
                    resolve(dados);
                });
            })
        }

        $scope.fechar = function () {

            window.close();
        }

        $scope.apagarParcial = function (id, valor) {


            id_parc = id;
            $http.post(urlServidor.urlServidorChatAdmin + '/pagar/apaga-parcial', { id: id_parc }).then(function (response) {
                if (response.data.code) {
                    myFunctions.showAlert('Não foi possível excluir o pagamento!!');
                } else {

                    var alt = {

                        idparc: id_parc,
                        valor: valor

                    }
                    var json = Object.assign({}, alt);
                    $scope.logs = {
                        id: _id,
                        operacao: 'EXCLUSÃO',
                        tipo: 'EXCLUSÃO DE PARCIAL',
                        id_usuario: localStorage.getItem('id_usuario'),
                        alteracoes: JSON.stringify(json),
                        anterior: null

                    };
                    $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro', $scope.logs).then(function (response) {
                        if (response.data.code) {
                            $scope.erro = response;

                        }
                    });
                    buscaParciais($scope.dados.id).then(function (result) {

                        if (result.length > 0) {

                            $scope.parciais = result;
                            $scope.totalparcial = result[0].total;
                            aux = valor - $scope.totalparcial;
                            $scope.dados.valor_baixa = myFunctions.numberToReal(aux);


                        } else {
                            $scope.parciais = 0
                            $scope.totalparcial = 0;
                            $scope.dados.valor_baixa = $scope.dados.valor;
                        }
                    });

                    setTimeout(function () {
                        myFunctions.showAlert('Pagamento excluido com sucesso !!');
                    }, 1000);

                }
            });
        }


        function buscaParciais(idbaixa) {
            return new Promise(function (resolve, reject) {
                $http.put(urlServidor.urlServidorChatAdmin + '/pagar/listar-parciais', { id: idbaixa }).then(function (response) {
                    let dados = response.data;
                    resolve(dados);
                });
            })
        }


        function makeid() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        }







    });