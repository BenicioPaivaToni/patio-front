"use strict";
angular.module("app").controller("veiculosNovoCtrl", function ($location, $scope, $http, urlServidor, myFunctions, $sce, urlImagens, buckets ) {

    $scope.dados = {
        id_empresa: "",
        descricao: "",
        placa: "",
        tipo: "",
        categoria: "",
        ativo: "1",
        observacao: "",
        pdf_crlv: "",
        pdf_seguro: "",
        vencimento_seguro: "",
        vencimento_crlv: "",
        ano_fabricacao: "",
        ano_modelo: "",
        chassi: "",
        nome_proprietario: "",
        combustivel: "",
        cor: "",
    };



    buscaEmpresas().then(function (result) {
        
        $scope.empresas =[]

        $scope.empresas = result

    })


    $scope.isDisabled = false;

    $scope.gravar = () => {
                
        $http
            .post(
                urlServidor.urlServidorChatAdmin + "/veiculos/cadastrar",
                $scope.dados
            )
            .then(({ data }) => {
            if (data.code) {
                myFunctions.showAlert("Erro na gravação!");
                return;
            }

            if (data == "11000") {
                myFunctions.showAlert("Veiculo já existe!");
                return;
            }

            myFunctions.showAlert("Cadastro executado com sucesso!");
            $scope.isDisabled = true;
        })

    }




    function buscaEmpresas() {
        return new Promise(function (resolve, reject) {
        $http.put(urlServidor.urlServidorChatAdmin + '/empresas/listar', ).then(function (response) {
                    
            let dados = response.data
                resolve(dados)
            })
        })
    }

    

});
