angular
  .module("app")
  .controller(
    "classificacaoLeilaoNovoCtrl",
    function ($scope, $http, urlServidor, myFunctions) {

      $scope.methods = {
          descricao :'',
          percentual:1.0,
          tipo:'',
          disable : false,


          async Salvar(){
             const dados = {
              descricao: this.descricao,
              percentual: this.percentual,
              tipo: this.tipo,
             }

            await $http.post(`${urlServidor.urlServidorChatAdmin}/classificacao-leilao/cadastrar`,dados)
             this.disable = true
             myFunctions.showAlert(`Classificação : ${this.descricao} foi criado com sucesso`)
            },


            
            Voltar : () => window.close()

      }



      
    }
  );
