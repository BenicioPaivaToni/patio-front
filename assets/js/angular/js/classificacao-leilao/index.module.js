angular
  .module("app")
  .controller(
    "classificacaoLeilaoCtrl",
    function ($scope, $http, urlServidor, myFunctions) {
      $scope.methods = {
        descricao: "",
        dados: "",
        async listar() {

          const response = await $http.get(
            `${urlServidor.urlServidorChatAdmin}/classificacao-leilao/listar`,
            this.descricao
          
          
          );

          const { data } = response;

          this.dados = data
          
        },
       
        editar (item) {
            console.log(item);


          window.open(`/#/classificacao-leilao-editar/?id=${item.id}&descricao=${item.descricao}&percentual=${item.percentual}&tipo=${item.tipo}`)
        },
        

        novo: () => window.open("/#/classificacao-leilao-novo"),
      };

      console.log(
        "🚀 ~ file: index.module.js ~ line 13 ~ $scope.methods",
        $scope.methods
      );
    }
  );
