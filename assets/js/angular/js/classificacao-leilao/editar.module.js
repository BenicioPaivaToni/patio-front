angular
  .module("app")
  .controller(
    "classificacaoLeilaoEditarCtrl",
    function ($scope, $http, urlServidor, myFunctions, $location) {
      const dados = $location.search();

      $scope.methods = {
        id: dados.id,
        descricao: dados.descricao,
        percentual: dados.percentual,
        tipo: dados.tipo,
        disable: false,

        async Salvar() {
          const dados = {
            id: this.id,
            descricao: this.descricao,
            percentual: this.percentual,
            tipo: this.tipo,
          };

          await $http.post(
            `${urlServidor.urlServidorChatAdmin}/classificacao-leilao/alterar`,
            dados
          );
          this.disable = true;
          myFunctions.showAlert(
            `Classificação : ${this.descricao} foi editado com sucesso`
          );
        },

        Voltar: () => window.close(),
      };
    }
  );
