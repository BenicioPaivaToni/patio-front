angular.module("app").controller('patiosEditarCtrl', function ($scope, $location, $http, urlServidor, myFunctions, estados, viaCep, $filter, buckets) {

    $scope.dados = {
        Search: ''
    }


    var urlParams = $location.search();


    AWS.config.update({ accessKeyId: dadosAws.ac, secretAccessKey: dadosAws.sc });
    AWS.config.region = dadosAws.regiao;
    var bucket = new AWS.S3();



    $scope.regraPercentual = function () {
        
        let calculaPercentul = $scope.patio.meta_apreensoes  * ($scope.metas.percentual_aumento / 100) + $scope.patio.meta_apreensoes
        $scope.metas.quantidade_veiculos = Math.round(calculaPercentul)
    }


   
    $scope.regraVeiculos = function () {
       $scope.metas.percentual_aumento = ((100 * ($scope.patio.meta_apreensoes - $scope.metas.quantidade_veiculos)) / $scope.patio.meta_apreensoes).toFixed(0)
        
        if ($scope.metas.percentual_aumento < 0 ) {
            $scope.metas.percentual_aumento = $scope.metas.percentual_aumento * -1
        }
        
        if ($scope.metas.quantidade_veiculos < $scope.patio.meta_apreensoes) {
            $scope.metas.percentual_aumento = 0
        }
    }


    $scope.patio = {
        id: urlParams.dados[0].id,
        nome: urlParams.dados[0].nome,
        endereco: urlParams.dados[0].endereco,
        bairro: urlParams.dados[0].bairro,
        cep: urlParams.dados[0].cep,
        cidade: urlParams.dados[0].cidade,
        estado: urlParams.dados[0].estado,
        telefone: urlParams.dados[0].telefone,
        ativo: urlParams.dados[0].ativo,
        email: urlParams.dados[0].email,
        responsavel: urlParams.dados[0].responsavel,
        conta: urlParams.dados[0].conta,
        observacao: urlParams.dados[0].observacao,
        info: urlParams.dados[0].info_liberacao,
        meta_apreensoes: urlParams.dados[0].meta_apreensoes,
        meta_liberacoes: urlParams.dados[0].meta_liberacoes,
        ponto_equilibrio: urlParams.dados[0].ponto_equilibrio,
        contrato: urlParams.dados[0].contrato,
        whatsapp: urlParams.dados[0].whatsapp,
        inicio_contrato: $filter('date')(urlParams.dados[0].inicio_contrato, 'dd/MM/yyyy', "+0000"),
        fim_contrato: $filter('date')(urlParams.dados[0].fim_contrato, 'dd/MM/yyyy', "+0000"),
        vencimento_seguro: $filter('date')(urlParams.dados[0].vencimento_seguro, 'dd/MM/yyyy', "+0000"),
        pdf_seguro: urlParams.dados[0].pdf_seguro,
        pdf_contrato: urlParams.dados[0].pdf_contrato,
        pdf_vistoria_checklist: urlParams.dados[0].pdf_vistoria_checklist,
    }

    $scope.arquivo = '';
    $scope.arquivoChecklist = '';
    $scope.arquivoSeguro = '';
    $scope.estados = estados.uf;
    $scope.buscaCep = (cep) => {

        if (cep.length == 9) {

            viaCep.get(cep).then(function (response) {

                $scope.patio.endereco = response.logradouro.toUpperCase();
                $scope.patio.bairro = response.bairro.toUpperCase();
                $scope.patio.cidade = response.localidade.toUpperCase();
                $scope.patio.estado = response.uf.toUpperCase();
            });
        }

    }
    $scope.cidades



    const getCidadesByPatio = async () => {
        const id_patio = $scope.patio.id

        const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/cidades/listar-patios-cidades`, { params: { id_patio: id_patio } })
        const { data } = response
        $scope.CidadesByPatio = data
    }
    getCidadesByPatio()



    $scope.BuscaCidades = async (cidade) => {

        if (cidade.length > 2) {
            const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/cidades/listar`, { params: { nome: cidade } })
            $scope.cidades = response.data
            document.getElementById("error").style.display = "none"

        } else if (cidade.length < 2) {
            document.getElementById("error").style.display = "block"
        }

    }


    const buscaMetas = async () => {
        const patio = $scope.patio.id

        $scope.dadosMetas = []

        const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/patios/metas-apreensao/${patio}`)
        const { data } = response

        $scope.dadosMetas = data.map(item => ({
            id: item.id,
            patio: $scope.patio.nome,
            mes_ano: item.mes_ano,
            percentual: item.percentual,
            qtde: item.qtde
        }))

    }

    buscaMetas()




    $scope.detalhe = function (ev, rowId) {

        var dados = $scope.dadosMetas.filter(function (obj) {
            return obj.id == rowId
        })
       
        $scope.metas = {
            metasId: dados[0].id,
            mes_ano: dados[0].mes_ano,
            quantidade_veiculos: dados[0].qtde,
            percentual_aumento: dados[0].percentual
        }
    }


    $scope.limpar = function (ev) {
        $scope.metas.metasId = ''
        $scope.metas.mes_ano = ''
        $scope.metas.percentual_aumento = ''
        $scope.metas.quantidade_veiculos = ''
    }



    $scope.AddCidade = async (cidade) => {
        const { id } = cidade

        const id_patio = $scope.patio.id

        await $http.post(`${urlServidor.urlServidorChatAdmin}/cidades/cadastrar-patios-cidades`, { id_patio: id_patio, id_cidade: id })
            .then(() => {
                myFunctions.showAlert(`Cidade ${cidade.nome} foi adicionada ao pátio ${$scope.patio.nome}`)
                getCidadesByPatio()


            })


    }



    $scope.RemoveCidade = async (cidade) => {

        await $http.post(`${urlServidor.urlServidorChatAdmin}/cidades/excluir-patios-cidades`, { id: cidade.id })
            .then(() => {
                myFunctions.showAlert(`Cidade ${cidade.nome} foi deletada do pátio ${$scope.patio.nome}`)
                getCidadesByPatio()
            })
    }


    function buscaContas() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/contas/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }


    buscaContas().then(function (result) {
        $scope.contas = result;
    });

    $scope.uploadContrato = (file) => {

        $scope.pdfContrato = file;
        $scope.arquivo = file;
        $scope.patio.pdf_contrato = file.name;
    };
    $scope.uploadChecklist = (file) => {

        $scope.pdfChecklist = file;
        $scope.arquivoChecklist = file;
        $scope.patio.pdf_vistoria_checklist = file.name;
    };
    $scope.uploadSeguro = (file) => {
        $scope.pdfSeguro = file;
        $scope.arquivoSeguro = file;
        $scope.patio.pdf_seguro = file.name;
    };
    $scope.limparPDFContrato = () => {
        $scope.pdfContrato = null;
        $scope.arquivo = null;
        $scope.patio.pdf_contrato = null;
    }

    $scope.limparPDFChecklist = () => {
        $scope.pdfChecklist = null;
        $scope.arquivoChecklist = null;
        $scope.patio.pdf_vistoria_checklist = null;
    }

    $scope.limparPDFSeguro = () => {
        $scope.pdfSeguro = null;
        $scope.arquivoSeguro = null;
        $scope.patio.pdf_seguro = null;
    };
    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/patios/alterar', $scope.patio).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    myFunctions.showAlert('Patio já existe!')
                } else {
                    myFunctions.showAlert('Alteração executada com sucesso!');
                    if ($scope.arquivo.name != undefined) {
                        var params = {
                            Key: $scope.arquivo.name,
                            ContentType: $scope.arquivo.type,
                            Body: $scope.arquivo,
                            ACL: 'public-read',
                            Bucket: buckets.patios
                        };
                        bucket.putObject(params, function (err, data) {
                            if (err) {
                                myFunctions.showAlert('Erro envio PDF Contrato, verifique...' + err);
                            }
                            ;
                        });
                    }

                    setTimeout(function () {

                        if ($scope.arquivoChecklist.name != undefined) {
                            var params = {
                                Key: $scope.arquivoChecklist.name,
                                ContentType: $scope.arquivoChecklist.type,
                                Body: $scope.arquivoChecklist,
                                ACL: 'public-read',
                                Bucket: buckets.patios
                            };
                            bucket.putObject(params, function (err, data) {
                                if (err) {
                                    myFunctions.showAlert('Erro envio PDF Checklist, verifique...' + err);
                                }
                                ;
                            });
                        }

                    }, 1500);
                    setTimeout(function () {

                        if ($scope.arquivoSeguro.name != undefined) {
                            var params = {
                                Key: $scope.arquivoSeguro.name,
                                ContentType: $scope.arquivoSeguro.type,
                                Body: $scope.arquivoSeguro,
                                ACL: 'public-read',
                                Bucket: buckets.patios
                            };
                            bucket.putObject(params, function (err, data) {
                                if (err) {
                                    myFunctions.showAlert('Erro envio PDF seguro, verifique... ' + err);
                                }
                                ;
                            });
                        }

                    }, 3000);
                }

            }
            ;
        });
    };



  
    $scope.gravarMetas = function() {

        if (!$scope.metas.metasId) {
            
            const procuraIguais = $scope.dadosMetas.find(item => item.mes_ano === $scope.metas.mes_ano)

            if (procuraIguais){

                myFunctions.showAlert( 'Já existe cadastro de metas para o mês e ano selecionado! Verifique!' )
            
                return 

            }else{

                $http.post(urlServidor.urlServidorChatAdmin+'/patios/metas-apreensao-cadastro', {

                    id_patio: urlParams.dados[0].id,
                    mes_ano: $scope.metas.mes_ano,
                    qtde: $scope.metas.quantidade_veiculos,
                    percentual: $scope.metas.percentual_aumento
            
                }).then( function(response){
    
                    if (response.data.code){
                
                        myFunctions.showAlert('Erro na gravação!')
    
                    }else{
    
                        {                    
                            myFunctions.showAlert( 'Gravação efetuada com sucesso!' );

                            $scope.limpar()
                           
                            buscaMetas()
                             
                        }         
                    }
                })

            }

        }else{            

            $http.post(urlServidor.urlServidorChatAdmin+'/patios/metas-apreensao-alterar', {

                id: $scope.metas.metasId,
                mes_ano: $scope.metas.mes_ano,
                qtde: $scope.metas.quantidade_veiculos,
                percentual: $scope.metas.percentual_aumento
        
            }).then( function(response){

                if (response.data.code){
            
                    myFunctions.showAlert( 'Erro na alteração dos dados!' )

                }else{

                    {                    
                        myFunctions.showAlert( 'Alteração efetuada com sucesso!' )

                        $scope.limpar()
                       
                        buscaMetas()
                         
                    }         
                }
            })

        }
        
    }
   
})