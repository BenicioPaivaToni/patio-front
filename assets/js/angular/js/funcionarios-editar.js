angular.module('app').controller('funcionariosEditarCtrl', function ($rootScope, $window, $scope, $location, $sce, $http, urlServidor, myFunctions, $mdDialog, estados, viaCep, Upload, S3UploadService, ncvFac, urlImagens,buckets) {

    const queryString = window.location.hash;

    var _id =  queryString.slice(queryString.indexOf("%") + 3); 

    $scope.dados = null ;
    var bucket = null;

    $scope.$on('$viewContentLoaded', function () {

        buscaDados(_id).then(function (result) {
            $scope.dados = result;
          $scope.dados.id_patio = $scope.dados.patio;
        });
        
        loadDocuments();
        loadHistorico();
    });
   
    
    buscaPatios().then( function(result){
        $scope.patios = result ;
    });
    

    buscaEmpresas().then(function (result) {
        $scope.empresas = result
      
    })


    $http.get(urlServidor.urlServidorChatAdmin + '/autoridades/vercodigo').then(function (response) {

        AWS.config.update({accessKeyId: response.data.ac, secretAccessKey: response.data.sc});
        AWS.config.region = response.data.regiao;
    }).finally( function(){

        bucket = new AWS.S3();
    })

    
	function loadDocuments() {
       
		return new Promise(function () {
            $http.get(urlServidor.urlServidorChatAdmin+'/funcionarios/listar-documentos', {params: {id: _id}}).then(function (response) {
					const { data } = response
					$scope.documentos = data
				}).catch(function (error) {
					console.log(error)
				})
        })
    }


    function buscaEmpresas() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/empresas/listar', ).then(function (response) {
                 
                let dados = response.data
                resolve(dados)
            })
        })
    }


    function loadHistorico() {
		return new Promise(function () {
            
            $http.get(urlServidor.urlServidorChatAdmin+'/funcionarios/listar-historicos',{params: {id: _id}}).then(function (response) {
					const { data } = response
					$scope.historico = data
				}).catch(function (error) {
					console.log(error)
				})
        })
    }

    
    function buscaPatios() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', {idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa')}).then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }
       
    
    $scope.atualizarDatas = function () {

        var minutes = 1000 * 60;
        var hours = minutes * 60;
        var days = hours * 24;
        var years = days * 365;
        let exp = days * 42;
      
        var data = $scope.dados.data_admissao;
        $scope.dados.data_ferias = new Date(data.getTime() + years);
        $scope.dados.data_experiencia = new Date(data.getTime() + exp);

        /*var data = $scope.dados.data_admissao;
        $scope.dados.data_ferias = new Date(data.getTime() + (365 * 24 * 60 * 60 * 1000));
        $scope.dados.data_experiencia = new Date(data.getTime() + (45 * 24 * 60 * 60 * 1000));*/
      }
      
    var confirmDocumento = $mdDialog.confirm()
    .title('Apagar Documento')
    .textContent('Tem certeza que deseja apagar esse arquivo ? ')
    .ariaLabel('Lucky day')
    .ok('Sim')
    .cancel('Não');

    var confirmHistorico = $mdDialog.confirm()
    .title('Apagar Histórico')
    .textContent('Tem certeza que deseja apagar esse histórico ? ')
    .ariaLabel('Lucky day')
    .ok('Sim')
    .cancel('Não');

    $scope.estados = estados.uf ;
    $scope.urlFotos = urlImagens.urlFotos;

    
    $scope.gravar = function(){
        $http.post(urlServidor.urlServidorChatAdmin+'/funcionarios/alterar', $scope.dados).then( function(response){

            if (response.data.code){
            
                myFunctions.showAlert('Erro na gravação!')
    
            }else{

                myFunctions.showAlert('Cadastro alterado com sucesso!')
            }
        })
    }

    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }

    $scope.fechar = function () {
        window.close();
    }

    
$scope.gravar_historicos = function () {
    $scope.historico.id_funcionario = _id;
    let tipo = $scope.historico.tipo ;
    let descricao =  $scope.historico.descricao ;
 
    $http.post(urlServidor.urlServidorChatAdmin + '/funcionarios/incluir-historico', { id_funcionario : _id , tipo: tipo , descricao : descricao  } ).then(function (response) {
     
          if (response.data.code) {
              myFunctions.showAlert('Erro na gravação!')
          } else {
  
              loadHistorico();
              myFunctions.showAlert('Histórico incluido com sucesso!');
          }
     
    });
  };

  $scope.editar_historico = function () {

    let tipo= $scope.historico.tipo;
    let descricao = $scope.historico.descricao;
 
    $http.post(urlServidor.urlServidorChatAdmin + '/funcionarios/alterar-historico', {id_funcionario: _id, tipo: tipo, descricao: descricao}).then(function (response) {
      
          if (response.data.code) {
              myFunctions.showAlert('Erro na gravação!')
          } else {
  
              loadHistorico();
              myFunctions.showAlert('Histórico alterado com sucesso!');
          }
      
    });
  };
  
  
    $scope.gravar_documentos = function (file) {
      
        $scope.documentos.id_funcionario = _id;
        let tipo = $scope.documentos.tipo ;
        let descricao =  $scope.documentos.descricao ;
  
     
      $http.post(urlServidor.urlServidorChatAdmin + '/funcionarios/incluir-documento', {id_funcionario: _id, tipo: tipo, descricao: descricao, pdf: $scope.documentos.pdf}).then(function (response) {
        if(response.data.code){
  
          myFunctions.showAlert('Erro na gravação!')
        }else{
  
            loadDocuments();
            setTimeout( function () {
                myFunctions.showAlert('Documento adicionado!');
              
            },1500);
          
        }
      });
  
  };

  $scope.openFile = function(e,rowId) {
    $scope.arquivo = $scope.documentos.filter(function (obj) {
        return obj.id == rowId
    })
    var documento = $scope.arquivo[0].pdf;
    
    return new Promise(function () {
        
        var url = `${urlImagens.urlDocumentosFuncionarios}/${documento}`;
        $http
            .get(url, {})
            .then(function (response) {
                
                $window.open(url, '_blank')
            })
            .catch(function (error) {
                console.log(error)
            })
    })
}
  
  $scope.apagar_documento = function(e, rowId){
    $mdDialog.show(confirmDocumento).then(function () {
        $http.post(urlServidor.urlServidorChatAdmin + '/funcionarios/apaga-documento',{ id: rowId}).then(function (response) {
            if(response.data.code){
        
                myFunctions.showAlert('Ocorreu um erro ao tentar apagar o documento!')
            }else{
        
                loadDocuments();
                setTimeout( function () {
                    myFunctions.showAlert('Documento apagado com sucesso!');
                  
                },1500);
                
            }
        });
    })
  }

  $scope.apagar_historico = function(ev, rowId){
      
    $mdDialog.show(confirmHistorico).then(function () {
        $http.post(urlServidor.urlServidorChatAdmin + '/funcionarios/apaga-historico',{id: rowId}).then(function (response) {
            if(response.data.code){
        
                myFunctions.showAlert('Ocorreu um erro aot entar apagar o historico!')
            }else{
        
                loadHistorico();
                
                setTimeout( function () {
                    myFunctions.showAlert('Histórico apagado com sucesso!');
                  
                },1500);
            }
        });
    })
  }

    $scope.uploadFoto = (file) => {

        if( file !== null ){    
    
            if (file.type !== null && file.type !== undefined) {
    
                if (file.type.substr(0, 5) == 'image') {
    
                    let nomefoto = makeid() + '.' + file.type.substr(6, 4);
    //			S3UploadService.Upload(file, 'imagens-reidospatios/'+_id , nomefoto ).then(function (result) {
                    S3UploadService.Upload(file, buckets.fotos_funcionarios, nomefoto).then(function (result) {
                        // Mark as success
                        file.Success = true;
                       $scope.dados.foto ='/' + nomefoto;
                    }, function (error) {
                        // Mark the error
                        $scope.Error = error;
                    }, function (progress) {
                        file.Progress = (progress.loaded / progress.total) * 100
                    });
                }
            }
        }
    }
    
    $scope.uploadDocumento = (file) => {

        $scope.pdfDocumento = null;
        $scope.arquivo = null;
        $scope.documentos.pdf = null;
        if (file) {
            $scope.pdfDocumento = file;
            $scope.arquivo = file;
            $scope.documentos.pdf = file.name;
            if ($scope.arquivo != undefined && $scope.arquivo != null) {
    
                            var params = {
                                Key: $scope.arquivo.name,
                                ContentType: $scope.arquivo.type,
                                ContentDisposition: 'inline',
                                Body: $scope.arquivo,
                                ACL: 'public-read',
                                Bucket: buckets.documentos_funcionarios
    
                            };
                            $scope.pdfDocumento = null;
                            $scope.arquivo = null;
                            $scope.documentos.pdf = null;
                            bucket.putObject(params, function (err, data) {
                                if (err) {
                                    myFunctions.showAlert('Erro no envio do PDF, verifique...' + err);
                                
                            }else{
  
                                
                            $scope.pdfDocumento = file;
                            $scope.arquivo = file;
                            $scope.documentos.pdf = file.name;
    
                        };
                    });
                    }
                }
                    
    }                    
      

    function buscaDados(_id) {

        return new Promise(function (resolve, reject) {   

            $http.get(urlServidor.urlServidorChatAdmin + '/funcionarios/busca-funcionario', {params:{ id: _id}}).then(function (response) {
                resolve(response.data[0]); 
            });
            
        })

    };
});
