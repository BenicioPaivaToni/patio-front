'use strict';
angular.module('app')
.controller("gruposNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions) {  

  $scope.dados = {
     descricao:'',
     tipo: '' ,
     ativo: '',
     idempresa: localStorage.getItem('id_empresa')
  }

  $scope.isDisabled = false

  $scope.gravar = function() {        

    $http.post(urlServidor.urlServidorChatAdmin+'/grupos/cadastrar', $scope.dados ).then( function(response){

        if (response.data.code){
            
          myFunctions.showAlert('Erro na gravação!')

        }else{

          if ( response.data == '11000' )                 
          {
            myFunctions.showAlert('Grupo já existe!')
          }else
          {                    

            myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
            $scope.isDisabled = true ;            

          }  
                
        }; 

    }); 

  }; 


  
    
});