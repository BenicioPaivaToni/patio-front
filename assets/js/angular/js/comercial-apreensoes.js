
angular.module('app').controller('comercialApreensoesCtrl', function($scope,$http,urlServidor, myFunctions){
  
    $scope.dataAtual = new Date()
    
    $scope.dados = {
        dataInicio: new Date($scope.dataAtual.getFullYear(), $scope.dataAtual.getMonth() - 1, 1),
        dataFinal: new Date($scope.dataAtual.getFullYear(), $scope.dataAtual.getMonth(), 0),
        patio: null,
        //id_grupo_patios: null,
       // periodo: 'TRIMESTRAL'
    }; 

    //$scope.ShowMessage = false;

    $scope.dataAtual = new Date()

    $scope.limpar = function (ev) {
        $scope.dados.dataInicio = new Date($scope.dataAtual.getFullYear(), $scope.dataAtual.getMonth() - 1, 1)
        $scope.dados.dataFinal = new Date($scope.dataAtual.getFullYear(), $scope.dataAtual.getMonth(), 0)
        $scope.dados.patio = null
        //$scope.dados.periodo = 'TRIMESTRAL'
       // $scope.dados.id_grupo_patios = null;
               
    };

    $scope.dataFinal = function (ev){

        const data = $scope.dados.dataFinal

        data.setMonth(data.getMonth() + 1); 
        data.setDate(0); 
        $scope.dados.dataFinal = data
    }

    
    const buscaPatiosGrupo = async () => {
        const response = await $http.get(
        `${urlServidor.urlServidorChatAdmin}/grupos-patios/listar`
        );
        const { data } = response;
        return data.filter((item) => item.ativo == 1);
    };

   
    buscaPatiosGrupo().then((Response) => ($scope.patiosGrupo = Response));



    $http.put(urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
        {
            idUsuario: localStorage.getItem("id_usuario"),
            idEmpresa: localStorage.getItem("id_empresa"),
        }
        ).then( ({ data })  => {
        $scope.patios = data.filter((item) => item.status == 1);
    });

   
    function atualizaGrid() {

        $scope.dataGridOptions = {
            dataSource: $scope.dadosList,
            // keyExpr: 'ID',
            showBorders: true,
            showColumnLines: true,
            showRowLines: true,
            noDataText: '',
            grouping: {
                contextMenuEnabled: true,
                autoExpandAll: true,
                texts: { 
                    groupContinuedMessage: "Continuação página anterior",
                    groupContinuesMessage: "Continua próxima página"
                }
            },
            editing: {
                mode: 'cell',
                allowUpdating: true,
            },
            
            toolbar: [
                {
                    location: 'top',
                    text: 'Texto da Barra Superior',
                    options: {
                        elementAttr: { class: 'custom-toolbar' }
                    }
                }
            ],
            onContentReady: function(e) {
                e.element.find('.dx-datagrid-header').addClass('custom-header');
            },

                columns: [{
 
                    caption: 'Mes', 
                    alignment: 'center',
                    dataField: 'mes',
                    format: 'fixedPoint',
                    cssClass: "captionTxt",
                    allowEditing: false 
                },             
                {
                
                    caption: 'Apreensoes', 
                    alignment: 'center',
                    cssClass: "captionTxt", 
                    dataField: 'qtde',
                    format: 'fixedPoint',
                    allowEditing: false,
                   // showFooter: true
                },                    
                {
                    caption: 'CONTRATO', alignment: 'center', 
                    columns: [{
                    caption: 'Qtde',
                    alignment: 'center',
                    dataField: 'contrato',
                    format: 'fixedPoint',
                    allowEditing: false 

                    }, {
                    caption: '% APxN', alignment: 'center',
                    alignment: 'center',
                    dataField: 'contrato_apxn',
                    format: 'percent',
                    allowEditing: false,
                    }, {
                        caption: 'Qtde - Ap',
                        alignment: 'center',
                        dataField: 'contrato_con_ap',
                        format: 'fixedPoint',
                        allowEditing: false,
                    }],
                    }, {
                    caption: 'PONTO EQUILIBRIO', alignment: 'center',
                    columns: [{
                    caption: 'Qtde',
                    alignment: 'center',
                    dataField: 'sustentavel_qtde',
                    format: 'fixedPoint',
                   // sortOrder: 'desc',
                    allowEditing: false,
                    }, {
                        caption: '% atingiu APxN',
                        alignment: 'center',
                        dataField: 'sustentavel_apxn',
                        format: 'percent',
                        allowEditing: false,
                    }, {
                        caption: 'Qtde Sust - Ap',
                        alignment: 'center',
                        dataField: 'sustentavel_sust_ap',
                        format: 'fixedPoint',
                        allowEditing: false,
                    }],
                
                },
               
            ],
            summary: {
                totalItems: [{
                    column: 'qtde', 
                   summaryType: 'sum', 
                   displayFormat: 'Total: {0}', 
                }],
            }    
                            
        }
    }



    function atualizaGridProjecao() {

        $scope.dataGridOptionsProjecao = {
            dataSourceProjecao: $scope.dadosListProjecao,             
            showBorders: true,
            showColumnLines: true,
            showRowLines: true,
            ShowNoDataMessage: false,
            noDataText: '',
            grouping: {
                contextMenuEnabled: true,
                autoExpandAll: true,
                texts: { 
                    groupContinuedMessage: "Continuação página anterior",
                    groupContinuesMessage: "Continua próxima página"
                }
            },
            editing: {
                mode: 'cell',
                allowUpdating: true,
            },
           
            columns:
                       
                [   
                    {
                        caption: 'Mes', 
                        alignment: 'center',
                        dataField: 'mes',
                        format: 'fixedPoint',
                       // cssClass: "captionTxt",
                        allowEditing: false 
                    },           
                    {
                    
                    caption: 'Apr. Media', 
                    alignment: 'center',
                   // cssClass: "captionTxt", 
                    dataField: 'qtde',
                    format: 'fixedPoint',
                    allowEditing: false,
                    // showFooter: true
                    }, 
                    {
                    caption: 'PONTO EQUILIBRIO', alignment: 'center',
                    columns: [{
                    caption: 'Qtde',
                    alignment: 'center',
                    dataField: 'sustentavel_qtde',
                    format: 'fixedPoint',
                   // sortOrder: 'desc',
                    allowEditing: true,
                    }, {
                        caption: '% atingiu APxN',
                        alignment: 'center',
                        dataField: 'sustentavel_apxn',
                        format: 'percent',
                        allowEditing: false,
                   
                    }],
                    }, {
                    
                        caption: 'PROJEÇÃO', alignment: 'center',
                            columns: [{
                                caption: '% Cresc.',
                                alignment: 'center',
                                dataField: 'ap_digita',
                                dataType: 'number',
                                allowEditing: true,
                                format: {
                                type: 'fixedPoint',
                                precision: 0,
                                }
                            }, {
                                caption: 'Qtde ',
                                alignment: 'center',
                                dataField: 'ap_qtde',
                                allowEditing: false,
                                format: {
                                    type: 'fixedPoint',
                                    precision: 0,
                                }
                                }, {
                                caption: 'Resultado',
                                alignment: 'center',
                                dataField: 'ap_ap_qtde',
                                allowEditing: false,
                                format: {
                                    type: 'fixedPoint',
                                    precision: 0
                                }   
                                
                            }],
                    }, {
                        
                        caption: 'GANHO COMERCIAL', alignment: 'center',
                            columns: [{
                                caption: 'Valor R$',
                                alignment: 'center',
                                dataField: 'ganho_digita',
                                allowEditing: true,
                                dataType: 'number',
                                format: {
                                type: 'fixedPoint',
                                precision: 0,
                                }
                            }, {
                                caption: 'Cresc.',
                                alignment: 'center',
                                dataField: 'ganho_repete',
                                allowEditing: false,
                                format: {
                                type: 'fixedPoint',
                                precision: 0,
                                }
                            }, {
                                caption: 'Resultado',
                                alignment: 'center',
                                dataField: 'ganho_multiplica',
                                format: {
                                    style: 'currency', 
                                    precision: 2,
                                    currency: 'BRL',
                                    symbol: 'R$ ',
                                },
                                allowEditing: false,             
                            }
                        ],
                        
                    },{   
                        caption: 'Adicionar',
                        alignment: 'center',
                        cssClass: "captionTxt",
                        cellTemplate: function (container, options) {
                            $("<div>")
                                .append(
                                    $("<button>")
                                        .addClass("btn btn-success")
                                        .text("+")
                                        .on("dxclick", function (e) {
                                            e.stopPropagation();
                                            
                                            const ultimaEntrada = $scope.dadosListProjecao[$scope.dadosListProjecao.length - 1]
                                            
                                            const novoMes = adicionarUmMes(ultimaEntrada.mes)
                            
                                            const qtde = ultimaEntrada.qtde + ultimaEntrada.ap_ap_qtde
                                            
                                            $scope.dadosListProjecao.push({
                                                mes: novoMes, 
                                                qtde: qtde, 
                                                sustentavel_qtde: null, 
                                                sustentavel_apxn: null, 
                                                sustentavel_sust_ap: null, 
                                                ap_digita: null, 
                                                ap_qtde: qtde, 
                                                ap_ap_qtde: null, 
                                                ganho_digita: null,
                                                ganho_repete: null, 
                                                ganho_multiplica: null 
                                            });

                                           $("#gridContainerProjecao").dxDataGrid({
                
                                            dataSource: $scope.dadosListProjecao
                                        
                                        })
                                    })
                                )
                            .appendTo(container);
                        }
            
                    }],
                          
                    summary: {
                        totalItems: [{
                            column: 'qtde', 
                           summaryType: 'sum', 
                           displayFormat: 'Total: {0}', 
                        }],
                    },

                    onEditorPreparing: function(e) {
            
                        if (e.parentType === "dataRow") {
                        var grid = e.component
                        var rowIdx = e.row.rowIndex
                        var rowData = e.row.data
                        
                        if (e.dataField === "sustentavel_qtde") { //campo "Qtde" em PONTO EQUILIBRIO
                            
                            e.editorOptions.onValueChanged = function(args) {
                                
                                grid.cellValue(rowIdx,e.dataField,args.value);
                                                              
                                let value = args.value

                                calculaPontoEquilibrio(value, rowIdx, rowData)

                            }
                        }

                        
                        if (e.dataField === "ap_digita") { //campo digitavel "% Cresc" em PROJEÇÃO
                            
                            e.editorOptions.onValueChanged = function(args) {
                                
                                grid.cellValue(rowIdx,e.dataField,args.value);
                                
                                let value = args.value
                                
                                calculaProjecao(value, rowIdx, rowData)

                            }
                        }


                        if (e.dataField === "ganho_digita") { //campo "Valor" em GANHO COMERCIAL
                            
                            e.editorOptions.onValueChanged = function(args) {
                                
                                grid.cellValue(rowIdx,e.dataField,args.value);
                                                              
                                let value = args.value

                                calculaGanhoComercial(value, rowIdx, rowData)

                            }
                        }


                    }
                }
            
                            
        }
    }



    function calculaPontoEquilibrio(value, rowIdx, rowData) {
        
        let dados = rowData
        const sustentavel_qtde = value
       
        const sustentavel_sust_ap = dados.qtde - sustentavel_qtde

        const sustentavel_apxn = (( dados.qtde / sustentavel_qtde) * 100).toFixed(1) + '%'
        
        dados.sustentavel_sust_ap = sustentavel_sust_ap

        dados.sustentavel_apxn = sustentavel_apxn
       
    }


    
    function calculaProjecao(value, rowIdx, rowData) {
        
        let dados = rowData
        const ap_digita = value
       
        const aumento = Math.ceil((ap_digita / 100) * dados.ap_qtde);

        dados.ap_ap_qtde = aumento
        dados.ganho_repete = aumento
       
    }


    function calculaGanhoComercial(value, rowIdx, rowData) {
        
        let dados = rowData
        
        const ganho_repete = dados.ap_ap_qtde

        if (!ganho_repete){
            
            myFunctions.showAlert('Entre com % Cresc. na coluna PROJEÇÃO antes de entrar com valor de GANHO COMERCIAL!')

            return
        }
                
        const ganho_digita = value
       
        const aumento = ganho_digita * dados.ganho_repete

        dados.ganho_multiplica = aumento

        dados.ganho_repete = ganho_repete

    }



    $scope.pesquisar = async () => { 
        
        if ($scope.dados.patio !== null || $scope.dados.id_grupo_patios !== null){ 
           
            let dados_valores
            
            $scope.dataGridOptions = null ;        
            
            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/gerencial/comercial-apreensoes', { params: $scope.dados })

            let dados = response.data

            let dadosContrato = dados.map(function(obj) {

                obj.contrato_con_ap = obj.qtde - obj.contrato
                
                if (obj.qtde && obj.contrato) {
                    obj.contrato_apxn = ((obj.qtde / obj.contrato) * 100).toFixed(1) + '%';
                } else {
                    obj.contrato_apxn = '0%'; 
                }

                return obj;
            });


            let dadosSustentavel = dadosContrato.map(function(idx) {

                idx.sustentavel_qtde = idx.ponto_equilibrio

                idx.sustentavel_sust_ap = idx.qtde - idx.ponto_equilibrio
           
                if (idx.qtde && idx.ponto_equilibrio) {
                    idx.sustentavel_apxn = (( idx.qtde / idx.ponto_equilibrio) * 100).toFixed(1) + '%'
                }else{
                    idx.sustentavel_apxn = '0%'; 
                }
                return idx;
            });

            let dadosAp = dadosSustentavel.map(function(tmp) {

                tmp.ap_qtde = tmp.qtde
 
                return tmp;
            })
            
            $scope.dadosList  = dadosAp
            
            $("#gridContainer").dxDataGrid({
                
                dataSource: $scope.dadosList
            
            })

            const ultimaEntrada = $scope.dadosList[$scope.dadosList.length - 1]
           
            const novoMes = adicionarUmMes(ultimaEntrada.mes)

            let somaQtde = 0

            for (const objeto of $scope.dadosList) {
                if (objeto.qtde !== undefined) {
                    somaQtde += objeto.qtde
                }
            }

            const media = Math.ceil(somaQtde / $scope.dadosList.length)
           
            const novoObjeto = {
                mes: novoMes,
                qtde: media,
                ponto_equilibrio: null,
                contrato: null,
                nome_patio: null,
                contrato_con_ap: null,
                contrato_apxn: null,
                sustentavel_qtde: null,
                sustentavel_sust_ap: null,
                sustentavel_apxn: null,
                ap_qtde: media,
                ap_ap_qtde: null,
                ganho_repete: null,
                ap_digita: null,
                ganho_multiplica: null,
                ganho_digita: null
            };

           
            const novoArray = [ novoObjeto]

            $scope.dadosListProjecao = novoArray
            
            $("#gridContainerProjecao").dxDataGrid({
                
                dataSource: $scope.dadosListProjecao
            
            })      

            if (!response.data.length > 0) {
               
                myFunctions.showAlert('Filtro não encontrou registros! Revise parâmetros da pesquisa!')
                
                return
            } else {
                
                $("#gridContainer").dxDataGrid("instance").refresh();
            
                $("#gridContainerProjecao").dxDataGrid("instance").refresh();

                atualizaGrid()      
                
                atualizaGridProjecao() 
            }

        }else{

            myFunctions.showAlert('Existem campos sem preencher! Verifique!')
        }
    }



    function adicionarUmMes(data) {
        const [mes, ano] = data.split('/');
        const dataAtual = new Date(parseInt(ano), parseInt(mes) - 1); // Subtrai 1 do mês, pois os meses em JavaScript começam em 0
        dataAtual.setMonth(dataAtual.getMonth() + 1);
        const novoMes = (dataAtual.getMonth() + 1).toString().padStart(2, '0');
        const novoAno = dataAtual.getFullYear().toString();
        return `${novoMes}/${novoAno}`;
    }
    



    $scope.gerarPDF = function () {

        if ($scope.dadosList.length !== 0){

        var getAll = $("#gridContainer").dxDataGrid('instance');

        getAll.getDataSource().store().load().done((res)=>{
            $scope.dadosPDF = res
        })
        
        let dataAtual = new Date().toISOString().slice(0, 10);

        $scope.patioSelecionado =  $scope.patios.find(objeto => objeto.id === $scope.dados.patio);

        var doc = new jsPDF();
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = "{total_pages_count_string}";
        // doc.setFontSize(10);

        doc.autoTable({
            columnStyles: {
                patio: { halign: "left" },
            },

            body: $scope.dadosPDF,
            columns: [
                { header: "Mês", dataKey: "mes" },
                { header: "Apreensoes", dataKey: "ap_qtde" },
                { header: "Contrato", dataKey: "contrato" },
                { header: "% APxN", dataKey: "contrato_apxn" },
                { header: "Qtde-AP", dataKey: "contrato_con_ap" },
                { header: "Ponto Equilibrio", dataKey: "sustentavel_qtde" },
                { header: "% Atingiu", dataKey: "sustentavel_apxn" },
                { header: "Qtde Sust.", dataKey: "sustentavel_sust_ap" },

            ],
            
            bodyStyles: {
                margin: 10,
                fontSize: 8,
            },

            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18);
                doc.setTextColor(40);
                doc.setFontStyle("normal");

                doc.text( 'Comercial apreensoes, periodo: ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + 
                $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 5, 18)
                doc.text("Patio: " + $scope.patioSelecionado.nome, data.settings.margin.left + 5, 25)


                // Footer
                var str = "Página " + doc.internal.getNumberOfPages();
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
                str =  str + "                                                         Data " +
                    dataAtual.substr(8, 2) +
                    "/" +
                    dataAtual.substr(5, 2) +
                    "/" +
                    dataAtual.substr(0, 4) + 
                    "                  Usuário " +
                    localStorage.getItem("nome_usuario");
                
                var totalpaginas = totalPagesExp;
                }

                doc.setFontSize(10);

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize;
                var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();

                doc.text(
                    str + "                        ",
                    data.settings.margin.left,
                    pageHeight - 10
                ); 

            },
            margin: { top: 30 },
        });
        
        if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
        }

        doc.save("Relatorio-Comercial-Apreensoes.pdf")

        $scope.gerarPDFProjecao()
        
    }else{

        myFunctions.showAlert('Sem dados para gerar PDF! Verifique!')

        }

    }




    $scope.gerarPDFProjecao = function () {

        var getAll = $("#gridContainerProjecao").dxDataGrid('instance');

        getAll.getDataSource().store().load().done((res)=>{
            $scope.dadosPDF = res
        })
        
        let dataAtual = new Date().toISOString().slice(0, 10);

        $scope.patioSelecionado =  $scope.patios.find(objeto => objeto.id === $scope.dados.patio);
        
        let dadosTmp = $scope.dadosPDF.map((el) => ({
            ...el,
        ganho_multiplica: new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(el.ganho_multiplica || 0)
        
        }))

        var doc = new jsPDF();
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = "{total_pages_count_string}";
    
        doc.autoTable({
            columnStyles: {
                patio: { halign: "left" },
            },

            body: dadosTmp,
            columns: [
                { header: "Mês", dataKey: "mes" },
                { header: "Apr. Media", dataKey: "ap_qtde" },
                { header: "P. Equil. Qtde", dataKey: "sustentavel_qtde" },
                { header: "% Atingiu", dataKey: "sustentavel_apxn" },
                { header: "% Cresc.", dataKey: "ap_digita" },
                { header: "Qtde", dataKey: "ap_qtde" },
                { header: "Resultado", dataKey: "ap_ap_qtde" },
                { header: "Valor R$", dataKey: "ganho_digita" },
                { header: "Cresc.", dataKey: "ganho_repete" },
                { header: "Resultado", dataKey: "ganho_multiplica" }

            ],
            
            bodyStyles: {
                margin: 10,
                fontSize: 8,
            },

            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18);
                doc.setTextColor(40);
                doc.setFontStyle("normal");

                doc.text( 'Comercial projecao, periodo: ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + 
                $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 5, 18)
                doc.text("Patio: " + $scope.patioSelecionado.nome, data.settings.margin.left + 5, 25)


                // Footer
                var str = "Página " + doc.internal.getNumberOfPages();
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
                str =  str + "                                                         Data " +
                    dataAtual.substr(8, 2) +
                    "/" +
                    dataAtual.substr(5, 2) +
                    "/" +
                    dataAtual.substr(0, 4) + 
                    "                  Usuário " +
                    localStorage.getItem("nome_usuario");
                
                var totalpaginas = totalPagesExp;
                }

                doc.setFontSize(10);

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize;
                var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();

                doc.text(
                    str + "                        ",
                    data.settings.margin.left,
                    pageHeight - 10
                ); 

            },
            margin: { top: 30 },
        });
        
        if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
        }

        doc.save("Relatorio-Comercial-Projecao.pdf")

    }


});