'use strict';
angular.module('app')
.controller("contasNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions) {  

  $scope.dados = {
     decricao:'',
     tipo: '',
     cnpj:'',
     banco: '',
     codigo_banco: '',
     agencia: '',
     conta:'',
     tx_boleto: '',
     titular: '',     
     gerente:'',
     telefone:'',
     email: ''
  }

  $scope.isDisabled = false

  $scope.gravar = function() {        

    $http.post(urlServidor.urlServidorChatAdmin+'/contas/cadastrar', $scope.dados ).then( function(response){

        if (response.data.code){
            
          myFunctions.showAlert('Erro na gravação!')

        }else{

          if ( response.data == '11000' )                 
          {
            myFunctions.showAlert('Conta já existe!')
          }else
          {                    

            myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
            $scope.isDisabled = true ;            

          }  
                
        }; 

    }); 

  }; 


  
    
});