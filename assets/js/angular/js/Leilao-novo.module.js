'use strict';
angular.module('app').controller("leilaoNovoCtrl", function ($scope, $http, urlServidor, myFunctions,viaCep) {





  $scope.optionsOrgao = ['DETRAN' , 'DER' , 'PRF' , 'PREFEITURA'],




  $scope.isDisabled = false



  $scope.dados = {
    descricao: '',
    data: new Date(),
    lote_inicial: '',
    id_patio: '',

    patio_cep:null,
    patio_endereco:null,
    patio_bairro:null,
    patio_cidade:null,
    patio_uf:null,


    devolucao_cep:null,
    devolucao_endereco:null,
    devolucao_bairro:null,
    devolucao_cidade:null,
    devolucao_uf:null,

    orgao:'',
    subtitulo1_edital: null,
    subtitulo2_edital: null,
    texto_edital: null,
    titulo_edital: null

  }

  $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') })
    .then(({ data }) => $scope.patios = data.filter((item) =>  item.status == !0 ))

		$scope.buscaCep = (cep) => {
			if (cep.length == 9) {

				viaCep.get(cep).then( (response) => {
          $scope.dados.patio_endereco = response.logradouro.toUpperCase()
					$scope.dados.patio_bairro = response.bairro.toUpperCase()
					$scope.dados.patio_cidade = response.localidade.toUpperCase()
					$scope.dados.patio_uf = response.uf
				})
			}
		}


    $scope.buscaCepEndereco = (cep) => {
			if (cep.length == 9) {

				viaCep.get(cep).then( (response) => {
          $scope.dados.devolucao_endereco = response.logradouro.toUpperCase()
					$scope.dados.devolucao_bairro = response.bairro.toUpperCase()
					$scope.dados.devolucao_cidade = response.localidade.toUpperCase()
					$scope.dados.devolucao_uf = response.uf
				})
			}
		}




  $scope.gravar = async () => {
    try {
    await $http.post(`${urlServidor.urlServidorChatAdmin}/leiloes/cadastrar`, $scope.dados)
      myFunctions.showAlert('Cadastro executado com sucesso!');
      $scope.isDisabled = true;
    } catch (error) {
      myFunctions.showAlert('Erro na gravação!')

    }
  };
  $scope.paginaAnterior = () => window.close()


});
