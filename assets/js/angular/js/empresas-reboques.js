app.controller('empresasReboquesCtrl', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.novo = function (ev, id) {
        $location.path('empresas-reboques-novo');
    };
    $scope.limpar = function (ev) {
        $scope.dados.nome = '';
        $scope.dados.contato = '';
        $scope.dados.ativo = '';
        $scope.dados.orderby = ''
        $scope.dados.cpf_cnpj = ''
    };
    $scope.pesquisar = function (ev) {

        $scope.dadosList = []

        $http.put(urlServidor.urlServidorChatAdmin + '/empresas_reboques/listar', $scope.dados).then(function (response) {

            let dados = response.data;

            if (dados.length == 0){
                myFunctions.showAlert('Pesquisa não retornou dados! Verifique!')
                return
            }

            if ($scope.dados.ordenacao == 'asc') {

                dados.sort(function (a, b) {
                    if ($scope.dados.orderby == 2) {
                        if (a.contato > b.contato) {
                            return 1;
                        }
                        if (a.contato < b.contato) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    } else {
                        if (a.nome > b.nome) {
                            return 1;
                        }
                        if (a.nome < b.nome) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    }

                });
            } else {

                dados.sort(function (a, b) {
                    if ($scope.dados.orderby == 2) {
                        if (a.contato < b.contato) {
                            return 1;
                        }
                        if (a.contato > b.contato) {
                            return -1;
                        }

                        return 0;
                    } else {
                        if (a.nome < b.nome) {
                            return 1;
                        }
                        if (a.nome > b.nome) {
                            return -1;
                        }

                        return 0;
                    }
                });
            }

            $scope.dadosList = dados;
        });
    };
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("empresas-reboques.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({orientation: "landscape"});
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: {
                nome: {halign: 'left'},
                econtato: {halign: 'left'},
                },
            body: $scope.dadosList,
            columns: [
                {header: 'Nome', dataKey: 'nome'},
                {header: 'Contato', dataKey: 'contato'},
                {header: 'CPF/CNPJ', dataKey: 'cpf_cnpj'},
                {header: 'E-mail', dataKey: 'email'},
                {header: 'Telefone', dataKey: 'telefone'},
                {header: 'Ativo', dataKey: 'desc_ativo'}
                 
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 8,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

               // doc.text('Relátorio de funcionários')

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: {top: 30},
        });

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        } 
       
        doc.save('empresas-reboques.pdf')
    }
    function ordemDecrescente(a, b) {
        if ($scope.dados.orderby == '1') {
            return a.nome < b.nome;
        } else if ($scope.dados.orderby == '2') {
            return a.contato < b.contato;
        } else {
            return a.nome < b.nome;
        }
    }
    ;
    function ordemCrescente(a, b) {
        if ($scope.dados.orderby == '1') {
            return a.nome > b.nome;
        } else if ($scope.dados.orderby == '2') {
            return a.contato > b.contato;
        } else {
            return a.nome > b.nome;
        }

    }
    ;
    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        $location.path('empresas-reboques-editar').search({ dados });
    };
});