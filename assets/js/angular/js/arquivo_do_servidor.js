/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


apiRoutes.get('/relatorio-operadores', function (req, res) {
    res.json(null);
});
apiRoutes.get('/relatorio-filtros-veiculo', function (req, res) {
    res.json([
        {label: "PÁTIO", field: "id_patio", tabela: "ncv", operador: "list"},
        {label: "DATA LIBERAÇÃO", field: "data", tabela: "ncv_liberacao", operador: "date"},
        {label: "DATA APREENÇÃO", field: "data", tabela: "ncv_apreensao", operador: "date"},
        {label: "STATUS", field: "status", tabela: "ncv", operador: "list"},
        {label: "TIPO VEICULO", field: "tipo_veiculo", tabela: "ncv", operador: "number"},
        {label: "KM PERCORRIDO", field: "km_percorrido", tabela: "ncv_guinchos", operador: "number"},
        {label: "MOTORISTA", field: "id_motorista", tabela: "ncv_guinchos", operador: "number"},
        {label: "TIPO DESCONTO", field: "tipo_desconto", tabela: "ncv_liberacao", operador: "list"},
        {label: "FORMA PAGTO", field: "tipo", tabela: "ncv_pagamentos", operador: "list"}
    ]);
});

apiRoutes.post('/relatorio-geral', function (req, res) {
    let mock = [
        {
            filtro:
                    {
                        label: 'PÁTIO',
                        field: 'id_patio',
                        tabela: 'ncv',
                        operador: 'list'
                    },
            operador: {
                label: 'DIFERENTE',
                valeu: '<>'},
            valor01: 53,
            valor02: ''
        }
    ];

    if (Array.isArray(req.body)) {

        let list = req.body;
        let consulta = "";
        let tabelas = list.map(function (item) {
            //console.log(item.filtro);
            return item.filtro.tabela;
        });

        if (tabelas.indexOf("ncv") !== -1) {
            consulta = consulta + "SELECT *";
            consulta = consulta + " FROM";
            consulta = consulta + " ncv as ncv_t1";
        }
        if (tabelas.indexOf("ncv_guinchos") !== -1) {
            consulta = consulta + " SELECT *";
            consulta = consulta + " FROM";
            consulta = consulta + " ncv_guinchos as ncv_guinchos_t2";
            if (tabelas.indexOf("ncv") !== -1) {
                consulta = consulta + " LEFT JOIN ncv_guinchos_t2";
                consulta = consulta + " ON ncv_t1.id = ncv_guinchos_t2.ncv";
            }
        }
        if (tabelas.indexOf("ncv_liberacao") !== -1) {
            consulta = consulta + " SELECT *, ncv_liberacao_t3.`data` as data_liberacao";
            consulta = consulta + " FROM";
            consulta = consulta + " ncv_liberacao as ncv_liberacao_t3";
            if (tabelas.indexOf("ncv") !== -1) {
                consulta = consulta + " LEFT JOIN ncv_liberacao_t3";
                consulta = consulta + " ON ncv_t1.id = ncv_liberacao_t3.ncv";
            }
        }
        if (tabelas.indexOf("ncv_apreensao") !== -1) {
            consulta = consulta + " SELECT *, ncv_apreensao_t4.`data` as data_apreensao";
            consulta = consulta + " FROM";
            consulta = consulta + " ncv as ncv_apreensao";
            if (tabelas.indexOf("ncv") !== -1) {
                consulta = consulta + " LEFT JOIN ncv_apreensao_t4";
                consulta = consulta + " ON ncv_t1.id = ncv_apreensao_t4.ncv";
            }
        }

        consulta = consulta + " WHERE";

        req.body.forEach(function (item) {
            if (item.filtro.field === 'id_patio' && item.filtro.valeu !== '') {
                consulta = consulta + " ncv_t1.id_patio " + item.operador.valeu + " " + item.valor01;
            }
            if (item.filtro.field === 'status' && item.filtro.valeu !== '') {
                consulta = consulta + " ncv_t1.status " + item.operador.valeu + " '" + item.valor01 + "'";
            }
            if (item.filtro.field === 'tipo_veiculo' && item.filtro.valeu !== '') {
                consulta = consulta + " ncv_t1.tipo_veiculo" + item.operador.valeu + " " + item.valor01 + "";
            }
            if (item.filtro.field === 'id_motorista' && item.filtro.valeu !== '') {
                consulta = consulta + " ncv_guinchos_t2.id_motorista " + item.operador.valeu + " " + item.valor01;
            }
            if (item.filtro.field === 'km_percorrido' && item.filtro.valeu !== '') {
                consulta = consulta + " ncv_guinchos_t2.km_percorrido BETWEEN " + item.valor01 + " AND " + item.valor02;
            }
            if (item.filtro.field === 'tipo_desconto' && item.filtro.valeu !== '') {
                consulta = consulta + " ncv_liberacao_t3.tipo_desconto = 'NEGOCIAÇÃO'";
            }
            if (item.filtro.field === 'forma_pagamento' && item.filtro.valeu !== '') {
                consulta = consulta + " ncv_liberacao_t3.forma_pagamento " + item.operador.valeu + " " + item.valor01;
            }
            if (item.filtro.field === 'data' && item.filtro.valeu !== '' && item.filtro.tabela === 'ncv_liberacao') {
                consulta = consulta + " ncv_liberacao_t3.`data` " + item.operador.valeu + " CAST('" + item.valor01.substr(0, 10) + "' AS DATE)";
            }
            if (item.filtro.field === 'data' && item.filtro.valeu !== '' && item.valeu === 'BETWEEN' && item.filtro.tabela === 'ncv_liberacao') {
                consulta = consulta + " (ncv_liberacao_t3.`data` BETWEEN CAST('" + item.valor01.substr(0, 10) + "' AS DATE) AND CAST('" + item.valor02.substr(0, 10) + "' AS DATE))";
            }
            if (item.filtro.field === 'data' && item.filtro.valeu !== '' && item.filtro.tabela === 'ncv_apreensao') {
                consulta = consulta + " ncv_apreensao_t4.`data` " + item.operador.valeu + " CAST('" + item.valor01.substr(0, 10) + "' AS DATE)";
            }
            if (item.filtro.field === 'data' && item.filtro.valeu !== '' && item.valeu === 'BETWEEN' && item.filtro.tabela === 'ncv_apreensao') {
                consulta = consulta + " (ncv_apreensao_t4.`data` BETWEEN CAST('" + item.valor01 + "' AS DATE) AND CAST('" + item.valor02.substr(0, 10) + "' AS DATE))";
            }
            consulta = consulta + " AND ";
        });

        consulta = consulta + " 1=1;";

        console.log(tabelas);
        console.log(consulta);

        req.connection.query(consulta, (err, itens) => {
            if (err) {
                res.json(err);
            } else {
                res.json(itens);
            }
        });
    } else {
        res.json([]);
    }
});
