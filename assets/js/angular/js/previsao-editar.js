'use strict';
angular.module('app')
.controller('previsaoEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor,  $sce, urlImagens, estados, viaCep) {

    var urlParams = $location.search();
    $scope.dados = {
        id: urlParams.dados[0].id,
        id_grupo: urlParams.dados[0].id_grupo,
        id_categoria: urlParams.dados[0].id_categoria,
        competencia: urlParams.dados[0].competencia,
        valor: urlParams.dados[0].valor,
        id_patio: urlParams.dados[0].id_patio
    }
    
    buscaPatios().then(function (result) {
        $scope._patios = result;
    });
    buscaGrupos().then(function (result) {
        $scope._grupos = result;
    });
    buscaCategorias($scope.dados.grupo).then(function (result) {
        $scope._categorias = result;
    });

    $scope.SelecionaCategoria = function(idgrupo){
        buscaCategorias(idgrupo).then( function(result){
          $scope.categorias = result ;
        });  
      };


      $scope.gravar = function () {
        $http.post(urlServidor.urlServidorChatAdmin + '/previsao-orcamentaria/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000')
                {
                    myFunctions.showAlert('Já existe!')
                } else
                {
                    myFunctions.showAlert('Alteração executada com sucesso!');
                }

            }
            ;
        });

      };

    function buscaPatios(idgrupo) {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    };

    function buscaCategorias(idgrupo) {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/categorias/listar', {grupo: idgrupo}).then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    };


    function buscaGrupos() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/grupos/listar').then(function (response) {

                let dados = response.data;
                dados.sort(function (a, b) {
                    if (a.descricao < b.descricao) {
                        return 1;
                    }
                    if (a.descricao > b.descricao) {
                        return -1;
                    }
                    return 0;
                });
                resolve(dados);
            });
        })

    };

});