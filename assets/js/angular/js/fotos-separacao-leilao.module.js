angular
  .module("app")
  .controller(
    "fotosSeparacaoLeilao",
    function ($scope, $location, $http, urlServidor, urlImagens) {
      const Params = $location.search();
      $scope.Params = Params;
      $scope.urlImagensConferente = urlImagens.urlImagensConferente;
      (async () => {
        const { data } = await $http.get(
          urlServidor.urlServidorChatAdmin + "/ncv/buscafotos-conferente",
          {
            params: { ncv: Params.id },
          }
        );
        const response = data.map((item) => ({
          ...item,
          data: moment(item.data).locale("pt").format("LLLL"),
          dataSemformataca: item.data,
          AnoMes: `${new Date(item.data).getMonth() + 1}/${new Date(
            item.data
          ).getFullYear()}`,
        }));
        const groupBy = (x, f) =>
          x.reduce((a, b) => ((a[f(b)] ||= []).push(b), a), {});

        $scope.listaImagensConferente = groupBy(response, (v) => v.AnoMes);
      })();
    }
  );
