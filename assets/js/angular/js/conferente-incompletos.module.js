angular
  .module("app")
  .controller("conferentesIncompletos", function ($scope, $http, urlServidor) {
    const getConferentes = async () => {
      const response = await $http.get(
        `${urlServidor.urlServidorChatAdmin}/relatorios/conferente-pendentes`,
        {
          params: {
            id_usuario: localStorage.getItem("id_usuario"),
          },
        }
      );
      $scope.dadosList = response.data;
    };

    getConferentes();


    $scope.detalhe = (evt,id) =>{
      window.open("/#/ncv-editar?id " + id, "_blank");
    }

   // http://localhost:5500
    $scope.gerarPDF = () => {
      var doc = new jsPDF({ orientation: "landscape" });
      var totalPagesExp = "{total_pages_count_string}";

      doc.setFontSize(10);
      doc.autoTable({
        columnStyles: {
          vencimento: { halign: "left" },
        },

        body: $scope.dadosList,
        columns: [
          { header: "NCV", dataKey: "NCV" },
          { header: "Placa", dataKey: "placa" },
          { header: "data Apreensão", dataKey: "data_apreensao" },
          { header: "Marca/Modelo", dataKey: "marca_modelo" },
          { header: "Cor", dataKey: "cor" },
          { header: "Ano", dataKey: "ano" },
        ],
        bodyStyles: {
          margin: 10,
          fontSize: 08,
        },
        didDrawPage: function (data) {
          // Header
          doc.setFontSize(18);
          doc.setTextColor(40);
          doc.setFontStyle("normal");


          // Footer
          var str = "Pagina " + doc.internal.getNumberOfPages();
          // Total page number plugin only available in jspdf v1.0+
          if (typeof doc.putTotalPages === "function") {
            str = str + " de " + totalPagesExp;
            var totalpaginas = totalPagesExp;
          }

          doc.setFontSize(10);

          // jsPDF 1.4+ uses getWidth, <1.4 uses .width
          var pageSize = doc.internal.pageSize;
          var pageHeight = pageSize.height
            ? pageSize.height
            : pageSize.getHeight();

          doc.text(
            str + "                        ",
            data.settings.margin.left,
            pageHeight - 10
          );
        },
        margin: { top: 30 },
      });
      if (typeof doc.putTotalPages === "function") {
        doc.putTotalPages(totalPagesExp);
      }

      doc.save("relatorio-conferente-incompletos.pdf");
    };
  });
