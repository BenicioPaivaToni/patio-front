'use strict';
angular.module('app')
.controller("tiposVeiculoNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions) {  

  $scope.dados = {
     decricao:'',
     tipo: '' ,
     idempresa: localStorage.getItem('id_empresa')
  }

  $scope.isDisabled = false

  $scope.gravar = function() {        

    $http.post(urlServidor.urlServidorChatAdmin+'/tipos_veiculo/cadastrar', $scope.dados ).then( function(response){

        if (response.data.code){
            
          myFunctions.showAlert('Erro na gravação!')

        }else{

          if ( response.data == '11000' )                 
          {
            myFunctions.showAlert('Tipo de Veiculo já existe!')
          }else
          {                    

            myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
            $scope.isDisabled = true ;            

          }  
                
        }; 

    }); 

  }; 


  
    
});