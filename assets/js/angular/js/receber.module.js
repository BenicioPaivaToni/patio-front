

angular.module('app').controller('receberCtrl', function ($scope, $location, $http, urlServidor, myFunctions, $filter, myFunctions) {


    const buscaPatiosGrupo = async () => {
        const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/grupos-patios/listar`,)

        return response.data
    }
    buscaPatiosGrupo().then((Response) => $scope.patiosGrupo = Response)

    $scope.dados = {
        dataInicio: null,

        dataFinal: null,

        data_venda_inicio: null,
        data_venda_final: null,


    };

    $scope.novo = () => { $location.path('receber-novo'); };

    $scope.limpar = () => {
        $scope.dados = {
            dataInicio: null,
            dataFinal: null,
            data_venda_inicio: null,
            data_venda_final: null ,
        };
        };

    $scope.mesAtual = () => {
        let hoje = new Date()

        $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
        $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);


        $scope.dados.data_venda_inicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
        $scope.dados.data_venda_final = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);



    };


    const listarPatios = async () => {
        const response = await $http.put(`${urlServidor.urlServidorChatAdmin}/patios/listar-patiosusuario`,
            { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') })

        const {data} = response




       return data.filter((item)=> {return  item.status ==! 0})

    }

    const buscaContas = async () => {

        const response = await $http.put(urlServidor.urlServidorChatAdmin + '/contas/listar')
        return response.data;

    }
    buscaContas().then(Result => $scope.contas = Result)

    listarPatios().then(result => {$scope.patios = result
    })






    function somardadosPagos(dados) {

        return new Promise((resolve, reject) => {

            var total = dados.reduce(function (prevVal, elem) {
                return (elem.status !== 'PAGO') ? prevVal : prevVal + elem.valor_semformatacao;
            }, 0);
            resolve(total)

        });
    }



    function somardadosAberto(dados) {

        return new Promise((resolve, reject) => {

            var total = dados.reduce(function (prevVal, elem) {
                return (elem.status !== 'ABERTO') ? prevVal : prevVal + elem.valor_semformatacao;
            }, 0);
            resolve(total)

        });
    }



    function CriaSubTotal(lancamentos) {
        return new Promise((resolve, reject) => {

            var dados = lancamentos;
            var total_patio = 0;
            var total_geral = 0;
            var patio_atual = lancamentos[0].patio;

            dados.forEach(function (elemento, idx) {

                if (dados[idx].patio !== patio_atual) {
                    const patio = `<div style="width:150px"><h4 style="font-size:14px;color:blue;padding-left:10px,padding-rigth:10px">TOTAL PÁTIO</h4></div>`

                    const Valor = `<div style="width:100px"><h4 style="font-size:14px;color:blue"> ${myFunctions.numberToReal(total_patio)} </h4></div>`



                    const Styleinformations = { patio: patio, vencimento: Valor, razao_nome: '', documento: '', valor: '', desc_tiporecebimento: '', status: '', editar: '', baixar: '', cancelar: '' }

                    dados.splice(idx, 0, Styleinformations)

                    total_patio = 0;
                    idx += 1;
                    patio_atual = dados[idx].patio;

                } else {

                    total_patio += dados[idx].valor_semformatacao
                    total_geral += dados[idx].valor_semformatacao

                }


            })

            resolve(dados)

        })

    }

    $scope.pesquisar = async (evt, dados) => {
      try {
        if (dados.dataInicio === null && dados.data_venda_inicio === null)
          throw new Error(
            "Ao menos um filtro por data é obrigatório, verifique"
          );

        const { data } = await $http.put(
          urlServidor.urlServidorChatAdmin + "/receber/listar",
          $scope.dados
        );

        if (data.length === 0)
          throw new Error("Filtro não encontrou registros, verifique!");

        CriaSubTotal(data).then(() => {
          somardadosPagos(data).then((result) => {
            $scope.total_recebidos = result;

            somardadosAberto(data).then((result) => {
              $scope.total_receber = result;
            });
          });

          $scope.dadosList = data;
        });
      } catch ({ message }) {
        console.error(message)
        myFunctions.showAlert(message);
      }
    };





    $scope.detalhe = function (ev, id) {

        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })



        window.open('/#/receber-editar?rowId ' + id, '_blank');

    };


    $scope.baixar = function (ev, rowId, id) {
        let dados = $scope.dadosList.filter((obj) => {
            return obj.id == id
        })
        if (dados[0].status == 'PAGO') {
            myFunctions.showAlert('Documento já esta baixado!')
        } else if (dados[0].status == 'CANCELADO') {
            myFunctions.showAlert('Documento esta cancelado!')
        } else {
            window.open('/#/receber-baixar?rowId ' + id, '_blank');

        }
    };



    $scope.gerarPlanilha = () => {

        alasql('SELECT * INTO XLSX("relatorio-contas-a-receber.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    };


    $scope.gerarPdf = function () {
        $scope.dadosList


        const PDFInformações = $scope.dadosList.map(index => ({
            patio: index.patio.replace(/(<([^>]+)>)/ig, " "),
            vencimento: index.vencimento.replace(/(<([^>]+)>)/ig, ""),
            documento: index.documento,
            desc_categoria: index.desc_categoria,
            valor: index.valor,
            status: index.status,
            competencia: index.competencia,
            nome: index.nome,
            desc_tiporecebimento: index.desc_tiporecebimento,
            conta: index.conta,

        }))




        var doc = new jsPDF({ orientation: "landscape" });

        var totalPagesExp = '{total_pages_count_string}'


        doc.setFontSize(10);


        doc.autoTable({

            columnStyles: {
                vencimento: { halign: 'left' },
            },

            body: PDFInformações,
            columns: [


                { header: 'Patio', dataKey: 'patio' },

                { header: 'Vencimento', dataKey: 'vencimento' },

                { header: 'Razão', dataKey: 'nome' },

                { header: 'Valor', dataKey: 'valor' },

                { header: 'Documento', dataKey: 'documento' },


                { header: 'Status', dataKey: 'status' },

                { header: 'Forma de Pag.', dataKey: 'desc_tiporecebimento' },

            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },

            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')


                /* Guardando Em constantes para ajudar na oraganização*/
                const dataInicioStr = $scope.dados.dataInicio.toLocaleDateString('pt-BR')
                const dataFinalStr = $scope.dados.dataFinal.toLocaleDateString('pt-BR')

                /* Por usar interpolação de Strings, precisamos criar uma variavel de espaço*/
                const WillGiveSpace = '            '



                doc.text(`Contas a receber com vencimento no periodo de ${dataInicioStr} a ${dataFinalStr}`, data.settings.margin.left + 15, 22);




                // Footer
                var str = `Pagina ${doc.internal.getNumberOfPages()}`

                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') { str = `${str} de ${totalPagesExp}` }

                const totalRecebido = $scope.total_recebidos.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
                const totalReceber = $scope.total_receber.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })

                var strTotal = `TOTAL A PAGAR ${totalRecebido} ${WillGiveSpace}  TOTAL PAGO ${totalReceber}`

                doc.setFontSize(10)





                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()


                doc.text(`${str} ${WillGiveSpace} ${strTotal} `, data.settings.margin.left, pageHeight - 10)



            },

            margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('relatorio-contas-a-receber.pdf');
    }







    $scope.cancelar = function (ev, rowId, id) {
        let dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        if (dados[0].status == 'PAGO' || dados[0].status == 'CANCELADO') {
            myFunctions.showAlert('Somente documento com status ABERTO pode ser cancelado!')
        } else {
            $location.path('receber-cancelar').search({ dados });
        }
    };
});
