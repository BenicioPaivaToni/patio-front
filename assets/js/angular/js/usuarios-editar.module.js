angular.module('app').controller("usuariosEditarCtrl", function($scope, myFunctions,$location, $http, urlServidor) { 

    var urlParams = $location.search();
    console.log(urlParams.dados[0])
    $scope.usuario = {
        id: urlParams.dados[0].id,
        nome: urlParams.dados[0].nome,
        login: urlParams.dados[0].login,
        apelido: urlParams.dados[0].apelido,
        email: urlParams.dados[0].email,
        nascimento: urlParams.dados[0].nascimento,
        cargo: urlParams.dados[0].cargo,
        senha: urlParams.dados[0].senha,
        tipo: urlParams.dados[0].tipo,
        ativo: urlParams.dados[0].ativo,
        apagar_docs_fotos: urlParams.dados[0].exclui_fotos_docs,
        ajusta_financeiro: urlParams.dados[0].ajusta_financeiro,
        ultimo_acesso: urlParams.dados[0].ultimo_acesso,
        observacao: urlParams.dados[0].observacao,
        tiponet: urlParams.dados[0].tiponet,
        tipo_situacao: urlParams.dados[0].tipo_situacao = urlParams.dados[0].tipo_situacao === 'SIM' ? 1 : 0,
        alerta_documentos:urlParams.dados[0].alerta_documentos,
        recebe_transferencia:urlParams.dados[0].recebe_transferencia === 'SIM' ? 1 : 0,
        status_ncv: urlParams.dados[0].altera_status_ncv === "SIM" ? 1: 0,
        altera_data_apreensao: urlParams.dados[0].altera_data_apreensao === "SIM" ? 1 : 0,
        ajuste_logistica: urlParams.dados[0].ajuste_logistica === "SIM" ? 1 : 0,
        altera_no_patio: urlParams.dados[0].altera_no_patio === "SIM" ? 1 : 0,
        altera_localizado_leilao: urlParams.dados[0].altera_localizado_leilao === "SIM" ? 1 : 0
    }

    $http.get(urlServidor.urlServidorChatAdmin + '/usuarios/opcoes-menu').then(function (response) {
        $scope.listas = response.data;
    });
    $http.get(urlServidor.urlServidorChatAdmin + '/usuarios/listar-itensacesso/', { params: { idUsuario: $scope.usuario.id, idEmpresa: localStorage.getItem('id_empresa') } }).then(function (response) {
        $scope.listasAcesso = response.data;
    });
    buscaPatios().then(function (result) {
        $scope.patios = result;
    });
    buscaPatiosUsuario($scope.usuario.id, localStorage.getItem('id_empresa')).then(function (result) {
        $scope.patiosUsuario = result;
    });
    $scope.incluiPatio = function (id) {

        let dados = {
            idPatio: id,
            idUsuario: $scope.usuario.id,
            idEmpresa: localStorage.getItem('id_empresa')
        };
        $http.post(urlServidor.urlServidorChatAdmin + '/patios/grava-usuariopatio', dados).then(function (response) {

            $scope.patiosUsuario = response.data;
            /*
             buscaPatiosUsuario(dados.idUsuario,dados.idEmpresa).then( function(result){
             $scope.patiosUsuario = result ;
             },
             function result(){
             console.log('falha ao obter dados de patios de usuario');              
             });
             */

        });
    };
    $scope.removePatio = function (id) {

        let dados = {
            idPatio: id,
            idUsuario: $scope.usuario.id,
            idEmpresa: localStorage.getItem('id_empresa')
        };
        $http.post(urlServidor.urlServidorChatAdmin + '/patios/remove-usuariopatio', dados).then(function (response) {

            $scope.patiosUsuario = response.data;
            /*
             buscaPatiosUsuario(dados.idUsuario,dados.idEmpresa).then( function(result){
             $scope.patiosUsuario = result ;
             },
             function result(){
             console.log('falha ao obter dados de patios de usuario');              
             });
             */
        });
    };
    $scope.incluiAcesso = function (id) {

        let dados = {
            idItem: id,
            idUsuario: $scope.usuario.id,
            idEmpresa: localStorage.getItem('id_empresa')
        };
        $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/grava-acesso', dados).then(function (response) {

            $scope.listasAcesso = response.data;
        });
    };
    $scope.removeAcesso = function (id) {

        let dados = {
            idItem: id,
            idUsuario: $scope.usuario.id,
            idEmpresa: localStorage.getItem('id_empresa')
        };
        $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/remove-acesso', dados).then(function (response) {

            $scope.listasAcesso = response.data;
        });
    };
    $scope.filtraOpcoesMenu = function () {

        let filtro = $scope.permissoes.menu;
    }


    $scope.gravar = function (usuario) {
        
        $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/alterar', usuario).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    myFunctions.showAlert('Usuário já existe!')
                } else {
                    myFunctions.showAlert('Alteração executada com sucesso!');
                }

            }
            ;
        });


    };
    function buscaPatios() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar').then(function (response) {

                resolve(response.data);
            });
        });
    }

    function buscaPatiosUsuario(idusuario, idempresa) {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: idusuario, idEmpresa: idempresa }).then(function (response) {

                resolve(response.data);
            });
        });
    }










})