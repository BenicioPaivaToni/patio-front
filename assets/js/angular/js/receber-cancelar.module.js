angular.module('app').controller('receberCancelarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep) {

    var urlParams = $location.search();
    $scope.dados = {
        id: urlParams.dados[0].id,
        id_cliente: urlParams.dados[0].id_cliente,
        valor: urlParams.dados[0].valor,
        documento: urlParams.dados[0].documento,
        vencimento: urlParams.dados[0].vencimento,
        forma_recebimento: urlParams.dados[0].forma_recebimento,
        categoria: urlParams.dados[0].categoria,
        comentario: urlParams.dados[0].comentario,
        datacancelamento: new Date()
    }

    buscaClientes().then(function (result) {
        $scope.clientes = result;
    });
    buscaCategorias().then(function (result) {
        $scope.categorias = result;
    });
    $scope.gravar = function () {

        myFunctions.showConfirma('Confirma Cancelamento ?').then(function (response) {

            $http.post(urlServidor.urlServidorChatAdmin + '/receber/cancela', $scope.dados).then(function (response) {
                if (response.data.code) {
                    myFunctions.showAlert('Erro na gravação!');
                } else {
                    myFunctions.showAlert('Cancelamento executado com sucesso!')
                    $scope.isDisabled = true;
                }
                ;
            });
        },
            function result() {


            });
    };
    function buscaClientes() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/clientes/listar').then(function (response) {

                let dados = response.data;
                dados.sort(function (a, b) {
                    if (a.nome < b.nome) {
                        return 1;
                    }
                    if (a.nome > b.nome) {
                        return -1;
                    }
                    return 0;
                });
                resolve(dados);
            });
        })

    }

    function buscaCategorias() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/categorias/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }


});