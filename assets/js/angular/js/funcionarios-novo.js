'use strict';
angular.module('app')
.controller("funcionariosNovoCtrl", function($location,$scope,$http,urlServidor, estados, Upload, S3UploadService,myFunctions,viaCep,$sce, urlImagens,buckets) {  

  $scope.urlImagens = urlImagens.urlDocumentosFuncionarios;


  $scope.dados = {
   
    nome:'',  
    apelido: '' ,
    dt_nascimento: null,
    nome_pai: '',
    nome_mae: '',
    email: '' ,
    celular: '' ,
    pis: '',
    foto: '',
    cep: '',
    endereco: '',
    bairro:'' ,
    cidade: '' ,
    uf: '',   
    cpf: '',
    rg: '',
    emissao_rg: null,
    habilitado: '',
    cnh: '' ,
    cnhVal: null ,
    cnhCat: '',
    observacao: '',
    empresa:'',
    id_patio: '',
    departamento: '' ,  
    tipo_contratacao: '',
    salario: '',
    data_admissao: null,
    data_demissao: null,
    data_ferias: null,
    data_experiencia: null,
    ativo: 'SIM'
    
 }

 $scope.isDisabled = false;
 var bucket = null;

 $scope.trustSrc = function(src) {
   return $sce.trustAsResourceUrl(src);
 }  


 $scope.estados = estados.uf ;

 
 $http.get(urlServidor.urlServidorChatAdmin+'/autoridades/vercodigo').then( function(response){

   AWS.config.update({ accessKeyId: response.data.ac, secretAccessKey: response.data.sc });
   AWS.config.region = response.data.regiao;

 }).finally( function(){

   bucket = new AWS.S3();
 })

 $scope.atualizarDatas = function () {

  var data = $scope.dados.data_admissao;
  $scope.dados.data_ferias = new Date(data.getTime() + (365 * 24 * 60 * 60 * 1000));
  $scope.dados.data_experiencia = new Date(data.getTime() + (45 * 24 * 60 * 60 * 1000));
}


buscaEmpresas().then(function (result) {
  $scope.empresas = result

})


buscaPatios().then( function(result){
  $scope.patios = result ;
});  

 $scope.buscaCep = (cep) => {

   if(cep.length == 9){

     viaCep.get(cep).then(function(response){
     
       $scope.dados.endereco = response.logradouro.toUpperCase() ;
       $scope.dados.bairro = response.bairro.toUpperCase() ;
       $scope.dados.cidade = response.localidade.toUpperCase() ;
       $scope.dados.uf = response.uf.toUpperCase() ;				 

     });

   }	

 }  

 $scope.urlFotos = urlImagens.urlFotos;

 function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
}
 
 $scope.uploadFoto = (file) => {

  if( file !== null ){    

      if (file.type !== null && file.type !== undefined) {

          if (file.type.substr(0, 5) == 'image') {

              let nomefoto =  makeid() + '.' + file.type.substr(6, 4);
//			S3UploadService.Upload(file, 'imagens-reidospatios/'+_id , nomefoto ).then(function (result) {
              S3UploadService.Upload(file, buckets.fotos_funcionarios, nomefoto).then(function (result) {
                  // Mark as success
                  file.Success = true;
                 $scope.dados.foto = '/' + nomefoto;
              }, function (error) {
                  // Mark the error
                  $scope.Error = error;
              }, function (progress) {
                  file.Progress = (progress.loaded / progress.total) * 100
              });
          }
      }
  }
}

 $scope.isDisabled = false

 $scope.gravar = function() {        

   $http.post(urlServidor.urlServidorChatAdmin+'/funcionarios/cadastrar', $scope.dados).then( function(response){

       if (response.data.code){
           
         myFunctions.showAlert('Erro na gravação!')

       }else{

         if ( response.data == '11000' )                 
         {
           myFunctions.showAlert('Funcionário já existe!')
         }else
         {                    

           myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
           $scope.isDisabled = true ;
        
           
                       
           


         }  
               
       }; 

   }); 

 }; 

 function buscaPatios() {
  return new Promise(function (resolve, reject) {
      $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', {idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa')}).then(function (response) {
          let dados = response.data;
          resolve(dados);
      });
  })
}


function buscaEmpresas() {
  return new Promise(function (resolve, reject) {
      $http.put(urlServidor.urlServidorChatAdmin + '/empresas/listar', ).then(function (response) {
           
          let dados = response.data
          resolve(dados)
      })
  })
}
    

});