angular.module('app').controller('receberBaixarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep) {

    const queryString = window.location.hash;

    var _id = queryString.slice(queryString.indexOf("%") + 3);


    $http.get(`${urlServidor.urlServidorChatAdmin}/receber/busca-id`, { params: { id_receber: _id } }).then((Result) => {
        const result = Result.data

        const map = result.map(Item => ({
            id: Item.id,
            id_cliente: Item.id_cliente,
            valor: Item.valor,
            documento: Item.documento,
            vencimento: Item.vencimento,

            tipo_recebimento: Item.tipo_recebimento,
            grupo: Item.grupo,
            categoria: Item.categoria,
            id_patio: Item.id_patio,
            comentario: Item.comentario,

            valorbaixa: Item.valor,
            databaixa: new Date(),

            data_pagto: Item.data_pagto,
            valor_baixa: Item.valor_baixa,
            desconto: Item.desconto,
            acrescimo: Item.acrescimo

        }))

        $scope.dados = map[0]


        const buscaClientesId = async () => {
            const idCliente = $scope.dados.id_cliente
            const response = await $http.put(urlServidor.urlServidorChatAdmin + '/clientes/listar-id', { id: idCliente })
            $scope.clientes = response.data;

        }

        async function buscaCategorias() {
            const response = await $http.put(urlServidor.urlServidorChatAdmin + '/categorias/listar')
            $scope.categorias = response.data;

        }

        async function buscaContas() {
            const response = await $http.put(urlServidor.urlServidorChatAdmin + '/contas/listar')
            $scope.contas = response.data;
        }



        async function buscaTipoRecebimento() {
            const response = await $http.put(`${urlServidor.urlServidorChatAdmin}/tipos-recebimento/listar-tipo`, { tipo: 'RECEBER' })
            $scope.tiposRecebimentos = response.data;

        }


        async function buscaGrupos() {
            const response = await $http.put(`${urlServidor.urlServidorChatAdmin}/grupos/listar`)
            $scope.grupos = response.data;
        }


        setTimeout(() => {
            buscaTipoRecebimento()
            buscaGrupos()
            buscaCategorias()
            buscaContas()
            buscaClientesId()

        }, 1000);






        $scope.gravar = function () {

            let valorbaixa = $scope.dados.valorbaixa.substring(2, $scope.dados.valorbaixa.length);
            valorbaixa = valorbaixa.replace('.', '');
            valorbaixa = valorbaixa.replace(',', '.');
            valorbaixa = Number(valorbaixa);


            let valor = $scope.dados.valor.substring(2, $scope.dados.valor.length);
            valor = valor.replace('.', '');
            valor = valor.replace(',', '.');
            valor = Number(valor);


            if (valorbaixa < valor) {

                myFunctions.showConfirma('Baixa de Conta a Receber', 'Valor da baixa é menor que valor do titulo, lançar como desconto ?').then(function (response) {

                    $scope.dados.valordesconto = (valor - valorbaixa);
                    $http.post(urlServidor.urlServidorChatAdmin + '/receber/baixa-comdesconto', $scope.dados).then(function (response) {
                        if (response.data.code) {
                            myFunctions.showAlert('Erro na gravação!');
                        } else {
                            myFunctions.showAlert('Baixa executada com sucesso!')
                            $scope.isDisabled = true;
                        }
                        ;
                    });
                },
                    function result() {

                        $http.post(urlServidor.urlServidorChatAdmin + '/receber/baixa', $scope.dados).then(function (response) {
                            if (response.data.code) {
                                myFunctions.showAlert('Erro na gravação!')
                            } else {
                                myFunctions.showAlert('Baixa executada com sucesso!')
                                $scope.isDisabled = true;
                            }
                            ;
                        });
                    });
            } else if (valorbaixa > valor) {

                myFunctions.showConfirma('Baixa de Conta a Receber', 'Valor da baixa é maior que valor do titulo, lançar como acréscimo ?').then(function (response) {

                    $scope.dados.valoracrescimo = (valorbaixa - valor);
                    $http.post(urlServidor.urlServidorChatAdmin + '/receber/baixa-comacrescimo', $scope.dados).then(function (response) {
                        if (response.data.code) {
                            myFunctions.showAlert('Erro na gravação!')
                        } else {
                            myFunctions.showAlert('Baixa executada com sucesso!');
                            $scope.isDisabled = true;
                        }
                        ;
                    });
                },
                    function result() {

                        $http.post(urlServidor.urlServidorChatAdmin + '/receber/baixa', $scope.dados).then(function (response) {
                            if (response.data.code) {
                                myFunctions.showAlert('Erro na gravação!')
                            } else {
                                myFunctions.showAlert('Baixa executada com sucesso!');
                                $scope.isDisabled = true;
                            }
                            ;
                        });
                    });
            } else {

                $http.post(urlServidor.urlServidorChatAdmin + '/receber/baixa', $scope.dados).then(function (response) {
                    if (response.data.code) {
                        myFunctions.showAlert('Erro na gravação!')
                    } else {
                        myFunctions.showAlert('Baixa executada com sucesso!');
                        $scope.isDisabled = true;
                    }
                    ;
                });
            }


        };






    })

});