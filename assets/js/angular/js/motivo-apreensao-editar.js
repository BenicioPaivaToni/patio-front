
app.controller('motivoApreensaoEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep) {

    const queryString = window.location.hash;

    var _id =  queryString.slice(queryString.indexOf("%") + 3); 

    $scope.dados = null ;

    buscaDados(_id).then(function (result) {
        $scope.dados = result;
    });
    

    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/ncv/alterar-motivoapreensao', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')
            } else {

                if (response.data == '11000')
                {
                    myFunctions.showAlert('Grupo já existe!')
                } else
                {
                    myFunctions.showAlert('Alteração executada com sucesso!');

              
                }

            }
            ;
        });
    };

    function buscaDados(ncv) {

        return new Promise(function (resolve, reject) {   

            $http.get(urlServidor.urlServidorChatAdmin + '/ncv/lista-motivoapreensao', {id: _id}).then(function (response) {
               
               resolve(response.data[0]);

                })
                
       

            });
            
        

    }

    $scope.fechar = function () {
        window.close();
    }
});