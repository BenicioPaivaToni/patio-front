angular.module('app').controller('GerencialNovoCtrl', function ($location,Upload, S3UploadService,$scope,$http,urlServidor,myFunctions,$mdDialog,estados,viaCep, $sce, urlImagens,$rootScope,buckets) {


    $scope.urlImagemNf = urlImagens.urlImagemNf;
    $scope.trustSrc = function(src) {
          return  $sce.trustAsResourceUrl(src);
      }  
  
    $scope.dados = {
       id_fornecedor:'',
       patio:'',
       valor:'',  
       documento: '' ,
       vencimento: new Date(),
       tipo_recebimento: '',
       categoria:'',
       grupo:'',
       qtde:1,
       comentario:'' ,
       competencia:'',
       checkparcelas:false,
       idempresa: localStorage.getItem('id_empresa')
    }
  
   
    $scope.idpagar = null;
    $scope.listaDocumentos = [] ;
  
    function geraParcelas(item,qtde){
  
          return new Promise((resolve, reject) => {  
  
        var inserts = [] ;
        var dataVenc =  new Date( item.vencimento );
        var competencia =  new Date( item.vencimento );
        var _competencia = item.competencia ;
  
        dataVenc = new Date( dataVenc.setMonth( dataVenc.getMonth() - 1 ) ) ;
  
        competencia = new Date( competencia.setMonth( competencia.getMonth() - 1 ) ) ;
  
        var i;
        for (i = 0; i < item.qtde ; i++) {           
              
                  inserts.push( { id_fornecedor: item.id_fornecedor , vencimento: dataVenc , cadastro: new Date() , 
                                  valor: item.valor , categoria: item.categoria , tipo_recebimento: item.tipo_recebimento ,
                    documento: item.documento+'-'+(i+1)+'/'+item.qtde , comentario: item.comentario , grupo: item.grupo, id_empresa: item.idempresa,
                    competencia: _competencia , id_patio: item.patio } )
          dataVenc = new Date( dataVenc.setMonth( dataVenc.getMonth() + 1 ) ) ;          
          competencia = new Date( competencia.setMonth( competencia.getMonth() + 1 ) ) ;    
  
              };	
  
        setTimeout(() => {
          resolve(inserts) ;  
        }, 1000);
  
          });			
  
    }
    
  
    buscaFornecedores().then( function(result){
       $scope.fornecedores = result ;
    });  
  
    buscaPatios().then( function(result){
      $scope.patios = result ;
    }); 
  
    
    buscaGrupos().then( function(result){
      $scope.grupos = result ;
    });  
  
    buscaTipoRecebimento().then( function(result){
      $scope.tiposRecebimentos = result ;
    });  
  
    $scope.SelecionaCategoria = function(idgrupo){
      buscaCategorias(idgrupo).then( function(result){
        $scope.categorias = result ;
      });  
    }
   
   
   
    $scope.isDisabled = false;
    $scope.gravar = function() {  
      
      if( $scope.dados.checkparcelas && $scope.dados.qtde > 1){
  
     
        geraParcelas( $scope.dados ).then( function(response){
  
          $scope.dados_parcelas = response;
          $scope.parcelas = $scope.dados_parcelas[0];
          $scope.parcelas = {
            vencimento: '',
            valor: '',
            competencia:''
          }
  
                 
               
          
                   
          $mdDialog.show({
            controller: function ($scope, $mdDialog, $mdToast) {
               
  
                $scope.gravarParcelas = function () {
                  $http.post(urlServidor.urlServidorChatAdmin+'/pagar/cadastrar-parcelas', $scope.dados_parcelas ).then( function(response){
                    if (response.data.code){
                        
                      myFunctions.showAlert('Erro na gravação!')
              
                    }else{
              
                      if ( response.data == '11000' )                 
                      {
                        myFunctions.showAlert('Pagar já existe!')
                      }else
                      {   
                        myFunctions.showAlert('Cadastrado com sucesso!');
                        $mdDialog.hide();
                        $scope.isDisabled = true ;
                        $scope.idpagar = response.data.insertId;   
                  }
              
                        }
                          
                        });
  
                }
  
                $scope.cancel = function () {//botão fechar
                    $mdDialog.cancel();
                    $scope.isDisabled = false ;
                };     
                
  
  
            
        },
            templateUrl: 'ajustes-parcelas.html',
            scope:$scope,
            preserveScope: true,
            scopoAnterior: $scope
        }).then(function (answer) {
            let a = answer;
            
        }, function () {
            $scope.statusdialog = 'You cancelled the dialog.';
        });
      
          
          
      
    
        });
  
      } else {
  
        $http.post(urlServidor.urlServidorChatAdmin+'/pagar/cadastrar', $scope.dados ).then( function(response){
  
            if (response.data.code){
                
              myFunctions.showAlert('Erro na gravação!')
  
            }else{
  
              if ( response.data == '11000' )                 
              {
                myFunctions.showAlert('Lançamento já existe!')
              }else
              {                    
  
                myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
                $scope.isDisabled = true ;   
                $scope.idpagar = response.data.insertId;            
  
              }  
                    
            }; 
  
        });
      } 
    
  
    }; 
  
  
   
  
    $scope.apagarDocumento = (dados) => {
      let idimagem = dados.id;
      let confirm = $mdDialog.confirm()
      .title('Documentos!')
      .textContent('Tem certeza que deseja apagar esse documento?')
      .ariaLabel('Lucky day')
      .ok('Apagar')
      .cancel('Cancelar');
      $mdDialog.show(confirm).then(function () {
      
        $http.post(urlServidor.urlServidorChatAdmin + '/pagar/apaga-documento', {id: idimagem}).then(function (response){
            if(response.data.code){
                myFunctions.showAlert('Não foi possível apagar o documento !!');
            }else {
                myFunctions.showAlert('Documento apagado !!');
                $http.get(urlServidor.urlServidorChatAdmin + '/pagar/busca-documentospagar', {params: {id_pagar: $scope.idpagar }}).then(function (response) {
                  $scope.listaDocumentos = response.data;
                });
            }
  
        });    
  
      });
  
    }
  
    $scope.uploadDocumento = (file) => {
  
      let nomefoto = '';
      if(file){
      if (file.type !== null && file.type !== undefined) {
  
          if (file.type.substr(0, 5) == 'image' || file.type == 'application/pdf') {
  
              if (file.type == 'application/pdf') {
                  nomefoto = $scope.idpagar + '/' + makeid() + '.' + file.type.substr(12, 3);
  
              S3UploadService.Upload(file, buckets.documentos_pagar , nomefoto).then(function (result) {
                  // Mark as success
                  file.Success = true;
                 
                  $http.post(urlServidor.urlServidorChatAdmin + '/pagar/incluir-documento',
                          {id_pagar: $scope.idpagar, documento: nomefoto}
                  ).then(function (response) {
  
                          $scope.listaDocumentos.push({documento: nomefoto, id: response.data.insertId, id_pagar: $scope.idpagar});
                    
                  });
  
  
              }, function (error) {
                  // Mark the error
                  $scope.Error = error;
              }, function (progress) {
                  // Write the progress as a percentage
  
                  file.Progress = (progress.loaded / progress.total) * 100
              });
  
              } else {
                  myFunctions.showAlert("Formato inválido!");
              }
              
          }
  
      }
      }
  
    }
  
  function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 7; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
  
    
     
  
    function buscaTipoRecebimento(){	
          return new Promise( function( resolve,reject){
              $http.put(urlServidor.urlServidorChatAdmin+'/tipos-recebimento/listar-tipo',{tipo:'PAGAR'}).then( function(response){
                  let dados = response.data ;
                  resolve(dados) ;
              });
          })
    }	
    
      function buscaCategorias(idgrupo){
          return new Promise( function( resolve,reject){
            $http.put(urlServidor.urlServidorChatAdmin+'/categorias/listar',{ grupo: idgrupo}).then( function(response){
              let dados = response.data ;
              resolve(dados) ;
            });
          })
    }
    
    function buscaPatios(idgrupo){
          return new Promise( function( resolve,reject){
            $http.put(urlServidor.urlServidorChatAdmin+'/patios/listar').then( function(response){
              let dados = response.data ;
              resolve(dados) ;
            });
          })
      }	
   
      
    function buscaFornecedores(){
  
      return new Promise( function( resolve,reject){
  
        $http.put(urlServidor.urlServidorChatAdmin+'/fornecedores/listar').then( function(response){
  
          let dados = response.data ;
          resolve(dados) ;
  
        });
  
      })
  
    }
  
    function buscaGrupos(){
  
      return new Promise( function( resolve,reject){
  
        $http.put(urlServidor.urlServidorChatAdmin+'/grupos/listar').then( function(response){
  
          let dados = response.data ;
          resolve(dados) ;
  
        });
  
      })
  
    }
  
  
      $scope.incluiFornecedor = incluiFornecedor ; 
  
      function incluiFornecedor() {
  
      var _novo = false ;
  
          $mdDialog.show({
  
              controller: function ($scope, $mdDialog, $mdToast) {
  
                  $scope.fornecedor = {
                      razao_nome:'',
                      cpf_cnpj: '' ,
                      endereco: '' ,
                      bairro: '' ,
            cidade: '' ,
            tipo_pessoa: '',
            id_tipo_fornecedor: 0 ,
                      cep: '' ,
                      uf: '' ,
                      email: '' ,
                      telefone_comercial: '',
                      status: '1' ,
                      idempresa: localStorage.getItem('id_empresa')
                  }			
  
              $scope.cancel = function () {
                $mdDialog.cancel();
              };
      
    
  
              $scope.gravaFornecedor = function () {
  
          if( _novo ){
  
              $http.post(urlServidor.urlServidorChatAdmin+'/fornecedores/cadastrar', $scope.fornecedor ).then( function(response){
                $rootScope.idFornecedor = response.data.insertId ;
              }); 				
  
          } else {
  
                myFunctions.showAlert('CNPJ já cadastrado!')
                $rootScope.idFornecedor = $scope.fornecedor.id ;
  
          }  
  
              };
  
              $scope.saveAndClose = function () {
                $scope.gravaFornecedor();
                $rootScope._fornecedor = $scope.fornecedor;
                $mdDialog.hide();
                
              };
  
              $scope.estados = estados.uf ;
  
              $scope.buscaCep = (cep) => {
            
                    if(cep.length == 9){
            
                        viaCep.get(cep).then(function(response){
                        
                            $scope.fornecedor.endereco = response.logradouro.toUpperCase() ;
                            $scope.fornecedor.bairro = response.bairro.toUpperCase() ;
                            $scope.fornecedor.cidade = response.localidade.toUpperCase() ;
                            $scope.fornecedor.uf = response.uf.toUpperCase() ;				 
            
                        });
            
                    }	
            
              }
              
              $scope.buscaCpfCnpj = (cpfCnpj) => {
            
                  if(cpfCnpj.length == 14 || cpfCnpj.length == 18 ){
          
                      $http.get(urlServidor.urlServidorChatAdmin+'/fornecedores/busca-cpfcnpj', { params: { cpf_cnpj: cpfCnpj }}).then( function(response){
                          
                          $scope.motoristas = response.data ;
  
                          if( response.data.length > 0 ){
                              $scope.fornecedor.cep = response.data[0].cep ;
                              $scope.fornecedor.nome = response.data[0].razao_nome ;
                              $scope.fornecedor.endereco = response.data[0].endereco ;
                              $scope.fornecedor.bairro = response.data[0].bairro ;
                              $scope.fornecedor.cidade = response.data[0].cidade ;
                              $scope.fornecedor.uf = response.data[0].uf ;				 
                              $scope.fornecedor.email = response.data[0].email ;
                $scope.fornecedor.telefone = response.data[0].telefone_comercial ;
                $scope.fornecedor.id = response.data[0].id ;                                    
                _novo = false ;
                          } else {
                _novo = true ;
              }
  
                      });						
          
                  }	
          
              } 
          
  
            },
            templateUrl: 'cadastra-fornecedor.html',
            preserveScope: true
          }).then(function(answer) {
        let a = answer ;	
        buscaFornecedores().then( function(response){
            $scope.$resolve.$scope.fornecedores = response ;
            $scope.$resolve.$scope.dados.id_fornecedor =  $rootScope.idFornecedor ;
        });
          }, function() {
              $scope.statusdialog = 'You cancelled the dialog.';
          });
  
      };			
  
  
  
  



})