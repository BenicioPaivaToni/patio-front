angular.module('app').controller('GerencialCtrl', function (myFunctions, $scope, $http, urlServidor, myFunctions) {

    const ApiCalls = {

        buscaPatiosGrupo: async () => {
            const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/grupos-patios/listar`,)
            $scope.patiosGrupo = response.data
        },
        buscaPatios: async () => {
            const response = await $http.put(`${urlServidor.urlServidorChatAdmin}/patios/listar-patiosusuario`, { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') })
            return $scope.patios = response.data

        }
    }

    // Init Chamadas 
    ApiCalls.buscaPatiosGrupo()
    ApiCalls.buscaPatios()


    //Actions 






    $scope.dados = {
        dataInicio: new Date(),
        dataFinal: new Date(),
        liberacaoInicio: new Date(),
        liberacaoFinal: new Date(),
        id_patio: null,
        patioGrupo: null,
    };



    $scope.Actions = {


        MesAtual: () => {
            hoje = new Date()

            $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
            $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
            $scope.dados.liberacaoInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
            $scope.dados.liberacaoFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
        },




        GetLiberacoes: async () => {
            const liberacoes = await $http.get(`${urlServidor.urlServidorChatAdmin}/relatorios/liberacoes-diario`, { params: $scope.dados })

            if (liberacoes.data.length > 0) {
                const Responseliberacoes = liberacoes.data.map(Item => ({
                    data: Item.data,
                    dataSemCss: Item.data,
                    total: Item.total,
                    TotalString: Item.total.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' }),

                }))
                var total = []
                Responseliberacoes.forEach(item => total.push(item.total))

                const TotalLiberacao = total.reduce((soma, i) => { return soma + i })

                Responseliberacoes.push({
                    data: `<h1 style="color:blue;font-weight:bold;font-size:18px;margin-left:40px">Total Período</h1>`,
                    TotalString: `<h1 style="color:blue;font-weight:bold;font-size:18px">${TotalLiberacao.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</h1>`
                })

                $scope.liberacoesDiario = Responseliberacoes
                return TotalLiberacao
            }
        },


        GetContasReceber: async () => {
            const ReceberDiario = await $http.get(`${urlServidor.urlServidorChatAdmin}/relatorios/receber-diario`, { params: $scope.dados })
            if (ReceberDiario.data.length > 0) {
                const ResponseReceber = ReceberDiario.data.map(Item => ({
                    vencimento: Item.vencimento,
                    VencimentoSemCss: Item.vencimento,
                    total: Item.total,
                    TotalString: Item.total.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' }),

                }))
                var total = []

                ResponseReceber.forEach(item => total.push(item.total))

                const TotalReceber = total.reduce((soma, i) => { return soma + i })

                ResponseReceber.push({
                    vencimento: `<h1 style="color:blue;font-weight:bold;font-size:18px;margin-left:40px">Total Periodo</h1>`,
                    TotalString: `<h1 style="color:blue;font-weight:bold;font-size:18px">${TotalReceber.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</h1>`
                })

                $scope.ReceberDiario = ResponseReceber
                return TotalReceber
            }

        },


        GetTranferenciasRecebidas: async () => {

            const Tranferencias = await $http.get(`${urlServidor.urlServidorChatAdmin}/relatorios/transferencias-creditodiario`,
                { params: $scope.dados })
            if (Tranferencias.data.length > 0) {
                const ResponseTranferencias = Tranferencias.data.map(Item => ({
                    vencimento: Item.vencimento,
                    vencimentoSemCss: Item.vencimento,
                    total: Item.total,
                    TotalString: Item.total.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' }),

                }))
                var total = []
                ResponseTranferencias.forEach(item => total.push(item.total))

                const TotalTranferencias = total.reduce((soma, i) => { return soma + i })

                ResponseTranferencias.push({
                    vencimento: `<h1 style="color:blue;font-weight:bold;font-size:18px;margin-left:40px">Total Periodo</h1>`,
                    TotalString: `<h1 style="color:blue;font-weight:bold;font-size:18px">${TotalTranferencias.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</h1>`
                })

                $scope.ResponseTranferencias = ResponseTranferencias
                return TotalTranferencias

            }



        },


        GetContasPagar: async () => {


            const ContasPagar = await $http.get(`${urlServidor.urlServidorChatAdmin}/relatorios/pagar-diario`,
                { params: $scope.dados })
            if (ContasPagar.data.length > 0) {

                const ResponseContasPagar = ContasPagar.data.map(Item => ({
                    vencimento: Item.vencimento,
                    vencimentoSemCss: Item.vencimento,
                    total: Item.total,
                    TotalString: Item.total.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' }),

                }))
                var total = []

                ResponseContasPagar.forEach(item => total.push(item.total))

                const TotalContasPagar = total.reduce((soma, i) => { return soma + i })

                ResponseContasPagar.push({
                    vencimento: `<h1 style="color:blue;font-weight:bold;font-size:18px;margin-left:40px">Total Periodo</h1>`,
                    TotalString: `<h1 style="color:blue;font-weight:bold;font-size:18px">${TotalContasPagar.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</h1>`
                })

                $scope.ResponseContasPagar = ResponseContasPagar
                return TotalContasPagar

            }


        },

        GetTranferenciasFeitas: async () => {

            const Tranferencias = await $http.get(`${urlServidor.urlServidorChatAdmin}/relatorios/transferencias-debitodiario`,
                { params: $scope.dados })
            if (Tranferencias.data.length > 0) {
                const ResponseTranferencias = Tranferencias.data.map(Item => ({
                    vencimento: Item.vencimento,
                    vencimentoSemCss: Item.data,
                    total: Item.total,
                    TotalString: Item.total.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' }),

                }))
                var total = []


                ResponseTranferencias.forEach(item => total.push(item.total))

                const TotalTranferencias = total.reduce((soma, i) => { return soma + i })

                ResponseTranferencias.push({
                    vencimento: `<h1 style="color:blue;font-weight:bold;font-size:18px;margin-left:40px">Total Periodo</h1>`,
                    TotalString: `<h1 style="color:blue;font-weight:bold;font-size:18px">${TotalTranferencias.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</h1>`
                })

                $scope.ResponseTranferencias = ResponseTranferencias
                return TotalTranferencias
            }


        },


        GetTaxasBancarias: async () => {

            const TaxasBancarias = await $http.get(`${urlServidor.urlServidorChatAdmin}/relatorios/taxas-bancarias`,
                { params: $scope.dados })

            if (TaxasBancarias.data.length > 0) {
                const ResponseTaxasBancarias = TaxasBancarias.data.map(Item => ({
                    data: Item.data,
                    dataSemCss: Item.data,
                    total: Item.total,
                    TotalString: Item.total.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' }),

                }))
                var total = []


                ResponseTaxasBancarias.forEach(item => total.push(item.total))

                const TotalTaxasBancarias = total.reduce((soma, i) => { return soma + i })

                ResponseTaxasBancarias.push({
                    data: `<h1 style="color:blue;font-weight:bold;font-size:18px;margin-left:40px">Total Periodo</h1>`,
                    TotalString: `<h1 style="color:blue;font-weight:bold;font-size:18px">${TotalTaxasBancarias.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</h1>`
                })

                $scope.ResponseTaxasBancarias = ResponseTaxasBancarias
                return TotalTaxasBancarias

            }

        },

        Pesquisar: async function () {

            //receitas
            const Liberacoes = await this.GetLiberacoes() || 0
            const ContasReceber = await this.GetContasReceber() || 0

            const TranferenciasRecebidas = await this.GetTranferenciasRecebidas() || 0

            //despesas
            const ContasPagar = await this.GetContasPagar() || 0

            const TranferenciasFeitas = await this.GetTranferenciasFeitas() || 0

            const TaxasBancarias = await this.GetTaxasBancarias() || 0


            this.GeraResulmo(Liberacoes, ContasReceber, TranferenciasRecebidas, ContasPagar, TranferenciasFeitas, TaxasBancarias)


        },
        GeraResulmo: async function (...Valores) {
            if (Valores) {


                const Receitas = Valores.slice(0, 3)
                const TotalReceitas = Receitas.reduce((soma, i) => { return soma + i })


                const Despesas = Valores.slice(3, 7)


                const TotalDespesas = Despesas.reduce((soma, i) => { return soma + i })


                const Total = TotalReceitas - TotalDespesas
                $scope.Resumo = [{
                    Desc: `<h1  style="font-size: 18px;">Receita - Liberações</h1>`,
                    total: `<h1 style="font-size: 18px;" > ${Valores[0].toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</h1>`
                },
                {
                    Desc: `<h1  style="font-size: 18px;">Receita - Contas a Receber</h1>`,
                    total: `<h1 style="font-size: 18px;" > ${Valores[1].toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</h1>`
                },
                {
                    Desc: `<h1  style="font-size: 18px;">Receita - Transferencia Recebida </h1>`,
                    total: `<h1 style="font-size: 18px;" > ${Valores[2].toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</h1>`
                },
                {
                    Desc: `<h1  style="font-size: 18px;"> Despesa - Contas a Pagar </h1>`,
                    total: `<h1 style="font-size: 18px;" > ${Valores[3].toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</h1>`
                },
                {
                    Desc: `<h1  style="font-size: 18px;">Despesa - Trasferencias Feitas </h1>`,
                    total: `<h1 style="font-size: 18px;" > ${Valores[4].toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</h1>`
                },
                {
                    Desc: `<h1  style="font-size: 18px;">   Despesa - Taxas Bancarias    </h1>`,
                    total: `<h1 style="font-size: 18px;" > ${Valores[5].toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</h1>`
                }
                ]

                if (Total < 0) {
                    $scope.Resumo.push({
                        Desc: `<h1  style="font-size: 18px;color:red">  Resultado   </h1>`,
                        total: `<h1 style="font-size: 18px;color:red" >  ${Total.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</h1>`
                    })
                } else {
                    $scope.Resumo.push({
                        Desc: `<h1  style="font-size: 18px;color:blue">  Resultado   </h1>`,
                        total: `<h1 style="font-size: 18px;color:blue" >  ${Total.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</h1>`
                    })
                }

                return $scope.Resumo
            }

        },

    }




    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("relatorio-contas-a-pagar.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    };




    $scope.gerarPdf = async function () {

        if ($scope.liberacoesDiario) {
             $scope.Liberacoes = $scope.liberacoesDiario.map(item => ({
                Data: item.data ? item.data.replace(/(<([^>]+)>)/ig, " ") : "Sem Registros",
                Total: item.TotalString ? item.TotalString.replace(/(<([^>]+)>)/ig, " ").toLocaleString('pt-br', { style: 'currency', currency: 'BRL' }) : 0,
            }))

        } else {
            $scope.Liberacoes= [{
                Data: "Sem Registros",
                Total: 0,

            }]

        }




        if ($scope.ReceberDiario) {

             $scope.Receber = $scope.ReceberDiario.map(item => ({

                Data: item.vencimento ? item.vencimento.replace(/(<([^>]+)>)/ig, " ") : "Sem Registros",
                Total: item.TotalString ? item.TotalString.replace(/(<([^>]+)>)/ig, " ") : 0,

            }))
        } else { 
            $scope.Receber = [{
                Data: "Sem Registros",
                Total: 0,
            }]
        }




        if ($scope.ResponseTranferencias) {

             $scope.TranferenciasRecebidas = $scope.ResponseTranferencias.map(item => ({
                Data: item.vencimento ? item.vencimento.replace(/(<([^>]+)>)/ig, " ") : "Sem Registros",
                Total: item.TotalString ? item.TotalString.replace(/(<([^>]+)>)/ig, " ") : 0,
            }))
        } else {
            $scope.TranferenciasRecebidas = [{
                Data: "Sem Registros",
                Total: 0,
            }]
        }


        if ($scope.ResponseContasPagar) {

          $scope.Pagar = $scope.ResponseContasPagar.map(item => ({
                Data: item.vencimento ? item.vencimento.replace(/(<([^>]+)>)/ig, " ") : "Sem Registros",
                Total: item.TotalString ? item.TotalString.replace(/(<([^>]+)>)/ig, " ") : 0,
            }))

        } else {
            $scope.Pagar= [{
                Data: "Sem Registros",
                Total: 0,
            }]
        }

        if ($scope.ResponseTranferencias) {


             $scope.TransferenciasFeitas = $scope.ResponseTranferencias.map(item => ({
                Data: item.vencimento ? item.vencimento.replace(/(<([^>]+)>)/ig, " ") : "Sem Registros",
                Total: item.TotalString ? item.TotalString.replace(/(<([^>]+)>)/ig, " ") : 0,
            }))
        } else {
            $scope.TransferenciasFeitas =[ {
                Data: "Sem Registros",
                Total: 0,
            }]
        }

        if ($scope.ResponseTaxasBancarias) {
           $scope.TaxasBancarias = $scope.ResponseTaxasBancarias.map(item => ({

                Data: item.data ? item.data.replace(/(<([^>]+)>)/ig, " ") : "Sem Registros",
                Total: item.TotalString ? item.TotalString.replace(/(<([^>]+)>)/ig, " ") : 0,

            }))

        } else {
            $scope.TaxasBancarias = [{
                Data: "Sem Registros",
                Total: 0,
            }]
        }
        console.log($scope.Resumo);

        
                 $scope.Resu = $scope.Resumo.map(item => ({
                    Desc: item.Desc.replace(/(<([^>]+)>)/ig, " "),
                    Total: item.total.replace(/(<([^>]+)>)/ig, " ").toLocaleString('pt-br', { style: 'currency', currency: 'BRL' }) 
                }))
      





        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = '{total_pages_count_string}'

        doc.setTextColor(100);
        doc.setFontSize(12);
        doc.autoTable({

            body: $scope.Liberacoes ,


            columns: [

                { header: 'Data', dataKey: 'Data' },
                { header: 'Total', dataKey: 'Total' },


            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },

            margin: { top: 30 },


            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')
                doc.text('Relatorio Gerencial' , data.settings.margin.left + 50, 22);

                // 

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            }
        });


        doc.autoTable({
            body: $scope.Receber,
          

        
        columns: [
            {header: 'Data', dataKey: 'Data'},
            {header: 'Total', dataKey: 'Total'},
        ],
        bodyStyles: {
            margin: 10,
            fontSize: 08,
        },
        })



        doc.autoTable({
            body: $scope.TranferenciasRecebidas,
          

        
        columns: [
            {header: 'Data', dataKey: 'Data'},
            {header: 'Total', dataKey: 'Total'},
        ],
        bodyStyles: {
            margin: 10,
            fontSize: 08,
        },
        })


        doc.autoTable({
            body:  $scope.Pagar,
          

        
        columns: [
            {header: 'Data', dataKey: 'Data'},
            {header: 'Total', dataKey: 'Total'},
        ],
        bodyStyles: {
            margin: 10,
            fontSize: 08,
        },
        })

        doc.autoTable({
            body:   $scope.TransferenciasFeitas ,
          

        
        columns: [
            {header: 'Data', dataKey: 'Data'},
            {header: 'Total', dataKey: 'Total'},
        ],
        bodyStyles: {
            margin: 10,
            fontSize: 08,
        },
        })


        doc.autoTable({
            body:  $scope.TaxasBancarias ,
          

        
        columns: [
            {header: 'Data', dataKey: 'Data'},
            {header: 'Total', dataKey: 'Total'},
        ],
        bodyStyles: {
            margin: 10,
            fontSize: 08,
        },
        })

        doc.autoTable({
            body:   $scope.Resu,
          

        
        columns: [
            {header: 'Descrição', dataKey: 'Desc'},
            {header: 'Total', dataKey: 'Total'},
        ],
        bodyStyles: {
            margin: 10,
            fontSize: 08,
        },
        })

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('Relatorio-Gerencial.pdf')


    }
});



