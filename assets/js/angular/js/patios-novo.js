'use strict'
angular
	.module('app')
	.controller('patiosNovoCtrl', function (
		$scope,
		$http,
		urlServidor,
		myFunctions,
		estados,
		viaCep
	) {
		$scope.patio = {
			nome: '',
			endereco: '',
			bairro: '',
			cep: '',
			cidade: '',
			estado: '',
			telefone: '',
			ativo: '',
			email: '',
			responsavel: '',
			observacao: '',
			info: '',
			meta_apreensoes: '',
			meta_liberacoes: '',
			ponto_equilibrio: '',
			contrato: '',
			whatsapp: '',
			inicio_contrato: '',
			fim_contrato: '',
			vencimento_seguro: '',
			conta:'',
			idempresa: localStorage.getItem('id_empresa'),
		}

		$scope.estados = estados.uf

		$scope.buscaCep = (cep) => {
			if (cep.length == 9) {
				viaCep.get(cep).then(function (response) {
					$scope.patio.endereco = response.logradouro.toUpperCase()
					$scope.patio.bairro = response.bairro.toUpperCase()
					$scope.patio.cidade = response.localidade.toUpperCase()
					$scope.patio.estado = response.uf.toUpperCase()
				})
			}
		}

		buscaContas().then(function (result) {
			$scope.contas = result;
		});

		$scope.isDisabled = false

		function buscaContas() {
			return new Promise(function (resolve, reject) {
				$http.put(urlServidor.urlServidorChatAdmin + '/contas/listar').then(function (response) {
					let dados = response.data;
					resolve(dados);
				});
			})
		}
		$scope.gravar = function () {
			
			$http
				.post(
					urlServidor.urlServidorChatAdmin + '/patios/cadastrar',
					$scope.patio
				)
				.then(function (response) {
					if (response.data.code) {
						myFunctions.showAlert('Erro na gravação!')
					} else {
						if (response.data == '11000') {
							myFunctions.showAlert('Patio já existe!')
						} else {
							myFunctions.showAlert('Cadastro executado com sucesso!')
							$scope.patio.id = response.data.insertId
							$scope.isDisabled = true
						}
					}
				})
			
		}
	})
