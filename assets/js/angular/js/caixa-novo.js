'use strict';
angular.module('app')
.controller("caixaNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions) {  

  $scope.dados = {
     valor:0.00,  
     data: dataAtualFormatada() ,
     grupo:'',
     categoria:'',
     comentario:'',
     documento:'',
     competencia:'',
     id_cliente_fornec:null,
     id_patio:null,
     conta:null,
     idempresa: localStorage.getItem('id_empresa')
  }

  $scope.mostrarcliente = false ;
  $scope.mostrarfornecedor = false ;
  
  buscaGrupos().then( function(result){
    $scope.grupos = result ;
  });  

  buscaContas().then( function(result){
    $scope.contas = result ;
  });  

  buscaFornecedores().then( function(result){
    $scope.fornecedores = result ;
  });  

  buscaClientes().then( function(result){
    $scope.clientes = result ;
  });  
  
  buscaPatios(localStorage.getItem('id_usuario'),localStorage.getItem('id_empresa')).then( function(result){
    $scope.patios = result ;
  });  

  $scope.SelecionaCategoria = function(idgrupo){
    $scope.dados.categoria = '' ;
    buscaCategorias(idgrupo).then( function(result){
      $scope.categorias = result ;
    });  
  }

  $scope.novoRegistro = function(){

    $scope.dados = {
      valor:0.00,  
      data: dataAtualFormatada() ,
      grupo:'',
      categoria:'',
      comentario:'',
      documento:'',
      competencia:'',
      id_cliente_fornec:null,
      id_patio: $scope.dados.id_patio,
      conta:null,
      idempresa: localStorage.getItem('id_empresa')
   }

  } 

  $scope.SelecionaTipo = function(id){

    buscaItemArray($scope.categorias,id).then( function(response) {

      if( response == 0 ){
        $scope.mostrarcliente = true  
        $scope.mostrarfornecedor = false
      } else {
        $scope.mostrarfornecedor = true
        $scope.mostrarcliente = false         
      }  
     
    })

  } 

  $scope.isDisabled = false

  $scope.gravar = function() {        

    $http.post(urlServidor.urlServidorChatAdmin+'/caixa/cadastrar', $scope.dados ).then( function(response){

        if (response.data.code){
            
          myFunctions.showAlert('Erro na gravação!')

        }else{

          if ( response.data == '11000' )                 
          {
            myFunctions.showAlert('Lançamento Caixa já existe!')
          }else
          {                    

            myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
            $scope.isDisabled = true ;            

          }  
                
        }; 

    }); 

  }; 

  function buscaItemArray(arrayItens,id){
    return new Promise( function( resolve,reject){

      for (var x in arrayItens) {
         if( arrayItens[x].id == id ){           
           resolve( arrayItens[x].tipo );
         } 
      }
      
    })   
  }    

  function buscaGrupos(){
    return new Promise( function( resolve,reject){
      $http.put(urlServidor.urlServidorChatAdmin+'/grupos/listar').then( function(response){
        let dados = response.data ;
        resolve(dados) ;
      });
    })
  }

  function buscaContas(){
    return new Promise( function( resolve,reject){
      $http.put(urlServidor.urlServidorChatAdmin+'/contas/listar').then( function(response){
        let dados = response.data ;
        resolve(dados) ;
      });
    })
  }

  function buscaCategorias(idgrupo){
    return new Promise( function( resolve,reject){
      $http.put(urlServidor.urlServidorChatAdmin+'/categorias/listar',{ grupo: idgrupo}).then( function(response){
        let dados = response.data ;
        resolve(dados) ;
      });
    })
  }

  function buscaFornecedores(){
    return new Promise( function( resolve,reject){
      $http.put(urlServidor.urlServidorChatAdmin+'/fornecedores/listar').then( function(response){
        let dados = response.data ;
        resolve(dados) ;
      });
    })
  }

  function buscaClientes(){
    return new Promise( function( resolve,reject){
      $http.put(urlServidor.urlServidorChatAdmin+'/clientes/listar').then( function(response){
        let dados = response.data ;
        resolve(dados) ;  
      });
    })  
  }  
  
  function buscaPatios(idUsuario,idEmpresa){
    return new Promise( function( resolve,reject){
      $http.put(urlServidor.urlServidorChatAdmin+'/patios/listar-patiosusuario',{ idUsuario: idUsuario , idEmpresa : idEmpresa  }).then( function(response){  
        let dados = response.data ;  
        resolve(dados) ;
      });
    })  
  }
  

  function dataAtualFormatada(){
    var data = new Date(),
        dia  = data.getDate().toString(),
        diaF = (dia.length == 1) ? '0'+dia : dia,
        mes  = (data.getMonth()+1).toString(), //+1 pois no getMonth Janeiro começa com zero.
        mesF = (mes.length == 1) ? '0'+mes : mes,
        anoF = data.getFullYear();
    return diaF+"/"+mesF+"/"+anoF;
  }

}); 