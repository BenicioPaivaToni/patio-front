app.controller('controleTrafegoCtrl', function ($scope, $location, $http, urlServidor, myFunctions, $mdDialog, $filter) {

    $scope.dados = {
        id_veiculo: null,
        data_saida: null,
        hora_saida: null,
        hodometro_saida: null,
        destino: null,
        nome_usuario: null,
    }

    $scope.dadosChegada = {
        data_chegada: null,
        hora_chegada: null,
        hodometro_chegada: null,
        km_rodado: null,
        abastecido_litros: 0
    }


    
    $scope.limpar = function (ev) {
        
        $scope.dados.id_veiculo = null
        $scope.dados.data_saida = null
        $scope.dados.hora_saida = null
        $scope.dados.hodometro_saida = null
        $scope.dados.destino = null
        $scope.dados.nome_usuario = null
        $scope.dadosList = []

    }



    $scope.limparAjustes = function (ev) {
        
        $scope.dadosAjustes.id_veiculo = null
        $scope.dadosAjustes.data_saida = null
        $scope.dadosAjustes.hora_saida = null
        $scope.dadosAjustes.hodometro_saida = null
        $scope.dadosAjustes.data_chegada = null
        $scope.dadosAjustes.hora_chegada = null
        $scope.dadosAjustes.hodometro_chegada = null
        $scope.dadosAjustes.destino = null
        $scope.dadosAjustes.nome_usuario = null
        $scope.dadosAjustes.km_rodado = null
        $scope.dadosAjustes.abastecido_litros = null
        $scope.dadosListAjustes = []

        $scope.isEnabled = true

    }
    
    $scope.isEnabled = true

    $scope.ajuste_logistica = localStorage.getItem('ajuste_logistica');

    
    buscaVeiculos().then(function (result) {
        $scope.veiculos = result
    })



    $scope.buscaDados = function () {

        $scope.dadosList = []

        let veiculoAtual = $scope.veiculos.filter(function(obj) {
            return obj.id == $scope.dados.id_veiculo
        })

        $scope.veiculoPlaca = veiculoAtual[0].placa
        $scope.veiculoDescricao = veiculoAtual[0].descricao

        $http.get(urlServidor.urlServidorChatAdmin + '/veiculos/listar-dados', { params: { id_veiculo: $scope.dados.id_veiculo } }).then(function (response) {

            let dados = response.data

            dados = dados.slice(0, 5)

            $scope.dadosList = dados.sort((a, b) => b.id - a.id)

            if ($scope.dadosList.length > 0){

                $scope.isDisabled = true

                $scope.dados.hodometro_saida = $scope.dadosList[0].hodometro_chegada

            }else{
                $scope.isDisabled = false
            }          

        })

        $scope.dados.data_saida = null
        $scope.dados.hora_saida = null
        $scope.dados.hodometro_saida = null
        $scope.dados.destino = null
        $scope.dados.nome_usuario = null

    }

   
        
    buscaFuncionarios().then(function (result) {
           
        // $scope.funcionarios = result.filter((item) => {
        //    return item.ativo == 'SIM'
        // })
      
       $scope.funcionarios = result
    })


    function buscaFuncionarios() {

        return new Promise(function (resolve, reject) {
        
            $http.get(urlServidor.urlServidorChatAdmin + '/funcionarios/listar').then(function (response) {
            
                let dados = response.data
                
                resolve(dados)
                
            })

        })
    }


   

    $scope.gravar = function () {
        
        let dataAtual = new Date()

        const payload = {
            
            id_veiculo: $scope.dados.id_veiculo,
            data_saida: dataAtual.toLocaleDateString('pt-BR', { day: '2-digit', month: '2-digit', year: 'numeric' }),
            hora_saida: dataAtual.toLocaleTimeString('pt-BR', { hour: '2-digit', minute: '2-digit', second: '2-digit' }),
            hodometro_saida: $scope.dados.hodometro_saida,
            destino: $scope.dados.destino,
            nome_usuario: $scope.dados.nome_usuario
        
        }

        if (payload.destino == null || payload.nome_usuario == null){

            myFunctions.showAlert('Exitem campos sem preencher! Verifique!')

            return

        }

        $http.get(urlServidor.urlServidorChatAdmin + '/veiculos/busca-preenchimento-dados', { params: { id_veiculo: $scope.dados.id_veiculo } }).then(function (response) {

            let retorno = response.data
            
            if (retorno[0].resultado == "SIM"){

                myFunctions.showAlertCustom("Este veiculo possui uma viagem em aberto!<br/><br/>É necessário finalizar a viagem anterior antes de prosseguir!")

                return

            }

            $http.post(urlServidor.urlServidorChatAdmin + '/veiculos/cadastrar-trafego-saida', payload).then(function (response) {

                if (response.data.code) {

                    myFunctions.showAlert('Erro na gravação!')

                } else {

                    myFunctions.showAlert('Cadastro de percurso executado com sucesso!')

                    $scope.buscaDados()
            
                    $scope.dados.data_saida = null
                    $scope.dados.hora_saida = null
                    $scope.dados.hodometro_saida = null
                    $scope.dados.destino = null
                    $scope.dados.nome_usuario = null
                
                }
            })

        })

    }


    
    $scope.gravarAjustes = function () {
        
        const payload = {
            
            id: $scope.dadosAjustes.id,
            data_saida: $scope.dadosAjustes.data_saida,
            hora_saida: $scope.dadosAjustes.hora_saida,
            hodometro_saida: $scope.dadosAjustes.hodometro_saida,
            data_chegada: $scope.dadosAjustes.data_chegada,
            hora_chegada: $scope.dadosAjustes.hora_chegada,
            hodometro_chegada: $scope.dadosAjustes.hodometro_chegada,
            destino: $scope.dadosAjustes.destino,
            nome_usuario: $scope.dadosAjustes.nome_usuario,
            km_rodado: $scope.dadosAjustes.km_rodado,
            abastecido_litros: $scope.dadosAjustes.abastecido_litros
  
        }
        
        $http.post(urlServidor.urlServidorChatAdmin + '/veiculos/alterar-registro-trafego', payload).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                myFunctions.showAlert('Registro de percurso alterado com sucesso!')

                $scope.buscaDadosAjustes()
           
                $scope.dadosAjustes.data_saida = null
                $scope.dadosAjustes.hora_saida = null
                $scope.dadosAjustes.hodometro_saida = null
                $scope.dadosAjustes.data_chegada = null
                $scope.dadosAjustes.hora_chegada = null
                $scope.dadosAjustes.hodometro_chegada = null
                $scope.dadosAjustes.destino = null
                $scope.dadosAjustes.nome_usuario = null
                $scope.dadosAjustes.km_rodado = null
                $scope.dadosAjustes.abastecido_litros = null

                $scope.isEnabled = true

            }
        })
    }




    $scope.calculaKM = function () {

       $scope.dadosChegada.km_rodado = $scope.dadosChegada.hodometro_chegada - $scope.dadosSaida.hodometro_saida

    }


    $scope.calculaKMAjustes = function () {

        $scope.dadosAjustes.km_rodado = $scope.dadosAjustes.hodometro_chegada - $scope.dadosAjustes.hodometro_saida
 
    }
    
    
  
    function buscaVeiculos() {

        return new Promise(function (resolve, reject) {

            $http.get(urlServidor.urlServidorChatAdmin + '/veiculos/listar').then(function (response) {

                let dados = response.data

                resolve(dados)
            })
        })

    }


    $scope.detalheAjustes = function (ev, id) {
        
        var dados = $scope.dadosListAjustes.filter(function (obj) {
            return obj.id == id

        })

        let _id = id
       
        $scope.dadosAjustes = {

            id: _id,
            id_veiculo: $scope.dadosAjustes.id_veiculo,
            hora_saida: dados[0].hora_saida,
            data_saida: dados[0].data_saida,
            hodometro_saida: dados[0].hodometro_saida,
            nome_usuario: dados[0].nome_usuario,
            destino: dados[0].destino,
            data_chegada: $filter("date")(dados[0].data_chegada, "dd/MM/yyyy" ),
            hora_chegada: dados[0].hora_chegada,
            hodometro_chegada: dados[0].hodometro_chegada,
            km_rodado: dados[0].km_rodado,
            abastecido_litros: dados[0].abastecido_litros

        }

        $scope.isEnabled = false

    }


     
    $scope.detalhe = function (ev, id) {
        
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id

        })

        let _id = id
       
        $scope.dadosSaida = {

            hora_saida: dados[0].hora_saida,
            data_saida: dados[0].data_saida,
            hodometro_saida: dados[0].hodometro_saida,
            nome_usuario: dados[0].nome_usuario,
            destino: dados[0].destino

        }



        $mdDialog.show({

            controller: function ($scope, $mdDialog, myFunctions, $http, urlServidor) {

                $scope.gravarDadosRetorno = function(){

                    let dataAtual = new Date()

                    const payload = {
                        id: _id,
                        data_chegada: dataAtual.toLocaleDateString('pt-BR', { day: '2-digit', month: '2-digit', year: 'numeric' }),
                        hora_chegada: dataAtual.toLocaleTimeString('pt-BR', { hour: '2-digit', minute: '2-digit', second: '2-digit' }),
                        hodometro_chegada: $scope.dadosChegada.hodometro_chegada,
                        km_rodado: $scope.dadosChegada.km_rodado,
                        abastecido_litros: $scope.dadosChegada.abastecido_litros
                    }

                    if (payload.hodometro_chegada < $scope.dadosSaida.hodometro_saida){

                        myFunctions.showAlert( 'Quilometragem de chegada não pode ser inferior a de saída! Verifique!' )

                        return
                    }

                    if (payload.km_rodado >= 4000){

                        myFunctions.showAlert( 'Quilometragem percorrida muito alta! Confira as informações preenchidas!' )

                        return
                    }
                           
                    $http.post(urlServidor.urlServidorChatAdmin+'/veiculos/cadastrar-trafego-chegada', payload).then( function(response){
                    
                        if (response.data.code){
                                
                            myFunctions.showAlert('Erro na gravação!')
                    
                        }else{
                                               
                            myFunctions.showAlert( 'Dados gravados com sucesso!' );
                                                                                    
                        }
                    })

                    setTimeout(function () {

                       $scope.buscaDados()

                    }, 1000);
                    
                    $scope.dadosChegada.hodometro_chegada = null
                    $scope.dadosChegada.km_rodado = null
                    $scope.dadosChegada.abastecido_litros = null
                    
                    $scope.cancelar()

                }
            },
          
            templateUrl: 'adiciona-dados-retorno.html',
            scope: $scope,
            preserveScope: true,
            scopoAnterior: $scope
        }).then(function (answer) {
            let a = answer;
           
        }, function () {
            $scope.statusdialog = 'You cancelled the dialog.'
        })
    
        $scope.cancelar = function() {
            $mdDialog.cancel();
        }

    }



    $scope.hideCheck = function (ev, rowId) {
        
        var row = $scope.dadosList.find((x) => x.id === rowId);
    
        if (row && row.km_rodado !== 0) {
            return true;
        }
    
        return false;
    }



    $scope.buscaDadosAjustes = function () {

        $scope.dadosListAjustes = []

        let veiculoAtual = $scope.veiculos.filter(function(obj) {
            return obj.id == $scope.dadosAjustes.id_veiculo
        })

        $scope.veiculoPlacaAjustes = veiculoAtual[0].placa
        $scope.veiculoDescricaoAjustes = veiculoAtual[0].descricao

        $http.get(urlServidor.urlServidorChatAdmin + '/veiculos/listar-dados', { params: { id_veiculo: $scope.dadosAjustes.id_veiculo } }).then(function (response) {

            let dados = response.data

            $scope.dadosListAjustes = dados.sort((a, b) => b.id - a.id)
                  
        })

    }




    $scope.gerarPlanilha = function () {

        if ($scope.dadosList){

            alasql('SELECT * INTO XLSX("veiculos.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);

        }else{

            myFunctions.showAlert("Sem dados para gerar Planilha!") 

        }
    }



    $scope.gerarPlanilhaAjustes = function () {

        if ($scope.dadosListAjustes){

            alasql('SELECT * INTO XLSX("veiculos-ajustes.xlsx",{headers:true}) FROM ?', [$scope.dadosListAjustes]);

        }else{

            myFunctions.showAlert("Sem dados para gerar Planilha!") 

        }
    }



    $scope.gerarPdf = function () {
        
        if ($scope.dadosList){

            let dataAtual = new Date().toISOString().slice(0, 10);
        
            var doc = new jsPDF({orientation: "landscape"});
            var totalPagesExp = '{total_pages_count_string}'

            doc.setFontSize(12);
            doc.autoTable({

                body: $scope.dadosList,
                columns: [
                    {header: 'Data Saida', dataKey: 'data_saida'},
                    {header: 'Hora Saida', dataKey: 'hora_saida'},
                    {header: 'Hodometro Saida', dataKey: 'hodometro_saida'},
                    {header: 'Data Chegada', dataKey: 'data_chegada'},
                    {header: 'Hora Chegada', dataKey: 'hora_chegada'},
                    {header: 'Hodometro Chegada', dataKey: 'hodometro_chegada'},
                    {header: 'KM Rodado', dataKey: 'km_rodado'},
                    {header: 'Usuario', dataKey: 'nome_usuario'},
                    {header: 'Abastecido', dataKey: 'abastecido_litros'},

                ],
                bodyStyles: {
                    margin: 10,
                    fontSize: 8,
                },
                didDrawPage: function (data) {
                    // Header
                    doc.setFontSize(18)
                    doc.setTextColor(40)
                    doc.setFontStyle('normal')

                    doc.text("Veiculo: " + $scope.veiculoDescricao + ' - Placa: ' + $scope.veiculoPlaca, data.settings.margin.left + 5, 18)

                    // Footer
                    var str = 'Pagina ' + doc.internal.getNumberOfPages()
                    // Total page number plugin only available in jspdf v1.0+
                    if (typeof doc.putTotalPages === 'function') {
                        str = str + ' de ' + totalPagesExp
                        str = str + "                                                                           Data " +
                        dataAtual.substr(8, 2) + "/" + dataAtual.substr(5, 2) +
                        "/" + dataAtual.substr(0, 4) + "                  Usuário " + localStorage.getItem("nome_usuario");

                    }

                    doc.setFontSize(10)

                    // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                    var pageSize = doc.internal.pageSize
                    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                    doc.text(str, data.settings.margin.left, pageHeight - 10)
                },
                margin: {top: 30},
            });
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp)
            }

            doc.save('Controle_trafego.pdf')

        }else{

            myFunctions.showAlert("Sem dados para gerar PDF!") 

        }
    }

 


    $scope.gerarPdfAjustes = function () {
        
        if ($scope.dadosListAjustes){

            let dataAtual = new Date().toISOString().slice(0, 10);
        
            var doc = new jsPDF({orientation: "landscape"});
            var totalPagesExp = '{total_pages_count_string}'

            doc.setFontSize(12);
            doc.autoTable({

                body: $scope.dadosListAjustes,
                columns: [
                    {header: 'Data Saida', dataKey: 'data_saida'},
                    {header: 'Hora Saida', dataKey: 'hora_saida'},
                    {header: 'Hodometro Saida', dataKey: 'hodometro_saida'},
                    {header: 'Data Chegada', dataKey: 'data_chegada'},
                    {header: 'Hora Chegada', dataKey: 'hora_chegada'},
                    {header: 'Hodometro Chegada', dataKey: 'hodometro_chegada'},
                    {header: 'KM Rodado', dataKey: 'km_rodado'},
                    {header: 'Usuario', dataKey: 'nome_usuario'},
                    {header: 'Abastecido', dataKey: 'abastecido_litros'},

                ],
                bodyStyles: {
                    margin: 10,
                    fontSize: 8,
                },
                didDrawPage: function (data) {
                    // Header
                    doc.setFontSize(18)
                    doc.setTextColor(40)
                    doc.setFontStyle('normal')

                    doc.text("Veiculo: " + $scope.veiculoDescricaoAjustes + ' - Placa: ' + $scope.veiculoPlacaAjustes, data.settings.margin.left + 5, 18)

                    // Footer
                    var str = 'Pagina ' + doc.internal.getNumberOfPages()
                    // Total page number plugin only available in jspdf v1.0+
                    if (typeof doc.putTotalPages === 'function') {
                        str = str + ' de ' + totalPagesExp
                        str = str + "                                                                           Data " +
                        dataAtual.substr(8, 2) + "/" + dataAtual.substr(5, 2) +
                        "/" + dataAtual.substr(0, 4) + "                  Usuário " + localStorage.getItem("nome_usuario");

                    }

                    doc.setFontSize(10)

                    // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                    var pageSize = doc.internal.pageSize
                    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                    doc.text(str, data.settings.margin.left, pageHeight - 10)
                },
                margin: {top: 30},
            });
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp)
            }

            doc.save('Controle_trafego-Ajustes.pdf')

        }else{

            myFunctions.showAlert("Sem dados para gerar PDF!") 

        }
    }


})