
angular.module('app').controller('documentosVencidosCtrl', function ($scope, $http, urlServidor, $scope, $http, urlServidor, myFunctions) {


   
    $scope.filtrado = false;
    
    const pesquisar = async  () => {
        const response = await $http.get(urlServidor.urlServidorChatAdmin + '/relatorios/documentos-validade')

            
            $scope.dadosPatios = response.data.Patios;
            $scope.dadosMotoristas = response.data.Motoristas;
            $scope.dadosReboques = response.data.Reboques;
            $scope.dadosEmpresasReboques = response.data.EmpresasReboques;
            $scope.dadosList = response.data;
            $scope.filtrado = true;
    };
    pesquisar()
    
    $scope.gerarPlanilhaPatios = function () {
        if($scope.filtrado){
            alasql('SELECT * INTO XLSX("documentos-validade-patios.xlsx",{headers:true}) FROM ?', [$scope.dadosPatios]);
    
        }else{
            myFunctions.showAlert("É necessário realizar a busca primeiro!")
        }
    }
    $scope.gerarPlanilhaEmpresas = function () {
        if($scope.filtrado){
            alasql('SELECT * INTO XLSX("documentos-validade-empresas.xlsx",{headers:true}) FROM ?', [$scope.dadosEmpresasReboques]); 
    
        }else{
            myFunctions.showAlert("É necessário realizar a busca primeiro!")
        }
    }

    $scope.gerarPlanilhaReboques = function () {
        if($scope.filtrado){
            alasql('SELECT * INTO XLSX("documentos-validade-reboques.xlsx",{headers:true}) FROM ?', [$scope.dadosReboques]);
    
        }
        else{
            myFunctions.showAlert("É necessário realizar a busca primeiro!")
        }
    }

    $scope.gerarPlanilhaMotoristas = function () {
        if($scope.filtrado){
            alasql('SELECT * INTO XLSX("documentos-validade-motoristas.xlsx",{headers:true}) FROM ?', [$scope.dadosMotoristas]);
    
        }else{
            myFunctions.showAlert("É necessário realizar a busca primeiro!")
        }
    }

    $scope.gerarPdf = function () {

        if($scope.filtrado){

            var doc = new jsPDF({orientation: "landscape"});
            var totalPagesExp = '{total_pages_count_string}'

            doc.setTextColor(100);
            doc.text("Documentos com prazo de validade expirado:", 75 , 15, null, null, "center")
            doc.setFontSize(12);
            doc.autoTable({

                body: $scope.dadosPatios,
            
               
                columns: [
                    {header: 'Pátio', dataKey: 'PATIO'},
                    {header: 'Documento', dataKey: 'DOCUMENTO'},
                    {header: 'Dias', dataKey: 'DIAS'},
                    {header: 'Inicio', dataKey: 'INICIO'},
                    {header: 'Vencimento', dataKey: 'VENCIMENTO'},
                ],
                bodyStyles: {
                    margin: 10,
                    fontSize: 08,
                },
            
                margin: {top: 30},

                
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

            // 

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            }
            });

            doc.autoTable({
                body: $scope.dadosMotoristas,
               
            
            columns: [
                {header: 'CNH', dataKey: 'CNH'},
                {header: 'Motorista', dataKey: 'MOTORISTA'},
                {header: 'Validade', dataKey: 'VALIDADE'},
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            })

            doc.autoTable({
                body: $scope.dadosEmpresasReboques,
               
            
            columns: [
                {header: 'Cidade', dataKey: 'CIDADE'},
                {header: 'Empresa', dataKey: 'EMPRESA_REBOQUE'},
                {header: 'Contato', dataKey: 'CONTATO'},
                {header: 'Inicio', dataKey: 'INICIO'},
                {header: 'Validade', dataKey: 'VALIDADE'},
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            
            })
            doc.autoTable({
                body: $scope.dadosReboques,
               
            
            columns: [
                {header: 'Placa', dataKey: 'PLACA'},
                {header: 'Tipo', dataKey: 'TIPO'},
                {header: 'Veiculo', dataKey: 'VEICULO'},
                {header: 'Inicio', dataKey: 'INICIO'},
                {header: 'Vencimento', dataKey: 'VENCIMENTO'},
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            })

        
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp)
            }
            
            doc.save('documentos-validade.pdf')
        }else{
            myFunctions.showAlert("É necessário realizar a busca primeiro!")
        }
        
    }

});

