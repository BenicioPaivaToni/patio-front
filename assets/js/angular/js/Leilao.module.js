"use strict";
angular
  .module("app")
  .controller("LeilaoCtrl", function ($scope, $http, urlServidor, $filter) {
    $scope.novo = () => window.open("/#/Leilao-novo");
    $scope.limpar = () => delete $scope.dados;
    $scope.dados = {
      descricao: "",
    };
    $scope.pesquisar = async () => {
      const { data } = await $http.get(
        `${urlServidor.urlServidorChatAdmin}/leiloes/listar/${$scope.dados.descricao}`
      );

      $scope.dadosList = data.map((Item) => ({
        ...Item,
        data: $filter("date")(Item.data, "dd/MM/yyyy"),
      }));
    };

    $scope.detalhe = function (ev, ID) {
      window.open(`/#/Leilao-editar/?id=${ID}`);
    };
  });
