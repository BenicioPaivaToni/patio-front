'use strict';
angular.module('app')
.controller("tiposrecebimentoNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions) {  

  $scope.dados = {
     decricao:'',
     tipo: '' ,
     prazo: '',
     idempresa: localStorage.getItem('id_empresa')

  }

  $scope.isDisabled = false

  $scope.gravar = function() {        

    $http.post(urlServidor.urlServidorChatAdmin+'/tipos-recebimento/cadastrar', $scope.dados ).then( function(response){

        if (response.data.code){
            
          myFunctions.showAlert('Erro na gravação!')

        }else{

          if ( response.data == '11000' )                 
          {
            myFunctions.showAlert('Tipo Recebimento já existe!')
          }else
          {                    

            myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
            $scope.isDisabled = true ;            

          }  
                
        }; 

    }); 

  }; 


  
    
});