	function PatioDocumentsController($window, $scope, $http, urlServidor) {
	this.$onInit = $onInit

	var baseUrl = urlServidor.urlServidorChatAdmin + '/patios/documentos'
	var patio = this.patio
	var ctrl = $scope

	ctrl.patio = patio
	ctrl.enviouForm = false
	ctrl.documents = []
	ctrl.loadDocumentsByPatio = loadDocumentsByPatio
	ctrl.deleteFile = deleteFile
	ctrl.verFile = verFile
	ctrl.editar = editar

	function $onInit() {
		loadDocumentsByPatio()
	}

	function loadDocumentsByPatio() {
		return new Promise(function () {
			var url = `${baseUrl}/${patio}`
			$http
				.get(url)
				.then(function (response) {
					const { data } = response
					ctrl.documents = data
				})
				.catch(function (error) {
					console.log(error)
				})
		})
	}

	function verFile(e,rowId,_id) {
		const selectedItem = ctrl.documents.find((item) => item.id === _id) || {}
		const arquivo = selectedItem['arquivo']
		return new Promise(function () {
			var url = `${baseUrl}/${patio}/${arquivo}`
			$http
				.get(url, {})
				.then(function (response) {
					const { data } = response
					$window.open(data.url, '_blank')
				})
				.catch(function (error) {
					console.log(error)
				})
		})
	}

	function editar(rowId) {
		// Open modal
		/*
		return new Promise(function () {
			var url = baseUrl + patio + '/' + rowId
			$http
				.put(url, {})
				.then(function (response) {
					loadDocumentsByPatio()
				})
				.catch(function (error) {
					console.log(error)
				})
		})*/
	}

	function deleteFile(rowId) {
		const selectedItem = ctrl.documents.find((item) => item.id === rowId) || {}
		const id = selectedItem['id']

		return new Promise(function () {
			var url = `${baseUrl}/${patio}/${id}`
			$http.delete(url).then(function (response) {
				ctrl.loadDocumentsByPatio()
			})
		})
	}
}

angular.module('app').component('patioDocumentos', {
	templateUrl: 'patio-documentos.html',
	bindings: {
		patio: '@'
	},
	bindToController: true,
	controller: PatioDocumentsController
})
