'use strict';
angular.module('app')
.controller("motivoApreensaoNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions) {  

  $scope.dados = {
     descricao:'',
     codigo: ''    
  }

  $scope.isDisabled = false;

  $scope.gravar = function() {        

    $http.post(urlServidor.urlServidorChatAdmin+'/ncv/incluir-motivoapreensao', $scope.dados ).then( function(response){

        if (response.data.code){
            
          myFunctions.showAlert('Erro na gravação!')

        }else{

          if ( response.data == '11000' )                 
          {
            myFunctions.showAlert('Já existe!')
          }else
          {                    

            myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
            $scope.isDisabled = true ;            

          }  
                
        }; 

    }); 

  }; 


  
    
});