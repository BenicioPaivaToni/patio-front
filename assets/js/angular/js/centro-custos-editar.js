app.controller('centroCustosEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor) {

    var urlParams = $location.search();
    $scope.dados = {
        id: urlParams.dados[0].id,
        descricao: urlParams.dados[0].descricao,
        ativo: urlParams.dados[0].ativo
    }


    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/centro-custo/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    myFunctions.showAlert('Centro de Custo já existe!')
                } else {
                    myFunctions.showAlert('Alteração executada com sucesso!')
                }

            }
            
        })
    }
})