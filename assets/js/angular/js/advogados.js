app.controller('advogadosCtrl', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.novo = function (ev, id) {
        $location.path('advogados-novo');
    };


    $scope.limpar = function (ev) {
        $scope.dados.nome = ''
        $scope.dados.ativo = ''
        $scope.dadosList = []
    };


    $scope.pesquisar = function (ev) {

        $scope.dadosList = []

        $http.put(urlServidor.urlServidorChatAdmin + '/advogados/listar', $scope.dados).then(function (response) {

            let dados = response.data

            if (dados.length === 0) {

                myFunctions.showAlert('Pesquisa não encontrou registros! Verifique!')

            }else{

               $scope.dadosList = dados 
            }

        })
    }
    


    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        $location.path('advogados-editar').search({ dados })
    }



    $scope.gerarPlanilha = function () {

        if ($scope.dadosList){

            alasql('SELECT * INTO XLSX("advogados.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);

        }else{

            myFunctions.showAlert("Sem dados para gerar Planilha!") 

        }
    }



    $scope.gerarPdf = function () {
        
        if ($scope.dadosList){
        
            var doc = new jsPDF({orientation: "landscape"});
            var totalPagesExp = '{total_pages_count_string}'

            doc.setFontSize(12);
            doc.autoTable({

                body: $scope.dadosList,
                columns: [
                    {header: '*', dataKey: 'id'},
                    {header: 'Nome', dataKey: 'nome'},
                    {header: 'Email', dataKey: 'email'},
                    {header: 'Telefone', dataKey: 'fone'},
                    {header: 'Cidade', dataKey: 'cidade'},
                    {header: 'Estado', dataKey: 'uf'},
                    {header: 'Ativo', dataKey: 'ativo'},
                ],
                bodyStyles: {
                    margin: 10,
                    fontSize: 08,
                },
                didDrawPage: function (data) {
                    // Header
                    doc.setFontSize(18)
                    doc.setTextColor(40)
                    doc.setFontStyle('normal')

                    // Footer
                    var str = 'Pagina ' + doc.internal.getNumberOfPages()
                    // Total page number plugin only available in jspdf v1.0+
                    if (typeof doc.putTotalPages === 'function') {
                        str = str + ' de ' + totalPagesExp
                    }

                    doc.setFontSize(10)

                    // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                    var pageSize = doc.internal.pageSize
                    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                    doc.text(str, data.settings.margin.left, pageHeight - 10)
                },
                margin: {top: 30},
            });
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp)
            }

            doc.save('advogados.pdf')

        }else{

            myFunctions.showAlert("Sem dados para gerar PDF!") 

        }
    };


});