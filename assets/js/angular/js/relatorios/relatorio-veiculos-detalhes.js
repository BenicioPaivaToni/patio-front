app.controller('relatorioVeiculosDetalhesCtrl', function ($scope, $location, $http, urlServidor, myFunctions) {

  $scope.dados = {
    data_inicio: '',
    data_final: '',
    marca_modelo: '',
    ano: '',
    cor: '',
    acessorios: '',
    fotos: '',
    patio: '',
    id_autoridade: '',
    tipo_veiculo: '',
    status: '',
    estado_situacao: '',
    id_advogado: ''
        
  }


  $scope.detalhe = function (ev, id) {
    window.open("/#/ncv-editar?id " + id, "_blank");
  }


  $scope.limpar = function (ev) {
    $scope.dados.marca_modelo = ''
    $scope.dados.ano = ''
    $scope.dados.cor = ''
    $scope.dados.acessorios = ''
    $scope.dados.patio = ''
    $scope.dados.fotos = ''
    $scope.dadosList = ''
    $scope.dados.id_autoridade = ''
    $scope.dados.data_inicio = ''
    $scope.dados.data_final = ''
    $scope.dados.tipo_veiculo = ''
    $scope.dados.status = ''
    $scope.dados.estado_situacao = ''
    $scope.dados.situacao = ''
    $scope.dados.id_advogado = ''
        
  }
    
  buscaTipoVeiculos().then((data) => {
    $scope.tiposVeiculos = data
  })

 
  buscaAutoridades().then( function(result){
    $scope.autoridades = result
      
  })


  buscaAdvogados().then( function(result){
    $scope.advogados = result
          
  })


  buscaPatios().then( function(result){
    $scope.patios = result.filter((item) => item.status == 1) 
  })
     

  $scope.pesquisar = function (ev) {
    
    if ($scope.dados.patio) {

      $scope.dadosList = null
                 
      $http.get(urlServidor.urlServidorChatAdmin + '/relatorios/veiculos-detalhes', { params: $scope.dados }).then(function (response) {

        let dados = response.data 
      
        if (dados.length !== 0) {

          $scope.dadosList = dados 
                            
        }else{
          myFunctions.showAlert('Pesquisa não encontrou registros! Revise!')
        }
    
      });
                                
        
    }else{
      myFunctions.showAlert('Nenhum Pátio foi selecionado para pesquisa! Verifique!')
    }   
            
  }
    


  function buscaTipoVeiculos() {
    return new Promise(function (resolve, reject) {
      $http.get(urlServidor.urlServidorChatAdmin + '/tipos_veiculo/listar', {params: {idEmpresa: localStorage.getItem('id_empresa')}}).then(function (response) {
             
        let dados = response.data
        resolve(dados)
      })
    })
  }

    

  function buscaAutoridades() {
    return new Promise(function (resolve, reject) {
      $http.put(urlServidor.urlServidorChatAdmin + '/autoridades/listar', {ativo: '1'}).then(function (response) {
             
        let dados = response.data
        resolve(dados)
      })
    })
  }


  function buscaAdvogados() {
    return new Promise(function (resolve, reject) {
      $http.put(urlServidor.urlServidorChatAdmin + '/advogados/listar', {ativo: '1'}).then(function (response) {
             
        let dados = response.data
        resolve(dados)
      })
    })
  }


    
  function buscaPatios() {
    return new Promise(function (resolve, reject) {
      $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') }).then(function (response) {
             
        let dados = response.data
        resolve(dados)
      })
    })
  }



  $scope.gerarPlanilha = () => {
    if ($scope.dadosList) {
  
      const excelAjustes = $scope.dadosList.map((item) => ({
        patio: item.PATIO,
        ncv: item.NCV,
        placa: item.placa,
        tipo_veiculo: item.tipo_veiculo,
        data_apreensao: item.data_apreensao, 
        marca_modelo: item.marca_modelo,
        cor: item.cor,
        ano: item.ano,        
      
      }))
  
      alasql(
        'SELECT * INTO XLSX("relatorio-veiculos-detalhes.xlsx",{headers:true}) FROM ?',[excelAjustes]
      )
  
    }else{    
      myFunctions.showAlert('Sem dados para gerar planilha! Verifique!')
    }
  }
  


  $scope.gerarPDF = function () {
    
    if ($scope.dadosList) {

    const PDFAjustes = $scope.dadosList.map((item) => ({
      patio: item.PATIO,
      ncv: item.NCV,
      placa: item.placa,
      marca_modelo: item.marca_modelo,
      tipo_veiculo: item.tipo_veiculo,
      ano: item.ano,
      cor: item.cor,
      data_apreensao: item.data_apreensao          
           
    }))

    var doc = new jsPDF({ orientation: "landscape" });

    var totalPagesExp = "{total_pages_count_string}";

    doc.setFontSize(10);

    doc.autoTable({
      columnStyles: {
        vencimento: { halign: "left" },
      },

      body: PDFAjustes,
      columns: [
        { header: "Patio", dataKey: "patio" },
        { header: "NCV", dataKey: "ncv" },
        { header: "Placa", dataKey: "placa" },
        { header: "Marca/Modelo", dataKey: "marca_modelo" },
        { header: "Tipo", dataKey: "tipo_veiculo" },
        { header: "Ano", dataKey: "ano" },
        { header: "Cor", dataKey: "cor" },
        { header: "Data Apreensão", dataKey: "data_apreensao" },  

      ],
          
      bodyStyles: {
        margin: 10,
        fontSize: 08,
      },

      didDrawPage: function (data) {
        // Header
        doc.setFontSize(18);
        doc.setTextColor(40);
        doc.setFontStyle("normal");

        doc.text(
          "Relatório Veículos Detalhes",
          data.settings.margin.left + 15, 22
        );

        // Footer
        var str = "Pagina " + doc.internal.getNumberOfPages();
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === "function") {
          str = str + " de " + totalPagesExp;
          var totalpaginas = totalPagesExp;
        }

        doc.setFontSize(10);

        // jsPDF 1.4+ uses getWidth, <1.4 uses .width
        var pageSize = doc.internal.pageSize;
        var pageHeight = pageSize.height
          ? pageSize.height
          : pageSize.getHeight();

        doc.text(
          str + "                        ",
          data.settings.margin.left,
          pageHeight - 10
        );

        },
      margin: { top: 30 },
    });
        
    if (typeof doc.putTotalPages === "function") {
      doc.putTotalPages(totalPagesExp);
    }

    doc.save("relatorio-veiculos-detalhes.pdf")  
        
    }else{

      myFunctions.showAlert('Sem dados para gerar PDF! Verifique!')

    }
  }


})