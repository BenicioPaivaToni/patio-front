angular
  .module("app")
  .controller(
    "relatorioTransferenciasVeiculosCtrl",
    function ($scope, $http, myFunctions, urlServidor, $mdDialog) {
      $scope.dados = {
        id_patio: "",
        data_inicio: new Date(),
        data_final: new Date(),
      };

      $scope.pesquisar = {
        buscaTransferencias: async () => {
          const response = await $http.get(
            `${urlServidor.urlServidorChatAdmin}/transferencias-veiculos/listar-transferencias-periodo`,
            {
              params: {
                data_inicio: $scope.dados.data_inicio,
                data_final: $scope.dados.data_final,
                patio_origem: $scope.dados.patio_origem,
                patio_destino: $scope.dados.patio_destino,
              },
            }
          );
          const { data } = response;
          console.log(data)
          $scope.dadosList = data;
        },

        limpar: (ev) => {
          ($scope.dados = {
            id_patio: "",
            patio_destino: "",
            data_inicio: new Date(),
            data_final: new Date(),
          }),
            ($scope.dadosList = "");
        },
      };

      $http
        .put(
          urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
          {
            idUsuario: localStorage.getItem("id_usuario"),
            idEmpresa: localStorage.getItem("id_empresa"),
          }
        )
        .then(({ data }) => {
          $scope.patios = data.filter((item) => {
            return item.status == !0;
          });
        });

      $scope.gerarPlanilha = () => {
        const excelAjustes = $scope.dadosList.map((item) => ({
          patio_origem: item.patio,
          patio_destino: item.patio_destino,
          ncv: item.id,
          placa: item.placa,
          marca_modelo: item.marca_modelo,
          cor: item.cor,
          ano: item.ano,
          tipo_veiculo: item.tipo_veiculo,
          data_transferencia: item.data_transferencia,
          data_conferencia: item.data_conferencia,
          data_apreensao: item.data_apreensao,
          hora_apreensao: item.hora_apreensao,
          km_percorrido: item.km_percorrido,
          romaneio:item.id_romaneio
        }));

        alasql(
          'SELECT * INTO XLSX("relatorio-transferencias-veiculos.xlsx",{headers:true}) FROM ?',
          [excelAjustes]
        );
      };

      $scope.gerarPDF = function () {
        const PDFAjustes = $scope.dadosList.map((item) => ({
          patio_origem: item.patio,
          patio_destino: item.patio_destino,
          ncv: item.id,
          placa: item.placa,
          marca_modelo: item.marca_modelo,
          cor: item.cor,
          ano: item.ano,
          tipo_veiculo: item.tipo_veiculo,
          data_transferencia: item.data_transferencia,
          data_conferencia: item.data_conferencia,
          data_apreensao: item.data_apreensao,
          hora_apreensao: item.hora_apreensao,
          km_percorrido: item.km_percorrido,
          romaneio:item.id_romaneio

        }));

        var doc = new jsPDF({ orientation: "landscape" });

        var totalPagesExp = "{total_pages_count_string}";

        doc.setFontSize(10);

        doc.autoTable({
          columnStyles: {
            vencimento: { halign: "left" },
          },

          body: PDFAjustes,
          columns: [
            { header: "Patio Origem", dataKey: "patio_origem" },
            { header: "Patio Destino", dataKey: "patio_destino" },
            { header: "NCV", dataKey: "ncv" },
            { header: "Placa", dataKey: "placa" },
            { header: "Marca/Modelo", dataKey: "marca_modelo" },
            { header: "Cor", dataKey: "cor" },
            { header: "Ano", dataKey: "ano" },
            { header: "Data Transferência", dataKey: "data_transferencia" },
            { header: "Data Conferência", dataKey: "data_conferencia" },
            { header: "Data Apreensão", dataKey: "data_apreensao" },
            { header: "Hora Apreensão", dataKey: "hora_apreensao" },
            { header: "KM Percorrido", dataKey: "km_percorrido" },
          ],
          bodyStyles: {
            margin: 10,
            fontSize: 8,
          },

          didDrawPage: function (data) {
            // Header
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle("normal");

            doc.text(
              "Relatório de transferências de veículos",
              data.settings.margin.left + 15,
              22
            );

            // Footer
            var str = "Pagina " + doc.internal.getNumberOfPages();
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === "function") {
              str = str + " de " + totalPagesExp;
              var totalpaginas = totalPagesExp;
            }

            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height
              ? pageSize.height
              : pageSize.getHeight();

            doc.text(
              str + "                        ",
              data.settings.margin.left,
              pageHeight - 10
            );
          },
          margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === "function") {
          doc.putTotalPages(totalPagesExp);
        }

        doc.save("relatorio-transferencias-veiculos.pdf");
      };
    }
  );
