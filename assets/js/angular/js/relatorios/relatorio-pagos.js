angular.module('app').controller('relatorioPagosCtrl', function ($scope, $location, $http, urlServidor,myFunctions,$window) {

   
    $scope.dados = {dataInicio: new Date(), dataFinal: new Date(), id_patio: null, id_conta: null};
    $scope.mesAtual = function (ev, id) {
        let hoje = new Date()
        $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
        $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    };
    $scope.limpar = function (ev) {

        $scope.dados.dataInicio = new Date();
        $scope.dados.dataFinal = new Date();
        $scope.dados.id_patio = null;
        $scope.dados.id_conta = null;

    };

    var total_pagos = null;

    buscaContas().then(function (result) {
        $scope.contas = result;
    });


    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("relatorio-pagos.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    };

    $scope.gerarPlanilhaDiarias = function () {

        alasql('SELECT * INTO XLSX("relatorio-pagos-diarias.xlsx",{headers:true}) FROM ?', [$scope.totalDia]);
    };

    $scope.pesquisar = function (ev) {

//        $http.put(urlServidor.urlServidorChatAdmin + '/pagar/listar', $scope.dados).then(function (response) {

            $scope.dadosList = null ;
            $scope.totalDia = null;

            $scope.dados.dataInicio.setHours(00, 00,00);
            $scope.dados.dataFinal.setHours(00, 00,00);
            $http.get(urlServidor.urlServidorChatAdmin + '/pagar/lista-pagos', {params: $scope.dados}).then(function (response) {
         
                    somarTotal(response.data.totalDia).then(function (result) {
                        total_pagos = result;
                    }).finally(function () {
                        var total_geral = total_pagos.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) ;
                        $scope.totalDia = response.data.totalDia;
                        $scope.dadosList = response.data.dadosPagos;
                        $scope.totalDia.push({data_pagto: 'TOTAL GERAL: ', Total_Formatado: total_geral});
                    });
               
            });

//        });

    };

    $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', {idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa')}).then(function (response) {
        $scope.patios = response.data;
    });

    function buscaContas() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/contas/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }


    $scope.gerarPdf = function () {
        var doc = new jsPDF({orientation: "landscape"});
        var totalPagesExp = '{total_pages_count_string}'
        var strTotal = 'TOTAL GERAL '+ total_pagos.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) ;
        var periodo = 'Contas pagas no período de ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' á ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR');
        doc.text(periodo, 20, 20)
        
        doc.setFontSize(10);
        doc.autoTable({

            columnStyles: { 
                vencimento: {halign: 'left'},
              },
          
            body: $scope.dadosList,
            columns: [
                {header: 'Cadastro', dataKey: 'Cadastro'},
                {header: 'Documento', dataKey: 'documento'},
                {header: 'Fornecedor', dataKey: 'Fornecedor'},
                {header: 'Conta', dataKey: 'Conta'}, 
                {header: 'Valor', dataKey: 'valor'},     
                {header: 'Acrescimo', dataKey: 'acrescimo'},
                {header: 'Desconto', dataKey: 'desconto'},
                {header: 'Forma de Pag.', dataKey: 'Forma_Pagto'},
                {header: 'Vencimento', dataKey: 'Vencimento'},
                {header: 'Pagamento', dataKey: 'Pagamento'},
                {header: 'Pátio', dataKey: 'Patio'},   
                
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                    var totalpaginas = totalPagesExp ;
                }

               // var strTotal = 'TOTAL '+ total_pagos.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) ;

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()

                doc.text(str+'                        '+strTotal, data.settings.margin.left, pageHeight - 10)


                

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                

              
            },
            margin: {top: 30},
        });
       
        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('relatorio-pagos.pdf');
    }

    $scope.gerarPdfDiarias = function () {
        var doc = new jsPDF({orientation: "landscape"});
        var totalPagesExp = '{total_pages_count_string}'
        var strTotal = 'TOTAL GERAL '+ total_pagos.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) ;
        var periodo = 'Total de gasto diários em contas pagas no período de ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' á ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR');
        doc.text(periodo, 20, 20)
        
        doc.setFontSize(10);
        doc.autoTable({
            body: $scope.totalDia,
             
            columns: [
                {header: 'Data', dataKey: 'data_pagto'},
                {header: 'Total Diário', dataKey: 'Total_Formatado'},
            ],
            
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                    var totalpaginas = totalPagesExp ;
                }

               // var strTotal = 'TOTAL '+ total_pagos.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) ;

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()

                doc.text(str+'                        ', data.settings.margin.left, pageHeight - 10)


                

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                

              
            },
            margin: {top: 30},
           
        })
       
        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('relatorio-pagos-diarias.pdf');
    }

    function somarTotal(dados) {

        return new Promise((resolve, reject) => {

            var total = dados.reduce(function (prevVal, elem) {
                return prevVal + elem.Total;
            }, 0);
            resolve(total)

        });
    }

    $scope.cancelar = function (ev, id) {
        let dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        if (dados[0].status == 'PAGO' || dados[0].status == 'CANCELADO') {
            myFunctions.showAlert('Somente documento com status ABERTO pode ser cancelado!')
        } else {
            window.open('/#/pagar-cancelar?id '+ id,'_blank');
        }
    };
});