
angular.module('app')
.controller('relatorioCentroCusto', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.dados = {
        DataInicio: null,
        DataFinal: null,
        patios: null
    }

    let hoje = new Date()
    $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
    $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    $scope.mesAtual = function (ev, id) {
        $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
        $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    };
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("relatorio-liberacoes.xlsx",{headers:true}) FROM ?', [$scope.dadosLiberacoes]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({orientation: "landscape"});
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: {tipo_veiculo: {halign: 'center'},
                valor_guincho: {halign: 'center'},
                valor_estadia: {halign: 'center'},
                total_diarias: {halign: 'center'},
                desconto: {halign: 'center'},
                acrescimo: {halign: 'center'},
                total: {halign: 'center'},
                estadias: {halign: 'center'}},
            body: $scope.dadosLiberacoes,
            columns: [
                {header: 'NCV', dataKey: 'id'},
                {header: 'Placa', dataKey: 'placa'},
                {header: 'Tipo Veiculo', dataKey: 'tipo_veiculo'},
                {header: 'Liberação', dataKey: 'data_liberacao'},
                {header: 'Apreensão', dataKey: 'data_apreensao'},
                {header: 'Guincho', dataKey: 'valor_guincho'},
                {header: 'Diaria', dataKey: 'valor_estadia'},
                {header: 'Diarias', dataKey: 'estadias'},
                {header: 'Total Diarias', dataKey: 'total_diarias'},
                {header: 'Desconto', dataKey: 'desconto'},
                {header: 'Acrescimo', dataKey: 'acrescimo'},
                {header: 'Recebido', dataKey: 'total'},
                {header: 'Forma Pgto', dataKey: 'forma_pagto'},
                {header: 'Patio', dataKey: 'patio'},
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                doc.text('Liberações no periodo de ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22)

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: {top: 30},
        });

        doc.autoTable({
            head: [
              ["TIPO RECEBIMENTO"]
            ],
          })

        $scope.dadosPagamentos.forEach(table);

        function table(element, index, array) {
           
              const e = element ; 
              doc.autoTable({
                body: [
                  [e.descricao+' : R$ '+e.valor]
                ],
                theme: 'grid'
              })
        }


        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('liberacoes.pdf')
    }


    $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', {idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa')}).then(function (response) {
        $scope.patios = response.data;
    });
    $http.put(urlServidor.urlServidorChatAdmin + '/tipos-recebimento/listar').then(function (response) {
        $scope.formasPagto = response.data;
    });
    $scope.pesquisar = function (param1) {

        $http.get(urlServidor.urlServidorChatAdmin + '/ncv/relatorio-liberacoes', {params: $scope.dados}).then(function (response) {
            $scope.dadosLiberacoes = response.data[0];
            $scope.dadosPagamentos = response.data[1];
        });
    };



    $scope.detalhe = function (ev, id) {


        
        window.open('/#/ncv-editar?id '+ id,'_blank');
    };
});
