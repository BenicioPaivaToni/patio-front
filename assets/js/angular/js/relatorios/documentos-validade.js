angular.module('app').controller('documentosValidadeCtrl', function ($scope, $http, urlServidor, $scope, $http, urlServidor, myFunctions) {

    $scope.dados = {
        vencidoInicio:1,
        vencidoFinal:1,
        patio:''
    };

    $scope.showMessageError = false
    $scope.disableButton = true
    $scope.showMessageErrorPatio = true

    const getPatios =async  () =>{
        const response = await $http.put(`${urlServidor.urlServidorChatAdmin}/patios/listar-patiosusuario`,{ idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') })
        $scope.patios = response.data
    }

    getPatios()

    
    $scope.Rules = {
        // Vencimentos não podem ser 0, e números negativos 
        vencimentoCannotbeZero () {
           $scope.dados.vencidoInicio === 0 ? $scope.dados.vencidoInicio = 1: $scope.dados.vencidoInicio = $scope.dados.vencidoInicio 
           $scope.dados.vencidoFinal === 0 ? $scope.dados.vencidoFinal = 1: $scope.dados.vencidoFinal = $scope.dados.vencidoFinal 
            this.CannotBeGreater()
        },

        //Vencimento Final não pode ser menor que o inicial
        CannotBeGreater() {
            $scope.dados.vencidoFinal <  $scope.dados.vencidoInicio ? $scope.showMessageError = true : $scope.showMessageError = false
       //Se for vamos igualar os valores
 //            $scope.showMessageError === true ? $scope.dados.vencidoFinal = $scope.dados.vencidoInicio: false
       
        }, 
        
        patioCannotBeNull(){
            if(  $scope.dados.patio == ''){
                $scope.showMessageErrorPatio= true

            }else{
                $scope.showMessageErrorPatio= false
                $scope.disableButton = false
            }
        }
        
    }
   
   
    $scope.filtrado = false;

    $scope.pesquisar = async (dados) => {
        var patios
        if (dados.patio.length == 1) {
            patios = [...dados.patio, ...dados.patio]
        } else {
            patios = dados.patio
        }

        if( $scope.dadosPatios) delete  $scope.dadosPatios
        if(  $scope.dadosMotoristas) delete   $scope.dadosMotoristas
        if($scope.dadosReboques ) delete $scope.dadosReboques 
        if( $scope.dadosEmpresasReboques) delete  $scope.dadosEmpresasReboques

        const response = await $http.get(urlServidor.urlServidorChatAdmin + '/relatorios/relatorio-validade',
            { params: { de: dados.vencidoInicio, ate: dados.vencidoFinal, patio: patios } })

    

        $scope.dadosPatios = response.data.Patios

        $scope.dadosMotoristas = response.data.Motoristas

        $scope.dadosReboques = response.data.Reboques
        $scope.dadosEmpresasReboques =response.data.EmpresasReboques
        
    }


    
    $scope.gerarPlanilhaPatios = function () {
        if($scope.filtrado){
            alasql('SELECT * INTO XLSX("documentos-validade-patios.xlsx",{headers:true}) FROM ?', [$scope.dadosPatios]);
    
        }else{
            myFunctions.showAlert("É necessário realizar a busca primeiro!")
        }
    }
    $scope.gerarPlanilhaEmpresas = function () {
        if($scope.filtrado){
            alasql('SELECT * INTO XLSX("documentos-validade-empresas.xlsx",{headers:true}) FROM ?', [$scope.dadosEmpresasReboques]); 
    
        }else{
            myFunctions.showAlert("É necessário realizar a busca primeiro!")
        }
    }

    $scope.gerarPlanilhaReboques = function () {
        if($scope.filtrado){
            alasql('SELECT * INTO XLSX("documentos-validade-reboques.xlsx",{headers:true}) FROM ?', [$scope.dadosReboques]);
    
        }
        else{
            myFunctions.showAlert("É necessário realizar a busca primeiro!")
        }
    }

    $scope.gerarPlanilhaMotoristas = function () {
        if($scope.filtrado){
            alasql('SELECT * INTO XLSX("documentos-validade-motoristas.xlsx",{headers:true}) FROM ?', [$scope.dadosMotoristas]);
    
        }else{
            myFunctions.showAlert("É necessário realizar a busca primeiro!")
        }
    }

    $scope.gerarPdf = function () {

        if($scope.filtrado){

            var doc = new jsPDF({orientation: "landscape"});
            var totalPagesExp = '{total_pages_count_string}'

            doc.setTextColor(100);
            doc.text("Documentos com prazo de validade expirado:", 75 , 15, null, null, "center")
            doc.setFontSize(12);
            doc.autoTable({

                body: $scope.dadosPatios,
            
               
                columns: [
                    {header: 'Pátio', dataKey: 'PATIO'},
                    {header: 'Documento', dataKey: 'DOCUMENTO'},
                    {header: 'Dias', dataKey: 'DIAS'},
                    {header: 'Inicio', dataKey: 'INICIO'},
                    {header: 'Vencimento', dataKey: 'VENCIMENTO'},
                ],
                bodyStyles: {
                    margin: 10,
                    fontSize: 08,
                },
            
                margin: {top: 30},

                
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

            // 

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            }
            });

            doc.autoTable({
                body: $scope.dadosMotoristas,
               
            
            columns: [
                {header: 'Pátio', dataKey: 'PATIO'},

                {header: 'CNH', dataKey: 'CNH'},
                {header: 'Motorista', dataKey: 'MOTORISTA'},
                {header: 'Validade', dataKey: 'VALIDADE'},
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            })

            doc.autoTable({
                body: $scope.dadosEmpresasReboques,
               
            
            columns: [
                {header: 'Pátio', dataKey: 'PATIO'},

                {header: 'Cidade', dataKey: 'CIDADE'},
                {header: 'Empresa', dataKey: 'EMPRESA_REBOQUE'},
                {header: 'Contato', dataKey: 'CONTATO'},
                {header: 'Inicio', dataKey: 'INICIO'},
                {header: 'Validade', dataKey: 'VALIDADE'},
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            
            })
            doc.autoTable({
                body: $scope.dadosReboques,
               
            
            columns: [
                {header: 'Pátio', dataKey: 'PATIO'},

                {header: 'Placa', dataKey: 'PLACA'},
                {header: 'Tipo', dataKey: 'TIPO'},
                {header: 'Veiculo', dataKey: 'VEICULO'},
                {header: 'Inicio', dataKey: 'INICIO'},
                {header: 'Vencimento', dataKey: 'VENCIMENTO'},
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            })

        
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp)
            }
            
            doc.save('documentos-validade.pdf')
        }else{
            myFunctions.showAlert("É necessário realizar a busca primeiro!")
        }
        
    }

});