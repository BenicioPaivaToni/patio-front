angular.module('app').controller("ncvIncompletoCtrl", function ($scope, $http, urlServidor, $filter, myFunctions) {

    $scope.dados = {
        dataInicio: new Date(),
        dataFinal: new Date(),
        patio:''
    };


    $scope.Methods = {
        
        async getDados() {

            var patios
            if ($scope.dados.patio.length == 1) {
                patios = [...$scope.dados.patio, ...$scope.dados.patio]
            } else {
                 patios = $scope.dados.patio
            }
    

            try {
                if(typeof($scope.dados.patio)=== 'object'){
                    [$scope.dados.patio]
                }
                console.log($scope.dados);

                const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/relatorios/ncvs-incompletos`, 
                { params: { dataInicio: $scope.dados.dataInicio, dataFinal: $scope.dados.dataFinal, patio: patios } })
    
                const { data } = response
               
                console.log(data);
                if(data == false){
                 throw new Error()

                }

                $scope.retornoAPI = data.map(Item => ({
                    NCV: Item.NCV,
                    PATIO: Item.PATIO,
                    QTDE_DIARIAS: Item.QTDE_DIARIAS,
                    ano: Item.ano,
                    cor: Item.cor,
                    data_apreensao: $filter('date')(Item.data_apreensao, 'dd/MM/yyyy', "+0000"),
                    marca_modelo: Item.marca_modelo,
                    placa: Item.placa,
                    tipo_veiculo: Item.tipo_veiculo,

                }))
            } catch (error) {
                console.error(error);

                myFunctions.showAlert(`Filtro não encontrou registros, verifique! ${error.statusText || 'Sem registros'}`)
            }


        },
        mesAtual() {
            let hoje = new Date()
            $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
            $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
        },
        limpar() {
            $scope.dados = {
                dataInicio: new Date(),
                dataFinal: new Date(),
                patio: null
            };

        }
    }
    $scope.PatioNull = true

    $scope.Rules = {
        //Ao selecionar data inicial a mesma já deve ser atribuída a data final
        changeDate() {
            $scope.dados.dataFinal = $scope.dados.dataInicio
        },
        //Não aceitar data final menor que data inicial
        isValidDate() {
            if ($scope.dados.dataFinal.getTime() < $scope.dados.dataInicio.getTime()) {
                $scope.showMessageError = true;
                $scope.disableButtonFilter = true
            } else {
                $scope.showMessageError = false;
                $scope.disableButtonFilter = false

            }
        },
        patioCannotBeNull() {
            //Pátio não pode ser Nulo 
            $scope.dados.patio === null? $scope.PatioNull = true : $scope.PatioNull = false



        }
    }

    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("relatorio-liberacoes-ncv-incompletos.xlsx",{headers:true}) FROM ?', [$scope.retornoAPI]);
    }
    $scope.ApiCalls = {

        async getPatios() {
            const response = await $http.put(`${urlServidor.urlServidorChatAdmin}/patios/listar-patiosusuario`, { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') })
            $scope.patios = response.data

        }
    }

    const onLoad = () => {
        $scope.ApiCalls.getPatios()
    }
    onLoad()




    $scope.gerarPdf = function () {



        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(10);
        doc.autoTable({

            columnStyles: {
                vencimento: { halign: 'left' },
            },

            body: $scope.retornoAPI,
            columns: [
                { header: 'NCV', dataKey: 'NCV' },
                { header: 'Patio', dataKey: 'PATIO' },
                { header: 'Diarias', dataKey: 'QTDE_DIARIAS' },
                { header: 'Ano', dataKey: 'ano' },
                { header: 'Cor', dataKey: 'cor' },
                { header: 'Data Apreensão', dataKey: 'data_apreensao' },
                { header: 'Marca/Modelo.', dataKey: 'marca_modelo' },
                { header: 'Placa', dataKey: 'placa' },
                { header: 'Tipo Veiculo', dataKey: 'tipo_veiculo' },
            ],






            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                doc.text('NCVS Incompletos de ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22);

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                    var totalpaginas = totalPagesExp;
                }

                var strTotal = `Total de ${$scope.retornoAPI.length} Veículos incompletos`

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()

                doc.text(str + '                        ' + strTotal, data.settings.margin.left, pageHeight - 10)
            },
            margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('relatorio-ncvs-incompletos.pdf');
    }






})