
angular.module('app').controller('relatorioConferentes', function ($scope, $http, urlServidor, $mdDialog, $rootScope, $scope, $location, $http, urlServidor, myFunctions, $mdDialog, estados, viaCep, Upload, S3UploadService, ncvFac, urlImagens,buckets) {

    $scope.dados = {
        DataInicio: null,
        DataFinal: null,
        patios: null
    }

    let _id_patio = 0 ;

    let hoje = new Date()
    $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
    $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    $scope.mesAtual = function (ev, id) {
        $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
        $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    };

    $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', {idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa')}).then(function (response) {
        $scope.patios = response.data;
    });
    
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("relatorio-conferente.xlsx",{headers:true}) FROM ?', [$scope.dadosConferentes]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({orientation: "landscape"});
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            body: $scope.dadosPatios,
            columns: [
                {header: 'Patio', dataKey: 'nome'},
                {header: 'Data', dataKey: 'DATA'},
                {header: 'Qtde', dataKey: 'QTDE'},
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                doc.text('Liberações no periodo de ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22)

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: {top: 30},
        });

        doc.autoTable({
            head: [
              ["RESUMO POR CONFERENTE"]
            ],
          })

        $scope.dadosConferentes.forEach(table);

        function table(element, index, array) {
           
              const e = element ; 
              doc.autoTable({
                body: [
                  [e.nome+' - '+e.DATA+' - '+e.QTDE]
                ],
                theme: 'grid'
              })
        }

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('conferentes.pdf')
    }


    $scope.detalhesPatio = function(Selecionado){

        let dataInicio = $scope.dados.dataInicio.toLocaleDateString() ;
        let dataFinal = $scope.dados.dataFinal.toLocaleDateString();

        if(Selecionado.DATA){
            dataInicio = Selecionado.DATA;
            dataFinal = Selecionado.DATA;    
        } 

        let idPatio   = Selecionado.id;
         
       $scope.listarDados = null ;

        $mdDialog.show({
            controller: function ($scope, $mdDialog, $mdToast) {

                $http.get(urlServidor.urlServidorChatAdmin + '/conferente/patio', {params:{id: idPatio  , datainicio: dataInicio, datafinal: dataFinal}}).then(function (response) {
                    $scope.listarDados = response.data
                    
                });

               $scope.editar = function (ev, id) {
                    window.open('/#/ncv-editar?id '+ id,'_blank');
                };

                $scope.cancel = function(){
                    $mdDialog.cancel();
                };
                
            },
            templateUrl: 'detalhes-conferentes.html',
           
            preserveScope: true,
            
        }).then(function (answer) {
            let a = answer;
            
        }, function () {
            $scope.statusdialog = 'You cancelled the dialog.';
        });
    }
    
    $scope.update = function(idpatio){

        _id_patio = idpatio ;

    }

    $scope.detalhesConferentes = function(Selecionado){

        let dataInicio = $scope.dados.dataInicio.toLocaleDateString() ;
        let dataFinal = $scope.dados.dataFinal.toLocaleDateString();

        if(Selecionado.DATA){
            dataInicio = Selecionado.DATA;
            dataFinal = Selecionado.DATA;    
        } 

        let idConferente = Selecionado.id;

        $scope.listarDados = null ; 

        $mdDialog.show({
            controller: function ($scope, $mdDialog, $mdToast) {

                $http.get(urlServidor.urlServidorChatAdmin + '/conferente/usuario', {params:{id: idConferente, datainicio: dataInicio, datafinal: dataFinal, id_patio: _id_patio }}).then(function (response) {
                    $scope.listarDados = response.data
                    
                });

               $scope.editar = function (ev, id) {
                    window.open('/#/ncv-editar?id '+ id,'_blank');
                };

                $scope.cancel = function(){
                    $mdDialog.cancel();
                };
                
            },
            templateUrl: 'detalhes-conferentes.html',
           
            preserveScope: true,
            
        }).then(function (answer) {
            let a = answer;
            
        }, function () {
            $scope.statusdialog = 'You cancelled the dialog.';
        });
    }
    
   


    $scope.pesquisar = function (param1) {
        $scope.dadosPatios = null ;
        $scope.dadosConferentes = null;
        $http.get(urlServidor.urlServidorChatAdmin + '/conferente/listar', {params: $scope.dados}).then(function (response) {
            $scope.dadosConferentes = response.data.resumoConferentes;
            $scope.dadosPatios = response.data.resumoPatios;

            $scope.totalDia =  alasql('SELECT DATA,sum(QTDE) as QTDE FROM ? WHERE DATA IS NOT NULL GROUP BY DATA', [$scope.dadosPatios]);

           

        });
    };



});

