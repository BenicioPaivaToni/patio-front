angular
  .module('app').controller('gerencialMediaApreensoesCtrl', function($scope,$http,urlServidor,myFunctions,$mdDialog){

  var dados = [] ;

  $scope.dados = {
    dataInicio: null,
    dataFinal: new Date(),
    apreendidos_ate : new Date(),
    patio: null,
    id_grupo_patios: null,
  };

  

  $scope.ShowMessage = false;

  $scope.limpar = function (ev) {
    $scope.dados.dataInicio = new Date();
    $scope.dados.dataFinal = new Date();
    $scope.dados.apreendidos_ate = new Date();
    $scope.dados.patio = null;
    $scope.dados.id_grupo_patios = null;
  };

  const buscaPatiosGrupo = async () => {
    const response = await $http.get(
      `${urlServidor.urlServidorChatAdmin}/grupos-patios/listar`
    );
    const { data } = response;
    return data.filter((item) => item.ativo == 1);
  };

  buscaPatiosGrupo().then((Response) => ($scope.patiosGrupo = Response));

  $http
  .put(
    urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
    {
      idUsuario: localStorage.getItem("id_usuario"),
      idEmpresa: localStorage.getItem("id_empresa"),
    }
  )
  .then( ({ data })  => {
    $scope.patios = data.filter((item) => item.status == 1);
  });

$scope.gridInstance = {} 
$scope.gridInstanceEstoque = {} 

$scope.gerarExcel = function () {
  const dadosApreensoes = dados.map((item) => ({
    patio: item.Nome_Patio,
    mes: item.Mes,
    ano: item.Ano,
    contrato: item.Contrato,
    ponto_equilibrio : item.Ponto_Equilibrio,    
    apreensoes: item.Qtde,
    meta: item.Meta_Mes,
    PercMeta: item.Perc,
    liberacoes: item.Liberacoes,
    valor_liberacoes: item.Total_Liberacoes,
    despesas_patios: item.Despesas_Patios
  }));


  dadosApreensoes.sort((a, b) => {
    if (a.Patio > b.Patio) {
      return 1;
    }
    if (a.Patio < b.Patio) {
      return -1;
    }
    return 0;
  });


  alasql(
    'SELECT * INTO XLSX("gerencial-media.xlsx",{headers:true}) FROM ?',
    [dadosApreensoes]
  );
};



$scope.MostaFrame = function (selectedItems,apreendidos_ate) {  

  $mdDialog.show({
    controller: function ($scope, $mdDialog, $mdToast) {

        $scope.detalhes = async () => {

          $scope.listarTransf = null 
          $scope.listarDados = null
          $scope.gridOptionsDetalhes = null ; 
          $scope.gridOptionsDetalhesTransf = null ; 
          $scope.qtdeRegistros = 0 

          if ( $scope.dados.detalhes == 'TRANSFERIDOS' ){
            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/analitico/gerencial-estoque-transferidos', { params: {patio: selectedItems.selectedRowsData[0].Patio , apreendidos_ate : apreendidos_ate } })
            $scope.qtdeRegistros = response.data.length 
            $scope.listarTransf = response.data  

            $("#gridContainer-transferidos").dxDataGrid({
              dataSource: $scope.listarTransf
            });
  
            if (!response.data.length > 0) {
                $scope.ShowSpinnerLoad = false
                myFunctions.showAlert('Filtro não encontrou registros! Revise parâmetros da pesquisa!')
                return
            } else {
                $("#gridContainer-transferidos").dxDataGrid("instance").refresh();
                $("#gridContainer-detalhes").dxDataGrid("instance").refresh();
                $scope.data_ate_apreensao = $scope.dados.apreendidos_ate.getDate()+'/'+($scope.dados.apreendidos_ate.getMonth()+1)+'/'+$scope.dados.apreendidos_ate.getFullYear()
                atualizaGridDetalhesTransf() ;       
            }
  
          } else {     

          if ( $scope.dados.detalhes == 'CANCELADOS' ){
            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/analitico/gerencial-estoque-cancelados', { params: {patio: selectedItems.selectedRowsData[0].Patio , apreendidos_ate : apreendidos_ate } })
            $scope.qtdeRegistros = response.data.length 
            $scope.listarDados = response.data
          } else if ( $scope.dados.detalhes == 'INVALIDADOS' ){
            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/analitico/gerencial-estoque-invalidado', { params: {patio: selectedItems.selectedRowsData[0].Patio , apreendidos_ate : apreendidos_ate } })
            $scope.qtdeRegistros = response.data.length 
            $scope.listarDados = response.data  
          } else if ( $scope.dados.detalhes == 'SALDO_PATIO' ){
            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/analitico/gerencial-estoque-saldopatio', { params: {patio: selectedItems.selectedRowsData[0].Patio , apreendidos_ate : apreendidos_ate } })
            $scope.qtdeRegistros = response.data.length 
            $scope.listarDados = response.data  
          } else if ( $scope.dados.detalhes == 'JUDICIAL' ){
            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/analitico/gerencial-estoque-judicial', { params: {patio: selectedItems.selectedRowsData[0].Patio , apreendidos_ate : apreendidos_ate } })
            $scope.qtdeRegistros = response.data.length 
            $scope.listarDados = response.data  
          } else if ( $scope.dados.detalhes == 'RENAJUD' ){
            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/analitico/gerencial-estoque-renajud', { params: {patio: selectedItems.selectedRowsData[0].Patio , apreendidos_ate : apreendidos_ate } })
            $scope.qtdeRegistros = response.data.length 
            $scope.listarDados = response.data  
          } else if ( $scope.dados.detalhes == 'PREPARACAO' ){
            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/analitico/gerencial-estoque-preparacao', { params: {patio: selectedItems.selectedRowsData[0].Patio , apreendidos_ate : apreendidos_ate } })
            $scope.qtdeRegistros = response.data.length 
            $scope.listarDados = response.data  
          } else if ( $scope.dados.detalhes == 'LEILOADO' ){
            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/analitico/gerencial-estoque-leiloado', { params: {patio: selectedItems.selectedRowsData[0].Patio , apreendidos_ate : apreendidos_ate } })
            $scope.qtdeRegistros = response.data.length 
            $scope.listarDados = response.data  
          } else if ( $scope.dados.detalhes == 'OFICIO' ){
            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/analitico/gerencial-estoque-oficio', { params: {patio: selectedItems.selectedRowsData[0].Patio , apreendidos_ate : apreendidos_ate } })
            $scope.qtdeRegistros = response.data.length 
            $scope.listarDados = response.data  
          } else if ( $scope.dados.detalhes == 'SALDO_LEILAO' ){            
            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/analitico/gerencial-estoque-saldoleilao', { params: {patio: selectedItems.selectedRowsData[0].Patio , apreendidos_ate : apreendidos_ate } })
            $scope.qtdeRegistros = response.data.length 
            $scope.listarDados = response.data  
          } else if ( $scope.dados.detalhes == 'NO_PATIO' ){            
            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/analitico/gerencial-estoque-nopatio', { params: {patio: selectedItems.selectedRowsData[0].Patio , apreendidos_ate : apreendidos_ate } })
            $scope.qtdeRegistros = response.data.length 
            $scope.listarDados = response.data              
          }

          $("#gridContainer-detalhes").dxDataGrid({
            dataSource: $scope.listarDados
          });

          if (!$scope.qtdeRegistros > 0 ) {
              $scope.ShowSpinnerLoad = false
              myFunctions.showAlert('Filtro não encontrou registros! Revise parâmetros da pesquisa!')
              return
          } else {
              $("#gridContainer-detalhes").dxDataGrid("instance").refresh();
              $("#gridContainer-transferidos").dxDataGrid("instance").refresh();
              $scope.data_ate_apreensao = $scope.dados.apreendidos_ate.getDate()+'/'+($scope.dados.apreendidos_ate.getMonth()+1)+'/'+$scope.dados.apreendidos_ate.getFullYear()
              atualizaGridDetalhes() ;           
          }
           
        }
        
        }  
      

       $scope.editar = function (ev, id) {
            window.open('/#/ncv-editar?id '+ id,'_blank');
        };

        $scope.cancel = function(){
            $mdDialog.cancel();
        };
        
        $scope.excelDados = function(){
          
            alasql(
              'SELECT * INTO XLSX("gerencial-media-detalhess.xlsx",{headers:true}) FROM ?',
              [ ( $scope.listarDados ? $scope.listarDados : $scope.listarTransf )  ]
            );
    
        }

        $scope.pdfDados = function () {

              var doc = new jsPDF({ orientation: "landscape" });
              var totalPagesExp = "{total_pages_count_string}";
    
              doc.setFontSize(12);
              doc.autoTable({
                columnStyles: { tipo_veiculo: { halign: "left" } },
                body: ( $scope.listarDados ? $scope.listarDados : $scope.listarTransf ) ,
                columns: [
                  { header: "Patio", dataKey: "Patio_Origem" },
                  { header: "Patio Destino", dataKey: "Patio_Destino" },
                  { header: "NCV", dataKey: "NCV" },
                  { header: "Apreensão", dataKey: "data_apreensao" },
                  { header: "Placa", dataKey: "placa" },
                  { header: "Marca/Modelo", dataKey: "marca_modelo" },
                  { header: "Ano", dataKey: "ano" },
                  { header: "Cor", dataKey: "cor" },
                ],
                bodyStyles: {
                  margin: 10,
                  fontSize: 08,
                },
                didDrawPage: function (data) {
                  // Header
                  doc.setFontSize(18);
                  doc.setTextColor(40);
                  doc.setFontStyle("normal");
    
                  doc.text(
                    "Lista de " +
                    $scope.dados.detalhes ,data.settings.margin.left + 15,
                22
                  );
    
                  // Footer
                  var str = "Pagina " + doc.internal.getNumberOfPages();
                  // Total page number plugin only available in jspdf v1.0+
                  if (typeof doc.putTotalPages === "function") {
                    str = str + " de " + totalPagesExp;
                  }
    
                  doc.setFontSize(10);
    
                  // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                  var pageSize = doc.internal.pageSize;
                  var pageHeight = pageSize.height
                    ? pageSize.height
                    : pageSize.getHeight();
                  doc.text(str, data.settings.margin.left, pageHeight - 10);
                },
                margin: { top: 30 },
              });
              if (typeof doc.putTotalPages === "function") {
                doc.putTotalPages(totalPagesExp);
              }
    
              doc.save("gerencial-media-resumos.pdf");
            
          };          

    },
    templateUrl: 'media-saldo.html',
   
    preserveScope: true,
    
  }).then(function (answer) {
      let a = answer;
      
  }, function () {
      $scope.statusdialog = 'You cancelled the dialog.';
  });

};

function atualizaGrid() {
 
  if( localStorage.getItem('tipo_usuario') == 'admin' )

    $scope.gridOptions = {
  //    dataSource: dados,
      keyExpr: 'ID',
      showBorders: true,
      showRowLines: true,
      rowAlternationEnabled: true,
      loadPanel: {
        enabled: true,
      },
      searchPanel: {
        visible: true,
        width: 240,
        placeholder: 'Pesquisar...',
      },

      export: {
        enabled: true,
        allowExportSelectedData: false,
      },
      onExporting(e) {
        const workbook = new ExcelJS.Workbook();
        const worksheet = workbook.addWorksheet('gerencial-media');

        DevExpress.excelExporter.exportDataGrid({
          component: e.component,
          worksheet,
          autoFilterEnabled: true,
        }).then(() => {
          workbook.xlsx.writeBuffer().then((buffer) => {
            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'gerencial-media.xlsx');
          });
        });
        e.cancel = true;
      },

      columns: [{
        dataField: 'Mes',
        width: 80,
        caption: 'Mês/Ano'
      },
      {
        dataField: 'Contrato',
        width: 90,
        caption: 'Contrato'
      },
      {
        dataField: 'Ponto_Equilibrio',
        width: 90,
        caption: 'Ponto EQ'
      },{
        dataField: 'Qtde',
        width: 110,
        caption: 'Apreensões'
      }, {
        dataField: 'Meta_Mes',
        width: 80,
        alignment: 'center',
        caption: 'Meta'
      },
    
      {
        dataField: 'Perc_meta_PEQ',
        width: 120,
        alignment: 'center',
        caption: '%P.EQ/Meta',
      },{
        dataField: 'Perc_meta_apreensao',
        width: 120,
        alignment: 'center',
        caption: '%Meta/Apreensoes',
      },
      {
        dataField: 'Liberacoes',
        width: 110,
        caption: 'Liberações'

      },
      {
        dataField: 'Total_Liberacoes',
        width: 150,
        caption: 'Total Liberações',
        dataType: 'number'
      },{
        dataField: 'Despesas_Patios',
        width: 150,
        caption: 'Despesas Patios',
        dataType: 'number'
      },
      {
        caption: 'Pátio',
        dataField: 'Nome_Patio',
        groupIndex: 0,

      }],
      sortByGroupSummaryInfo: [{
        summaryItem: 'count',
      }],
      summary: {
        groupItems: [{
          column: 'Qtde',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        },{
          column: 'Qtde',
          summaryType: 'avg',
          valueFormat: "#,##0",
          displayFormat: 'Media: {0}',
          showInGroupFooter: true,
        },
        {
          column: 'Liberacoes',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        },
        {
          column: 'Liberacoes',
          summaryType: 'avg',
          valueFormat: "#,##0",
          displayFormat: 'Média: {0}',  
          showInGroupFooter: true,
        },{
          column: 'Total_Liberacoes',
          summaryType: 'sum',
          valueFormat: "R$ #,##0.##",
          displayFormat: '{0}',
          showInGroupFooter: true,
        },{
          column: 'Despesas_Patios',
          summaryType: 'sum',
          valueFormat: "R$ #,##0.##",
          displayFormat: '{0}',
          showInGroupFooter: true,
        }],
      },onInitialized: function (e) {
        $scope.gridInstance = e.component;    
      }

    };

  else {


    $scope.gridOptions = {
  //    dataSource: dados,
      keyExpr: 'ID',
      showBorders: true,
      showRowLines: true,
      rowAlternationEnabled: true,
      loadPanel: {
        enabled: true,
      },
      searchPanel: {
        visible: true,
        width: 240,
        placeholder: 'Pesquisar...',
      },

      export: {
        enabled: true,
        allowExportSelectedData: false,
      },
      onExporting(e) {
        const workbook = new ExcelJS.Workbook();
        const worksheet = workbook.addWorksheet('gerencial-media');

        DevExpress.excelExporter.exportDataGrid({
          component: e.component,
          worksheet,
          autoFilterEnabled: true,
        }).then(() => {
          workbook.xlsx.writeBuffer().then((buffer) => {
            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'gerencial-media.xlsx');
          });
        });
        e.cancel = true;
      },

      columns: [{
        dataField: 'Mes',
        width: 80,
        caption: 'Mês/Ano'
      },
      {
        dataField: 'Contrato',
        width: 90,
        caption: 'Contrato'
      },
      {
        dataField: 'Ponto_Equilibrio',
        width: 90,
        caption: 'Ponto EQ'
      },{
        dataField: 'Qtde',
        width: 110,
        caption: 'Apreensões'
      }, {
        dataField: 'Meta_Mes',
        width: 80,
        alignment: 'center',
        caption: 'Meta'
      },
    
      {
        dataField: 'Perc_meta_PEQ',
        width: 120,
        alignment: 'center',
        caption: '%P.EQ/Meta',
      },{
        dataField: 'Perc_meta_apreensao',
        width: 120,
        alignment: 'center',
        caption: '%Meta/Apreensoes',
      },
      {
        dataField: 'Liberacoes',
        width: 110,
        caption: 'Liberações'

      },
      {
        caption: 'Pátio',
        dataField: 'Nome_Patio',
        groupIndex: 0,

      }],
      sortByGroupSummaryInfo: [{
        summaryItem: 'count',
      }],
      summary: {
        groupItems: [{
          column: 'Qtde',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        },{
          column: 'Qtde',
          summaryType: 'avg',
          valueFormat: "#,##0",
          displayFormat: 'Media: {0}',
          showInGroupFooter: true,
        },
        {
          column: 'Liberacoes',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        },
        {
          column: 'Liberacoes',
          summaryType: 'avg',
          valueFormat: "#,##0",
          displayFormat: 'Média: {0}',  
          showInGroupFooter: true,
        }],
      },onInitialized: function (e) {
        $scope.gridInstance = e.component;    
      }

    };
    
  }  
}

function atualizaGridEstoque() {
 
  $scope.gridOptionsEstoque = {
//    dataSource: dados,
    keyExpr: 'ID',
    allowColumnReordering: false,
    selection: {
      mode: 'single',
    },
    hoverStateEnabled: true,
    showBorders: true,
    showRowLines: true,
    rowAlternationEnabled: true,
    loadPanel: {
      enabled: true,
    },
    searchPanel: {
      visible: true,
      width: 240,
      placeholder: 'Pesquisar...',
    },

    export: {
      enabled: true,
      allowExportSelectedData: false,
    },
    onExporting(e) {
      const workbook = new ExcelJS.Workbook();
      const worksheet = workbook.addWorksheet('gerencial-media');

      DevExpress.excelExporter.exportDataGrid({
        component: e.component,
        worksheet,
        autoFilterEnabled: true,
      }).then(() => {
        workbook.xlsx.writeBuffer().then((buffer) => {
          saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'gerencial-media-estoque.xlsx');
        });
      });
      e.cancel = true;
    },

    columns: [{
      dataField: 'NCV_GERADOS',
      width: 120,
      caption: 'NCV_GERADOS'
    },
    {
      dataField: 'Liberados',
      width: 90,
      caption: 'Liberados'
    },
    {
      dataField: 'Lib_ARR',
      width: 80,
      caption: 'Lib_ARR'
    },
    {
      dataField: 'Cancelados',
      width: 100,
      caption: 'Cancelados'
    },
    {
      dataField: 'Inativos',
      width: 80,
      caption: 'Inativos'
    },
    {
      dataField: 'Invalidados',
      width: 100,
      caption: 'Invalidados'
    },{
      dataField: 'Saldo_Patio',
      width: 110,
      caption: 'SALDO PATIO'
    },{
      dataField: 'Judicial',
      width: 80,
      caption: 'Judicial'
    },
    {
      dataField: 'Renajud',
      width: 80,
      caption: 'Renajud',
    },
    {
      dataField: 'Preparacao',
      width: 100,
      caption: 'Preparação',
    },
    {
      dataField: 'Leiloado',
      width: 80,
      caption: 'Leiloado',
    },
    {
      dataField: 'Saldo_Leilao',
      width: 110,
      caption: 'SALDO LEILÃO',
    },
    {
      dataField: 'LocalVeiculo',
      width: 100,
      caption: 'Local'
    },
    {
      caption: 'Pátio',
      dataField: 'Nome_Patio',
      groupIndex: 0,

    }],
      summary: {
        groupItems: [{
          column: 'NCV_GERADOS',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        },{
          column: 'Liberados',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        },{
          column: 'Lib_ARR',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        },{
          column: 'Cancelados',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        },{
          column: 'Inativos',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        },{
          column: 'Invalidados',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        },{
          column: 'Saldo_Patio',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        },{
          column: 'Judicial',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        },{
        column: 'Renajud',
        summaryType: 'sum',
        displayFormat: ' {0}',
        showInGroupFooter: true,
        },{
          column: 'Preparacao',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        },{
          column: 'Leiloado',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        },{
          column: 'Saldo_Leilao',
          summaryType: 'sum',
          displayFormat: ' {0}',
          showInGroupFooter: true,
        }        
      ],    
      totalItems: [{
        column: 'NCV_GERADOS',
        summaryType: 'sum',
        displayFormat: ' {0}'
    },
    {
      column: 'Liberados',
      summaryType: 'sum',
      displayFormat: ' {0}',
      showInGroupFooter: true,
    },{
      column: 'Lib_ARR',
      summaryType: 'sum',
      displayFormat: ' {0}',
      showInGroupFooter: true,
    },
    {
      column: 'Cancelados',
      summaryType: 'sum',
      displayFormat: ' {0}',
      showInGroupFooter: true,
    },{
      column: 'Inativos',
      summaryType: 'sum',
      displayFormat: ' {0}',
      showInGroupFooter: true,
    },{
      column: 'Invalidados',
      summaryType: 'sum',
      displayFormat: ' {0}',
      showInGroupFooter: true,
    },{
      column: 'Transf',
      summaryType: 'sum',
      displayFormat: ' {0}',
      showInGroupFooter: true,
    },{
      column: 'Saldo_Patio',
      summaryType: 'sum',
      displayFormat: ' {0}',
      showInGroupFooter: true,
    },{
      column: 'Judicial',
      summaryType: 'sum',
      displayFormat: ' {0}',
      showInGroupFooter: true,
    },
    {
      column: 'Renajud',
      summaryType: 'sum',
      displayFormat: ' {0}',
      showInGroupFooter: true,
    },    
    {
      column: 'Preparacao',
      summaryType: 'sum',
      displayFormat: ' {0}',
      showInGroupFooter: true,
    },    
    {
      column: 'Leiloado',
      summaryType: 'sum',
      displayFormat: ' {0}',
      showInGroupFooter: true,
    },  
    {
      column: 'Saldo_Leilao',
      summaryType: 'sum',
      displayFormat: ' {0}',
      showInGroupFooter: true,
    }]
    },
    sortByGroupSummaryInfo: [{
      summaryItem: 'count',
    }] 
    ,onInitialized: function (e) {
      $scope.gridInstanceEstoque = e.component;    
    },
    onSelectionChanged(selectedItems) {
      $scope.MostaFrame(selectedItems,$scope.dados.apreendidos_ate);
    },

  };
}

function atualizaGridDetalhes() {
$scope.gridOptionsDetalhes = {
  //    dataSource: dados,
      keyExpr: 'ID',
      showBorders: true,
      showRowLines: true,
      rowAlternationEnabled: true,
      loadPanel: {
        enabled: true,
      },
      searchPanel: {
        visible: true,
        width: 240,
        placeholder: 'Pesquisar...',
      },

      export: {
        enabled: true,
        allowExportSelectedData: false,
      },
      onExporting(e) {
        const workbook = new ExcelJS.Workbook();
        const worksheet = workbook.addWorksheet('gerencial-media-detalhes');

        DevExpress.excelExporter.exportDataGrid({
          component: e.component,
          worksheet,
          autoFilterEnabled: true,
        }).then(() => {
          workbook.xlsx.writeBuffer().then((buffer) => {
            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'gerencial-media-detalhes.xlsx');
          });
        });
        e.cancel = true;
      },

      columns: [{
        dataField: 'NCV',
        width: 50,
        caption: 'Ncv'
      },
      {
        dataField: 'marca_modelo',
        width: 120,
        caption: 'Marco Modelo'
      },
      {
        dataField: 'ano',
        width: 40,
        caption: 'ANO'
      },{
        dataField: 'Cor',
        width: 60,
        caption: 'COR'
      }, {
        dataField: 'tipo_veiculo',
        width: 60,
        alignment: 'center',
        caption: 'TIPO VEICULO'
      },
      {
        dataField: 'data_apreensao',
        width: 80,
        alignment: 'center',
        caption: 'Data Apreensão',
      },{
        dataField: 'situacao',
        width: 80,
        alignment: 'center',
        caption: 'Situacão',
      },
      {
        dataField: 'tipo',
        width: 85,
        caption: 'Tipo'
      },{
        caption: 'PATIO_DESTINO',
        width: 150,
        dataField: 'Patio_Destino',
        groupIndex: 0,    
      }],
      sortByGroupSummaryInfo: [{
        summaryItem: 'count',
      }], 
        onInitialized: function (e) {
        $scope.gridInstance = e.component;    
      }

    };
}

function atualizaGridDetalhesTransf() {
  $scope.gridOptionsDetalhesTransf = {
    //    dataSource: dados,
        keyExpr: 'Patio_Destino',
        showBorders: true,
        showRowLines: true,
        rowAlternationEnabled: true,
        loadPanel: {
          enabled: true,
        },
        searchPanel: {
          visible: true,
          width: 240,
          placeholder: 'Pesquisar...',
        },
  
        export: {
          enabled: true,
          allowExportSelectedData: false,
        },
        onExporting(e) {
          const workbook = new ExcelJS.Workbook();
          const worksheet = workbook.addWorksheet('gerencial-media-transferidos');
  
          DevExpress.excelExporter.exportDataGrid({
            component: e.component,
            worksheet,
            autoFilterEnabled: true,
          }).then(() => {
            workbook.xlsx.writeBuffer().then((buffer) => {
              saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'gerencial-media-transferidos.xlsx');
            });
          });
          e.cancel = true;
        },
  
        columns: [
        {
          dataField: 'NCV',
          width: 50,
          caption: 'Ncv'
        },
        {
          dataField: 'marca_modelo',
          width: 90,
          caption: 'Marco Modelo'
        },
        {
          dataField: 'ano',
          width: 60,
          caption: 'ANO'
        },{
          dataField: 'Cor',
          width: 60,
          caption: 'COR'
        }, {
          dataField: 'tipo_veiculo',
          width: 60,
          alignment: 'center',
          caption: 'TIPO VEICULO'
        },
        {
          dataField: 'data_apreensao',
          width: 60,
          alignment: 'center',
          caption: 'Data Apreensão',
        },{
          dataField: 'situacao',
          width: 80,
          alignment: 'center',
          caption: 'Situacão',
        },
        {
          dataField: 'tipo',
          width: 80,
          caption: 'Tipo'
        },
        {
          caption: 'PATIO_DESTINO',
          width: 150,
          dataField: 'Patio_Destino',
          groupIndex: 0,    
        }],
        sortByGroupSummaryInfo: [{
          summaryItem: 'count',
        }], 
          onInitialized: function (e) {
          $scope.gridInstance = e.component;    
        }
  
      };
  }

function atualiza(dataSource) {
    
    $scope.chartOptions = {
      commonSeriesSettings: {
        type: 'bar',
      },
      tooltip: {
        enabled: true,
        format: 'number',
        customizeTooltip(args) {
          return {
            html: `${args.seriesName} | Total<div class='number'>${args.valueText}</div>`,
          };
        },
      },
      size: {
        height: 200,
      },
      adaptiveLayout: {
        width: 450,
      },
      onInitialized(e) {
 //       $scope.chart = e.component;
      },
    };
  
    $scope.pivotGridOptions = {
      allowSortingBySummary: false,
      allowFiltering: false,
      showBorders: true,
      showColumnGrandTotals: true,
      showRowGrandTotals: true,
      showRowTotals: false,
      showColumnTotals: false,
      fieldChooser: {
        enabled: true,
        height: 400,
      },
 
      onInitialized(e) {
  /*      
        e.component.bindChart($scope.chart, {
          dataFieldsDisplayMode: 'splitPanes',
          alternateDataFields: false,
        });
  */      
 //       expand(e.component.getDataSource());
      }, scrolling: {
        mode: 'virtual',
      },
      dataSource: {
        fields: [{
          caption: 'Pátio',
          width: 300,
          dataField: 'Nome_Patio',
          area: 'row',
        },{ 
          dataField: 'Ano',
          area: 'column',   
          expanded: true,
        },{ 
          dataField: 'Mes',
          area: 'column',
        },
        {
          caption: 'Total',
          dataField: 'Qtde',
          dataType: 'number',
          summaryType: 'sum',
          area: 'data',
        },{
          caption: 'Media',
          dataField: 'Media',
          dataType: 'number',
          summaryType: 'sum',
          area: 'data',
        },
      ],
        store: dados,
      },
    };
}

    $scope.pesquisar = async () => {

          
          var dataGrid = $("#gridContainer").dxDataGrid("getDataSource");          

          dados = null ;
          $scope.gridOptions = null ;  
          
          $scope.ShowSpinnerLoad = true
          const response = await $http.get(urlServidor.urlServidorChatAdmin + '/analitico/gerencial-media-mes', { params: $scope.dados })
          dados  = response.data

          $("#gridContainer").dxDataGrid({
            dataSource: dados
          });

          if (!response.data.length > 0) {
              $scope.ShowSpinnerLoad = false
              myFunctions.showAlert('Filtro não encontrou registros! Revise parâmetros da pesquisa!')
              return
          } else {
              $("#gridContainer").dxDataGrid("instance").refresh();
              $scope.data_ate_apreensao = $scope.dados.apreendidos_ate.getDate()+'/'+($scope.dados.apreendidos_ate.getMonth()+1)+'/'+$scope.dados.apreendidos_ate.getFullYear()
              atualizaGrid() ; 
 //             pesquisarEstoque() ;            
                pesquisarEstoqueGeral() ;            
          }

    }    


    async function pesquisarEstoque() {
          
//      var dataGridEstoque = $("#gridContainer-estoque").dxDataGrid("getDataSource-estoque");          

      dadosEstoque = null ;
      $scope.gridOptionsEstoque = null ;        

      $scope.ShowSpinnerLoad = true
      const response = await $http.get(urlServidor.urlServidorChatAdmin + '/analitico/gerencial-estoque-patios', { params: $scope.dados })
      dadosEstoque  = response.data

      $("#gridContainer-estoque").dxDataGrid({
        dataSource: dadosEstoque
      });

      if (!response.data.length > 0) {
          $scope.ShowSpinnerLoad = false
          myFunctions.showAlert('Filtro não encontrou registros! Revise parâmetros da pesquisa!')
          return
      } else {
          $("#gridContainer-estoque").dxDataGrid("instance").refresh();  
          atualizaGridEstoque() ;         
      }

    }    


    async function pesquisarEstoqueGeral() {
          
      //      var dataGridEstoque = $("#gridContainer-estoque").dxDataGrid("getDataSource-estoque");          
      
            dadosEstoque = null ;
            $scope.gridOptionsEstoque = null ;        
      
            $scope.ShowSpinnerLoad = true
            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/analitico/gerencial-estoque-complemento', { params: $scope.dados })
            dadosEstoque  = response.data
      
            $("#gridContainer-estoque").dxDataGrid({
              dataSource: dadosEstoque
            });
      
            if (!response.data.length > 0) {
                $scope.ShowSpinnerLoad = false
                myFunctions.showAlert('Filtro não encontrou registros! Revise parâmetros da pesquisa!')
                return
            } else {
                $("#gridContainer-estoque").dxDataGrid("instance").refresh();  
                atualizaGridEstoque() ;         
            }
      
    }    
      


    function expand(dataSource) {
      setTimeout(() => {
        dataSource.expandHeaderItem('row', ['North America']);
        dataSource.expandHeaderItem('column', [2013]);
      }, 0);
    }
  });
