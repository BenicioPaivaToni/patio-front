angular
  .module("app")
  .controller(
    "relatorioApreensoes",
    function ($scope, $mdDialog, $http, urlServidor, myFunctions, $mdDialog) {
      $scope.dados = {
        dataInicio: new Date(),
        dataFinal: new Date(),
        patio: null,
        detalhes: false,
        id_grupo_patios: null,
        id_usuario: null,
      };
      $scope.ShowSpinnerLoad = false;

      $scope.tipoUsuario = localStorage.getItem("tipo_usuario")
      
      const getUsuariosAutoridade = async () => {
        const response = await $http.get(
          urlServidor.urlServidorChatAdmin + "/usuarios/listar"
        );
        return response.data.filter((item) => item.tipo === "app autoridade");
      };

      getUsuariosAutoridade().then((data) => {
        $scope.listaUsuarios = data;
      });

      $scope.limpar = function (ev) {
        $scope.dados.dataInicio = new Date();
        $scope.dados.dataFinal = new Date();
        $scope.dados.patio = null;
        $scope.dados.detalhes = null;
        $scope.dados.id_grupo_patios = null;
        $scope.dados.id_usuario = null

      };

      const buscaPatiosGrupo = async () => {
        const response = await $http.get(
          `${urlServidor.urlServidorChatAdmin}/grupos-patios/listar`
        );

        return response.data.filter((item) => item.ativo == 1);

      };

      buscaPatiosGrupo().then((Response) => ($scope.patiosGrupo = Response));

      $scope.mesAtual = function (ev, id) {
        let hoje = new Date();

        $scope.dados.dataInicio = new Date(
          hoje.getFullYear(),
          hoje.getMonth(),
          01
        );
        $scope.dados.dataFinal = new Date(
          hoje.getFullYear(),
          hoje.getMonth() + 1,
          0
        );
      };

      $http
        .put(
          urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
          {
            idUsuario: localStorage.getItem("id_usuario"),
            idEmpresa: localStorage.getItem("id_empresa"),
          }
        )
        .then( ({data})  => {
          $scope.patios = data.filter((item) => item.status == 1);
        });


      $scope.pesquisar = function () {
        $scope.ShowSpinnerLoad = true;
        if ($scope.dados.patio || $scope.dados.id_grupo_patios) {
          $scope.dadosApreensoes = null;

          $http
            .get(
              urlServidor.urlServidorChatAdmin + "/ncv/relatorio-apreensoes",
              { params: $scope.dados }
            )
            .then(({ data }) => { 

              if ($scope.tipoUsuario == 'consulta contabil'){

                $scope.dadosApreensoes = data.filter((item)=> item.nota_fiscal !== null)

              }else{

                $scope.dadosApreensoes = data

              }

            });

          $scope.ShowSpinnerLoad = false;

        } else {
          myFunctions.showAlert(
            "Nenhum Pátio ou Grupo de Pátios foi selecionado! Verifique!"
          );

          $scope.ShowSpinnerLoad = false;
        }
      };

      $scope.ShowHistorico = (ev, id) => {
        const _id = id;
        $mdDialog
          .show({
            controller: async function ($scope, $mdDialog, $mdToast) {
              const response = await $http.get(
                urlServidor.urlServidorChatAdmin + "/ncv/busca-historico",
                { params: { ncv: _id } }
              );
              const { data } = response;
              $scope.Historicos = data;

              $scope.cancel = function () {
                $mdDialog.cancel();
              };
            },
            templateUrl: "MostrarHistorico.html",

            preserveScope: true,
          })
          .then(
            function (answer) {
              let a = answer;
            },
            function () {
              $scope.statusdialog = "You cancelled the dialog.";
            }
          );
      };

      $scope.gerarPlanilha = function () {
        alasql(
          'SELECT * INTO XLSX("relatorio-apreensoes.xlsx",{headers:true}) FROM ?',
          [$scope.dadosApreensoes]
        );
      };

      $scope.gerarPdf = function () {
        if ($scope.dados.detalhes === true) {
          var doc = new jsPDF({ orientation: "landscape" });
          var totalPagesExp = "{total_pages_count_string}";

          doc.setFontSize(12);
          doc.autoTable({
            columnStyles: { tipo_veiculo: { halign: "left" } },
            body: $scope.dadosApreensoes,
            columns: [
              { header: "NCV", dataKey: "id" },
              { header: "Placa", dataKey: "placa" },
              { header: "Tipo Veiculo", dataKey: "tipo_veiculo" },
              { header: "Apreensão", dataKey: "data_apreensao" },
              { header: "Marca/Modelo", dataKey: "marca_modelo" },
              { header: "Ano", dataKey: "ano" },
              { header: "Cor", dataKey: "cor" },
              { header: "KM Perc.", dataKey: "km_percorrido" },
              { header: "Patio", dataKey: "patio" },
              { header: "Chassi", dataKey: "chassi" },
              { header: "Local", dataKey: "localSemCss" },
              { header: "Numero Motor", dataKey: "numero_motor" },
              { header: "Placa Municipio", dataKey: "placa_municipio" },
              { header: "Placa UF", dataKey: "placa_uf" },
            ],
            bodyStyles: {
              margin: 10,
              fontSize: 08,
            },
            didDrawPage: function (data) {
              // Header
              doc.setFontSize(18);
              doc.setTextColor(40);
              doc.setFontStyle("normal");

              doc.text(
                "Apreensões no periodo de " +
                  $scope.dados.dataInicio.toLocaleDateString("pt-BR") +
                  " a " +
                  $scope.dados.dataFinal.toLocaleDateString("pt-BR"),
                data.settings.margin.left + 15,
                22
              );

              // Footer
              var str = "Pagina " + doc.internal.getNumberOfPages();
              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
              }

              doc.setFontSize(10);

              // jsPDF 1.4+ uses getWidth, <1.4 uses .width
              var pageSize = doc.internal.pageSize;
              var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();
              doc.text(str, data.settings.margin.left, pageHeight - 10);
            },
            margin: { top: 30 },
          });
          if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
          }

          doc.save("relatorio-apreensoes-com-Detalhes.pdf");
        } else {
          var doc = new jsPDF({ orientation: "landscape" });
          var totalPagesExp = "{total_pages_count_string}";

          doc.setFontSize(12);
          doc.autoTable({
            columnStyles: { tipo_veiculo: { halign: "left" } },
            body: $scope.dadosApreensoes,
            columns: [
              { header: "NCV", dataKey: "id" },
              { header: "Placa", dataKey: "placa" },
              { header: "Tipo Veiculo", dataKey: "tipo_veiculo" },
              { header: "Apreensão", dataKey: "data_apreensao" },
              { header: "Marca/Modelo", dataKey: "marca_modelo" },
              { header: "Ano", dataKey: "ano" },
              { header: "Cor", dataKey: "cor" },
              { header: "KM Perc.", dataKey: "km_percorrido" },
              { header: "Patio", dataKey: "patioSemCss" },
            ],
            bodyStyles: {
              margin: 10,
              fontSize: 08,
            },
            didDrawPage: function (data) {
              // Header
              doc.setFontSize(18);
              doc.setTextColor(40);
              doc.setFontStyle("normal");

              doc.text(
                "Apreensões no periodo de " +
                  $scope.dados.dataInicio.toLocaleDateString("pt-BR") +
                  " a " +
                  $scope.dados.dataFinal.toLocaleDateString("pt-BR"),
                data.settings.margin.left + 15,
                22
              );

              // Footer
              var str = "Pagina " + doc.internal.getNumberOfPages();
              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
              }

              doc.setFontSize(10);

              // jsPDF 1.4+ uses getWidth, <1.4 uses .width
              var pageSize = doc.internal.pageSize;
              var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();
              doc.text(str, data.settings.margin.left, pageHeight - 10);
            },
            margin: { top: 30 },
          });
          if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
          }

          doc.save("relatorio-apreensoes.pdf");
        }
      };

      $scope.detalhe = function (ev, id) {
        window.open("/#/ncv-editar?id " + id, "_blank");
      };
    }
  );
