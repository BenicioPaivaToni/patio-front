
angular.module('app').controller('relatorioConferenciaRecebimentosCtrl', function($scope,$http,urlServidor, myFunctions){
  
    var dados = [] ;

    $scope.dados = {
        dataInicio: new Date(),
        dataFinal: new Date(),
        patio: null,
        id_grupo_patios: null,
    }; 

    $scope.ShowMessage = false;

    $scope.limpar = function (ev) {
        $scope.dados.dataInicio = new Date();
        $scope.dados.dataFinal = new Date();
        $scope.dados.patio = null;
        $scope.dados.id_grupo_patios = null;
    };

    $scope.mesAtual = function (ev, id) {
        let hoje = new Date()
        $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
        $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    };


    const buscaPatiosGrupo = async () => {
        const response = await $http.get(
        `${urlServidor.urlServidorChatAdmin}/grupos-patios/listar`
        );
        const { data } = response;
        return data.filter((item) => item.ativo == 1);
    };

    buscaPatiosGrupo().then((Response) => ($scope.patiosGrupo = Response));

    $http
    .put(
            urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
        {
            idUsuario: localStorage.getItem("id_usuario"),
            idEmpresa: localStorage.getItem("id_empresa"),
        }
    )
    .then( ({ data })  => {
        $scope.patios = data.filter((item) => item.status == 1);
    });

   
    function atualizaGrid() {

        $scope.dataGridOptions = {
            dataSource: dados,
           // keyExpr: 'ID',
            showBorders: true,
            noDataText: '',
            grouping: {
                contextMenuEnabled: true,
                autoExpandAll: true,
                texts: { 
                    groupContinuedMessage: "Continuação da página anterior",
                    groupContinuesMessage: "Continua na próxima página"
                }
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 20, 50, 100],
              },

            searchPanel: {
                visible: true,
                width: 240,
                placeholder: 'Pesquisar...',
              },
            selection: {
                mode: 'sigle',
            },
            columns: [{
                dataField: 'placa',
                alignment: 'center',
                width: 90,
                caption: 'Placa',
            }, {
                dataField: 'Marca/Modelo',
                alignment: 'center',
                dataField: 'marca_modelo',
            }, {
                dataField: 'patio',
                groupIndex: 0,
            }, {
                caption: 'Ano',
                alignment: 'center',
                width: 70,
                dataField: 'ano',
            }, {
                caption: 'Data Apree.',
                alignment: 'center',
                width: 100,
                dataField: 'data_apreensao',
            }, {
                caption: 'Data Pgto.',
                alignment: 'center',
                width: 100,
                dataField: 'data_pagamento',
            }, {
                dataField: 'nome_pagador',
                alignment: 'center',
                caption: 'Pagador',
            }, {
                dataField: 'cpf_cnpj_pagador',
                alignment: 'center',
                caption: 'CPF/CNPJ',
            },{
                dataField: 'descricao',
                alignment: 'center',
                caption: 'Descrição',
            },{
                dataField: 'valor',
                alignment: 'center',
                dataType: 'number',
                format: "R$ #,##0.##",
                value: 'valor',
                caption: 'Valor',
            },          
            ],
            sortByGroupSummaryInfo: [{
              summaryItem: 'count',
            }],
            summary: {
              groupItems: [{
                column: 'patio',
                summaryType: 'count',
                displayFormat: '{0} recebimento(s)',
              }, 
               {
                column: 'valor',
                summaryType: 'sum',
                valueFormat: "R$ #,##0.##",
                displayFormat: 'Total: {0}',
                showInGroupFooter: true,
              }],
            },

        }
    }



    $scope.pesquisar = async () => {   

        if (($scope.dados.patio) || ($scope.dados.id_grupo_patios)) {
            dados = null ;
            $scope.dataGridOptions = null ;        

            $scope.ShowSpinnerLoad = true

            const response = await $http.get(urlServidor.urlServidorChatAdmin + '/ncv/conferencia-recebimentos', { params: $scope.dados })
            dados  = response.data
            
            $("#gridContainer").dxDataGrid({
            dataSource: dados
            });

            if (!response.data.length > 0) {
                $scope.ShowSpinnerLoad = false
                myFunctions.showAlert('Filtro não encontrou registros! Revise parâmetros da pesquisa!')
                return
            } else {
                $("#gridContainer").dxDataGrid("instance").refresh();
            
                atualizaGrid()       
            }

        }else{

            myFunctions.showAlert('Nenhum Pátio ou Grupo Pátio foi selecionado! Verifique!')
        }
    }


    $scope.gerarPDF = function () {

        if (dados.length !== 0){

        var getAll = $("#gridContainer").dxDataGrid('instance');

        getAll.getDataSource().store().load().done((res)=>{
            $scope.dadosPDF = res
        })
   
        var doc = new jsPDF();
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = "{total_pages_count_string}";
       // doc.setFontSize(10);

        doc.autoTable({
            columnStyles: {
                patio: { halign: "left" },
            },

            body: $scope.dadosPDF,
            columns: [
                { header: "Pátio", dataKey: "patio" },
                { header: "Placa", dataKey: "placa" },
                { header: "Marca/Modelo", dataKey: "marca_modelo" },
                { header: "Ano", dataKey: "ano" },
                { header: "Data Apreensão", dataKey: "data_apreensao" },
                { header: "Data Pagamento", dataKey: "data_pagamento" },
                { header: "Pagador", dataKey: "nome_pagador" },
                { header: "CPF/CNPJ", dataKey: "cpf_cnpj_pagador" },
                { header: "Descrição", dataKey: "descricao" },
                { header: "Valor", dataKey: "valor" }

            ],
            
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },

            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18);
                doc.setTextColor(40);
                doc.setFontStyle("normal");

                doc.text(
                'Conferência dos recebimentos, periodo: ' + 
                $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + 
                $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 10, 22)

                // Footer
                var str = "Página " + doc.internal.getNumberOfPages();
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
                var totalpaginas = totalPagesExp;
                }

                doc.setFontSize(10);

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize;
                var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();

                doc.text(
                    str + "                        ",
                    data.settings.margin.left,
                    pageHeight - 10
                );

            },
            margin: { top: 30 },
        });
        
        if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
        }

        doc.save("Conferencia-recebimentos.pdf")
        
    }else{

        myFunctions.showAlert('Sem dados para gerar PDF! Verifique!')
  
        }
   
    }


    $scope.gerarExcel = function () {
    
        if (dados.length !== 0){

        const workbook = new ExcelJS.Workbook();
        const worksheet = workbook.addWorksheet('Conferencia-recebimentos');
        worksheet.columns = [  
            { width: 10 }, 
            { width: 25 }, 
            { width: 10 }, 
            { width: 15 }, 
            { width: 15 }, 
            { width: 30 }, 
            { width: 20 }, 
            { width: 25 }, 
            { width: 20 }, 
        ]

        DevExpress.excelExporter.exportDataGrid({
            component: $("#gridContainer").dxDataGrid("instance"),
            worksheet: worksheet,
            autoFilterEnabled: true,
            keepColumnWidths: false //false se utilizado "worksheet.collumns". Se true, "worksheet.collumns" fica sem efeito
        }).then(() => {
            workbook.xlsx.writeBuffer().then((buffer) => {
            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'Conferencia-recebimentos.xlsx');
            });
        });
    
        }else{

            myFunctions.showAlert('Sem dados para gerar planilha! Verifique!')
  
        }
    }


});