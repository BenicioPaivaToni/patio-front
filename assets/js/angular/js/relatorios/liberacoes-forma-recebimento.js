angular
  .module("app")
  .controller(
    "liberacoes-forma-pagtoCtrl",
    function ($scope, $http, urlServidor, myFunctions) {
      $scope.dados = {
        dataInicio: new Date(),
        dataFinal: new Date(),
        patio: null,
        detalhes: false,
        tipo_liberacao: "",
        id_grupo_patios: null,
      };
      $scope.ShowSpinnerLoad = false;

      const buscaPatiosGrupo = async () => {
        const response = await $http.get(
          `${urlServidor.urlServidorChatAdmin}/grupos-patios/listar`
        );

        return response.data;
      };

      buscaPatiosGrupo().then((Response) => ($scope.patiosGrupo = Response));

      $scope.mesAtual = function (ev, id) {
        let hoje = new Date();

        $scope.dados.dataInicio = new Date(
          hoje.getFullYear(),
          hoje.getMonth(),
          01
        );
        $scope.dados.dataFinal = new Date(
          hoje.getFullYear(),
          hoje.getMonth() + 1,
          0
        );
      };

      $scope.limpar = function (ev) {
        $scope.dados.dataInicio = new Date();
        $scope.dados.dataFinal = new Date();
        $scope.dados.patio = null;
        $scope.dados.detalhes = null;
        $scope.dados.forma_pagto = null;
        $scope.dados.tipo_liberacao = "LIBERADO";
        $scope.dados.id_grupo_patios = null;
        $scope.quantidadeRegistros = null;
        $scope.dadosLiberacoes = null;
        $scope.totalGeral = null;
        $scope.dadosPagamentos = null;
      };

      $http
        .put(
          urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
          {
            idUsuario: localStorage.getItem("id_usuario"),
            idEmpresa: localStorage.getItem("id_empresa"),
          }
        )
        .then(({ data }) => {
          $scope.patios = data;
        });

      $http
        .put(urlServidor.urlServidorChatAdmin + "/tipos-recebimento/listar")
        .then(({ data }) => {
          $scope.formasPagto = data.filter((item) => item.tipo === "PAGAR");
        });

      $scope.pesquisar = async () => {
        if ($scope.dados.patio || $scope.dados.id_grupo_patios) {
          const response = await $http.get(
            urlServidor.urlServidorChatAdmin + "/ncv/dados-recebimentos",
            { params: $scope.dados }
          );
          const { data } = response;

          if (!data[1].length > 0) {
            myFunctions.showAlert(
              "Filtro não encontrou registros! Revise parâmetros da pesquisa!"
            );
            return;
          }
          $scope.dadosPDF = data[0];

          $scope.dadosExcel = data[0];

          $scope.resumoRecebimentos = data[1];

          const Lancamentos = data[0];

          const totais = Lancamentos.shift();

          $scope.totalRegistros = data[0].length;

          $scope.recebimentosPatio = data[2];

          const dataConvertida = Lancamentos.map((item) => {
            var apreensaoArr = item.data_apreensao.split("/");
            var apreensaoStr =
              apreensaoArr[1] + "-" + apreensaoArr[0] + "-" + apreensaoArr[2];
            var data_apreensao = new Date(apreensaoStr);
            var liberacaoArr = item.data_liberacao.split("/");
            var liberacaoStr =
              liberacaoArr[1] + "-" + liberacaoArr[0] + "-" + liberacaoArr[2];
            var data_liberacao = new Date(liberacaoStr);
            return {
              data_apreensao,
              data_liberacao,
              total: item.total,
            };
          });

          $scope.outOfFilter = [];

          $scope.infFilter = [];

          dataConvertida.forEach((element) => {
            var dataInicio = new Date().setDate(
              $scope.dados.dataInicio.getDate() - 1
            );

            dataInicio = new Date(dataInicio);
            if (
              element.data_apreensao.getTime() >= dataInicio.getTime() &&
              element.data_apreensao.getTime() <=
                $scope.dados.dataFinal.getTime()
            ) {
              $scope.infFilter.push(element);
            } else {
              $scope.outOfFilter.push(element);
            }
          });

          $scope.totalPorValor = function (items) {
            let resultado = 0;

            if (!Array.isArray(items)) return resultado;

            for (const item of items) {
              if (item.total) {
                let value = item.total.replace(/,/g, ".");
                resultado += parseFloat(value);
              }
            }

            return resultado;
          };

          const groupBy = (x, f) =>
            x.reduce((a, b) => ((a[f(b)] ||= []).push(b), a), {});

          const groupedTypes = groupBy(Lancamentos, (item) => item.patio);

          await criaTable(groupedTypes, groupBy, totais);
        } else {
          myFunctions.showAlert(
            "Nenhum Pátio ou Grupo Pátio foi selecionado! Verifique!"
          );
        }
      };

      const criaTable = async (data, groupBy, totais) => {
        for (var prop in data) {
          const group = groupBy(data[prop], (item) => item.forma_pagto);
          data[prop] = group;
        }

        const infos = data;

        const helperFunctions = {
          stringValueToNumber(value) {
            return parseFloat(
              value.replace("R$ ", "").replace(".", "").replace(",", ".")
            );
          },

          getTotalPatio(item) {
            const items = Object.values(item).flat(1);
            const Total = items.reduce(
              (acumulador, valorAtual) =>
                acumulador + this.stringValueToNumber(valorAtual.total),
              0
            );

            return Total.toLocaleString("pt-BR");
          },

          getTotalLiberacoes(item) {
            return Object.values(item).flat(1);
          },

          gerarTotalPorTipo(item) {
            const Total = item.reduce(
              (acumulador, valorAtual) =>
                acumulador + this.stringValueToNumber(valorAtual.total),
              0
            );
            return Total.toLocaleString("pt-BR");
          },
        };

        const infoHtml = Object.keys(infos).map(
          (key) => /*HTML*/ `
                <div class="patio">
                  
                <p>${key}  - QTDE LIBERAÇÕES:${
            helperFunctions.getTotalLiberacoes(infos[key]).length
          } </p> 
                </div>

                    <div  class="categoria" style="color:#0613cc">

                          <p>TOTAL PÁTIO:R$ ${helperFunctions.getTotalPatio(
                            infos[key]
                          )} </p>

                    </div>

                  

                ${Object.keys(infos[key])
                  .map(
                    (info) => /*HTML*/ `
                <div class="categoria">
                <p>${info} R$
                ${helperFunctions.gerarTotalPorTipo(infos[key][info])}
                </p>
                </div>

               
              <div class="result">
                         

                    ${infos[key][info]
                      .map(
                        (item, idx) =>
                          `   
                            <div class="resultsRow">
                            <div class="number">
                            <h3 class="title">${idx === 0 ? `NCV` : ``}</h3>
                                <h4>${item.id}</h4>
                            </div>

                            <div>
                            <h3 class="title">${idx === 0 ? `Placa` : ``}</h3>
                                <h4>${item.placa}</h4>
                            </div>
                           
                            <div class="marca_modelo">
                                <h3 class="title">${
                                  idx === 0 ? `Marca/Modelo` : ``
                                }</h3>
                                <h4>${item.marca_modelo}</h4>
                            </div>

                            <div >
                            <h3 class="title">${
                              idx === 0 ? `Liberação` : ``
                            }</h3>
                                <h4>${item.data_liberacao}</h4>
                            </div>

                            <div > 
                                <h3 class="title">${
                                  idx === 0 ? `Apreensão` : ``
                                }</h3>

                                <h4>${item.data_apreensao}</h4>
                            </div>

                            <div class="number">
                                <h3 class="title">${
                                  idx === 0 ? `Guincho` : ``
                                }</h3>
                                <h4>${item.valor_guincho}</h4>
                            </div>

                            <div class="number">
                                <h3 class="title">${
                                  idx === 0 ? `Diaria` : ``
                                }</h3>
                                <h4>${item.valor_estadia}</h4>
                            </div>

                            <div class="number" >
                            <h3 class="title">${idx === 0 ? `Diarias` : ``}</h3>

                                <h4>${item.estadias}</h4>
                            </div>

                            <div >
                                <h3 class="title">${
                                  idx === 0 ? `Total Diarias` : ``
                                }</h3>

                                <h4>${item.total_diarias}</h4>
                            </div>

                            <div class="number">
                            <h3 class="title">${
                              idx === 0 ? `Desconto` : ``
                            }</h3>

                                <h4>${item.desconto}</h4>
                            </div>
                            <div class="number">
                            <h3 class="title">${
                              idx === 0 ? `Acrescimo` : ``
                            }</h3>

                                <h4>${item.acrescimo}</h4>
                            </div>

                            <div class="number">
                            <h3 class="title">${
                              idx === 0 ? `Recebido` : ``
                            }</h3>

                                <h4>${item.total}</h4>
                            </div>
                            </hr>


                            </div>

                                         
                        
                        
                        `
                      )
                      .join("\n")}
                    
                </div>
                
                        
                        `
                  )
                  .join("\n")}
              
                

                `
        );

        const tableMain = document.getElementById("tableMain");
        tableMain.innerHTML = infoHtml.join("\n");

        const tableMainPDF = document.getElementById("tableMainPDF");
        tableMainPDF.innerHTML = infoHtml.join("\n");
      };

      $scope.gerarPlanilha = function () {
        $scope.dadosPlanilha = null;

        $scope.dadosPlanilha = $scope.dadosExcel.map((x) => {
          let v_guincho = x.valor_guincho || "0";
          v_guincho = parseNumber(v_guincho);

          let t_diarias = x.total_diarias || "0";
          t_diarias = parseNumber(t_diarias);

          let v_estadia = x.valor_estadia || "0";
          v_estadia = parseNumber(v_estadia);

          let v_desconto = x.desconto || "0";
          v_desconto = parseNumber(v_desconto);

          let n_estadias = x.estadias || "0";
          n_estadias = parseNumber(n_estadias);

          let v_acrescimo = x.acrescimo || "0";
          v_acrescimo = parseNumber(v_acrescimo);

          let v_total = x.total || "0";
          v_total = parseNumber(v_total);

          function parseNumber(strg) {
            var strg = strg || "";
            var decimal = ".";
            strg = strg.replace(/[^0-9$.,]/g, "");
            if (strg.indexOf(",") > strg.indexOf(".")) decimal = ",";
            if ((strg.match(new RegExp("\\" + decimal, "g")) || []).length > 1)
              decimal = "";
            if (
              decimal != "" &&
              strg.length - strg.indexOf(decimal) - 1 == 3 &&
              strg.indexOf("0" + decimal) !== 0
            )
              decimal = "";
            strg = strg.replace(new RegExp("[^0-9$" + decimal + "]", "g"), "");
            strg = strg.replace(",", ".");
            return parseFloat(strg);
          }

          return {
            patio: x.patio,
            NCV: x.id,
            data_apreensao: x.data_apreensao,
            data_liberacao: x.data_liberacao,
            valor_guincho: v_guincho,
            total_diarias: t_diarias,
            estadias: n_estadias,
            valor_estadia: v_estadia,
            desconto: v_desconto,
            acrescimo: v_acrescimo,
            total: v_total,
            tipo_veiculo: x.tipo_veiculo,
            forma_pagto: x.forma_pagto,
            placa: x.placa,
            local: x.local,
            placa_uf: x.placa_uf,
            placa_municipio: x.placa_municipio,
            chassi: x.chassi,
            numero_motor: x.numero_motor,
            marca_modelo: x.marca_modelo,
          };
        });

        $scope.dadosPlanilha.push({
          patio: "TOTAL GERAL",
          total:
            $scope.totalPorValor($scope.outOfFilter) +
            $scope.totalPorValor($scope.infFilter),
        });

        alasql(
          'SELECT *  INTO XLSX("relatorio-liberacao-forma-de-recebimento.xlsx",{headers:true}) FROM ?',
          [$scope.dadosPlanilha]
        );
      };

      function sleep(milliseconds) {
        return new Promise((resolve) => setTimeout(resolve, milliseconds));
      }

      $scope.gerarPdf = () => {
        const resultlength = $scope.dadosPDF.length;
        var TimeOut;
        if (resultlength < 100) {
          TimeOut = 1000;
        } else if (resultlength < 800) {
          TimeOut = 2000;
        } else if (resultlength < 1500) {
          TimeOut = 4000;
        }

        setTimeout(async () => {
          document.getElementById("tableBodyPDF").style.display = "block";
          document.getElementById("Modal").style.display = "block";

          var pdf = new jsPDF({ orientation: "landscape" });
          const thumbnailsHtml = document.getElementById("tableBodyPDF");
          await sleep(TimeOut);

          const canvas = await html2canvas(thumbnailsHtml, {
            windowWidth: 1700,
            windowHeight: 1500,
            scrollX: 0,
            scrollY: 0,
          });

          const imgData = canvas.toDataURL("image/jpeg", 1);

          await sleep(700);
          const imgProps = pdf.getImageProperties(imgData);

          const pdfWidth = 300; /* this.pdf.internal.pageSize.getWidth() */
          const pageHeight = pdf.internal.pageSize.getHeight();
          const imgHeight =
            (imgProps.height * pdfWidth) / 1400; /* imgProps.width */
          var heightLeft = imgHeight;
          var position = 0;
          pdf.addImage(
            imgData,
            "PNG",
            0, // Position of image from left and right
            0, // Position of image from top and bottom
            pdfWidth,
            imgHeight,
            undefined,
            "SLOW"
          );
          heightLeft -= pageHeight;
          while (heightLeft >= 0) {
            position = heightLeft - imgHeight;
            pdf.addPage();
            pdf.addImage(
              imgData,
              "JPEG",
              0,
              position + 0,
              pdfWidth,
              imgHeight,
              undefined,
              "SLOW"
            );
            heightLeft -= pageHeight;
          }
          $scope.ShowMessage = false;
          pdf.save("relatorio-liberacao-forma-de-recebimento.pdf");

          document.getElementById("tableBodyPDF").style.display = "none";
          document.getElementById("Modal").style.display = "none";


          return;
        }, TimeOut);
      };
    }
  );
