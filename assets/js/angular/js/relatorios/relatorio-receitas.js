
angular.module('app')
.controller('relatorioReceitasCtrl', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.dados = {
        dataInicio: null,
        dataFinal: null,
        //patios: null
    }

    let hoje = new Date()
    $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
    $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    $scope.mesAtual = function (ev, id) {
        $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
        $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    };
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("relatorio-receitas.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({orientation: "landscape"});
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({
            columnStyles: { 
                Patio: {halign: 'left'},
              },
           
            body: $scope.dadosList,
            columns: [
                {header: 'Pátio', dataKey: 'Patio'},
                {header: 'Valor Receita', dataKey: 'total'},
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                doc.text('Receitas do periodo de ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22)

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: {top: 30},
        });

        
        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('receitas.pdf')
    }


    $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', {idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa')}).then(function (response) {
        $scope.patios = response.data;
    });
    $http.put(urlServidor.urlServidorChatAdmin + '/tipos-recebimento/listar').then(function (response) {
        $scope.formasPagto = response.data;
    });
    $scope.pesquisar = function (param1) {

        $scope.dadosList = null;

        $http.get(urlServidor.urlServidorChatAdmin + '/relatorios/relatorio-receitas', {params: $scope.dados}).then(function (response) {
            $scope.dadosList = response.data;
            
        });
    };

});
