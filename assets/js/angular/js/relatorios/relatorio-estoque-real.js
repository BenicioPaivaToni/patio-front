angular
  .module("app")
  .controller(
    "relatorioEstoqueRealCtrl",
    function ($scope, $http, myFunctions, urlServidor, $mdDialog) {
      $scope.dados = {
        patio: "",
      }; 


      $scope.pesquisar = {

        buscaEstoqueReal: async () => {
            if ($scope.dados.patio) {

              const response = await $http.get(
                `${urlServidor.urlServidorChatAdmin}/relatorios/estoque-real`,
                {
                    params: {
                    patio: $scope.dados.patio
                    
                    },
                }
                )
                const { data } = response

                $scope.dadosList = data

                
            }else{

                myFunctions.showAlert('Nenhum Pátio foi selecionado para pesquisa! Verifique!')

            }
        },
           
        limpar: (ev) => {
            $scope.dados = {
                id_patio: ""
            },
            
            $scope.dadosList = ""
        }

      }

      

      $http
        .put(
          urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
          {
            idUsuario: localStorage.getItem("id_usuario"),
            idEmpresa: localStorage.getItem("id_empresa"),
          }
        )
        .then(({ data }) => {
          $scope.patios = data.filter((item) => {
            return item.status == 1
            
          });
        });

      
      $scope.gerarPlanilha = () => {
      if ($scope.dadosList) {

        const excelAjustes = $scope.dadosList.map((item) => ({
            patio: item.PATIO,
            ncv: item.NCV,
            placa: item.placa,
            tipo_veiculo: item.tipo_veiculo,
            data_apreensao: item.data_apreensao, 
            marca_modelo: item.marca_modelo,
            cor: item.cor,
            ano: item.ano,        
            diarias: item.QTDE_DIARIAS
            
        }))

        alasql(
          'SELECT * INTO XLSX("relatorio-estoque-real.xlsx",{headers:true}) FROM ?',[excelAjustes]
        )

      }else{
        
        myFunctions.showAlert('Sem dados para gerar planilha! Verifique!')

      }
    }


    $scope.gerarPDF = function () {
    
        if ($scope.dadosList) {

        const PDFAjustes = $scope.dadosList.map((item) => ({
            patio: item.PATIO,
            ncv: item.NCV,
            placa: item.placa,
            marca_modelo: item.marca_modelo,
            cor: item.cor,
            ano: item.ano,
            tipo_veiculo: item.tipo_veiculo,
            data_apreensao: item.data_apreensao,            
            diarias: item.QTDE_DIARIAS
           
        }))

        var doc = new jsPDF({ orientation: "landscape" });

        var totalPagesExp = "{total_pages_count_string}";

        doc.setFontSize(10);

        doc.autoTable({
          columnStyles: {
            vencimento: { halign: "left" },
          },

          body: PDFAjustes,
          columns: [
            { header: "Patio", dataKey: "patio" },
            { header: "NCV", dataKey: "ncv" },
            { header: "Placa", dataKey: "placa" },
            { header: "Tipo", dataKey: "tipo_veiculo" },
            { header: "Data Apreensão", dataKey: "data_apreensao" },  
            { header: "Marca/Modelo", dataKey: "marca_modelo" },
            { header: "Cor", dataKey: "cor" },
            { header: "Ano", dataKey: "ano" },
            { header: "Diarias", dataKey: "diarias" },

          ],
          bodyStyles: {
            margin: 10,
            fontSize: 08,
          },

          didDrawPage: function (data) {
            // Header
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle("normal");

            doc.text(
              "Relatório de Estoque Real",
              data.settings.margin.left + 15, 22
            );

            // Footer
            var str = "Pagina " + doc.internal.getNumberOfPages();
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === "function") {
              str = str + " de " + totalPagesExp;
              var totalpaginas = totalPagesExp;
            }

            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height
              ? pageSize.height
              : pageSize.getHeight();

            doc.text(
              str + "                        ",
              data.settings.margin.left,
              pageHeight - 10
            );
          },
          margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === "function") {
          doc.putTotalPages(totalPagesExp);
        }

        doc.save("relatorio-estoque-real.pdf")  
        
        }else{

            myFunctions.showAlert('Sem dados para gerar PDF! Verifique!')

        }
    }

})
