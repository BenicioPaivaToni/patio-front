angular.module('app').controller("relatorioFechamentoMotoristasCtrl", function ($scope, $http, urlServidor, myFunctions, $mdDialog) {

  $scope.dados = {
    data_Inicio: new Date(),
    data_Final: new Date(),
    id_motorista: null,
    empresa: null
  }

  const getEmpresas = async () => {
    const response = await $http.put(urlServidor.urlServidorChatAdmin + '/empresas_reboques/listar')
    return await response.data
  }

  async function init() {
    
    $scope.empresas = await getEmpresas()
  }

  init()

  $scope.getMotoristaByEmpresa = async () => {
    const response = await $http.get(urlServidor.urlServidorChatAdmin + '/motoristas/listar-motoristas', { params : { id_empresa_reboque: $scope.dados.empresa } } )
    const { data } = response
    $scope.motoristas = data
  }

  $scope.limpar = () =>{
      
    $scope.dados = {
      data_Inicio: new Date(),
      data_Final: new Date(),
      id_motorista: null,
      empresa: null
    }

    $scope.datalist = []
    $scope.motorista = null
    $scope.empresaReboque = null
    $scope.length = 0

  }

     
  $scope.Buscar = async (dados) => {

    $scope.datalist = []
    $scope.length = 0
    $scope.empresaReboque = null
    
    if (!$scope.dados.empresa) {

      myFunctions.showAlert('Campo "Empresa de Reboque" precisa estar preenchido!')

      return

    }

    const response = await $http.get(urlServidor.urlServidorChatAdmin + '/motoristas/fechamento-empresa', { params: dados })
    const { data } = response
    
    if (data == false) {
      myFunctions.showAlertCustom('Nenhum registro foi encontrado com os parâmetros de pesquisa selecionados!<br/><br/>Verifique e tente novamente!')

      return

    }
    
    $scope.length = data.length
  
    let dataMapped = []

    for(let [index, item] of data.entries()) {
   
      let id = 0

      let dataTmp = data.map((x) => ({
     
        ...x,
     
        id: (id = id + 1),
        valor_guincho: (x.hora_apreensao >= '06:00:00' && x.hora_apreensao <= '18:00:00') ? x.saida_dia : x.saida_noite,
        pendencias: (x.qt_documentos == 0 || x.qt_fotos == 0) ? 1 : 0
          
      }))
      
      dataMapped = dataTmp.map((obj) => ({

        ...obj,
  
        nome_motorista: obj.nome_motorista.slice(0, 20),
        patio: obj.patio.slice(0,30),
        km_excedente: Math.max(0, obj.km_percorrido - obj.km_padrao ) || 0,
        valor_km_excedente: (Math.max(0, obj.km_percorrido - obj.km_padrao ) * obj.valor_km),
        total_receber: ((Math.max(0, obj.km_percorrido - obj.km_padrao ) * obj.valor_km) + 
        obj.valor_pedagio + obj.total_extras + obj.valor_guincho - obj.valor_desconto || 0.0) 
      
      }))
      
      let totalGeral = dataMapped.reduce((n, {total_receber}) => n + total_receber, 0)
      $scope.totalEmpresa = totalGeral
     
    }
 
    let totalMotorista
    $scope.totalMotorista = totalMotorista
    $scope.empresaReboque = dataMapped.map(item => item.nome_empresa_reboque)
    $scope.empresaReboque = $scope.empresaReboque[0]
    $scope.totalRegistros = data.length
                   
    const dadosTmp = []                                       
    let motoristaAtual = null  
    let countTotalMotorista = 0
                
    const motoristaBlue = `<div style="width:100px"><h4 style="font-size:14px;color:0000FF;font-weight: bold"> TOTAL MOTORISTA </h4></div>`
    const motoristaRed = `<div style="width:100px"><h4 style="font-size:14px;color:FF0000;font-weight: bold"> TOTAL MOTORISTA </h4></div>`            
    
    for(let [index, item] of dataMapped.entries()) {
                    
      if(!motoristaAtual) motoristaAtual = item.nome_motorista
                    
      if((motoristaAtual && motoristaAtual != item.nome_motorista)) {

        if (countTotalMotorista >= 0){

          dadosTmp.push({
            nome_motorista: motoristaBlue,
            patio: `<div style="width:100px"><h4 style="font-size:14px;color:0000FF;font-weight: bold"> ${new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(countTotalMotorista)} </h4></div>`            
          })
        
        }else{

          dadosTmp.push({
            nome_motorista: motoristaRed,
            patio: `<div style="width:100px"><h4 style="font-size:14px;color:FF0000;font-weight: bold"> ${new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(countTotalMotorista)} </h4></div>`
          })

        }

        motoristaAtual = item.nome_motorista

        countTotalMotorista = 0
      }

      countTotalMotorista += item.total_receber 
                            
      dadosTmp.push(item)

      if(data.length - 1 === index) {
        
        if (countTotalMotorista >= 0){

          dadosTmp.push({
            nome_motorista: motoristaBlue,
            patio: `<div style="width:100px"><h4 style="font-size:14px;color:0000FF;font-weight: bold"> ${new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(countTotalMotorista)} </h4></div>`
          })

        }else{

          dadosTmp.push({
            nome_motorista: motoristaRed,
            patio: `<div style="width:100px"><h4 style="font-size:14px;color:FF0000;font-weight: bold"> ${new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(countTotalMotorista)} </h4></div>`
          })

        }
    
        const pagamentos = data.filter((x) => x.nome_motorista === motoristaAtual)         

      }
                        
    }
                
   // dadosTmp.push({
   //   nome_motorista: ''
   // }) 
                
    dadosTmp.push({
      nome_motorista: `<div style="width:100px"><h4 style="font-size:14px;color:BF4917;font-weight: bold"> TOTAL EMPRESA </h4></div>`,
      patio: `<div style="width:100px"><h4 style="font-size:14px;color:BF4917;font-weight: bold"> ${new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format($scope.totalEmpresa)} </h4></div>`
            
    })

    let datalistTmp = dadosTmp.map((el) => ({

      ...el,

      valor_pedagio: el.nome_motorista.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.valor_pedagio || 0),
      total_extras: el.nome_motorista.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.total_extras || 0),
      valor_desconto: el.nome_motorista.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.valor_desconto || 0),
      valor_guincho: el.nome_motorista.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.valor_guincho || 0),
      valor_km_excedente: el.nome_motorista.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.valor_km_excedente || 0),
      valor_desconto: el.nome_motorista.includes('TOTAL') ? el.valor_desconto = '' : myFunctions.numberToReal(el.valor_desconto || 0),
      total_receber: el.nome_motorista.includes('TOTAL') ? el.valor_pedagio = '' : new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(el.total_receber || 0)
    
    }))

    $scope.datalist = datalistTmp
  
  }
  

  $scope.checkboxDetalhe = function (ev, rowId) {

    if(!rowId) return false

    var row = $scope.datalist.find((x) => x.id === rowId)

    if(!row) return false
    
    return row.pendencias == 1
    
  }
  

  $scope.hideCheck = function (ev, rowId){
        
    if(!rowId) return true
  }
     


  $scope.buscaExtrasEmpresa = function(ev, rowId, $index) {

    let ncvId = $scope.datalist.find((x) => x.id === rowId).ncv

    $scope.extrasGuincho = []

    $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-extrasguincho', { params: { ncv: ncvId } }).then(function (response) {

      $scope.extrasGuincho = response.data  
 
    })

    $mdDialog.show({ controller: function ($scope, $mdDialog, $mdToast) {
      $scope.cancel = function () {
        $mdDialog.cancel();
        $scope.isDisabled = false ;
      };

      },
      templateUrl: 'extras-guincho.html',
        scope:$scope,
        preserveScope: true,
        scopoAnterior: $scope
      }).then(function (answer) {
        let a = answer;

      }, function () {
        $scope.statusdialog = 'You cancelled the dialog.';
    });
   
  }



  $scope.gerarPlanilha = function () {

    if ($scope.datalist) {

      const excelInfo = $scope.datalist.map(index => ({
      ...index,
      
      patio: index.patio.replace(/(<([^>]+)>)/ig, " "),
      nome_motorista: index.nome_motorista.replace(/(<([^>]+)>)/ig, " "),

    }))

    excelInfo.pop()        

    excelInfo.push({
      nome_motorista: 'TOTAL MOTORISTA',
      patio: $scope.totalMotorista
               
    }) 
      
     alasql('SELECT * INTO XLSX("relatorio-fechamento-motorista-empresa.xlsx",{headers:true}) FROM ? ', [excelInfo])

    } else{

      myFunctions.showAlert('Sem dados para gerar Excel! Verifique!')

    }   

  }


  $scope.gerarPdf = function () {

    if ($scope.datalist) {    

    const PDFInformacoes = $scope.datalist.map(index => ({
      ...index,
    patio: index.patio.replace(/(<([^>]+)>)/ig, " "),
    nome_motorista: index.nome_motorista.replace(/(<([^>]+)>)/ig, " "),
    }))

    var doc = new jsPDF({ orientation: "landscape" });
    var totalPagesExp = '{total_pages_count_string}'

    doc.setFontSize(10);
    doc.autoTable({

    columnStyles: {
      vencimento: { halign: 'left' },
    },

    body: PDFInformacoes,

      columns: [
        { header: 'Motorista', dataKey: 'nome_motorista' },
        { header: 'NCV', dataKey: 'ncv' },
        { header: 'Patio', dataKey: 'patio' },
        { header: 'Placa', dataKey: 'placa' },
        { header: 'Marca/Modelo', dataKey: 'marca_modelo' },
        { header: 'Tipo Veiculo', dataKey: 'tipo_veiculo' },
        { header: 'Remoção', dataKey: 'valor_guincho' },
        { header: 'KM Excedente', dataKey: 'km_excedente' },
        { header: 'Valor Excedente', dataKey: 'valor_km_excedente' },
        { header: 'Pedágio', dataKey: 'valor_pedagio' },
        { header: 'Desconto', dataKey: 'valor_desconto' },
        { header: 'Extras', dataKey: 'total_extras' },
        { header: 'Total Receber', dataKey: 'total_receber' },
      ],
    bodyStyles: {
      margin: 10,
      fontSize: 8,
    },
    didDrawPage: function (data) {
      // Header
      doc.setFontSize(18)
      doc.setTextColor(40)
      doc.setFontStyle('normal')
 
      doc.text('Fechamento de: ' + $scope.dados.data_Inicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.data_Final.toLocaleDateString('pt-BR'), data.settings.margin.left + 5, 18);
      doc.text('Empresa: ' + $scope.empresaReboque, data.settings.margin.left + 5, 25);
      // Footer
      var str = 'Pagina ' + doc.internal.getNumberOfPages()
      // Total page number plugin only available in jspdf v1.0+
      if (typeof doc.putTotalPages === 'function') {
        str = str + ' de ' + totalPagesExp
        var totalpaginas = totalPagesExp;
      }

      var strTotal = ''
      doc.setFontSize(10)

      // jsPDF 1.4+ uses getWidth, <1.4 uses .width
      var pageSize = doc.internal.pageSize
      var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()

      doc.text(str + '                        ' + strTotal + "         Data: " + new Date().getDate() + '/' + (new Date().getMonth()+1) + '/' + new Date().getFullYear() + 
      "     Hora: " + new Date().getHours() + ":" + new Date().getMinutes(), data.settings.margin.left, pageHeight - 10) 
    },
      margin: { top: 30 },
    });

    if (typeof doc.putTotalPages === 'function') {
      doc.putTotalPages(totalPagesExp)
    }

    doc.save('Fechamento-motoristas-empresa.pdf');

    }else{

    myFunctions.showAlert('Sem dados para gerar PDF! Verifique!')

    }

  }



  //ABA PATIO \/

  $scope.dadosPatio = {
    dataInicial: new Date(),
    dataFinal: new Date(),
    id_patio: null,
    id_motorista: null,
    empresa: null
  }

  $scope.limparDadosPatio = () =>{  
    $scope.dadosPatio = {
      dataInicial: new Date(),
      dataFinal: new Date(),
      id_patio: null,
      id_motorista: null,
      empresa: null
    }

    $scope.dadosPatioList = []
    $scope.dadosPatioLength = 0
    $scope.patioPesquisa = null

  }

  $scope.motoristaByEmpresaPatio = async () => {
    const response = await $http.get(urlServidor.urlServidorChatAdmin + '/motoristas/listar-motoristas', { params : { id_empresa_reboque: $scope.dadosPatio.empresa } } )
    const { data } = response
    $scope.motoristasPatio = data
  }


  $scope.BuscarDadosPatio = async (dadosPatio) => {

    $scope.dadosPatioList = []
    $scope.dadosPatioLength = 0
    
    if ( !$scope.dadosPatio.id_patio ) {

      myFunctions.showAlert('Campo Pátio precisa estar preenchido! Verifique!')

      return

    }

    const response = await $http.get(urlServidor.urlServidorChatAdmin + '/motoristas/fechamento-patio',
    { 
      params: {
        data_inicial: $scope.dadosPatio.dataInicial,
        data_final: $scope.dadosPatio.dataFinal,
        id_patio: $scope.dadosPatio.id_patio,
        id_motorista: $scope.dadosPatio.id_motorista,
        empresa: $scope.dadosPatio.empresa
      },
    
    })
    const { data } = response
    
    if (data == false) {
      myFunctions.showAlertCustom('Nenhum registro foi encontrado com os parâmetros de pesquisa selecionados!<br/><br/>Verifique e tente novamente!')

      return

    }
    
    $scope.dadosPatioLength = data.length
    $scope.patioPesquisa = data[0].patio
  
    let dataMapped = []

    for(let [index, item] of data.entries()) {
   
      let id = 0

      let dataTmp = data.map((x) => ({
     
        ...x,
     
        id: (id = id + 1),
        valor_guincho: (x.hora_apreensao >= '06:00:00' && x.hora_apreensao <= '18:00:00') ? x.saida_dia : x.saida_noite,
        pendencias: (x.qt_documentos == 0 || x.qt_fotos == 0) ? 1 : 0
          
      }))
      
      dataMapped = dataTmp.map((obj) => ({

        ...obj,
  
        nome_motorista: obj.nome_motorista.slice(0, 25),
        patio: obj.patio.slice(0, 25),
        marca_modelo: obj.marca_modelo?.slice(0, 10),
        km_excedente: Math.max(0, obj.km_percorrido - obj.km_padrao ) || 0,
        valor_km_excedente: (Math.max(0, obj.km_percorrido - obj.km_padrao ) * obj.valor_km),
        total_receber: ((Math.max(0, obj.km_percorrido - obj.km_padrao ) * obj.valor_km) + 
        obj.valor_pedagio + obj.total_extras + obj.valor_guincho - obj.valor_desconto || 0.0) 
      
      }))
     
    }
 
    let totalMotorista
    $scope.totalMotorista = totalMotorista
    $scope.totalRegistros = data.length
                   
    const dadosTmp = []                                       
    let motoristaAtual = null  
    let countTotalMotorista = 0
                
    const motoristaBlue = `<div style="width:100px"><h4 style="font-size:14px;color:0000FF;font-weight: bold"> TOTAL MOTORISTA </h4></div>`
    const motoristaRed = `<div style="width:100px"><h4 style="font-size:14px;color:FF0000;font-weight: bold"> TOTAL MOTORISTA </h4></div>`            
    
    for(let [index, item] of dataMapped.entries()) {
                    
      if(!motoristaAtual) motoristaAtual = item.nome_motorista
                    
      if((motoristaAtual && motoristaAtual != item.nome_motorista)) {

        if (countTotalMotorista >= 0){

          dadosTmp.push({
            nome_motorista: motoristaBlue,
            patio: `<div style="width:100px"><h4 style="font-size:14px;color:0000FF;font-weight: bold"> ${new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(countTotalMotorista)} </h4></div>`            
          })
        
        }else{

          dadosTmp.push({
            nome_motorista: motoristaRed,
            patio: `<div style="width:100px"><h4 style="font-size:14px;color:FF0000;font-weight: bold"> ${new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(countTotalMotorista)} </h4></div>`
          })

        }

        motoristaAtual = item.nome_motorista

        countTotalMotorista = 0
      }

      countTotalMotorista += item.total_receber 
                            
      dadosTmp.push(item)

      if(data.length - 1 === index) {
        
        if (countTotalMotorista >= 0){

          dadosTmp.push({
            nome_motorista: motoristaBlue,
            patio: `<div style="width:100px"><h4 style="font-size:14px;color:0000FF;font-weight: bold"> ${new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(countTotalMotorista)} </h4></div>`
          })

        }else{

          dadosTmp.push({
            nome_motorista: motoristaRed,
            patio: `<div style="width:100px"><h4 style="font-size:14px;color:FF0000;font-weight: bold"> ${new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(countTotalMotorista)} </h4></div>`
          })

        }        

      }
                        
    }
   
    let datalistTmp = dadosTmp.map((el) => ({

      ...el,

      valor_pedagio: el.nome_motorista.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.valor_pedagio || 0),
      total_extras: el.nome_motorista.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.total_extras || 0),
      valor_desconto: el.nome_motorista.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.valor_desconto || 0),
      valor_guincho: el.nome_motorista.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.valor_guincho || 0),
      valor_km_excedente: el.nome_motorista.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.valor_km_excedente || 0),
      valor_desconto: el.nome_motorista.includes('TOTAL') ? el.valor_desconto = '' : myFunctions.numberToReal(el.valor_desconto || 0),
      total_receber: el.nome_motorista.includes('TOTAL') ? el.valor_pedagio = '' : new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(el.total_receber || 0)
    
    }))

    $scope.dadosPatioList = datalistTmp
  
  }


  $scope.buscaExtrasPatio = function(ev, rowId, $index) {

    let ncvId = $scope.dadosPatioList.find((x) => x.id === rowId).ncv

    $scope.extrasPatioGuincho = []

    $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-extrasguincho', { params: { ncv: ncvId } }).then(function (response) {

      $scope.extrasPatioGuincho = response.data
 
    })

    $mdDialog.show({ controller: function ($scope, $mdDialog, $mdToast) {
      $scope.cancel = function () {
        $mdDialog.cancel();
        $scope.isDisabled = false ;
      };

      },
      templateUrl: 'extras-patio-guincho.html',
        scope:$scope,
        preserveScope: true,
        scopoAnterior: $scope
      }).then(function (answer) {
        let a = answer;

      }, function () {
        $scope.statusdialog = 'You cancelled the dialog.';
    });
   
  }


  $scope.editarNcvPatio = function (ev, rowId){
    var row = $scope.dadosPatioList.find((x) => x.id === rowId)
    window.open("/#/ncv-editar?id " + row.ncv, "_blank")
  }



  $scope.checkboxPatioDetalhe = function (ev, rowId) {
    if(!rowId) return false
    var row = $scope.dadosPatioList.find((x) => x.id === rowId)
    if(!row) return false 
    return row.pendencias == 1
    
  }
  

  $scope.hidePatioCheck = function (ev, rowId){      
    if(!rowId) return true
  }



  $http.put(urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
          {
            idUsuario: localStorage.getItem("id_usuario"),
            idEmpresa: localStorage.getItem("id_empresa"),
          }
        ).then(({ data }) => {$scope.patios = data.filter((item) => {
          return item.status == !0;
      })
  })


  $scope.gerarPatioPlanilha = function () {

    if ($scope.dadosPatioList) {

      const excelInfo = $scope.dadosPatioList.map(index => ({
      ...index,
      
      patio: index.patio.replace(/(<([^>]+)>)/ig, " "),
      nome_motorista: index.nome_motorista.replace(/(<([^>]+)>)/ig, " "),

    }))
  
    excelInfo.push({
      nome_motorista: 'TOTAL MOTORISTA',
      patio: $scope.totalMotorista
               
    }) 
      
     alasql('SELECT * INTO XLSX("relatorio-fechamento-motoristas-patio.xlsx",{headers:true}) FROM ? ', [excelInfo])

    } else{

      myFunctions.showAlert('Sem dados para gerar Excel! Verifique!')

    }   

  }



  $scope.gerarPatioPdf = function () {

    if ($scope.dadosPatioList) {    

    const PDFInformacoes = $scope.dadosPatioList.map(index => ({
      ...index,
    patio: index.patio.replace(/(<([^>]+)>)/ig, " "),
    nome_motorista: index.nome_motorista.replace(/(<([^>]+)>)/ig, " "),
    }))

    var doc = new jsPDF({ orientation: "landscape" });
    var totalPagesExp = '{total_pages_count_string}'

    doc.setFontSize(10);
    doc.autoTable({

    columnStyles: {
      vencimento: { halign: 'left' },
    },

    body: PDFInformacoes,

      columns: [
        { header: 'Motorista', dataKey: 'nome_motorista' },
        { header: 'NCV', dataKey: 'ncv' },
        { header: 'Patio', dataKey: 'patio' },
        { header: 'Placa', dataKey: 'placa' },
        { header: 'Marca/Modelo', dataKey: 'marca_modelo' },
        { header: 'Tipo Veiculo', dataKey: 'tipo_veiculo' },
        { header: 'Remoção', dataKey: 'valor_guincho' },
        { header: 'KM Excedente', dataKey: 'km_excedente' },
        { header: 'Valor Excedente', dataKey: 'valor_km_excedente' },
        { header: 'Pedágio', dataKey: 'valor_pedagio' },
        { header: 'Desconto', dataKey: 'valor_desconto' },
        { header: 'Extras', dataKey: 'total_extras' },
        { header: 'Total Receber', dataKey: 'total_receber' },
      ],
    bodyStyles: {
      margin: 10,
      fontSize: 8,
    },
    didDrawPage: function (data) {
      // Header
      doc.setFontSize(18)
      doc.setTextColor(40)
      doc.setFontStyle('normal')

      doc.text('Fechamento de: ' + $scope.dadosPatio.dataInicial.toLocaleDateString('pt-BR') + ' a ' + $scope.dadosPatio.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 5, 18);
      doc.text('Pátio: ' + $scope.patioPesquisa, data.settings.margin.left + 5, 25);

      // Footer
      var str = 'Pagina ' + doc.internal.getNumberOfPages()
      // Total page number plugin only available in jspdf v1.0+
      if (typeof doc.putTotalPages === 'function') {
        str = str + ' de ' + totalPagesExp
        var totalpaginas = totalPagesExp;
      }

      var strTotal = ''
      doc.setFontSize(10)

      // jsPDF 1.4+ uses getWidth, <1.4 uses .width
      var pageSize = doc.internal.pageSize
      var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()

      doc.text(str + '                        ' + strTotal + "         Data: " + new Date().getDate() + '/' + (new Date().getMonth()+1) + '/' + new Date().getFullYear() + 
      "     Hora: " + new Date().getHours() + ":" + new Date().getMinutes(), data.settings.margin.left, pageHeight - 10) 
    },
      margin: { top: 30 },
    });

    if (typeof doc.putTotalPages === 'function') {
      doc.putTotalPages(totalPagesExp)
    }

    doc.save('Fechamento-Motoristas-Patio.pdf');

    }else{

    myFunctions.showAlert('Sem dados para gerar PDF! Verifique!')

    }

  }




  //ABA MOTORISTA \/

  $scope.dadosMotorista = {
    dataInicial: new Date(),
    dataFinal: new Date(),
    id_patio: null,
    id_motorista: null,
    empresa: null
  }

  $scope.limparDadosMotorista = () =>{  
    $scope.dadosMotorista = {
      dataInicial: new Date(),
      dataFinal: new Date(),
      id_motorista: null,
     
    }

    $scope.dadosMotoristaList = []
    $scope.dadosMotoristaLength = 0
    $scope.motoristaPesquisa = null
 
  }

  
  $http.put(urlServidor.urlServidorChatAdmin + '/motoristas/listar', {idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa')}).then(function (response) {
    
    $scope.listaMotoristas = response.data.filter((item ) => {
      return item.id_usuario_app !== null
    })
   
  });
 
  

  $scope.buscarDadosMotorista = async () => {

    $scope.dadosMotoristaList = []
    $scope.dadosMotoristaLength = 0
    
    if ( !$scope.dadosMotorista.id_motorista ) {

      myFunctions.showAlert('Campo Motorista precisa estar preenchido! Verifique!')

      return

    }

    const response = await $http.get(urlServidor.urlServidorChatAdmin + '/motoristas/fechamento-motorista',    
    { 
      params: {
        data_inicial: $scope.dadosMotorista.dataInicial,
        data_final: $scope.dadosMotorista.dataFinal,
        id_motorista: $scope.dadosMotorista.id_motorista
      },
    
    })
    const { data } = response
    
    if (data == false) {
      myFunctions.showAlertCustom('Nenhum registro foi encontrado com os parâmetros de pesquisa selecionados!<br/><br/>Verifique e tente novamente!')

      return

    }
    
    $scope.dadosMotoristaLength = data.length
    $scope.motoristaPesquisa = data[0].nome_motorista
  
    let dataMapped = []

    for(let [index, item] of data.entries()) {
   
      let id = 0

      let dataTmp = data.map((x) => ({
     
        ...x,
     
        id: (id = id + 1),
        valor_guincho: (x.hora_apreensao >= '06:00:00' && x.hora_apreensao <= '18:00:00') ? x.saida_dia : x.saida_noite,
        pendencias: (x.qt_documentos == 0 || x.qt_fotos == 0) ? 1 : 0
          
      }))
      
      dataMapped = dataTmp.map((obj) => ({

        ...obj,
  
        nome_motorista: obj.nome_motorista.slice(0, 25),
        patio: obj.patio.slice(0, 25),
        marca_modelo: obj.marca_modelo?.slice(0, 10),
        km_excedente: Math.max(0, obj.km_percorrido - obj.km_padrao ) || 0,
        valor_km_excedente: (Math.max(0, obj.km_percorrido - obj.km_padrao ) * obj.valor_km),
        total_receber: ((Math.max(0, obj.km_percorrido - obj.km_padrao ) * obj.valor_km) + 
        obj.valor_pedagio + obj.total_extras + obj.valor_guincho - obj.valor_desconto || 0.0) 
      
      }))

      let totalGeral = dataMapped.reduce((n, {total_receber}) => n + total_receber, 0)
      $scope.totalMotorista = totalGeral
    }
 
    let totalPatio
    $scope.totalPatio = totalPatio
    $scope.totalRegistros_ = data.length
                   
    const dadosTmp = []                                       
    let patioAtual = null  
    let countTotalPatio = 0
                
    const patioBlue = `<div style="width:100px"><h4 style="font-size:14px;color:0000FF;font-weight: bold"> TOTAL PATIO </h4></div>`
    const patioRed = `<div style="width:100px"><h4 style="font-size:14px;color:FF0000;font-weight: bold"> TOTAL PATIO </h4></div>`            
    
    for(let [index, item] of dataMapped.entries()) {
                    
      if(!patioAtual) patioAtual = item.patio
                    
      if((patioAtual && patioAtual != item.patio)) {

        if (countTotalPatio >= 0){

          dadosTmp.push({
            patio: patioBlue,
            placa: `<div style="width:100px"><h4 style="font-size:14px;color:0000FF;font-weight: bold"> ${new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(countTotalPatio)} </h4></div>`            
          })
        
        }else{

          dadosTmp.push({
            patio: patioRed,
            placa: `<div style="width:100px"><h4 style="font-size:14px;color:FF0000;font-weight: bold"> ${new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(countTotalPatio)} </h4></div>`
          })

        }

        patioAtual = item.patio

        countTotalPatio = 0
      }

      countTotalPatio += item.total_receber 
                            
      dadosTmp.push(item)

      if(data.length - 1 === index) {
        
        if (countTotalPatio >= 0){

          dadosTmp.push({
            patio: patioBlue,
            placa: `<div style="width:100px"><h4 style="font-size:14px;color:0000FF;font-weight: bold"> ${new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(countTotalPatio)} </h4></div>`
          })

        }else{

          dadosTmp.push({
            patio: patioRed,
            placa: `<div style="width:100px"><h4 style="font-size:14px;color:FF0000;font-weight: bold"> ${new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(countTotalPatio)} </h4></div>`
          })

        }        

      }
                        
    }

    dadosTmp.push({
      patio: `<div style="width:100px"><h4 style="font-size:14px;color:BF4917;font-weight: bold"> TOTAL MOTORISTA </h4></div>`,
      placa: `<div style="width:100px"><h4 style="font-size:14px;color:BF4917;font-weight: bold"> ${new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format($scope.totalMotorista)} </h4></div>`
            
    })


   
    let datalistTmp = dadosTmp.map((el) => ({

      ...el,

      valor_pedagio: el.patio.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.valor_pedagio || 0),
      total_extras: el.patio.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.total_extras || 0),
      valor_desconto: el.patio.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.valor_desconto || 0),
      valor_guincho: el.patio.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.valor_guincho || 0),
      valor_km_excedente: el.patio.includes('TOTAL') ? el.valor_pedagio = '' : myFunctions.numberToReal(el.valor_km_excedente || 0),
      valor_desconto: el.patio.includes('TOTAL') ? el.valor_desconto = '' : myFunctions.numberToReal(el.valor_desconto || 0),
      total_receber: el.patio.includes('TOTAL') ? el.valor_pedagio = '' : new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(el.total_receber || 0)
    
    }))

    $scope.dadosMotoristaList = datalistTmp
  
  }


  $scope.buscaExtrasMotorista = function(ev, rowId, $index) {

    let ncvId = $scope.dadosMotoristaList.find((x) => x.id === rowId).ncv

    $scope.extrasMotoristaGuincho = []

    $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-extrasguincho', { params: { ncv: ncvId } }).then(function (response) {

      $scope.extrasMotoristaGuincho = response.data
 
    })

    $mdDialog.show({ controller: function ($scope, $mdDialog, $mdToast) {
      $scope.cancel = function () {
        $mdDialog.cancel();
        $scope.isDisabled = false ;
      };

      },
      templateUrl: 'extras-motorista-guincho.html',
        scope:$scope,
        preserveScope: true,
        scopoAnterior: $scope
      }).then(function (answer) {
        let a = answer;

      }, function () {
        $scope.statusdialog = 'You cancelled the dialog.';
    });
   
  }


  $scope.editarNcvMotorista = function (ev, rowId){
    var row = $scope.dadosMotoristaList.find((x) => x.id === rowId)
    window.open("/#/ncv-editar?id " + row.ncv, "_blank")
  }



  $scope.motoristaPendenciasDetalhe = function (ev, rowId) {
    if(!rowId) return false
    var row = $scope.dadosMotoristaList.find((x) => x.id === rowId)
    if(!row) return false 
    return row.pendencias == 1
    
  }
  

  $scope.hideMotoristaCheck = function (ev, rowId){      
    if(!rowId) return true
  }


  $scope.gerarMotoristaPlanilha = function () {

    if ($scope.dadosMotoristaList) {

      const excelInfo = $scope.dadosMotoristaList.map(index => ({
      ...index,
      
      patio: index.patio.replace(/(<([^>]+)>)/ig, " "),
      placa: index.placa.replace(/(<([^>]+)>)/ig, " "),

    }))
  /*
    excelInfo.push({
      nome_motorista: 'TOTAL MOTORISTA',
      placa: $scope.totalMotorista
               
    })
    */
      
     alasql('SELECT * INTO XLSX("relatorio-fechamento-motoristas.xlsx",{headers:true}) FROM ? ', [excelInfo])

    } else{

      myFunctions.showAlert('Sem dados para gerar Excel! Verifique!')

    }   

  }



  $scope.gerarMotoristaPdf = function () {

    if ($scope.dadosMotoristaList) {    

    const PDFInformacoes = $scope.dadosMotoristaList.map(index => ({
      ...index,
      patio: index.patio.replace(/(<([^>]+)>)/ig, " "),
      placa: index.placa.replace(/(<([^>]+)>)/ig, " "),
    }))

    var doc = new jsPDF({ orientation: "landscape" });
    var totalPagesExp = '{total_pages_count_string}'

    doc.setFontSize(10);
    doc.autoTable({

    columnStyles: {
      vencimento: { halign: 'left' },
    },

    body: PDFInformacoes,

      columns: [
        { header: 'Patio', dataKey: 'patio' },
        { header: 'NCV', dataKey: 'ncv' },
        { header: 'Placa', dataKey: 'placa' },
        { header: 'Marca/Modelo', dataKey: 'marca_modelo' },
        { header: 'Tipo Veiculo', dataKey: 'tipo_veiculo' },
        { header: 'Remoção', dataKey: 'valor_guincho' },
        { header: 'KM Excedente', dataKey: 'km_excedente' },
        { header: 'Valor Excedente', dataKey: 'valor_km_excedente' },
        { header: 'Pedágio', dataKey: 'valor_pedagio' },
        { header: 'Desconto', dataKey: 'valor_desconto' },
        { header: 'Extras', dataKey: 'total_extras' },
        { header: 'Total Receber', dataKey: 'total_receber' },
      ],
    bodyStyles: {
      margin: 10,
      fontSize: 8,
    },
    didDrawPage: function (data) {
      // Header
      doc.setFontSize(18)
      doc.setTextColor(40)
      doc.setFontStyle('normal')

      doc.text('Fechamento de: ' + $scope.dadosMotorista.dataInicial.toLocaleDateString('pt-BR') + ' a ' + $scope.dadosMotorista.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 5, 18);
      doc.text('Motorista: ' + $scope.motoristaPesquisa, data.settings.margin.left + 5, 25);

      // Footer
      var str = 'Pagina ' + doc.internal.getNumberOfPages()
      // Total page number plugin only available in jspdf v1.0+
      if (typeof doc.putTotalPages === 'function') {
        str = str + ' de ' + totalPagesExp
        var totalpaginas = totalPagesExp;
      }

      var strTotal = ''
      doc.setFontSize(10)

      // jsPDF 1.4+ uses getWidth, <1.4 uses .width
      var pageSize = doc.internal.pageSize
      var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()

      doc.text(str + '                        ' + strTotal + "         Data: " + new Date().getDate() + '/' + (new Date().getMonth()+1) + '/' + new Date().getFullYear() + 
      "     Hora: " + new Date().getHours() + ":" + new Date().getMinutes(), data.settings.margin.left, pageHeight - 10) 
    },
      margin: { top: 30 },
    });

    if (typeof doc.putTotalPages === 'function') {
      doc.putTotalPages(totalPagesExp)
    }

    doc.save('Fechamento-Motoristas.pdf');

    }else{

    myFunctions.showAlert('Sem dados para gerar PDF! Verifique!')

    }

  }


});