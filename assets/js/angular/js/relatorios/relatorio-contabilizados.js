
angular.module('app').controller('relatorioContabilizadosCtrl', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.dados = {
        dataInicio: null,
        dataFinal: null,

    }

    let hoje = new Date()
    $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
    $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    $scope.mesAtual = function (ev, id) {
        $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
        $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    };
    
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("relatorio-contabilizados.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }

    $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', {idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa')}).then(function (response) {
        $scope.patios = response.data;
    });
    $scope.pesquisar = function (param1) {
        var dtInicio = $scope.dados.dataInicio;
    var dtFinal = $scope.dados.dataFinal;

        $scope.dadosList = null;
        $http.get(urlServidor.urlServidorChatAdmin + '/pagar/lista-contabilizado', {params: $scope.dados}).then(function (response) {
            $scope.dadosList = response.data;
        });
    };
    $scope.gerarPdf = function () {
        var doc = new jsPDF({orientation: "landscape"});
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(10);
        doc.autoTable({

            columnStyles: { 
                vencimento: {halign: 'left'},
              },
           
            body: $scope.dadosList,
            columns: [
                {header: 'Documento', dataKey: 'documento'},
                {header: 'Vencimento', dataKey: 'vencimento'}, 
                {header: 'Categoria', dataKey: 'desc_categoria'},
                {header: 'Grupo', dataKey: 'desc_grupo'},
                {header: 'Valor', dataKey: 'valor'},
                {header: 'Acrescimo', dataKey: 'acrescimo'},
                {header: 'Desconto', dataKey: 'desconto'},
                {header: 'Status', dataKey: 'status'},
                {header: 'Classif.', dataKey: 'classificacao'},
                {header: 'Razão', dataKey: 'razao_nome'},
                {header: 'Pátio', dataKey: 'nome'},
                {header: 'Forma de Pag.', dataKey: 'desc_tiporecebimento'},
                
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                doc.text('Contabilizados no periodo de ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22);

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                    var totalpaginas = totalPagesExp ;
                }

             
                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()

                doc.text(str+'                        ', data.settings.margin.left, pageHeight - 10)
            },
            margin: {top: 30},
        });
        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }
        doc.save('relatorio-contabilizados.pdf')
    }



});
