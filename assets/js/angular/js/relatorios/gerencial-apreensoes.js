angular
  .module("app")
  .controller(
    "gerencialApreensoesCtrl",
    function ($scope, $mdDialog, $http, urlServidor, myFunctions, $mdDialog) {
      $scope.dados = {
        dataInicio: new Date(),
        dataFinal: new Date(),
        patio: null,
        id_grupo_patios: null,
      };

      $scope.ShowMessage = false;

      $scope.limpar = function (ev) {
        $scope.dados.dataInicio = new Date();
        $scope.dados.dataFinal = new Date();
        $scope.dados.patio = null;
        $scope.dados.id_grupo_patios = null;
      };

      const buscaPatiosGrupo = async () => {
        const response = await $http.get(
          `${urlServidor.urlServidorChatAdmin}/grupos-patios/listar`
        );
        const { data } = response;
        return data.filter((item) => item.ativo == 1);
      };

      buscaPatiosGrupo().then((Response) => ($scope.patiosGrupo = Response));

      $http
        .put(urlServidor.urlServidorChatAdmin + "/tipos-recebimento/listar")
        .then(({ data }) => {
          $scope.formasPagto = data.filter((item) => item.tipo === "PAGAR");
        });
      $scope.mesAtual = function (ev, id) {
        let hoje = new Date();

        $scope.dados.dataInicio = new Date(
          hoje.getFullYear(),
          hoje.getMonth(),
          01
        );
        $scope.dados.dataFinal = new Date(
          hoje.getFullYear(),
          hoje.getMonth() + 1,
          0
        );
      };

      $http
        .put(
          urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
          {
            idUsuario: localStorage.getItem("id_usuario"),
            idEmpresa: localStorage.getItem("id_empresa"),
          }
        )
        .then( ({ data })  => {
          $scope.patios = data.filter((item) => item.status == 1);
        });

      $scope.pesquisar = async () => {
        if ($scope.dados.patio || $scope.dados.id_grupo_patios) {
          $scope.ShowSpinnerLoad = true;
          const response = await $http.get(
            urlServidor.urlServidorChatAdmin + "/ncv/relatorio-apreensoes",
            { params: $scope.dados }
          );
          const { data } = response;

          if (data.length == 0) {
            $scope.ShowSpinnerLoad = false;
            myFunctions.showAlert(
              "Filtro não encontrou registros! Revise parâmetros da pesquisa!"
            );
            return;
          }

          $scope.dadosApreensoes = data;
          $scope.apreendidosTotalGeral = $scope.dadosApreensoes.filter(
            (item) => item.data_liberacao !== "00/00/0000"
          );

          $scope.SaldoGeral = Math.abs(
            Math.round(
              (($scope.apreendidosTotalGeral.length -
                $scope.dadosApreensoes.length) /
                $scope.dadosApreensoes.length) *
                100
            )
          ).toFixed(2);

          const Lancamentos = data.map((item) => ({
            ...item,
            tipo_veiculo: item.tipo_veiculo || "Não definido",
          }));

          const groupBy = (x, f) =>
            x.reduce((a, b) => ((a[f(b)] ||= []).push(b), a), {});

          const groupedTypes = groupBy(Lancamentos, (item) => item.patio);

          $scope.dadosLiberacoes = await criaTable(groupedTypes, groupBy);
        } else {
          myFunctions.showAlert(
            "Pátio ou Grupo Pátio Obrigatorio para pesquisa!"
          );
        }
      };

      const criaTable = async (data, groupBy, totais) => {
        for (var prop in data) {
          const group = groupBy(data[prop], (item) => item.tipo_veiculo);
          data[prop] = group;
        }

        const infos = data;

        var totalLiberados;
        var totalApreendidos;

        var apreendidosTotal;
        var liberadosTotal;

        const ultilAcumulador = {
          somaLiberados(items, key) {
            let Liberados = [];
            const elements = key.map((element) => ({ ...items[element] }));

            elements.forEach((item) => {
              Object.keys(item).forEach((obj) => {
                Liberados.push(item[obj]);
              });
            });

            Liberados = Liberados.filter(
              (item) => item.data_liberacao !== "00/00/0000"
            );

            return Liberados;
          },

          somaApreendidos(items, key) {
            let Apreendidos = [];
            const elements = key.map((element) => ({ ...items[element] }));

            elements.forEach((item) => {
              Object.keys(item).forEach((obj) => {
                Apreendidos.push(item[obj]);
              });
            });

            return Apreendidos;
          },
        };

        const infoHtml = Object.keys(infos).map(
          (key) => /*HTML*/ `

              

          
            <div class="patio" style="page-break-after: always;">
            <p>${key}</p> 
            <p> Apreendidos ${(apreendidosTotal =
              ultilAcumulador.somaApreendidos(
                infos[key],
                Object.keys(infos[key])
              ).length)}
              



            </p>
            <p> Liberados  : ${(liberadosTotal = ultilAcumulador.somaLiberados(
              infos[key],
              Object.keys(infos[key])
            ).length)} 

            </p>
            <p> 
            ${Math.abs(
              Math.round(
                ((liberadosTotal - apreendidosTotal) / apreendidosTotal) * 100
              )
            ).toFixed(2)}%

         
          
          <p/>

          </div>

            ${Object.keys(infos[key])
              .map(
                (info) =>
                  `
            <div class="categoria">
            <p>${info}</p>
            <p> Apreendidos : ${(totalApreendidos =
              infos[key][info].length)} </p>
               
            
            <p>Liberados : ${(totalLiberados = infos[key][info].filter(
              (item) => item.data_liberacao !== "00/00/0000"
            ).length)}</p>

                <p> 
                Saldo   

                  ${Math.abs(
                    Math.round(
                      ((totalLiberados - totalApreendidos) / totalApreendidos) *
                        100
                    )
                  ).toFixed(2)}%
                
                
      
                <p/>

          </div>

         



          <div class="result" >
                     

                ${infos[key][info]
                  .map(
                    (item, idx) =>
                      `   
                        <div class="resultsRow">
                        <div class="number">
                        <h3 class="title">${idx === 0 ? `NCV` : ``}</h3>
                            <h4>${item.id}</h4>
                        </div>

                        <div>
                        <h3 class="title">${idx === 0 ? `Placa` : ``}</h3>
                            <h4>${item.placa}</h4>
                        </div>
                       
                        <div>

                        <h3 class="title">${idx === 0 ? `Apreensão` : ``}</h3>

                        <h4>${item.data_apreensao}</h4>
                        </div>

                        <div>
                        <h3 class="title">${idx === 0 ? `Liberação` : ``}</h3>
                            <h4>${item.data_liberacao}</h4>
                        </div>


                        <div class="marca_modelo">
                            <h3 class="title">${
                              idx === 0 ? `Descrição` : ``
                            }</h3>
                            <h4>${item.marca_modelo}</h4>
                        </div>



                        <div class="number">
                            <h3 class="title">${idx === 0 ? `Ano` : ``}</h3>
                            <h4>${item.ano}</h4>
                        </div>


                        <div class="number">
                            <h3 class="title">${idx === 0 ? `Cor` : ``}</h3>
                            <h4>${item.cor}</h4>
                        </div>


                        <div class="number">
                        <h3 class="title">${idx === 0 ? `KM Perc` : ``}</h3>
                        <h4>${item.km_percorrido}</h4>
                        </div>
                        <div class="number">
                        <h3 class="title">${idx === 0 ? `Estadias` : ``}</h3>
                        <h4>${item.estadias || 0}</h4>
                        </div>

                         
                        <div class="number">
                        <h3 class="title">${idx === 0 ? `Acrescimo` : ``}</h3>
                        <h4>${item.acrescimo || 0}</h4>
                        </div>

                        <div class="number">
                        <h3 class="title">${idx === 0 ? `Desconto` : ``}</h3>
                        <h4>${item.desconto || 0}</h4>
                        </div>


                        <div class="number">
                        <h3 class="title">${idx === 0 ? `Estadia` : ``}</h3>
                        <h4>${item.valor_estadia || 0}</h4>
                        </div>


                        <div class="number">
                        <h3 class="title">${idx === 0 ? `Guincho` : ``}</h3>
                        <h4>${item.valor_guincho || 0}</h4>
                        </div>


                        <div class="number">
                        <h3 class="title">${idx === 0 ? `Total` : ``}</h3>
                        <h4>${item.total || 0}</h4>
                        </div>
                        </div>



                                     
                    
                    
                    `
                  )
                  .join("\n")}
                
            </div>
            
                    
                    `
              )
              .join("\n")}
          
            

            `
        );

        const tableMain = document.getElementById("tableMain");
        tableMain.innerHTML = infoHtml.join("\n");

        const tableMainPDF = document.getElementById("tableMainPDF");
        tableMainPDF.innerHTML = infoHtml.join("\n");
      };

      $scope.gerarPlanilha = function () {
        console.log($scope.dadosApreensoes);
        const dadosApreensoes = $scope.dadosApreensoes.map((item) => ({
          patio: item.patio,
          Ncv: item.id,
          placa: item.placa,
          data_apreensao: item.data_apreensao,
          data_liberacao: item.data_liberacao,
          marca_modelo: item.marca_modelo,
          ano: item.ano,
          cor: item.cor,
          km_percorrido: item.km_percorrido,
          chassi: item.chassi,
          Estadias: item.estadias,
          Acrescimo: item.acrescimo,
          Desconto: item.Desconto,

          valor_estadia: item.valor_estadia,
          valor_guincho: item.valor_guincho,
          total: item.total,
        }));

        const getTotal = Math.round(
          dadosApreensoes.reduce(
            (previousValue, currentValue) => previousValue + currentValue.total,
            0
          )
        ).toFixed(2);

        dadosApreensoes.sort((a, b) => {
          if (a.patio > b.patio) {
            return 1;
          }
          if (a.patio < b.patio) {
            return -1;
          }
          return 0;
        });

        dadosApreensoes.push({
          patio: "TOTAL GERAL",
          total: getTotal,
        });

        alasql(
          'SELECT * INTO XLSX("relatorio-apreensoes.xlsx",{headers:true}) FROM ?',
          [dadosApreensoes]
        );
      };

      function sleep(milliseconds) {
        return new Promise((resolve) => setTimeout(resolve, milliseconds));
      }

      $scope.gerarPdf = () => {
        const resultlength = $scope.dadosApreensoes.length;
        var TimeOut;
        if (resultlength < 100) {
          TimeOut = 1000;
        } else if (resultlength < 800) {
          TimeOut = 2000;
        } else if (resultlength < 1000) {
          TimeOut = 5000;
        }

        setTimeout(async () => {
          document.getElementById("tableBodyPDF").style.display = "block";
          document.getElementById("Modal").style.display = "block";

          var pdf = new jsPDF({ orientation: "landscape" });
          const thumbnailsHtml = document.getElementById("tableBodyPDF");
          await sleep(700);

          const canvas = await html2canvas(thumbnailsHtml, {
            windowWidth: 1700,
            windowHeight: 1500,
            scrollX: 0,
            scrollY: 0,
            allowTaint: true,
          });

          const imgData = canvas.toDataURL("image/jpeg", 1.0);

          await sleep(700);
          const imgProps = pdf.getImageProperties(imgData);

          const pdfWidth = 300; /* this.pdf.internal.pageSize.getWidth() */
          const pageHeight = pdf.internal.pageSize.getHeight();
          const imgHeight =
            (imgProps.height * pdfWidth) / 1000; /* imgProps.width */
          var heightLeft = imgHeight;
          var position = 0;

          pdf.addImage(
            imgData,
            "JPEG",
            0, // Position of image from left and right
            0, // Position of image from top and bottom
            pdfWidth,
            imgHeight,
            undefined,
            "SLOW"
          );
          heightLeft -= pageHeight;
          while (heightLeft >= 0) {
            position = heightLeft - imgHeight;
            pdf.addPage();
            pdf.addImage(
              imgData,
              "JPEG",
              0,
              position + 0,
              pdfWidth,
              imgHeight,
              undefined,
              "SLOW"
            );
            heightLeft -= pageHeight;
          }
          $scope.ShowMessage = false;

          pdf.save("relatorio-gerencial-apreensoes.pdf");
          document.getElementById("tableBodyPDF").style.display = "none";
          document.getElementById("Modal").style.display = "none";

          return;
        }, TimeOut);
      };
    }
  );
