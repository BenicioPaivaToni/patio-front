
angular.module('app')
    .controller('relatorioLiberacoes', function ($scope, $mdDialog, $http, urlServidor, myFunctions) {

        $scope.dados = {

            dataInicio: new Date(),
            dataFinal: new Date(),
            patio: null,
            detalhes: false,
            tipo_liberacao: "",
            id_grupo_patios: null,
            forma_pagto: null,
            tipo_usuario: localStorage.getItem('tipo_usuario')
            
        }
        $scope.ShowSpinnerLoad = false

        const buscaPatiosGrupo = async () => {
            const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/grupos-patios/listar`,)

            const { data } = response;
            return data.filter((item) => item.ativo == 1);
        }

        buscaPatiosGrupo().then((Response) => $scope.patiosGrupo = Response)



        $scope.mesAtual = function (ev, id) {
            let hoje = new Date()

            $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 0o1);
            $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
        };

        $scope.limpar = function (ev) {

            $scope.dados.dataInicio = new Date()
            $scope.dados.dataFinal = new Date()
            $scope.dados.patio = null
            $scope.dados.detalhes = null
            $scope.dados.forma_pagto = null
            $scope.dados.tipo_liberacao = "LIBERADO"
            $scope.dados.id_grupo_patios = null
            $scope.quantidadeRegistros = null
            $scope.dadosLiberacoes = null
            $scope.totalGeral = null
            $scope.dadosPagamentos = null

        };


        $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') }).then( ({data}) => {
            $scope.patios = data.filter((item) => item.status == 1);
        });


        $http.put(urlServidor.urlServidorChatAdmin + '/tipos-recebimento/listar').then(function (response) {
            
            let tmpFormaPgto = response.data.filter( el => !el.tipo.includes('PAGAR'))

            $scope.formasPagto = tmpFormaPgto
            
        });



        $scope.pesquisar = async () => {
            $scope.dadosLiberacoes && delete $scope.dadosLiberacoes
            $scope.dadosPagamentos = null
            $scope.totalGeral = null
            $scope.totalExcel = null
            $scope.dadosLiberacoes = null
            $scope.planilhaValor = null
            if (($scope.dados.patio) || ($scope.dados.id_grupo_patios)) {
                
                $scope.ShowSpinnerLoad = true
                const response = await $http.get(urlServidor.urlServidorChatAdmin + '/ncv/relatorio-liberacoes-novo', { params: $scope.dados })
                const { data } = response           
                $scope.planilhaValor = data[1]
                if (data[1].length > 0){

                let separaTotais = data[0].shift()  
                $scope.totaisExcel = separaTotais

                if (!data[0].length){
                   
                    $scope.ShowSpinnerLoad = false
                    myFunctions.showAlert('Filtro não encontrou registros! Revise parâmetros da pesquisa!')

                }else{
                    
                    $scope.quantidadeRegistros = data[0]
                                         
                    let geraTotal = separaTotais.total
                    geraTotal = geraTotal.replace(/,/g, '')
                    $scope.totalExcel = geraTotal
                    geraTotal = (parseFloat(geraTotal, 10))
                    $scope.totalGeral = geraTotal
                        
                    $scope.dadosLiberacoes = data[0]                      
                    
                    const dadosTmp = []
                    const patio = "TOTAL PATIO"
                    let patioAtual = null  
                    let countTotalPatio = 0

                    for(let [index, item] of data[0].entries()) {
                        
                        if(!patioAtual) patioAtual = item.patio
                            
                        if((patioAtual && patioAtual != item.patio)) {
                                
                            dadosTmp.push({
                                patio: patio,
                                id: myFunctions.numberToReal(countTotalPatio),
                                color: '#0613cc',
                                fontWeight:'bold',
                            })
                             

                            let recebimentosPatio = data[2].filter( el => {
                                return el.patio === patioAtual
                            })

                           
                            for(let item of recebimentosPatio) {
                                dadosTmp.push({
                                    patio: item.descricao,
                                    color: '#0da68c', 
                                    fontWeight:'bold',
                                    id: item.total
                                })
                            }

                            dadosTmp.push({
                                patio: ''
                            })

                            patioAtual = item.patio

                            countTotalPatio = 0
                        }

                        countTotalPatio += parseFloat(item.total.replace(',', '.'))
                                
                        dadosTmp.push(item)

                        if(data[0].length - 1 === index) {
                            dadosTmp.push({
                                patio: patio,
                                id: myFunctions.numberToReal(countTotalPatio),
                                color: '#0613cc',
                                fontWeight:'bold',
                                
                            })

                            let listaRecebimentos = data[2].filter( el => {
                                return el.patio === patioAtual
                            })


                            for(let item of listaRecebimentos) {
                                dadosTmp.push({
                            
                                    patio: item.descricao,
                                    color: '#0da68c', 
                                    fontWeight:'bold',
                                    id: item.total,
                                })
                            }

                        }
                            
                    }
                       
                    
                    dadosTmp.push({
                        patio: ''
                    })

                    dadosTmp.push({
                        patio: 'TOTAL GERAL',
                        id: myFunctions.numberToReal(parseFloat(separaTotais.total.replace(/,/g, ''))),
                        acrescimo: myFunctions.numberToReal(parseFloat(separaTotais.acrescimo.replace(/,/g, ''))),
                        desconto: myFunctions.numberToReal(parseFloat(separaTotais.desconto.replace(/,/g, ''))),
                        total_diarias: myFunctions.numberToReal(parseFloat(separaTotais.total_diarias.replace(/,/g, ''))),
                        valor_guincho: myFunctions.numberToReal(parseFloat(separaTotais.valor_guincho.replace(/,/g, ''))),
                        total: myFunctions.numberToReal(parseFloat(separaTotais.total.replace(/,/g, ''))),
                        color: '#bf4917',
                        fontSize: '15px',
                        fontWeight:'bold',
                    })

                    $scope.dadosList = dadosTmp;

                    let dadosLiberacao = dadosTmp
                
                    if (dadosLiberacao.length > 500) {
                        Paginate(dadosLiberacao)

                    } else { 
                        
                        $scope.dadosLiberacoes = dadosLiberacao
                        $scope.ShowSpinnerLoad = false
                    }

                    $scope.dadosPagamentos = data[1]
                   
                }

                }else{

                    $scope.quantidadeRegistros = data[0]
                    data[0].shift()
                    $scope.dadosLiberacoes = data[0]
                    $scope.ShowSpinnerLoad = false
                    $scope.dadosPagamentos = [{}]


                }

            } else {

                myFunctions.showAlert('Nenhum Pátio ou Grupo Pátio foi selecionado! Verifique!');
            }

        };


        const Paginate = (dados) => {
            let metade = dados.slice(0, dados.length / 2)

            $scope.dadosLiberacoes = metade

            const timer = dados.length > 1000 ? 1000 : 100


            setTimeout(() => {
                $scope.dadosLiberacoes = dados

                $scope.ShowSpinnerLoad = false

            }, timer)
        }



        $scope.ShowHistorico = ({ id }) => {
           
            $mdDialog.show({
                
                controller: async function ($scope, $mdDialog, $mdToast) {
                 
                    const response = await $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-historico', { params: { ncv: id } })
                    const { data } = response
                    $scope.Historicos = data

                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };

                },
                templateUrl: 'MostrarHistorico.html',

                preserveScope: true,

            }).then(function (answer) {
                let a = answer;

            }, function () {
                $scope.statusdialog = 'You cancelled the dialog.';
            });
        }



        $scope.detalhe = function ({ id }) {

            window.open('/#/ncv-editar?id ' + id, '_blank');
        };



        
        $scope.gerarPlanilha = function () {
            let newLiberacoes = null

            newLiberacoes = $scope.dadosLiberacoes.filter( el => 
                !el.patio.includes('TOTAL') && 
                !el.patio.includes('TRANSFERENCIA') && 
                !el.patio.includes('CARTÃO') &&
                !el.patio.includes('BOLETO') &&
                !el.patio.includes('ISENTO') &&
                el.patio !== '')                                      

                if ($scope.planilhaValor.length === 0){

                    newLiberacoes.push({
                        patio: 'TOTAL GERAL',
                        total: 0.00,
                        valor_guincho: 0.00,
                        total_diarias: 0.00,
                        desconto: 0.00,
                        acrescimo: 0.00
        
                    }) 



                }else{

                newLiberacoes.push({
                patio: 'TOTAL GERAL',
                total: $scope.totalExcel,
                valor_guincho: $scope.totaisExcel.valor_guincho,
                total_diarias: $scope.totaisExcel.total_diarias,
                desconto: $scope.totaisExcel.desconto,
                acrescimo: $scope.totaisExcel.acrescimo

                })
          
                    
            }
              
            $scope.dadosPlanilha = newLiberacoes.map((x) => {
              
                let v_guincho =  x.valor_guincho || '0'
                v_guincho = parseNumber(v_guincho)
                
                let t_diarias = x.total_diarias || '0'
                t_diarias = parseNumber(t_diarias)
                
                let v_estadia = x.valor_estadia || '0'
                v_estadia = parseNumber(v_estadia)
               
                let v_desconto = x.desconto || '0'
                v_desconto = parseNumber(v_desconto)
                
                let n_estadias = x.estadias || '0'
                n_estadias = parseNumber(n_estadias)
               
                let v_acrescimo = x.acrescimo || '0'
                v_acrescimo = parseNumber(v_acrescimo)
               
                let v_total = x.total || '0'
                v_total = parseNumber(v_total)
             
               

                function parseNumber(strg) {
                    var strg = strg || "";
                    var decimal = '.';
                    strg = strg.replace(/[^0-9$.,]/g, '')
                    if(strg.indexOf(',') > strg.indexOf('.')) decimal = ','
                    if((strg.match(new RegExp("\\" + decimal,"g")) || []).length > 1) decimal=""
                    if (decimal != "" && (strg.length - strg.indexOf(decimal) - 1 == 3) && strg.indexOf("0" + decimal)!==0) decimal = ""
                    strg = strg.replace(new RegExp("[^0-9$" + decimal + "]","g"), "")
                    strg = strg.replace(',', '.')
                    return parseFloat(strg)
                }



                return {
                    patio: x.patio,
                    total: v_total,
                    id: x.id,
                    data_apreensao: x.data_apreensao,
                    data_liberacao: x.data_liberacao,
                    valor_guincho: v_guincho,
                    total_diarias: t_diarias,
                    estadias: n_estadias,
                    valor_estadia: v_estadia,
                    desconto: v_desconto,
                    acrescimo: v_acrescimo, 
                    tipo_veiculo: x.tipo_veiculo,
                    nome_pagador: x.nome_pagador, 
                    forma_pagto: x.forma_pagto, 
                    placa: x.placa,
                    local: x.local,
                    placa_uf: x.placa_uf,
                    placa_municipio: x.placa_municipio,
                    chassi: x.chassi,
                    numero_motor: x.numero_motor,
                    marca_modelo: x.marca_modelo    
          
                }

            })

            alasql('SELECT *  INTO XLSX("relatorio-liberacoes.xlsx",{headers:true}) FROM ?', [$scope.dadosPlanilha])
        }




        $scope.gerarPdf = async function () {


            if ($scope.dados.detalhes === true) {

                var doc = new jsPDF({ orientation: "landscape" });

                var totalPagesExp = '{total_pages_count_string}'

                doc.setFontSize(12);
                doc.autoTable({

                    columnStyles: {
                        tipo_veiculo: { halign: 'center' },
                        valor_guincho: { halign: 'center' },
                        valor_estadia: { halign: 'center' },
                        total_diarias: { halign: 'center' },
                        desconto: { halign: 'center' },
                        acrescimo: { halign: 'center' },
                        total: { halign: 'center' },
                        estadias: { halign: 'center' }
                    },
                    body: $scope.dadosLiberacoes,

                    columns: [
                        { header: 'Patio', dataKey: 'patio' },
                        { header: 'NCV', dataKey: 'id' },
                        { header: 'Placa', dataKey: 'placa' },
                        { header: 'Tipo', dataKey: 'tipo_veiculo' },
                        { header: 'Liberação', dataKey: 'data_liberacao' },
                        { header: 'Apreensão', dataKey: 'data_apreensao' },
                        { header: 'Guincho', dataKey: 'valor_guincho' },
                        { header: 'Diaria', dataKey: 'valor_estadia' },
                        { header: 'Diarias', dataKey: 'estadias' },
                        { header: 'Total Diarias', dataKey: 'total_diarias' },
                        { header: 'Desconto', dataKey: 'desconto' },
                        { header: 'Acrescimo', dataKey: 'acrescimo' },
                        { header: 'Recebido', dataKey: 'total' },

                        { header: 'Chassi', dataKey: 'chassi' },
                        { header: 'Número Motor', dataKey: 'numero_motor' },
                        { header: 'Municipio', dataKey: 'placa_municipio' },
                        { header: 'UF', dataKey: 'placa_uf' },

                    ],
                    bodyStyles: {
                        margin: 0,
                        fontSize: 8,
                    },
                    didDrawPage: function (data) {
                        // Header
                        doc.setFontSize(18)
                        doc.setTextColor(40)
                        doc.setFontStyle('normal')

                        doc.text('Liberações no periodo de ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22)

                        // Footer
                        var str = 'Pagina ' + doc.internal.getNumberOfPages()
                        // Total page number plugin only available in jspdf v1.0+
                        if (typeof doc.putTotalPages === 'function') {
                            str = str + ' de ' + totalPagesExp
                        }

                        doc.setFontSize(10)

                        // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                        var pageSize = doc.internal.pageSize
                        var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                        doc.text(str, data.settings.margin.left, pageHeight - 10)
                    },
                    margin: { top: 30 },
                });

               
                doc.autoTable({
                    head: [
                        ["TIPO RECEBIMENTO"]
                    ],
                })

                $scope.dadosPagamentos.forEach(table);

                function table(element, index, array) {

                    const e = element;
                    doc.autoTable({
                        body: [
                            [e.descricao + ' : R$ ' + e.valor]
                        ],
                        theme: 'grid'
                    })
                }

                if (typeof doc.putTotalPages === 'function') {
                    doc.putTotalPages(totalPagesExp)
                }

                doc.save('liberacoes-com-detalhes.pdf')


            } else {


                var doc = new jsPDF({ orientation: "landscape" });

                var totalPagesExp = '{total_pages_count_string}'


                doc.setFontSize(12);
                doc.autoTable({

                    columnStyles: {
                        tipo_veiculo: { halign: 'center' },
                        valor_guincho: { halign: 'center' },
                        valor_estadia: { halign: 'center' },
                        total_diarias: { halign: 'center' },
                        desconto: { halign: 'center' },
                        acrescimo: { halign: 'center' },
                        total: { halign: 'center' },
                        estadias: { halign: 'center' }
                    },
                    body: $scope.dadosLiberacoes,

                    columns: [
                        { header: 'Patio', dataKey: 'patio' },
                        { header: 'NCV', dataKey: 'id' },
                        { header: 'Placa', dataKey: 'placa' },
                        { header: 'Tipo', dataKey: 'tipo_veiculo' },
                        { header: 'Liberação', dataKey: 'data_liberacao' },
                        { header: 'Apreensão', dataKey: 'data_apreensao' },
                        { header: 'Guincho', dataKey: 'valor_guincho' },
                        { header: 'Diaria', dataKey: 'valor_estadia' },
                        { header: 'Diarias', dataKey: 'estadias' },
                        { header: 'Total Diarias', dataKey: 'total_diarias' },
                        { header: 'Desconto', dataKey: 'desconto' },
                        { header: 'Acrescimo', dataKey: 'acrescimo' },
                        { header: 'Recebido', dataKey: 'total' },
                        { header: 'Pagador', dataKey: 'nome_pagador' },
                        { header: 'Forma Pgto', dataKey: 'forma_pagto' },
                        

                    ],
                    bodyStyles: {
                        margin: 0,
                        fontSize: 8,
                    },
                    didDrawPage: function (data) {
                        // Header
                        doc.setFontSize(18)
                        doc.setTextColor(40)
                        doc.setFontStyle('normal')

                        doc.text('Liberações no periodo de ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22)

                        // Footer
                        var str = 'Pagina ' + doc.internal.getNumberOfPages()
                        // Total page number plugin only available in jspdf v1.0+
                        if (typeof doc.putTotalPages === 'function') {
                            str = str + ' de ' + totalPagesExp
                        }

                        doc.setFontSize(10)

                        // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                        var pageSize = doc.internal.pageSize
                        var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                        doc.text(str, data.settings.margin.left, pageHeight - 10)
                    },
                    margin: { top: 30 },
                });

               
                doc.autoTable({
                    head: [
                        ["TIPO RECEBIMENTO"]
                    ],
                })


                
                $scope.dadosPagamentos.forEach(table);

                function table(element, index, array) {

                    const e = element;
                    doc.autoTable({
                        body: [
                            [e.descricao + ' : R$ ' + e.valor]
                        ],
                        theme: 'grid'
                    })
                }


                if (typeof doc.putTotalPages === 'function') {
                    doc.putTotalPages(totalPagesExp)
                }

                doc.save('Relatorio-liberacoes.pdf')
            }

        }
    });
