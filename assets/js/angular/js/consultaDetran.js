angular
  .module("app")
  .controller(
    "consultaDetranCtrl",
    function ($scope, $http, urlServidor, urlImagens, $sce, $filter) {
      const { hash } = window.location;

      const _id = hash.slice(hash.indexOf("=") + 1);
      const _idUsuario = localStorage.getItem("id_usuario");

      $scope.$on("$viewContentLoaded", async function () {
        //IMagens
        $scope.images = {
          urlFotos: urlImagens.urlImagensFotos,
          urlImagensConferente: urlImagens.urlImagensConferente,
          listaImagens: null,
          listaImagensConferente: null,

          async getImages() {
            const conferente = await $http.get(
              `${urlServidor.urlServidorChatAdmin}/ncv/buscafotos-conferente`,
              { params: { ncv: _id } }
            );
            this.listaImagensConferente = conferente.data;

            const checklist = await $http.get(
              `${urlServidor.urlServidorChatAdmin}/ncv/busca-fotos`,
              {
                params: { ncv: _id },
              }
            );
            this.listaImagens = checklist.data;
          },
        };

        $scope.images.getImages();

        //PDF

        $scope.urlBuscaDetran = urlImagens.urlBuscaDetran;

        //CLassificações

        const response = await $http.put(
          urlServidor.urlServidorChatAdmin + "/ncv/listar-usuario",
          { ncv: _id, idusuario: _idUsuario }
        );
        const { placa } = response.data[0];

        const GetDadosByPlaca = async () => {
          try {
            const { data } = await $http.get(
              `${urlServidor.urlServidorChatAdmin}/integracoes/detran/${placa}`
            );
            const infos = data.map((item) => ({
              ...item,
              data_hora_importacao: $filter("date")(
                item.data_hora_importacao,
                "dd/MM/yyyy HH:mm:ss"
              ),
            }));
            
            const [dados] = infos;
            console.log(dados)
            if(dados === []) throw new Error("ola")
            return dados;
          } catch (error) {
            console.log(error);
            alert(`Busca Detran com a Placa ${placa} falhou`);
          }
        };

        const DadosDoVeiculo = (result) => {
          $scope.dadosVeiculo = [
            //Primira Coluna
            [
              {
                Label: "Data Consulta :",
                dataKey: result.data_hora_importacao,
              },
              {
                Label: "Placa :",
                dataKey: result.placa,
              },
              {
                Label: "Ano modelo :",
                dataKey: result.ano_modelo,
              },
              {
                Label: "Ano Fabricação :",
                dataKey: result.ano_fabricacao,
              },
              {
                Label: "Chassi:",
                dataKey: result.chassi,
              },
              {
                Label: "Combustível :",
                dataKey: result.combustivel,
              },
              {
                Label: "Blindagem :",
                dataKey: result.blindagem,
              },
              {
                Label: "Tipo :",
                dataKey: result.tipo,
              },
              {
                Label: "Ultimo licenciamento :",
                dataKey: result.ultimo_licenciamento,
              },
              {
                Label: "Carroceria :",
                dataKey: result.carroceria,
              },
            ],
            //  Segunda  Coluna
            [
              {
                Label: "Renavam :",
                dataKey: result.renavam,
              },
              {
                Label: "Marca Modelo :",
                dataKey: result.marca_modelo,
              },
              {
                Label: "Cor :",
                dataKey: result.cor,
              },
              {
                Label: "Remarcação do chassi :",
                dataKey: result.remarcacao_chassi,
              },
              {
                Label: "Bloqueio de furto :",
                dataKey: result.bloqueio_furto,
              },
              {
                Label: "Bloqueio de guincho :",
                dataKey: result.bloqueio_guincho,
              },
              {
                Label: "Restrição 1 :",
                dataKey: result.restricao1,
              },
              {
                Label: "Restrição 2 :",
                dataKey: result.restricao2,
              },
              {
                Label: "Arrolamento :",
                dataKey: result.arrolhamento,
              },
            ],
          ];
        };

        const DadosDoProprietario = (result) => {
          $scope.DadosDoProprietario = [
            //Primira Coluna
            [
              {
                Label: "Nome :",
                dataKey: result.nome_proprietario,
              },
              {
                Label: "Endereço :",
                dataKey: result.endereco_proprietario,
              },
              {
                Label: "Número :",
                dataKey: result.numero_proprietario,
              },
              {
                Label: "CEP :",
                dataKey: result.cep_proprietario,
              },
              {
                Label: "Munícipio :",
                dataKey: result.municipio_proprietario,
              },
            ],
            //  Segunda  Coluna
            [
              {
                Label: "CPF/CNPJ :",
                dataKey: result.cpf_cnpj_proprietario,
              },
              {
                Label: "Bairro :",
                dataKey: result.bairro_proprietario,
              },
              {
                Label: "Complemento :",
                dataKey: result.complemento_proprietario,
              },
              {
                Label: "UF :",
                dataKey: result.uf_proprietario,
              },
            ],
          ];
        };

        const DadosVenda = (result) => {
          $scope.DadosVenda = [
            //Primira Coluna
            [
              {
                Label: "Data da inclusão :",
                dataKey: result.data_inclusao_cvenda,
              },
              {
                Label: "Nome :",
                dataKey: result.nome_cvenda,
              },
              {
                Label: "Endereço :",
                dataKey: result.endereco_cvenda,
              },
              {
                Label: "Complemento :",
                dataKey: result.complemento_cvenda,
              },
              {
                Label: "Bairro :",
                dataKey: result.bairro_cvenda,
              },

              {
                Label: "Municipio :",
                dataKey: result.municipio_cvenda,
              },
            ],
            //  Segunda  Coluna
            [
              {
                Label: "Data da Venda :",
                dataKey: result.data_venda_cvenda,
              },
              {
                Label: "CPF/CNPJ :",
                dataKey: result.cpf_cnpj_cvenda,
              },
              {
                Label: "Número :",
                dataKey: result.numero_cvenda,
              },
              {
                Label: "CEP :",
                dataKey: result.cep_cvenda,
              },
              {
                Label: "UF :",
                dataKey: result.uf_cvenda,
              },
            ],
          ];
        };

        const DadosFinanceira = (result) => {
          $scope.DadosFinanceira = [
            //Primira Coluna
            [
              {
                Label: "Código da Financeira :",
                dataKey: result.codigo_financeira,
              },
              {
                Label: "Número do contrato :",
                dataKey: result.contrato_financeira,
              },
              {
                Label: "CGC Financiado :",
                dataKey: result.cgc_financiado,
              },
            ],
            //  Segunda  Coluna
            [
              {
                Label: "CNPJ :",
                dataKey: result.cnpj_financeira,
              },
              {
                Label: "Vigência do contrato :",
                dataKey: result.vigencia_contrato_financeira,
              },
              {
                Label: "Nome :",
                dataKey: result.nome_financeira,
              },
            ],
          ];
        };

        const DadosBinMotor = (result) => {
          $scope.DadosBinMotor = [
            //Primira Coluna
            [
              {
                Label: "Número do Motor Bin :",
                dataKey: result.numero_motor_bin,
              },
              {
                Label: "Restrição 1 :",
                dataKey: result.restricao1_bin,
              },
              {
                Label: "Restrição 3 :",
                dataKey: result.restricao3_bin,
              },
              {
                Label: "Chassi Regravado :",
                dataKey: result.chassi_regravado_bin,
              },
              {
                Label: "Número do Motor :",
                dataKey: result.numero_motor2_bin,
              },
              {
                Label: "Data da baixa :",
                dataKey: result.data_baixa_bin,
              },
            ],
            //  Segunda  Coluna
            [
              {
                Label: "Peso Bruto Total :",
                dataKey: result.peso_bruto_bin,
              },
              {
                Label: "Restrição 2 :",
                dataKey: result.restricao2_bin,
              },
              {
                Label: "Restrição 4 :",
                dataKey: result.restricao4_bin,
              },
              {
                Label: "Situação do Veículo :",
                dataKey: result.situacao_veiculo_bin,
              },
              {
                Label: "Motivo da Baixa :",
                dataKey: result.motivo_baixa,
              },
              {
                Label: "Hora da Baixa :",
                dataKey: result.hora_baixa_bin,
              },
            ],
          ];
        };

        const DadosBloqueio1 = (result) => {
          $scope.DadosBloqueio1 = [
            //Primira Coluna

            [
              {
                Label: "Tipo :",
                dataKey: result.tipo_bloqueio1,
              },
              {
                Label: "Descrição :",
                dataKey: result.descricao_bloqueio1,
              },
              {
                Label: "Número Edital",
                dataKey: result.numero_editai_bloqueio1,
              },
              {
                Label: "Número Lote :",
                dataKey: result.numero_lote_bloqueio1,
              },
              {
                Label: "Número Protocolo :",
                dataKey: result.protocolo__bloqueio1,
              },
              {
                Label: "Data inclusão :",
                dataKey: result.data_inclusao_bloqueio1,
              },
              {
                Label: "Motivo 1 :",
                dataKey: result.motivo1_bloqueio1,
              },
              {
                Label: "Motivo 3 :",
                dataKey: result.motivo3_bloqueio1,
              },
            ],
            //  Segunda  Coluna
            [
              {
                Label: "Município :",
                dataKey: result.municipio_bloqueio1,
              },
              {
                Label: "Ano Edital :",
                dataKey: result.ano_edital_bloqueio1,
              },
              {
                Label: "Autoridade :",
                dataKey: result.autoridade_bloqueio1,
              },
              {
                Label: "Ano Protocolo :",
                dataKey: result.ano_protocolo_bloqueio1,
              },
              {
                Label: "Hora inclusão :",
                dataKey: result.hora_inclusao_bloqueio1,
              },
              {
                Label: "Motivo 2 :",
                dataKey: result.motivo2_bloqueio1,
              },
              {
                Label: "Motivo 4 :",
                dataKey: result.motivo4_bloqueio1,
              },
            ],
          ];
        };

        const DadosBloqueio2 = (result) => {
          $scope.DadosBloqueio2 = [
            //Primira Coluna

            [
              {
                Label: "Tipo :",
                dataKey: result.tipo_bloqueio2,
              },
              {
                Label: "Descrição :",
                dataKey: result.descricao_bloqueio2,
              },
              {
                Label: "Número Edital",
                dataKey: result.numero_edital_bloqueio2,
              },
              {
                Label: "Número Lote :",
                dataKey: result.numero_lote_bloqueio2,
              },
              {
                Label: "Número Protocolo :",
                dataKey: result.protocolo_bloqueio2,
              },
              {
                Label: "Data inclusão :",
                dataKey: result.data_inclusao_bloqueio2,
              },
              {
                Label: "Motivo 1 :",
                dataKey: result.motivo1_bloqueio2,
              },
              {
                Label: "Motivo 3 :",
                dataKey: result.motivo3_bloqueio2,
              },
            ],
            //  Segunda  Coluna
            [
              {
                Label: "Município :",
                dataKey: result.municipio_bloqueio2,
              },
              {
                Label: "Ano Edital :",
                dataKey: result.ano_edital_bloqueio2,
              },
              {
                Label: "Autoridade :",
                dataKey: result.autoridade_bloqueio2,
              },
              {
                Label: "Ano Protocolo :",
                dataKey: result.ano_protocolo_bloqueio2,
              },
              {
                Label: "Hora inclusão :",
                dataKey: result.hora_inclusao_bloqueio2,
              },
              {
                Label: "Motivo 2 :",
                dataKey: result.motivo2_bloqueio2,
              },
              {
                Label: "Motivo 4 :",
                dataKey: result.motivo4_bloqueio2,
              },
            ],
          ];
        };

        const DadosRenajud = (result) => {
          $scope.DadosRenajud = [
            //Primira Coluna
            [
              {
                Label: "Tribunal :",
                dataKey: result.renajud_tribunal,
              },
              {
                Label: "Órgão Judicial :",
                dataKey: result.renajud_orgao_judicial,
              },
            ],
            //  Segunda  Coluna
            [
              {
                Label: "Número Processo :",
                dataKey: result.renajud_numero_processo,
              },
              {
                Label: "Descrição :",
                dataKey: result.renajud_descricao,
              },
            ],
          ];
        };

        const DadosRenajudRest1 = (result) => {
          $scope.DadosRenajudRest1 = [
            //Primira Coluna
            [
              {
                Label: "Ativa :",
                dataKey: result.renajud_restricao1_ativa,
              },
              {
                Label: "Data :",
                dataKey: result.renajud_restricao1_data,
              },
            ],
            //  Segunda  Coluna
            [
              {
                Label: "Descrição :",
                dataKey: result.renajud_restricao1_descricao,
              },
              {
                Label: "Hora :",
                dataKey: result.renajud_restricao1_hora,
              },
            ],
          ];
        };

        const DadosRenajudRest2 = (result) => {
          $scope.DadosRenajudRest2 = [
            //Primira Coluna
            [
              {
                Label: "Ativa :",
                dataKey: result.renajud_restricao2_ativa,
              },
              {
                Label: "Data :",
                dataKey: result.renajud_restricao2_data,
              },
            ],
            //  Segunda  Coluna
            [
              {
                Label: "Descrição :",
                dataKey: result.renajud_restricao2_descricao,
              },
              {
                Label: "Hora :",
                dataKey: result.renajud_restricao2_hora,
              },
            ],
          ];
        };

        const Render = () => {
          GetDadosByPlaca().then((dados) => {
            console.log(dados)
            DadosDoProprietario(dados);
            DadosDoVeiculo(dados);
            DadosVenda(dados);
            DadosFinanceira(dados);
            DadosBinMotor(dados);
            DadosBloqueio1(dados);
            DadosBloqueio2(dados);
            DadosRenajud(dados);
            DadosRenajudRest1(dados);
            DadosRenajudRest2(dados);

            $scope.buscaPDF = urlImagens.urlBuscaDetran;
            $scope.trustSrc = function () {
              return $sce.trustAsResourceUrl(`${$scope.buscaPDF}${dados.pdf}`);
            };
          });
        };
        Render();

        $scope.fetchDetran = async () => {
          try {
            const response = await $http.get(
              `https://sync-detran.herokuapp.com/placa/${placa}`
            );
            Render();
            console.log({ response });
          } catch (error) {
            alert(`Busca Detran com a Placa ${placa} falhou`);
          }
        };
      });

      $scope.GerarPDF = (dados) => {
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = "{total_pages_count_string}";
        const Info = new Array(dados);

        doc.setFontSize(12);
        doc.autoTable({
          body: Info,

          columns: [
            { header: "NCV", dataKey: "id" },
            { header: "Ano", dataKey: "ano" },
            { header: "Chassi", dataKey: "chassi" },
            { header: "Cor", dataKey: "cor" },

            { header: "Data Apreensão", dataKey: "data_apreensao" },
            { header: "Data Cadastro", dataKey: "data_cadastro" },
            { header: "Estado", dataKey: "estado_veiculo" },

            { header: "Marca/Modelo", dataKey: "marca_modelo" },
            { header: "Pátio", dataKey: "nome" },
            { header: "Número Motor", dataKey: "numero_motor" },
            { header: "Pátio Destino", dataKey: "patio_destino" },
            { header: "Placa", dataKey: "placa" },
            { header: "Placa Municipio", dataKey: "placa_municipio" },
            { header: "Placa UF", dataKey: "placa_uf" },
            { header: "Situação", dataKey: "situacao" },
            { header: "Status", dataKey: "status" },
            { header: "Tipo Veiculo", dataKey: "tipo_veiculo" },
            { header: "Veiculo Trancado", dataKey: "veiculo_trancado" },
          ],
          bodyStyles: {
            margin: 0,
            fontSize: 08,
          },
          didDrawPage: function (data) {
            // Header
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle("normal");

            // Footer
            var str = "Pagina " + doc.internal.getNumberOfPages();
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === "function") {
              str = str + " de " + totalPagesExp;
            }

            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height
              ? pageSize.height
              : pageSize.getHeight();
            doc.text(str, data.settings.margin.left, pageHeight - 10);
          },
          margin: { top: 30 },
        });

        if (typeof doc.putTotalPages === "function") {
          doc.putTotalPages(totalPagesExp);
        }

        doc.save(`${_id}dados.pdf`);
      };
    }
  );
