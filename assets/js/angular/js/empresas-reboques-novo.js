'use strict';
angular.module('app')
.controller("empresasReboquesNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions,estados,viaCep,$sce, urlImagens,buckets) {  

  $scope.urlImagens = urlImagens.urlDocumentosEmpresaReb;

  $scope.dados = {
    nome:'',  
    contato: '' ,
    telefone: '' ,
    valorMoto: 0.00 ,
    valorCarro: 0.00 ,
    valorCaminhao: 0.00 ,
    ativo: '' ,
    observacao: ''  ,
    endereco: '',
     bairro:'' ,
     cidade: '' ,
    cep: '',
    uf: '',
    email: '',
    inicio_contrato: '',
    fim_contrato: '',
    pdf_contrato: '' ,
    idempresa: localStorage.getItem('id_empresa'),
    cpf_cnpj:''
 }
 var bucket = null;
 $scope.trustSrc = function(src) {
   return $sce.trustAsResourceUrl(src);
 }  

 $scope.estados = estados.uf ;
 var urlParams = $location.search();
 $http.get(urlServidor.urlServidorChatAdmin + '/autoridades/vercodigo').then(function (response) {

     AWS.config.update({accessKeyId: response.data.ac, secretAccessKey: response.data.sc});
     AWS.config.region = response.data.regiao;
 }).finally( function(){
     bucket = new AWS.S3();
     /*if (urlParams.dados[0].pdf_contrato != undefined) {
         bucket.getObject(
                 {Bucket: "documentos-reidospatios", Key: urlParams.dados[0].pdf_contrato, ResponseContentDisposition: 'inline'},
                 function (error, data) {
                     if (error != null) {
                         myFunctions.showAlert("Falha no carregamento do PDF: " + error);
                     } else {

                         $scope.pdfContrato = new Blob([data.Body], {type: "application/pdf"});
                     }
                 }
         );
     }*/

 });
 

 $scope.buscaCep = (cep) => {

   if(cep.length == 9){

     viaCep.get(cep).then(function(response){
     
       $scope.dados.endereco = response.logradouro.toUpperCase() ;
       $scope.dados.bairro = response.bairro.toUpperCase() ;
       $scope.dados.cidade = response.localidade.toUpperCase() ;
       $scope.dados.uf = response.uf.toUpperCase() ;				 

     });

   }	

 }


  $scope.buscaCepRep = (rep_cep) => {

    if(rep_cep.length == 9){

      viaCep.get(rep_cep).then(function(response){
      
        $scope.dados.rep_endereco = response.logradouro.toUpperCase() ;
        $scope.dados.rep_bairro = response.bairro.toUpperCase() ;
        $scope.dados.rep_cidade = response.localidade.toUpperCase() ;
        $scope.dados.rep_uf = response.uf.toUpperCase() ;				 

      });

    }	

  }

 

 $scope.uploadFile = (file) => {

   $scope.pdfContrato = file;
   $scope.arquivo = file;
   $scope.dados.pdf_contrato = file.name;
 
    var params = {
             Key: $scope.arquivo.name,
             ContentType: $scope.arquivo.type ,
             ContentDisposition: 'inline',
             Body: $scope.arquivo ,
             ACL: 'public-read',
             Bucket: buckets.empresas_reboques
           };
           $scope.pdfContrato = null;
           $scope.arquivo = null;
           $scope.dados.pdf_contrato = null;
           
           bucket.putObject(params, function(err, data) {
               if (err) {
                   showAlert('ERROR: ' + err);
               }else{
                 setTimeout(function(){
     
                   myFunctions.showAlert( 'Arquivo cadastrado com sucesso!' ); 
     
                 }, 1500);
                 $scope.pdfContrato = file;
                 $scope.arquivo = file;
                 $scope.dados.pdf_contrato = file.name;
                 
               };
           });  

 };  

 $scope.isDisabled = false

 $scope.gravar = function() {        

   $http.post(urlServidor.urlServidorChatAdmin+'/empresas_reboques/cadastrar', $scope.dados ).then( function(response){

       if (response.data.code){
           
         myFunctions.showAlert('Erro na gravação!')

       }else{

         if ( response.data == '11000' )                 
         {
           myFunctions.showAlert('Já existe uma empresa cadastrada com o CPF/CNPJ: ' + $scope.dados.cpf_cnpj )
         }else
         {                    

           myFunctions.showAlert( 'Cadastro executado com sucesso!' ); 
           $scope.isDisabled = true ;     
           
          

         }  
               
       }; 

   }); 

 }
    
});