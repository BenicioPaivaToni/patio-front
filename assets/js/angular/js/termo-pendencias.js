angular
  .module("app").controller("termoPendenciasCtrl", function ($scope, $http, urlServidor) {
    
    
    const buscaPendencias = async () => {const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/empresas_reboques/busca-termo-pendencias`);
      $scope.dadosList = response.data;
    };


    buscaPendencias();

    
    $scope.gerarPlanilha = function () {

        if ($scope.dadosList){

        alasql('SELECT * INTO XLSX("pendencias-termo-credenciamento.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);

        }else {

            myFunctions.showAlert('Sem dados para gerar planilha! Verifique!')

        }   
    }




    $scope.gerarPDF = () => {
      var doc = new jsPDF({ orientation: "landscape" });
      var totalPagesExp = "{total_pages_count_string}";

      doc.setFontSize(10);
      doc.autoTable({
        columnStyles: {
          vencimento: { halign: "left" },
        },

        body: $scope.dadosList,
        columns: [
          { header: "Nome Empresa Reboque", dataKey: "nome_empresa_reboque" },
          { header: "Data / Hora Envio", dataKey: "data_hora_envio" },
          
        ],
        bodyStyles: {
          margin: 10,
          fontSize: 08,
        },
        didDrawPage: function (data) {
          // Header
          doc.setFontSize(18);
          doc.setTextColor(40);
          doc.setFontStyle("normal");

          doc.text( "Informativo de pendência na assinatura do Termo de Credenciamento", data.settings.margin.left + 5, 18 )
            doc.text( "Data da geração: " + new Date().toLocaleDateString("pt-BR"), data.settings.margin.left + 5, 25 )


          // Footer
          var str = "Pagina " + doc.internal.getNumberOfPages();
          // Total page number plugin only available in jspdf v1.0+
          if (typeof doc.putTotalPages === "function") {
            str = str + " de " + totalPagesExp;
            var totalpaginas = totalPagesExp;
          }

          doc.setFontSize(10);

          // jsPDF 1.4+ uses getWidth, <1.4 uses .width
          var pageSize = doc.internal.pageSize;
          var pageHeight = pageSize.height
            ? pageSize.height
            : pageSize.getHeight();

          doc.text(
            str + "                        ",
            data.settings.margin.left,
            pageHeight - 10
          );
        },
        margin: { top: 30 },
      });
      if (typeof doc.putTotalPages === "function") {
        doc.putTotalPages(totalPagesExp);
      }

      doc.save("relatorio-pendencias-termo.pdf");
    };
  });
