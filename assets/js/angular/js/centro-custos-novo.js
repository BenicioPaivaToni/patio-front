'use strict';
angular.module('app')
.controller("centroCustosNovoCtrl", function($location,$scope,$http,urlServidor,myFunctions) {  

  $scope.dados = {
    descricao:'',
    ativo: '' 
    
  }

  $scope.isDisabled = false

  $scope.gravar = function() {        

    console.log("enviado: ", $scope.dados)

    $http.post(urlServidor.urlServidorChatAdmin+'/centro-custo/cadastrar', $scope.dados ).then( function(response){

        if (response.data.code){
            
          myFunctions.showAlert('Erro na gravação!')

        }else{

          if ( response.data == '11000' )                 
          {
            myFunctions.showAlert('Centro de Custo já existe!')
          }else
          {                    

            myFunctions.showAlert( 'Centro de Custo cadastrado com sucesso!' ); 
            $scope.isDisabled = true ;            

          }  
                
        }

    })

  }
    
})