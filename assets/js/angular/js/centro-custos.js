app.controller('centroCustosCtrl', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.novo = function (ev, id) {
      $location.path('centro-custos-novo');
    }

    $scope.dados = {
      descricao:'',
      ativo: '' ,
      idempresa: localStorage.getItem('id_empresa')
    
    }


    $scope.limpar = function (ev) {
      $scope.dados.descricao = ''
      $scope.dadosList = []
    }


    $scope.pesquisar = function (ev) {
      $scope.dadosList = []

      $http.get(urlServidor.urlServidorChatAdmin + '/centro-custo/listar', { params: { descricao: $scope.dados.descricao } }).then(function (response) {

        let dados = response.data

      if (dados.length > 0){

          $scope.dadosList = dados

        }else{

          myFunctions.showAlert('Filtro não encontrou registros, verifique!')
          
        }

      })

    }



    $scope.detalhe = function (ev, id) {
      var dados = $scope.dadosList.filter(function (obj) {
        return obj.id == id
      })
        
      if (dados[0].id > 0) {
        $location.path('centro-custos-editar').search({ dados })
      }
    }



    $scope.gerarPDF = function () {

      if ($scope.dadosList){

        const PDFAjustes = $scope.dadosList.map((item) => ({
          descricao: item.descricao,
          ativo: item.desc_ativo,

        }))

        var doc = new jsPDF({ orientation: "landscape" })
        var totalPagesExp = "{total_pages_count_string}"

        doc.setFontSize(10)

        doc.autoTable({
          columnStyles: {
            vencimento: { halign: "left" },
          },

          body: PDFAjustes,
          columns: [
            { header: "Descrição", dataKey: "descricao" },
            { header: "Ativo", dataKey: "ativo" },

          ],
          bodyStyles: {
            margin: 10,
            fontSize: 08,
          },

          didDrawPage: function (data) {
            // Header
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle("normal");

            doc.text(
              "Centro de Custos",
              data.settings.margin.left + 15,
              22
            );

            // Footer
            var str = "Pagina " + doc.internal.getNumberOfPages();
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === "function") {
              str = str + " de " + totalPagesExp;
              var totalpaginas = totalPagesExp;
            }

            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height
              ? pageSize.height
              : pageSize.getHeight();

            doc.text(
              str + "                        ",
              data.settings.margin.left,
              pageHeight - 10
            );
          },
          margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === "function") {
          doc.putTotalPages(totalPagesExp);
        }

        doc.save("centro-custos.pdf");

        }else{

        myFunctions.showAlert('Sem dados para gerar PDF! Verifique!')
  
        }
    }



    $scope.gerarPlanilha = () => {

      if ($scope.dadosList) {

        const excelAjustes = $scope.dadosList.map((item) => ({
            descricao: item.descricao,
            ativo: item.desc_ativo,

        }));

        alasql(
          'SELECT * INTO XLSX("centro-custos.xlsx",{headers:true}) FROM ?', [excelAjustes]
        )

      }else{    
        myFunctions.showAlert('Sem dados para gerar planilha! Verifique!')
      }
        
    }

})