angular.module('app').controller('painelChamadosCtrl', function ($scope, $mdUtil, $http, urlServidor, myFunctions) {

  
    $scope.limpar = function (ev) {
        $scope.dados.data_chamados = null;
        dataInicio: null;
        dataFinal: null;
        $scope.dados.status = null;
    };
    var audio = new Audio('assets/sons/beepbeep.wav');
    var socket = io.connect(urlServidor.urlServidorChatAdmin, { transports: ['websocket'], upgrade: false }); //, {'force new connection': true}

    $scope.play = function () {

        //		var audio = new Audio('assets/sons/buzina.mp3');
        //		audio.play();

        $http.get(urlServidor.urlServidorChatAdmin + '/chamados/listar-painel', { params: $scope.dados }).then(function (response) {
            let dados = response.data;
            $scope.dadosList = dados;
        });
    };
    $scope.getStyle = function (status) {
        if (status == "Aguardando")
            return { 'background-color': 'red' };
        if (status == "Em Atendimento")
            return { 'background-color': 'yellow' };
        if (status == "Concluido")
            return { 'background-color': 'green' };
    }


    let hoje = new Date();
    let dia = hoje.getDate(), ano = hoje.getFullYear(), mes = hoje.getMonth();
    $scope.dados = {
        dataInicio: new Date(ano, mes, 01),
        dataFinal: new Date(ano, mes, dia),
        idEmpresa: localStorage.getItem('id_empresa'),
        origem: 'painel'
    };
    $scope.dados.dataInicio.setHours(00, 01)
    $scope.dados.dataFinal.setHours(23, 59);
    $http.get(urlServidor.urlServidorChatAdmin + '/chamados/listar-painel', { params: $scope.dados }).then(function (response) {
        let dados = response.data;
        $scope.dadosList = dados;
    });


    setTimeout(function () {
     


        socket.on('novochamado', function () {
            //			var audio = new Audio('assets/sons/beepbeep.wav');
            //				audio.play();

            $http.get(urlServidor.urlServidorChatAdmin + '/chamados/listar-painel', { params: $scope.dados }).then(function (response) {
                let dados = response.data;
                $scope.dadosList = dados;
            }).finally(function () {
                audio.play();

                        //Auto Scroll
                const body = document.querySelector("[role='main']");
                const ContentArea = body.querySelector('md-content[md-scroll-y]');
                const ScrollBottom = () => $mdUtil.animateScrollTo(ContentArea, 200000, 0);
            

                ScrollBottom()


            });
        })

        socket.on('aceitechamado', function () {
            //			var audio = new Audio('assets/sons/beepbeep.wav');
            //				audio.play();

            $http.get(urlServidor.urlServidorChatAdmin + '/chamados/listar-painel', { params: $scope.dados }).then(function (response) {
                let dados = response.data;
                $scope.dadosList = dados;
            }).finally(function () {
                audio.play();
            });
        })

    }, 500);


})