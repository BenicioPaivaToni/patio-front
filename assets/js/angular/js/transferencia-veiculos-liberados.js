angular
  .module("app")
  .controller(
    "transferenciaVeiculosLiberadosCtrl",
    function ($scope, $http, myFunctions, urlServidor, $mdDialog) {
      $scope.dados = {
        id_patio: "",
        ncv_placa: "",
        data_transferencia: new Date(),
      };

      $scope.alterardados = {
        id_patio: "",
        ncv_placa: "",
        data_transferencia: new Date(),
        id_motorista: "",
        //  id_reboque:''
      };

      $scope.dadosAgendados = {
        data_agendamento: new Date(),
      };

      setTimeout(() => {
        document.getElementById("Container").style.display = "none";
        document.getElementById("ErrorLength").style.display = "none";
      }, 100);

      $scope.HideModal = () => {
        // o Set timeout vai nos ajudar para não correr o risco de o modal fechar antes do usuario clicar
        setTimeout(() => {
          document.getElementById("Container").style.display = "none";
        }, 2000);
      };

      $scope.ChangeScope = (clientes) => {
        // Vamos substituir o inupt texte, visto que não precisamos mais do container ele receber um none
        $scope.DadosGeraisClientes = clientes;
        $scope.DadosPeriodo.id_cliente = clientes.nome;

        document.getElementById("Container").style.display = "none";
      };

      
     // $scope.recebe_transferencia = localStorage.getItem( "recebe_transferencia" )

      $scope.checkboxClienteRetira = function () {
        if ($scope.DadosPeriodo.cliente_retira) {
          $scope.DadosPeriodo.id_reboque = null;
          $scope.DadosPeriodo.id_motorista = null;
        }
      };

      $scope.Disable = true;
      $scope.ncv = 0;

      $scope.id = 0;
      $scope.isCanceled = false;
      $scope.isRecebido = false;
      $scope.usuarioCancelamento = "";
      $scope.dataCancelamento = "";
      $scope.motivoCancelamento = "";

      $scope.usuarioRecebimento = "";
     // $scope.dataRecebimento = "";

      $scope.Dados = [];

      $http
        .put(urlServidor.urlServidorChatAdmin + "/motoristas/listar", {
          idUsuario: localStorage.getItem("id_usuario"),
          ativo: "1",
        })
        .then(function (response) {
          $scope.motoristas = response.data;
        });

      $http
        .put(urlServidor.urlServidorChatAdmin + "/reboques/listar-ativos")
        .then(function (response) {
          $scope.reboques = response.data;
        });

      $scope.pesquisar = {
        cancelarAgendamento: async () => {
          $mdDialog
            .show({
              controller: function ($scope, $mdDialog, $mdToast) {
                $scope.cancelar_agendamento = function () {
                  if ($scope.dados.motivo_cancelamento !== undefined) {
                    $http
                      .post(
                        urlServidor.urlServidorChatAdmin +
                          "/leiloes/cancela-agendamento",
                        {
                          id: $scope.dados.numero_protocolo,
                          id_usuario_cancelamento:
                            localStorage.getItem("id_usuario"),
                          motivo_cancelamento: $scope.dados.motivo_cancelamento,
                        }
                      )
                      .then(function (response) {
                        if (response.data.code) {
                          myFunctions.showAlert("Erro na gravação!");
                        } else {
                          myFunctions.showAlert(
                            "Agendameno cancelado com sucesso!"
                          );
                          $scope.pesquisar.buscaDadosProtocolo();
                        }
                      });

                    $scope.fechar();
                  } else {
                    myFunctions.showAlert("Descreva o motivo do cancelamento!");
                  }
                };

                $scope.fechar = function () {
                  $mdDialog.cancel();
                };
              },
              templateUrl: "cancelar-agendamento.html",
              scope: $scope,
              preserveScope: true,
              scopoAnterior: $scope,
            })
            .then(
              function (answer) {
                let a = answer;
              },
              function () {
                $scope.statusdialog = "You cancelled the dialog.";
              }
            );
        },

        buscaDadosProtocolo: async () => {
          $scope.nomeMotorista = "";
          $scope.nomeReboque = "";
          $scope.nomeCliente = "";
          $scope.dataEntrega = "";
          $scope.dataCadastro = "";
          $scope.usuarioCadastro = "";

          $scope.nomeUsuario = "";
          $scope.dataUsuario = "";

          $scope.usuarioCancelamento = "";
          $scope.dataCancelamento = "";
          $scope.motivoCancelamento = "";

          $scope.usuarioEntrega = "";
         // $scope.dataRecebimento = "";

          $scope.dadosList = [];

          let numeroProtocolo = $scope.dados.numero_protocolo;
          if (!numeroProtocolo) {
            myFunctions.showAlert("Informar número do Protocolo");
          } else {
            let DadosTransferencias = null;

            DadosTransferencias = await $http.get(
              `${urlServidor.urlServidorChatAdmin}/leiloes/listar-agendamento-veiculos`,
              {
                params: {
                  numeroProtocolo,
                },
              }
            );

            if (DadosTransferencias.data.length == 0) {
              myFunctions.showAlert(
                "Pesquisa não encontrou registros! Verifique!"
              );
            } else {
              $scope.nomeMotorista = DadosTransferencias.data[0].motorista;
              $scope.nomeReboque = DadosTransferencias.data[0].reboque;
              $scope.nomeCliente = DadosTransferencias.data[0].nome_cliente;
              $scope.dataEntrega = DadosTransferencias.data[0].data_entrega;
              $scope.dataCadastro = DadosTransferencias.data[0].data_cadastro;
              $scope.usuarioCadastro = DadosTransferencias.data[0].usuario_cadastro;
              $scope.nomeUsuario = DadosTransferencias.data[0].nome_usuario;
              $scope.dataUsuario = DadosTransferencias.data[0].data_usuario;
              $scope.isCanceled = DadosTransferencias.data[0].id_usuario_cancelamento !== null;
              $scope.usuarioCancelamento = DadosTransferencias.data[0].usuario_cancelamento;
              $scope.dataCancelamento = DadosTransferencias.data[0].data_cancelamento;
              $scope.motivoCancelamento = DadosTransferencias.data[0].motivo_cancelamento;
              $scope.usuarioEntrega = DadosTransferencias.data[0].usuario_entrega;
            //  $scope.dataRecebimento = DadosTransferencias.data[0].data_recebimento;
              $scope.isRecebido = DadosTransferencias.data[0].usuario_entrega !== null;
              $scope.dadosList = DadosTransferencias.data;
            }
          }
        },

        BuscaNcvPlaca: async () => {
          if (
            !$scope.DadosPeriodo.id_patio_origem ||
            !$scope.DadosPeriodo.id_cliente ||
            !$scope.DadosPeriodo.data ||
            (!$scope.DadosPeriodo.delivery &&
            !$scope.DadosPeriodo.cliente_retira)
          ) {
            myFunctions.showAlert(
              `Informe todos os dados antes de inserir veiculo!`
            );
          } else {
            console.log();
            const response = await $http.get(
              `${urlServidor.urlServidorChatAdmin}/transferencias-veiculos/ncv-placa-liberados`,
              {
                params: {
                  ncv_placa: $scope.alterardados.ncv_placa,
                  id_patio: $scope.DadosPeriodo.id_patio_origem,
                },
              }
            );

            const data = response.data[0];

            if (data == undefined) {
              myFunctions.showAlert(
                `Veiculo não encontrato! Verifique se foi digitado corretamente e se o mesmo é do pátio selecionado!`
              );
            } else {
              $scope.ncv = response.data[0].ncv;

              const txt = `
                        <div>
                        <header>
                    
                            <title>
                            
                            </title>
                    
                        </header>
                        </br>
                    
                        <body> 
                        
                        <div> 
                            <div>NCV :</div>
                            <div><strong>${data.ncv}</strong></div>
                        </div>
                        </br>

                        <div> 
                            <div>PLACA :</div>
                            <div><strong>${data.placa}</strong></div>
                        </div>
                        </br>

                        <div> 
                        <div>MARCA/MODELO :</div>
                        <div><strong>${data.marca_modelo || 0}</strong></div>
                        </div>

                        </br>

                        <div> 
                            <div>ANO :</div>
                            <div><strong>${data.ano || 0}</strong></div>
                        </div>
                        </br>
                    

                        <div> 
                            <div>NÚMERO DO MOTOR :</div>
                            <div><strong>${
                              data.numero_motor || 0
                            }</strong></div>
                        </div>
                        </br>
                        
                        <div> 
                            <div>CHASSI :</div>
                            <div><strong>${data.chassi || 0}</strong></div>
                        </div>
                        </br>
                        </body>
                        
                        </div>
                    
                        `;

              let confirm = $mdDialog
                .confirm()
                .ariaLabel("Lucky day")
                .title("Confira os dados do veículo")
                .ok("ADICIONAR VEICULO")
                .cancel("Cancelar")
                .htmlContent(txt);
              $mdDialog.show(confirm).then(() => {
                $scope.Dados.push(response.data[0]);
                $scope.alterardados.ncv_placa = "";
              });
            }
          }
        },

        limparAgendados: (ev) => {
          $scope.DadosPeriodo.data = new Date();
          $scope.alterardados.ncv_placa = "";
        },

        limparAgendamentos: (ev) => {
          ($scope.dadosAgendados.data_agendamento = new Date()),
          ($scope.agendamentosList = []);
        },
      };

      $scope.DadosPeriodo = {
        data: new Date(),
        id_patio: null,
        id_cliente: null,
        delivery: null,
        cliente_retira: null,
        id_reboque: null,
        id_motorista: null,
      };

      $http
        .put(
          urlServidor.urlServidorChatAdmin + "/patios/listar-patiosusuario",
          {
            idUsuario: localStorage.getItem("id_usuario"),
            idEmpresa: localStorage.getItem("id_empresa"),
          }
        )
        .then(({ data }) => {
          $scope.patios = data.filter((item) => {
            return item.status == !0;
          });
        });

      $scope.SeparacaoPeriodo = {
        hoje: new Date(),

        //Busca NCV ou Placa
        Buscar: async () => {
          if (
            !$scope.DadosPeriodo.id_patio_origem &&
            !$scope.DadosPeriodo.id_cliente &&
            (!$scope.DadosPeriodo.delivery ||
            !$scope.DadosPeriodo.cliente_retira)
          ) {
            myFunctions.showAlert(
              "Pátio e Cliente precisam ser preenchidos! Verifique!"
            );
          } else {
            const response = await $http.get(
              `${urlServidor.urlServidorChatAdmin}/transferencias-veiculos/listar-periodo`,
              {
                params: {
                  id_patio: $scope.DadosPeriodo.id_patio,
                },
              }
            );
            const { data } = response;

            $scope.Dados = data;
          }
        },

        //Limpa os campos tela
        limpar: function (ev) {
          $scope.DadosPeriodo.id_patio_origem = null;
          $scope.DadosPeriodo.delivery = null;
          $scope.DadosPeriodo.cliente_retira = null;
          $scope.DadosPeriodo.id_cliente = null;
          $scope.DadosPeriodo.id_motorista = null;
          $scope.DadosPeriodo.id_reboque = null;
          $scope.Dados = "";
          $scope.DadosPeriodo.data = new Date();
          $scope.alterardados.ncv_placa = "";
        },

        LimparResultado: () => {
          $scope.Dados = "";
        },

        //Botão de selecionar todos ou desmarcar todos
        Selecionar: "SELECIONAR TODOS",

        SelecionarTodos: function (Dados) {
          if (this.Selecionar === "SELECIONAR TODOS") {
            Dados.forEach((dados) => {
              dados.selecionado = true;
            });
            this.Selecionar = "DESMARCAR TODOS";
          } else {
            Dados.forEach((dados) => {
              dados.selecionado = false;
            });
            this.Selecionar = "SELECIONAR TODOS";
          }
        },

        confirmarEntrega: async (Dados, Dado) => {
          if ($scope.dadosList.length > 0) {
            $http
              .post(
                urlServidor.urlServidorChatAdmin +
                  "/leiloes/grava-confirma-entrega",
                {
                  id: $scope.dados.numero_protocolo,
                  id_usuario_entrega: localStorage.getItem("id_usuario"),
                }
              )
              .then(function (response) {
                myFunctions.showAlert(
                  `Confirmação de Entrega gravada com sucesso!`
                );
                $scope.pesquisar.buscaDadosProtocolo();
              });
          } else {
            return myFunctions.showAlert(
              `Nenhum veiculo na lista de entrega! Verifique!`
            );
          }
        },

        //Botão de gravar dados selecionados
        Inserir: async (Dados) => {
          $scope.nomeMotorista = "";
          $scope.nomeReboque = "";

          const result = Dados.filter((Veiculo) => {
            return Veiculo.selecionado;
          });

          if (result.length > 0) {
            $scope.dados.id_cliente = $scope.DadosGeraisClientes.id;
            let idProtocolo = 0;

            let dados = {
              usuario_cadastro: localStorage.getItem("id_usuario"),
              id_veiculo: $scope.DadosPeriodo.id_reboque,
              id_cliente: $scope.DadosGeraisClientes.id,
              id_motorista: $scope.DadosPeriodo.id_motorista,
              data_entrega: $scope.DadosPeriodo.data,
              id_patio: $scope.DadosPeriodo.id_patio_origem,
            };

            $http
              .post(
                urlServidor.urlServidorChatAdmin +
                  "/leiloes/grava-protocolo-agendamento",
                dados
              )
              .then(function (response) {
                idProtocolo = response.data.insertId;
                $scope.idProtocoloTransferencia = idProtocolo;
              })
              .finally(async function () {
                var transferencia = result.map((x) => {
                  return {
                    ncv: x.id,
                    id_protocolo: idProtocolo,
                  };
                });

                let responseRomaneio = await $http.post(
                  `${urlServidor.urlServidorChatAdmin}/leiloes/grava-veiculos-agendamento`,
                  { dados: transferencia }
                );

                let confirm = $mdDialog
                  .confirm()
                  .title("Agendamento de Transporte")
                  .textContent(
                    "Número Protocolo de Agendamento: " + idProtocolo
                  )
                  .ariaLabel("Lucky day")
                  .ok("Ok");
                $mdDialog.show(confirm).then(function () {

                  $scope.gerarPDFTransferencia();

                  $scope.DadosPeriodo = "";

                  $scope.Dados = [];
                });
              });
          } else {
            return myFunctions.showAlert(
              `Nenhum veículo foi selecionado, Verifique!`
            );
          }
        },
      };

      $scope.buscaAgendamentos = async () => {
        if ($scope.dadosAgendados.data_agendamento == null) {
          myFunctions.showAlert("Informe a data para pesquisa!");
        } else {
          let dados = {
            data: $scope.dadosAgendados.data_agendamento,
          };

          $http
            .get(
              urlServidor.urlServidorChatAdmin + "/leiloes/busca-agendamentos",
              { params: dados }
            )
            .then(function (response) {
              let dados = response.data;
              $scope.agendamentosList = dados;
            });
        }
      };

      $scope.buscarClientes = async () => {
        const length = $scope.DadosPeriodo.id_cliente.length;
        //Somente com 3 letras para iniciar a pesquisa
        // Tendo 3 letras o container vai aparecer e o erro some
        if (length >= 3) {
          document.getElementById("Container").style.display = "block";
          document.getElementById("ErrorLength").style.display = "none";

          // faz a chamada e retorna os dados
          const response = await $http.put(
            urlServidor.urlServidorChatAdmin + "/clientes/listar",
            { razao_social: $scope.DadosPeriodo.id_cliente }
          );
          let dados = response.data;

          $scope.clientes = dados;
          //se for menor que 3 o modal some e o erro aparece
        } else if (length < 3) {
          document.getElementById("ErrorLength").style.display = "block";
          document.getElementById("Container").style.display = "none";
          // se estiver vazio o container some
        } else if ($scope.DadosPeriodo.id_cliente == "") {
          document.getElementById("Container").style.display = "none";
          alert("h");
        }
      };

     
      $scope.gerarPlanilhaTransferencia = () => {
        
        if ($scope.Dados.length > 0) {
        
          var excelSelecao = $scope.Dados.map((x) => {
            return {
              patio_origem: x.patio,
              ncv: x.id,
              placa: x.placa,
              data_apreensao: x.data_apreensao,
              marca_modelo: x.marca_modelo,
              cor: x.cor,
              ano: x.ano,
              hora_apreensao: x.hora_apreensao,
              km_percorrido: x.km_percorrido,
              tipo_veiculo: x.tipo_veiculo,
            };
          });

          alasql(
            'SELECT * INTO XLSX("Agendamento-entrega-veiculos.xlsx",{headers:true}) FROM ?',
            [excelSelecao]
          );

        } else {
          myFunctions.showAlert("Sem dados para gerar Excel! Verifique!");
        }

      };

     
      $scope.gerarPDFTransferencia = function () {
        
        if ($scope.Dados.length > 0) {
          let patio = $scope.patios.find((item) => item.id === $scope.DadosPeriodo.id_patio_origem);
          let cliente = $scope.DadosPeriodo.id_cliente;
          let dataAtual = new Date().toISOString().slice(0, 10);
          let motorista = $scope.motoristas.find((item) => item.id === $scope.DadosPeriodo.id_motorista);
          let reboque = $scope.reboques.find((item) => item.id === $scope.DadosPeriodo.id_reboque);
          let idProtocolo = $scope.idProtocoloTransferencia;

          reboque = $scope.DadosPeriodo.id_reboque !== null ? reboque.descricao : "---";
          motorista = $scope.DadosPeriodo.id_motorista !== null ? motorista.nome : "---";
          idProtocolo = $scope.idProtocoloTransferencia !== undefined ? $scope.idProtocoloTransferencia : "---"

          var PDFSelecao = $scope.Dados.map((x) => {
            return {
              ncv: x.id,
              placa: x.placa,
              data_apreensao: x.data_apreensao,
              marca_modelo: x.marca_modelo,
              cor: x.cor,
              ano: x.ano,
              hora_apreensao: x.hora_apreensao,
              tipo_veiculo: x.tipo_veiculo,
            };
          });

          var doc = new jsPDF({ orientation: "landscape" });
          var totalPagesExp = "{total_pages_count_string}";

          doc.setFontSize(10);

          doc.autoTable({
            columnStyles: {
              vencimento: { halign: "left" },
            },

            body: PDFSelecao,
            columns: [
              { header: "NCV", dataKey: "ncv" },
              { header: "Ano", dataKey: "ano" },
              { header: "Data Apreensão", dataKey: "data_apreensao" },
              { header: "Hora Apreensão", dataKey: "hora_apreensao" },
              { header: "Marca/Modelo", dataKey: "marca_modelo" },
              { header: "Placa", dataKey: "placa" },
              { header: "Tipo de Veiculo", dataKey: "tipo_veiculo" },
            ],
            bodyStyles: {
              margin: 10,
              fontSize: 08,
            },

            didDrawPage: function (data) {
              // Header
              doc.setFontSize(12);
              doc.setTextColor(40);
              doc.setFontStyle("normal");

              doc.text(
                "Número Protocolo: " + idProtocolo + "      " + "Cliente: " + cliente,
                data.settings.margin.left + 5, 18
              );

              doc.text(
                "Motorista: " + motorista + "   Transp.: " + reboque + "   Data Entrega: " + 
                $scope.DadosPeriodo.data.toLocaleDateString("pt-BR"), data.settings.margin.left + 5, 25
              );

              // Footer
              var str = "Pagina " + doc.internal.getNumberOfPages();
              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
                str =
                  str +
                  "                                                                           Data " +
                  dataAtual.substr(8, 2) +
                  "/" +
                  dataAtual.substr(5, 2) +
                  "/" +
                  dataAtual.substr(0, 4) +
                  "                  Usuário " +
                  localStorage.getItem("nome_usuario");
                var totalpaginas = totalPagesExp;
              }

              doc.setFontSize(10);

              // jsPDF 1.4+ uses getWidth, <1.4 uses .width
              var pageSize = doc.internal.pageSize;
              var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();

              doc.text(
                str + "                        ",
                data.settings.margin.left,
                pageHeight - 10
              );
            },
            margin: { top: 30 },
          });
          if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
          }

          doc.save("Agendamento-entrega-veiculos.pdf");
        } else {
          myFunctions.showAlert("Sem dados para gerar PDF! Verifique!");
        }
      };


      $scope.gerarPlanilhaEntrega = () => {
        
        if ($scope.dadosList) {
        
          var excelSelecao = $scope.dadosList.map((x) => {
            return {
              patio_origem: x.patio,
              ncv: x.id,
              placa: x.placa,
              data_apreensao: x.data_apreensao,
              marca_modelo: x.marca_modelo,
              cor: x.cor,
              ano: x.ano,
              hora_apreensao: x.hora_apreensao,
              km_percorrido: x.km_percorrido,
              tipo_veiculo: x.tipo_veiculo,
            };
          });

          alasql(
            'SELECT * INTO XLSX("Entrega-veiculos-agendados.xlsx",{headers:true}) FROM ?',
            [excelSelecao]
          );

        } else {
          myFunctions.showAlert("Sem dados para gerar Excel! Verifique!");
        }

      };
    



      $scope.gerarPDFEntrega = function () {
        if ($scope.dadosList) {
          let dataAtual = new Date().toISOString().slice(0, 10);
          let reboque = $scope.nomeReboque !== null ? $scope.nomeReboque : "---";
          let motorista = $scope.nomeMotorista !== null ? $scope.nomeMotorista : "---";

          var PDFSelecao = $scope.dadosList.map((x) => {
            return {
              patio: x.patio,
              ncv: x.id,
              placa: x.placa,
              data_apreensao: x.data_apreensao,
              marca_modelo: x.marca_modelo,
              cor: x.cor,
              ano: x.ano,
              hora_apreensao: x.hora_apreensao,
              tipo_veiculo: x.tipo_veiculo,
            };
          });

          var doc = new jsPDF({ orientation: "landscape" });

          var totalPagesExp = "{total_pages_count_string}";

          doc.setFontSize(10);

          doc.autoTable({
            columnStyles: {
              vencimento: { halign: "left" },
            },

            body: PDFSelecao,
            columns: [
              { header: "Patio", dataKey: "patio" },
              { header: "NCV", dataKey: "ncv" },
              { header: "Ano", dataKey: "ano" },
              { header: "Data Apreensão", dataKey: "data_apreensao" },
              { header: "Hora Apreensão", dataKey: "hora_apreensao" },
              { header: "Marca/Modelo", dataKey: "marca_modelo" },
              { header: "Placa", dataKey: "placa" },
              { header: "Tipo de Veiculo", dataKey: "tipo_veiculo" },
            ],
            bodyStyles: {
              margin: 10,
              fontSize: 08,
            },

            didDrawPage: function (data) {
              // Header
              doc.setFontSize(12);
              doc.setTextColor(40);
              doc.setFontStyle("normal");

              doc.text(
                "Número Protocolo: " + $scope.dados.numero_protocolo,
                data.settings.margin.left + 5,18
              );

              doc.text(
                "Cliente: " +
                  $scope.nomeCliente +
                  "     Motorista: " +
                  motorista +
                  "   Transp.: " +
                  reboque +
                  "   Data Entrega: " +
                  $scope.dataEntrega,
                data.settings.margin.left + 5,25
              );

              // Footer
              var str = "Pagina " + doc.internal.getNumberOfPages();
              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
                str =
                  str +
                  "                                                                           Data " +
                  dataAtual.substr(8, 2) +
                  "/" +
                  dataAtual.substr(5, 2) +
                  "/" +
                  dataAtual.substr(0, 4) +
                  "                  Usuário " +
                  localStorage.getItem("nome_usuario");
                var totalpaginas = totalPagesExp;
              }

              doc.setFontSize(10);

              // jsPDF 1.4+ uses getWidth, <1.4 uses .width
              var pageSize = doc.internal.pageSize;
              var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();

              doc.text(
                str + "                        ", data.settings.margin.left, pageHeight - 10
              );
            },
            margin: { top: 30 },
          });
          if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
          }

          doc.save("Entrega-veiculos-agendados.pdf");
        } else {
          myFunctions.showAlert("Sem dados para gerar PDF! Verifique!");
      }
    };
    

 
    $scope.gerarPlanilhaAgendamentos = function () {
      if ($scope.agendamentosList) {alasql(
          'SELECT * INTO XLSX("relatorio-lista-agendamentos.xlsx",{headers:true}) FROM ?',
          [$scope.agendamentosList]
        );
      } else {
        myFunctions.showAlert("Sem dados para gerar planilha! Verifique!");
      }
    };

   
    $scope.gerarPDFAgendamentos = function () {
      if ($scope.agendamentosList) {
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = "{total_pages_count_string}";

        doc.setFontSize(10);
        doc.autoTable({
          columnStyles: {
            vencimento: { halign: "left" },
          },

          body: $scope.agendamentosList,
          columns: [
            { header: "Protocolo", dataKey: "id_protocolo" },
            { header: "Cliente", dataKey: "cliente" },
            { header: "Data Agendada", dataKey: "data_entrega" },
            { header: "Motorista", dataKey: "motorista" },
            { header: "Reboque", dataKey: "reboque" },
          ],
          bodyStyles: {
            margin: 10,
            fontSize: 08,
          },
          didDrawPage: function (data) {
            // Header
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle("normal");

            doc.text(
              "Agendamentos de entrega de veículos para o dia " +
                $scope.dadosAgendados.data_agendamento.toLocaleDateString(
                  "pt-BR"
                ),
              data.settings.margin.left + 15,22
            );

            // Footer
            var str = "Pagina " + doc.internal.getNumberOfPages();
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === "function") {
              str = str + " de " + totalPagesExp;
              var totalpaginas = totalPagesExp;
            }

            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
            },margin: { top: 30 },
          });
          
          if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
          }

          doc.save("relatorio-lista-agendamentos.pdf");

        } else {
          myFunctions.showAlert("Sem dados para gerar PDF! Verifique!");
        }
      };
    }
  );
