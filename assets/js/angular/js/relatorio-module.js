/* 
 * Luiz A C Costa - 9 de Set 2020: 15:58
 */
/* global app */
app.factory('relatorioFac', [
    '$http',
    '$location',
    'controlePatioFac',
    'urlServidor',
    'myFunctions', function ($http, $location, controlePatioFac, urlServidor, myFunctions) {
        let url = '' ; 
        
//        window.location.hostname.indexOf("localhost") !== -1 ? "http://localhost:1337/" : "https://serverapi.reidospatios.com.br:1337";

        let relatorioFac = {
            controlePatioFac: controlePatioFac,
            myFunctions: myFunctions,
            urlServidor: urlServidor,
            $location: $location,
            $http: $http,
            veiculos: {
                filtros: function () {
                    return $http.get(urlServidor.urlServidorChatAdmin + "/relatorios/relatorio-filtros-veiculo").then(function (cb) {
                        return cb.data;
                    }, function () {
                        return [];
                    });
                }
            },
            operadores: [
                {type: ["string"], label: "CONTÉM", value: "LIKE"},
                {type: ["string", "list"], label: "DIFERENTE", value: "<>"},
                {type: ["number", "date"], label: "ENTRE", value: "BETWEEN"},
                {type: ["number", "date", "string", "list"], label: "IGUAL", value: "="},
                {type: ["number"], label: "MAIOR QUE", value: ">"},
                {type: ["number"], label: "MAIOR OU IGUAL", value: ">="},
                {type: ["number"], label: "MENOR QUE", value: "<"},
                {type: ["number"], label: "MENOR OU IGUAL", value: "<="}
            ],
            pesquisar: function (filtros,estoque_real) {
                return $http.post(urlServidor.urlServidorChatAdmin + "/relatorios/relatorio-geral", { filtro: filtros , estoque_real: estoque_real }).then(function (cb) {
                    return cb.data;
                }, function () {
                    return [];
                });
            },

            gravarNoPatio: function(dadosTmp) {
                return $http.post(urlServidor.urlServidorChatAdmin + '/ncv/altera-patio', {
                    id: dadosTmp.NCV, 
                    no_patio: dadosTmp.no_patio 
                })
                .then(function (response) {
                    return response.data;
                })
                .catch(function (error) {
                    console.log.error(error);
                    return [];
                })
            },

            gravarLocalizadoLeilao: function(dadosTmp) {
                return $http.post(urlServidor.urlServidorChatAdmin + '/ncv/altera-localizado-leilao', {
                    id: dadosTmp.NCV, 
                    localizado_leilao: dadosTmp.localizado_leilao
                })
                .then(function (response) {
                    return response.data;
                })
                .catch(function (error) {
                    console.log.error(error);
                    return [];
                })
            },
            
      
        };
        return relatorioFac;
    }
]); 





app.controller('relatorioController', ['$scope', 'relatorioFac','myFunctions', function ($scope, relatorioFac, myFunctions ) {

    let hoje = new Date();

    $scope.isEmpty = relatorioFac.controlePatioFac.isEmpty;
        
    /*
    $scope.fnColunas = function(){
        return Object.keys($scope.dados[0]);
    };
    */

    $scope.init = function () {

       // $scope.dados = [{}];

        $scope.filtro = {
            valor01: "",
            valor02: ""
        };
        $scope.filtros = [];
        $scope.opcoesDeFiltro = [];
        $scope.operadoresDisponiveis = angular.copy(relatorioFac.operadores);

        $scope.operadoresDisponiveisLoad = function () {
            if ($scope.filtro.operador) {
                $scope.operadoresDisponiveis = relatorioFac.operadores.filter(x => x.type.indexOf($scope.filtro.operador) !== -1);
            }
        };

        $scope.removeFiltro = function (i) {
            $scope.filtros.splice(i, 1);
        }
        $scope.addFiltro = function () {
            $scope.filtros.push({
                filtro: angular.copy($scope.filtro),
                operador: angular.copy($scope.operador),
                valor01: angular.copy($scope.filtro.valor01),
                valor02: angular.copy($scope.filtro.valor02)
            });
        };
        /*
            relatorioFac.veiculos.filtros().then(function (cb) {
                console.log(cb);
                $scope.opcoesDeFiltro = cb;
                $scope.filtro = angular.copy(cb[0]);
            });
        */

        $scope.opcoesDeFiltro = [
            {label: "PÁTIO", field: "id_patio", tabela: "ncv", operador: "list"},
            {label: "PÁTIO DESTINO", field: "patio_destino", tabela: "ncv", operador: "list"},
            {label: "STATUS", field: "status", tabela: "ncv", operador: "list"},
            {label: "TIPO", field: "tipo", tabela: "ncv", operador: "list"},
            {label: "SITUACAO", field: "situacao", tabela: "ncv", operador: "list"},
            {label: "NO PÁTIO", field: "no_patio", tabela: "ncv", operador: "list"},
            {label: "LOCALIZADO LEILÃO", field: "localizado_leilao", tabela: "ncv", operador: "list"},               
        ]

        relatorioFac.$http.put(relatorioFac.urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', {
            idUsuario: localStorage.getItem('id_usuario'),
            idEmpresa: localStorage.getItem('id_empresa')
        }).then(function (response) {
            $scope.patios = response.data;
            //$scope.pesquisa.patio = response.data[0].id;
        });
    };


    $scope.noPatio = 0;
    $scope.localizadoLeilao = 0;
    $scope.alteraLocalizadoLeilao = localStorage.getItem("altera_localizado_leilao") === "1" ? false : true;
    $scope.alterarNoPatio = localStorage.getItem("altera_no_patio") === "1" ? false : true;

        
    async function findPatioFiltro(arrayDados) {
            
        for await (const object of arrayDados) {
            
            if (object.filtro.field === "id_patio" || object.filtro.field === "patio_destino" ) {            
            return object;
            }

        }
            return null;
        }        

        $scope.pesquisar = async function () {

            if($scope.tipoUsuario == 'admin' ||  $scope.alterarNoPatio == "1"){
                $scope.altera_no_patio = true;
              }else{
                $scope.altera_no_patio = false;
            }

            $scope.dados = null ;

            const retorno = await findPatioFiltro($scope.filtros)
                
            if( retorno ){

                relatorioFac.pesquisar($scope.filtros, $scope.filtro.estoque_real  ).then(function (cb) {
                    $scope.dados = cb;
                });    
    
            } else {
                myFunctions.showAlert('Informar Patio ou Patio destino')
            }                      
           
        };

        $scope.opcoesDePesquisa = function () {
            return {
                patio: "",
                patio_destino:"",
                tipo:"",
                situacao:""
            };
        };

        $scope.limpar = function (e) {
            $scope.dados = null ;
            $scope.filtros.splice(0, $scope.filtros.length );
            $scope.opcoesDePesquisa = {
                patio: "",
                patio_destino:"",
                tipo:"",
                situacao:""
            };
            
        };


        $scope.checkboxDetalhe = function (ev, rowId) {
            
            if (!rowId || !Array.isArray($scope.dados)) {
            return false;
            }
    
            var row = $scope.dados.find((x) => x.NCV === rowId);
    
            if (row) {
                $scope.noPatio = row.no_patio;
                return row.no_patio === 1 ? true : false;
            } else {
                return false;
            }
        }
    
          
        $scope.checkboxLocalizado = function (ev, rowId) {
            
            if (!rowId || !Array.isArray($scope.dados)) {
              return false;
            }
    
            var row = $scope.dados.find((x) => x.NCV === rowId);
    
            if (row) {
              $scope.localizadoLeilao = row.localizado_leilao;
              return row.localizado_leilao === 1 ? true : false;
            } else {
                return false;
            }
        }


        $scope.informaPatio = async (ev, rowId) =>{

            let rowData = $scope.dados.findIndex((x) => x.NCV == rowId);

            var dadosTmp = $scope.dados[rowData];
    
            dadosTmp.no_patio = dadosTmp.no_patio === 1 ? 0 : 1;
      
            $scope.dados[rowData] = dadosTmp;

            console.log("rowData: ", rowData)
      
            relatorioFac.gravarNoPatio(dadosTmp).then(function (result) {
            
                if (result.code){

                    myFunctions.showAlert('Erro ao gravar No Patio!');

                }
            
            })
           
            myFunctions.showAlert('Informação de pátio alterada com sucesso!');
      
        }


       
        $scope.informaLocalizadoLeilao = async (ev, rowId) => {

            let rowData = $scope.dados.findIndex((x) => x.NCV == rowId);
            
            var dadosTmp = $scope.dados[rowData];
    
            dadosTmp.localizado_leilao = dadosTmp.localizado_leilao === 1 ? 0 : 1;
      
            $scope.dados[rowData] = dadosTmp;
            
            relatorioFac.gravarLocalizadoLeilao(dadosTmp).then(function (result) {
            
                if (result.code){

                    myFunctions.showAlert('Erro ao gravar Localizado Leilão!');

                };
            
            });
      
            myFunctions.showAlert('Informação de Localizado Leilão alterada com sucesso!');
            
            if(dadosTmp.no_patio !== 1 && dadosTmp.localizado_leilao === 1){
             
                dadosTmp.no_patio = 1

                relatorioFac.gravarNoPatio(dadosTmp).then(function (result) {
            
                    if (result.code){
    
                        $scope.error = response.data;
    
                    };
                
                });
    
            };
    
        };

        
        
        $scope.hideCheck = function (ev, rowId){
          
            if(!rowId) return true;
        }


        
        $scope.gerarPlanilha = function () {
            alasql('SELECT * INTO XLSX("relatorio-liberacoes.xlsx",{headers:true}) FROM ?', [$scope.dados]);
        }


        $scope.gerarPdf = function () {

            const dadosPdf = $scope.dados.map(obj => ({
                ...obj, 
                no_patio: obj.no_patio === 1 ? "SIM" : "NÃO",
                localizado_leilao: obj.localizado_leilao === 1 ? "SIM" : "NÃO"
            }));

            let dataAtual = new Date().toISOString().slice(0, 10);

            var doc = new jsPDF({orientation: "landscape"});
            var totalPagesExp = '{total_pages_count_string}'
            doc.setFontSize(10);
            doc.autoTable({
                columnStyles: {
                    NCV: {halign: 'center'},
                    PLACA: {halign: 'center'},
                    MARCA_MODELO: {halign: 'center'},
                    ANO: {halign: 'center'},
                    COR: {halign: 'center'},
                    PLACA_UF: {halign: 'center'},
                    APREENSAO: {halign: 'center'},
                    STATUS: {halign: 'center'},
                    TIPO: {halign: 'center'},
                    SITUACAO: {halign: 'center'},
                    PATIO: {halign: 'center'},
                    PATIO_DESTINO: {halign: 'center'},
                    NO_PATIO: {halign: 'center'},
                    LOCAL_LEILAO: {halign: 'center'}
                },
                body: dadosPdf,
                columns: [
                    {header: 'NCV', dataKey: 'NCV'},
                    {header: 'PLACA', dataKey: 'PLACA'},
                    {header: 'MARCA/MODELO', dataKey: 'MARCA_MODELO'},
                    {header: 'ANO', dataKey: 'ANO'},
                    {header: 'COR', dataKey: 'COR'},
                    {header: 'UF', dataKey: 'placa_uf'},                    
                    {header: 'APREENSAO', dataKey: 'APREENSAO'},
                    {header: 'STATUS', dataKey: 'STATUS'},
                    {header: 'TIPO', dataKey: 'tipo'},
                    {header: 'SITUACAO', dataKey: 'situacao'},
                    {header: 'PATIO', dataKey: 'PATIO'},
                    {header: 'PATIO DEST.', dataKey: 'patio_destino'},
                    {header: 'NO PATIO', dataKey: 'no_patio'},
                    {header: 'LOCAL. LEILÃO', dataKey: 'localizado_leilao'}
                ],
                bodyStyles: {
                    margin: 5,
                    fontSize: 8
                },
                didDrawPage: function (data) {
                    // Header
                    doc.setFontSize(18);
                    doc.setTextColor(40);
                    doc.setFontStyle('normal');

//                    doc.text('Veiculos ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22);
                    doc.text('Relatório Veiculos - Qtde '+$scope.dados.length,data.settings.margin.left + 5, 22);

                    // Footer
                    var str = 'Pagina ' + doc.internal.getNumberOfPages();
                    // Total page number plugin only available in jspdf v1.0+
                    if (typeof doc.putTotalPages === 'function') {
                        str = str + ' de ' + totalPagesExp
                        str = str + "                                                                           Data " +
                        dataAtual.substr(8, 2) + "/" + dataAtual.substr(5, 2) +
                        "/" + dataAtual.substr(0, 4) + "                  Usuário " + localStorage.getItem("nome_usuario");

                    }

                    doc.setFontSize(10);

                    var pageSize = doc.internal.pageSize;
                    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
                    doc.text(str, data.settings.margin.left, pageHeight - 10);
                },
                margin: {top: 30}
            });
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp);
            }

            doc.save('relatorio-veiculos.pdf');
        };

    }
]);
