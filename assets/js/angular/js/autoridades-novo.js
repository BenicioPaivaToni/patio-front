'use strict';
angular.module('app')
.controller("autoridadesNovoCtrl", function($location,$scope,$http,urlServidor,$mdDialog,estados,viaCep) {  

  $scope.dados = {
     descricao:'',  
     sigla: '' ,
     local: '' ,
     ativo: '' ,
     observacao: '',
     endereco: '' ,
     bairro: '' ,
     cidade: '' ,
     cep: '',
     uf: '',
     email: '' ,
     telefone: '',
     contato:'', 
     idempresa: localStorage.getItem('id_empresa')  
  }

  $scope.estados = estados.uf ;

  $scope.buscaCep = (cep) => {

		if(cep.length == 9){

			viaCep.get(cep).then(function(response){
			
				$scope.dados.endereco = response.logradouro.toUpperCase() ;
				$scope.dados.bairro = response.bairro.toUpperCase() ;
				$scope.dados.cidade = response.localidade.toUpperCase() ;
				$scope.dados.uf = response.uf.toUpperCase() ;				 

      });

		}	

	}  

  $scope.isDisabled = false;

  $scope.gravar = function() {  

    $http.post(urlServidor.urlServidorChatAdmin+'/autoridades/cadastrar', $scope.dados ).then( function(response){

        if (response.data.code){
            
          showAlert('Erro na gravação!')

        }else{

          if ( response.data == '11000' )                 
          {
              showAlert('Autoridade já existe!')
          }else
          {                    
            showAlert( 'Cadastro executado com sucesso!' ); 
            $scope.isDisabled = true;
                    
          }  
                
        }; 

    }); 

  }; 


  var showAlert = function(ev) {
    $mdDialog.show(
    $mdDialog.alert()
        .parent(angular.element(document.querySelector('#popupContainer')))
        .clickOutsideToClose(true)
        .title('Atenção!')
        .textContent(ev)
        .ariaLabel('')
        .ok('Ok!')
        .targetEvent(ev)
        .multiple(true)
    );
  };

    
});