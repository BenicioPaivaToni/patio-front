angular.module("app").controller('contabilizadoCtrl', function ($scope, $location, $http, urlServidor, myFunctions, $window, $mdDialog, urlImagens, $sce) {
   
    $scope.HideModal = () => {
        // o Set timeout vai nos ajudar para não correr o risco de o modal fechar antes do usuario clicar
        setTimeout(() => {
           document.getElementById("Container").style.display = "none"
  
        }, 2000);
    }
  

    $scope.ChangeScope = (fornecedores) => {
        // Vamos substituir o inupt texte, visto que não precisamos mais do container ele receber um none 
          
        $scope.dados.id_fornecedor = fornecedores.razao_nome
         
        document.getElementById("Container").style.display = "none"
    
    }
  
  
  
    const buscaPatiosGrupo = async () => {
        const response = await $http.get(`${urlServidor.urlServidorChatAdmin}/grupos-patios/listar`,)
        const { data } = response;
        return data.filter((item) => item.ativo == 1);
    }
    

    buscaPatiosGrupo().then((Response) => $scope.patiosGrupo = Response)

    buscaCentroCusto()




    $scope.buscaFornecedores = async(event) => {
     
    
        const length = $scope.dados.id_fornecedor.length

            //Somente com 3 letras iremos iniciar a pesquisa 
            // Tendo 3 letras o container vai aparecere e o erro some 
        if (length >= 3) {
            document.getElementById("Container").style.display = "block"
            document.getElementById("ErrorLength").style.display = "none"

            // fazemos a chamda e retornamos os dados 
        const response = await $http.put(urlServidor.urlServidorChatAdmin + '/fornecedores/listar', {razao_social: $scope.dados.id_fornecedor })
   
        let dados = response.data;
        $scope.fornecedores = dados

            //se for menor que 3 o modal some e o erro aparece 
        } else if (length < 3) {
            document.getElementById("ErrorLength").style.display = "block"
            document.getElementById("Container").style.display = "none"
        
        // se estiver vazio o container some 
        } else if ($scope.dados.id_fornecedor == '') {
            document.getElementById("Container").style.display = "none"
            alert("h")
        }

    }



    $scope.dados = {
        dataInicio: new Date(),
        dataFinal: new Date(),
        patio: null,
        forma_pagamento: null,
        cpf_cnpj: null,
        patioGrupo: null,
    }


    $scope.mesAtual = function (ev, id) {
        let hoje = new Date()
        $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
        $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    };


    $scope.limpar = function (ev) {
        $scope.dados.nome = '';
        $scope.dados.status = '';
        $scope.dados.documento = '';
        $scope.dados.dataInicio = new Date();
        $scope.dados.dataFinal = new Date();
        $scope.dados.forma_pagamento = null;
        $scope.dados.patio = null;
        $scope.dados.conta = null;
        $scope.dados.cpf_cnpj = null
        $scope.dados.patioGrupo = null
        $scope.dados.centro_custo = null
        $scope.dados.competencia = null
        $scope.total_pago = null
        $scope.total_pagar = null
        $scope.totalRegistros = null
        $scope.dadosList = []
        $scope.dados.servico = 0
        $scope.dados.id_fornecedor = ''

    }

    buscaTipoRecebimento().then(function (result) {
        $scope.tiposRecebimentos = result;
    });

    buscaContas().then(function (result) {
        $scope.contas = result;
    });



    $scope.urlImagemNf = urlImagens.urlImagemNf

    $scope.trustSrc = function(src) {
        return  $sce.trustAsResourceUrl(src);
    }



    $scope.visualizarNota = function (ev, rowId) {

        $scope.listaDocumentos = null

        $http.get (urlServidor.urlServidorChatAdmin + '/pagar/busca-documentospagar', {params: {id_pagar: rowId}}).then(function (response) {

            $scope.listaDocumentos = response.data
           
        })

        setTimeout(() => {

            if ($scope.listaDocumentos.length <= 0){

                myFunctions.showAlert('Documento PDF contendo NF não foi localizado, verifique!');
            
            }else{
        
                $mdDialog.show({controller: function ($scope, $mdDialog, $mdToast) {
            
                $scope.fechar = function () {
                    $mdDialog.cancel();
                };
                },
                    templateUrl: 'visualizar-documento.html',
                    scope: $scope,
                    preserveScope: true,
                    scopoAnterior: $scope
                }).then(function (answer) {
                        let a = answer;

                    }, function () {
                        $scope.statusdialog = 'You cancelled the dialog.';
                });

            } 
    
        }, 1000);
    }


    $scope.contabilizar = function (ev, rowId) {
              
        var indexDados = $scope.dadosList.findIndex((x) => x.id == rowId)

        var dadosTmp = $scope.dadosList[indexDados]

        dadosTmp.contabilizado = dadosTmp.contabilizado === 'SIM' ? false : true

        $http.post(urlServidor.urlServidorChatAdmin + '/pagar/altera-contabilizado', {id: dadosTmp.id, contabilizado: dadosTmp.contabilizado }).then(function (response) {

            if (response.data.code) {
                $scope.error = response.data
            }

            myFunctions.showAlert('Contabilizado alterado com sucesso!')

           $scope.pesquisar()
        })
    }
 


    $scope.checkboxDetalhe = function (ev, rowId) {

        if(!rowId) return false

        var row = $scope.dadosList.find((x) => x.id === rowId)
        
        if(!row) return false
        
        return row.contabilizado == 'SIM' ? 1 : 0
        
    }

    
    $scope.hideCheck = function (ev, rowId){
        
        if(!rowId) return true
    }
   

    $scope.pesquisar = async function (ev) {

        $scope.total_pago = null
        $scope.total_pagar = null
        $scope.totalRegistros = null
        $scope.dadosList = []
        $scope.dados.dataInicio.setHours(00, 00, 00);
        $scope.dados.dataFinal.setHours(00, 00, 00);

        if ($scope.dados.cpf_cnpj == "") {
            delete $scope.dados.cpf_cnpj
        }

        $scope.dadosPesquisa = {
            dataInicio: $scope.dados.dataInicio,
            dataFinal: $scope.dados.dataFinal,
            prestador_servico: $scope.dados.servico === true ? 1 : 0,
            patio: $scope.dados.patio,
            forma_pagamento: $scope.dados.forma_pagamento,
            cpf_cnpj: $scope.dados.cpf_cnpj,
            patioGrupo: $scope.dados.patioGrupo,
            nome: $scope.dados.id_fornecedor,
            status: $scope.dados.status,
            competencia: $scope.dados.competencia,
            conta: $scope.dados.conta,
            centro_custo: $scope.dados.centro_custo
        }

        $http.put(urlServidor.urlServidorChatAdmin + '/pagar/listar', $scope.dadosPesquisa).then(function (response) {

            const { data } = response;
            // Angular converte todos os valores Falsy para False, .. assim, caso seja Undefined,null, ou false, no final será false

            if (data[0] == false) {
                myFunctions.showAlert('Filtro não encontrou registros, verifique!');

            } else {

                $scope.totalRegistros = data[0].length
                   
                let totalAberto = data[0].reduce(function (val, elem) {
                    return (elem.status !== 'ABERTO') ? val : val + elem.valor_semformatacao;
                }, 0)

                $scope.total_pagar = totalAberto

                let totalPago = data[0].reduce(function (val, elem) {
                    return (elem.status !== 'PAGO') ? val : val + elem.valor_semformatacao;
                }, 0)
            
                $scope.total_pago = totalPago
                    
                dadosRecebidos = data[0]
                
                const dadosTmp = []
                                            
                let patioAtual = null  
                let countTotalPatio = 0
                
                const patio = `<div style="width:100px"><h4 style="font-size:14px;color:0000FF;font-weight: bold"> TOTAL PÁTIO </h4></div>`
                
                for(let [index, item] of data[0].entries()) {
                    
                    if(!patioAtual) patioAtual = item.nome
                    
                    if((patioAtual && patioAtual != item.nome)) {
                            
                        dadosTmp.push({
                            nome: patio,
                            vencimento: `<div style="width:100px"><h4 style="font-size:14px;color:0000FF;font-weight: bold"> ${myFunctions.numberToReal(countTotalPatio)} </h4></div>`
                        
                        })

                        const pagamentos = data[1].filter((x) => x.patio === patioAtual)                        

                        let countTotal = 0

                        for(let pagamento of pagamentos) {
                            dadosTmp.push({
                                nome: `<div style="width:100px"><h4 style="font-size:14px;color:0DA68C;font-weight: bold"> ${pagamento.descricao} </h4></div>`,
                                vencimento: `<div style="width:100px"><h4 style="font-size:14px;color:0DA68C;font-weight: bold"> ${pagamento.total} </h4></div>`
                            })

                            countTotal += parseFloat(pagamento.total.toString().replace('R$ ', ''))
                        } 

                        patioAtual = item.nome

                        countTotalPatio = 0
                    }

                    countTotalPatio += item.valor_semformatacao 
                            
                    dadosTmp.push(item)

                    if(data[0].length - 1 === index) {
                        dadosTmp.push({
                            nome: patio,
                            vencimento: `<div style="width:100px"><h4 style="font-size:14px;color:0000FF;font-weight: bold"> ${myFunctions.numberToReal(countTotalPatio)} </h4></div>`
                        })

                        const pagamentos = data[1].filter((x) => x.patio === patioAtual)

                        let countTotal = 0

                        for(let pagamento of pagamentos) {
                            dadosTmp.push({
                                nome: `<div style="width:100px"><h4 style="font-size:14px;color:0DA68C;font-weight: bold"> ${pagamento.descricao} </h4></div>`,
                                vencimento: `<div style="width:100px"><h4 style="font-size:14px;color:0DA68C;font-weight: bold"> ${pagamento.total} </h4></div>`
                            })
                            
                            countTotal += item.valor_semformatacao
                        } 


                    }
                        
                }
                
                dadosTmp.push({
                    nome: ''
                }) 
                
                dadosTmp.push({
                    nome: `<div style="width:100px"><h4 style="font-size:14px;color:BF4917;font-weight: bold"> TOTAL A PAGAR </h4></div>`,
                    vencimento: `<div style="width:100px"><h4 style="font-size:14px;color:BF4917;font-weight: bold"> ${myFunctions.numberToReal(totalAberto)} </h4></div>`
                 
                })

                dadosTmp.push({
                    nome: `<div style="width:100px"><h4 style="font-size:14px;color:BF4917;font-weight: bold"> TOTAL PAGO </h4></div>`,
                    vencimento: `<div style="width:100px"><h4 style="font-size:14px;color:BF4917;font-weight: bold"> ${myFunctions.numberToReal(totalPago)} </h4></div>`

                })

               // if (dadosTmp.contabilizado == "SIM"){ //revisar

               // }

                $scope.dadosList = dadosTmp;

            }

        });

    };

   

    $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') })
    .then( ({data})  => {
        $scope.patios = data.filter((item) => item.status == 1);


    });

    function buscaContas() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/contas/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }


    function buscaTipoRecebimento() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/tipos-recebimento/listar-tipo', { tipo: 'PAGAR' }).then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }


    function buscaCentroCusto() {
        return new Promise(function (resolve, reject) {
            $http.get(urlServidor.urlServidorChatAdmin + '/centro-custo/listar').then(function (response) {
                let dados = response.data
                resolve(dados)

                $scope.centroCustos = dados.filter((item) => {
                    return item.ativo == 1
                })
            })
        })
    }



    $scope.gerarPlanilha = function () {

        if ($scope.dadosList){

        let dadosPlanilha = $scope.dadosList.filter( el => 
            !el.nome.includes('TOTAL') && 
            !el.nome.includes('TRANSFERENCIA') && 
            !el.nome.includes('CARTÃO') &&
            !el.nome.includes('BOLETO') &&
            !el.nome.includes('ISENTO') &&
            !el.nome.includes('div') &&
            el.nome !== 'div style') 

        alasql('SELECT * INTO XLSX("relatorio-contas-a-pagar.xlsx",{headers:true}) FROM ?', [dadosPlanilha]);

        }else {

            myFunctions.showAlert('Sem dados para gerar planilha! Verifique!')

        }   
    }


    $scope.gerarPdf = function () {

        if ($scope.dadosList){

        const PDFInformações = $scope.dadosList.map(index => ({

            nome: index.nome ? index.nome.replace(/(<([^>]+)>)/ig, " ") : 'Sem Pátio',
            vencimento: index.vencimento ? index.vencimento.replace(/(<([^>]+)>)/ig, " ") : 'Sem Vencimento',
            saldo_pagar: index.saldo_pagar ? index.saldo_pagar.replace(/(<([^>]+)>)/ig, " ") : 'Sem Saldo',
            documento: index.documento,
            desc_categoria: index.desc_categoria,
            status: index.status,
            competencia: index.competencia,
            razao_nome: index.razao_nome,
            desc_tiporecebimento: index.desc_tiporecebimento,
            conta: index.conta || 'Sem Conta',
            centro_custo: index.centro_custo,
            cpf_cnpj: index.cpf_cnpj

        }))
        
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(10);
        doc.autoTable({

            columnStyles: {
                vencimento: { halign: 'left' },
            },

            body: PDFInformações,
            columns: [
                { header: 'Pátio', dataKey: 'nome' },
                { header: 'Vencimento', dataKey: 'vencimento' },
                { header: 'CPf/CNPJ', dataKey: 'cpf_cnpj' },
                { header: 'Categoria', dataKey: 'desc_categoria' },
                { header: 'Valor Total', dataKey: 'saldo_pagar' },
                { header: 'Status', dataKey: 'status' },
                { header: 'Compet.', dataKey: 'competencia' },
                { header: 'Razão', dataKey: 'razao_nome' },
                { header: 'Forma de Pagamento', dataKey: 'desc_tiporecebimento' },
                { header: 'Centro Custo', dataKey: 'centro_custo' },
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                doc.text('Contas a pagar com vencimento no periodo de ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22);

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                    var totalpaginas = totalPagesExp;
                }

                var strTotal = 'TOTAL A PAGAR ' + $scope.total_pagar.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) +
                    '            TOTAL PAGO ' + $scope.total_pago.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()

                doc.text(str + '                        ' + strTotal, data.settings.margin.left, pageHeight - 10)
            },
            margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('relatorio-contas-a-pagar.pdf');

        }else{

            myFunctions.showAlert('Sem dados para gerar PDF! Verifique!')

        }  
    }


});