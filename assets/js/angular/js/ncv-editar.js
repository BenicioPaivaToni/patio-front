
angular.module('app').controller('ncvEditarCtrl', function ($rootScope, $scope, $location, $http, urlServidor, myFunctions, $mdDialog,  $sce, estados, viaCep, Upload, S3UploadService, ncvFac, urlImagens,buckets) {
    
    const queryString = window.location.hash;

    const _id =  queryString.slice(queryString.indexOf("%") + 3); 

    $scope.dados = null;
   
    var data_liberacao;
    var veiculo;
    var mymap = '';

    $scope.tipo ={
        tipo: ''
    }
    $scope.tipo.user =  localStorage.getItem('tipo_usuario');

    $scope.fotos = { 
        novotipo: '' , 
       novadescricao: ''
    }   

    $scope.documentos = { 
        novotipo: '' , 
       novadescricao: ''
    }  
   
    //Verifica se o Usuário é apenas consulta 
    if($scope.tipo.user === 'consulta'){
        $scope.Userconsult = true
    }

    $scope.tipo_situacao =  localStorage.getItem('tipo_situacao');
    
    var pdfjsLib = window['pdfjs-dist/build/pdf'];


    $scope.$on('$viewContentLoaded', function () {

        buscaDados(_id).then(function (result) {
        $scope.dados = result;
       
        $scope.statusNcv = $scope.dados.status

        $scope.buscaCidadesByPatio = async () => {
           const result = await buscaCidadesByPatio()
           $scope.CidadesByPatio = result ;
           const cidade = result.find((x) => x.id_cidade === $scope.dados.id_cidade)
           $scope.dados.id_cidade = cidade ? cidade.id_cidade : null
         }    


     
            $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-apreensao', {params: {ncv: _id}}).then(function (response) {
                
                if (ncvFac.controlePatioFac.isEmpty(response.data)) {
                    $scope.apreensao = {};
                    return;
                    
                }
                $scope.apreensao = {
                    ncv: response.data[0].ncv,
                    data: (response.data[0].data ? new Date(response.data[0].data.substr(3, 2) + '-' + response.data[0].data.substr(0, 2) + '-' + response.data[0].data.substr(6, 4)) : null),
                    hora: response.data[0].hora,
                    id_autoridade: response.data[0].id_autoridade,
                    boletim_ocorrencia: response.data[0].boletim_ocorrencia,
                    local: response.data[0].local,
                    id_motivo: response.data[0].id_motivo,
                    referencia: response.data[0].referencia,
                    cb_adulterado: ((response.data[0].cb_adulterado = 0) ? true : false),
                    cb_crimestransito: ((response.data[0].cb_crimestransito) ? true : false),
                    cb_emtela: ((response.data[0].cb_emtela) ? true : false),
                    cb_foracirculacao: ((response.data[0].cb_foracirculacao) ? true : false),
                    cb_judicial: ((response.data[0].cb_judicial) ? true : false),
                    cb_leasing: ((response.data[0].cb_leasing) ? true : false),
                    cb_motorqueixa: ((response.data[0].cb_motorqueixa) ? true : false),
                    cb_pedirbaixa: ((response.data[0].cb_pedirbaixa) ? true : false),
                    cb_policialcivil: ((response.data[0].cb_policialcivil) ? true : false),
                    cb_roubofurto: ((response.data[0].cb_roubofurto) ? true : false),
                    cb_semdocumentocrv: ((response.data[0].cb_semdocumentocrv) ? true : false),
                    cb_traficodedrogas: ((response.data[0].cb_traficodedrogas) ? true : false),
                    cb_infracao: ((response.data[0].cb_infracao) ? true : false),
                    
                }
        
                $scope.ncv_apreensao = {
                    ncv: response.data[0].ncv,
                    data: (response.data[0].data ? new Date(response.data[0].data.substr(3, 2) + '-' + response.data[0].data.substr(0, 2) + '-' + response.data[0].data.substr(6, 4)) : null),
                    hora: response.data[0].hora,
                    id_autoridade: response.data[0].id_autoridade,
                    boletim_ocorrencia: response.data[0].boletim_ocorrencia,
                    local: response.data[0].local,
                    id_motivo: response.data[0].id_motivo,
                    referencia: response.data[0].referencia,
                    cb_adulterado: ((response.data[0].cb_adulterado = 0) ? true : false),
                    cb_crimestransito: ((response.data[0].cb_crimestransito) ? true : false),
                    cb_emtela: ((response.data[0].cb_emtela) ? true : false),
                    cb_foracirculacao: ((response.data[0].cb_foracirculacao) ? true : false),
                    cb_judicial: ((response.data[0].cb_judicial) ? true : false),
                    cb_leasing: ((response.data[0].cb_leasing) ? true : false),
                    cb_motorqueixa: ((response.data[0].cb_motorqueixa) ? true : false),
                    cb_pedirbaixa: ((response.data[0].cb_pedirbaixa) ? true : false),
                    cb_policialcivil: ((response.data[0].cb_policialcivil) ? true : false),
                    cb_roubofurto: ((response.data[0].cb_roubofurto) ? true : false),
                    cb_semdocumentocrv: ((response.data[0].cb_semdocumentocrv) ? true : false),
                    cb_traficodedrogas: ((response.data[0].cb_traficodedrogas) ? true : false),
                    cb_infracao: ((response.data[0].cb_infracao) ? true : false),
                }       
        
            });
            setTimeout(function (){
                const  altera_status_ncv= localStorage.getItem('altera_status_ncv') === "1" ? true :  false
                
                if ($scope.dados.status == 'APREENDIDO' || $scope.dados.status == 'LEILOADO' || $scope.dados.status == 'INVALIDADO' || $scope.dados.status == 'INATIVO' || $scope.dados.status == '' || $scope.dados.status == null) {
                    $scope._gravar = false;
                } else if($scope.dados.status === 'CANCELADO' ||  altera_status_ncv) {
                    $scope._gravar = false;
                }else {
                    $scope._gravar = true;

                }
               
                
                $scope.origem = {
                    id: _id,
                    data_cadastro: $scope.dados.data_cadastro,
                    id_patio: $scope.dados.id_patio,
                    patio_destino: $scope.dados.patio_destino,
                    chaves: $scope.dados.chaves,
                    id_tarifa: $scope.dados.id_tarifa,
                    numero_etiqueta: $scope.dados.numero_etiqueta,
                    tipo_veiculo: $scope.dados.tipo_veiculo,
                    placa: $scope.dados.placa,
                    estado_veiculo: $scope.dados.estado_veiculo,
                    veiculo_trancado: $scope.dados.veiculo_trancado,
                    marca_modelo: $scope.dados.marca_modelo,
                    codigo_veiculo_prep: $scope.dados.codigo_veiculo_prep,
                    chamado_ultimos: $scope.dados.chamado_ultimos,
                    id_empresa: localStorage.getItem('id_empresa'),
                    ano: $scope.dados.ano,
                    cor: $scope.dados.cor,
                    nome: $scope.dados.nome,  
                    status: $scope.dados.status,
                    situacao: $scope.dados.situacao,
                    adicionado: $scope.dados.adicionado,
                    alterado: $scope.dados.alterado,
                    data_apreensao: $scope.dados.data_apreensao,
                    patio: $scope.dados.id_patio,
                    chassi: $scope.dados.chassi,
                    placa_uf: $scope.dados.placa_uf,
                    placa_municipio: $scope.placa_municipio,
                    numero_motor: $scope.dados.numero_motor,
                    tem_motor: $scope.dados.tem_motor,
                    tipo: $scope.dados.tipo,
                    rua: $scope.dados.rua,
                    edital: $scope.dados.edital,
                    tabelaTarifas: $scope.dados.id_tarifa,
                    localizado_leilao_desc: $scope.dados.localizado_leilao_desc
            
                }; 
                
                $scope.transferencia_veiculos = { data_transferencia : null }

                $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-transferencia', {params: {ncv: _id, patio_origem: $scope.dados.id_patio , patio_destino : $scope.dados.patio_destino }}).then(function (response) {
                    $scope.transferencia_veiculos.data_transferencia   = response.data[0].data_recebimento;
                });
        
                $http.get(urlServidor.urlServidorChatAdmin + '/tipos_veiculo/listar', {params: {idEmpresa: $scope.origem.id_empresa}}).then(function (response) {
                    $scope.tiposVeiculos = response.data;
                    var id = $scope.dados.tipo_veiculo;
                    $scope.veiculo = $scope.tiposVeiculos.filter(function (obj) {
                        return obj.id == id;
                    })
                veiculo = $scope.veiculo[0].descricao;
                });
                $http.get(urlServidor.urlServidorChatAdmin + '/tarifas/itens-tarifas', {params: {idEmpresa: $scope.dados.idempresa}}).then(function (response) {
                    $scope.tabelaTarifas = response.data;
                });

                buscaPatios().then(function (result) {
                    var id_patio = $scope.dados.id_patio;
                    $scope.patios = result ;
                    $scope.patio_termo = $scope.patios.filter(function (obj) {
                        return obj.id == id_patio;
                    })
                    $scope.endereco_patio = $scope.patio_termo[0];
                });

                buscaPatios().then(function (result) {
                  
                    if (result && result.length > 0) {
                        const patioDestino = result.find((item) => item.id == $scope.dados.patio_destino)
                        $scope.patioDestino = patioDestino && patioDestino.nome
                    } else {
                        $scope.patioDestino = ''
                    }

                })

              $scope.buscaCidadesByPatio()
                
                buscaMotivos().then(function (result) {
                    $scope.motivos = result;
                });

            },1000);

        });
        (async () => {
            const { data } = await $http.get(
              urlServidor.urlServidorChatAdmin + "/ncv/buscafotos-conferente",
              {
                params: { ncv: _id },
              }
            );
           // console.log(data)
            const response = data.map((item) => ({
              ...item,
              data: moment(item.data).locale("pt").format("LLLL"),
              dataSemformataca: item.data,
              AnoMes: `${new Date(item.data).getMonth() + 1}/${new Date(
                item.data
              ).getFullYear()}`,
              chassi :$scope.dados.chassi,
              motor :$scope.dados.numero_motor
    
            }));
            const groupBy = (x, f) =>
              x.reduce((a, b) => ((a[f(b)] ||= []).push(b), a), {});
    
            $scope.listaImagensConferente = groupBy(response, (v) => v.AnoMes);
          })();
    });

    $scope.urlImagensChecklist = urlImagens.urlImagensChecklist;
    $scope.urlDocumentos = urlImagens.urlImagensDocumentos;
    $scope.urlFotos = urlImagens.urlImagensFotos;
    $scope.urlImagensConferente = urlImagens.urlImagensConferente;
 
    if(localStorage.getItem('apagar_docs_fotos') == 'SIM') {
        $scope._apagar = true;
    }else{
        $scope._apagar = false;
    }

    if(localStorage.getItem('ajusta_financeiro') == 'SIM') {
        $scope.alterarFinanceiro = true;
    }else{
        $scope.alterarFinanceiro = false;
    }

    if(localStorage.getItem('altera_data_apreensao') == '1') {
      $scope.altera_data_apreensao = true;
    }else{
      $scope.altera_data_apreensao = false;
    }
    
    var ncv_historico = {
        ncv: _id,
        tipo: null,
        descricao: null,
        usuario: localStorage.getItem('nome_usuario')

    };

    $scope.historicos = {
        ncv: _id,
        tipo: null,
        descricao: null,
        usuario: localStorage.getItem('nome_usuario')
    };

    
    function buscaPagamentos() {

      $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-pagamentos', {params: {ncv: _id}}).then(function (response) {

        if (ncvFac.controlePatioFac.isEmpty(response.data)) {
          $scope.apreensao = {};
          return;
        }

        $scope.ncv_pagamentos = response.data ;
        $scope.pagamentos = response.data ;
         
        var arr = [];
        
        for (var i = 0; i <= $scope.ncv_pagamentos.length-1; i++){
          arr[i] =  $scope.ncv_pagamentos[i].data.toString();
          $scope.ncv_pagamentos[i].data  = new Date(arr[i].substr(6,4), arr[i].substr(3,2)-1,arr[i].substr(0,2));
    
        }        
              
      });
    }

    $scope.buscaPagamentos = function () {
      buscaPagamentos()
    }

    $scope.buscaPagamentos()

    
    var ncv_guincho = null;    

    $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-guincho', {params: {ncv: _id}}).then(function (response) {
        if (ncvFac.controlePatioFac.isEmpty(response.data)) {
            $scope.guincho = {};
            return;
        }
        
       ncv_guincho = {
        ncv: response.data[0].ncv,
        blitz: response.data[0].blitz,
        id_reboque: response.data[0].id_reboque,
        id_motorista: response.data[0].id_motorista,
        guinchado: response.data[0].guinchado,
        guincho_coletivo: response.data[0].guincho_coletivo,
        km_percorrido: response.data[0].km_percorrido,
        aplicativo: response.data[0].aplicativo,
        ordem_servico: response.data[0].ordem_servico,
        pedagio: response.data[0].pedagio,
        observacao_pedagio: response.data[0].observacao_pedagio,
        desconto: response.data[0].desconto,
        observacao_desconto: response.data[0].observacao_desconto


       };
        
        $scope.guincho = {
            ncv: response.data[0].ncv,
            blitz: response.data[0].blitz,
            id_reboque: response.data[0].id_reboque,
            id_motorista: response.data[0].id_motorista,
            guinchado: response.data[0].guinchado,
            guincho_coletivo: response.data[0].guincho_coletivo,
            km_percorrido: response.data[0].km_percorrido,
            aplicativo: response.data[0].aplicativo,
            ordem_servico: response.data[0].ordem_servico,
            pedagio: response.data[0].pedagio,
            observacao_pedagio: response.data[0].observacao_pedagio,
            desconto: response.data[0].desconto,
            observacao_desconto: response.data[0].observacao_desconto

        };
        return ncv_guincho;

    });



    $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-fotos', {params: {ncv: _id}}).then(function (response) {
        $scope.listaImagens = response.data;
    });
    $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-documentosimagens', {params: {ncv: _id}}).then(function (response) {
        $scope.listaDocumentosImagem = response.data;
    });
    $scope.trustSrc = function(src) {
        return  $sce.trustAsResourceUrl(src);
    }  

    const GetDocuments =async () => {
       const response =  await   $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-documentospdf', {params: {ncv: _id}})
       
            $scope.listaDocumentosPdf = response.data;
       
    }
    GetDocuments()
    $http.put(urlServidor.urlServidorChatAdmin + '/tipos-recebimento/listar-tipo',{tipo: 'RECEBER'}).then(function (response) {
        $scope.tiposRecebimento = response.data;
    });
    var autoridade;
    $http.put(urlServidor.urlServidorChatAdmin + '/autoridades/listar', {ativo: '1'}).then(function (response) {
        $scope.autoridades = response.data;
        
    });

    $http.get(urlServidor.urlServidorChatAdmin + '/ncv/buscafotos-conferente', {params: {ncv: _id}}).then(function (response) {

        $scope.listaImagensConferente = response.data ;
    
    });

    $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-checklist', {params: {ncv: _id}}).then(function (response) {

        $scope.checklist = {
            ncv: response.data[0].ncv,
            id_chamado: response.data[0].id_chamado,
            data: (response.data[0].data ? new Date(response.data[0].data.substr(3, 2) + '-' + response.data[0].data.substr(0, 2) + '-' + response.data[0].data.substr(6, 4)) : null),
            placa: response.data[0].placa,
            ano: response.data[0].ano,
            marca: response.data[0].marca,
            modelo: response.data[0].modelo,
            chassi: response.data[0].chassi,
            cor: response.data[0].cor,
            km: response.data[0].km,
            imagem_km: response.data[0].imagem_km,
            imagem_combustivel: response.data[0].imagem_combustivel,
            marcabateria: response.data[0].marcabateria,
            estadopintura: response.data[0].estadopintura,
            estadotapecaria: response.data[0].estadotapecaria,
            estadopneus: response.data[0].estadopneus,
            arcondicionado: ((response.data[0].arcondicionado) ? true : false),
            vidroeletrico: ((response.data[0].vidroeletrico) ? true : false),
            cambiomanual: ((response.data[0].cambiomanual) ? true : false),
            cambioautomatico: ((response.data[0].cambioautomatico) ? true : false),
            acendedor: ((response.data[0].acendedor) ? true : false),
            radiocd: ((response.data[0].radiocd) ? true : false),
            frente: ((response.data[0].frente) ? true : false),
            tela: ((response.data[0].tela) ? true : false),
            extintor: ((response.data[0].extintor) ? true : false),
            pneustep: ((response.data[0].pneustep) ? true : false),
            macaco: ((response.data[0].macaco) ? true : false),
            triangulo: ((response.data[0].triangulo) ? true : false),
            rodacomum: ((response.data[0].rodacomum) ? true : false),
            rodaespecial: ((response.data[0].rodaespecial) ? true : false),
            calotas: ((response.data[0].calotas) ? true : false),
            antena: ((response.data[0].antena) ? true : false),
            documento: ((response.data[0].documento) ? true : false),
            carrofuncionando: ((response.data[0].carrofuncionando) ? true : false),
            bateria: ((response.data[0].bateria) ? true : false),
            chaverodas: ((response.data[0].chaverodas) ? true : false)
        }

        $scope.listaImagensChecklist = response.data[1];
        if ($scope.listaImagensChecklist !== undefined) {
            if (response.data[0].imagem_combustivel) {
                $scope.listaImagensChecklist.push({tipo: 'combustivel', imagens: response.data[0].imagem_combustivel});
            }
            if (response.data[0].imagem_km) {
                $scope.listaImagensChecklist.push({tipo: 'km', imagens: response.data[0].imagem_km});
            }
        }

       
        
    });
/// Detran Colsulta

$scope.GotoDetran = () =>{

    window.open(`/#/Consulta-Detran?id=${_id}`);

}
    $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-historico', {params: {ncv: _id}}).then(function (response) {

        $scope.listaHistoricos = response.data;
    });

    let hoje = new Date();
    $scope.estados = estados.uf;
    
    
    $scope.uploadFotoConferente = (file) => {
  
        if( file !== null ){    
            if (file.type !== null && file.type !== undefined) {
    
                if (file.type.substr(0, 5) == 'image') {
    
                    let nomefoto = _id + '/' + makeid() + '.' + file.type.substr(6, 4);
                        
                    S3UploadService.Upload(file, buckets.fotos_conferente, nomefoto.substr(0, nomefoto.lastIndexOf('.'))+'.jpg'  ,'us-east-1').then(function (result) {
                        // Mark as success
                        file.Success = true;
                     //   console.log(result);
                       
                        $http.post(urlServidor.urlServidorChatAdmin + '/ncv/incluir-foto-conferente',
                                {ncv: _id, tipo: $scope.conferente.novotipo, descricao: $scope.conferente.novadescricao,
                                    imagem:  nomefoto.substr(0, nomefoto.lastIndexOf('.'))  }
                        ).then( (response)=> {

                            $scope.listaImagensConferente.push({tipo: $scope.conferente.novotipo, imagem: nomefoto.substr(0, nomefoto.lastIndexOf('.')) , id: response.data.insertId  });

                        });
                    }, function (error) {
                        // Mark the error
                        $scope.Error = error;
                    }, function (progress) {
                        // Write the progress as a percentage
                        var alt = {
                            ncv: _id,
                            tipo: $scope.fotos.novotipo,
                            descricao: $scope.fotos.novadescricao,
                            imagem: nomefoto
    
                        }
                        
                        var json = Object.assign({}, alt);
                        $scope.logs = {
                            ncv: _id,
                            operacao: 'INCLUSÃO',
                            tipo: 'FOTOS',
                            id_usuario: localStorage.getItem('id_usuario'),
                            alteracoes: JSON.stringify(json),
                            anterior: null
        
                            
                        };
                        $http.post(urlServidor.urlServidorChatAdmin + '/ncv/logs-ncv', $scope.logs).then(function (response){
                            if(response.data.code){
                                $scope.erro = response;
                            }
                        });
                        file.Progress = (progress.loaded / progress.total) * 100
                    });
                }
    
            }            
    
        }
    
    }
       

    $scope.uploadFoto = (file) => {

    if( file !== null ){    

        if (file.type !== null && file.type !== undefined) {

            if (file.type.substr(0, 5) == 'image') {

                let nomefoto = _id + '/' + makeid() + '.' + file.type.substr(6, 4);
//			S3UploadService.Upload(file, 'imagens-reidospatios/'+_id , nomefoto ).then(function (result) {
                S3UploadService.Upload(file, buckets.fotos_ncv, nomefoto).then(function (result) {
                    // Mark as success
                    file.Success = true;
                   
                    $http.post(urlServidor.urlServidorChatAdmin + '/ncv/incluir-foto',
                            {ncv: _id, tipo: $scope.fotos.novotipo, descricao: $scope.fotos.novadescricao,
                                imagem: nomefoto}
                    ).then(function (response) {

                        $scope.listaImagens.push({tipo: $scope.fotos.novotipo, imagem: nomefoto, id: response.data.insertId});
                        
                    });
                }, function (error) {
                    // Mark the error
                    $scope.Error = error;
                }, function (progress) {
                    // Write the progress as a percentage
                    var alt = {
                        ncv: _id,
                        tipo: $scope.fotos.novotipo,
                        descricao: $scope.fotos.novadescricao,
                        imagem: nomefoto

                    }
                    
                    var json = Object.assign({}, alt);
                    $scope.logs = {
                        ncv: _id,
                        operacao: 'INCLUSÃO',
                        tipo: 'FOTOS',
                        id_usuario: localStorage.getItem('id_usuario'),
                        alteracoes: JSON.stringify(json),
                        anterior: null
    
                        
                    };
                    $http.post(urlServidor.urlServidorChatAdmin + '/ncv/logs-ncv', $scope.logs).then(function (response){
                        if(response.data.code){
                            $scope.erro = response;
                        }
                    });
                    file.Progress = (progress.loaded / progress.total) * 100
                });
            }

        }            

    }

    }
   

    $scope.mapa = function (ncv, $root, $rootScope) {
        

        if(($scope.dados.latitude != null && $scope.dados.longitude != null) && ($scope.dados.latitude != '' && $scope.dados.longitude != '')){
            
                $scope.coordenadas = {
                    latitude: $scope.dados.latitude,
                    longitude: $scope.dados.longitude
                }
            
        }else{
            $scope.coordenadas = 0;
        }
       
        if(($scope.dados.rfid != null && $scope.dados.rfid != '') || $scope.coordenadas != 0){
        
            $mdDialog.show({
                controller: function ($scope, $mdDialog, $mdToast) {

                    function openMap () {

                        //	var latlng = L.latLng(urlParams.dados[0].latitude_atendimento, urlParams.dados[0].longitude_atendimento);

                        if (mymap.off != undefined) {
                            mymap.off();
                            mymap.remove();
                        }
                        mymap = L.map('mapid').setView([$scope.dados.latitude, $scope.dados.longitude], 15);
                        mapLink = '<a href="https://openstreetmap.org">OpenStreetMap</a>';
                        L.tileLayer(
                                'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                                    attribution: 'Map data &copy; ' + mapLink,
                                    maxZoom: 20,
                                }).addTo(mymap);
                //		var marker = L.marker([$scope.dados.latitude_chamador, $scope.dados.longitude_chamador]).addTo(mymap);

                        var marker = L.marker([$scope.dados.latitude, $scope.dados.longitude]).addTo(mymap);
                    }


                    setTimeout(function(){
                        openMap();
                    }, 500);

                    $scope.cancel = function (){
                        $mdDialog.cancel();
                    }

                    
                },
                templateUrl: 'openMap.html',
                scope:$scope,
                preserveScope: true,
                scopoAnterior: $scope
            }).then(function (answer) {
                let a = answer;
                
            }, function () {
                $scope.statusdialog = 'You cancelled the dialog.';
            });
        }else {
            myFunctions.showAlert("Não foi possível consultar a geolocalização ! ");
         
        }
        
    }

    $scope.uploadDocumento = (file) => {

    let nomefoto = '';

    if( file !== null ){    

        if (file.type !== null && file.type !== undefined) {

            if (file.type.substr(0, 5) == 'image' || file.type == 'application/pdf') {

                if (file.type == 'application/pdf') {
                    nomefoto = _id + '/' + makeid() + '.' + file.type.substr(12, 3);
                } else {
                    nomefoto = _id + '/' + makeid() + '.' + file.type.substr(6, 4);
                }

                S3UploadService.Upload(file, buckets.documentos_ncv, nomefoto).then(function (result) {
                    // Mark as success
                    file.Success = true;
                   
                    $http.post(urlServidor.urlServidorChatAdmin + '/ncv/incluir-documento',
                            {ncv: _id, tipo: $scope.documentos.novotipo, descricao: $scope.documentos.novadescricao,
                                imagem: nomefoto}
                    ).then(function (response) {

                        if (file.type == 'application/pdf') {
                            $scope.listaDocumentosPdf.push({tipo: $scope.documentos.novotipo, imagem: nomefoto, id: response.data.insertId});
                        } else {
                            $scope.listaDocumentosImagem.push({tipo: $scope.documentos.novotipo, imagem: nomefoto, id: response.data.insertId});
                        }
                       
                       
                    });

                }, function (error) {
                    // Mark the error
                    $scope.Error = error;
                }, function (progress) {
                    // Write the progress as a percentage
                    var alt = {
                        tipo: $scope.documentos.novotipo, 
                        descricao: $scope.documentos.novadescricao,
                        imagem: nomefoto
                        
                    }
                    var json = Object.assign({}, alt);
                    $scope.logs = {
                        ncv: _id, 
                        operacao: 'INCLUSÃO',
                        tipo: 'DOCUMENTOS',
                        id_usuario: localStorage.getItem('id_usuario'),
                        alteracoes: JSON.stringify(json),
                        anterior: null

                    };
                    $http.post(urlServidor.urlServidorChatAdmin + '/ncv/logs-ncv', $scope.logs).then(function (response){
                        if(response.data.code){
                            $scope.erro = response;
                        }
                    });
                    file.Progress = (progress.loaded / progress.total) * 100
                });
            }

        }

    }

    }

    $scope.apagarImagem = (dados) => {
    idimagem = dados.id;
    let confirm = $mdDialog.confirm()
    .title('Fotos NCV!')
    .textContent('Tem certeza que deseja apagar essa imagem?')
    .ariaLabel('Lucky day')
    .ok('Apagar')
    .cancel('Cancelar');
    $mdDialog.show(confirm).then(function () {
        
        $http.post(urlServidor.urlServidorChatAdmin + '/ncv/apaga-fotoncv',{id: idimagem}).then(function (response){
            if(response.data.code){
                myFunctions.showAlert('Não foi possível apagar a imagem !!');
            }else{
                myFunctions.showAlert('Imagem apagada !!');
                var alt = {
                    idImagem: dados.id,
                    imagem: dados.imagem,
                    tipo: dados.tipo,
                    descricao: dados.descricao
                }
                
              
                var json = Object.assign({}, alt);
                $scope.logs = {
                    ncv: _id, 
                    operacao: 'EXCLUSÃO',
                    tipo: 'FOTOS',
                    id_usuario: localStorage.getItem('id_usuario'),
                    alteracoes: '',
                    anterior: JSON.stringify(json)
                };
                
                $http.post(urlServidor.urlServidorChatAdmin + '/ncv/logs-ncv', $scope.logs).then(function (response){
                    if(response.data.code){
                        $scope.erro = response;
                    }else{
                        dados = null;
                        alt = null;   
                        setTimeout(function(){
            
                            $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-fotos', {params: {ncv: _id}}).then(function (response) {
                                $scope.listaImagens = response.data;
                            }); 
              
                          }, 1500);     
                        
                    }
                });
            }
        });        
       
    });

    }

    $scope.apagarDocumento = (dados) => {
        idimagem = dados.id;
        let confirm = $mdDialog.confirm()
        .title('Documentos NCV !')
        .textContent('Tem certeza que deseja apagar esse documento?')
        .ariaLabel('Lucky day')
        .ok('Apagar')
        .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {

            $http.post(urlServidor.urlServidorChatAdmin + '/ncv/apaga-documentoncv', {id: idimagem}).then(function (response){
                if(response.data.code){
                    myFunctions.showAlert('Não foi possível apagar o documento !!');
                    
                }else{
                    myFunctions.showAlert('Documento apagado !!');
                }
            });
            var alt = {
                idImagem: dados.id,
                imagem: dados.imagem,
                tipo: dados.tipo,
                descricao: dados.descricao
            }
                
            var json = Object.assign({}, alt);
            
            $scope.logs = {
                ncv: _id, 
                operacao: 'EXCLUSÃO',
                tipo: 'DOCUMENTOS',
                id_usuario: localStorage.getItem('id_usuario'),
                alteracoes: '',
                anterior: JSON.stringify(json)
            };

            $http.post(urlServidor.urlServidorChatAdmin + '/ncv/logs-ncv', $scope.logs).then(function (response){
                if(response.data.code){
                    $scope.erro = response;
                }else{
                    $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-documentosimagens', {params: {ncv: _id}}).then(function (response) {
                        $scope.listaDocumentosImagem = response.data;
                    });
                    $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-documentospdf', {params: {ncv: _id}}).then(function (response) {
                        $scope.listaDocumentosPdf = response.data;
                    });
                }
            });
            
            dados = null;
        });

    }

    const altera_status_ncv =
    localStorage.getItem("altera_status_ncv") === "1" ? true : false;

    $scope.alteraStatus = altera_status_ncv
    $scope.apagarConferente = (dados) => {
        idimagem = dados.id;
        let confirm = $mdDialog.confirm()
        .title('Conferentes NCV !')
        .textContent('Tem certeza que deseja apagar essa imagem?')
        .ariaLabel('Lucky day')
        .ok('Apagar')
        .cancel('Cancelar');
    $mdDialog.show(confirm).then(function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/ncv/apaga-fotoconferente', {id: idimagem}).then(function (response){
            if(response.data.code){
                myFunctions.showAlert('Não foi possível apagar a imagem!!');
                
            }else{
                myFunctions.showAlert('Imagem apagada!!');
            }
        });

        var alt = {
            idImagem: dados.id,
            imagem: dados.imagem,
            tipo: dados.tipo
        }
              
        var json = Object.assign({}, alt);
        
        $scope.logs = {
            ncv: _id, 
            operacao: 'EXCLUSÃO',
            tipo: 'CONFERENTE',
            id_usuario: localStorage.getItem('id_usuario'),
            alteracoes: '',
            anterior: JSON.stringify(json)
        };
        $http.post(urlServidor.urlServidorChatAdmin + '/ncv/logs-ncv', $scope.logs).then(function (response){
            if(response.data.code){
                $scope.erro = response;
            }else{
                $http.get(urlServidor.urlServidorChatAdmin + '/ncv/buscafotos-conferente',{params: {ncv: _id}}).then(function (response) {

                    $scope.listaImagensConferente = response.data ;
                });
                
            }
        });
        dados = null;
    });

    }

    
    $scope.apagarChecklist = (dados) => {
        idimagem = dados.id;
        let confirm = $mdDialog.confirm()
        .title('Checklist NCV !')
        .textContent('Tem certeza que deseja apagar essa imagem?')
        .ariaLabel('Lucky day')
        .ok('Apagar')
        .cancel('Cancelar');
    $mdDialog.show(confirm).then(function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/ncv/apaga-fotochecklist', {id: idimagem}).then(function (response){
            if(response.data.code){
                myFunctions.showAlert('Não foi possível apagar a imagem!!');
                
            }else{
                myFunctions.showAlert('Imagem apagada!!');
            }
        });

        var alt = {
            idImagem: dados.id,
            imagem: dados.imagem,
            tipo: dados.tipo
        }
              
        var json = Object.assign({}, alt);
        
        $scope.logs = {
            ncv: _id, 
            operacao: 'EXCLUSÃO',
            tipo: 'CHECKLIST',
            id_usuario: localStorage.getItem('id_usuario'),
            alteracoes: '',
            anterior: JSON.stringify(json)
        };
        $http.post(urlServidor.urlServidorChatAdmin + '/ncv/logs-ncv', $scope.logs).then(function (response){
            if(response.data.code){
                $scope.erro = response;
            }else{
                $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-checklist',{params: {ncv: _id}}).then(function (response) {

                   
                    $scope.listaImagensChecklist=response.data[1];
                });
                
            }
        });
        dados = null;
    });

    }

    $scope.fechar = function () {
        window.close();
    }

    function buscaDados(ncv) {

        return new Promise(function (resolve, reject) {   

            $http.put(urlServidor.urlServidorChatAdmin + '/ncv/listar-usuario', {ncv: _id, idusuario: localStorage.getItem('id_usuario')}).then(function (response) {
                resolve(response.data[0]); 
            });
            
        })

    }

    $scope.isDisabled = false;

    $scope.buscaPlaca = function (placa) {

        $http.get(urlServidor.urlServidorChatAdmin + '/busca-placa', {params: {placa: placa}}).then(function (response) {

            var texto = 'Marca/Modelo: ' + response.data.modelo + '   -    Ano: ' + response.data.ano
            texto = texto + '  -  Cor: ' + response.data.cor + '  -  ' + response.data.municipio;
            let confirm = $mdDialog.confirm()
                    .title('Dados de Veiculo!')
                    .textContent(texto)
                    .ariaLabel('Lucky day')
                    .ok('Gravar')
                    .cancel('Não Gravar');
            $mdDialog.show(confirm).then(function () {

                $scope.dados.ano = response.data.ano;
                $scope.dados.marca_modelo = response.data.modelo;
                $scope.dados.cor = response.data.cor;
                $scope.dados.placa_municipio = response.data.municipio;
                $scope.dados.chassi = response.data.chassi;
                $scope.dados.numero_motor = response.data.motor;
            }, function () {
                $scope.dados.ano = '';
                $scope.dados.marca_modelo = '';
                $scope.dados.cor = '';
                $scope.dados.placa_municipio = '';
                $scope.dados.chassi = '';
                $scope.dados.numero_motor = '';
            })


        }, function errorCallback(response) {

            myFunctions.showAlert("Erro na busca de dados do veiculo , tente novamente ou digite manualmente!")

        })

    }   

    $http.put(urlServidor.urlServidorChatAdmin + '/reboques/listar-ativos').then(function (response) {
        $scope.reboques = response.data;
    });


    //busca nome de usuarios app motorista para ser preenchido como nome de motorista
    $http.get(urlServidor.urlServidorChatAdmin + '/usuarios/listar', {idEmpresa: localStorage.getItem('id_empresa')}).then(function (response) {
        $scope.motoristas = response.data;

    });

/*
    $http.put(urlServidor.urlServidorChatAdmin + '/motoristas/listar', {idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa')}).then(function (response) {
        $scope.motoristas = response.data;
    });

    $http.get(urlServidor.urlServidorChatAdmin + '/usuarios/usuarios-app-motorista', {idEmpresa: localStorage.getItem('id_empresa')}).then(function (response) {
        $scope.motoristas = response.data;
    });

    $scope.buscaMotoristas = function (idreboque) {
        $http.get(urlServidor.urlServidorChatAdmin + '/motoristas/listar-motoristasreboque', {params: {id_reboque: $scope.guincho.id_reboque}}).then(function (response) {
            $scope.motoristas = response.data;
        });
    };
*/

    const  compareObjects = (obj1, obj2) =>{
    const diff = {};
    // Verificar propriedades do objeto 1
    for (const key in obj1) {
      if (obj1.hasOwnProperty(key) && !obj2.hasOwnProperty(key)) {
        diff[key] = {
            anterior: obj1[key],
            alteracao: undefined,
        };
      } else if (obj1.hasOwnProperty(key) && obj2.hasOwnProperty(key)) {
        if (typeof obj1[key] === 'object' && typeof obj2[key] === 'object') {
          const nestedDiff = compareObjects(obj1[key], obj2[key]);
          if (Object.keys(nestedDiff).length > 0) {
            diff[key] = nestedDiff;
          }
        } else if (obj1[key] !== obj2[key]) {
          diff[key] = {
            anterior: obj1[key],
            alteracao: obj2[key],
          };
        }
      }
    }
  
    // Verificar propriedades do objeto 2 que não existem no objeto 1
    for (const key in obj2) {
      if (obj2.hasOwnProperty(key) && !obj1.hasOwnProperty(key)) {
        diff[key] = {
            anterior: undefined,
            alteracao: obj2[key],
        };
      }
    }
  
    return diff;
     }


     const ResetaNcv = async (origem, dados) => {
      

       try {
        if(origem.status !== "LIBERADO" &&  origem.status !== "LIBERADO ARR" ) return

        if (dados.status !== "APREENDIDO")return

         const modal = await $mdDialog
           .confirm()
           .title("Atenção !")
           .textContent(
             "Alterar STATUS LIBERADO para APREENDIDO vai excluir todas informacoes de liberacao lançadas ! deseja Continuar ?"
           )
           .ariaLabel("Lucky day")
           .ok("Gravar")
           .cancel("Não Gravar");
         await $mdDialog.show(modal);

         const res = await $http.put(
           `${urlServidor.urlServidorChatAdmin}/ncv/reset-pgto-lib?id=${dados.id}`
         );
         console.info(res.status);
         return "Success";
       } catch (error) {
         console.error(error);
         return "Not Allowed";
       } finally {
         buscaLiberacao();
       }
     };

     $scope.gravar_NCV = async function (_dados) {
       $scope.dados.patio = $scope.dados.id_patio;
       // $scope.dados.id_cidade_patio = $scope.dados.id_cidade;
       $scope.dados.tabelaTarifas = $scope.dados.id_tarifa;
       $scope.dados.id_empresa = localStorage.getItem("id_empresa");
       $scope.dados.nomeUsuario = localStorage.getItem("nome_usuario");

       const differences = compareObjects($scope.origem, $scope.dados);
       const resetedPagLib = await ResetaNcv($scope.origem, $scope.dados);
       if(resetedPagLib === "Not Allowed"){
        return myFunctions.showAlert("Processo cancelado")
       }

       $http
         .post(
           urlServidor.urlServidorChatAdmin + "/ncv/alterar-ncv",
           $scope.dados
         )
         .then((response) => {
           if (response.data.code) {
             myFunctions.showAlert("Erro na gravação!");
             return;
           }
           myFunctions.showAlert(resetedPagLib === "Success" ? "Alteração executada com sucesso, Veiculo retornado à Apreendido, dados de liberação e pagamento foram resetados" : "Alteração executado com sucesso!");
           if($scope.dados.status === 'CANCELADO' || $scope.dados.status === 'LIBERADO' || $scope.dados.status === 'LIBERADO ARR'){
            $scope.changeStatusPatio($scope.dados.id);
           }
          })
         .catch((error) =>
           myFunctions.showAlert(
             "Erro na gravação!",
             JSON.stringify(error.message)
           )
         )
         .finally(buscaLiberacao());

       $scope.logs = {
         ncv: _id,
         operacao: resetedPagLib === "Success" ? "ALTERAÇÃO DE STATUS RESETADO" :"ALTERAÇÃO",
         tipo: "DADOS NCV",
         id_usuario: localStorage.getItem("id_usuario"),
         alteracoes: JSON.stringify(differences),
         anterior: JSON.stringify($scope.origem),
       };
       $http
         .post(urlServidor.urlServidorChatAdmin + "/ncv/logs-ncv", $scope.logs)
         .then(function (response) {
           if (response.data.code) {
             $scope.erro = response;
           }
         });
     };
 

    $scope.gravar_apreensao = function () {

        var novos_dados = $scope.apreensao;
        var ncv_origem = $scope.ncv_apreensao;
        function isEquivalent(ncv_origem, novos_dados) {
            // nome das propriedades
            var ncv_origemProps = Object.getOwnPropertyNames(ncv_origem);
            var novos_dadosProps = Object.getOwnPropertyNames(novos_dados);
        
            //comparando se os dois arrays são iguais
            if (ncv_origemProps.length != novos_dadosProps.length) {
                return false;
            }
            var diference = [];
            for (var i = 0; i < ncv_origemProps.length; i++) {
                var propName = ncv_origemProps[i];
        
               //comparando se os valores são iguais
                if (ncv_origem[propName] != novos_dados[propName]) {
                    diference[propName] = novos_dados[propName];
                   
                    //se forem diferente o array diference recebe o valor original
                }
            }
            
            return diference;
            
            
        }
        
        var alterados = isEquivalent(ncv_origem, novos_dados);
       
        var json = Object.assign({}, alterados);
        if(JSON.stringify(json) !== '{}'){
            var origem_json = Object.assign({}, ncv_origem);

            $scope.logs = {
                ncv: _id,
                operacao: 'ALTERAÇÃO',
                tipo: 'APREENSAO',
                id_usuario: localStorage.getItem('id_usuario'),
                alteracoes: JSON.stringify(json),
                anterior: JSON.stringify(origem_json)
            
           
            //data_hora: new Date()

            };

            $http.post(urlServidor.urlServidorChatAdmin + '/ncv/logs-ncv/', $scope.logs).then(function (response){
                if(response.data.code){
                    $scope.erro = response;
                }
            });
        }

        $http.post(urlServidor.urlServidorChatAdmin + '/ncv/alterar-apreensao', $scope.apreensao).then(function (response) {
            if (response.data.code) {
                myFunctions.showAlert('Erro na gravação!');
            } else {
                buscaLiberacao();
                myFunctions.showAlert('Atualização executada com sucesso!');
            }
            ;
        });

        
    };
    $scope.gravar_guincho = function () {
        
        /*var ncv_guincho = {
            ncv: response.data[0].ncv,
            blitz: response.data[0].blitz,
            id_reboque: response.data[0].id_reboque,
            id_motorista: response.data[0].id_motorista,
            guinchado: response.data[0].guinchado,
            guincho_coletivo: response.data[0].guincho_coletivo,
            km_percorrido: response.data[0].km_percorrido,
            aplicativo: response.data[0].aplicativo,
            ordem_servico: response.data[0].ordem_servico
        };*/

        var novos_dados = $scope.guincho;
        function isEquivalent(ncv_guincho, novos_dados) {
            // nome das propriedades
            var ncv_guinchoProps = Object.getOwnPropertyNames(ncv_guincho);
            var novos_dadosProps = Object.getOwnPropertyNames(novos_dados);
        
            //comparando se os dois arrays são iguais
            if (ncv_guinchoProps.length != novos_dadosProps.length) {
                return false;
            }
            var diference = []
            for (var i = 0; i < ncv_guinchoProps.length; i++) {
                var propName = ncv_guinchoProps[i];
        
               //comparando se os valores são iguais
                if (ncv_guincho[propName] !== novos_dados[propName]) {
                    diference[propName] = novos_dados[propName];
                    //se forem diferente o array diference recebe o valor original
                }
            }
            
            return diference;
        }
        
        var alterados = isEquivalent(ncv_guincho, novos_dados);
        var json = Object.assign({}, alterados);
       
        
        $http.post(urlServidor.urlServidorChatAdmin + '/ncv/alterar-guincho', $scope.guincho).then(function (response) {
            if (response.data.code) {
                myFunctions.showAlert('Erro na gravação!');
            } else {
                buscaLiberacao();
                myFunctions.showAlert('Atualização executada com sucesso!');
            }
        });
        if(JSON.stringify(json) !== '{}'){
            
            var origem_json = Object.assign({}, ncv_guincho);


            $scope.logs = {
                ncv: _id,
                operacao: 'ALTERAÇÃO',
                tipo: 'GUINCHO',
                id_usuario: localStorage.getItem('id_usuario'),
                alteracoes: JSON.stringify(json),
                anterior: JSON.stringify(origem_json),
                data_hora: new Date()
           
            //data_hora: new Date()

            };
            $http.post(urlServidor.urlServidorChatAdmin + '/ncv/logs-ncv', $scope.logs).then(function (response){
                if(response.data.code){
                    $scope.erro = response;
                }
            });
        }
        
    };



    $scope.gravaLiberacao = function (event) {

        let total = $scope.infoPgto.total ; 

        if( $scope.infoPgto.total.toFixed(2) != $scope.liberacao.total.toFixed(2) ){

            myFunctions.showAlert('Soma dos pagamentos deve ser igual a total calculado da liberação!');

        } else {
        
            ncvFac.validaCamposObrigatorios($scope.dados, $scope.guincho, $scope.apreensao, $scope.liberacao).then(function (_resultado) {
                
                var textoDados = _resultado;
                if (_resultado === '') {
                    $http.post(urlServidor.urlServidorChatAdmin + '/ncv/grava-liberacao', $scope.liberacao).then(function (response) {
                        if (response.data.code) {
                            myFunctions.showAlert('Erro na gravação!')
                        } else {
                            $scope.dados.version = ($scope.dados.version || 0) + 1;
                            $scope._gravar = true;
                            myFunctions.showAlert('Liberação gravada com sucesso!');
                            $scope.changeStatusPatio($scope.dados.id);
                        }
                    });

                } else {
                        let confirm = $mdDialog.alert()
                                .title('Campos Obrigatórios!')
                                .htmlContent(textoDados)
                                .ariaLabel('Lucky day')
                                .ok('Ok');
                        $mdDialog.show(confirm).then(function () {
                            let ok = '';
                        });
                }

            });
        }

    };

    $scope.gravaPgto = function (retorno) {
        $scope.infoPgto.total = retorno.total;
        $scope.dados.version = ($scope.dados.version || 0) + 1;
    };

    $scope.openDialogFormaPagto = function () {};

    $scope.gravar_historicos = function () {
        $http.post(urlServidor.urlServidorChatAdmin + '/ncv/incluir-historico', $scope.historicos).then(function (response) {
            if (response.data.code) {
                myFunctions.showAlert('Erro na gravação!')
            } else {

                $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-historico', {params: {ncv: _id}}).then(function (response) {
                    $scope.listaHistoricos = response.data;
                    $scope.historicos.ncv = _id,
                            $scope.historicos.tipo = null,
                            $scope.historicos.descricao = null,
                            $scope.historicos.usuario = localStorage.getItem('nome_usuario')

                    myFunctions.showAlert('Gravação executada com sucesso!');
                });
            }
        });
        var novos_dados = $scope.historicos;
        function isEquivalent(ncv_historico, novos_dados) {
            // nome das propriedades
            var ncv_historicoProps = Object.getOwnPropertyNames(ncv_historico);
            var novos_dadosProps = Object.getOwnPropertyNames(novos_dados);
        
            //comparando se os dois arrays são iguais
            if (ncv_historicoProps.length != novos_dadosProps.length) {
                return false;
            }
            var diference = []
            for (var i = 0; i < ncv_historicoProps.length; i++) {
                var propName = ncv_historicoProps[i];
        
               //comparando se os valores são iguais
                if (ncv_historico[propName] !== novos_dados[propName]) {
                    diference[propName] = novos_dados[propName];
                    //se forem diferente o array diference recebe o valor original
                }
            }
            
            return diference;
            
            
        }
        
        var alterados = isEquivalent(ncv_historico, novos_dados);
       
        var json = Object.assign({}, alterados);
        
        if(JSON.stringify(json) !== '{}'){
            $scope.logs = {
                ncv: _id,
                operação: 'INCLUSÃO',
                tipo: 'HISTORICOS',
                id_usuario: localStorage.getItem('id_usuario'),
                alteracoes: JSON.stringify(json),
                anterior: null
                
               
                //data_hora: new Date()
    
            };
    
            $http.post(urlServidor.urlServidorChatAdmin + '/ncv/logs-ncv', $scope.logs).then(function (response){
                if(response.data.code){
                    $scope.erro = response;
                }
            });
        }
        
    };

    $scope.changeStatusPatio = function(ncv){

      var noPatio = 0

      $http.post(urlServidor.urlServidorChatAdmin + '/ncv/altera-patio', {id: ncv, no_patio: noPatio }).then(function (response) {
           
        if (response.data.code) {
          $scope.error = response.data
        }

      })

    }

    function buscaPatios() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', {idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa')}).then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }

    function buscaCidadesByPatio() {
        return new Promise(function (resolve, reject) {

            $http.get(`${urlServidor.urlServidorChatAdmin}/cidades/listar-patios-cidades`, { params: { id_patio: $scope.dados.id_patio } }).then( function (response){
                let dados = response.data;
                resolve(dados);
            })       

        })    
    }    

    function days_between(date1, date2) {

        // The number of milliseconds in one day
        var ONE_DAY = 1000 * 60 * 60 * 24

        // Convert both dates to milliseconds
        var date1_ms = date1.getTime()
        var date2_ms = date2.getTime()

        // Calculate the difference in milliseconds
        var difference_ms = Math.abs(date1_ms - date2_ms)

        // Convert back to days and return
        return Math.round(difference_ms / ONE_DAY)

    }

    $scope.incluiFaturado = incluiFaturado;

    function incluiFaturado(ncv) {
        var _ncv = ncv;
        var _action = 'incluir';
        $mdDialog.show({
            controller: function ($scope, $mdDialog, $mdToast) {
                $scope.dados_faturado = {
                    id: '',
                    nome: '',
                    cpf_cnpj: '',
                    endereco: '',
                    bairro: '',
                    cidade: '',
                    cep: '',
                    uf: '',
                    email: '',
                    telefone: '',
                    status: '1',
                    idempresa: localStorage.getItem('id_empresa')
                }
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.saveFaturado = function (idmotoristaApp) {

                    if (_action == 'incluir') {

                        $http.post(urlServidor.urlServidorChatAdmin + '/clientes/cadastrar', $scope.dados_faturado).then(function (response) {
                            $rootScope.dadoscliente = $scope.dados_faturado;
                        });
                    }

                    if (_action == 'alterar') {

                        $http.post(urlServidor.urlServidorChatAdmin + '/clientes/alterar', $scope.dados_faturado).then(function (response) {
                            $rootScope.dadoscliente = $scope.dados_faturado;
                        });
                    }


                };
                $scope.saveAndClose = function () {
                    $scope.saveFaturado();
                    $rootScope._dadosfaturado = $scope.dados_faturado;
                    $mdDialog.hide();
                };
                $scope.estados = estados.uf;
                $scope.buscaCep = (cep) => {

                    if (cep.length == 9) {

                        viaCep.get(cep).then(function (response) {

                            $scope.dados_faturado.endereco = response.logradouro.toUpperCase();
                            $scope.dados_faturado.bairro = response.bairro.toUpperCase();
                            $scope.dados_faturado.cidade = response.localidade.toUpperCase();
                            $scope.dados_faturado.uf = response.uf.toUpperCase();
                        });
                    }
                    

                }
                $scope.buscaCpfCnpj = (cpfCnpj) => {

                    if (cpfCnpj.length == 14 || cpfCnpj.length == 18) {

                        $http.get(urlServidor.urlServidorChatAdmin + '/clientes/busca-cpfcnpj', {params: {cpf_cnpj: cpfCnpj}}).then(function (response) {

                            $scope.motoristas = response.data;
                            if (response.data.length > 0) {
                                $scope.dados_faturado.id = response.data[0].id;
                                $scope.dados_faturado.cep = response.data[0].cep;
                                $scope.dados_faturado.nome = response.data[0].nome;
                                $scope.dados_faturado.endereco = response.data[0].endereco;
                                $scope.dados_faturado.bairro = response.data[0].bairro;
                                $scope.dados_faturado.cidade = response.data[0].cidade;
                                $scope.dados_faturado.uf = response.data[0].uf;
                                $scope.dados_faturado.email = response.data[0].email;
                                $scope.dados_faturado.telefone = response.data[0].telefone;
                                _action = 'alterar';
                            }
                            faturado_nome = $scope.dados_faturado.nome;
                            faturado_telefone =  $scope.dados_faturado.telefone;
                            cidade_faturado = $scope.dados_faturado.cidade+'-'+$scope.dados_faturado.uf;
        

                        });
                    }

                }
            },
            templateUrl: 'cadastra-faturado.html',
            preserveScope: true
        }).then(function (answer) {
            let a = answer;
            $scope.$resolve.$scope.liberacao.cliente = angular.copy($rootScope._dadosfaturado);
            $scope.$resolve.$scope.liberacao.cpf_faturado= $rootScope._dadosfaturado.cpf_cnpj;
            $scope.$resolve.$scope.liberacao.faturado_para = $rootScope._dadosfaturado.nome;
        }, function () {
            $scope.statusdialog = 'You cancelled the dialog.';
        });
    }

    
    const somarTotalNoArray = (array) => {
      if (array && array.length > 0) {
        const total = array.reduce(
          (accumulator, currentItem) => accumulator + parseFloat(currentItem.valor),
          0
        );
        return total.toFixed(2); // Arredonda para 2 casas decimais
      } else {
        myFunctions.showAlert("Sem pagamento anterior!");
        return "0.00"; 
      }
    };
   
    const getAvaliblePrice = (currentTotal, total) => total - currentTotal;
  
    $scope.incluiFormaPagamento = async () => {
      const pagamentos = $scope.ncv_pagamentos;
      const currentTotal = somarTotalNoArray($scope.ncv_pagamentos);
      const total = $scope.ajustes_liberacao.total;

      if (currentTotal == total) {
        myFunctions.showAlert(
          "Não foi possivel adicionar um novo pagamento! Pagamento atual é equivalante ao valor total!"
        );
        return;
      }
      const avaliblePrice = getAvaliblePrice(currentTotal, total);
      
      const payload = {
       ...pagamentos[pagamentos.length - 1],
        valor: avaliblePrice,
        id: null
      };

      if ("$$hashKey" in payload) delete payload["$$hashKey"];
      $scope.ncv_pagamentos.push(payload);

    };
    
    $scope.comunicacaoDiretiva = {
      watchNcv: function () {
        $scope.$broadcast('chamarFuncaoNaDiretiva', {})
      }
    }
    
    $scope.ajustesLiberacao = ajustesLiberacao;

    function ajustesLiberacao(ncv, estadias, valor_estadia, valor_guincho,desconto,acrescimo,total,$root,$rootScope) {
      
      var _ncv = ncv;
      var _estadias = estadias;
      var _valorEstadia = valor_estadia;
      var _valorGuincho = valor_guincho;
      var _desconto = desconto;
      var _acrescimo = acrescimo;
      var _total = total;

      $scope.buscaPagamentos()

      $mdDialog
        .show({
          controller: function ($scope, $mdDialog, $mdToast) {

            $scope.ajustes_liberacao = $scope.liberacao;

            var origem_pagamento = $scope.pagamentos;

            $scope.atualizaValorLiberacao = function () {
              let data_liberacao = $scope.ajustes_liberacao.data;
              let data_ap = $scope.apreensao.data;

              const diff = Math.abs(
                data_liberacao.getTime() - data_ap.getTime()
              );
              const days = Math.ceil(diff / (1000 * 60 * 60 * 24));

              $scope.ajustes_liberacao.estadias = days + 1;

              let estadias = $scope.ajustes_liberacao.estadias;
              let valor_estadia = $scope.ajustes_liberacao.valor_estadia;
              let valor_guincho = $scope.ajustes_liberacao.valor_guincho;
              let vtotal =
                Number.parseInt(estadias) * Number.parseFloat(valor_estadia) +
                Number.parseFloat(valor_guincho);
              //		vtotal =  $scope.liberacao.total.replace(',','.') ;
              let acrescimo = $scope.ajustes_liberacao.acrescimo;
              //		acrescimo =  $scope.liberacao.acrescimo.replace(',','.') ;
              let desconto = $scope.ajustes_liberacao.desconto;
              //		desconto =  $scope.liberacao.desconto.replace(',','.') ;

              $scope.ajustes_liberacao.total =
                Number.parseFloat(vtotal) +
                Number.parseFloat(acrescimo) -
                Number.parseFloat(desconto);
            };

            $scope.cancel = function () {
              //botão fechar

              $scope.comunicacaoDiretiva.watchNcv()

              _ncv = null;
              _estadias = null;
              _valorEstadia = null;
              _valorGuincho = null;
              _desconto = null;
              _acrescimo = null;
              _total = null;
              
              $scope.ajustes_liberacao = {
                ncv: null,
                estadias: null,
                valorEstadia: null,
                valorGuincho: null,
                desconto: null,
                acrescimo: null,
                total: null,
              };

              $mdDialog.cancel();
            };

            function isEquivalent(ncv_origem, novos_dados) {
              // nome das propriedades
              var ncv_origemProps = Object.getOwnPropertyNames(ncv_origem);
              var novos_dadosProps = Object.getOwnPropertyNames(novos_dados);

              //comparando se os dois arrays são iguais
              if (ncv_origemProps.length != novos_dadosProps.length) {
                return false;
              }
              var diference = [];
              for (var i = 0; i < ncv_origemProps.length; i++) {
                var propName = ncv_origemProps[i];

                //comparando se os valores são iguais
                if (ncv_origem[propName] != novos_dados[propName]) {
                  if (propName == "data") {
                    if (
                      ncv_origem[propName].getDate() ==
                        novos_dados[propName].getDate() &&
                      ncv_origem[propName].getMonth() ==
                        novos_dados[propName].getMonth() &&
                      ncv_origem[propName].getFullYear() ==
                        novos_dados[propName].getFullYear()
                    ) {
                      novos_dados[propName] = ncv_origem[propName];
                    } else {
                      diference[propName] = novos_dados[propName];
                    }
                  } else {
                    diference[propName] = novos_dados[propName];
                  }

                  //se forem diferente o array diference recebe o valor original
                }
              }

              return diference;
            }


            $scope.apagarPagamento = function (
              id,
              data,
              forma_pagamento,
              valor
            ) {
              let confirm = $mdDialog
                .confirm()
                .title("Pagamentos NCV")
                .textContent(
                  "Tem certeza que deseja apagar esses dados de pagamento?"
                )
                .ariaLabel("Lucky day")
                .ok("Apagar")
                .cancel("Cancelar");
              $mdDialog.show(confirm).then(function () {
                var _id = id;
                var _data = data;
                var formaPagamento = forma_pagamento;
                var _valor = valor;

                var pgto = {
                  id: _id,
                  data: _data,
                  forma_pagamento: formaPagamento,
                  valor: _valor,
                };

                $scope.logs = {
                  ncv: ncv,
                  operacao: "EXCLUSÃO",
                  tipo: "PAGAMENTOS",
                  id_usuario: localStorage.getItem("id_usuario"),
                  alteracoes: "",
                  anterior: JSON.stringify(pgto),
                };

                $http
                  .post(
                    urlServidor.urlServidorChatAdmin + "/ncv/apaga-pagamento",
                    { id: id }
                  )
                  .then(function (response) {
                    if (response.data.code) {
                      myFunctions.showAlert(
                        "Ocorreu um erro ao tentar apagar o arquivo!"
                      );
                    }
                  });
                $http
                  .post(
                    urlServidor.urlServidorChatAdmin + "/ncv/logs-ncv",
                    $scope.logs
                  )
                  .then(function (response) {
                    if (response.data.code) {
                      var error = response.data;
                    }
                  });

                var _id = null;
                var _data = null;
                var formaPagamento = null;
                var _valor = null;

                $scope.buscaPagamentos()
              
               $scope.comunicacaoDiretiva.watchNcv()

              });
            };



            $scope.alterarPagamento = function ( id, data, forma_pagamento, valor, cpf_cnpj_pagador, nome_pagador ) {
              
              if (!cpf_cnpj_pagador || !nome_pagador) {
                myFunctions.showAlert(
                  "Nome e CPF/CNPJ do pagador precisam estar preenchidos! Verifique!"
                );
                return;
              }

              const currentTotal = somarTotalNoArray($scope.ncv_pagamentos);
              const total = $scope.ajustes_liberacao.total;

              if (currentTotal > total) {
                myFunctions.showAlert(
                  "Edição não permitida, valor que está tentando alterar ultrapassa o total! Verifique!"
                );
                return;
              }

              const origem = $scope.ncv_pagamentos.find(
                (item) => item.id === id
              );

              $scope.ncv_pagamento = {
                id,
                data,
                forma_pagamento,
                valor,
                cpf_cnpj_pagador,
                nome_pagador,
                ncv
              };


              if ($scope.ncv_pagamento.id == null){             
              
                $http.post(urlServidor.urlServidorChatAdmin + "/ncv/grava-pagamento", $scope.ncv_pagamento).then(() => {
                    
                  if (JSON.stringify($scope.ncv_pagamento) !== "{}") {
                      $scope.logs = {
                        ncv: _id,
                        operacao: "INCLUSÃO",
                        tipo: "PAGAMENTOS",
                        id_usuario: localStorage.getItem("id_usuario"),
                        alteracoes: JSON.stringify($scope.ncv_pagamento),
                        anterior: "",
                        data_hora: new Date(),
                      };
                      
                      $http.post(urlServidor.urlServidorChatAdmin + "/ncv/logs-ncv", $scope.logs).then(function (response) {
                          
                        if (response.data.code) {
                          $scope.erro = response;
                        }
                      });
                    }
                  }
                );
              
                myFunctions.showAlert("Pagamento adicionado com sucesso!");
              
              } else{
            
                $http.post(urlServidor.urlServidorChatAdmin + "/ncv/altera-pagamento", $scope.ncv_pagamento).then(({ data }) => {                 
                  
                  if (data.code)
                    return myFunctions.showAlert("Erro na gravação!");

                  $scope.log_pgto = {
                    ncv: ncv,
                    tipo: "PAGAMENTOS",
                    id_usuario: localStorage.getItem("id_usuario"),
                    alteracoes: JSON.stringify($scope.ncv_pagamento),
                    anterior: JSON.stringify(origem),
                    data_hora: "",
                    operacao: "ALTERAÇÃO",
                  };

                  $http.post(urlServidor.urlServidorChatAdmin + "/ncv/logs-ncv", $scope.log_pgto );

                  $scope._gravar = true;
        
                });
              
             
                myFunctions.showAlert("Pagamento alterado com sucesso!");

              }  

              setTimeout(function() {
                
                $scope.buscaPagamentos()

              }, 500)
                
            };

    
            $scope.saveLiberacao = function () {
              $scope.ajustes_liberacao = $scope.liberacao;

              var ncv_origem = {
                ncv: _ncv,
                estadias: _estadias,
                valor_estadia: _valorEstadia,
                valor_guincho: _valorGuincho,
                desconto: _desconto,
                acrescimo: _acrescimo,
                total: _total,
              };

              var novos_dados = {
                ncv: $scope.liberacao.ncv,
                estadias: $scope.liberacao.estadias,
                valor_estadia: $scope.liberacao.valor_estadia,
                valor_guincho: $scope.liberacao.valor_guincho,
                desconto: $scope.liberacao.desconto,
                acrescimo: $scope.liberacao.acrescimo,
                total: $scope.liberacao.total,
              };

              $http
                .post(
                  urlServidor.urlServidorChatAdmin + "/ncv/altera-liberacao",
                  $scope.ajustes_liberacao
                )
                .then(function (response) {
                  if (response.data.code) {
                    myFunctions.showAlert("Erro na gravação!");
                  } else {
                    var alterados = isEquivalent(ncv_origem, novos_dados);

                    var json = Object.assign({}, alterados);
                    if (JSON.stringify(json) !== "{}") {
                      var origem_json = Object.assign({}, ncv_origem);
                      //ncv,tipo,id_usuario,alteracoes,anterior,data_hora,operacao
                      $scope.logs = {
                        ncv: _ncv,
                        tipo: "LIBERAÇÃO",
                        id_usuario: localStorage.getItem("id_usuario"),
                        alteracoes: JSON.stringify(json),
                        anterior: JSON.stringify(origem_json),
                        data_hora: "",
                        operacao: "ALTERAÇÃO",
                      };
                      _ncv = null;
                      _estadias = null;
                      _valorEstadia = null;
                      _valorGuincho = null;
                      _desconto = null;
                      _acrescimo = null;
                      _total = null;
                    }
                    $http
                      .post(
                        urlServidor.urlServidorChatAdmin + "/ncv/logs-ncv/",
                        $scope.logs
                      )
                      .then(function (response) {
                        if (response.data.code) {
                          $scope.erro = response;
                        }
                      });
                    $scope._gravar = true;
                    myFunctions.showAlert("Liberação gravada com sucesso!");
                  }
                });
            };
          },
          templateUrl: "ajustes-liberacao.html",
          scope: $scope,
          preserveScope: true,
          scopoAnterior: $scope,
        })
        .then(
          function (answer) {
            let a = answer;
          },
          function () {
            $scope.statusdialog = "You cancelled the dialog.";
          }
        );
    }




    $scope.incluiLiberado = incluiLiberado;

    function incluiLiberado(ncv) {

        var _ncv = ncv;
        var _action = 'incluir';
        $mdDialog.show({

            controller: function ($scope, $mdDialog, $mdToast) {

                $scope.dados_liberado = {
                    id: '',
                    nome: '',
                    cpf_cnpj: '',
                    endereco: '',
                    bairro: '',
                    cidade: '',
                    cep: '',
                    uf: '',
                    email: '',
                    telefone: '',
                    status: '1',
                    idempresa: localStorage.getItem('id_empresa')
                }

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.saveLiberado = function () {

                    if (_action == 'incluir') {

                        $http.post(urlServidor.urlServidorChatAdmin + '/clientes/cadastrar', $scope.dados_liberado).then(function (response) {
                            return $scope.dados_clientes;
                        });
                    }

                    if (_action == 'alterar') {

                        $http.post(urlServidor.urlServidorChatAdmin + '/clientes/alterar', $scope.dados_liberado).then(function (response) {
                            return $scope.dados_clientes;
                        });
                    }

                };

                $scope.saveAndClose = function () {
                    $scope.saveLiberado();
                    $rootScope._dadosliberado = $scope.dados_liberado;

//                    myFunctions.showAlert('As alterações foram salvas!');
                    $mdDialog.hide();
                };

                $scope.estados = estados.uf;
                $scope.buscaCep = (cep) => {

                    if (cep.length == 9) {

                        viaCep.get(cep).then(function (response) {

                            $scope.dados_liberado.endereco = response.logradouro.toUpperCase();
                            $scope.dados_liberado.bairro = response.bairro.toUpperCase();
                            $scope.dados_liberado.cidade = response.localidade.toUpperCase();
                            $scope.dados_liberado.uf = response.uf.toUpperCase();
                        });
                    }

                }

                $scope.buscaCpfCnpj = (cpfCnpj) => {

                    if (cpfCnpj.length == 14 || cpfCnpj.length == 18) {

                        $http.get(urlServidor.urlServidorChatAdmin + '/clientes/busca-cpfcnpj', {params: {cpf_cnpj: cpfCnpj}}).then(function (response) {

                            if (response.data.length > 0) {
                                $scope.dados_liberado.cep = response.data[0].cep;
                                $scope.dados_liberado.nome = response.data[0].nome;
                                $scope.dados_liberado.endereco = response.data[0].endereco;
                                $scope.dados_liberado.bairro = response.data[0].bairro;
                                $scope.dados_liberado.cidade = response.data[0].cidade;
                                $scope.dados_liberado.uf = response.data[0].uf;
                                $scope.dados_liberado.email = response.data[0].email;
                                $scope.dados_liberado.telefone = response.data[0].telefone;
                                _action = 'alterar';
                            }
                            

                                nome_liberado =  $scope.dados_liberado.nome;
                                cidade_liberado= $scope.dados_liberado.cidade+'-'+$scope.dados_liberado.uf;
                                liberado_telefone = $scope.dados_liberado.telefone;

                        });
                    }

                }


            },
            templateUrl: 'cadastra-liberado.html',
            preserveScope: true
        }).then(function (answer) {
            $scope.$resolve.$scope.liberacao.cpf_liberado = $rootScope._dadosliberado.cpf_cnpj;
            $scope.$resolve.$scope.liberacao.liberado_para = $scope._dadosliberado.nome;
        }, function () {
            $scope.statusdialog = 'You cancelled the dialog.';
        });
    }
    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }
    function buscaLiberacao() {
        $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-liberacao', {params: {ncv: _id, tipo_veiculo: $scope.dados.tipo_veiculo, id_tarifa: $scope.dados.id_tarifa}}).then(function (response) {

            console.log(response)
            if ($scope.dados.status == 'APREENDIDO' || $scope.dados.status == '' || $scope.dados.status == null) {

                let dia = new Date();
                let total = 0;
                let desconto = 0;
                $scope.diasdesconto = 0;
                let tipo_desconto = '';
                tipo_desconto = 'NÃO TEVE';
                let acrescimo = 0;
                let _valor_guincho = 0;
                if (ncvFac.controlePatioFac.isEmpty(response.data)) {
                    response.data[0] = {}
                    response.data[0]._limite_km = 0;
                }
                if (response.data[0]._limite_km) {
                    acrescimo = ($scope.guincho.km_percorrido > response.data[0]._limite_km ? (($scope.guincho.km_percorrido - response.data[0]._limite_km) * response.data[0]._valor_km) : 0.00);
                }
                if (response.data[0]._guincho_dia == response.data[0]._guincho_noite) {
                    if ($scope.guincho.id_reboque == 0 && $scope.guincho.id_motorista == 0) {
                        _valor_guincho = 0;
                    } else {
                        _valor_guincho = response.data[0]._guincho_dia;
                    }
                    if (response.data[0]._estadias > 180) {
                        tipo_desconto = 'LEGISLAÇÃO';
                        $scope.diasdesconto = response.data[0]._estadias - 180;
                        total = (180 * response.data[0]._valorestadia) + response.data[0]._guincho_dia;
                        desconto = ((response.data[0]._estadias * response.data[0]._valorestadia) + _valor_guincho) - total;
                    } else {
                        total = ((response.data[0]._estadias + 1) * response.data[0]._valorestadia) + _valor_guincho
                    }
                } else {
                    if (response.data[0].hora_apreensao >= '06:00:00' && response.data[0].hora_apreensao <= '18:00:00') {

                        if ($scope.guincho.id_reboque == 0 && $scope.guincho.id_motorista == 0) {
                            _valor_guincho = 0;
                        } else {
                            _valor_guincho = response.data[0]._guincho_dia;
                        }


                        if (response.data[0]._estadias > 180) {
                            tipo_desconto = 'LEGISLAÇÃO';
                            $scope.diasdesconto = response.data[0]._estadias - 180;
                            total = (180 * response.data[0]._valorestadia) + _valor_guincho;
                            desconto = ((response.data[0]._estadias * response.data[0]._valorestadia) + _valor_guincho) - total;
                        } else {
                            total = ((response.data[0]._estadias + 1) * response.data[0]._valorestadia) + _valor_guincho
                        }

                    } else {

                        if ($scope.guincho.id_reboque == 0 && $scope.guincho.id_motorista == 0) {
                            _valor_guincho = 0;
                        } else {
                            _valor_guincho = response.data[0]._guincho_noite;
                        }

                        if (response.data[0]._estadias > 180) {
                            tipo_desconto = 'LEGISLAÇÃO';
                            $scope.diasdesconto = response.data[0]._estadias - 180;
                            total = (180 * response.data[0]._valorestadia) + response.data[0]._guincho_noite;
                            desconto = ((response.data[0]._estadias * response.data[0]._valorestadia) + _valor_guincho) - total;
                        } else {
                            total = ((response.data[0]._estadias + 1) * response.data[0]._valorestadia) + _valor_guincho
                        }

                    }
                }
                $scope.liberacao = {
                    ncv: _id,
                    data:(response.data[0].data ? new Date(response.data[0].data.substr(3, 2) + '-' + response.data[0].data.substr(0, 2) + '-' + response.data[0].data.substr(6, 4)) : dia),
                    hora: dia.getHours() + ":" + dia.getMinutes(),
                    alvara: response.data[0].alvara,
                    tipo_alvara: response.data[0].tipo_alvara,
                    tipo_desconto: tipo_desconto,
                    estadias: response.data[0]._estadias + 1,
                    valor_estadia: response.data[0]._valorestadia,
                    valor_guincho: _valor_guincho,
                    desconto: desconto,
                    acrescimo: acrescimo,
                    total: parseFloat((total + acrescimo).toFixed(2)),
                    liberado_para: '',
                    faturado_para: '',
                    nota_fiscal: response.data[0].nota_fiscal,
                    cpf_liberado: response.data[0].cpf_liberado,
                    cpf_faturado: response.data[0].cpf_faturado,
                    liberado_por: localStorage.getItem('nome_usuario'),
                    pago: parseFloat((total + acrescimo).toFixed(2)) ,
                    forma_pagamento: response.data[0].forma_pagamento,
                    observacao: response.data[0].observacao,
                    tipoliberacao: $scope.dados.status
                };


            } else {
                $scope.liberacao = {
                    ncv: response.data[0].ncv,
                    data: (response.data[0].data ? new Date(response.data[0].data.substr(3, 2) + '-' + response.data[0].data.substr(0, 2) + '-' + response.data[0].data.substr(6, 4)) : null),
                    hora: response.data[0].hora,
                    alvara: response.data[0].alvara,
                    tipo_alvara: response.data[0].tipo_alvara,
                    tipo_desconto: response.data[0].tipo_desconto,
                    estadias: response.data[0].estadias,
                    valor_estadia: response.data[0].valor_estadia,
                    valor_guincho: response.data[0].valor_guincho,
                    desconto: response.data[0].desconto,
                    acrescimo: response.data[0].acrescimo,
                    total: response.data[0].total,
                    nota_fiscal: response.data[0].nota_fiscal,
                    liberado_para: response.data[0].liberado_para,
                    faturado_para: response.data[0].faturado_para,
                    cpf_liberado: response.data[0].cpf_liberado,
                    cpf_faturado: response.data[0].cpf_faturado,
                    liberado_por: response.data[0].liberado_por,
                    pago: response.data[0].pago,
                    forma_pagamento: response.data[0].forma_pagamento,
                    observacao: response.data[0].observacao,
                    tipoliberacao: $scope.dados.status
                };
            }
            data_liberacao = response.data[0].data;
            $scope.$applyAsync();
        });
    }

    $scope.atualizaValorLiberacao = function () {

        let vtotal = (($scope.liberacao.estadias * $scope.liberacao.valor_estadia) + $scope.liberacao.valor_guincho);
//		vtotal =  $scope.liberacao.total.replace(',','.') ;
        let acrescimo = $scope.liberacao.acrescimo;
//		acrescimo =  $scope.liberacao.acrescimo.replace(',','.') ;		
        let desconto = $scope.liberacao.desconto;
//		desconto =  $scope.liberacao.desconto.replace(',','.') ;		

        $scope.liberacao.total = ((vtotal + acrescimo)) - desconto;
        $scope.liberacao.total = parseFloat($scope.liberacao.total.toFixed(2));
    };
    function buscaMotivos() {

        return new Promise(function (resolve, reject) {

            $http.get(urlServidor.urlServidorChatAdmin + '/ncv/lista-motivoapreensao').then(function (response) {

                let dados = response.data;
                dados.sort(function (a, b) {
                    if (a.descricao < b.descricao) {
                        return 1;
                    }
                    if (a.descricao > b.descricao) {
                        return -1;
                    }
                    return 0;
                });
                resolve(dados);
            });
        });
    };
 
    $scope.termoEntrega = async function () {
        var doc = new jsPDF({orientation: "portrait"});
        var totalPagesExp = '{total_pages_count_string}'
        var totalEstadia =  $scope.liberacao.estadias * $scope.liberacao.valor_estadia;
     
        doc.setFont("times", "normal");
        doc.setFontSize('8');
        doc.setLineWidth(0.5);
        doc.line(10, 8, 200, 8);
        


     
        async function ImageTranspiler (path){
            var image = new Image();
            image.src= path
            return image
        }

        const Sistema = window.location.host;

        var Desc=""
        var Número= ""
        var Site= ""
        let logo ;
        var Acrescimo = "";
      
        if(Sistema === "rs.reidospatios.com.br"){
             Desc = 'R.S. FERNADES'
             Número = '(11) 95477-8486'
             Site = 'Estr. Variante, 10100 - Cidade Miguel Badra, Suzano - SP, 08690-760'
             logo = await ImageTranspiler("/assets/images/RsLogo.JPEG")
             Acrescimo = "KM Rodado/Rebocado: "

        }else if(Sistema === "sistema.reidospatios.com.br" ){
            var Desc = 'Grupo Carvalho Gestão de Pátios'
            var Número = '0800 970 9752 | (14) 99904.9598'
            var Site = 'www.grupocarvalhogestao.com.br'
            logo = await ImageTranspiler("/assets/images/logo.jpg")
            Acrescimo = "Acrescimo: "


        } else {
            var Desc = 'Grupo Carvalho Gestão de Pátios'
            var Número = '0800 970 9752 | (14) 99904.9598'
            var Site = 'www.grupocarvalhogestao.com.br'
            logo = await ImageTranspiler("/assets/images/logo.jpg")
            Acrescimo = "Acrescimo: "
        }



         doc.addImage(logo, 'JPEG',  90, 10, 40, 15)
        
        
        doc.text(Desc,110 , 32, null, null, "center");
        doc.text(Número, 110, 36, null, null, "center");
        doc.text(Site, 110, 39, null, null, "center");
        doc.setLineWidth(0.5);
        doc.line(10, 44, 200, 44);
        
        doc.setFontSize('12');
        doc.setFont("helvetica", "bold");
        doc.text("TERMO DE RESTITUIÇÃO", 84,60);
     

        doc.autoTable({
            head: [
                ['1.IDENTIFICAÇÃO DO VEÍCULO', '', '']
               
              ],
              columnStyles: { 
                0: {  fontStyle:'normal', fontSize:8.5},
                1: { halign: 'left', fontStyle:'normal', fontSize:8.5},
                2: { halign: 'center', fontStyle:'normal', fontSize:8.5},
        },
              
            // Cells in first column centered and green
            margin: { top: 80, bottom:0, right: 20, left:20 },
            body: [
                
              ['NCV: '+ _id, 'Tipo Veículo: '+veiculo, 'Modelo: '+$scope.dados.marca_modelo,],
              [ 'Placa: '+ $scope.dados.placa, 'Cor: '+ $scope.dados.cor, 'Ano: '+ $scope.dados.ano]
            
            ],
          })
         
          doc.autoTable({

            headStyles: {fontSize:10},
            margin:{ top:0, bottom:0, right: 20, left:20},
            head: [
                ['2.DADOS APREENSÃO','', '']
               
              ],
          
            // Cells in first column centered and green
            columnStyles: { 
                    0: { halign: 'left', fontStyle:'normal', fontSize:8.5},
                    1: { fontStyle:'normal', fontSize:8.5}
                    
            },
      // Cells in first column centered and green
            body: [
              ['Data: '+ $scope.dados.data_apreensao, 'Local: '+ $scope.apreensao.local],
             
             
            ],
          })
          
          doc.autoTable({
            headStyles: { fontSize: 10 },
            headStyles:{0:{fontSize:9} },
            margin:{ top:0, bottom: 0, right: 20, left:20},
            head: [
                ['3.LIBERADO PARA:','']
               
              ],
          
            // Cells in first column centered and green
           columnStyles: { 0: { fontSize:8.5},
           1: { fontSize:8.5}
           
         },
           
            body: [
              ['Nome: '+  $scope.liberacao.liberado_para, 'CPF/CNPJ: '+ $scope.liberacao.cpf_liberado],
              
             
            ],
          })
         
          doc.autoTable({
            headStyles: { fontSize: 10 },
            margin:{ top:0, bottom:0, right: 20, left:20},
            head: [
                ['4.FATURADO PARA:','']
               
              ],
              columnStyles: { 0: {  fontSize:8.5},
              1: {  fontSize:8.5}
              },
                       // Cells in first column centered and green
            
            body: [
              ['Nome: '+ $scope.liberacao.faturado_para, 'CPF/CNPJ: '+ $scope.liberacao.cpf_faturado],
             
            ],
          })
          doc.autoTable({
            headStyles: { fontSize: 10 },
            margin:{ top:0, right: 20, left:20},

            head: [
                ['5.SERVIÇOS:', '', '']
               
              ],
              columnStyles: { 
                0: {  fontStyle:'normal', fontSize:8.5},
                1: { halign: 'center', fontStyle:'normal', fontSize:8.5},
                2: { halign: 'center', fontStyle:'normal', fontSize:8.5}
             },
            // Cells in first column centered and green
           
            body: [
                
              ['Pátio: '+ $scope.dados.nome,  'Apreensão: '+ $scope.dados.data_apreensao,'Liberação: '+data_liberacao],
              ['Diárias: '+ $scope.liberacao.estadias+' - '+'Valor Diária: '+'R$'+ $scope.liberacao.valor_estadia, 'Total Diárias: '+'R$'+ totalEstadia, 'Guincho: '+'R$'+ $scope.liberacao.valor_guincho],
              
             ['Desconto: '+'R$'+ $scope.liberacao.desconto,Acrescimo+'R$'+ $scope.liberacao.acrescimo, 'Total: '+'R$'+ $scope.liberacao.total]
            ],
          })
         
        
        doc.setLineWidth(0.5)
        doc.line(20, 248, 90, 248);
        doc.setFont("times", "normal");
        doc.setFontSize('10');
        doc.text("Retirado por:",165, 230, null, null, "right");
        doc.text("Concedido por:", 50, 230, null, null, "left");
        doc.setLineWidth(0.5)
        doc.line(120, 248, 190, 248);
        
      

// jsPDF 1.4+ uses getWidth, <1.4 uses .width
             
                var strTotal = ($scope.endereco_patio.endereco +'. '+ $scope.endereco_patio.bairro+' - '+$scope.endereco_patio.cidade+'/'+ $scope.endereco_patio.estado+ ' | '+ 'CONTATO: '+$scope.endereco_patio.telefone+'.' );

              
             
               
                doc.setFontSize(8);// optional
                doc.text('                         '+strTotal, 10, doc.internal.pageSize.height - 10);
                
                

            
                if (typeof doc.putTotalPages === 'function') {
                        doc.putTotalPages(totalPagesExp)
                 }

                doc.save('Termo de Entrega.pdf');
                doc.autoPrint();
    }


    $scope.$watch("dados.id", function (nv, ov) {
        if (!ncvFac.controlePatioFac.isEmpty(nv)) {
            buscaLiberacao();
        }
    });



    $scope.gerarLaudo = async function () {
      let mywindow = window.open("", "PRINT", "height=650,width=900");

      mywindow.document.write(`
          <!DOCTYPE html>
          <html lang="en">
            <head>
              <meta charset="UTF-8" />
              <meta
                name="viewport"
                content="width=device-width, initial-scale=1.0"
              />
              <title>Laudo</title>
            </head>
            <body>
              <style>
                body {
                  max-width: 800px;
                  margin: 0 auto;
                  font-family: "Segoe UI", Tahoma, Geneva, Verdana, sans-serif;
                }
                header {
                  display: flex;
                  flex-direction: column;
                  justify-content: center;
                  width: 100%;
                  align-items: center;
                  gap: 10px;
                  margin-top: 20px;
                }
                .divider {
                  width: 100%;
                  height: 5px;
                  background-color: black;
                }
                main {
                  display: flex;
                  flex-direction: column;
                  justify-content: center;
                  width: 100%;
                  align-items: center;
                  gap: 10px;
                  margin-top: 20px;
                }
                h1 {
                  text-align: left;
                }
  
                .identificacao .titleBox {
                  background-color: #3583b5;
                  width: 800px;
                  color: white;
                  display: flex;
                  align-items: center;
                  margin-top: 10px;
                  height: 40px;
                }
              </style>
  
              <header>
                <div class="divider"></div>
                <img
                  src="/assets/images/logo.jpg"
                  alt=""
                  width="300"
                  height="100"
                />
                <span>Grupo Carvalho Gestão de Pátios</span>
                <span>0800 970 9752 | (14) 99904.9598</span>
                <a href="https://grupocarvalhogestao.com.br/"
                  >https://grupocarvalhogestao.com.br/</a
                >
                <div class="divider"></div>
              </header>
              <main>
                <h1>Laudo</h1>
                <div class="identificacao">
                <div class="titleBox" style="background-color: rgb(54, 132, 181); color: white" >
                <span style="font-size: 20px; margin-left: 10px">
                      1 . DADOS
                    </span>
                  </div>
                  <div
                    style="
              display: flex;
              justify-content: space-between;
              margin-top: 20px;
            "
                  >
                    <span style="font-size: 20px">  NCV: ${_id}</span>
                    <span style="font-size: 20px">  Tipo: ${veiculo}</span>
                    <span style="font-size: 20px"
                      >Modelo: ${$scope.dados.marca_modelo}</span
                    >
                  </div>
                  <div
                    class="secondLine"
                    style="
              display: flex;
              justify-content: space-between;
              margin-top: 20px;
            "
                  >
                    <span style="font-size: 20px">
                      Placa: ${$scope.dados.placa}</span
                    >
                    <span style="font-size: 20px">Cor: ${
                      $scope.dados.cor
                    }</span>
                    <span style="font-size: 20px"> Ano: ${
                      $scope.dados.ano
                    }</span>
                  </div>
                </div>
  
                <div class="fotosSlider"  
                style="
                display: flex;
                justify-content: space-between;
                margin-top: 40px;
                gap: 10px;
                flex-flow: row wrap;
                margin-bottom: 40px;


              "
                >
                  ${$scope.listaImagens.map(
                    ({ imagem }) =>
                      `<img src="${$scope.urlFotos}${imagem}"  width="200" height="200"><img/>`
                  )}
                </div>
                <div
                class="ass"
                style="
                  display: flex;
                  justify-content: space-between;
                  margin-top: 20px;
                  width: 100%;
                "
              >
                <div
                  class="conferido"
                  style="display: flex; flex-direction: column; align-items: center"
                >
                  <span> Conferido por: </span>
                  <div
                    style="
                      width: 240px;
                      height: 2px;
                      background-color: black;
                      margin-top: 50px;
                    "
                  ></div>
                </div>
        
                <div
                  class="vistoriado"
                  style="display: flex; flex-direction: column; align-items: center"
                >
                  <span>Vistoriado por:</span>
                  <div
                    style="
                      width: 240px;
                      height: 2px;
                      background-color: black;
                      margin-top: 50px;
                    "
                  ></div>
                </div>
              </div>
              </main>
            </body>
          </html>
        `);

      mywindow.document.close(); // necessary for IE >= 10
      mywindow.focus(); // necessary for IE >= 10*/
      mywindow.print();
      mywindow.close();

      return true;
    };

    $scope.adicionarExtra = function () {

        $mdDialog.show({
            controller: function ($scope, $mdDialog, myFunctions, $http, urlServidor) {
                   
                $http.get(urlServidor.urlServidorChatAdmin + '/ncv/busca-extrasguincho', { params: { ncv: $scope.dados.id } }).then(function (response) {

                    let dadosRecebidos = response.data
                    
                    if (dadosRecebidos.length > 0){

                        $scope.extrasGuincho = dadosRecebidos
                    }else{

                        $scope.extrasGuincho = [{ }]
                    }
                
                })

  
                $scope.adicionaExtra = function() {
                    
                    $scope.extrasGuincho.length + 1
                    
                    $scope.extrasGuincho.push({ })
            
                }
            

                $scope.removeExtra = function() {
                
                    var lastItem = $scope.extrasGuincho.length - 1
                
                    if($scope.extrasGuincho.length >= 2){
                    
                        $scope.extrasGuincho.splice(lastItem)

                    }
                }



                $scope.gravarExtrasGuincho = function(){

                    let dados_enviar = $scope.extrasGuincho.map((x) => {
        
                        return {
                            ncv: $scope.dados.id,
                            valor: x.valor,
                            observacao: x.observacao                    
                        }
        
                    })

                               
                    $http.post(urlServidor.urlServidorChatAdmin+'/ncv/cadastrar-extrasguincho', dados_enviar).then( function(response){
                    
                        if (response.data.code){
                                
                            myFunctions.showAlert('Erro na gravação!')
                    
                        }else{
                                               
                            myFunctions.showAlert( 'Dados gravados com sucesso!' );
                                                                                    
                        }
                    })
                    
                    $scope.cancelar()

                }

    
            },
          
            templateUrl: './views/ncv/guincho-valores-extras.html',
            scope: $scope,
            preserveScope: true,
            scopoAnterior: $scope
        }).then(function (answer) {
            let a = answer;
        
    
           
        }, function () {
            $scope.statusdialog = 'You cancelled the dialog.'
        })
    
        $scope.cancelar = function() {
            $mdDialog.cancel();
        }
        
    
    }

  
});