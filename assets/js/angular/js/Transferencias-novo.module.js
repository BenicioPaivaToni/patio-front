angular.module('app').controller('TransferenciasNovoCtrl', function ($scope, $http, urlServidor, myFunctions) {




    $scope.dados = {
        PatioOrigem: '',
        PatioDestino: '',
        ContaOrigem: '',
        ContaDestino: '',
        DataMovimento: new Date(),
        valor: ''
    }


    const buscaPatios = async () => {
        const response = await $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar')
        const { data } = response
        $scope.patios = data
    }


    const buscaContas = async () => {
        const response = await $http.put(urlServidor.urlServidorChatAdmin + '/contas/listar')
        $scope.contas = response.data;
    }




    buscaPatios()
    buscaContas()



    $scope.gravar = async function () {

        try {
          


                await $http.post(`${urlServidor.urlServidorChatAdmin}/transferencias/cadastrar`, $scope.dados)
                myFunctions.showAlert('Cadastro executado com sucesso!');
                $scope.isDisabled = true;


            




        } catch (error) {
            console.error(error);
            myFunctions.showAlert('Erro na gravação!')
        }
    }




})


