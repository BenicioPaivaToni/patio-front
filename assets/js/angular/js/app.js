let moneyMask = angular.module("money-mask", []);
moneyMask.factory("moneyMaskService", ["$filter", function ($filter) {
    var _otherCharacters = /[^0-9]/g;
    var _zeros = /^0+/;
    var _fromStringToMoney = function (string) {
        return $filter("currency")((_fromStringToNumber(string) / 100).toFixed(2));
    };
    var _fromStringToNumber = function (string) {
        if (!string)
            return 0;
        return string.replace(_otherCharacters, '').replace(_zeros, '') || 0;
    };
    return {
        fromStringToMoney: _fromStringToMoney
    };
}
]);


moneyMask.directive('moneyMask', ['$timeout', 'moneyMaskService', function ($timeout, moneyMaskService) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            var execute = function () {
                scope.$apply(function () {
                    ctrl.$setViewValue(moneyMaskService.fromStringToMoney(ctrl.$modelValue));
                    ctrl.$render();
                });
            };
            element.bind('keyup', function () {
                execute();
            });
            $timeout(function () {
                execute();
            }, 500);
        }
    };
}
]);


let app = angular.module('app', ['dx','ngMaterial', 'ngMessages', 'ngRoute', 'ngAria', 'ngAnimate', 'ngSanitize', 'mdDataTable', 'money-mask', 'ngMask', 'angular.viacep', 'ngFileUpload', 'angular-loading-bar', 'ui.utils.masks']);

app.config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = true;
}]);

const nameToken = 'jwtToken';

var dadosAws = {};

app.run(['$http', '$rootScope', '$location', function ($http, $rootScope, $location) {
    const token = localStorage.getItem(nameToken);
    $http.get(`${window.host}/autoridades/vercodigo`).then(function (response) {
        dadosAws = { ac: response.data.ac, sc: response.data.sc, regiao: response.data.regiao };
    });
    $http
        .post(`${window.host}/autenticacao/validar`, {})
        .catch(function (error) {
            if (error.status === 401) {
                localStorage.removeItem(nameToken);
                window.location = `${window.base_url}/login.html`;
            }
        });
    /*
     $rootScope.$on('$locationChangeStart', function (event, next, current) {
     let rotas_usuario = localStorage.getItem('rotas_usuario').split(',') ;
     if( $location.path() !== "" && $location.path() !== "/painel" ){
     if( rotas_usuario.indexOf($location.path()) == -1){
     if(current.indexOf('painel') === -1){
     window.location = `${window.base_url}/painel.html`;
     }
     }
     }
     });
     */
    /*
     $rootScope.$on("$locationChangeStart", function(event, next, current) {
     // handle route changes
     let pos = current.indexOf('painel-chamados') ;
     if( pos > 0 ){
     socket.close()
     }
     });
     */
}]);
app.config(AppConfig);

app.factory("SampleInterceptor", SampleInterceptor);

SampleInterceptor.$inject = ["$q"];

app.filter("groupBy",["$parse","$filter",function($parse,$filter){
    return function(array,groupByField){
      var result	= [];
              var prev_item = null;
              var groupKey = false;
              var filteredData = $filter('orderBy')(array,groupByField);
              for(var i=0;i<filteredData.length;i++){
                groupKey = false;
                if(prev_item !== null){
                  if(prev_item[groupByField] !== filteredData[i][groupByField]){
                    groupKey = true;
                  }
                } else {
                  groupKey = true;
                }
                if(groupKey){
                  filteredData[i]['group_by_key'] =true;
                } else {
                  filteredData[i]['group_by_key'] =false;
                }
                result.push(filteredData[i]);
                prev_item = filteredData[i];
              }
              return result;
    }
  }])

function SampleInterceptor($q) {

    return {

        request: function (config) {
            const ignoreUrl = ['/autenticacao/login'];
            if (config.url.indexOf('.html') === -1) {
                const url = new URL(config.url);
                const baseUrl = new URL(window.host);
                if (!ignoreUrl.includes(url.pathname) && url.host === baseUrl.host) {
                    const token = localStorage.getItem(nameToken);
                    config.headers['Authorization'] = `Bearer ${token}`;
                }
            }


            return config;
        },
        responseError: function (error) {
            console.log(error);
            if (error.status === 401) {

               alert('Tempo de acesso expirou, faça um novo login para continuar!')


                window.open('/login.html')

                window.close()
            }

            if (error.status === 403) {
                //faz alguma coisa.
            }


            return $q.reject(error);
        },


    };
}

var fileReader = function ($q, $log) {

    var onLoad = function (reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.resolve(reader.result);
            });
        };
    };
    var onError = function (reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.reject(reader.result);
            });
        };
    };
    var onProgress = function (reader, scope) {
        return function (event) {
            scope.$broadcast("fileProgress",
                {
                    total: event.total,
                    loaded: event.loaded
                });
        };
    };
    var getReader = function (deferred, scope) {
        var reader = new FileReader();
        reader.onload = onLoad(reader, deferred, scope);
        reader.onerror = onError(reader, deferred, scope);
        reader.onprogress = onProgress(reader, scope);
        return reader;
    };
    var readAsDataURL = function (file, scope) {
        var deferred = $q.defer();
        var reader = getReader(deferred, scope);
        reader.readAsDataURL(file);
        return deferred.promise;
    };
    return {
        readAsDataUrl: readAsDataURL
    };
};
app.factory("fileReader",
    ["$q", "$log", fileReader]);

AppConfig.$inject = ["$httpProvider"];
function AppConfig($httpProvider) {

    //Push the interceptor in the chain
    $httpProvider.interceptors.push("SampleInterceptor");
}


app.config(function ($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = function (date) {
        try {
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            return day + '/' + (monthIndex + 1) + '/' + year;
        } catch (e) {
            return null;
        }
    };
});

app.constant('urlServidor', {


 //    urlServidorChatAdmin: "https://api-reidospatios-dev.herokuapp.com"


  //urlServidorChatAdmin: window.location.hostname.indexOf("localhost") !== -1 ? "http://localhost:5500" : ""

  urlServidorChatAdmin: window.location.hostname.indexOf("localhost") !== -1 ? "http://localhost:1337" : ""

// urlServidorChatAdmin:"https://serverapi.reidospatios.com.br:1337"

 //  urlServidorChatAdmin: "https://api.rs.reidospatios.com.br"

//       urlServidorChatAdmin: "https://api.soscatanduva.reidospatios.com.br"

//    urlServidorChatAdmin: "https://api.vaptvuptleiloes.com.br"

    //urlServidorChatAdmin: window.location.hostname.indexOf("localhost") !== -1 ? "http://localhost:1337" : "https://api.peninha.reidospatios.com.br"

});

// RS SUZANO

app.constant('urlImagens', {

   // urlImagensChecklist: "https://s3-us-east-2.amazonaws.com/fotos.rs.checklist/",
 //   urlImagemNf: "https://s3-us-east-2.amazonaws.com/documentos.rs.contas-pagar/",
 //   urlImagensConferente: "https://s3.amazonaws.com/fotos.rs.conferente/",
 //   urlImagensFotos: "https://s3-us-east-2.amazonaws.com/fotos.rs.reidospatios/",
 //   urlImagensDocumentos: "https://s3-us-east-2.amazonaws.com/documentos.rs.reidospatios/", // Motoristas, Empresa Reboque, Reboques e NCV Documentos.
//    urlDocumentosMotoristas: "https://s3-us-east-2.amazonaws.com/documentos.rs-reidospatios/",
//    urlDocumentosReboque: "https://s3-us-east-2.amazonaws.com/documentos.rs-reidospatios/",
//    urlDocumentosEmpresaReb: "https://s3-us-east-2.amazonaws.com/documentos.rs-reidospatios/",
//    urlFotos: "https://s3-us-east-2.amazonaws.com/fotos.rs.funcionarios",
//    urlDocumentosFuncionarios: "https://s3-us-east-2.amazonaws.com/documentos.rs.funcionarios",
//    urlAnexosOrcamentos: "https://s3-us-east-2.amazonaws.com/anexos.rs.orcamentos/"


/*

    urlImagensChecklist: "https://s3-us-east-2.amazonaws.com/fotos.rs.checklist/",
    urlImagemNf: "https://s3-us-east-2.amazonaws.com/documentos.rs.contas-pagar/",
    urlImagensConferente: "https://s3.amazonaws.com/fotos.rs.conferente/",
    urlImagensFotos: "https://s3-us-east-2.amazonaws.com/fotos.rs.reidospatios/",
    urlImagensDocumentos: "https://s3-us-east-2.amazonaws.com/documentos.rs.reidospatios/", // Motoristas, Empresa Reboque, Reboques e NCV Documentos.
    urlDocumentosMotoristas: "https://s3-us-east-2.amazonaws.com/documentos.rs-reidospatios/",
    urlDocumentosReboque: "https://s3-us-east-2.amazonaws.com/documentos.rs-reidospatios/",
    urlDocumentosEmpresaReb: "https://s3-us-east-2.amazonaws.com/documentos.rs-reidospatios/",
    urlFotos: "https://s3-us-east-2.amazonaws.com/fotos.rs.funcionarios",
    urlDocumentosFuncionarios: "https://s3-us-east-2.amazonaws.com/documentos.rs.funcionarios",
    urlAnexosOrcamentos: "https://s3-us-east-2.amazonaws.com/anexos.rs.orcamentos/"
*/

//  CARVALHO



    urlImagensChecklist: "https://s3-sa-east-1.amazonaws.com/fotos.checklist/",
    urlImagemNf: "https://s3.sa-east-1.amazonaws.com/documentos.contas-pagar/",
    urlImagensConferente: "https://s3.amazonaws.com/fotos.conferente/",
    urlImagensFotos: "https://s3-sa-east-1.amazonaws.com/fotos.reidospatios/",
    urlImagensDocumentos: "https://s3-sa-east-1.amazonaws.com/documentos.reidospatios/", // Motoristas, Empresa Reboque, Reboques e NCV Documentos.
    urlDocumentosMotoristas: "https://s3.sa-east-1.amazonaws.com/documentos-reidospatios/",
    urlDocumentosReboque: "https://s3.sa-east-1.amazonaws.com/documentos-reidospatios/",
    urlDocumentosEmpresaReb: "https://s3.sa-east-1.amazonaws.com/documentos-reidospatios/",
    urlFotos: "https://s3-sa-east-1.amazonaws.com/fotos.funcionarios",
    urlDocumentosFuncionarios: "https://s3-sa-east-1.amazonaws.com/documentos.funcionarios",
    urlAnexosOrcamentos: "https://s3-sa-east-1.amazonaws.com/anexos.orcamentos/",
    urlBuscaDetran:"https://s3-sa-east-1.amazonaws.com/pdf.detran/",
    urlArs: "https://anexos-orcamentos.s3.sa-east-1.amazonaws.com/",
    urlDocumentosJuridico: "https://s3-sa-east-1.amazonaws.com/documentos.juridico/",
    //urlDocumentosVeiculos: "https://s3.amazonaws.com/documentos.veiculos/",
    urlDocumentosVeiculos: "https://s3-sa-east-1.amazonaws.com/documentos.reidospatios/"

/*

        urlImagensChecklist: "https://s3-us-east-2.amazonaws.com/fotos.peninha.checklist/",
        urlImagemNf: "https://s3-us-east-2.amazonaws.com/documentos.peninha.contas-pagar/",
        urlImagensConferente: "https://s3.us-east-2.amazonaws.com/fotos.peninha.conferente/",
        urlImagensFotos: "https://s3.us-east-2.amazonaws.com/fotos.peninha.reidospatios/",
        urlImagensDocumentos: "https://s3.us-east-2.amazonaws.com/documentos.peninha.reidospatios/", // Motoristas, Empresa Reboque, Reboques e NCV Documentos.
        urlDocumentosMotoristas: "https://s3.us-east-2.amazonaws.com/documentos.peninha-reidospatios/",
        urlDocumentosReboque: "https://s3.us-east-2.amazonaws.com/documentos.peninha-reidospatios/",
        urlDocumentosEmpresaReb: "https://s3.us-east-2.amazonaws.com/documentos.peninha-reidospatios/",
        urlFotos: "https://s3.us-east-2.amazonaws.com/fotos.peninha.funcionarios",
        urlDocumentosFuncionarios: "https://s3.us-east-2.amazonaws.com/documentos.peninha.funcionarios"



        urlImagensChecklist: "https://s3-us-east-2.amazonaws.com/fotos.soscatanduva.checklist/",
        urlImagemNf: "https://s3-us-east-2.amazonaws.com/documentos.soscatanduva.contas-pagar/",
        urlImagensConferente: "https://s3.us-east-2.amazonaws.com/fotos.soscatanduva.conferente/",
        urlImagensFotos: "https://s3.us-east-2.amazonaws.com/fotos.soscatanduva.reidospatios/",
        urlImagensDocumentos: "https://s3.us-east-2.amazonaws.com/documentos.soscatanduva.reidospatios/", // Motoristas, Empresa Reboque, Reboques e NCV Documentos.
        urlDocumentosMotoristas: "https://s3.us-east-2.amazonaws.com/documentos.soscatanduva-reidospatios/",
        urlDocumentosReboque: "https://s3.us-east-2.amazonaws.com/documentos.soscatanduva-reidospatios/",
        urlDocumentosEmpresaReb: "https://s3.us-east-2.amazonaws.com/documentos.soscatanduva-reidospatios/",
        urlFotos: "https://s3.us-east-2.amazonaws.com/fotos.soscatanduva.funcionarios",
        urlDocumentosFuncionarios: "https://s3.us-east-2.amazonaws.com/documentos.soscatanduva.funcionarios"

*/   

        //VAPTVUPT LEILOES II
/*
        urlImagensChecklist: "https://s3.us-east-2.amazonaws.com/fotos.checklist.vaptvuptleiloes/",
        urlImagemNf: "https://s3.us-east-2.amazonaws.com/documentos.contas-pagar.vaptvuptleiloes/",
        urlImagensConferente: "https://s3.us-east-2.amazonaws.com/fotos.conferente.vaptvuptleiloes/",
        urlImagensFotos: "https://s3.us-east-2.amazonaws.com/fotos.vaptvuptleiloes/",
        urlImagensDocumentos: "https://s3.us-east-2.amazonaws.com/documentos.reidospatios.vaptvuptleiloes/", // Motoristas, Empresa Reboque, Reboques e NCV Documentos.
        urlDocumentosMotoristas: "https://s3.us-east-2.amazonaws.com/documentos.vaptvuptleiloes/",
        urlDocumentosReboque: "https://s3.us-east-2.amazonaws.com/documentos.vaptvuptleiloes/",
        urlDocumentosEmpresaReb: "https://s3.us-east-2.amazonaws.com/documentos.vaptvuptleiloes/",
        urlFotos: "https://s3-us-east-2.amazonaws.com/fotos.funcionarios.vaptvuptleiloes/",
        urlDocumentosFuncionarios: "https://s3-us-east-2.amazonaws.com/documentos.funcionarios.vaptvuptleiloes/",
        urlAnexosOrcamentos: "https://s3-us-east-2.amazonaws.com/anexos.orcamentos.vaptvuptleiloes/",
        urlBuscaDetran:"https://s3-us-east-2.amazonaws.com/pdf.detran.vaptvuptleiloes/",
        urlArs: "https://s3.us-east-2.amazonaws.com/anexos-orcamentos/"
*/

});

app.constant('buckets', {

    //  CARVALHO

    motoristas: "documentos-reidospatios",
    reboques: "documentos-reidospatios",
    empresas_reboques: "documentos-reidospatios",
    patios: "documentos.patios",
    fotos_ncv: "fotos.reidospatios",
    fotos: "fotos.reidospatios",
    ars:'anexos-orcamentos',
    documentos_ncv: "documentos.reidospatios",
    documentos_pagar: "documentos.contas-pagar",
    fotos_funcionarios: "fotos.funcionarios",
    documentos_funcionarios: "documentos.funcionarios",
    anexos_orcamentos: "anexos.orcamentos",
    fotos_conferente:"fotos.conferente",
    busca_detran:"benicio-paiva",
    documentos_juridico:"documentos.reidospatios",
    //documentos_veiculos:"documentos.veiculos",
    documentos_veiculos:"documentos.reidospatios"



    //  CATANDUVA
/*  
           motoristas: "documentos.soscatanduva-reidospatios",
           reboques: "documentos.soscatanduva-reidospatios",
           empresas_reboques: "documentos.soscatanduva-reidospatios",
           patios: "documentos.soscatanduva.patios",
           fotos_ncv: "fotos.soscatanduva.reidospatios",
           documentos_ncv: "documentos.soscatanduva.reidospatios",
           documentos_pagar: "documentos.soscatanduva.contas-pagar",
           fotos_funcionarios: "fotos.soscatanduva.funcionarios",
           documentos_funcionarios: "documentos.soscatanduva.funcionarios",
           fotos_conferente:"fotos.soscatanduva.conferente",
*/
    //  PENINHA

    /*

            motoristas: "documentos.peninha-reidospatios",
            reboques: "documentos.peninha-reidospatios",
            empresas_reboques: "documentos.peninha-reidospatios",
            patios: "documentos.peninha.patios",
            fotos_ncv: "fotos.peninha.reidospatios",
            documentos_ncv: "documentos.peninha.reidospatios",
            documentos_pagar: "documentos.peninha.contas-pagar",
            fotos_funcionarios: "fotos.peninha.funcionarios",
            documentos_funcionarios: "documentos.peninha.funcionarios"

    */

    //  RS SUZANO

    /*
            motoristas: "documentos.rs-reidospatios",
            reboques: "documentos.rs-reidospatios",
            empresas_reboques: "documentos.rs-reidospatios",
            patios: "documentos.rs.patios",
            fotos_ncv: "fotos.rs.reidospatios",
            documentos_ncv: "documentos.rs.reidospatios",
            documentos_pagar: "documentos.rs.contas-pagar",
            fotos_funcionarios: "fotos.rs.funcionarios",
            documentos_funcionarios: "documentos.rs.funcionarios",
*/

 //  VAPTVUPT

/*
 motoristas: "documentos.vaptvuptleiloes",
 reboques: "documentos.vaptvuptleiloes",
 empresas_reboques: "documentos.vaptvuptleiloes",
 patios: "documentos.vaptvuptleiloes",
 fotos_ncv: "fotos.vaptvuptleiloes",
 fotos: "fotos.vaptvuptleiloes",
 ars:'anexos.vaptvuptleiloes',
 documentos_ncv: "documentos.vaptvuptleiloes",
 documentos_pagar: "documentos.contas-pagar.vaptvuptleiloes",
 fotos_funcionarios: "fotos.funcionarios.vaptvuptleiloes",
 documentos_funcionarios: "documentos.funcionarios.vaptvuptleiloes",
 anexos_orcamentos: "anexos.orcamentos.vaptvuptleiloes",
 fotos_conferente:"fotos.conferente.vaptvuptleiloes",
 busca_detran:"benicio-paiva"
*/

});


// PDF
var pdfjsLib = window['pdfjs-dist/build/pdf'];
//	pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';
// PDF

//	var socket = io('http://localhost:1337');

app.service('myFunctions', function ($mdDialog, $http, $sce) {

    this.showAlert = function (ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Atenção!')
                .textContent(ev)
                .ariaLabel('')
                .ok('Ok!')
                .targetEvent(ev)
                .multiple(true)
        );
    };
    this.showAlertCustom = function (ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Atenção!')
                .htmlContent(ev)
                .ariaLabel('')
                .ok('Ok!')
                .targetEvent(ev)
                .multiple(true)
        );
    };
    this.showConfirma = function (ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
            .title(ev)
            .textContent(ev)
            .ariaLabel('Lucky day')
            .targetEvent(ev)
            .ok('Baixar')
            .cancel('Fechar');
        $mdDialog.show(confirm).then(function () {
            $scope.status = 'You decided to get rid of your debt.';
        }, function () {
            $scope.status = 'You decided to keep your debt.';
        });
    };


    this.showConfirmaCancelar = function (ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
            .title(ev)
            .textContent('')
            .ariaLabel('Lucky day')
            .targetEvent(ev)
            .ok('Sim')
            .cancel('Fechar');
        $mdDialog.show(confirm).then(function () {
            let ok = '';
        }, function () {
            let ok = '';
        });
    };


    this.showConfirmParcial = function (ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
            .title('Baixa de Contas a Pagar?')
            .textContent('Valor informado menor que o valor em aberto, o lançamento será ? ')
            .ariaLabel('Lucky day')
            .targetEvent(ev)
            .ok('Pagamento Parcial')
            .cancel('Diferença como Desconto');
        $mdDialog.show(confirm).then(function () {
            $scope.status = 'You decided to get rid of your debt.';
        }, function () {
            $scope.status = 'You decided to keep your debt.';
        });
    };
    this.carregaEnderaco = function (cep) {

        viaCep.get(cep).then(function (response) {
            $scope.address = response
        });
    }

    this.numberToReal = function (numero) {
        var numero = numero.toFixed(2).split('.');
        numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
        return numero.join(',');
    };
});
app.service('S3UploadService', function ($q, $http, urlServidor) {




    //    var bucket = new AWS.S3({ params: { Bucket: 'fotos.reidospatios', maxRetries: 10 }, httpOptions: { timeout: 360000 } });

    this.Progress = 0;
    this.Upload = function (file, _bucket, _nomeimagem,regiao) {


        AWS.config.update({ accessKeyId: dadosAws.ac, secretAccessKey: dadosAws.sc });
        if(regiao){
            AWS.config.region = dadosAws.regiao
        } else {
            AWS.config.region = dadosAws.regiao
        }

        var bucket = new AWS.S3();

        var deferred = $q.defer();
        var params = { Bucket: _bucket, Key: _nomeimagem, ContentType: file.type, Body: file };
        var options = {
            // Part Size of 10mb
            partSize: 10 * 1024 * 1024,
            queueSize: 1,
                // Give the owner of the bucket full control
            ACL: 'public-read'
        };
        var uploader = bucket.upload(params, options, function (err, data) {
            if (err) {
                deferred.reject(err);
            }
            deferred.resolve();
        });
        uploader.on('httpUploadProgress', function (event) {
            deferred.notify(event);
        });
        return deferred.promise;
    };
});
app.constant('estados', {

    uf: [
        { "nome": "Acre", "sigla": "AC" },
        { "nome": "Alagoas", "sigla": "AL" },
        { "nome": "Amapá", "sigla": "AP" },
        { "nome": "Amazonas", "sigla": "AM" },
        { "nome": "Bahia", "sigla": "BA" },
        { "nome": "Ceará", "sigla": "CE" },
        { "nome": "Distrito Federal", "sigla": "DF" },
        { "nome": "Espírito Santo", "sigla": "ES" },
        { "nome": "Goiás", "sigla": "GO" },
        { "nome": "Maranhão", "sigla": "MA" },
        { "nome": "Mato Grosso", "sigla": "MT" },
        { "nome": "Mato Grosso do Sul", "sigla": "MS" },
        { "nome": "Minas Gerais", "sigla": "MG" },
        { "nome": "Pará", "sigla": "PA" },
        { "nome": "Paraíba", "sigla": "PB" },
        { "nome": "Paraná", "sigla": "PR" },
        { "nome": "Pernambuco", "sigla": "PE" },
        { "nome": "Piauí", "sigla": "PI" },
        { "nome": "Rio de Janeiro", "sigla": "RJ" },
        { "nome": "Rio Grande do Norte", "sigla": "RN" },
        { "nome": "Rio Grande do Sul", "sigla": "RS" },
        { "nome": "Rondônia", "sigla": "RO" },
        { "nome": "Roraima", "sigla": "RR" },
        { "nome": "Santa Catarina", "sigla": "SC" },
        { "nome": "São Paulo", "sigla": "SP" },
        { "nome": "Sergipe", "sigla": "SE" },
        { "nome": "Tocantins", "sigla": "TO" }

    ]

});
app.directive('cpfCnpj', cpfCnpj);
function cpfCnpj() {
    return {
        link: link,
        require: 'ngModel'
    };
    function link(scope, element, attrs, ngModelController) {
        attrs.$set('maxlength', 18);
        scope.$watch(attrs['ngModel'], applyMask);
        function applyMask(event) {
            var value = element.val().replace(/\D/g, "");
            if (value.length <= 11) {
                value = value.replace(/(\d{3})(\d)/, "$1.$2");
                value = value.replace(/(\d{3})(\d)/, "$1.$2");
                value = value.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
            } else {
                value = value.replace(/^(\d{2})(\d)/, "$1.$2")
                value = value.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
                value = value.replace(/\.(\d{3})(\d)/, ".$1/$2");
                value = value.replace(/(\d{4})(\d)/, "$1-$2");
            }
            element.val(value);
            if ('asNumber' in attrs) {
                ngModelController.$setViewValue(
                    isNaN(parseInt(value.replace(/\D/g, ""), 10))
                        ? undefined
                        : parseInt(value.replace(/\D/g, ""), 10));
            } else {
                ngModelController.$setViewValue(value);
            }
        }
    }
    ; //link
}
; //cpfCnpj

/*
 app.factory('socket', ['$rootScope', 'urlServidor', function($rootScope,urlServidor) {

 //	var socket = io(urlServidor.urlServidorChatAdmin);

 var io_server = io.connect(urlServidor.urlServidorChatAdmin, {transports:['websocket'], upgrade: false  } );  //, {'force new connection': true}
 var socket = io('/painelchamados')

 //	const socket = io();
 //	const adminSocket = io('/admin');
 // a single connection will be established


 return {
 on: function(eventName, callback){
 socket.on(eventName, callback);
 },
 emit: function(eventName, data) {
 socket.emit(eventName, data);
 },
 close: function() {
 socket.close();
 }

 };

 }]);

 */

app.config(function ($routeProvider) {
    $routeProvider.when("/painel", {
        templateUrl: 'painel.html',
        controller: 'painelCtrl'
    });
    $routeProvider.when("/autoridades", {
        templateUrl: 'autoridades.html',
        controller: 'autoridadesCtrl'
    });
    $routeProvider.when("/autoridades-novo", {
        templateUrl: 'autoridades-novo.html',
        controller: 'autoridadesNovoCtrl',
    });
    $routeProvider.when("/autoridades-editar", {
        templateUrl: 'autoridades-editar.html',
        controller: 'autoridadesEditarCtrl',
    });
    $routeProvider.when("/clientes", {
        templateUrl: 'clientes.html',
        controller: 'clientesCtrl'
    });
    $routeProvider.when("/clientes-novo", {
        templateUrl: 'clientes-novo.html',
        controller: 'clientesNovoCtrl',
    });
    $routeProvider.when("/clientes-editar", {
        templateUrl: 'clientes-editar.html',
        controller: 'clientesEditarCtrl',
    });
    $routeProvider.when("/categorias", {
        templateUrl: 'categorias.html',
        controller: 'categoriasCtrl'
    });
    $routeProvider.when("/categorias-novo", {
        templateUrl: 'categorias-novo.html',
        controller: 'categoriasNovoCtrl'
    });
    $routeProvider.when("/categorias-editar", {
        templateUrl: 'categorias-editar.html',
        controller: 'categoriasEditarCtrl'
    });
    $routeProvider.when("/contas", {
        templateUrl: 'contas.html',
        controller: 'contasCtrl'
    });
    $routeProvider.when("/contas-novo", {
        templateUrl: 'contas-novo.html',
        controller: 'contasNovoCtrl'
    });
    $routeProvider.when("/contas-editar", {
        templateUrl: 'contas-editar.html',
        controller: 'contasEditarCtrl'
    });

    // Classificação Leilão

    $routeProvider.when("/classificacao-leilao", {
        templateUrl: './views/classificacao-leilao/index.html',
        controller: 'classificacaoLeilaoCtrl'
    });
    $routeProvider.when("/classificacao-leilao-editar", {
        templateUrl: './views/classificacao-leilao/editar.html',
        controller: 'classificacaoLeilaoEditarCtrl'
    });

    $routeProvider.when("/classificacao-leilao-novo", {
        templateUrl: './views/classificacao-leilao/novo.html',
        controller: 'classificacaoLeilaoNovoCtrl'
    });

    //Routes Financeiro Tranferencias
    $routeProvider.when("/Conferente-incompletos", {
        templateUrl: 'conferente-incompletos.html',
        controller: 'conferentesIncompletos'
    });

    //Termos Credenciamento Pendencias
    $routeProvider.when("/termo-pendencias", {
        templateUrl: 'termo-pendencias.html',
        controller: 'termoPendenciasCtrl'
    });

    //Routes Financeiro Tranferencias
    $routeProvider.when("/Transferencias", {
        templateUrl: 'Transferencias.html',
        controller: 'TransferenciasCtrl'
    });
    $routeProvider.when("/Transferencias-novo", {
        templateUrl: 'Transferencias-novo.html',
        controller: 'TransferenciasNovoCtrl'
    });
    $routeProvider.when("/Transferencias-editar", {
        templateUrl: 'Transferencias-editar.html',
        controller: 'TransferenciasEditarCtrl'
    });



    // Routes Cosula detran

    $routeProvider.when("/Consulta-Detran", {
        templateUrl: 'consultaDetran.html',
        controller: 'consultaDetranCtrl'
    });
    $routeProvider.when("/Consulta-Denatran", {
        templateUrl: 'consultaDenatran.html',
        controller: 'consultaDenatranCtrl'
    });
    $routeProvider.when("/Consulta-Detran-leilao", {
        templateUrl: 'consultaDetranLeilao.html',
        controller: 'consultaDetranLeilaoCtrl'
    });


    //Routes Requisição Financeiro1

    $routeProvider.when("/Lançamentos", {
        templateUrl: './views/Compras/Requisicao/requisicao.html',
        controller: 'RequisicaoCtrl'
    });
    $routeProvider.when("/Lançamentos-novo", {
        templateUrl: './views/Compras/Requisicao/requisicao-novo.html',
        controller: 'RequisicaoNovoCtrl'
    });




    //Routes Orcamento Financeiro

    $routeProvider.when("/orcamento", {
        templateUrl: './views/Compras/Orcamento/orcamento.html',
        controller: 'orcamentoCtrl'
    });






    //Routes Grupos Pátios
    $routeProvider.when("/grupos-patios", {
        templateUrl: './views/GruposPátios/gruposPatios.html',
        controller: 'gruposPatiosCtrl'
    });
    $routeProvider.when("/grupos-patios-novo", {
        templateUrl: './views/GruposPátios/gruposPatios-novo.html',
        controller: 'gruposPatios-NovoCtrl'
    });
    $routeProvider.when("/grupos-patios-editar", {
        templateUrl: './views/GruposPátios/gruposPatios-editar.html',
        controller: 'gruposPatios-EditarCtrl'
    });

    //Relatorio de NCV incompleto Routes

    $routeProvider.when("/relatorio-ncv-incompleto", {
        templateUrl: 'relatorio-ncv-incompleto.html',
        controller: 'ncvIncompletoCtrl'
    });




    $routeProvider.when("/caixa", {
        templateUrl: 'caixa.html',
        controller: 'caixaCtrl'
    });
    $routeProvider.when("/caixa-novo", {
        templateUrl: 'caixa-novo.html',
        controller: 'caixaNovoCtrl'
    });
    $routeProvider.when("/caixa-editar", {
        templateUrl: 'caixa-editar.html',
        controller: 'caixaEditarCtrl'
    });
    $routeProvider.when("/caixa-cancelar", {
        templateUrl: 'caixa-cancelar.html',
        controller: 'caixaCancelarCtrl'
    });

    $routeProvider.when("/chamados", {
        templateUrl: 'chamados.html',
        controller: 'chamadosCtrl'
    });
    $routeProvider.when("/chamados-editar", {
        templateUrl: 'chamados-editar.html',
        controller: 'chamadosEditarCtrl'
    });

    $routeProvider.when("/dados-pessoais", {
        templateUrl: 'dados_pessoais.html',
        controller: 'dadosPessoaisCtrl',
    });
    $routeProvider.when("/documentos-validade", {
        templateUrl: 'documentos-validade.html',
        controller: 'documentosValidadeCtrl',
    });
    $routeProvider.when("/documentos-vencidos", {
        templateUrl: 'documentos-vencidos.html',
        controller: 'documentosVencidosCtrl',
    });

    $routeProvider.when("/veiculos", {
        templateUrl: 'veiculos.html',
        controller: 'veiculosCtrl'
    });
    $routeProvider.when("/veiculos-novo", {
        templateUrl: 'veiculos-novo.html',
        controller: 'veiculosNovoCtrl',
    });
    $routeProvider.when("/veiculos-editar", {
        templateUrl: 'veiculos-editar.html',
        controller: 'veiculosEditarCtrl',
    });

    $routeProvider.when("/controle-trafego", {
        templateUrl: 'controle-trafego.html',
        controller: 'controleTrafegoCtrl',
    });

    $routeProvider.when("/comercial-apreensoes", {
        templateUrl: 'comercial-apreensoes.html',
        controller: 'comercialApreensoesCtrl',
    });

    $routeProvider.when("/empresas-reboques", {
        templateUrl: 'empresas-reboques.html',
        controller: 'empresasReboquesCtrl'
    });
    $routeProvider.when("/empresas-reboques-novo", {
        templateUrl: 'empresas-reboques-novo.html',
        controller: 'empresasReboquesNovoCtrl',
    });
    $routeProvider.when("/empresas-reboques-editar", {
        templateUrl: 'empresas-reboques-editar.html',
        controller: 'empresasReboquesEditarCtrl',
    });

    $routeProvider.when("/empresas", {
        templateUrl: 'empresas.html',
        controller: 'empresasCtrl'
    });
    $routeProvider.when("/empresas-novo", {
        templateUrl: 'empresas-novo.html',
        controller: 'empresasNovoCtrl',
    });
    $routeProvider.when("/empresas-editar", {
        templateUrl: 'empresas-editar.html',
        controller: 'empresasEditarCtrl',
    });

    $routeProvider.when("/fornecedores", {
        templateUrl: 'fornecedores.html',
        controller: 'fornecedoresCtrl'
    });
    $routeProvider.when("/fornecedores-novo", {
        templateUrl: 'fornecedores-novo.html',
        controller: 'fornecedoresNovoCtrl',
    });
    $routeProvider.when("/fornecedores-editar", {
        templateUrl: 'fornecedores-editar.html',
        controller: 'fornecedoresEditarCtrl',
    });
    $routeProvider.when("/fluxo-caixa", {
        templateUrl: 'fluxo-caixa.html',
        controller: 'fluxoCaixaCtrl'
    });
    $routeProvider.when("/funcionarios", {
        templateUrl: 'funcionarios.html',
        controller: 'funcionariosCtrl',
    });
    $routeProvider.when("/funcionarios-novo", {
        templateUrl: 'funcionarios-novo.html',
        controller: 'funcionariosNovoCtrl',
    });
    $routeProvider.when("/funcionarios-editar", {
        templateUrl: 'funcionarios-editar.html',
        controller: 'funcionariosEditarCtrl',
    });


    $routeProvider.when("/grupos", {
        templateUrl: 'grupos.html',
        controller: 'gruposCtrl'
    });
    $routeProvider.when("/grupos-novo", {
        templateUrl: 'grupos-novo.html',
        controller: 'gruposNovoCtrl'
    });
    $routeProvider.when("/grupos-editar", {
        templateUrl: 'grupos-editar.html',
        controller: 'gruposEditarCtrl'
    });


    $routeProvider.when("/motoristas", {
        templateUrl: 'motoristas.html',
        controller: 'motoristasCtrl'
    });
    $routeProvider.when("/motoristas-novo", {
        templateUrl: 'motoristas-novo.html',
        controller: 'motoristasNovoCtrl',
    });
    $routeProvider.when("/motoristas-editar", {
        templateUrl: 'motoristas-editar.html',
        controller: 'motoristasEditarCtrl',
    });
    $routeProvider.when("/motivo-apreensao", {
        templateUrl: 'motivo-apreensao.html',
        controller: 'motivoApreensaoCtrl'
    });
    $routeProvider.when("/motivo-apreensao-editar", {
        templateUrl: 'motivo-apreensao-editar.html',
        controller: 'motivoApreensaoEditarCtrl'
    });
    $routeProvider.when("/motivo-apreensao-novo", {
        templateUrl: 'motivo-apreensao-novo.html',
        controller: 'motivoApreensaoNovoCtrl'
    });


    $routeProvider.when("/ncv", {
        templateUrl: 'ncv.html',
        controller: 'ncvCtrl',
    });
    $routeProvider.when("/ncv-novo", {
        templateUrl: 'ncv-novo.html',
        controller: 'ncvNovoCtrl',
    });
    $routeProvider.when("/ncv-editar", {
        templateUrl: 'ncv-editar.html',
        controller: 'ncvEditarCtrl',
    });


    //

    $routeProvider.when("/comissao-motoristas", {
        templateUrl: 'comissao-motoristas.html',
        controller: 'comissaoMotoristasCtrl'
    });


    /* Juridico Routes */
    $routeProvider.when("/juridico", {
        templateUrl: 'juridico.html',
        controller: 'juridicoCtrl',
    });

    
    $routeProvider.when("/juridico-editar", {
        templateUrl: 'juridico-editar.html',
        controller: 'juridicoEditarCtrl',
    });


    /*Leilão Routes */

    $routeProvider.when("/Leilao", {
        templateUrl: 'Leilao.html',
        controller: 'LeilaoCtrl'

    });

    $routeProvider.when("/fotos-separacao-leilao", {
        templateUrl: 'fotos-separacao-leilao.html',
        controller: 'fotosSeparacaoLeilao'

    });


    $routeProvider.when("/Leilao-novo", {
        templateUrl: 'Leilao-Novo.html',
        controller: 'leilaoNovoCtrl'
    });

    $routeProvider.when("/Leilao-editar", {
        templateUrl: 'Leilao-Editar.html',
        controller: 'leilaoEditarCtrl'

    });





    /*Gerencial Financeiro Routes Routes */

    $routeProvider.when("/Gerencial", {
        templateUrl: './views/Financeiro_Gerencial/Gerencial.html',
        controller: 'GerencialCtrl'

    });

    $routeProvider.when("/Gerencial-novo", {
        templateUrl: './views/Financeiro_Gerencial/Gerencial_novo.html',
        controller: 'GerencialNovoCtrl'
    });

    $routeProvider.when("/Gerencial-editar", {
        templateUrl: './views/Financeiro_Gerencial/Gerencial_editar.html',
        controller: 'GerencialEditarCtrl'

    });

    /*Financeiro > Cadastro > Centro de Custos - Routes */
    $routeProvider.when("/centro-custos", {
        templateUrl: 'centro-custos.html',
        controller: 'centroCustosCtrl'
    });
    $routeProvider.when("/centro-custos-novo", {
        templateUrl: 'centro-custos-novo.html',
        controller: 'centroCustosNovoCtrl'
    });
    $routeProvider.when("/centro-custos-editar", {
        templateUrl: 'centro-custos-editar.html',
        controller: 'centroCustosEditarCtrl'
    });

    /*Financeiro > Lancamentos > Contabilizado - Route */
    $routeProvider.when("/contabilizado", {
        templateUrl: 'contabilizado.html',
        controller: 'contabilizadoCtrl'
    });

    $routeProvider.when("/patios", {
        templateUrl: 'patios.html',
        controller: 'patiosCtrl'
    });
    $routeProvider.when("/patios-novo", {
        templateUrl: 'patios_novo.html',
        controller: 'patiosNovoCtrl',
    });
    $routeProvider.when("/patios-editar", {
        templateUrl: 'patios_editar.html',
        controller: 'patiosEditarCtrl',
    });
    
    $routeProvider.when("/advogados", {
        templateUrl: 'advogados.html',
        controller: 'advogadosCtrl'
    });
    $routeProvider.when("/advogados-novo", {
        templateUrl: 'advogados-novo.html',
        controller: 'advogadosNovoCtrl',
    });
    $routeProvider.when("/advogados-editar", {
        templateUrl: 'advogados-editar.html',
        controller: 'advogadosEditarCtrl',
    });
    
    $routeProvider.when("/permissoes", {
        templateUrl: 'permissoes.html',
        controller: 'permissoesCtrl',
    });
    $routeProvider.when("/permissoes-editar", {
        templateUrl: 'permissoes-editar.html',
        controller: 'permissoesEditarCtrl'
    });
    $routeProvider.when("/pagar", {
        templateUrl: 'pagar.html',
        controller: 'pagarCtrl'
    });
    $routeProvider.when("/painel-chamados", {
        templateUrl: 'painel-chamados.html',
        controller: 'painelChamadosCtrl'
    });
    $routeProvider.when("/pagar-novo", {
        templateUrl: 'pagar-novo.html',
        controller: 'pagarNovoCtrl'
    });
    $routeProvider.when("/pagar-editar", {
        templateUrl: 'pagar-editar.html',
        controller: 'pagarEditarCtrl'
    });
    $routeProvider.when("/pagar-baixar", {
        templateUrl: 'pagar-baixar.html',
        controller: 'pagarBaixarCtrl'
    });
    $routeProvider.when("/pagar-cancelar", {
        templateUrl: 'pagar-cancelar.html',
        controller: 'pagarCancelarCtrl'
    });
    $routeProvider.when("/previsao", {
        templateUrl: 'previsao.html',
        controller: 'previsaoCtrl'
    });
    $routeProvider.when("/previsao-novo", {
        templateUrl: 'previsao-novo.html',
        controller: 'previsaoNovoCtrl'
    });
    $routeProvider.when("/previsao-editar", {
        templateUrl: 'previsao-editar.html',
        controller: 'previsaoEditarCtrl'
    });
    $routeProvider.when("/receber", {
        templateUrl: 'receber.html',
        controller: 'receberCtrl'
    });
    $routeProvider.when("/receber-novo", {
        templateUrl: 'receber-novo.html',
        controller: 'receberNovoCtrl'
    });
    $routeProvider.when("/receber-editar", {
        templateUrl: 'receber-editar.html',
        controller: 'receberEditarCtrl'
    });
    $routeProvider.when("/receber-baixar", {
        templateUrl: 'receber-baixar.html',
        controller: 'receberBaixarCtrl'
    });
    $routeProvider.when("/receber-cancelar", {
        templateUrl: 'receber-cancelar.html',
        controller: 'receberCancelarCtrl'
    });
    $routeProvider.when("/relatorio-pagos", {
        templateUrl: 'relatorio-pagos.html',
        controller: 'relatorioPagosCtrl'
    });
    $routeProvider.when("/reboques", {
        templateUrl: 'reboques.html',
        controller: 'reboquesCtrl'
    });
    $routeProvider.when("/separacao-leilao", {
        templateUrl: 'separacao-leilao.html',
        controller: 'separacaoLeilaoCtrl'
    });
    $routeProvider.when("/transferencia-veiculos", {
        templateUrl: 'transferencia-veiculos.html',
        controller: 'transferenciaVeiculosCtrl'
    });

    $routeProvider.when("/agendamento-veiculos", {
        templateUrl: 'agendamento-veiculos.html',
        controller: 'agendamentoVeiculosCtrl'
    });

    $routeProvider.when("/resumo-diario", {
        templateUrl: 'resumo-diario.html',
        controller: 'resumoDiarioCtrl'
    });

    $routeProvider.when("/relatorio-conferencia-recebimentos", {
        templateUrl: 'relatorio-conferencia-recebimentos.html',
        controller: 'relatorioConferenciaRecebimentosCtrl'
    });

    $routeProvider.when("/gerencial-apreensoes", {
        templateUrl: 'gerencial-apreensoes.html',
        controller: 'gerencialApreensoesCtrl'
    });

    $routeProvider.when("/gerencial-media-apreensoes", {
        templateUrl: 'gerencial-media-apreensoes.html',
        controller: 'gerencialMediaApreensoesCtrl'
    });

    $routeProvider.when("/gerencial-liberacoes", {
        templateUrl: 'gerencial-liberacoes.html',
        controller: 'gerencialLiberacoesCtrl'
    });

    $routeProvider.when("/dados-detran", {
        templateUrl: 'dados-detran.html',
        controller: 'dadosDetranCtrl'
    });

    $routeProvider.when("/dados-denatran", {
        templateUrl: 'dados-denatran.html',
        controller: 'dadosDenatranCtrl'
    });

    $routeProvider.when("/importar-veiculos", {
        templateUrl: 'importar-veiculos.html',
        controller: 'importarVeiculosCtrl'
    });

    $routeProvider.when("/reboques-editar", {
        templateUrl: 'reboques-editar.html',
        controller: 'reboquesEditarCtrl',
    });
    $routeProvider.when("/reboques-novo", {
        templateUrl: 'reboques-novo.html',
        controller: 'reboquesNovoCtrl',
    });
    $routeProvider.when("/relatorio-contabilizados", {
        templateUrl: 'relatorio-contabilizados.html',
        controller: 'relatorioContabilizadosCtrl'
    });
    $routeProvider.when("/relatorio-liberacoes", {
        templateUrl: 'relatorio-liberacoes.html',
        controller: 'relatorioLiberacoes'
    });

    $routeProvider.when("/relatorio-transferencias-veiculos", {
        templateUrl: 'relatorio-transferencias-veiculos.html',
        controller: 'relatorioTransferenciasVeiculosCtrl'
    });

    $routeProvider.when("/relatorio-veiculos-detalhes", {
        templateUrl: 'relatorio-veiculos-detalhes.html',
        controller: 'relatorioVeiculosDetalhesCtrl'
    });

    $routeProvider.when("/relatorio-fechamento-motoristas", {
        templateUrl: 'relatorio-fechamento-motoristas.html',
        controller: 'relatorioFechamentoMotoristasCtrl'
    });

    $routeProvider.when("/relatorio-liberacoes-recebimento", {
        templateUrl: 'liberacoes-forma-recebimento.html',
        controller: 'liberacoes-forma-pagtoCtrl'
    });

    $routeProvider.when("/relatorio-receitas", {
        templateUrl: 'relatorio-receitas.html',
        controller: 'relatorioReceitasCtrl'
    });
    $routeProvider.when("/relatorio-centrocusto", {
        templateUrl: 'relatorio-centrocusto.html',
        controller: 'relatorioCentroCusto'
    });
    $routeProvider.when("/relatorio-liberacoes-seccional", {
        templateUrl: 'relatorio-liberacoes-seccional.html',
        controller: 'relatorioLiberacoesSec'
    });
    $routeProvider.when("/relatorio-veiculos", {
        templateUrl: './views/relatorios/veiculos.html',
        controller: 'relatorioController'
    });
    $routeProvider.when("/relatorio-apreensoes", {
        templateUrl: 'relatorio-apreensoes.html',
        controller: 'relatorioApreensoes'
    });
    $routeProvider.when("/relatorio-estoque", {
        templateUrl: 'relatorio-estoque.html',
        controller: 'relatorioEstoque'
    });
    $routeProvider.when("/relatorio-estoque-real", {
        templateUrl: 'relatorio-estoque-real.html',
        controller: 'relatorioEstoqueRealCtrl'
    });
    $routeProvider.when("/pesquisa-geral", {
        templateUrl: 'pesquisa-geral.html',
        controller: 'pesquisaGeral'
    });
    $routeProvider.when("/relatorio-conferentes", {
        templateUrl: 'relatorio-conferentes.html',
        controller: 'relatorioConferentes'
    });
    $routeProvider.when("/tipos-recebimento", {
        templateUrl: 'tipos-recebimento.html',
        controller: 'tiposrecebimentoCtrl'
    });
    $routeProvider.when("/tipos-recebimento-novo", {
        templateUrl: 'tipos-recebimento-novo.html',
        controller: 'tiposrecebimentoNovoCtrl'
    });
    $routeProvider.when("/tipos-recebimento-editar", {
        templateUrl: 'tipos-recebimento-editar.html',
        controller: 'tiposrecebimentoEditarCtrl'
    });
    $routeProvider.when("/tarifas", {
        templateUrl: 'tarifas.html',
        controller: 'tarifasCtrl'
    });
    $routeProvider.when("/tarifas-novo", {
        templateUrl: 'tarifas-novo.html',
        controller: 'tarifasNovoCtrl'
    });
    $routeProvider.when("/tarifas-editar", {
        templateUrl: 'tarifas-editar.html',
        controller: 'tarifasEditarCtrl'
    });
    $routeProvider.when("/tipos-veiculo", {
        templateUrl: 'tipos-veiculo.html',
        controller: 'tiposVeiculoCtrl'
    });
    $routeProvider.when("/tiposveiculo-novo", {
        templateUrl: 'tipos-veiculo-novo.html',
        controller: 'tiposVeiculoNovoCtrl'
    });
    $routeProvider.when("/tiposveiculo-editar", {
        templateUrl: 'tipos-veiculo-editar.html',
        controller: 'tiposVeiculoEditarCtrl'
    });
    $routeProvider.when("/usuarios", {
        templateUrl: 'usuarios.html',
        controller: 'usuariosCtrl',
    });
    $routeProvider.when("/usuarios-editar", {
        templateUrl: 'usuarios-editar.html',
        controller: 'usuariosEditarCtrl',
    });
    $routeProvider.when("/usuarios-novo", {
        templateUrl: 'usuarios-novo.html',
        controller: 'usuariosNovoCtrl',
    });

    $routeProvider.otherwise({ redirectTo: "/painel" });
});
app.controller('gCarvalhoCtrl', function ($scope, $location, $mdToast, $mdDialog, $mdUtil,$http,urlServidor) {


    // aqui busca acessos aos menus
    let rotas_usuario = localStorage.getItem('rotas_usuario'); // .split(',')

    if (rotas_usuario) {

        if (rotas_usuario.indexOf('/autoridades') === -1)
            $scope._autoridades = true;
        if (rotas_usuario.indexOf('/clientes') === -1)
            $scope._clientes = true;
        if (rotas_usuario.indexOf('/chamados') === -1)
            $scope._chamados = true;
        if (rotas_usuario.indexOf('/documentos-validade') === -1)
            $scope._documentos_validade = true;
        if (rotas_usuario.indexOf('/empresas-reboques') === -1)
            $scope._empresas_reboques = true;
        if (rotas_usuario.indexOf('/empresas') === -1)
            $scope._empresas = true;
        if (rotas_usuario.indexOf('/fornecedores') === -1)
            $scope._fornecedores = true;
        if (rotas_usuario.indexOf('/grupos') === -1)
            $scope._grupos = true;
        if (rotas_usuario.indexOf('/motoristas') === -1)
            $scope._motoristas = true;
        if (rotas_usuario.indexOf('/motivo-apreensao') === -1)
            $scope._motivo_apreensao = true;
        if (rotas_usuario.indexOf('/classificacao-leilao') === -1)
            $scope.classificacao_leilao = true;
        if (rotas_usuario.indexOf('/separacao-leilao') === -1)
            $scope._separacao_leilao = true;

        if (rotas_usuario.indexOf('/ncv') === -1)
            $scope._ncv = true;

        if (rotas_usuario.indexOf('/conferencia-recebimentos') === -1)
            $scope._conferencia_recebimentos = true;

        if (rotas_usuario.indexOf('/gerencial') === -1)
            $scope._gerencial = true;

         if (rotas_usuario.indexOf('/ncv-incompletos') === -1)
            $scope._ncv_incompletos = true;

        if (rotas_usuario.indexOf('/comissao-motorista') === -1)
            $scope._comissao_motorista = true;

        if (rotas_usuario.indexOf('/fechamento-motoristas') === -1)
            $scope._fechamento_motoristas = false;

        if (rotas_usuario.indexOf('/compras') === -1)
            $scope._compras = true;

        if (rotas_usuario.indexOf('/orcamento') === -1)
            $scope._orcamento = true;

        if (rotas_usuario.indexOf('/patios') === -1)
            $scope._patios = true;

        if (rotas_usuario.indexOf('/advogados') === -1)
            $scope._advogados = true;

        if (rotas_usuario.indexOf('/painel_chamados') === -1)
            $scope._painel_chamados = true;

        if (rotas_usuario.indexOf('/reboques') === -1)
            $scope._reboques = true;

        if (rotas_usuario.indexOf('/juridico') === -1)
            $scope._juridico = true;

        if (rotas_usuario.indexOf('/resumo-diario') === -1)
            $scope._resumo_diario = true;

        if (rotas_usuario.indexOf('/dados-detran') === -1)
            $scope._dados_detran = true;

        if (rotas_usuario.indexOf('/relatorio-gerencial-apreensoes') === -1)
            $scope._gerencial_apreensoes = true;

        if (rotas_usuario.indexOf('/relatorio-liberacoes-recebimento') === -1)
            $scope._liberacoes_forma_recebimento = true;

        if (rotas_usuario.indexOf('/leilao') === -1)
            $scope._leilao = true;

        if (rotas_usuario.indexOf('/transferencias') === -1)
            $scope._transferencias = true;

        if (rotas_usuario.indexOf('/transferencia-veiculos') === -1)
            $scope._transferencia_veiculos = true;

        if (rotas_usuario.indexOf('/agendamento-veiculos') === -1)
            $scope._agendamento_veiculos = true;

        if (rotas_usuario.indexOf('/detalhes-veiculos') === -1)
            $scope._veiculos_detalhes = true;

        if (rotas_usuario.indexOf('/importar-veiculos') === -1)
            $scope._importar_veiculos = false;

        if (rotas_usuario.indexOf('/grupos-patios') === -1)
            $scope._grupos_patios = true;

        if (rotas_usuario.indexOf('/tarifas') === -1)
            $scope._tarifas = true;
        if (rotas_usuario.indexOf('/tipos_veiculo') === -1)
            $scope._tipos_veiculo = true;
        if (rotas_usuario.indexOf('/usuarios') === -1)
            $scope._usuarios = true;
        if (rotas_usuario.indexOf('/pagar') === -1)
            $scope._pagar = true;
        if (rotas_usuario.indexOf('/receber') === -1)
            $scope._receber = true;
        if (rotas_usuario.indexOf('/caixa') === -1)
            $scope._caixa = true;
        if (rotas_usuario.indexOf('/contabilizado') === -1)
            $scope._contabilizado = true;
        if (rotas_usuario.indexOf('/contas') === -1)
            $scope._contas = true;
        if (rotas_usuario.indexOf('/fluxo_caixa') === -1)
            $scope._fluxo_caixa = true;
        if (rotas_usuario.indexOf('/tipos_recebimentos') === -1)
            $scope._tipos_recebimentos = true;
        if (rotas_usuario.indexOf('/categorias') === -1)
            $scope._categorias = true;
        if (rotas_usuario.indexOf('/centro-custos') === -1)
            $scope._centro_custos = true;
        if (rotas_usuario.indexOf('/relatorio-transferencias-periodo') === -1)
            $scope._relatorio_transferencias_veiculos = true;
        if (rotas_usuario.indexOf('/gerencial-media-apreensoes') === -1)
            $scope._gerencial_media_apreensoes = true;        
        if (rotas_usuario.indexOf('/gerencial-medias') === -1)
            $scope._gerencial_medias = true;
        if (rotas_usuario.indexOf('/relatorio_liberacoes_seccional') === -1)
            $scope._relatorio_liberacoes_seccional = true;
        if (rotas_usuario.indexOf('/veiculos') === -1)
            $scope._veiculos = true;
        if (rotas_usuario.indexOf('/cadastro-veiculos') === -1)
            $scope._cadastro_veiculos = true;
        if (rotas_usuario.indexOf('/controle-trafego') === -1)
            $scope._controle_trafego = true;
        if (rotas_usuario.indexOf('/comercial-apreensoes') === -1)
            $scope._comercial_apreensoes = true;
        if (rotas_usuario.indexOf('/relatorio_apreensoes') === -1)
            $scope._relatorio_apreensoes = true;
        if (rotas_usuario.indexOf('/relatorio_liberacoes') === -1)
            $scope._relatorio_liberacoes = true;
        if (rotas_usuario.indexOf('/relatorio_estoque') === -1)
            $scope._relatorio_estoque = true;
        if (rotas_usuario.indexOf('/estoque-real') === -1)
            $scope._relatorio_estoque_real = true;
        if (rotas_usuario.indexOf('/pesquisa_geral') === -1)
            $scope._pesquisa_geral = true;
        if (rotas_usuario.indexOf('/seccional') === -1)
            $scope._seccional = true;
        if (rotas_usuario.indexOf('/relatorio_conferentes') === -1)
            $scope._relatorio_conferentes = true;
        if (rotas_usuario.indexOf('/previsao_caixa') === -1)
            $scope._previsao_caixa = true;
        if (rotas_usuario.indexOf('/relatorio_receitas') === -1)
            $scope._relatorio_receitas = true;
        if (rotas_usuario.indexOf('/relatorio_centrocusto') === -1)
            $scope._relatorio_centrocusto = true;
        if (rotas_usuario.indexOf('/relatorio_contabilizados') === -1)
            $scope._relatorio_contabilizados = true;
        if (rotas_usuario.indexOf('/relatorio_pagos') === -1)
            $scope._relatorio_pagos = true;
        if (rotas_usuario.indexOf('/funcionarios') === -1)
            $scope._funcionarios = true;
        if (rotas_usuario.indexOf('/grupo-itens') === -1)
            $scope._grupo_itens = true;
        if (rotas_usuario.indexOf('/orcamento') === -1)
            $scope._orcamento = true;


    }

    $scope.isFirstAccess = localStorage.getItem("first_access");

    if ($location.path().indexOf('usuario-editar') !== -1) {
        $scope.routebreadcrumb = 'Meus Dados';
    } else if ($location.path().indexOf('dados-pessoais') !== -1) {
        $scope.routebreadcrumb = 'Dados Pessoais';
    } else {
        $scope.routebreadcrumb = 'DashBoard';
    }
    $scope.addBreadcrumb = function (bread) {
        $scope.routebreadcrumb = bread;
    };
    $scope.sairDoApp = function () {
        localStorage.removeItem(nameToken);

        window.location = `${window.base_url}/login.html`;

    }

    $scope.suporteTi = function () {
        window.open('https://grupocarvalhogestao.sabesim.com/ext/appform/20wu0ux5b1eecwj4vsty', '_blank');
    }


    //Open menu lateral
    $scope.sideNavMenu = true;
    $scope.openMenu = function () {
        if ($scope.sideNavMenu) {
            $scope.sideNavMenu = false;
        } else {
            $scope.sideNavMenu = true;
        }

        //$mdSidenav('left').toggle();
        //$timeout(function() { $mdSidenav('left').toggle(); });
    };
    //fim Open menu lateral

    //toast
    var last = {
        bottom: false,
        top: true,
        left: false,
        right: true
    };
    $scope.toastPosition = angular.extend({}, last);
    $scope.getToastPosition = function () {
        sanitizePosition();
        return Object.keys($scope.toastPosition)
            .filter(function (pos) {
                return $scope.toastPosition[pos];
            })
            .join(' ');
    };
    function sanitizePosition() {
        var current = $scope.toastPosition;
        if (current.bottom && last.top)
            current.top = false;
        if (current.top && last.bottom)
            current.bottom = false;
        if (current.right && last.left)
            current.left = false;
        if (current.left && last.right)
            current.right = false;
        last = angular.extend({}, current);
    }

    $scope.mostrarMsg = function (msg) {
        var pinTo = $scope.getToastPosition();
        $mdToast.show(
            $mdToast.simple()
                .textContent(msg)
                .position(pinTo)
                .hideDelay(5000)
        );
    };
    //fim toast

    //  menu do usuario
    $scope.openUserMenu = function ($mdOpenMenu, ev) {

        originatorEv = ev;
        $mdOpenMenu(ev);
    };
    //scrolltop
    $scope.scrollTop = scrollTop;
    var mainContentArea = document.querySelector("[role='main']");
    var scrollContentEl = mainContentArea.querySelector('md-content[md-scroll-y]');
    function scrollTop() {
        $mdUtil.animateScrollTo(scrollContentEl, 0, 200);
    }
    //fim scrolltop

    $scope.fixDate = function (date) {
        return new Date(date);
    };
    $scope.paginaAnterior = function () {
        window.history.back();
    };
    $scope.hideModal = function () {
        $mdDialog.hide();
    };
    $scope.stringToInt = function (string) {
        return parseInt(string);
    };
    $scope.stringToTime = function (data, hora) {

        var dataCompleta = new Date(data);
        var dia = dataCompleta.getDate();
        var mes = dataCompleta.getMonth() + 1;
        var ano = dataCompleta.getFullYear();
        var dataHora = ano + '-' + mes + '-' + dia + ' ' + hora;
        var _dataHora = new Date(dataHora);
        $scope._dataHora = _dataHora;
        return _dataHora;
    };
    $scope.alterarSenha = function () {
        $scope.alterar_senha = {

            senhaNova: '',
            senhaNova1: '',
            idUsuario: localStorage.getItem('id_usuario')
        };

        $mdDialog.show({

            controller: function ($scope, $mdDialog, urlServidor, $http, $mdToast, myFunctions) {

                $scope.isFirstAccess = localStorage.getItem("first_access");

                var idUsuario = localStorage.getItem('id_usuario');
                var login =  localStorage.getItem('nome_usuario');

                $scope.cancel = function () {
                    $mdDialog.cancel();
                };

                $scope.saveAndClose = function () {

                    if (($scope.alterar_senha.senhaNova != $scope.alterar_senha.senhaNova1)) {
                        myFunctions.showAlert('Senhas diferentes, a senha deve ser igual nos dois campos !');
                    } else if($scope.alterar_senha.senhaNova == null || $scope.alterar_senha.senhaNova == ''){
                        myFunctions.showAlert('Senha inválida, verifique se os dois campos foram preenchidos !');
                    } else {

                        $scope.dadosNovos = {
                            id: localStorage.getItem('id_usuario'),
                            senha: $scope.alterar_senha.senhaNova,
                            login: login
                        };


                        $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/alterar-senha', $scope.dadosNovos).then(function (response) {
                            console.log(response);
                            if (response.data.code) {
                                myFunctions.showAlert('Ocorreu um erro ao tentar alterar a senha!');
                            } else {
                                myFunctions.showAlert('Senha alterada com sucesso!');

                                //$rootScope._dadosliberado = $scope.dados_liberado;
                                $mdDialog.hide();
                                setTimeout(function () {
                                    localStorage.removeItem(nameToken);
                                    window.location = `${window.base_url}/login.html`;
                                }, 1500);

                            }

                        }).catch(function(error) {
                            if (error.status === 422){
                               myFunctions.showAlert(error.data.error);
                            }
                        })


                    }




                };
            },
            templateUrl: 'alterar-senha.html',

            preserveScope: true
        }).then(function (answer) {
            //$scope.$resolve.$scope.liberacao.cpf_liberado_para = $rootScope._dadosliberado.cpf_cnpj;
            //$scope.$resolve.$scope.liberacao.liberado_para = $scope._dadosliberado.nome;
        }, function () {
            $scope.statusdialog = 'Alteração Cancelada';
        });
    }



    // Animação do DashBoard



    $scope.HideCad = true;
    $scope.HiddeMenuCadastros = () => {

         $scope.HideCad === true
        ? ($scope.HideCad = false)
        : ($scope.HideCad = true);


        $scope.HideOperacao = true;

        $scope.HideCompras = true;

        $scope.HideLogistica = true;

        $scope.HideGerencial = true;

        $scope.HideFinanceiro = true;

        $scope.HideFinanceiroCadastros = true;

        $scope.HideFinanceiroLancamentos = true;

        $scope.HideFinanceiroRelatorios = true;

        $scope.HideRelatorios = true;


    };

    $scope.HideOperacao = true;
    $scope.HiddeMenuOperacao = () => {
      $scope.HideOperacao === true
        ? ($scope.HideOperacao = false)
        : ($scope.HideOperacao = true);

        $scope.HideCad = true;

        $scope.HideCompras = true;

        $scope.HideLogistica = true;

        $scope.HideGerencial = true;

        $scope.HideFinanceiro = true;

        $scope.HideFinanceiroCadastros = true;

        $scope.HideFinanceiroLancamentos = true;

        $scope.HideFinanceiroRelatorios = true;

        $scope.HideRelatorios = true;

    };

    $scope.HideCompras = true;
    $scope.HiddeMenuCompras = () => {
      $scope.HideCompras === true
        ? ($scope.HideCompras = false)
        : ($scope.HideCompras = true);

        $scope.HideCad = true;

        $scope.HideOperacao = true;

        $scope.HideLogistica = true;

        $scope.HideGerencial = true;

        $scope.HideFinanceiro = true;

        $scope.HideFinanceiroCadastros = true;

        $scope.HideFinanceiroLancamentos = true;

        $scope.HideFinanceiroRelatorios = true;

        $scope.HideRelatorios = true;
    };


    $scope.HideLogistica = true;
    $scope.HiddeMenuLogistica = () => {
      $scope.HideLogistica === true
        ? ($scope.HideLogistica = false)
        : ($scope.HideLogistica = true);

        $scope.HideCad = true;

        $scope.HideOperacao = true;

        $scope.HideFinanceiro = true;

        $scope.HideGerencial = true;

        $scope.HideFinanceiroCadastros = true;

        $scope.HideFinanceiroLancamentos = true;

        $scope.HideFinanceiroRelatorios = true;

        $scope.HideRelatorios = true;
    };


    $scope.HideGerencial = true;
    $scope.HideMenuGerencial = () => {
      $scope.HideGerencial === true
        ? ($scope.HideGerencial = false)
        : ($scope.HideGerencial = true);

        $scope.HideCad = true;

        $scope.HideOperacao = true;

        $scope.HideLogistica = true;

        $scope.HideFinanceiro = true;

        $scope.HideFinanceiroCadastros = true;

        $scope.HideFinanceiroLancamentos = true;

        $scope.HideFinanceiroRelatorios = true;

        $scope.HideRelatorios = true;
    };



    $scope.HideFinanceiro = true;
    $scope.HiddeMenuHideFinanceiro = () => {
      $scope.HideFinanceiro === true
        ? ($scope.HideFinanceiro = false)
        : ($scope.HideFinanceiro = true);
        $scope.HideCad = true;

        $scope.HideOperacao = true;

        $scope.HideCompras = true;

        $scope.HideGerencial = true;

        $scope.HideLogistica = true;

        $scope.HideRelatorios = true;
    };

    $scope.HideFinanceiroCadastros = true;
    $scope.HiddeMenuHideFinanceiroCadastros = () => {
      $scope.HideFinanceiroCadastros === true
        ? ($scope.HideFinanceiroCadastros = false)
        : ($scope.HideFinanceiroCadastros = true);

    };

    $scope.HideFinanceiroLancamentos = true;
    $scope.HiddeMenuHideFinanceiroLancamentos = () => {
      $scope.HideFinanceiroLancamentos === true
        ? ($scope.HideFinanceiroLancamentos = false)
        : ($scope.HideFinanceiroLancamentos = true);

    };

    $scope.HideFinanceiroRelatorios = true;
    $scope.HiddeMenuHideFinanceiroRelatorios = () => {
      $scope.HideFinanceiroRelatorios === true
        ? ($scope.HideFinanceiroRelatorios = false)
        : ($scope.HideFinanceiroRelatorios = true);
    };

    $scope.HideRelatorios = true;
    $scope.HiddeMenuHideRelatorios = () => {
      $scope.HideRelatorios === true
        ? ($scope.HideRelatorios = false)
        : ($scope.HideRelatorios = true);
        $scope.HideCad = true;

        $scope.HideOperacao = true;

        $scope.HideCompras = true;

        $scope.HideLogistica = true;

        $scope.HideGerencial = true;

        $scope.HideFinanceiro = true;

        $scope.HideFinanceiroCadastros = true;

        $scope.HideFinanceiroLancamentos = true;

        $scope.HideFinanceiroRelatorios = true;


    };


    // Mensagem de alerta para Pendencias em Termos de Credenciamento
    const alertaPendencias = localStorage.getItem('alerta_documentos')
    $scope.mensagemTermoPendencias = false

    if (alertaPendencias === "SIM") {
    const buscaPendencias = async () => {
        const response = await $http.get(urlServidor.urlServidorChatAdmin + '/empresas_reboques/busca-termo-pendencias')
        const {data} = response

        if (data.length > 0){
            $scope.mensagemTermoPendencias = true
        }else{
            $scope.mensagemTermoPendencias = false
        }

    } 
    buscaPendencias()
    }



    // Mensagem de Documentos Vencidos
    const alertaDocumento = localStorage.getItem('alerta_documentos')
    $scope.MessagaDocuments = false

    if (alertaDocumento === "SIM") {
    const getDocumentosValidade = async () => {
        const response = await $http.get(urlServidor.urlServidorChatAdmin + '/relatorios/documentos-validade')
        const {data} = response
        data != [] ? $scope.MessagaDocuments = true : $scope.MessagaDocuments = false

    } 
    getDocumentosValidade()
    }

    // Mensagem de Documentos Vencidos

    const getConferentes = async () => {
    const response = await $http.get(`${
        urlServidor.urlServidorChatAdmin
    }/relatorios/conferente-pendentes`, {
        params: {
        id_usuario: localStorage.getItem("id_usuario")
        }
    });
    return await response.data
    };


    getConferentes().then((data) => {
    data !== [] ? $scope.MessageConferente = true : $scope.MessageConferente = false
    const isFirstRender = localStorage.getItem("firstRender");
    const isFirstAccess = localStorage.getItem("first_access");
   
    if (isFirstRender == 'true') {
        $scope.showModal = true
        $scope.totalConferente = data.length
    }
    if (isFirstAccess  == 'true') {
        $scope.alterarSenha();
    }
    });

    $scope.conferirConferentesIncompletos = () => {
    window.open('/#/Conferente-incompletos')
    localStorage.setItem("firstRender", 'false');;
     $scope.showModal = false

    }
 $scope.closeModalConf = () => {
    localStorage.setItem("firstRender", 'false');;
     $scope.showModal = false
    }

});




app.controller('painelCtrl', function ($scope, $http, urlServidor,$mdDialog) {


    buscaClientes().then(function (result) {
        $scope.clientes = result
        localStorage.setItem('dadosClientes', JSON.stringify(result))

    })


    $scope.tipousuario = localStorage.getItem('tipo_usuario')
    console.log("Tipo usuario: ", $scope.tipousuario)

    $scope.idUsuario = localStorage.getItem('id_usuario')
    console.log("ID USUARIO: ", $scope.idUsuario)


    $scope.alturaTela = window.innerHeight
    $scope.larguraTela = window.innerWidth

    $scope.dados = {
        data: null,
        patio: null,
        id_grupo_patios: null
    }

    $scope.showOption = false

    $scope.patios = ''


    $scope.limpar = function (ev) {
        $scope.dados.data = new Date(new Date().setDate(new Date().getDate() -1 ))
        $scope.dados.patio = null
        $scope.dados.id_grupo_patios = null

      //  window.location.reload()
      }


    dadosPesquisa()

    buscaPatiosGrupo()

    $scope.buscaInfosGrafico = async () => {

        const selectInfo = $scope.hideFiltroPesquisar
        let selectInputData = null

        switch (selectInfo) {
            case false:
                selectInputData = buscaDadosGraficos()
                break

            case true:
               selectInputData = buscaDadosGraficosUsuario()
        }

        await selectInputData.then(function (result) {

            const resultArrayTmp = []

                for(const itemObject in result) {
                    for(const itemArray of result[itemObject][0]) {
                        const patioArrayIndex = resultArrayTmp.findIndex((x) => x.patio === itemArray.patio)

                        const objectTmp = {}

                        for(const resultItem in itemArray) {
                            const field = resultItem.indexOf(itemObject) === -1 && resultItem !== 'patio' ? `${itemObject}_${resultItem}` : resultItem
                            objectTmp[field] = itemArray[resultItem];
                        }

                        patioArrayIndex !== -1 ? resultArrayTmp[patioArrayIndex] = {...objectTmp, ...resultArrayTmp[patioArrayIndex]} : resultArrayTmp.push(objectTmp)

                    }
                }


                const listaDados = resultArrayTmp.map((index) => {
                return {
                nomePatio: index.patio,
                apreensoesDia: index.apreensoes_dia,
                apreensoesAcumulado: index.apreensoes_acumulado,
                liberacoesDia: index.liberacoes_dia,
                liberacoesAcumulado: index.liberacoes_acumulado,
                apreensoesMeta: index.apreensoes_acumulado_meta,
                liberacoesMeta: index.liberacoes_acumulado_meta,
                data: String($scope.dados.data.getDate()).padStart(2, "0"),
                };
            });

            $scope.listaDados = listaDados
            let totalApreensoes = 0
            let totalLiberacoes = 0
            let totalApreensoesAcumulado = 0
            let totalLiberacoesAcumulado = 0

            var dadosGraficoApreensoes = [['Patio', 'Acumulado', {role: 'annotation'}, 'Meta', {role: 'annotation'}]];
                for(const item of listaDados) {
                    let resultApreensoes = [item.nomePatio, item.apreensoesAcumulado, item.apreensoesAcumulado, item.apreensoesMeta, item.apreensoesMeta]

                    totalApreensoes += item.apreensoesDia || 0
                    totalApreensoesAcumulado += item.apreensoesAcumulado || 0

                    if(item.apreensoesDia && item.apreensoesAcumulado) {
                    dadosGraficoApreensoes.push(resultApreensoes)
                }
            }

            var dadosTotalGraficoApreensoes = [['APREENSÕES', 'TOTAL DIA', {role: 'annotation'}, 'ACUMULADO', {role: 'annotation'}]];

            var resultTotalApreensoes = ["APREENSÕES", totalApreensoes, totalApreensoes, totalApreensoesAcumulado, totalApreensoesAcumulado]
            dadosTotalGraficoApreensoes.push(resultTotalApreensoes)
            $scope.dadosTotalGraficoApreensoes = dadosTotalGraficoApreensoes
            $scope.dadosGraficoApreensoes = dadosGraficoApreensoes

            var dadosGraficoLiberacoes = [['Patio', 'Acumulado', {role: 'annotation'}, 'Meta', {role: 'annotation'}]];
                for(const item of listaDados) {
                    let resultLiberacoes = [item.nomePatio, item.liberacoesAcumulado, item.liberacoesAcumulado, item.liberacoesMeta, item.liberacoesMeta]

                    totalLiberacoes += item.liberacoesDia || 0
                    totalLiberacoesAcumulado += item.liberacoesAcumulado || 0

                    if(item.liberacoesDia && item.liberacoesAcumulado) {
                    dadosGraficoLiberacoes.push(resultLiberacoes)
                }
            }

            var dadosTotalGraficoLiberacoes = [['LIBERAÇÕES', 'TOTAL DIA', {role: 'annotation'}, 'ACUMULADO', {role: 'annotation'}]];

            var resultTotalLiberacoes = ["LIBERAÇÕES", totalLiberacoes, totalLiberacoes, totalLiberacoesAcumulado, totalLiberacoesAcumulado]
            dadosTotalGraficoLiberacoes.push(resultTotalLiberacoes)
            $scope.dadosTotalGraficoLiberacoes = dadosTotalGraficoLiberacoes
            $scope.dadosGraficoLiberacoes = dadosGraficoLiberacoes


            var dadosGraficoApreensoesAcumulado = [['Patio', 'Apreenssoes Acumulado']]
                for(const item of listaDados) {
                    let resultApreensoesAcumulado = [item.nomePatio, item.apreensoesAcumulado]
                    if(item.apreensoesAcumulado) {
                    dadosGraficoApreensoesAcumulado.push(resultApreensoesAcumulado)
                }
            }

            $scope.dadosGraficoApreensoesAcumulado = dadosGraficoApreensoesAcumulado

            var dadosGraficoLiberacoesAcumulado = [['Patio', 'Liberacoes Acumulado']]
                for(const item of listaDados) {
                    let resultLiberacoesAcumulado = [item.nomePatio, item.liberacoesAcumulado]
                    if(item.liberacoesAcumulado) {
                    dadosGraficoLiberacoesAcumulado.push(resultLiberacoesAcumulado)
                }
            }

            $scope.dadosGraficoLiberacoesAcumulado = dadosGraficoLiberacoesAcumulado

        })

        $scope.imagemGraficoApreensoes = null
        $scope.imagemGraficoLiberacoes = null
        $scope.imagemGraficoApreensoesAcumulado = null
        $scope.imagemGraficoLiberacoesAcumulado = null



        graficoApreensoes()
        graficoLiberacoes()
        graficoApreensoesAcumulado()
        graficoLiberacoesAcumulado()
        graficoTotalApreensoes()
        graficoTotalLiberacoes()
    };


    hideFiltroPesquisar()

    function graficoApreensoes(){

        if ($scope.dadosGraficoApreensoes.length === 1){

            const elementoGrafico = document.getElementById("painel_apreensoes")
                while (elementoGrafico.hasChildNodes()) {
                elementoGrafico.removeChild(elementoGrafico.lastChild)
            }

        }else{

        let alturaGrafico = null

        if ($scope.dadosGraficoApreensoes.length > 11){
            alturaGrafico = 1600
        }else{
            alturaGrafico = 800
        }

        google.charts.load('current', {'packages':['corechart']})
        google.setOnLoadCallback(drawChart)

        function drawChart() {
            var data = google.visualization.arrayToDataTable($scope.dadosGraficoApreensoes)

            var options = {
                title:
                    "APREENSÕES - " +
                    String($scope.dados.data.getDate()).padStart(2, "0") + " de " +
                    $scope.dados.data.toLocaleString("default", {month: "long"})
                    + " de " + $scope.dados.data.getFullYear(),
                subtitle: 'Grupo Carvalho',
                titleTextStyle: { color: "#000000", fontName: "arial", fontSize: 14, bold: true, italic: false},
                legend: { position: "bottom", maxLines: 1, alignment: "center", textStyle: {fontSize: 12}},
                animation: { duration: 3000, easing: "out", startup: true},
                width: 'auto',
                height: alturaGrafico,
                fontSize: 10,
                chartArea: { width: "80%", height: "90%" },
                seriesType: "bars",
                series: [{color: 'blue', visibleInLegend: true}, {color: 'green', visibleInLegend: true}],
                backgroundColor: "transparent",
                annotations: { textStyle: { color: "black", fontSize: 10 },
                }
            }


            var chart = new google.visualization.BarChart(document.getElementById("painel_apreensoes"))
            google.visualization.events.addListener(chart, 'error', function (googleError) {
            google.visualization.errors.removeError(googleError.id)
            })

            google.visualization.events.addListener(chart, 'ready', function () {
            $scope.imagemGraficoApreensoes = chart.getImageURI()
            })


            chart.draw(data, options)

        }
      }
    }

    function graficoLiberacoes() {

        if ($scope.dadosGraficoLiberacoes.length === 1){

            const elementoGrafico = document.getElementById("painel_liberacoes")
                while (elementoGrafico.hasChildNodes()) {
                elementoGrafico.removeChild(elementoGrafico.lastChild)
            }

        }else{

        let alturaGrafico = null

        if ($scope.dadosGraficoLiberacoes.length > 11){
            alturaGrafico = 1600
        }else{
            alturaGrafico = 800
        }

        google.charts.load('current', {'packages':['corechart']})
        google.setOnLoadCallback(drawChart)

        function drawChart() {

            var data = google.visualization.arrayToDataTable($scope.dadosGraficoLiberacoes)

            var options = {
                title:
                    "LIBERAÇÕES - " +
                    String($scope.dados.data.getDate()).padStart(2, "0") + " de " +
                    $scope.dados.data.toLocaleString("default", {month: "long"})
                    + " de " + $scope.dados.data.getFullYear(),
                titleTextStyle: { color: "#000000", fontName: "arial", fontSize: 14, bold: true, italic: false},
                subtitle: 'Grupo Carvalho',
                legend: { position: "bottom", maxLines: 1, alignment: "center", textStyle: {fontSize: 12}},
                animation: { duration: 3000, easing: "out", startup: true},
                width: 'auto',
                height: alturaGrafico,
                fontSize: 10,
                chartArea: { width: "80%", height: "90%" },
                seriesType: "bars",
                series: [{color: 'purple', visibleInLegend: true}, {color: 'green', visibleInLegend: true}],
                backgroundColor: "transparent",
                annotations: { textStyle: { color: "black", fontSize: 10 },
                }
            }


            var chart = new google.visualization.BarChart(document.getElementById("painel_liberacoes"));
            google.visualization.events.addListener(chart, 'error', function (googleError) {
            google.visualization.errors.removeError(googleError.id)
            });

            google.visualization.events.addListener(chart, 'ready', function () {
            $scope.imagemGraficoLiberacoes = chart.getImageURI()
            })

            chart.draw(data, options)

        }
      }
    }

    function graficoApreensoesAcumulado(){

        if ($scope.dadosGraficoApreensoesAcumulado.length === 1){

            const elementoGrafico = document.getElementById("painel_apreensoesAcumulado")
                while (elementoGrafico.hasChildNodes()) {
                elementoGrafico.removeChild(elementoGrafico.lastChild)
            }

        }else{

            let alturaGrafico = null

            if ($scope.larguraTela > 600 && $scope.larguraTela < 700){
                alturaGrafico = 400
                $scope.heightGraficosAcumulado = "50%"

            }else if ($scope.larguraTela > 700 && $scope.larguraTela < 800){
                alturaGrafico = 430
                $scope.heightGraficosAcumulado = "53%"

            }else if ($scope.larguraTela > 800 && $scope.larguraTela < 900){
                alturaGrafico = 460
                $scope.heightGraficosAcumulado = "56%"

            }else if ($scope.larguraTela > 900 && $scope.larguraTela < 1000){
                alturaGrafico = 480
                $scope.heightGraficosAcumulado = "59%"

            }else if ($scope.larguraTela > 1000 && $scope.larguraTela < 1100){
                alturaGrafico = 520
                $scope.heightGraficosAcumulado = "62%"

            }else{
                alturaGrafico = 700
                $scope.heightGraficosAcumulado = "65%"
            }

        google.charts.load("current", { packages: ["corechart"] })
        google.setOnLoadCallback(drawChart)

        function drawChart() {
            var data = google.visualization.arrayToDataTable(
            $scope.dadosGraficoApreensoesAcumulado
            );

            var options = {
                title:
                    "APREENSÕES ACUMULADO - " +
                    String($scope.dados.data.getDate()).padStart(2, "0") + " de " +
                    $scope.dados.data.toLocaleString("default", { month: "long" }) +
                    " de " + $scope.dados.data.getFullYear(),
                titleTextStyle: { color: "#000000", fontName: "arial", fontSize: 14, bold: true, italic: false },
                subtitle: 'Grupo Carvalho',
                animation: { duration: 3000, easing: "out", startup: true },
                is3D: true,
                width: 'auto',
                height: alturaGrafico,
                fontSize: 10,
                chartArea: { width: "98%", height: $scope.heightGraficosAcumulado}, //"90%", height: "80%" },
                backgroundColor: "transparent",
                legend: { position: "labeled" },
            };

            var chart = new google.visualization.PieChart(
            document.getElementById("painel_apreensoesAcumulado")
            );

            google.visualization.events.addListener(
            chart, "error", function (googleError) {
            google.visualization.errors.removeError(googleError.id)
            });


            google.visualization.events.addListener(chart, "ready", function () {
            $scope.imagemGraficoApreensoesAcumulado = chart.getImageURI()
            });

            chart.draw(data, options)

        }
      }
    }

    function graficoLiberacoesAcumulado(){

        if ($scope.dadosGraficoLiberacoesAcumulado.length === 1){

            const elementoGrafico = document.getElementById("painel_liberacoesAcumulado")
                while (elementoGrafico.hasChildNodes()) {
                elementoGrafico.removeChild(elementoGrafico.lastChild)
            }

        }else{


        let alturaGrafico = null

        if ($scope.larguraTela > 600 && $scope.larguraTela < 700){
            alturaGrafico = 400
            $scope.heightGraficosAcumulado = "50%"

        }else if ($scope.larguraTela > 700 && $scope.larguraTela < 800){
            alturaGrafico = 430
            $scope.heightGraficosAcumulado = "53%"

        }else if ($scope.larguraTela > 800 && $scope.larguraTela < 900){
            alturaGrafico = 460
            $scope.heightGraficosAcumulado = "56%"

        }else if ($scope.larguraTela > 900 && $scope.larguraTela < 1000){
            alturaGrafico = 480
            $scope.heightGraficosAcumulado = "59%"

        }else if ($scope.larguraTela > 1000 && $scope.larguraTela < 1100){
            alturaGrafico = 520
            $scope.heightGraficosAcumulado = "62%"

        }else{
            alturaGrafico = 700
            $scope.heightGraficosAcumulado = "65%"
        }

        google.charts.load('current', {'packages':['corechart']})
        google.setOnLoadCallback(drawChart)

        function drawChart() {
            var data = google.visualization.arrayToDataTable($scope.dadosGraficoLiberacoesAcumulado)

            var options = {
                title : 'LIBERAÇÕES ACUMULADO - ' + String($scope.dados.data.getDate()).padStart(2, '0') + ' de '
                + $scope.dados.data.toLocaleString('default', {month: 'long'})+' de '+ $scope.dados.data.getFullYear(),
                titleTextStyle:{ color: '#000000', fontName: 'arial', fontSize: 14, bold: true, italic: false },
                animation:{ duration: 3000, easing: 'out', startup: true },
                subtitle: 'Grupo Carvalho',
                is3D: true,
                width: 'auto',
                height: alturaGrafico,
                fontSize: 10,
                chartArea: {width: "98%", height: $scope.heightGraficosAcumulado},
                backgroundColor: 'transparent',
                legend: { position: 'labeled' },

            }

            var chart = new google.visualization.PieChart(document.getElementById("painel_liberacoesAcumulado"))

            google.visualization.events.addListener(chart, 'error', function (googleError) {
            google.visualization.errors.removeError(googleError.id)

            })

            google.visualization.events.addListener(chart, 'ready', function () {
            $scope.imagemGraficoLiberacoesAcumulado = chart.getImageURI()
            })

            chart.draw(data, options)

        }
      }
    }

    function graficoTotalApreensoes(){

        if ($scope.dadosTotalGraficoApreensoes[1][3] === 0){

            const elementoGrafico = document.getElementById("painel_totalApreensoes")
                while (elementoGrafico.hasChildNodes()) {
                elementoGrafico.removeChild(elementoGrafico.lastChild)
            }

        }else{

        let alturaGrafico = 350

        google.charts.load('current', {'packages':['corechart']})
        google.setOnLoadCallback(drawChart)

        function drawChart() {
            var data = google.visualization.arrayToDataTable($scope.dadosTotalGraficoApreensoes)

            var options = {
                title:
                    "TOTAL GERAL - APREENSÕES - " +
                    String($scope.dados.data.getDate()).padStart(2, "0") + " de " +
                    $scope.dados.data.toLocaleString("default", {month: "long"})
                    + " de " + $scope.dados.data.getFullYear(),
                subtitle: 'Grupo Carvalho',
                titleTextStyle: { color: "#000000", fontName: "arial", fontSize: 14, bold: true, italic: false},
                legend: { position: "bottom", maxLines: 1, alignment: "center", textStyle: {fontSize: 12}},
                animation: { duration: 3000, easing: "out", startup: true},
                width: 'auto',
                height: alturaGrafico,
                fontSize: 12,
                chartArea: { width: "80%", height: "90%" },
                seriesType: "bars",
                series: [{color: 'd80032', visibleInLegend: true, lineWidth: 10}, {color: 'f85e00', visibleInLegend: true}],
                backgroundColor: "transparent",
                annotations: { textStyle: { color: "black", fontSize: 12 },
                gridlines: { color: 'transparent'},
                }
            }


            var chart = new google.visualization.BarChart(document.getElementById("painel_totalApreensoes"))
            google.visualization.events.addListener(chart, 'error', function (googleError) {
            google.visualization.errors.removeError(googleError.id)
            })

            google.visualization.events.addListener(chart, 'ready', function () {
            $scope.imagemGraficoTotalApreensoes = chart.getImageURI()
            })


            chart.draw(data, options)

        }
      }
    }

    function graficoTotalLiberacoes(){

    if ($scope.dadosTotalGraficoLiberacoes[1][3] === 0 ){

            const elementoGrafico = document.getElementById("painel_totalLiberacoes")
                while (elementoGrafico.hasChildNodes()) {
                elementoGrafico.removeChild(elementoGrafico.lastChild)
            }

        }else{

        let alturaGrafico = 350

        google.charts.load('current', {'packages':['corechart']})
        google.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable($scope.dadosTotalGraficoLiberacoes)

            var options = {
                title:
                    "TOTAL GERAL - LIBERAÇÕES - " +
                    String($scope.dados.data.getDate()).padStart(2, "0") + " de " +
                    $scope.dados.data.toLocaleString("default", {month: "long"})
                    + " de " + $scope.dados.data.getFullYear(),
                subtitle: 'Grupo Carvalho',
                titleTextStyle: { color: "#000000", fontName: "arial", fontSize: 14, bold: true, italic: false},
                legend: { position: "bottom", maxLines: 1, alignment: "center", textStyle: {fontSize: 12}},
                animation: { duration: 3000, easing: "out", startup: true},
                width: 'auto',
                height: alturaGrafico,
                fontSize: 12,
                chartArea: { width: "80%", height: "90%" },
                seriesType: "bars",
                series: [{color: 'a5be00', visibleInLegend: true}, {color: 'teal', visibleInLegend: true}],
                backgroundColor: "transparent",
                annotations: { textStyle: { color: "black", fontSize: 12 },
                //bar: { groupWidth: '20%', gap : 10 },

                }
            }


            var chart = new google.visualization.BarChart(document.getElementById("painel_totalLiberacoes"))
            google.visualization.events.addListener(chart, 'error', function (googleError) {
            google.visualization.errors.removeError(googleError.id)
            });

            google.visualization.events.addListener(chart, 'ready', function () {
            $scope.imagemGraficoTotalLiberacoes = chart.getImageURI()
            });


            chart.draw(data, options)

        }
      }
    }

    function buscaDadosGraficos() {
        let dadosPesquisa = {
            data: $scope.dados.data,
            patio: $scope.dados.patio,
            id_grupo_patios: $scope.dados.id_grupo_patios
        }
        return new Promise(function (resolve, reject) {
            $http.get(urlServidor.urlServidorChatAdmin + '/analitico/dashboard-admin-graficos', { params: dadosPesquisa }).then(function (response) {
                let dados = response.data
                resolve(dados)
            });
        })
    }

    function buscaPatiosGrupo() {
        return new Promise(function (resolve, reject) {
            $http.get(urlServidor.urlServidorChatAdmin + '/grupos-patios/listar').then(function (response) {
                let dados = response.data
                resolve(dados)

                $scope.patiosGrupo = dados.filter((item) => {
                    return item.ativo !== 0
                })
            })
        })
    }

    function buscaDadosGraficosUsuario() {
        let dadosPesquisa = {
            data: $scope.dados.data,
            id_usuario: $scope.idUsuario
        }
        return new Promise(function (resolve, reject) {
            $http.get(urlServidor.urlServidorChatAdmin + '/analitico/dashboard-graficos', { params: dadosPesquisa }).then(function (response) {
                let dados = response.data
                resolve(dados)
            });
        })
    }

    function buscaPatios() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') }).then(function (response) {
                let dados = response.data
                resolve(dados)
            })
        })
    }

    function dadosPesquisa(){

        $scope.dados.data = new Date(new Date().setDate(new Date().getDate() -1 ));
        $scope.dados.id_usuario = $scope.idUsuario
    }

    function hideFiltroPesquisar() {
        if ($scope.tipousuario === 'admin'){
            $scope.hideFiltroPesquisar = false

        buscaPatios().then(function (result) {

            $scope.patios = result.filter((item) => {
                return item.status !== 0
            })
        })

        }else{
            $scope.hideFiltroPesquisar = true

            $scope.buscaInfosGrafico()
        }
    }

    function buscaClientes() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/clientes/listar').then(function (response) {

                let dados = response.data
                dados.sort(function (a, b) {
                    if (a.nome > b.nome) {
                        return 1
                    }
                    if (a.nome < b.nome) {
                        return -1
                    }
                    return 0;
                });
                resolve(dados)
            });
        })

    }

});





app.controller('clientesCtrl', function ($scope, $location, urlServidor, $http) {

    $scope.novo = function (ev, id) {
        $location.path('clientes-novo');
    };
    $scope.limpar = function (ev, id) {
        $scope.dados.razao_social = '';
        $scope.dados.status = '';
    };
    $scope.pesquisar = function (ev) {

        $http.put(urlServidor.urlServidorChatAdmin + '/clientes/listar', $scope.dados).then(function (response) {


            let dados = response.data;
            if ($scope.dados.ordenacao == 'asc') {

                dados.sort(function (a, b) {
                    if ($scope.dados.orderby == 2) {
                        if (a.sigla > b.sigla) {
                            return 1;
                        }
                        if (a.sigla < b.sigla) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    } else {
                        if (a.razao_nome > b.razao_nome) {
                            return 1;
                        }
                        if (a.razao_nome < b.razao_nome) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    }

                });
            } else {

                dados.sort(function (a, b) {
                    if ($scope.dados.orderby == 2) {
                        if (a.sigla < b.sigla) {
                            return 1;
                        }
                        if (a.sigla > b.sigla) {
                            return -1;
                        }

                        return 0;
                    } else {
                        if (a.razao_nome < b.razao_nome) {
                            return 1;
                        }
                        if (a.razao_nome > b.razao_nome) {
                            return -1;
                        }

                        return 0;
                    }
                });
            }

            $scope.dadosList = dados;
        });
        console.log($scope.dadosList);
    };
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("clientes.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: {
                nome: { halign: 'left' },
                telefone: { halign: 'left' }
            },
            body: $scope.dadosList,
            columns: [
                { header: 'Nome', dataKey: 'nome' },
                { header: 'CPF/CNPJ', dataKey: 'cpf_cnpj' },
                { header: 'Cidade', dataKey: 'cidade' },
                { header: 'UF', dataKey: 'uf' },
                { header: 'Telefone', dataKey: 'telefone' },
                { header: 'Status', dataKey: 'desc_ativo' },
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: { top: 30 },
        });

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('clientes.pdf')
    }
    function ordemDecrescente(a, b) {
        if ($scope.dados.orderby == '1') {
            return a.nome < b.nome;
        } else if ($scope.dados.orderby == '2') {
            return a.sigla < b.sigla;
        } else {
            return a.nome < b.nome;
        }
    }
    ;
    function ordemCrescente(a, b) {
        if ($scope.dados.orderby == '1') {
            return a.razao_nome > b.razao_nome;
        } else if ($scope.dados.orderby == '2') {
            return a.sigla > b.sigla;
        } else {
            return a.razao_nome > b.razao_nome;
        }

    }
    ;
    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        $location.path('clientes-editar').search({ dados });
    };
});
app.controller('clientesEditarCtrl', function ($scope, $location, $http, urlServidor, $mdDialog, estados, viaCep) {

    var urlParams = $location.search();
    $scope.dados = {
        id: urlParams.dados[0].id,
        nome: urlParams.dados[0].nome,
        cpf_cnpj: urlParams.dados[0].cpf_cnpj,
        endereco: urlParams.dados[0].endereco,
        bairro: urlParams.dados[0].bairro,
        cidade: urlParams.dados[0].cidade,
        cep: urlParams.dados[0].cep,
        uf: urlParams.dados[0].uf,
        email: urlParams.dados[0].email,
        telefone: urlParams.dados[0].telefone,
        status: urlParams.dados[0].status
    }

    $scope.estados = estados.uf;
    $scope.buscaCep = (cep) => {

        if (cep.length == 9) {

            viaCep.get(cep).then(function (response) {

                $scope.dados.endereco = response.logradouro.toUpperCase();
                $scope.dados.bairro = response.bairro.toUpperCase();
                $scope.dados.cidade = response.localidade.toUpperCase();
                $scope.dados.uf = response.uf.toUpperCase();
            });
        }

    }

    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/clientes/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    showAlert('Cliente já existe!')
                } else {
                    showAlert('Alteração executada com sucesso!');
                }

            }
            ;
        });
    };
    var showAlert = function (ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Atenção!')
                .textContent(ev)
                .ariaLabel('')
                .ok('Ok!')
                .targetEvent(ev)
                .multiple(true)
        );
    };
});
app.controller('fornecedoresCtrl', function ($scope, $location, urlServidor, $http) {

    $scope.novo = function (ev, id) {
        $location.path('fornecedores-novo');
    };
    $scope.limpar = function (ev, id) {
        $scope.dados.cpf_cnpj = '';
        $scope.dados.razao_social = '';
        $scope.dados.status = '';
    };
    

    $scope.pesquisar = function (ev) {

        $http.put(urlServidor.urlServidorChatAdmin + '/fornecedores/listar', $scope.dados).then(function (response) {


            let dados = response.data;
            if ($scope.dados.ordenacao == 'asc') {

                dados.sort(function (a, b) {
                    if ($scope.dados.orderby == 2) {
                        if (a.sigla > b.sigla) {
                            return 1;
                        }
                        if (a.sigla < b.sigla) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    } else {
                        if (a.razao_nome > b.razao_nome) {
                            return 1;
                        }
                        if (a.razao_nome < b.razao_nome) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    }

                });
            } else {

                dados.sort(function (a, b) {
                    if ($scope.dados.orderby == 2) {
                        if (a.sigla < b.sigla) {
                            return 1;
                        }
                        if (a.sigla > b.sigla) {
                            return -1;
                        }

                        return 0;
                    } else {
                        if (a.razao_nome < b.razao_nome) {
                            return 1;
                        }
                        if (a.razao_nome > b.razao_nome) {
                            return -1;
                        }

                        return 0;
                    }
                });
            }

            $scope.dadosList = dados;
        });
    };
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("fornecedores.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: {
                nome: { halign: 'left' },
                telefone: { halign: 'left' }
            },
            body: $scope.dadosList,
            columns: [
        
                { header: 'Razão/Nome', dataKey: 'razao_nome' },
                { header: 'CPF/CNPJ', dataKey: 'cpf_cnpj' },
                { header: 'Tipo', dataKey: 'tipo_pessoa' },
                { header: 'Telefone comercial', dataKey: 'telefone_comercial' },
                { header: 'Celular', dataKey: 'celular' },
                { header: 'Status', dataKey: 'desc_status' },
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: { top: 30 },
        });

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('fornecedores.pdf')
    }
    function ordemDecrescente(a, b) {
        if ($scope.dados.orderby == '1') {
            return a.razao_nome < b.razao_nome;
        } else if ($scope.dados.orderby == '2') {
            return a.sigla < b.sigla;
        } else {
            return a.razao_nome < b.razao_nome;
        }
    }
    ;
    function ordemCrescente(a, b) {
        if ($scope.dados.orderby == '1') {
            return a.razao_nome > b.razao_nome;
        } else if ($scope.dados.orderby == '2') {
            return a.sigla > b.sigla;
        } else {
            return a.razao_nome > b.razao_nome;
        }

    }
    ;
    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        $location.path('fornecedores-editar').search({ dados });
    };
});
app.controller('fornecedoresEditarCtrl', function ($scope, $location, $http, urlServidor, $mdDialog, estados, viaCep) {

    var urlParams = $location.search();
    $scope.dados = {
        id: urlParams.dados[0].id,
        razao_nome: urlParams.dados[0].razao_nome,
        tipo_pessoa: urlParams.dados[0].tipo_pessoa,
        id_tipo_fornecedor: urlParams.dados[0].id_tipo_fornecedor,
        cpf_cnpj: urlParams.dados[0].cpf_cnpj,
        endereco: urlParams.dados[0].endereco,
        bairro: urlParams.dados[0].bairro,
        cidade: urlParams.dados[0].cidade,
        cep: urlParams.dados[0].cep,
        uf: urlParams.dados[0].uf,
        email: urlParams.dados[0].email,
        telefone_comercial: urlParams.dados[0].telefone_comercial,
        celular: urlParams.dados[0].celular,
        responsavel: urlParams.dados[0].responsavel,
        responsavel_faturamento: urlParams.dados[0].responsavel_faturamento,
        tel_resp_faturamento: urlParams.dados[0].tel_resp_faturamento,
        responsavel_logistica: urlParams.dados[0].responsavel_logistica,
        tel_resp_logistica: urlParams.dados[0].tel_resp_logistica,
        dados_bancarios: urlParams.dados[0].dados_bancarios,
        forma_pagto: urlParams.dados[0].forma_pagto,
        status: urlParams.dados[0].status,
        pix_chave: urlParams.dados[0].pix_chave,
        pix_tipo: urlParams.dados[0].pix_tipo
    }

    $scope.tipos = [
        {
            id: 1,
            descricao: 'IMPOSTOS,TAXAS'
        },
        {
            id: 2,
            descricao: 'AGUA,LUZ,TELEFONE,GAZ'
        },
        {
            id: 3,
            descricao: 'REBOQUES'
        },
        {
            id: 4,
            descricao: 'TRANSPORTADORAS'
        },
        {
            id: 5,
            descricao: 'ALUGUEL'
        },
        {
            id: 6,
            descricao: 'SEGUROS'
        },
        {
            id: 7,
            descricao: 'FOLHA PAGTO, COMISSOES'
        },
        {
            id: 8,
            descricao: 'PRESTADOR AUTONOMO'
        },
        {
            id: 9,
            descricao: 'MATERIAL ADMINISTRACAO'
        },
        {
            id: 10,
            descricao: 'OFICINAS'
        },
        {
            id: 11,
            descricao: 'OFICINAS'
        }
    ]

    $scope.estados = estados.uf;
    $scope.buscaCep = (cep) => {

        if (cep.length == 9) {

            viaCep.get(cep).then(function (response) {

                $scope.dados.endereco = response.logradouro.toUpperCase();
                $scope.dados.bairro = response.bairro.toUpperCase();
                $scope.dados.cidade = response.localidade.toUpperCase();
                $scope.dados.uf = response.uf.toUpperCase();
            });
        }

    }

    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/fornecedores/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    showAlert('Fornecedor já existe!')
                } else {
                    showAlert('Alteração executada com sucesso!');
                }

            }
            ;
        });
    };
    var showAlert = function (ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Atenção!')
                .textContent(ev)
                .ariaLabel('')
                .ok('Ok!')
                .targetEvent(ev)
                .multiple(true)
        );
    };
});


app.controller('motivoApreensaoCtrl', function ($scope, $location, $http, urlServidor, $mdDialog, myFunctions) {

    $scope.novo = function (ev, id) {
        $location.path('motivo-apreensao-novo');
    };
    $scope.limpar = function (ev) {
        $scope.dados.descricao = '';

    };
    $scope.dados = {
        descricao: ''
    };
    $scope.pesquisar = function (ev) {

        $scope.dadosList = null;

        $http.get(urlServidor.urlServidorChatAdmin + '/ncv/lista-motivoapreensao', { params: { descricao: $scope.dados.descricao } }).then(function (response) {

            let dados = response.data;
            $scope.dadosList = dados;
        });
    };
    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        window.open('/#/motivo-apreensao-editar?id ' + id, '_blank');
    };

});

app.controller('gruposCtrl', function ($scope, $location, $http, urlServidor, $mdDialog, myFunctions) {

    $scope.novo = function (ev, id) {
        $location.path('grupos-novo');
    };
    $scope.limpar = function (ev) {
        $scope.dados.descricao = '';
        $scope.dados.tipo = '';
        $scope.dadosList = [];
    };
    $scope.pesquisar = function (ev) {

        $scope.dadosList = [];

        $http.put(urlServidor.urlServidorChatAdmin + '/grupos/listar', $scope.dados).then(function (response) {

            let dados = response.data;
            $scope.dadosList = dados;
        });
    };
    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        $location.path('grupos-editar').search({ dados });
    };


    $scope.excluir = (ev, id) => {

        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        let confirm = $mdDialog.confirm()
            .title('Grupos!')
            .textContent('Tem certeza que deseja excluir esse item?')
            .ariaLabel('Lucky day')
            .ok('Excluir')
            .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {

            $http.post(urlServidor.urlServidorChatAdmin + '/grupos/exclui-grupo', { id: dados[0].id }).then(function (response) {
                if (response.data.code) {
                    myFunctions.showAlert('Não foi possível excluir o grupo !');
                } else {
                    myFunctions.showAlert('Grupo exclído com sucesso !');
                    var alt = dados[0];


                    var json = Object.assign({}, alt);
                    $scope.logs = {
                        operacao: 'EXCLUSÃO',
                        tipo: 'GRUPOS',
                        id_usuario: localStorage.getItem('id_usuario'),
                        alteracoes: '',
                        anterior: JSON.stringify(json)
                    };

                    $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro', $scope.logs).then(function (response) {
                        if (response.data.code) {
                            $scope.erro = response;
                        } else {
                            dados = null;
                            alt = null;
                            setTimeout(function () {

                                location.reload();

                            }, 1500);

                        }
                    });
                }
            });
        });
    }


    $scope.gerarPdf = function () {

        if ($scope.dadosList){
  
          const PDFAjustes = $scope.dadosList.map((item) => ({
            descricao: item.descricao,
            ativo: item.ativo_desc
  
          }))
  
          var doc = new jsPDF({ orientation: "landscape" })
          var totalPagesExp = "{total_pages_count_string}"
  
          doc.setFontSize(10)
  
          doc.autoTable({
            columnStyles: {
              vencimento: { halign: "left" },
            },
  
            body: PDFAjustes,
            columns: [
              { header: "Descrição", dataKey: "descricao" },
              { header: "Ativo", dataKey: "ativo" },
                
            ],
            bodyStyles: {
              margin: 10,
              fontSize: 08,
            },
  
            didDrawPage: function (data) {
              // Header
              doc.setFontSize(18);
              doc.setTextColor(40);
              doc.setFontStyle("normal");
  
              doc.text(
                "Grupos",
                data.settings.margin.left + 15,
                22
              );
  
              // Footer
              var str = "Pagina " + doc.internal.getNumberOfPages();
              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
                var totalpaginas = totalPagesExp;
              }
  
              doc.setFontSize(10);
  
              // jsPDF 1.4+ uses getWidth, <1.4 uses .width
              var pageSize = doc.internal.pageSize;
              var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();
  
              doc.text(
                str + "                        ",
                data.settings.margin.left,
                pageHeight - 10
              );
            },
            margin: { top: 30 },
          });
          if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
          }
  
          doc.save("grupos.pdf");
  
          }else{
  
          myFunctions.showAlert('Sem dados para gerar PDF! Verifique!')
    
        }
    }

    $scope.gerarPlanilha = () => {

      if ($scope.dadosList) {

        const excelAjustes = $scope.dadosList.map((item) => ({
            descricao: item.descricao,
            ativo: item.ativo_desc

        }));

        alasql(
          'SELECT * INTO XLSX("grupos.xlsx",{headers:true}) FROM ?', [excelAjustes]
        )

        }else{    
        myFunctions.showAlert('Sem dados para gerar planilha! Verifique!')
        }
        
    }

});





app.controller('gruposEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep) {

    var urlParams = $location.search();
    $scope.dados = {
        id: urlParams.dados[0].id,
        descricao: urlParams.dados[0].descricao,
        tipo: urlParams.dados[0].tipo,
        ativo: urlParams.dados[0].ativo
    }

    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/grupos/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')
            } else {

                if (response.data == '11000') {
                    myFunctions.showAlert('Grupo já existe!')
                } else {
                    myFunctions.showAlert('Alteração executada com sucesso!');


                }

            }
            ;
        });
    };
});
app.controller('categoriasCtrl', function ($scope, $location, $http, urlServidor, $mdDialog, myFunctions) {

    $scope.novo = function (ev, id) {
        $location.path('categorias-novo');
    };
    $scope.limpar = function (ev) {
        $scope.dados.descricao = '';
        $scope.dados.tipo = '';
        $scope.dados.grupo = '';
        $scope.dadosList = []
    };
    buscaGrupos().then(function (result) {
        $scope.grupos = result;
    });
    $scope.pesquisar = function (ev) {
        $scope.dadosList = null;

        $http.put(urlServidor.urlServidorChatAdmin + '/categorias/listar', $scope.dados).then(function (response) {

            let dados = response.data;
            $scope.dadosList = dados;
        });
    };
    function buscaGrupos() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/grupos/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }

    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        if (dados[0].id > 0) {
            $location.path('categorias-editar').search({ dados });
        }
    };


    $scope.excluir = (ev, id) => {

        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        let confirm = $mdDialog.confirm()
            .title('Categorias!')
            .textContent('Tem certeza que deseja excluir esse item?')
            .ariaLabel('Lucky day')
            .ok('Excluir')
            .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {

            $http.post(urlServidor.urlServidorChatAdmin + '/categorias/exclui-categoria', { id: dados[0].id }).then(function (response) {
                if (response.data.code) {
                    myFunctions.showAlert('Não foi possível excluir a categoria !');
                } else {

                    var alt = dados[0];


                    var json = Object.assign({}, alt);
                    $scope.logs = {
                        operacao: 'EXCLUSÃO',
                        tipo: 'CATEGORIAS',
                        id_usuario: localStorage.getItem('id_usuario'),
                        alteracoes: '',
                        anterior: JSON.stringify(json)
                    };

                    $http.post(urlServidor.urlServidorChatAdmin + '/usuarios/logs-financeiro', $scope.logs).then(function (response) {
                        if (response.data.code) {
                            $scope.erro = response;
                        } else {
                            dados = null;
                            alt = null;
                            setTimeout(function () {

                                location.reload();

                            }, 1500);

                        }

                    });
                    myFunctions.showAlert('Categoria excluída com sucesso!');
                }
            });

        });

    }

    $scope.gerarPdf = function () {

        if ($scope.dadosList){
  
          const PDFAjustes = $scope.dadosList.map((item) => ({
            descricao: item.descricao,
            ativo: item.ativo_desc,
            tipo: item.tipo_categoria
  
          }))
  
          var doc = new jsPDF({ orientation: "landscape" })
          var totalPagesExp = "{total_pages_count_string}"
  
          doc.setFontSize(10)
  
          doc.autoTable({
            columnStyles: {
              vencimento: { halign: "left" },
            },
  
            body: PDFAjustes,
            columns: [
              { header: "Descrição", dataKey: "descricao" },
              { header: "Ativo", dataKey: "ativo" },
              { header: "Tipo", dataKey: "tipo" },
                
            ],
            bodyStyles: {
              margin: 10,
              fontSize: 08,
            },
  
            didDrawPage: function (data) {
              // Header
              doc.setFontSize(18);
              doc.setTextColor(40);
              doc.setFontStyle("normal");
  
              doc.text(
                "Categorias",
                data.settings.margin.left + 15,
                22
              );
  
              // Footer
              var str = "Pagina " + doc.internal.getNumberOfPages();
              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
                var totalpaginas = totalPagesExp;
              }
  
              doc.setFontSize(10);
  
              // jsPDF 1.4+ uses getWidth, <1.4 uses .width
              var pageSize = doc.internal.pageSize;
              var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();
  
              doc.text(
                str + "                        ",
                data.settings.margin.left,
                pageHeight - 10
              );
            },
            margin: { top: 30 },
          });
          if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
          }
  
          doc.save("categorias.pdf");
  
          }else{
  
          myFunctions.showAlert('Sem dados para gerar PDF! Verifique!')
    
        }
    }

    $scope.gerarPlanilha = () => {

      if ($scope.dadosList) {

        const excelAjustes = $scope.dadosList.map((item) => ({
            descricao: item.descricao,
            tipo: item.tipo_categoria,
            ativo: item.ativo_desc

        }));

        alasql(
          'SELECT * INTO XLSX("categorias.xlsx",{headers:true}) FROM ?', [excelAjustes]
        )

        }else{    
        myFunctions.showAlert('Sem dados para gerar planilha! Verifique!')
        }
        
    }

});


app.controller('categoriasEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep) {

    var urlParams = $location.search();
    $scope.dados = {
        id: urlParams.dados[0].id,
        descricao: urlParams.dados[0].descricao,
        tipo: urlParams.dados[0].tipo,
        ativo: urlParams.dados[0].ativo
    }

    buscaGrupos().then(function (result) {
        $scope.grupos = result;
    });
    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/categorias/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    myFunctions.showAlert('Categoria já existe!')
                } else {
                    myFunctions.showAlert('Alteração executada com sucesso!');
                }

            }
            ;
        });
    };
    function buscaGrupos() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/grupos/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }

});
app.controller('tiposrecebimentoCtrl', function ($scope, $location, $http, urlServidor) {

    $scope.novo = function (ev, id) {
        $location.path('tipos-recebimento-novo');
    };
    $scope.limpar = function (ev) {
        $scope.dados.descricao = '';
        $scope.dados.tipo = '';
        $scope.dados.prazo = '';
    };
    $scope.pesquisar = function (ev) {

        $http.put(urlServidor.urlServidorChatAdmin + '/tipos-recebimento/listar', $scope.dados).then(function (response) {

            $scope.dadosList = response.data;
        });
    };
    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        if (dados[0].id > 0) {
            $location.path('tipos-recebimento-editar').search({ dados });
        }
    }

    $scope.gerarPDF = function () {

        const PDFAjustes = $scope.dadosList.map((item) => ({
            descricao: item.descricao,
            tipo: item.tipo,
            prazo: item.prazo,
            ativo: item.desc_ativo,

        }))

        var doc = new jsPDF({ orientation: "landscape" })
        var totalPagesExp = "{total_pages_count_string}"

        doc.setFontSize(10)

        doc.autoTable({
          columnStyles: {
            vencimento: { halign: "left" },
          },

          body: PDFAjustes,
          columns: [
            { header: "Descrição", dataKey: "descricao" },
            { header: "Tipo", dataKey: "tipo" },
            { header: "Prazo", dataKey: "prazo" },
            { header: "Ativo", dataKey: "ativo" },

          ],
          bodyStyles: {
            margin: 10,
            fontSize: 08,
          },

          didDrawPage: function (data) {
            // Header
            doc.setFontSize(18);
            doc.setTextColor(40);
            doc.setFontStyle("normal");

            doc.text(
              "Tipos de Recebimento",
              data.settings.margin.left + 15,
              22
            );

            // Footer
            var str = "Pagina " + doc.internal.getNumberOfPages();
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === "function") {
              str = str + " de " + totalPagesExp;
              var totalpaginas = totalPagesExp;
            }

            doc.setFontSize(10);

            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize;
            var pageHeight = pageSize.height
              ? pageSize.height
              : pageSize.getHeight();

            doc.text(
              str + "                        ",
              data.settings.margin.left,
              pageHeight - 10
            );
          },
          margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === "function") {
          doc.putTotalPages(totalPagesExp);
        }

        doc.save("tipos-recebimento.pdf");
    };



    $scope.gerarPlanilha = () => {

       const excelAjustes = $scope.dadosList.map((item) => ({
            descricao: item.descricao,
            tipo: item.tipo,
            prazo: item.prazo,
            ativo: item.desc_ativo,

        }));

        alasql(
          'SELECT * INTO XLSX("tipos-recebimento.xlsx",{headers:true}) FROM ?', [excelAjustes]
        );
    };

});

app.controller('tiposrecebimentoEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep) {

    var urlParams = $location.search();
    $scope.dados = {
        id: urlParams.dados[0].id,
        descricao: urlParams.dados[0].descricao,
        tipo: urlParams.dados[0].tipo,
        prazo: urlParams.dados[0].prazo,
        ativo: urlParams.dados[0].ativo
    }


    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/tipos-recebimento/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    myFunctions.showAlert('Tipo Recebimento já existe!')
                } else {
                    myFunctions.showAlert('Alteração executada com sucesso!');
                }

            }
            ;
        });
    };
});
app.controller('contasCtrl', function ($scope, $location, $http, urlServidor) {

    $scope.novo = function (ev, id) {
        $location.path('contas-novo');
    };
    $scope.limpar = function (ev) {
        $scope.dados.descricao = '';
        $scope.dados.tipo = '';
    };
    $scope.pesquisar = function (ev) {

        $http.put(urlServidor.urlServidorChatAdmin + '/contas/listar', $scope.dados).then(function (response) {

            let dados = response.data;
            $scope.dadosList = dados;
        });
    };
    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        if (dados[0].id > 0) {
            $location.path('contas-editar').search({ dados });
        }
    };
});
app.controller('contasEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep) {

    var urlParams = $location.search();
    $scope.dados = {
        id: urlParams.dados[0].id,
        descricao: urlParams.dados[0].descricao,
        tipo: urlParams.dados[0].tipo,
        codigo_banco: urlParams.dados[0].codigo_banco,
        agencia: urlParams.dados[0].agencia,
        conta: urlParams.dados[0].conta,
        cnpj: urlParams.dados[0].cnpj,
        gerente: urlParams.dados[0].gerente,
        telefone: urlParams.dados[0].telefone,
        email: urlParams.dados[0].email,
        titular: urlParams.dados[0].titular,
        tx_boleto: urlParams.dados[0].tx_boleto,
        banco: urlParams.dados[0].banco,
        saldo: 0,
        data: null
    }

    $http.get(urlServidor.urlServidorChatAdmin + '/contas/listar-saldos', { params: { idconta: $scope.dados.id } }).then(function (response) {
        $scope.dadosSaldos = response.data;
    });
    $scope.isDisabledSaveSaldo = true;
    $scope.editarSaldo = function ($event, rowId) {

        let pos = $scope.dadosSaldos.map(function (e) {
            return e.id;
        });
        let itemPosicao = pos.indexOf(rowId)
        let aData = $scope.dadosSaldos[itemPosicao].data.split('/')
        $scope.dados.valor = $scope.dadosSaldos[itemPosicao].valor;
        $scope.dados.data = new Date(aData[2], aData[1] - 1, aData[0]);
        if ($scope.dadosSaldos[itemPosicao].saldo < 0) {
            $scope.dados.checksaldo = true;
        } else {
            $scope.dados.checksaldo = false;
        }

        $scope.isDisabledSaveSaldo = false;
    };
    $scope.novoSaldo = function ($event, rowId) {

        $scope.dados.saldo = '';
        $scope.dados.data = new Date();
        $scope.isDisabledSaveSaldo = false;
    };
    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/contas/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    myFunctions.showAlert('Conta já existe!')
                } else {
                    myFunctions.showAlert('Alteração executada com sucesso!');
                }

            }
            ;
        });
    };
    $scope.gravaSaldo = function () {

        $scope.isDisabledSaveSaldo = true;
        $http.post(urlServidor.urlServidorChatAdmin + '/contas/cadastrar-saldo', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                myFunctions.showAlert('Saldo gravado com sucesso!');
                $http.get(urlServidor.urlServidorChatAdmin + '/contas/listar-saldos', { params: { idconta: $scope.dados.id } }).then(function (response) {
                    $scope.dadosSaldos = response.data;
                });
                $scope.dados.saldo = '';
                $scope.dados.data = new Date();
            }
            ;
        });
    };
});




app.controller('caixaCtrl', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.novo = function (ev, id) {
        $location.path('caixa-novo');
    };
    $scope.mesAtual = function (ev, id) {
        let hoje = new Date()
        $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
        $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    };
    $scope.dados = { dataInicio: null, dataFinal: null };
    $scope.limpar = function (ev) {
        $scope.dados.tipo = null;
        $scope.dados.dataInicio = null;
        $scope.dados.dataFinal = null;
        $scope.dados.categoria = null;
        $scope.dados.conta = null;
    };

    $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') }).then(function (response) {
        $scope.patios = response.data;
    });

    buscaCategorias().then(function (result) {
        $scope.categorias = result;
    });
    buscaContas().then(function (result) {
        $scope.contas = result;
    });

    function somarDespesas(dados) {

        return new Promise((resolve, reject) => {

            var total = dados.reduce(function (prevVal, elem) {
                return (elem.tipo == 0 || elem.tipo == 2) ? prevVal : prevVal + elem.valor_semformatacao;
            }, 0);
            resolve(total)

        });
    }

    function somarReceitas(dados) {

        return new Promise((resolve, reject) => {

            var total = dados.reduce(function (prevVal, elem) {
                return (elem.tipo == 1 || elem.tipo == 2) ? prevVal : prevVal + elem.valor_semformatacao;
            }, 0);
            resolve(total)

        });
    }


    $scope.pesquisar = function (ev) {

        if ($scope.dados.dataInicio == null || $scope.dados.dataFinal == null) {

            myFunctions.showAlert('Obrigatório informar periodo para filtro!')

        } else {

            var dataInicio = $scope.dados.dataInicio;
            var dataFinal = $scope.dados.dataFinal;
            if ((dataInicio !== null && dataFinal !== null) && (dataFinal >= dataInicio)) {

                $http.put(urlServidor.urlServidorChatAdmin + '/caixa/listar', $scope.dados).then(function (response) {

                    let dados = response.data;
                    somarReceitas(dados).then(function (result) {
                        $scope.total_receita = result;
                        somarDespesas(dados).then(function (result) {
                            $scope.total_despesa = result;
                            $scope.dadosList = dados;
                        });
                    });
                });
            } else {
                if (dataInicio !== null && dataFinal !== null) {
                    myFunctions.showAlert('Data final não pode ser anterior a data inicial!')
                }
            }

        }

    };
    function buscaCategorias() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/categorias/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }

    function buscaContas() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/contas/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }

    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("relatorio-caixa.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }


    $scope.gerarPdf = function () {
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: {
                mascara: { halign: 'center' },
                data: { halign: 'center' },
                saida: { halign: 'center' },
                entrada: { halign: 'center' },
                saldo: { halign: 'center' },
                descricao_grupo: { halign: 'center' },
                descricao_categoria: { halign: 'center' },
                tipo_conta: { halign: 'center' },
                status: { halign: 'center' }
            },
            body: $scope.dadosList,
            columns: [
                { header: 'Plano', dataKey: 'mascara' },
                { header: 'Data', dataKey: 'data' },
                { header: 'Saida', dataKey: 'saida' },
                { header: 'Entrada', dataKey: 'entrada' },
                { header: 'Saldo', dataKey: 'saldo' },
                { header: 'Grupo', dataKey: 'descricao_grupo' },
                { header: 'Categoria', dataKey: 'descricao_categoria' },
                { header: 'Conta', dataKey: 'tipo_conta' },
                { header: 'Status', dataKey: 'status' },
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                doc.text('Movimento de Caixa de ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22)

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('caixa-analitico.pdf')
    }



    $scope.detalhe = function (ev, rowId, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        $location.path('caixa-editar').search({ dados });
    };
    $scope.cancelar = function (ev, rowId, id) {
        let dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        if (dados[0].status == 'CANCELADO') {
            myFunctions.showAlert('Lançamento já esta cancelado!')
        } else {
            $location.path('caixa-cancelar').search({ dados });
        }
    };


});


app.controller('caixaEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep) {

    var urlParams = $location.search();
    $scope.dados = {
        id: urlParams.dados[0].id,
        id_cliente_fornec: urlParams.dados[0].id_cliente_fornec,
        valor: urlParams.dados[0].valor,
        documento: urlParams.dados[0].documento,
        competencia: urlParams.dados[0].competencia,
        data: urlParams.dados[0].data,
        comentario: urlParams.dados[0].comentario,
        grupo: urlParams.dados[0].grupo,
        tipo: urlParams.dados[0].tipo,
        status: urlParams.dados[0].status,
        categoria: urlParams.dados[0].categoria,
        conta: urlParams.dados[0].id_conta,
        id_patio: urlParams.dados[0].id_patio
    }

    if ($scope.dados.tipo == 0) {
        $scope.mostrarcliente = true;
    } else {
        $scope.mostrarfornecedor = true;
    }

    $scope.grupoSelecionado = function (tipo_desc) {
        let item = $scope.grupos.find(item => item.id === tipo_desc);
        $scope.dados.tipo = item.tipo;
    };
    buscaGrupos().then(function (result) {
        $scope.grupos = result;
    });
    buscaContas().then(function (result) {
        $scope.contas = result;
    });
    buscaCategorias($scope.dados.grupo).then(function (result) {
        $scope.categorias = result;
    });
    buscaFornecedores().then(function (result) {
        $scope.fornecedores = result;
    });
    buscaClientes().then(function (result) {
        $scope.clientes = result;
    });
    buscaPatios(localStorage.getItem('id_usuario'), localStorage.getItem('id_empresa')).then(function (result) {
        $scope.patios = result;
    });
    $scope.SelecionaCategoria = function (idgrupo) {
        $scope.dados.categoria = '';
        buscaCategorias(idgrupo).then(function (result) {
            $scope.categorias = result;
        });
    }

    $scope.SelecionaTipo = function (id) {

        buscaItemArray($scope.categorias, id).then(function (response) {

            if (response == 0) {
                $scope.mostrarcliente = true
                $scope.mostrarfornecedor = false
            } else {
                $scope.mostrarfornecedor = true
                $scope.mostrarcliente = false
            }

        })

    }


    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/caixa/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    myFunctions.showAlert('Conta a Pagar já existe!')
                } else {
                    myFunctions.showAlert('Alteração executada com sucesso!');
                }

            }
            ;
        });
    };
    function buscaGrupos() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/grupos/listar').then(function (response) {

                let dados = response.data;
                dados.sort(function (a, b) {
                    if (a.descricao < b.descricao) {
                        return 1;
                    }
                    if (a.descricao > b.descricao) {
                        return -1;
                    }
                    return 0;
                });
                resolve(dados);
            });
        })

    }

    function buscaItemArray(arrayItens, id) {
        return new Promise(function (resolve, reject) {

            for (var x in arrayItens) {
                if (arrayItens[x].id == id) {
                    resolve(arrayItens[x].tipo);
                }
            }

        })
    }

    function buscaContas() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/contas/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }

    function buscaCategorias(idgrupo) {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/categorias/listar', { grupo: idgrupo }).then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }

    function buscaFornecedores() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/fornecedores/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }

    function buscaClientes() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/clientes/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }

    function buscaPatios(idUsuario, idEmpresa) {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: idUsuario, idEmpresa: idEmpresa }).then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }




});


app.controller('caixaCancelarCtrl', function ($scope, $http, $location, myFunctions, urlServidor) {

    var urlParams = $location.search();
    $scope.dados = {
        id: urlParams.dados[0].id,
        id_cliente_fornec: null,
        valor: urlParams.dados[0].valor,
        categoria: urlParams.dados[0].categoria,
        comentario: urlParams.dados[0].comentario,
        data: urlParams.dados[0].data,
        status: urlParams.dados[0].status,
        documento: urlParams.dados[0].documento
    }


    buscaCategorias().then(function (result) {
        $scope.categorias = result;
    });
    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/caixa/cancela', $scope.dados).then(function (response) {

            if (response.data.code) {
                myFunctions.showAlert('Erro na gravação!')
            } else {
                myFunctions.showAlert('Lançamento cancelado!');
            }
            ;
        });
    };
    function buscaCategorias() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/categorias/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })

    }

});
app.controller('fluxoCaixaCtrl', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.mesAtual = function (ev, id) {

        let hoje = new Date()

        $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
        $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth(), hoje.getDate());
    };

    $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') }).then(function (response) {
        $scope.patios = response.data;
    });

    buscaContas().then(function (result) {
        $scope.contas = result;
    });
    $scope.dados = { dataInicio: null, dataFinal: null, conta: null };
    $scope.limpar = function (ev) {
        $scope.dados.dataInicio = null;
        $scope.dados.dataFinal = null;
        $scope.dados.conta = null;
    };
    function busca(obj_, item) {
        return new Promise((resolve, reject) => {

            for (var j = 0; j < obj_.length; ++j) {
                if (obj_[j].data == item) {
                    resolve(j);
                }
            }

        })
    }


    function ajustaDados(lancamentos) {

        return new Promise((resolve, reject) => {

            var dados = lancamentos;
            var saldo_atual = 0;
            dados.forEach(function (elemento, idx) {

                if ((dados[idx].saldo_anterior > 0) || (dados[idx].saldo_anterior < 0)) {
                    dados[idx].saldo = (dados[idx].saldo_anterior + dados[idx].receita) - dados[idx].despesa;
                    saldo_atual = (dados[idx].saldo_anterior + dados[idx].receita) - dados[idx].despesa;
                    dados[idx].saldo_ = myFunctions.numberToReal(saldo_atual);
                }

                if (dados[idx].saldo_anterior == 0) {
                    dados[idx].saldo_anterior_ = myFunctions.numberToReal(saldo_atual);
                    dados[idx].saldo_anterior = saldo_atual;
                    dados[idx].saldo = (dados[idx].saldo_anterior + dados[idx].receita) - dados[idx].despesa;
                    dados[idx].saldo_ = myFunctions.numberToReal(dados[idx].saldo);
                    saldo_atual = (dados[idx].saldo_anterior + dados[idx].receita) - dados[idx].despesa;
                }


            });
            resolve(dados);
        });
    }

    function somarDespesas(dados) {

        return new Promise((resolve, reject) => {


            var total = dados.reduce(function (prevVal, elem) {
                return prevVal + elem.despesa;
            }, 0);
            resolve(total)

        });
    }

    function somarReceitas(dados) {

        return new Promise((resolve, reject) => {

            var total = dados.reduce(function (prevVal, elem) {
                return prevVal + elem.receita;
            }, 0);
            resolve(total)

        });
    }

    function buscaContas() {
        return new Promise(function (resolve, reject) {
            $http.put(urlServidor.urlServidorChatAdmin + '/contas/listar').then(function (response) {
                let dados = response.data;
                resolve(dados);
            });
        })
    }



    $scope.pesquisar = function (ev) {

        if (($scope.dados.dataInicio == null) ||
            ($scope.dados.dataFinal == null)) {

            myFunctions.showAlert('Obrigatório informar periodo!')

        } else {

            var dataInicio = $scope.dados.dataInicio;
            var dataFinal = $scope.dados.dataFinal;
            var conta = $scope.dados.conta;
            if ((dataInicio !== null && dataFinal !== null) && (dataFinal >= dataInicio)) {

                $http.get(urlServidor.urlServidorChatAdmin + '/caixa/fluxo-caixa', { params: { dataInicio: dataInicio, dataFinal: dataFinal, conta: conta } }).then(function (response) {

                    let dadosFluxoCaixa = response.data[0];
                    somarReceitas(dadosFluxoCaixa).then(function (result) {
                        $scope.total_receita = result;
                        somarDespesas(dadosFluxoCaixa).then(function (result) {
                            $scope.total_despesa = result;
                            ajustaDados(dadosFluxoCaixa).then(function (result) {
                                $scope.dadosList = result;
                            })
                        });
                    });
                });
            } else {
                if (dataInicio !== null && dataFinal !== null) {
                    myFunctions.showAlert('Data final não pode ser anterior a data inicial!')
                }
            }

        }

    };
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("relatorio-fluxo-caixa.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(06);
        doc.autoTable({



            body: $scope.dadosList,
            columns: [

                { header: 'Data', dataKey: 'data' },
                { header: 'Saldo Anterior', dataKey: 'saldo_anterior_' },
                { header: 'Despesa', dataKey: 'despesa_' },
                { header: 'Receita', dataKey: 'receita_' },
                { header: 'Saldo', dataKey: 'saldo_' },


            ],
            bodyStyles: {
                margin: 5,
                fontSize: 06,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(16)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                doc.text('Fluxo de caixa no periodo de ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22)

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(06)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('relatorio-fluxo-caixa.pdf')
    }
});
app.controller('autoridadesCtrl', function ($scope, $location, urlServidor, $http) {

    $scope.novo = function (ev, id) {
        $location.path('autoridades-novo');
    };
    $scope.limpar = function (ev, id) {
        $scope.dados.descricao = '';
        $scope.dados.sigla = '';
        $scope.dados.ativo = '';
    };
    $scope.pesquisar = function (ev) {

        $http.put(urlServidor.urlServidorChatAdmin + '/autoridades/listar', $scope.dados).then(function (response) {


            let dados = response.data;
            if ($scope.dados.ordenacao == 'asc') {

                dados.sort(function (a, b) {
                    if ($scope.dados.orderby == 2) {
                        if (a.sigla > b.sigla) {
                            return 1;
                        }
                        if (a.sigla < b.sigla) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    } else {
                        if (a.descricao > b.descricao) {
                            return 1;
                        }
                        if (a.descricao < b.descricao) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    }

                });
            } else {

                dados.sort(function (a, b) {
                    if ($scope.dados.orderby == 2) {
                        if (a.sigla < b.sigla) {
                            return 1;
                        }
                        if (a.sigla > b.sigla) {
                            return -1;
                        }

                        return 0;
                    } else {
                        if (a.descricao < b.descricao) {
                            return 1;
                        }
                        if (a.descricao > b.descricao) {
                            return -1;
                        }

                        return 0;
                    }
                });
            }

            $scope.dadosList = dados;
        });
    };

    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("autoridades.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({orientation: "landscape"});
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: {descricao: {halign: 'left'}},
            body: $scope.dadosList,
            columns: [
                {header: 'Descrição', dataKey: 'descricao'},
                {header: 'Local', dataKey: 'local'},
                {header: '%', dataKey: 'porcentagem'},
                {header: 'Ativo', dataKey: 'desc_ativo'},
              
                
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 8,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

               // doc.text('Relátorio de funcionários')

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: {top: 30},
        });

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        } 
       
        doc.save('autoridades.pdf')
    }
      
    function ordemDecrescente(a, b) {
        if ($scope.dados.orderby == '1') {
            return a.descricao < b.descricao;
        } else if ($scope.dados.orderby == '2') {
            return a.sigla < b.sigla;
        } else {
            return a.descricao < b.descricao;
        }
    }
    ;
    function ordemCrescente(a, b) {
        if ($scope.dados.orderby == '1') {
            return a.descricao > b.descricao;
        } else if ($scope.dados.orderby == '2') {
            return a.sigla > b.sigla;
        } else {
            return a.descricao > b.descricao;
        }

    }
    ;
    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        window.open('/#/autoridades-editar?id ' + id, '_blank');

    };
});
app.controller('autoridadesEditarCtrl', function ($scope, $location, $http, urlServidor, $mdDialog, estados, viaCep) {


    const queryString = window.location.hash;

    var _id = queryString.slice(queryString.indexOf("%") + 3);
    //var urlParams = $location.search();

    $http.get(urlServidor.urlServidorChatAdmin + '/autoridades/busca-id', { params: { id: _id } }).then(function (response) {
        $scope.autoridades = response.data;
        $scope.dados = $scope.autoridades[0];
    });


    /*$scope.dados = {
        id: urlParams.dados[0].id,
        descricao: urlParams.dados[0].descricao,
        sigla: urlParams.dados[0].sigla,
        local: urlParams.dados[0].local,
        ativo: urlParams.dados[0].ativo,
        observacao: urlParams.dados[0].observacao,
        endereco: urlParams.dados[0].endereco,
        bairro: urlParams.dados[0].bairro,
        cidade: urlParams.dados[0].cidade,
        cep: urlParams.dados[0].cep,
        uf: urlParams.dados[0].uf,
        email: urlParams.dados[0].email,
        telefone: urlParams.dados[0].telefone,
        celular: urlParams.dados[0].celular,
        status: urlParams.dados[0].status,
        contato: urlParams.dados[0].contato,
    }*/

    $scope.estados = estados.uf;
    $scope.buscaCep = (cep) => {

        if (cep.length == 9) {

            viaCep.get(cep).then(function (response) {

                $scope.dados.endereco = response.logradouro.toUpperCase();
                $scope.dados.bairro = response.bairro.toUpperCase();
                $scope.dados.cidade = response.localidade.toUpperCase();
                $scope.dados.uf = response.uf.toUpperCase();
            });
        }

    }

    $scope.fechar = function () {
        window.close();
    }

    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/autoridades/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    showAlert('Autoridade já existe!')
                } else {
                    showAlert('Alteração executada com sucesso!');
                }

            }
            ;
        });
    };
    var showAlert = function (ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Atenção!')
                .textContent(ev)
                .ariaLabel('')
                .ok('Ok!')
                .targetEvent(ev)
                .multiple(true)
        );
    };
});



app.controller('patiosCtrl', function ($scope, $location, $http, urlServidor) {

    $scope.novo = function (ev, id) {
        $location.path('patios-novo');
    };
    $scope.limpar = function (ev) {
        $scope.dados.descricao = '';
        $scope.dados.empresa = '';
        $scope.dados.ativo = '';
    };
    $scope.pesquisar = function (ev) {

        $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar', $scope.dados).then(function (response) {

            let dados = response.data;
            if ($scope.dados.ordenacao == 'asc') {

                dados.sort(function (a, b) {
                    if ($scope.dados.orderby == 2) {
                        if (a.cidade > b.cidade) {
                            return 1;
                        }
                        if (a.cidade < b.cidade) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    } else {
                        if (a.nome > b.nome) {
                            return 1;
                        }
                        if (a.nome < b.nome) {
                            return -1;
                        }
                        // a must be equal to b
                        return 0;
                    }

                });
            } else {

                dados.sort(function (a, b) {
                    if ($scope.dados.orderby == 2) {
                        if (a.cidade < b.cidade) {
                            return 1;
                        }
                        if (a.cidade > b.cidade) {
                            return -1;
                        }

                        return 0;
                    } else {
                        if (a.nome < b.nome) {
                            return 1;
                        }
                        if (a.nome > b.nome) {
                            return -1;
                        }

                        return 0;
                    }
                });
            }

            $scope.dadosList = dados;
        });
    };
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("patios.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: {
                nome: { halign: 'left' },
                telefone: { halign: 'left' }
            },
            body: $scope.dadosList,
            columns: [
                { header: 'Nome', dataKey: 'nome' },
                { header: 'Cidade', dataKey: 'cidade' },
                { header: 'Telefone', dataKey: 'telefone' },
                { header: 'Ativo', dataKey: 'desc_ativo' },
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: { top: 30 },
        });

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('patios.pdf')
    }
    function ordemDecrescente(a, b) {
        if ($scope.dados.orderby == '1') {
            return a.nome < b.nome;
        } else if ($scope.dados.orderby == '2') {
            return a.cidade < b.cidade;
        } else {
            return a.nome < b.nome;
        }
    }
    ;
    function ordemCrescente(a, b) {
        if ($scope.dados.orderby == '1') {
            return a.nome > b.nome;
        } else if ($scope.dados.orderby == '2') {
            return a.cidade > b.cidade;
        } else {
            return a.nome > b.nome;
        }

    }
    ;
    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        $location.path('patios-editar').search({ dados });
    };
});

app.controller('reboquesCtrl', function ($scope, $location, $http, urlServidor) {

    $scope.novo = function (ev, id) {
        $location.path('reboques-novo');
    };
    $scope.limpar = function (ev) {
        $scope.dados.empresa = '';
        $scope.dados.descricao = '';
        $scope.dados.placa = '';
        $scope.dados.cidade = '';
        $scope.dados.endereco = '';
        $scope.dados.bairro = '';
        $scope.dados.filtrar_por = '';
    };
    buscaEmpresas().then(function (result) {
        $scope.empresas = result;
    });
    $scope.pesquisar = function (ev) {

        $http.put(urlServidor.urlServidorChatAdmin + '/reboques/listar', $scope.dados).then(function (response) {

            let dados = response.data;

            $scope.dadosList = dados;
        });
    };
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("reboques.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: {
                descricao: { halign: 'left' },
                placa: { halign: 'left' }
            },
            body: $scope.dadosList,
            columns: [
            
                { header: 'Descrição', dataKey: 'descricao' },
                { header: 'Placa', dataKey: 'placa' },
                { header: 'Tipo', dataKey: 'tipo' },
                { header: 'Categoria', dataKey: 'categoria' },
                { header: 'Porcentagem', dataKey: 'porcentagem' },
                { header: 'Ativo', dataKey: 'desc_ativo' },
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: { top: 30 },
        });

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('reboques.pdf')
    }
    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        $location.path('reboques-editar').search({ dados });
    };
    function buscaEmpresas() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/empresas_reboques/listar').then(function (response) {

                let dados = response.data;
                dados.sort(function (a, b) {
                    if (a.nome < b.nome) {
                        return 1;
                    }
                    if (a.nome > b.nome) {
                        return -1;
                    }
                    return 0;
                });
                resolve(dados);
            });
        })

    }

});

app.controller('reboquesEditarCtrl', function ($scope, $location, $http, urlServidor, myFunctions, $filter, $sce, urlImagens, $mdDialog, buckets) {

    $scope.urlImagens = urlImagens.urlDocumentosReboque;

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }
    var bucket = null;

    var urlParams = $location.search();
    $http.get(urlServidor.urlServidorChatAdmin + '/autoridades/vercodigo').then(function (response) {

        AWS.config.update({ accessKeyId: response.data.ac, secretAccessKey: response.data.sc });
        AWS.config.region = response.data.regiao;
    }).finally(function () {
        bucket = new AWS.S3();
        /*if (urlParams.dados[0].pdf_clrv != '' && urlParams.dados[0].pdf_clrv != null ) {
            bucket.getObject(
                    {Bucket: "documentos-reidospatios", Key: urlParams.dados[0].pdf_clrv, ResponseContentDisposition: 'inline'},
                    function (error, data) {
                        if (error != null) {
                            myFunctions.showAlert("Falha no carregamento do PDF: " + error);
                        } else {
                            $scope.pdfCLRV = new Blob([data.Body], {type: "application/pdf"});
                        }
                    }
            );
        }

        if (urlParams.dados[0].pdf_seguro != '' && urlParams.dados[0].pdf_seguro != null ) {
            bucket.getObject(
                    {Bucket: "documentos-reidospatios", Key: urlParams.dados[0].pdf_seguro, ResponseContentDisposition: 'inline'},
                    function (error, data) {
                        if (error != null) {
                            myFunctions.showAlert("Falha no carregamento do PDF: " + error);
                        } else {
                            $scope.pdfSeguro = new Blob([data.Body], {type: "application/pdf"});
                        }
                    }
            );
        }*/

    });
    

    $scope.dados = {
        id: urlParams.dados[0].id,
        empresa: urlParams.dados[0].id_empresa_reboque,
        descricao: urlParams.dados[0].descricao,
        placa: urlParams.dados[0].placa,
        tipo: urlParams.dados[0].tipo,
        categoria: urlParams.dados[0].categoria,
        ativo: urlParams.dados[0].ativo,
        observacao: urlParams.dados[0].observacao,
        vencimento_seguro: $filter('date')(urlParams.dados[0].vencimento_seguro, 'dd/MM/yyyy', "+0000"),
        vencimento_clrv: $filter('date')(urlParams.dados[0].vencimento_clrv, 'dd/MM/yyyy', "+0000"),
        pdf_seguro: urlParams.dados[0].pdf_seguro,
        pdf_clrv: urlParams.dados[0].pdf_clrv,
        ano_fabricacao: urlParams.dados[0].ano_fabricacao,
        ano_modelo: urlParams.dados[0].ano_modelo,
        chassi: urlParams.dados[0].chassi,
        nome_proprietario: urlParams.dados[0].nome_proprietario,
        combustivel: urlParams.dados[0].combustivel,
        cor: urlParams.dados[0].cor
    }

    $scope.arquivoCLRV = '';
    $scope.arquivoSeguro = '';
    $scope.uploadCLRV = (file) => {
        $scope.arquivoCLRV = file;
        $scope.dados.pdf_clrv = file.name;
        $scope.pdfCLRV = file;
        if ($scope.arquivoCLRV.name != undefined) {

            var params = {
                Key: $scope.arquivoCLRV.name,
                ContentType: $scope.arquivoCLRV.type,
                ContentDisposition: 'inline',
                Body: $scope.arquivoCLRV,
                ACL: 'public-read',
                Bucket: buckets.reboques

            };
            $scope.arquivoCLRV = null;
            $scope.dados.pdf_clrv = null;
            $scope.pdfCLRV = null;
            bucket.putObject(params, function (err, data) {
                if (err) {
                    myFunctions.showAlert('Erro envio PDF da CLRV, verifique...' + err);
                } else {
                    setTimeout(function () {
                        myFunctions.showAlert('Arquivo alterado!');

                    }, 1500);
                    $scope.arquivoCLRV = file;
                    $scope.dados.pdf_clrv = file.name;
                    $scope.pdfCLRV = file;
                }

            });

        };
    };

    $scope.uploadSeguro = (file) => {
        $scope.arquivoSeguro = file;
        $scope.dados.pdf_seguro = file.name;
        $scope.pdfSeguro = file;
        if ($scope.arquivoSeguro.name != undefined) {


            var paramsSeguro = {
                Key: $scope.arquivoSeguro.name,
                ContentType: $scope.arquivoSeguro.type,
                ContentDisposition: 'inline',
                Body: $scope.arquivoSeguro,
                ACL: 'public-read',
                Bucket: buckets.reboques

            };
            $scope.arquivoSeguro = null;
            $scope.dados.pdf_seguro = null;
            $scope.pdfSeguro = null;
            bucket.putObject(paramsSeguro, function (err, data) {
                if (err) {
                    myFunctions.showAlert('Erro envio PDF Seguro, verifique...' + err);
                } else {
                    setTimeout(function () {
                        myFunctions.showAlert('Arquivo alterado!');

                    }, 1500);

                    $scope.arquivoSeguro = file;
                    $scope.dados.pdf_seguro = file.name;
                    $scope.pdfSeguro = file;

                }
            });

        }
    };

    $scope.limparPDFCLRV = () => {
        let confirm = $mdDialog.confirm()
            .title('CLRV')
            .textContent('Tem certeza que deseja apagar esse documento?')
            .ariaLabel('Lucky day')
            .ok('Apagar')
            .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {

            var params = { Bucket: buckets.reboques, Key: $scope.dados.pdf_clrv };

            bucket.deleteObject(params, function (err, data) {
                if (err) {
                    myFunctions.showAlert("Não foi possivel apagar o arquivo!");
                }  // error
                else {

                    $scope.arquivoCLRV = null;
                    $scope.dados.pdf_clrv = null;
                    $scope.pdfCLRV = null;
                    myFunctions.showAlert('Arquivo apagado!');

                }
            });

        });

    };

    $scope.limparPDFSeguro = () => {
        let confirm = $mdDialog.confirm()
            .title('Seguro')
            .textContent('Tem certeza que deseja apagar esse documento?')
            .ariaLabel('Lucky day')
            .ok('Apagar')
            .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {

            var params = { Bucket: buckets.reboques, Key: $scope.dados.pdf_seguro };


            bucket.deleteObject(params, function (err, data) {
                if (err) {
                    myFunctions.showAlert("Não foi possivel apagar o arquivo!");
                }  // error
                else {

                    $scope.arquivoSeguro = null;
                    $scope.dados.pdf_seguro = null;
                    $scope.pdfSeguro = null;
                    myFunctions.showAlert('Arquivo apagado!');

                }
            });
        });
    };
    buscaEmpresas().then(function (result) {
        $scope.empresas = result;
    });
    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/reboques/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    myFunctions.showAlert('Reboque já existe!')
                } else {
                    myFunctions.showAlert('Alteração executada com sucesso!');

                }

            };
        });
    };

    function buscaEmpresas() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/empresas_reboques/listar').then(function (response) {

                let dados = response.data;
                dados.sort(function (a, b) {
                    if (a.nome < b.nome) {
                        return 1;
                    }
                    if (a.nome > b.nome) {
                        return -1;
                    }
                    return 0;
                });
                resolve(dados);
            });
        })

    }



});

app.controller('tarifasCtrl', function ($scope, $location, $http, urlServidor) {

    $scope.novo = function (ev, id) {
        $location.path('tarifas-novo');
    };
    $scope.limpar = function (ev) {
        $scope.dados.descricao = '';
    };
    $scope.pesquisar = function (ev) {

        $http.get(urlServidor.urlServidorChatAdmin + '/tarifas/itens-tarifas', { params: { idEmpresa: localStorage.getItem('id_empresa'), descricao: $scope.dados.descricao } }).then(function (response) {

            $scope.dadosList = response.data;
        });
    };
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("tarifas.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: {
                descricao: { halign: 'left' }
            },
            body: $scope.dadosList,
            columns: [
            
                { header: 'Descrição', dataKey: 'descricao' },
                
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: { top: 30 },
        });

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('tarifas.pdf')
    }
    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id === id
        })
        $location.path('tarifas-editar').search({ dados });
    };
});
app.controller('tarifasEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep) {

    var urlParams = $location.search();
    $scope.dados = {
        idTarifa: urlParams.dados[0].id,
        descricao: urlParams.dados[0].descricao,
        idEmpresa: urlParams.dados[0].id_empresa,
        valorEstadia: 0,
        valorGuinchoDia: 0,
        valorGuinchoNoite: 0,
        limiteKm: 0,
        valorKm: 0,
        max: 0,
        idEmpresa: localStorage.getItem('id_empresa')
    };

    $http.get(urlServidor.urlServidorChatAdmin + '/tipos_veiculo/listar', {
        params: {
            idEmpresa: $scope.dados.idEmpresa
        }
    }).then(function (response) {
        $scope.tiposVeiculos = response.data;
    });

    $http.get(urlServidor.urlServidorChatAdmin + '/tarifas/listar', { params: { descricao: $scope.dados.descricao, idEmpresa: $scope.dados.idEmpresa } }).then(function (response) {

        if (response.data.length === 0) {
            $scope.dadosTarifas = [{ estadia: "", guincho_dia: "", guincho_noite: "", valor_km: "", id: 0, id_tarifa: $scope.dados.idTarifa }]
        } else {
            $scope.dadosTarifas = response.data;
        }

    });
    $scope.isDisabled = false;
    $scope.editar = function ($event, rowId) {

        let pos = $scope.dadosTarifas.map(function (e) {
            return e.id;
        });
        let itemPosicao = pos.indexOf(rowId);
        $scope.dados.valorEstadia = $scope.dadosTarifas[itemPosicao].estadia;
        $scope.dados.valorGuinchoDia = $scope.dadosTarifas[itemPosicao].guincho_dia;
        $scope.dados.valorGuinchoNoite = $scope.dadosTarifas[itemPosicao].guincho_noite;
        $scope.dados.limiteKm = $scope.dadosTarifas[itemPosicao].limite_km;
        $scope.dados.valorKm = $scope.dadosTarifas[itemPosicao].valor_km;
        $scope.dados.maxEstadias = $scope.dadosTarifas[itemPosicao].max_estadia;
        $scope.dados.idTipoVeiculo = $scope.dadosTarifas[itemPosicao].id_tipo_veiculo;
        $scope.dados.idTarifaDetalhe = $scope.dadosTarifas[itemPosicao].id;
        $scope.isDisabled = false;
    };
    $scope.novoItem = function ($event, rowId) {

        $scope.dados.idTarifaDetalhe = '';
        $scope.dados.valorEstadia = ''
        $scope.dados.valorGuinchoDia = ''
        $scope.dados.valorGuinchoNoite = ''
        $scope.dados.limiteKm = ''
        $scope.dados.valorKm = ''
        $scope.dados.maxEstadias = ''
        $scope.dados.idTipoVeiculo = 0

        $scope.isDisabled = false;
    };
    $scope.gravaItem = function () {


        if ($scope.dados.idTarifaDetalhe) {

            $http.post(urlServidor.urlServidorChatAdmin + '/tarifas/alterar-tarifadetalhes', $scope.dados).then(function (response) {

                if (response.data.code) {

                    myFunctions.showAlert('Erro na gravação!')

                } else {

                    $http.get(urlServidor.urlServidorChatAdmin + '/tarifas/listar', { params: { descricao: $scope.dados.descricao, idEmpresa: $scope.dados.idEmpresa } }).then(function (response) {
                        $scope.dadosTarifas = response.data;
                        $scope.dados.valorEstadia = 0
                        $scope.dados.valorGuinchoDia = 0
                        $scope.dados.valorGuinchoNoite = 0
                        $scope.dados.limiteKm = 0
                        $scope.dados.valorKm = 0
                        $scope.dados.maxEstadias = 0
                        $scope.dados.idTipoVeiculo = 0
                    });
                    $scope.isDisabled = true;
                    myFunctions.showAlert('Alteração executada com sucesso!');
                }
                ;
            });
        } else {

            $http.post(urlServidor.urlServidorChatAdmin + '/tarifas/cadastrar-tarifadetalhes', $scope.dados).then(function (response) {

                if (response.data.insertId) {

                    $http.get(urlServidor.urlServidorChatAdmin + '/tarifas/listar', { params: { descricao: $scope.dados.descricao, idEmpresa: $scope.dados.idEmpresa } }).then(function (response) {
                        $scope.dadosTarifas = response.data;
                        $scope.dados.valorEstadia = 0
                        $scope.dados.valorGuinchoDia = 0
                        $scope.dados.valorGuinchoNoite = 0
                        $scope.dados.limiteKm = 0
                        $scope.dados.valorKm = 0
                        $scope.dados.maxEstadias = 0
                        $scope.dados.idTipoVeiculo = 0
                    });
                    $scope.isDisabled = true;
                    myFunctions.showAlert('Cadastro executado com sucesso!');
                } else {

                    myFunctions.showAlert('Erro na gravação!')

                }
                ;
            });
        }

    };
});
app.controller('tiposVeiculoCtrl', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.novo = function (ev, id) {
        $location.path('tiposveiculo-novo');
    };
    $scope.limpar = function (ev) {
        $scope.dados.descricao = '';
    };
    $scope.pesquisar = function (ev, txt) {

        $http.get(urlServidor.urlServidorChatAdmin + '/tipos_veiculo/listar', { params: { descricao: $scope.dados.descricao, idEmpresa: localStorage.getItem('id_empresa') } }).then(function (response) {

            let dados = response.data;
            $scope.dadosList = dados;
        });
    };
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("tipos-veiculos.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: {
                descricao: { halign: 'left' }
            },
            body: $scope.dadosList,
            columns: [
            
                { header: 'Descrição', dataKey: 'descricao' },
                
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: { top: 30 },
        });

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('tipos-veiculos.pdf')
    }

    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        $location.path('tiposveiculo-editar').search({ dados });
    };
});
app.controller('tiposVeiculoEditarCtrl', function ($scope, $http, $location, myFunctions, urlServidor, estados, viaCep) {

    var urlParams = $location.search();
    $scope.dados = {
        id: urlParams.dados[0].id,
        descricao: urlParams.dados[0].descricao
    }

    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/tipos_veiculo/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    myFunctions.showAlert('Tarifa já existe!')
                } else {
                    myFunctions.showAlert('Alteração executada com sucesso!');
                }

            }
            ;
        });
    };
});
app.controller('permissoesCtrl', function ($scope, $location) {
    $scope.list = [
        {
            id: 1,
            nome: 'Novo registro'
        },
        {
            id: 2,
            nome: 'Prodyto DAO'
        },
        {
            id: 3,
            nome: 'Carta registrada'
        },
        {
            id: 4,
            nome: 'New registro'
        }
    ];
    $scope.detalhe = function (ev, id) {
        $location.path('permissoes-editar');
    };
});
app.controller('permissoesEditarCtrl', function ($scope) {
    $scope.permissoes = { menu: 1, nome: 'ADEMIR.FERREIRA' };
    $scope.list = [
        {
            id: 1,
            menu: 'CADASTROS',
            aplicacao: 'AUTORIDADES',
            tipo: 'MENU'
        },
        {
            id: 2,
            menu: 'CADASTROS',
            aplicacao: 'CONFIGURAÇÕES',
            tipo: 'MENU'
        },
        {
            id: 3,
            menu: 'CADASTROS',
            aplicacao: 'EMPRESAS REBOQUE',
            tipo: 'MENU'
        },
        {
            id: 4,
            menu: 'CADASTROS',
            aplicacao: 'MOTORISTAS',
            tipo: 'MENU'
        },
        {
            id: 5,
            menu: 'CADASTROS',
            aplicacao: 'PATIOS',
            tipo: 'MENU'
        },
        {
            id: 6,
            menu: 'CADASTROS',
            aplicacao: 'PERMISSOES',
            tipo: 'MENU'
        },
        {
            id: 7,
            menu: 'CADASTROS',
            aplicacao: 'REBOQUES',
            tipo: 'MENU'
        },
        {
            id: 8,
            menu: 'CADASTROS',
            aplicacao: 'USUARIOS',
            tipo: 'MENU'
        }
    ];
});
app.controller('usuariosCtrl', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.novo = function (ev, id) {
        $location.path('usuarios-novo');
    };
    $scope.usuario = {
        nome: '',
        tivo: ''
    }

    $scope.limpar = function (ev) {
        $scope.usuario.patio = '';
        $scope.usuario.nome = '';
        $scope.usuario.ativo = '';
    };
    buscaPatios().then(function (result) {
        $scope.patios = result;
    });
    $scope.pesquisar = function (ev) {

        $http.get(urlServidor.urlServidorChatAdmin + '/usuarios/listar', { params: { nome: $scope.usuario.nome, patio: $scope.usuario.patio, ativo: $scope.usuario.ativo } }).then(function (response) {

            let dados = response.data;
            if ($scope.dados.ordenacao == 'asc') {

                dados.sort(function (a, b) {
                    if (a.nome > b.nome) {
                        return 1;
                    }
                    if (a.nome < b.nome) {
                        return -1;
                    }
                    // a must be equal to b
                    return 0;
                });
            } else {

                dados.sort(function (a, b) {
                    if (a.nome < b.nome) {
                        return 1;
                    }
                    if (a.nome > b.nome) {
                        return -1;
                    }
                    return 0;
                });
            }

            $scope.dadosList = dados;
        });
    };
    $scope.detalhe = function (ev, id) {
        var dados = $scope.dadosList.filter(function (obj) {
            return obj.id == id
        })
        console.log({dados})
        $location.path('usuarios-editar').search({ dados });
    };
    function buscaPatios() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar').then(function (response) {

                resolve(response.data);
            });
        });
    }

    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("usuarios.xlsx",{headers:true}) FROM ?', [$scope.dadosList]);
    }

    $scope.PDF = () =>{



            var doc = new jsPDF({ orientation: "landscape" });
            var totalPagesExp = '{total_pages_count_string}'

            doc.setFontSize(10);
            doc.autoTable({

                columnStyles: {
                    vencimento: { halign: 'left' },
                },

                body: $scope.dadosList,
                columns: [
                    { header: 'Nome', dataKey: 'nome' },
                    { header: 'Login', dataKey: 'login' },
                    { header: 'Tipo', dataKey: 'tipo' },
                    { header: 'Ativo', dataKey: 'desc_ativo' },
                    { header: 'Cargo', dataKey: 'cargo' },


                ],    bodyStyles: {
                    margin: 10,
                    fontSize: 08,
                },
                didDrawPage: function (data) {
                    // Header
                    doc.setFontSize(18)
                    doc.setTextColor(40)
                    doc.setFontStyle('normal')


                    // Footer
                    var str = 'Pagina ' + doc.internal.getNumberOfPages()
                    // Total page number plugin only available in jspdf v1.0+
                    if (typeof doc.putTotalPages === 'function') {
                        str = str + ' de ' + totalPagesExp
                        var totalpaginas = totalPagesExp;
                    }

                    doc.setFontSize(10)

                    // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                    var pageSize = doc.internal.pageSize
                    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()

                },
                margin: { top: 30 },
            });
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp)
            }

            doc.save('relatorio-usuários.pdf');





    }
});




app.controller('relatorioLiberacoesSec', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.dados = {
        DataInicio: null,
        DataFinal: null,
        patios: null
    }

    let hoje = new Date()
    $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
    $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    $scope.mesAtual = function (ev, id) {
        $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
        $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    };
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("relatorio-liberacoes-sec.xlsx",{headers:true}) FROM ?', [$scope.dadosLiberacoes]);
    }

    $scope.gerarPdf = function () {
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: {
                tipo_veiculo: { halign: 'center' },
                valor_guincho: { halign: 'center' },
                valor_estadia: { halign: 'center' },
                total_diarias: { halign: 'center' },
                desconto: { halign: 'center' },
                acrescimo: { halign: 'center' },
                total: { halign: 'center' },
                estadias: { halign: 'center' }
            },
            body: $scope.dadosLiberacoes,
            columns: [
                { header: 'NCV', dataKey: 'id' },
                { header: 'Placa', dataKey: 'placa' },
                { header: 'Tipo Veiculo', dataKey: 'tipo_veiculo' },
                { header: 'Liberação', dataKey: 'data_liberacao' },
                { header: 'Apreensão', dataKey: 'data_apreensao' },
                { header: 'Guincho', dataKey: 'valor_guincho' },
                { header: 'Diaria', dataKey: 'valor_estadia' },
                { header: 'Diarias', dataKey: 'estadias' },
                { header: 'Total Diarias', dataKey: 'total_diarias' },
                { header: 'Desconto', dataKey: 'desconto' },
                { header: 'Acrescimo', dataKey: 'acrescimo' },
                { header: 'Recebido', dataKey: 'total' },
                { header: 'Patio', dataKey: 'patio' },
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                doc.text('Liberações no periodo de ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22)

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: { top: 30 },
        });

        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('liberacoes-seccional.pdf')
    }


    $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') }).then(function (response) {
        $scope.patios = response.data;
    });

    $scope.pesquisar = function (param1) {

        $http.get(urlServidor.urlServidorChatAdmin + '/ncv/relatorio-liberacoes', { params: $scope.dados }).then(function (response) {
            $scope.dadosLiberacoes = response.data[0];
        });
    };
});



app.controller('pesquisaGeral', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.dados = {
        DataInicio: null,
        DataFinal: null,
        patios: null
    }

    let hoje = new Date()
    $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
    $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    $scope.mesAtual = function (ev, id) {
        $scope.dados.dataInicio = new Date(hoje.getFullYear(), hoje.getMonth(), 01);
        $scope.dados.dataFinal = new Date(hoje.getFullYear(), hoje.getMonth() + 1, 0);
    };
    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("relatorio-apreensoes.xlsx",{headers:true}) FROM ?', [$scope.dadosApreensoes]);
    }

    $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') }).then(function (response) {
        $scope.patios = response.data;
    });
    $scope.pesquisar = function (param1) {

        $http.get(urlServidor.urlServidorChatAdmin + '/ncv/relatorio-apreensoes', { params: $scope.dados }).then(function (response) {
            $scope.dadosApreensoes = response.data;
        });
    };
    $scope.gerarPdf = function () {
        var doc = new jsPDF({ orientation: "landscape" });
        var totalPagesExp = '{total_pages_count_string}'

        doc.setFontSize(12);
        doc.autoTable({

            columnStyles: { tipo_veiculo: { halign: 'left' } },
            body: $scope.dadosApreensoes,
            columns: [
                { header: 'NCV', dataKey: 'id' },
                { header: 'Placa', dataKey: 'placa' },
                { header: 'Tipo Veiculo', dataKey: 'tipo_veiculo' },
                { header: 'Apreensão', dataKey: 'data_apreensao' },
                { header: 'Marca/Modelo', dataKey: 'marca_modelo' },
                { header: 'Ano', dataKey: 'ano' },
                { header: 'Cor', dataKey: 'cor' },
                { header: 'Patio', dataKey: 'patio' }
            ],
            bodyStyles: {
                margin: 10,
                fontSize: 08,
            },
            didDrawPage: function (data) {
                // Header
                doc.setFontSize(18)
                doc.setTextColor(40)
                doc.setFontStyle('normal')

                doc.text('Apreensões no periodo de ' + $scope.dados.dataInicio.toLocaleDateString('pt-BR') + ' a ' + $scope.dados.dataFinal.toLocaleDateString('pt-BR'), data.settings.margin.left + 15, 22)

                // Footer
                var str = 'Pagina ' + doc.internal.getNumberOfPages()
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + ' de ' + totalPagesExp
                }

                doc.setFontSize(10)

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize
                var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
                doc.text(str, data.settings.margin.left, pageHeight - 10)
            },
            margin: { top: 30 },
        });
        if (typeof doc.putTotalPages === 'function') {
            doc.putTotalPages(totalPagesExp)
        }

        doc.save('apreensoes.pdf')
    }


});
app.controller('relatorioEstoque', function ($scope, $location, $http, urlServidor, myFunctions) {

    $scope.dados = {
        patios: null
    }

    $scope.gerarPlanilha = function () {

        alasql('SELECT * INTO XLSX("relatorio-estoque.xlsx",{headers:true}) FROM ?', [$scope.dadosEstoque]);
    }

    $http.put(urlServidor.urlServidorChatAdmin + '/patios/listar-patiosusuario', { idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') }).then(function (response) {
        $scope.patios = response.data;
    });
    $scope.pesquisar = function (param1) {

        $http.get(urlServidor.urlServidorChatAdmin + '/ncv/relatorio-estoque', { params: $scope.dados }).then(function (response) {
            $scope.dadosEstoque = response.data;
            $scope.sum = $scope.dadosEstoque.reduce(function (prevVal, elem) {
                return prevVal + elem.estoque;

            }, 0);
        });
    };
});
app.directive('mdtCustomCellButton', function () {
    return {
        scope: {
            icon: '@'
        },
        template: '<md-button class="md-icon-button md-secondary" aria-label="Button"><md-icon>{{icon}}</md-icon></md-button>',
    };
});
app.directive('apsUploadFile', apsUploadFile);

function apsUploadFile() {
    var directive = {
        restrict: 'E',
        template: '<input id="fileInput" type="file" class="ng-hide"> <md-button id="uploadButton" class="md-raised md-primary" aria-label="attach_file">Anexar</md-button><md-input-container  md-no-float>    <input id="textInput" ng-model="fileName" type="text" placeholder="Nenhum Arquivo" ng-readonly="true"></md-input-container>',
        link: apsUploadFileLink
    };
    return directive;
}
function apsUploadFileLink(scope, element, attrs) {
    var input = $(element[0].querySelector('#fileInput'));
    var button = $(element[0].querySelector('#uploadButton'));
    var textInput = $(element[0].querySelector('#textInput'));
    if (input.length && button.length && textInput.length) {
        button.click(function (e) {
            input.click();
        });
        textInput.click(function (e) {
            input.click();
        });
    }

    input.on('change', function (e) {
        var files = e.target.files;
        if (files[0]) {
            scope.fileName = files[0].name;
        } else {
            scope.fileName = null;
        }
        scope.$apply();
    });
}
