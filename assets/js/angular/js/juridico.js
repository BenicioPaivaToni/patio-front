angular
  .module("app")
  .controller(
    "juridicoCtrl",
    function ($scope, $location, $http, urlServidor, myFunctions, $mdDialog) {
      $scope.advogado = {
        id_advogado: null,
        ncv: null,
        tipo: null,
      };

      $scope.limparPesquisaNcv = () => {
        $scope.advogado.ncv = null;
      };

      $scope.OptionsType = ["RENAJUD", "JURIDICO", "ADMINISTRATIVO"];

      $scope.usuario = localStorage.getItem("tipo_usuario");

      $http
        .put(urlServidor.urlServidorChatAdmin + "/advogados/listar")
        .then(
          ({ data }) => ($scope.advogados = data.filter((item) => item.status))
        );

      $scope.veiculosList = [];

      $scope.buscaNcv = async () => {
        if ($scope.advogado.id_advogado == null) {
          myFunctions.showAlert(
            `Informe um advogado antes de adicionar veículo(s)!`
          );
          return;
        }

        if ($scope.advogado.ncv == null) {
          myFunctions.showAlert(`Informe um NCV ou PLACA para pesquisa!`);
          return;
        }

        const verificaExitente = $scope.veiculosList.some(
          (obj) => obj.ncv == $scope.advogado.ncv
        );

        if (verificaExitente == true) {
          myFunctions.showAlert(
            `O NCV ${$scope.advogado.ncv} já encontra-se na lista de veículos! Verifique!`
          );
          return;
        }

        let { data } = await $http.get(
          `${urlServidor.urlServidorChatAdmin}/advogados/busca-ncv-placa`,
          {
            params: {
              ncv_placa: $scope.advogado.ncv,
            },
          }
        );

        if ("tipo" in $scope.advogado) {
          data[0].tipo = $scope.advogado.tipo;
        }

        if (data.length > 1) {
          myFunctions.showAlertCustom(
            "Veiculo com placa " +
              $scope.advogado.ncv +
              " possui mais de um NCV associado!<br/><br/>Verifique cadastro ou digite o NCV específico referente à movimentação deste veiculo!"
          );
          return;
        }

        if (!Boolean(data[0])) {
          myFunctions.showAlert(
            `Veiculo ${$scope.advogado.ncv} não encontrato! Verifique se digitou corretamente!`
          );
          return;
        } else {
          const txt = `
                    <div>
                        <header>
                            
                            <title>
                                    
                            </title>
                            
                        </header>
                        <br/>
                            
                        <body>
                        
                          <div> 
                            <div>TIPO:</div>
                            <div><strong>${data[0].tipo}</strong></div>
                          </div>
                          <br/>

                          <div> 
                              <div>NCV:</div>
                              <div><strong>${data[0].ncv}</strong></div>
                            </div>
                            <br/>
      
                            <div> 
                                <div>PLACA:</div>
                                <div><strong>${data[0].placa}</strong></div>
                            </div>
                            <br/>

                            <div> 
                            <div>MARCA/MODELO:</div>
                            <div><strong>${data[0].marca_modelo}</strong></div>
                            </div>

                            <br/>

                            <div> 
                                <div>ANO:</div>
                                <div><strong>${data[0].ano || 0}</strong></div>
                            </div>
                            <br/> 

                            <div> 
                                <div>MOTOR:</div>
                                <div><strong>${
                                  data[0].numero_motor || 0
                                }</strong></div>
                            </div>
                            <br/>
                                    
                            <div> 
                                <div>CHASSI:</div>
                                <div><strong>${
                                  data[0].chassi || 0
                                }</strong></div>
                            </div>
                            <br/>
                        </body>
                                
                    </div>
                            
                `;

          let confirm = $mdDialog
            .confirm()
            .ariaLabel("Lucky day")
            .title("Confira os dados do veículo")
            .ok("ADICIONAR")
            .cancel("Cancelar")
            .htmlContent(txt);
          $mdDialog.show(confirm).then(() => {
            $scope.veiculosList.push(data[0]);

            $scope.limparPesquisaNcv();
          });
        }
      };

      $scope.nomeBotao = "SELECIONAR TODOS";

      $scope.marcarDesmarcarTodos = function (veiculosList) {
        if ($scope.nomeBotao == "SELECIONAR TODOS") {
          veiculosList.forEach((dados) => {
            dados.selecionado = true;
          });

          $scope.nomeBotao = "DESMARCAR TODOS";
        } else {
          veiculosList.forEach((dados) => {
            dados.selecionado = false;
          });
          $scope.nomeBotao = "SELECIONAR TODOS";
        }
      };

      $scope.gravarSelecionados = async function (veiculosList) {
        const dados = veiculosList.filter((obj) => obj.selecionado);

        if (dados.length > 0) {
          $scope.ShowMessage = true;

          for (let [index, item] of dados.entries()) {
            $scope.message = `Gravando ncv ${item.ncv} - veiculo ${
              index + 1
            } de ${dados.length}. Por favor aguarde...`;

            await new Promise((resolve, reject) => {
              setTimeout(resolve, 1000);
            }).then(async () => {
              await $http
                .post(
                  urlServidor.urlServidorChatAdmin + "/advogados/alterar-ncv",
                  {
                    ncv: item.ncv,
                    id_advogado: $scope.advogado.id_advogado,
                  }
                )
                .then(({ data }) => {
                  $http.post(
                    urlServidor.urlServidorChatAdmin + "/ncv/alterar-tipo/",
                    {
                      tipo: item.tipo,
                      ncv: item.ncv,
                      historico: {
                        tipo: "JURIDICO",
                        descricao: "ENCAMINHAMENTO DE NCV PARA ADVOGADO",
                      },
                      usuarioId: localStorage.getItem("nome_usuario"),
                    }
                  );

                  if (data.code) {
                    myFunctions.showAlert("Erro na gravação!");
                  }
                });
            });
          }

          $scope.ShowMessage = false;

          myFunctions.showAlert("Gravação finalizada com sucesso!");

          $scope.veiculosList = [];

          $scope.nomeBotao = "SELECIONAR TODOS";
        } else {
          return myFunctions.showAlert(
            `Nenhum veículo foi selecionado, Verifique!`
          );
        }
      };

      $scope.gerarPlanilha = () => {
        if ($scope.veiculosList.length > 0) {
          const excelAjustes = $scope.veiculosList.map((item) => ({
            ncv: item.ncv,
            placa: item.placa,
            marca_modelo: item.marca_modelo,
            tipo: item.tipo,
            status: item.status,
            situacao: item.situacao,
            classi: item.chassi,
            cor: item.cor,
            ano: item.ano,
          }));

          alasql(
            'SELECT * INTO XLSX("lista-veiculos.xlsx",{headers:true}) FROM ?',
            [excelAjustes]
          );
        } else {
          myFunctions.showAlert("Sem dados para gerar planilha! Verifique!");
        }
      };

      $scope.gerarPDF = function () {
        if ($scope.veiculosList.length > 0) {
          const PDFAjustes = $scope.veiculosList.map((item) => ({
            ncv: item.ncv,
            placa: item.placa,
            marca_modelo: item.marca_modelo,
            tipo: item.tipo,
            status: item.status,
            situacao: item.situacao,
            chassi: item.chassi,
            ano: item.ano,
            cor: item.cor,
          }));

          const buscaAdvogado = $scope.advogados.find(
            (obj) => obj.id == $scope.advogado.id_advogado
          );

          var doc = new jsPDF({ orientation: "landscape" });

          var totalPagesExp = "{total_pages_count_string}";

          doc.setFontSize(10);

          doc.autoTable({
            columnStyles: {
              vencimento: { halign: "left" },
            },

            body: PDFAjustes,
            columns: [
              { header: "NCV", dataKey: "ncv" },
              { header: "Placa", dataKey: "placa" },
              { header: "Marca/Modelo", dataKey: "marca_modelo" },
              { header: "Tipo", dataKey: "tipo" },
              { header: "Status", dataKey: "status" },
              { header: "Situação", dataKey: "situacao" },
              { header: "Chassi", dataKey: "chassi" },
              { header: "Ano", dataKey: "ano" },
              { header: "Cor", dataKey: "cor" },
            ],

            bodyStyles: {
              margin: 10,
              fontSize: 8,
            },

            didDrawPage: function (data) {
              // Header
              doc.setFontSize(18);
              doc.setTextColor(40);
              doc.setFontStyle("normal");

              doc.text(
                "Lista veículos advogado: " + buscaAdvogado.nome,
                data.settings.margin.left + 5,
                18
              );
              doc.text(
                "Quantidade: " +
                  $scope.veiculosList.length +
                  "            " +
                  "Data: " +
                  new Date().toLocaleDateString("pt-BR"),
                data.settings.margin.left + 5,
                25
              );

              // Footer
              var str = "Pagina " + doc.internal.getNumberOfPages();
              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
                var totalpaginas = totalPagesExp;
              }

              doc.setFontSize(10);

              // jsPDF 1.4+ uses getWidth, <1.4 uses .width
              var pageSize = doc.internal.pageSize;
              var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();

              doc.text(
                str + "                        ",
                data.settings.margin.left,
                pageHeight - 10
              );
            },
            margin: { top: 30 },
          });

          if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
          }

          doc.save("lista-veiculos.pdf");
        } else {
          myFunctions.showAlert("Sem dados para gerar PDF! Verifique!");
        }
      };

      //ABA LISTAR ENCAMINHADOS

      $scope.relatorio = {
        id_advogado: null,
        data_inicial: new Date(),
        data_final: new Date(),
      };

      $scope.limpar = function (ev) {
        $scope.relatorio.id_advogado = null;
        $scope.relatorio.data_inicial = new Date();
        $scope.relatorio.data_final = new Date();
        $scope.relatorioList = [];
      };

      $scope.buscaDadosRelatorio = async function () {
        $scope.relatorioList = [];

        const response = await $http.get(
          urlServidor.urlServidorChatAdmin + "/advogados/listar-ncv-advogado",
          {
            params: {
              id_advogado: $scope.relatorio.id_advogado,
              data_inicial: $scope.relatorio.data_inicial,
              data_final: $scope.relatorio.data_final,
            },
          }
        );
        console.log({ response });
        if (response.data.length == 0) {
          myFunctions.showAlert(
            "Pesquisa não encontrou registros! Revise parâmetros da pesquisa!"
          );
        } else {
          $scope.relatorioList = response.data;
        }
      };

      $scope.detalhe = function (ev, id) {
        var dados = $scope.relatorioList.filter(function (obj) {
          return obj.id == id;
        });
        window.open("/#/juridico-editar?id " + id, "_blank");
      };

      $scope.gerarPlanilhaListar = () => {
        if ($scope.relatorioList.length > 0) {
          const excelAjustes = $scope.relatorioList.map((item) => ({
            patio: item.patio,
            patio_destino: item.patio_destino,
            ncv: item.ncv,
            placa: item.placa,
            tipo_veiculo: item.tipo_veiculo,
            marca_modelo: item.marca_modelo,
            tipo: item.tipo,
            ano: item.ano,
            data_envio: item.data_envio_advogado,
          }));

          alasql(
            'SELECT * INTO XLSX("veiculos-encaminhados-advogado.xlsx",{headers:true}) FROM ?',
            [excelAjustes]
          );
        } else {
          myFunctions.showAlert("Sem dados para gerar planilha! Verifique!");
        }
      };

      $scope.gerarPDFListar = function () {
        if ($scope.relatorioList.length > 0) {
          const PDFAjustes = $scope.relatorioList.map((item) => ({
            patio: item.patio,
            ncv: item.ncv,
            placa: item.placa,
            marca_modelo: item.marca_modelo,
            tipo_veiculo: item.tipo_veiculo,
            ano: item.ano,
            tipo: item.tipo,
            data: item.data_envio_advogado,
            patio_destino: item.patio_destino,
          }));

          const buscaAdvogado = $scope.advogados.find(
            (obj) => obj.id == $scope.relatorio.id_advogado
          );

          var doc = new jsPDF({ orientation: "landscape" });

          var totalPagesExp = "{total_pages_count_string}";

          doc.setFontSize(10);

          doc.autoTable({
            columnStyles: {
              vencimento: { halign: "left" },
            },

            body: PDFAjustes,
            columns: [
              { header: "Patio", dataKey: "patio" },
              { header: "Patio Destino", dataKey: "patio_destino" },
              { header: "NCV", dataKey: "ncv" },
              { header: "Placa", dataKey: "placa" },
              { header: "Marca/Modelo", dataKey: "marca_modelo" },
              { header: "Tipo Veiculo", dataKey: "tipo_veiculo" },
              { header: "Ano", dataKey: "ano" },
              { header: "Tipo Apreensão", dataKey: "tipo" },
              { header: "Data Envio", dataKey: "data" },
            ],

            bodyStyles: {
              margin: 10,
              fontSize: 8,
            },

            didDrawPage: function (data) {
              // Header
              doc.setFontSize(18);
              doc.setTextColor(40);
              doc.setFontStyle("normal");

              doc.text(
                "Lista veículos encaminhados entre: " +
                  $scope.relatorio.data_inicial.toLocaleDateString("pt-BR") +
                  " a " +
                  $scope.relatorio.data_final.toLocaleDateString("pt-BR") +
                  "      Quantidade: " +
                  $scope.relatorioList.length,
                data.settings.margin.left + 5,
                18
              );
              doc.text(
                "Data geração: " +
                  new Date().toLocaleDateString("pt-BR") +
                  "       " +
                  "Advogado: " +
                  buscaAdvogado.nome,
                data.settings.margin.left + 5,
                25
              );

              // Footer
              var str = "Pagina " + doc.internal.getNumberOfPages();
              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === "function") {
                str = str + " de " + totalPagesExp;
                var totalpaginas = totalPagesExp;
              }

              doc.setFontSize(10);

              // jsPDF 1.4+ uses getWidth, <1.4 uses .width
              var pageSize = doc.internal.pageSize;
              var pageHeight = pageSize.height
                ? pageSize.height
                : pageSize.getHeight();

              doc.text(
                str + "                        ",
                data.settings.margin.left,
                pageHeight - 10
              );
            },
            margin: { top: 30 },
          });

          if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
          }

          doc.save("veiculos-encaminhados-advogado.pdf");
        } else {
          myFunctions.showAlert("Sem dados para gerar PDF! Verifique!");
        }
      };
    }
  );
