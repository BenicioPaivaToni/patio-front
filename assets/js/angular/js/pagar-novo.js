'use strict';
angular.module('app')
.controller("pagarNovoCtrl", function($location,Upload, S3UploadService,$scope,$http,urlServidor,myFunctions,$mdDialog,estados,viaCep, $sce, urlImagens,$rootScope,buckets) {


  $scope.urlImagemNf = urlImagens.urlImagemNf;
  $scope.trustSrc = function(src) {
        return  $sce.trustAsResourceUrl(src);
    }


  $scope.dados = {
    id_fornecedor:'',
    patio:'',
    valor:'',
    documento: '' ,
    vencimento: new Date(),
    tipo_recebimento: '',
    categoria:'',
    grupo:'',
    qtde:1,
    comentario:'' ,
    competencia:'',
    centro_custo:'',
    checkparcelas: false,
    prestador_servico: false,
    idempresa: localStorage.getItem('id_empresa'),
  }



  // Vamos esconder nossa mensagem de erro e tambem o container
  setTimeout(()=>{
  
    document.getElementById("Container").style.display = "none"
    document.getElementById("Container_cpf_cnpj").style.display = "none"

    document.getElementById("ErrorLength").style.display = "none"
    document.getElementById("ErrorLength_1").style.display = "none"

  },100)

  // Caso o usuário Clique No input porém não mude nada, corre o risco do Modal não sumir
  //Para isso chamaremos uma função caso o usuario não mude nada e desfoque o input

  $scope.HideModal = () => {
      // o Set timeout vai nos ajudar para não correr o risco de o modal fechar antes do usuario clicar
      setTimeout(() => {
          document.getElementById("Container").style.display = "none"
      }, 2000);
  }

  $scope.hideModalCpfCnpj = () => {
    // o Set timeout vai nos ajudar para não correr o risco de o modal fechar antes do usuario clicar
    setTimeout(() => {
      document.getElementById("Container_cpf_cnpj").style.display = "none"
    }, 2000);
  }


  $scope.buscaFornecedorCpfCnpj = async () => {

    const length = $scope.dados.cpf_cnpj.length
    //Somente com 3 letras iremos iniciar a pesquisa
    // Tendo 3 letras o container vai aparecer e o erro some
    if (length >= 3) {
      document.getElementById("Container_cpf_cnpj").style.display = "block"
      document.getElementById("ErrorLength_1").style.display = "none"

      const response = await $http.get(urlServidor.urlServidorChatAdmin + '/fornecedores/busca-cpfcnpj', { params: { cpf_cnpj: $scope.dados.cpf_cnpj }})
      let dados = response.data;
      $scope.listaFornecedores = dados;
       
    } else if (length < 3) {
      document.getElementById("ErrorLength_1").style.display = "block"
      document.getElementById("Container_cpf_cnpj").style.display = "none"
        // se estiver vazio o container some
    } else if ($scope.dados.cpf_cnpj === '') {
      document.getElementById("Container_cpf_cnpj").style.display = "none"

    }
  }



  $scope.buscarClientes = async () => {

      const length = $scope.dados.nome.length
      //Somente com 3 letras iremos iniciar a pesquisa
      // Tendo 3 letras o container vai aparecere e o erro some
      if (length >= 3) {
          document.getElementById("Container").style.display = "block"
          document.getElementById("ErrorLength").style.display = "none"

          // fazemos a chamda e retornamos os dados
          const response = await $http.put(urlServidor.urlServidorChatAdmin + '/fornecedores/listar', { razao_social: $scope.dados.nome })
          let dados = response.data;
          $scope.clientes = dados;
          //se for menor que 3 o modal some e o erro aparece
      } else if (length < 3) {
          document.getElementById("ErrorLength").style.display = "block"
          document.getElementById("Container").style.display = "none"
          // se estiver vazio o container some
      } else if ($scope.dados.nome === '') {
          document.getElementById("Container").style.display = "none"

      }


  }


  $scope.ChangeScope = (clientes) => {
      // Vamos substituir o inupt text, visto que não precisamos mais do container ele receber um none
      if (clientes) {

        $scope.DadosGeraisClientes = clientes
        $scope.dados.nome = clientes.razao_nome

        const cpfCnpj = $scope.fornecedores.find((item) => item.razao_nome === $scope.dados.nome).cpf_cnpj
     
        $scope.dados.cpf_cnpj = cpfCnpj

        document.getElementById("Container").style.display = "none"
        document.getElementById("Container_cpf_cnpj").style.display = "none"
          
      } else {
        $scope.DadosGeraisClientes = false
      }
  }


  $scope.changeScopeCpfCnpj = (cpf_cnpj) => {
    // Vamos substituir o inupt text, visto que não precisamos mais do container ele receber um none
    if (cpf_cnpj) {
        $scope.DadosGeraisClientes = clientes
        $scope.dados.nome = clientes.razao_nome
        document.getElementById("cpf_cnpj").style.display = "none"
    } else {
        $scope.DadosGeraisClientes = false

    }
  }



  $scope.idpagar = null;
  $scope.listaDocumentos = [] ;

  function geraParcelas(item,qtde){

		return new Promise((resolve, reject) => {

      var inserts = [] ;
      var dataVenc =  new Date( item.vencimento );
      var competencia =  new Date( item.vencimento );
      var _competencia = item.competencia ;

      dataVenc = new Date( dataVenc.setMonth( dataVenc.getMonth() - 1 ) ) ;

      competencia = new Date( competencia.setMonth( competencia.getMonth() - 1 ) ) ;

      var i;
      for (i = 0; i < item.qtde ; i++) {

				inserts.push({ 
          id_fornecedor: item.id_fornecedor, 
          vencimento: dataVenc, 
          cadastro: new Date(),
          valor: item.valor, 
          categoria: item.categoria, 
          tipo_recebimento: item.tipo_recebimento,
          documento: item.documento+'-'+(i+1)+'/'+item.qtde,
          comentario: item.comentario, 
          grupo: item.grupo, 
          id_empresa: item.idempresa,
          competencia: _competencia, 
          id_patio: item.patio,
          centro_custo: item.centro_custo,
          prestador_servico: item.prestador_servico === true ? 1 : 0
          
        })

        dataVenc = new Date( dataVenc.setMonth( dataVenc.getMonth() + 1 ) ) ;
        competencia = new Date( competencia.setMonth( competencia.getMonth() + 1 ) ) ;

			};

      setTimeout(() => {
        resolve(inserts) ;
      }, 1000);

		});

  }


  buscaCentroCusto()

  buscaFornecedores().then( function(result){
     $scope.fornecedores = result ;
  });

  buscaPatios().then( function(result){
   
    $scope.patios = result.filter((item) => {
      return item.status == 1
    })

  });


  buscaGrupos().then( function(result){
    $scope.grupos = result.filter((item) => {
      return item.ativo == 1
    })
  });

  buscaTipoRecebimento().then( function(result){
    $scope.tiposRecebimentos = result ;
  });

  buscaCategorias().then( function(result){
    $scope.categorias = result.filter((item) => {
      return item.ativo == 1
    })
  });


  /*
  $scope.SelecionaCategoria = function(idgrupo){
    buscaCategorias(idgrupo).then( function(result){
      $scope.categorias = result ;
    });
  }

  */


  $scope.isDisabled = false

  $scope.gravar = function() {
    if ($scope.DadosGeraisClientes) {

      $scope.dados.id_fornecedor = $scope.DadosGeraisClientes.id;
    } else {
      $scope.dados;
    }
    if( $scope.dados.checkparcelas && $scope.dados.qtde > 1){

      geraParcelas( $scope.dados ).then( function(response){

        $scope.dados_parcelas = response;
        $scope.parcelas = $scope.dados_parcelas[0];
        $scope.parcelas = {
          vencimento: '',
          valor: '',
          competencia:''
        }


        $mdDialog.show({ controller: function ($scope, $mdDialog, $mdToast) {

          $scope.gravarParcelas = function () {
               
            $http.post(urlServidor.urlServidorChatAdmin+'/pagar/cadastrar-parcelas', $scope.dados_parcelas ).then( function(response){
              
              if (response.data.code){

                myFunctions.showAlert('Erro na gravação!')

              }else{

                if ( response.data == '11000' )
                  {
                    myFunctions.showAlert('Pagamento já existe!')
                  }else
                  {
                    myFunctions.showAlert('Pagamento parcelado cadastrado com sucesso!');
                    $mdDialog.hide();
                    $scope.isDisabled = true ;
                   // $scope.idpagar = response.data.insertId;
                   
                   let idPagarParcelas = response.data
                   
                   let idpagar = idPagarParcelas.shift()
                   
                   $scope.idpagar = idpagar.id
                  
                   $scope.idPagarParcelas = idPagarParcelas

                  }
                
                }
              });
            }

            $scope.cancel = function () {//botão fechar
              $mdDialog.cancel();
              $scope.isDisabled = false ;
            };

          },
            templateUrl: 'ajustes-parcelas.html',
            scope:$scope,
            preserveScope: true,
            scopoAnterior: $scope
        }).then(function (answer) {
          let a = answer;

        }, function () {
          $scope.statusdialog = 'You cancelled the dialog.';
        });

      });

    } else {

      $scope.dados.prestador_servico = $scope.dados.prestador_servico === true ? 1 : 0
      
      $http.post(urlServidor.urlServidorChatAdmin+'/pagar/cadastrar', $scope.dados ).then( function(response){

        if (response.data.code){

          myFunctions.showAlert('Erro na gravação!')

        }else{

          if ( response.data == '11000' )
          {
            myFunctions.showAlert('Lançamento já existe!')
          }else
          {

            myFunctions.showAlert( 'Cadastro executado com sucesso!' );
            $scope.isDisabled = true ;
            $scope.idpagar = response.data.insertId;

          }

        };

      });

    }

  };




  $scope.apagarDocumento = (dados) => {
    let idimagem = dados.id;
    let confirm = $mdDialog.confirm()
    .title('Documentos!')
    .textContent('Tem certeza que deseja apagar esse documento?')
    .ariaLabel('Lucky day')
    .ok('Apagar')
    .cancel('Cancelar');
    $mdDialog.show(confirm).then(function () {

      $http.post(urlServidor.urlServidorChatAdmin + '/pagar/apaga-documento', {id: idimagem}).then(function (response){
        if(response.data.code){
          myFunctions.showAlert('Não foi possível apagar o documento !!');
        }else {
          myFunctions.showAlert('Documento apagado !!');
          $http.get(urlServidor.urlServidorChatAdmin + '/pagar/busca-documentospagar', {params: {id_pagar: $scope.idpagar }}).then(function (response) {
            $scope.listaDocumentos = response.data;
          });
        }

      });

    });

  }

  $scope.uploadDocumento = (file) => {

    let nomefoto = '';
    if(file){
      if (file.type !== null && file.type !== undefined) {

        if (file.type.substr(0, 5) == 'image' || file.type == 'application/pdf') {

          if (file.type == 'application/pdf') {
              
            nomefoto = $scope.idpagar + '/' + makeid() + '.' + file.type.substr(12, 3);
            
            S3UploadService.Upload(file, buckets.documentos_pagar , nomefoto).then(function (result) {
            // Mark as success
            file.Success = true;

              $http.post(urlServidor.urlServidorChatAdmin + '/pagar/incluir-documento', {id_pagar: $scope.idpagar, documento: nomefoto}
                ).then(function (response) {

                $scope.listaDocumentos.push({documento: nomefoto, id: response.data.insertId, id_pagar: $scope.idpagar});

              })

              
              if ( $scope.idPagarParcelas ){
              
                for(let item of $scope.idPagarParcelas) {

                  $http.post(urlServidor.urlServidorChatAdmin + '/pagar/incluir-documento', {id_pagar: item.id, documento: nomefoto}
                    ).then(function (response) {

                  })

                }
              }else{
                return
              }


            }, function (error) {
              // Mark the error
              $scope.Error = error;
            }, function (progress) {
              // Write the progress as a percentage

              file.Progress = (progress.loaded / progress.total) * 100
            });


          } else {
            myFunctions.showAlert("Formato inválido!");
          }

        }

      }
    }

  }

function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < 7; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
}



function buscaCentroCusto() {
  return new Promise(function (resolve, reject) {
      $http.get(urlServidor.urlServidorChatAdmin + '/centro-custo/listar').then(function (response) {
          let dados = response.data
          resolve(dados)

          $scope.centroCustos = dados.filter((item) => {
              return item.ativo == 1
          })
      })
  })
}



  function buscaTipoRecebimento(){
		return new Promise( function( resolve,reject){
			$http.put(urlServidor.urlServidorChatAdmin+'/tipos-recebimento/listar-tipo',{tipo:'PAGAR'}).then( function(response){
				let dados = response.data ;
				resolve(dados) ;
			});
		})
  }

	function buscaCategorias(idgrupo){
		return new Promise( function( resolve,reject){
		  $http.put(urlServidor.urlServidorChatAdmin+'/categorias/listar',{ grupo: idgrupo}).then( function(response){
			let dados = response.data ;
			resolve(dados) ;
		  });
		})
  }

  function buscaPatios(idgrupo){
		return new Promise( function( resolve,reject){
		  $http.put(urlServidor.urlServidorChatAdmin+'/patios/listar-patiosusuario',{ idUsuario: localStorage.getItem('id_usuario'), idEmpresa: localStorage.getItem('id_empresa') }).then( function(response){  
        let dados = response.data ;
        resolve(dados)
		  });
		})
	}


  function buscaFornecedores(){

    return new Promise( function( resolve,reject){

      $http.put(urlServidor.urlServidorChatAdmin+'/fornecedores/listar').then( function(response){

        let dados = response.data ;
        resolve(dados) ;

      });

    })

  }

  function buscaGrupos(){

    return new Promise( function( resolve,reject){

      $http.put(urlServidor.urlServidorChatAdmin+'/grupos/listar').then( function(response){

        let dados = response.data ;
        resolve(dados) ;

      });

    })

  }


	$scope.incluiFornecedor = incluiFornecedor ;

	function incluiFornecedor() {

    var _novo = false ;

		$mdDialog.show({

		    controller: function ($scope, $mdDialog, $mdToast) {

				$scope.fornecedor = {
					razao_nome:'',
					cpf_cnpj: '' ,
					endereco: '' ,
					bairro: '' ,
          cidade: '' ,
          tipo_pessoa: '',
          id_tipo_fornecedor: 0 ,
					cep: '' ,
					uf: '' ,
					email: '' ,
					telefone_comercial: '',
					status: '1' ,
					idempresa: localStorage.getItem('id_empresa')
				}

			$scope.cancel = function () {
			  $mdDialog.cancel();
			};



			$scope.gravaFornecedor = function () {

        if( _novo ){

          $http.post(urlServidor.urlServidorChatAdmin+'/fornecedores/cadastrar', $scope.fornecedor ).then( function(response){
            $rootScope.idFornecedor = response.data.insertId ;
          });

        } else {

          myFunctions.showAlert('CPF / CNPJ já existe, verifique!')
          $rootScope.idFornecedor = $scope.fornecedor.id ;

        }

			};

			$scope.saveAndClose = function () {
			  $scope.gravaFornecedor();
			  $rootScope._fornecedor = $scope.fornecedor;
        $rootScope.fornecedor = $scope.fornecedor
			  $mdDialog.hide();
			};

			$scope.estados = estados.uf ;

			$scope.buscaCep = (cep) => {

				  if(cep.length == 9){

					  viaCep.get(cep).then(function(response){

						  $scope.fornecedor.endereco = response.logradouro.toUpperCase() ;
						  $scope.fornecedor.bairro = response.bairro.toUpperCase() ;
						  $scope.fornecedor.cidade = response.localidade.toUpperCase() ;
						  $scope.fornecedor.uf = response.uf.toUpperCase() ;

					  });

				  }

			}

			$scope.buscaCpfCnpj = (cpfCnpj) => {

				if(cpfCnpj.length == 14 || cpfCnpj.length == 18 ){

					$http.get(urlServidor.urlServidorChatAdmin+'/fornecedores/busca-cpfcnpj', { params: { cpf_cnpj: cpfCnpj }}).then( function(response){

						$scope.motoristas = response.data ;

						if( response.data.length > 0 ){
							$scope.fornecedor.cep = response.data[0].cep ;
							$scope.fornecedor.nome = response.data[0].razao_nome ;
							$scope.fornecedor.endereco = response.data[0].endereco ;
							$scope.fornecedor.bairro = response.data[0].bairro ;
							$scope.fornecedor.cidade = response.data[0].cidade ;
							$scope.fornecedor.uf = response.data[0].uf ;
							$scope.fornecedor.email = response.data[0].email ;
              $scope.fornecedor.telefone = response.data[0].telefone_comercial ;
              $scope.fornecedor.id = response.data[0].id ;
              $scope.fornecedor.cpf_cnpj = response.data[0].cpf_cnpj

              _novo = false ;
						} else {
              _novo = true ;
            }

					});

				}

		    }


		  },
		  templateUrl: 'cadastra-fornecedor.html',
		  preserveScope: true
		}).then(function(answer) {
      let a = answer ;
      buscaFornecedores().then( function(response){
        $scope.$resolve.$scope.fornecedores = response ;
        $scope.$resolve.$scope.dados.id_fornecedor =  $rootScope.idFornecedor ;

        let filtraFornecedor = $scope.$resolve.$scope.fornecedores.filter(function (obj) {
          return obj.razao_nome == $scope.fornecedor.razao_nome
        
        })
        
        let filtraId = filtraFornecedor.map(function (item) {
          return item.id
        })

        $scope.dados.nome = $scope.$resolve.$scope.fornecedor.razao_nome
        $scope.dados.id_fornecedor = parseInt(filtraId)
        $scope.dados.cpf_cnpj = $scope.$resolve.$scope.fornecedor.cpf_cnpj
        
        $scope.DadosGeraisClientes.nome = $scope.$resolve.$scope.fornecedor.razao_nome
        $scope.DadosGeraisClientes.id = parseInt(filtraId)
        $scope.DadosGeraisClientes.cpf_cnpj = $scope.$resolve.$scope.fornecedor.cpf_cnpj

      });
		}, function() {
			$scope.statusdialog = 'You cancelled the dialog.';
		});

	};


});
