app.controller('veiculosEditarCtrl', function ($scope, $location, $http, urlServidor, myFunctions, $filter, $sce, urlImagens, $mdDialog, buckets, S3UploadService) {
   
   $scope.urlDocumentos = urlImagens.urlDocumentosVeiculos
   
    $scope.trustSrc = function(src) {
        return  $sce.trustAsResourceUrl(src)
    }


    var bucket = buckets.documentos_juridico

    var urlParams = $location.search();    
    

    $scope.dados = {
        id: urlParams.dados[0].id,
        empresa: urlParams.dados[0].id_empresa,
        descricao: urlParams.dados[0].descricao,
        placa: urlParams.dados[0].placa,
        tipo: urlParams.dados[0].tipo,
        categoria: urlParams.dados[0].categoria,
        ativo: urlParams.dados[0].ativo,
        observacao: urlParams.dados[0].observacao,
        vencimento_seguro: $filter('date')(urlParams.dados[0].vencimento_seguro, 'dd/MM/yyyy', "+0000"),
        vencimento_crlv: $filter('date')(urlParams.dados[0].vencimento_crlv, 'dd/MM/yyyy', "+0000"),
        pdf_seguro: urlParams.dados[0].pdf_seguro,
        pdf_crlv: urlParams.dados[0].pdf_crlv,
        ano_fabricacao: urlParams.dados[0].ano_fabricacao,
        ano_modelo: urlParams.dados[0].ano_modelo,
        chassi: urlParams.dados[0].chassi,
        nome_proprietario: urlParams.dados[0].nome_proprietario,
        combustivel: urlParams.dados[0].combustivel,
        cor: urlParams.dados[0].cor
    }

    $scope.pdfCRLV = null
    $scope.pdfSeguro = null


   buscaDocumentosPdf()
        
    
    $scope.uploadPdfSeguro = (file) => {

        let nomefoto = ''
        let _id = $scope.dados.id

        if (file !== null) {

            if (file.type !== null && file.type !== undefined) {

                if (file.type.substr(0, 5) == 'image' || file.type == 'application/pdf') {

                    nomefoto = _id + '-' + makeid() + '.' + file.type.substr(12, 3);
                
                    S3UploadService.Upload(file, buckets.documentos_veiculos, nomefoto).then(function (result) {
                     
                        file.Success = true;

                        $http.post(urlServidor.urlServidorChatAdmin + '/veiculos/incluir-pdf',
                            {
                                id: $scope.dados.id, 
                                pdf_seguro: nomefoto
                               
                            }
                        ).then(function (response) {

                            $scope.pdfSeguro = nomefoto

                        })
                        
                    }, function (error) {
                       
                        $scope.Error = error;
                    }, function (progress) {
                        
                        file.Progress = (progress.loaded / progress.total) * 100
                   })
                  
                }

            }

        }

    }




    $scope.uploadPdfCRLV = (file) => {

        let nomefoto = ''
        let _id = $scope.dados.id

        if (file !== null) {

            if (file.type !== null && file.type !== undefined) {

                if (file.type.substr(0, 5) == 'image' || file.type == 'application/pdf') {

                    nomefoto = _id + '-' + makeid() + '.' + file.type.substr(12, 3);
                
                    S3UploadService.Upload(file, buckets.documentos_veiculos, nomefoto).then(function (result) {
                     
                        file.Success = true;

                        $http.post(urlServidor.urlServidorChatAdmin + '/veiculos/incluir-pdf',
                            {
                                id: $scope.dados.id, 
                                pdf_crlv: nomefoto
                               
                            }
                        ).then(function (response) {
                                
                            $scope.pdfCRLV = nomefoto

                        })
                        
                    }, function (error) {
                       
                        $scope.Error = error;
                    }, function (progress) {
                        
                        file.Progress = (progress.loaded / progress.total) * 100
                   })
                  
                }

            }

        }

    }



    $scope.apagarPdfSeguro = () => {
               
        let confirm = $mdDialog.confirm()
        .title('Atenção !!!')
        .textContent('Tem certeza que deseja apagar o documento?')
        .ariaLabel('Lucky day')
        .ok('Apagar')
        .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {

            $http.post(urlServidor.urlServidorChatAdmin + '/veiculos/apagar-pdf', { id: $scope.dados.id, pdf_seguro: $scope.pdfSeguro }).then(function (response){
                
                if(response.data.code){
                    
                    myFunctions.showAlert('Não foi possível apagar o documento!');
                    
                }else{
                    myFunctions.showAlert('Documento apagado!');
                }
            });
            
            setTimeout(() => {

                buscaDocumentosPdf()

            }, 1000)
                   
        })

    }


    $scope.apagarPdfCRLV = () => {
               
        let confirm = $mdDialog.confirm()
        .title('Atenção !!!')
        .textContent('Tem certeza que deseja apagar o documento?')
        .ariaLabel('Lucky day')
        .ok('Apagar')
        .cancel('Cancelar');
        $mdDialog.show(confirm).then(function () {

            $http.post(urlServidor.urlServidorChatAdmin + '/veiculos/apagar-pdf', { id: $scope.dados.id, pdf_crlv: $scope.pdfCRLV }).then(function (response){
                
                if(response.data.code){
                    
                    myFunctions.showAlert('Não foi possível apagar o documento!');
                    
                }else{
                    myFunctions.showAlert('Documento apagado!');
                }
            });
            
            setTimeout(() => {

                buscaDocumentosPdf()

            }, 1000)
                   
        })

    }


    
    buscaEmpresas().then(function (result) {
        $scope.empresas = result;
    });
    
  

    
    $scope.gravar = function () {

        $http.post(urlServidor.urlServidorChatAdmin + '/veiculos/alterar', $scope.dados).then(function (response) {

            if (response.data.code) {

                myFunctions.showAlert('Erro na gravação!')

            } else {

                if (response.data == '11000') {
                    myFunctions.showAlert('Veículo já existe!')
                } else {
                    myFunctions.showAlert('Alteração executada com sucesso!');

                }

            };
        });
    };



    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }



    function buscaDocumentosPdf () {
        
        return new Promise(function (resolve, reject) {
        
            const response = $http.get(urlServidor.urlServidorChatAdmin + '/veiculos/busca-pdf', {params: {id: $scope.dados.id}}).then(function (response) {
        
            let dados = response.data
           
            if (dados.length == 0){

                return
            }

            $scope.pdfCRLV = dados[0].pdf_crlv
            $scope.pdfSeguro = dados[0].pdf_seguro

           resolve(dados);
            })
        })
        
    }



    function buscaEmpresas() {

        return new Promise(function (resolve, reject) {

            $http.put(urlServidor.urlServidorChatAdmin + '/empresas/listar').then(function (response) {

                let dados = response.data;
                dados.sort(function (a, b) {
                    if (a.nome < b.nome) {
                        return 1;
                    }
                    if (a.nome > b.nome) {
                        return -1;
                    }
                    return 0;
                });
                resolve(dados);
            });
        })

    }



});
